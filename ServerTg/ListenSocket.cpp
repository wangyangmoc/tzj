// ListenSocket.cpp : 实现文件
//

#include "stdafx.h"
#include "ServerTg.h"
#include "ListenSocket.h"
#include "ServerSocket.h"

// CListenSocket

CListenSocket::CListenSocket()
{
}

CListenSocket::~CListenSocket()
{
}


// CListenSocket 成员函数


void CListenSocket::OnAccept(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	lt::call<void>(theApp.m_l, "ListenProc", "OnAccept", this, nErrorCode);
	CAsyncSocket::OnAccept(nErrorCode);
}
