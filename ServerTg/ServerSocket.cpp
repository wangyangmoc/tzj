// ServerSocket.cpp : 实现文件
//

#include "stdafx.h"
#include "ServerTg.h"
#include "ServerSocket.h"


// CServerSocket

CServerSocket::CServerSocket()
{
}

CServerSocket::~CServerSocket()
{
}


// CServerSocket 成员函数


void CServerSocket::OnClose(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	lt::call<void>(theApp.m_l, "ServerProc", "OnClose", this, nErrorCode);
	CAsyncSocket::OnClose(nErrorCode);
}


void CServerSocket::OnConnect(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	lt::call<void>(theApp.m_l, "ServerProc", "OnConnect", this, nErrorCode);
	CAsyncSocket::OnConnect(nErrorCode);
}


void CServerSocket::OnReceive(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	lt::call<void>(theApp.m_l, "ServerProc", "OnReceive", this, nErrorCode);
	CAsyncSocket::OnReceive(nErrorCode);
}
