// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include <atlconv.h>
#include <sstream>
#include "../msktools/msktools.h"
#include "../nomodule/MemoryModule.h"
DWORD _CreateFileW = (DWORD)GetProcAddress(GetModuleHandleA("Kernel32.dll"), "CreateFileW") + 5;
HANDLE
WINAPI
MyCreateFileW(LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
{
	__asm jmp _CreateFileW
}

/* 版本2
HANDLE __declspec(naked) __stdcall
MyCreateFileA(
_In_ LPCSTR lpFileName,
_In_ DWORD dwDesiredAccess,
_In_ DWORD dwShareMode,
_In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
_In_ DWORD dwCreationDisposition,
_In_ DWORD dwFlagsAndAttributes,
_In_opt_ HANDLE hTemplateFile
)
{
__asm{
mov edi, edi
push ebp
mov ebp, esp
jmp _CreateFileA
}
}
*/

typedef void(*BeginGameHack)(int);

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		//MessageBoxA(NULL, "", "", 0);
	    PRT("dll loader entry");
		FILE *fp = fopen("C:\\mskd", "r");
		if (NULL == fp){
			PRT("mskd cant find");
			return FALSE;
		}
		char appPath[MAX_PATH] = {0};
		fread(appPath, MAX_PATH, 1, fp);
		fclose(fp);
		int port = 0;
		for (auto i = 0; i < strlen(appPath);i++){
			if (appPath[i] == ','){
				appPath[i] = 0;
				port = atoi(appPath+i+1);
				break;
			}
		}
#ifndef _VER_RELEASE
		strcat(appPath, "dll_template.dll");
		HMODULE hDll = LoadLibraryA(appPath);
		auto bg = (BeginGameHack)GetProcAddress(hDll, "BeginGameHack");
		bg(port);
#else
		strcat_s(appPath, MAX_PATH, "mskdata:msk.dll");
		USES_CONVERSION;
		HANDLE hOpenFile = MyCreateFileW(A2W(appPath), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (INVALID_HANDLE_VALUE == hOpenFile) {
			std::stringstream ss;
			ss << "createfile error lasterr:" << GetLastError();
			PRT(ss.str().c_str());
			return FALSE;
		}
		char *pBuffer;
		DWORD RSize;
		DWORD fileSize = 0;
		fileSize = GetFileSize(hOpenFile, NULL);
		pBuffer = new char[fileSize];
		ReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
		HMEMORYMODULE handle = MemoryLoadLibrary(pBuffer);
		if (handle == NULL){
			PRT("msk_dl:MemoryLoadLibrary failed");
		}
		else{
			BeginGameHack bg = (BeginGameHack)MemoryGetProcAddress(handle, "BeginGameHack");
			bg(port);
		}
		PRT("MemLoadLibrary ok");
		delete []pBuffer;
		CloseHandle(hOpenFile);
#endif
		return FALSE;
	}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return FALSE;
}

