#ifndef _FUNCTION_H_
#define _FUNCTION_H_
unsigned int __stdcall GameHackStart(void *);
void HanckINit();

//lua
lua_State * InitLuaBase(lua_State *&gl);
void InitGameLuaState(lua_State *L);
const char* lmc_getfile(const char *fanme,size_t *sz);
void RegisterGameApi(lua_State *L, bool bGame = false);

//tools
int GetRandomNum(int iMin, int iMax);
void inline_hook(int hookaddr, int newaddr, int codelen, char *code=nullptr);
int SerchCode(char* code, int len);
void inline_unhook(int hookaddr, char *code, int codelen);

//protect
void mskL_setProtect();
void mskL_SetProtectThreadId(DWORD tid);

#endif // !_FUNCTION_H_
