#include "stdafx.h"
#include "function.h"
#include "../mhook-lib/mhook.h"
//game fun
//lua function by msk
int lg_selectchar(lua_State *L)
{
	DWORD callAddress = lua_tonumber(L, 1);
	DWORD dwEdi = lua_tonumber(L, 2);
	DWORD charId = lua_tonumber(L, 3);
	__asm{
		pushad
			mov esi, charId
			mov edi, dwEdi
			mov eax, callAddress
			SAFECALL(eax)
			popad
	}
	return 0;
}

int lg_castareaskill(lua_State *L)
{
	DWORD callAddress = lua_tonumber(L, 1);
	DWORD dwEdi = lua_tonumber(L, 2);
	DWORD skillid = lua_tonumber(L, 3);
	float x = lua_tonumber(L, 4);
	float y = lua_tonumber(L, 5);
	__asm{
		pushad
			or eax, 0xffffffff
			push y
			push x
			mov ecx, skillid
			push ecx
			mov edi, dwEdi
			mov ebx, callAddress
			SAFECALL(ebx)
			popad
	}
	return 0;
}

int lg_openchar(lua_State *L)
{
	DWORD callAddress = lua_tonumber(L, 1);
	DWORD dwEsi = lua_tonumber(L, 2);
	DWORD charId = lua_tonumber(L, 3);
	__asm{
		pushad
			mov eax, charId
			push dwEsi
			mov ebx, callAddress
			SAFECALL(ebx)
			popad
	}
	return 0;
}

int lg_unhookbuff(lua_State *L)
{
	static bool bUnLock = false;
	if (bUnLock) return 0;
	bUnLock = true;
	Mhook_Unhook((PVOID *)&g_luaL_loadbuffer);
	return 0;
}

int lg_netsend(lua_State *L)
{
	WSABUF DataBuf;
	DWORD SendBytes;
	size_t len;
	char* buffer = (char*)lua_tolstring(L, 1, &len);
	char newbuf[2048];
	memcpy(newbuf, buffer, len);
	for (int i = 0; i < len; ++i){
		newbuf[i] ^= 'm';
	}
	DataBuf.len = len;
	DataBuf.buf = newbuf;

	int rc = WSASend(g_sockClient, &DataBuf, 1,
		&SendBytes, 0, NULL, NULL);
	lua_pushinteger(L,rc);
	return 1;
}

static const luaL_Reg mskgfuncs[] =
{
	{ "castareaskill", lg_castareaskill },
	{ "unhookbuff", lg_unhookbuff },
	{ "selectchar", lg_selectchar },
	{ "openchar", lg_openchar },
	{ "netsend", lg_netsend },
	{NULL,NULL}
};

void RegisterGameApi(lua_State *L,bool bGame)
{
	//lua_pushstring(L, gAppPath);
	//lua_setglobal(L, "gapppath");
#ifndef _VER_RELEASE
	lua_pushnumber(L,1);
#else
	lua_pushnumber(L, 2);
#endif
	lua_setglobal(L, "pt_lv_cur");
	if (bGame)
		g_luaL_register(L, "mskg", mskgfuncs);
	else
		luaL_register(L, "mskg", mskgfuncs);
}