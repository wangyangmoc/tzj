#include "stdafx.h"
#include "../mhook-lib/mhook.h"
#include <TlHelp32.h>
#include <exception>
#include <random>
int GetRandomNum(int iMin, int iMax)
{
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_int_distribution<> dis(iMin, iMax);
	return dis(mt);
}

void inline_hook(int hookaddr, int newaddr, int codelen, char *code )
{
	if (codelen < 5)
		MessageBoxA(NULL, "coodelen too short", "error", MB_OK);
	DWORD old;
	VirtualProtect((LPVOID)hookaddr, codelen, PAGE_READWRITE, &old);
	if (code != nullptr)
		memcpy(code, (void *)hookaddr, codelen);
	int jmpaddr = newaddr - hookaddr - 5;
	*(char *)hookaddr = 0xe9;
	hookaddr++;
	*(int *)hookaddr = jmpaddr;
	hookaddr += 4;
	for (int i = 0; i < (codelen - 5); ++i)
	{
		*(char *)hookaddr = 0x90;
		hookaddr++;
	}
	VirtualProtect((LPVOID)hookaddr, codelen, old, &old);
}

int SerchCode(char* code, int len)
{
	int addr = 0x00401000;
	while (addr < 0xffffffff)
	{
		if (memcmp((void *)addr, code, len) == 0)
			return addr;
	}
	return 0;
}

void change_code(int addr, int codelen, char *code)
{
	DWORD old;
	VirtualProtect((LPVOID)addr, codelen, PAGE_READWRITE, &old);
	memcpy((void *)addr, code, codelen);
	VirtualProtect((LPVOID)addr, codelen, old, &old);
}

void inline_unhook(int hookaddr, char *code, int codelen)
{
	DWORD old;
	VirtualProtect((LPVOID)hookaddr, codelen, PAGE_READWRITE, &old);
	memcpy((void *)hookaddr, code, codelen);
	VirtualProtect((LPVOID)hookaddr, codelen, old, &old);
}


char duokai_code[] = { 0x3d, 0xb7, 0x00, 0x00, 0x00 };//多开修改位置特征码
void duokai()
{
	int addr = SerchCode(duokai_code, 5);
	char ar[1] = { 0xeb };
	if (addr != 0)
		inline_unhook(addr + 5, ar, 1);
}

typedef BOOL(WINAPI *fQueryThread)(HANDLE hSnapshot, LPTHREADENTRY32 lpte);
fQueryThread trueThread32First;
fQueryThread trueThread32Next;
DWORD threadNeedProtect[20] = { 0 };
BOOL WINAPI mskPH_Thread32First(HANDLE hSnapshot, LPTHREADENTRY32 lpte)
{
	BOOL ret = trueThread32First(hSnapshot, lpte);
	for (int i = 0; i < sizeof(threadNeedProtect); ++i){
		if (threadNeedProtect[i] == lpte->th32ThreadID){
			lpte->th32OwnerProcessID = GetCurrentProcessId();
			lpte->th32ThreadID = GetCurrentThreadId();
		}
	}
	return ret;
}

BOOL WINAPI mskPH_Thread32Next(HANDLE hSnapshot, LPTHREADENTRY32 lpte)
{
	BOOL ret = trueThread32Next(hSnapshot, lpte);
	for (int i = 0; i < sizeof(threadNeedProtect); ++i){
		if (threadNeedProtect[i] == lpte->th32ThreadID){
			lpte->th32OwnerProcessID = GetCurrentProcessId();
			lpte->th32ThreadID = GetCurrentThreadId();
		}
	}
	return ret;
}

void mskL_SetProtectThreadId(DWORD tid)
{
	for (int i = 0; i < sizeof(threadNeedProtect); ++i){
		if (threadNeedProtect[i] == 0){
			threadNeedProtect[i] = tid;
			return;
		}
	}
	threadNeedProtect[0] = tid;
}

void mskP_ProtectThread()
{
	trueThread32First = Thread32First;
	trueThread32Next = Thread32Next;
	Mhook_SetHook((PVOID *)&trueThread32First, mskPH_Thread32First);
	Mhook_SetHook((PVOID *)&trueThread32Next, mskPH_Thread32Next);
}

HOOKAPIPRE(GetActiveWindow);
HWND WINAPI myGetActiveWindow1()
{
	if (g_hGame)
		return g_hGame;
	return trueGetActiveWindow();
}

HOOKAPIPRE(GetForegroundWindow);
HWND WINAPI myGetForegroundWindow()
{
	if (g_hGame)
		return g_hGame;
	return trueGetForegroundWindow();
}

HOOKAPIPRE(IsIconic);
BOOL WINAPI myIsIconic(HWND hwnd)
{
	return FALSE;
}


HOOKAPIPRE(Process32FirstW);
HOOKAPIPRE(Process32NextW);
BOOL WINAPI myProcess32FirstW(HANDLE hSnapshot, LPPROCESSENTRY32W lppe)
{
	BOOL ret = trueProcess32FirstW(hSnapshot, lppe);
	while (ret){
		if (lppe->th32ProcessID == GetCurrentProcessId()){
			return TRUE;
		}
		ret = trueProcess32NextW(hSnapshot, lppe);
	}
	return ret;
}

BOOL WINAPI myProcess32NextW(HANDLE hSnapshot, LPPROCESSENTRY32W lppe)
{
	return FALSE;
}

WCHAR *skipModule[] = {
	L"msvcp120.dll",
	L"msvcr120.dll",
};

HOOKAPIPRE(Module32FirstW);
HOOKAPIPRE(Module32NextW);
BOOL WINAPI myModule32FirstW(HANDLE hSnapshot, LPMODULEENTRY32W lppe)
{
	BOOL ret = trueModule32FirstW(hSnapshot, lppe);
	bool bFind;
	while (ret){
		bFind = false;
		for (int i = 0; i < sizeof(skipModule); ++i){
			if (wcsstr(lppe->szModule, skipModule[i]) != NULL){
				PRT("skip module:%d", i);
				bFind = true;
				break;
			}
		}
		if (false == bFind) break;
		ret = trueModule32NextW(hSnapshot, lppe);
	}
	return ret;
}

BOOL WINAPI myModule32NextW(HANDLE hSnapshot, LPMODULEENTRY32W lppe)
{
	BOOL ret = trueModule32NextW(hSnapshot, lppe);
	bool bFind;
	while (ret){
		bFind = false;
		for (int i = 0; i < sizeof(skipModule); ++i){
			if (wcsstr(lppe->szModule, skipModule[i]) != NULL){
				bFind = true;
				break;
			}
		}
		if (false == bFind) break;
		ret = trueModule32NextW(hSnapshot, lppe);
	}
	return ret;
}

DWORD addrGetActiveWindow = (DWORD)GetActiveWindow;
DWORD newAddress;
__declspec(naked) void myGetActiveWindow()
{
	__asm{
		mov eax,g_hGame
		cmp eax,0
		je GETWINBDOWOK
		push 1
		call newAddress
GETWINBDOWOK:
		retn
	}
}

void mskL_setProtect()
{
#ifdef _VER_RELEASE
	newAddress = *(DWORD *)(addrGetActiveWindow + 3) + addrGetActiveWindow + 3 + 4;
	inline_hook(addrGetActiveWindow, (int)myGetActiveWindow,5,NULL);

																//HOOKAPI(GetActiveWindow);
	HOOKAPI(GetForegroundWindow);
	HOOKAPI(IsIconic);
#endif
	HOOKAPI(Process32FirstW);
	HOOKAPI(Process32NextW);
	HOOKAPI(Module32FirstW);
	HOOKAPI(Module32NextW);
	mskP_ProtectThread();
}