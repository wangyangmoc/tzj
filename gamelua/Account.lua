function Account_OnLoad()
  gUI_CurrentStage = 2
  local root = WindowSys_Instance:GetRootWindow()
  if root then
    WindowSys_Instance:SetKeyboardCaptureWindow(root)
  end
  local wnd = WindowSys_Instance:GetWindow("Messagebox")
  wnd:SetProperty("Visible", "false")
end
