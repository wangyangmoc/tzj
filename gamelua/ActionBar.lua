if gUI and not gUI.ActBr then
  gUI.ActBr = {}
end
local MainMenuRoot = -1
local SKILLID = 0
local STRUTPOS = -1
local TARGETPOSX = -1
local TARGETPOSY = -1
local Act_Struct_ItemTemp = {}
local Act_Struct_ItemWndList = {}
local AOT_EMPTY = 0
local AOT_SKILL = 1
local Refush_Eff_Times = 0
local NeedRefush = false
local ITEM_EFF_USERGERIDE = 1
local _ActBr_ActionBarOffset = {
  action_main = 0,
  action_side1 = 30,
  action_side2 = 40,
  action_side3 = 52,
  endFlag = 64
}
function _ActBr_GetWinAndOffest(slot)
  local action_bar
  if slot < _ActBr_ActionBarOffset.action_side1 then
    local ceiling = gUI.ActBr.CurMainBarPage * 10
    local floor = ceiling - 10
    if slot < ceiling and slot >= floor then
      action_bar = gUI.ActBr.Main
      slot = slot - floor
    end
  elseif slot < _ActBr_ActionBarOffset.action_side2 then
    action_bar = gUI.ActBr.Side1
    slot = slot - _ActBr_ActionBarOffset.action_side1
  elseif slot < _ActBr_ActionBarOffset.action_side3 then
    action_bar = gUI.ActBr.Side2
    slot = slot - _ActBr_ActionBarOffset.action_side2
  end
  return action_bar, slot
end
function _ActBr_UpdatePageLabel(cur_page)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Page_dlab")
  local wndUp = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Up_btn")
  local wndDown = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Down_btn")
  if cur_page == 3 then
    wndDown:SetProperty("Enable", "false")
    wndUp:SetProperty("Enable", "true")
  elseif cur_page == 1 then
    wndDown:SetProperty("Enable", "true")
    wndUp:SetProperty("Enable", "false")
  else
    wndDown:SetProperty("Enable", "true")
    wndUp:SetProperty("Enable", "true")
  end
  if cur_page <= 3 and cur_page >= 1 then
    gUI.ActBr.CurMainBarPage = cur_page
    wnd:SetProperty("Text", tostring(cur_page))
  end
end
function _ActBr_UpdataAllIcon()
  for i = 0, 64 do
    WorldStage:UpdataAction(i)
  end
end
function _ActBr_SetActionbtn(from_type, from_off, to_index, spellid)
  if Lua_Bag_IsBackPackbox(from_type) then
    local num, id, quality, name, IdTable, class = GetItemBaseInfoBySlot(gUI_GOODSBOX_BAG[from_type], from_off)
    local purpose = class == 2 and 6 or 3
    if IdTable < 5000 then
      Lua_Chat_ShowOSD("PET_ACTION_BAR_NO")
      return
    end
    WorldStage:setActionBtn(to_index, purpose, gUI_GOODSBOX_BAG[from_type], from_off)
    UserGuideCompleteEvent(9, IdTable)
  elseif from_type == "skill2" then
    WorldStage:setActionBtn(to_index, 2, 0, spellid)
  elseif Lua_Bag_IsSkillBar(from_type) then
    WorldStage:setActionBtn(to_index, 1, 0, spellid)
    UserGuideCompleteEvent(9, spellid)
  elseif from_type == "motion" then
    WorldStage:setActionBtn(to_index, 4, 0, spellid)
  end
end
function Lua_ActBr_GetIndexByOffset(barType, offset)
  local begin = 0
  if barType ~= "action_main" then
    begin = _ActBr_ActionBarOffset[barType]
  else
    begin = (gUI.ActBr.CurMainBarPage - 1) * 10
  end
  return begin + offset
end
function Lua_ActBr_Lock()
  UI_ActBr_Lock()
end
function Lua_ActBr_UnLock()
  UI_ActBr_Lock()
end
function UI_ActBr_ScrolBtnClick(msg)
  local actionbar = WindowToGoodsBox(gUI.Bag.GoodsBoxBag:GetParent())
  actionbar:ResetDragItem()
  local wnd = msg:get_window()
  local offset = wnd:GetProperty("WindowName") == "Down_btn" and 1 or -1
  _ActBr_UpdatePageLabel(gUI.ActBr.CurMainBarPage + offset)
  UpdateActionBtnSet(gUI.ActBr.CurMainBarPage, 10)
  if gUI.ActBr.CurMainBarPage == 1 then
    _OnActBr_HideByMax(0, 29)
    _OnActBr_ShowByMax(0, 9)
  elseif gUI.ActBr.CurMainBarPage == 2 then
    _OnActBr_HideByMax(0, 29)
    _OnActBr_ShowByMax(10, 19)
  elseif gUI.ActBr.CurMainBarPage == 3 then
    _OnActBr_HideByMax(0, 29)
    _OnActBr_ShowByMax(20, 29)
  end
end
function UI_ActBr_RBtnClick(msg)
  local offset = msg:get_lparam()
  local action_bar = WindowToGoodsBox(msg:get_window())
  local index = Lua_ActBr_GetIndexByOffset(action_bar:GetProperty("BoxType"), offset)
  if msg:get_wparam() ~= 1 and IsKeyPressed(56) then
    WorldStage:SetSkillTargetToSelf(true)
  end
  WorldStage:useActionBtn(index)
  WorldStage:SetSkillTargetToSelf(false)
end
function UI_ActBr_LBtnClick(msg)
  gUI.ActBr.DragIconFlag = true
  UI_ActBr_RBtnClick(msg)
  WorldStage:playSoundByID(28)
end
function UI_ActBr_CastVehicleSkill(msg)
  local index = msg:get_lparam()
  local spellid = gUI.ActBr.Vehiclegb:GetItemData(index)
  CastSkill(spellid)
end
function UI_ActBr_CastTemporarySkill(msg)
  local index = msg:get_lparam()
  local spellid = gUI.ActBr.Temporary:GetItemData(index)
  CastSkill(spellid)
end
function UI_ActBr_MouseDown(msg)
  if msg:get_wparam() == 0 then
    gUI.ActBr.DragIconFlag = false
  end
end
function UI_ActBr_ItemDrop(msg)
  local to_off = msg:get_lparam()
  local to_win = msg:get_window()
  to_win = WindowToGoodsBox(to_win)
  local from_win = to_win:GetDragItemParent()
  local from_off = msg:get_wparam()
  local to_type = to_win:GetProperty("BoxType")
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" or from_type == "backpack1" then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
  end
  if Lua_Bag_IsActionbtn(from_type) then
    if from_type == to_type and from_off == to_off then
      return
    else
      local src_index = Lua_ActBr_GetIndexByOffset(from_type, from_off)
      local dest_index = Lua_ActBr_GetIndexByOffset(to_type, to_off)
      WorldStage:swapActionBtn(src_index, dest_index)
    end
  elseif from_type == "temp" then
    if to_win:GetItemIcon(to_off) ~= "" then
      to_win:SetDragItem(to_off)
      to_win:SetNotClearDragIcon()
    end
    WorldStage:SetNewAction(Lua_ActBr_GetIndexByOffset(to_type, to_off))
  elseif not Lua_Bag_IsMerchantBar(from_type) then
    if to_win:GetItemIcon(to_off) ~= "" then
      to_win:SetDragItem(to_off)
      to_win:SetNotClearDragIcon()
    end
    from_win = WindowToGoodsBox(from_win)
    WorldStage:SaveOldActionData(Lua_ActBr_GetIndexByOffset(to_type, to_off))
    _ActBr_SetActionbtn(from_type, from_off, Lua_ActBr_GetIndexByOffset(to_type, to_off), from_win:GetItemData(from_off))
  end
  WorldStage:playSoundByID(29)
end
function UI_ActBr_ShowTips(msg)
  local goodsbox = msg:get_window()
  local itemIndex = Lua_ActBr_GetIndexByOffset(goodsbox:GetProperty("BoxType"), msg:get_wparam())
  local icon_type, info1, info2 = GetActionBtnInfoByIndex(itemIndex)
  local tooltip = goodsbox:GetToolTipWnd(0)
  if icon_type == 1 then
    local currentlv = GetPlayerSkillLv(info1)
    Lua_Tip_Skill(tooltip, info1, currentlv, false, false)
  elseif icon_type == 3 or icon_type == 6 then
    if info2 ~= -1 then
      Lua_Tip_Item(tooltip, info1, info2, nil, nil, nil, nil, FORM_TYPE_SHORTCUT)
    else
      Lua_Tip_Item(tooltip, nil, nil, nil, nil, info1, nil, FORM_TYPE_SHORTCUT)
    end
  elseif icon_type == 4 then
  end
end
function UI_ActBr_ShowVehicleTips(msg)
  local index = msg:get_wparam()
  local spellid = gUI.ActBr.Vehiclegb:GetItemData(index)
  local goodsbox = msg:get_window()
  local tooltip = goodsbox:GetToolTipWnd(0)
  Lua_Tip_Skill(tooltip, spellid, 1, false, false)
end
function UI_ActBr_ShowTemporaryTips(msg)
  local index = msg:get_wparam()
  local spellid = gUI.ActBr.Temporary:GetItemData(index)
  local goodsbox = msg:get_window()
  local tooltip = goodsbox:GetToolTipWnd(0)
  local currentlv = GetPlayerSkillLv(spellid)
  Lua_Tip_Skill(tooltip, spellid, currentlv, false, false)
end
function UI_ActBr_Lock(msg)
  local lockBtn = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Lock_btn")
  local unLockBtn = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Unlock_btn")
  if lockBtn:IsVisible() == true then
    SetActionBarLockState(true)
    gUI.ActBr.Main:SetProperty("ItemCanDrag", "false")
    gUI.ActBr.Side1:SetProperty("ItemCanDrag", "false")
    gUI.ActBr.Side2:SetProperty("ItemCanDrag", "false")
    lockBtn:SetProperty("Visible", "false")
    lockBtn:SetProperty("Enable", "false")
    unLockBtn:SetProperty("Visible", "true")
    unLockBtn:SetProperty("Enable", "true")
  else
    SetActionBarLockState(false)
    gUI.ActBr.Main:SetProperty("ItemCanDrag", "true")
    gUI.ActBr.Side1:SetProperty("ItemCanDrag", "true")
    gUI.ActBr.Side2:SetProperty("ItemCanDrag", "true")
    lockBtn:SetProperty("Visible", "true")
    lockBtn:SetProperty("Enable", "true")
    unLockBtn:SetProperty("Visible", "false")
    unLockBtn:SetProperty("Enable", "false")
  end
end
function UI_ActBr_SystemBtnClick(msg)
  local option = WindowSys_Instance:GetWindow("SystemOption")
  option:SetProperty("Visible", tostring(not option:IsVisible()))
  if IsFinal() then
    WindowSys_Instance:GetWindow("SystemOption.DisplayAdjust"):SetProperty("Visible", "false")
  end
end
function UI_ActBr_ShowToolBarTip(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local index = tonumber(wnd:GetProperty("CustomUserData"))
  Lua_Tip_ToolBar(tooltip, index)
end
function UI_ActBr_LeaveMultiMount()
  LeaveMultiMount()
end
function UI_ActBar_MouseEnter()
end
function UI_ActBar_MouseLeave()
end
function _OnActBr_ChangeCombatForm(combat_form_id, vehicletype)
  local name, menpai, gender, head, head_model, hair_model, hair_color = GetMyPlayerStaticInfo()
  local v1, v2
  local MainMenuBar = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg")
  local BtnsBar = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg")
  local expbar = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Exp_bar")
  if combat_form_id == -1 then
    v1 = "true"
    v2 = "false"
    _OnActBr_CoolDown = _OnActBr_CoolDownNormal
    UnbindVehicleHotKey()
    SetHotKey(2)
    WindowSys_Instance:GetWindow("SystemOption.Key"):SetProperty("Enable", "true")
  else
    v1 = "false"
    v2 = "true"
    gUI.ActBr.Vehiclegb:ResetAllGoods(true)
    for index = 0, 4 do
      local skill_id, skill_icon = GetTemporarySkillInfo(combat_form_id, index)
      if skill_id then
        gUI.ActBr.Vehiclegb:SetItemGoods(index, skill_icon, -1)
        gUI.ActBr.Vehiclegb:SetItemData(index, skill_id)
      end
    end
    _OnActBr_CoolDown = _OnActBr_CoolDownVehicle
    BindVehicleHotKey()
    WindowSys_Instance:GetWindow("SystemOption.Key"):SetProperty("Enable", "false")
  end
  gUI.ActBr.Main_bg:SetProperty("Visible", v1)
  gUI.ActBr.Side1:SetProperty("Visible", v1)
  gUI.ActBr.Side2:SetProperty("Visible", v1)
  MainMenuBar:SetProperty("Visible", v1)
  BtnsBar:SetProperty("Visible", v1)
  expbar:SetProperty("Visible", v1)
  gUI.ActBr.Vehiclegb:GetParent():SetProperty("Visible", v2)
  gUI.ActBr.RightEff:SetProperty("Visible", v1)
  if v2 == "true" then
    _OnActBr_HideByMax(0, 50)
  else
    _OnActBr_ShowByMax(0, 50)
  end
end
function _OnActBr_AddIcon(index, icon, shortcut_type, data, enable, duration, remain, canUse, isEquiped)
  local action_bar, off = _ActBr_GetWinAndOffest(index)
  if action_bar then
    local icon_type, info1, info2 = GetActionBtnInfoByIndex(index)
    if icon_type == 3 or icon_type == 6 then
      if info1 ~= -1 then
        action_bar:SetItemGoods(off, icon, Lua_Bag_GetQuality(info2, info1))
        local intenLev = Lua_Bag_GetStarInfo(info2, info1)
        if ForgeLevel_To_Stars[intenLev] > 0 then
          action_bar:SetItemStarState(off, ForgeLevel_To_Stars[intenLev])
        else
          action_bar:SetItemStarState(off, 0)
        end
      else
        action_bar:SetItemGoods(off, icon, -1)
      end
      action_bar:SetItemEffectFile(off, ITEM_EFF_USERGERIDE, "")
    else
      if Act_Struct_ItemWndList[index] ~= info1 then
        action_bar:SetItemEffectFile(off, ITEM_EFF_USERGERIDE, "")
        Act_Struct_ItemWndList[index] = 0
      end
      action_bar:SetItemGoods(off, icon, -1)
      if icon_type == AOT_SKILL and SKILLID == info1 then
        SetNewStudyEffect(TARGETPOSX, TARGETPOSY, SKILLID, index)
        Act_Struct_ItemWndList[index] = SKILLID
        SKILLID = 0
        STRUTPOS = -1
        TARGETPOSX = -1
        TARGETPOSY = -1
        table.remove(Act_Struct_ItemTemp, 1)
        NeedRefush = true
        Refush_Eff_Times = 0
      end
    end
    if gUI.ActBr.PlayerDead == true then
      action_bar:SetItemEnable(off, false)
    elseif isEquiped ~= 1 then
      action_bar:SetItemEnable(off, enable)
    end
    action_bar:SetIconState(off, 7, canUse == 0)
    action_bar:SetIconState(off, 1, isEquiped == 1)
    if shortcut_type == 3 then
      if data ~= -1 then
        if data > 999 then
          str = "*"
        else
          str = tostring(data)
        end
        action_bar:SetItemSubscript(off, str)
      end
    else
      action_bar:SetItemData(off, data)
    end
    if remain <= 0 then
      action_bar:ClearItemCoolTime(off)
    else
      action_bar:SetItemCoolTime(off, duration, remain)
    end
  end
end
function _OnActBr_TemporarySkillStudy(icon, skillId, enable, category)
  if category == 6 then
    gUI.ActBr.Temporary:SetVisible(true)
    gUI.ActBr.Temporary:SetItemGoods(0, icon, -1)
    gUI.ActBr.Temporary:SetItemData(0, skillId)
    gUI.ActBr.Temporary:SetItemEnable(0, enable)
  end
end
function _OnActBr_TemporarySkillRemove(category, skillid)
  if category == 6 then
    local spellid = gUI.ActBr.Temporary:GetItemData(0)
    if spellid == skillid then
      gUI.ActBr.Temporary:ResetAllGoods(true)
      gUI.ActBr.Temporary:SetVisible(false)
    end
  end
end
function _OnActBr_CoolDownNormal(index, duration, rest)
  local action_bar, slot = _ActBr_GetWinAndOffest(index)
  if action_bar then
    if rest == 0 then
      action_bar:ClearItemCoolTime(slot)
    else
      action_bar:SetItemCoolTime(slot, duration, rest)
    end
  end
end
function _OnActBr_CoolDownVehicle(index, duration, rest)
  local action_bar = gUI.ActBr.Vehiclegb
  if rest == 0 then
    action_bar:ClearItemCoolTime(index)
  else
    action_bar:SetItemCoolTime(index, duration, rest)
  end
end
function _OnActBr_CoolDownTemporary(skillid, category, duration, rest)
  local spellid = gUI.ActBr.Temporary:GetItemData(0)
  local action_bar = gUI.ActBr.Temporary
  if category == 6 and spellid == skillid then
    if rest == 0 then
      action_bar:ClearItemCoolTime(0)
    else
      action_bar:SetItemCoolTime(0, duration, rest)
    end
  end
end
function _OnActBr_DelIcon(slot)
  local action_bar, off = _ActBr_GetWinAndOffest(slot)
  if action_bar and off >= 0 then
    action_bar:ClearGoodsItem(off)
    action_bar:SetItemEnable(off, true)
  end
end
function _OnActBr_PlayerDead()
  gUI.ActBr.PlayerDead = true
  for i = 0, 9 do
    if gUI.ActBr.Main:GetItemIcon(i) ~= "" then
      gUI.ActBr.Main:SetItemEnable(i, false)
    end
    if gUI.ActBr.Side1:GetItemIcon(i) ~= "" then
      gUI.ActBr.Side1:SetItemEnable(i, false)
    end
  end
  for i = 0, 11 do
    if gUI.ActBr.Side2:GetItemIcon(i) ~= "" then
      gUI.ActBr.Side2:SetItemEnable(i, false)
    end
  end
end
function SetNewStudyEffect(tarX, tarY, skillid, Actpos)
  local gb = _OnActBr_GetGbByIndex(Actpos)
  local index = Actpos % 10
  if gb then
    gb:SetItemEffectFile(index, ITEM_EFF_USERGERIDE, "efxc_ui_hint02")
  end
end
function _OnActBr_GetGbByIndex(nIndex)
  local goodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Main_gbox"))
  local goodsBox1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Top_gbox"))
  local goodsBox2 = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Right_gbox"))
  if nIndex <= 29 then
    return goodsBox
  elseif nIndex <= 39 then
    return goodsBox1
  elseif nIndex <= 49 then
    return goodsBox2
  end
end
function _OnActBr_ShowByMax(nMinIndex, nMaxIndex)
end
function _OnActBr_HideByMax(nMinIndex, nMaxIndex)
end
function End_Effect_IndexChange(index, skillid, IconType)
  local gb = _OnActBr_GetGbByIndex(index)
  local iindex = index % 10
  if gb then
    gb:SetItemEffectFile(iindex, ITEM_EFF_USERGERIDE, "")
  end
end
function _OnAct_RemoveSkillEffect(skillid)
  for i = 0, 49 do
    local icon_type, info1, info2 = GetActionBtnInfoByIndex(i)
    if icon_type == AOT_SKILL and info1 == skillid then
      local gb = _OnActBr_GetGbByIndex(i)
      local iindex = i % 10
      if gb then
        gb:SetItemEffectFile(iindex, ITEM_EFF_USERGERIDE, "")
        Act_Struct_ItemWndList[iindex] = 0
      end
    end
  end
end
function ActBar_SetSourceData(sourceX, sourceY, speeds, pic1, effect, skillId)
  ActBr_SetSrollPicTemp(sourceX, sourceY, speeds, pic1, effect, skillId)
end
function ActBr_SetSrollPicTemp(sourceX, sourceY, speeds, pic1, effect, skillId)
  if #Act_Struct_ItemTemp == 0 then
    ActBr_SetScrollPicInfo(sourceX, sourceY, speeds, pic1, effect, skillId)
  end
  local item = {}
  item.sourceX = sourceX
  item.sourceY = sourceY
  item.speeds = speeds
  item.pic1 = pic1
  item.effect = effect
  item.skillId = skillId
  table.insert(Act_Struct_ItemTemp, item)
end
function UI_EndPicture_Scroll(msg)
  local pic = WindowSys_Instance:GetWindow("Scroll_bg")
  pic:SetProperty("Visible", "false")
  pic:SetProperty("EffectFile", tostring(""))
  if SKILLID ~= 0 and STRUTPOS ~= -1 then
    _ActBr_SetActionbtn("skill1", 0, STRUTPOS, SKILLID)
  end
end
function _RefshEff_TimeEvent(_timer)
  if NeedRefush then
    Refush_Eff_Times = Refush_Eff_Times + _timer:GetRealInterval()
    if Refush_Eff_Times > 1 then
      if #Act_Struct_ItemTemp > 0 then
        local item = Act_Struct_ItemTemp[1]
        ActBr_SetScrollPicInfo(item.sourceX, item.sourceY, item.speeds, item.pic1, item.effect, item.skillId)
      end
      NeedRefush = false
    end
  end
end
function ActBr_SetScrollPicInfo(sourceX, sourceY, speeds, pic1, effect, skillId)
  local targetX = -1
  local targetY = -1
  local pos = -1
  local goodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Main_gbox"))
  local goodsBox1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Top_gbox"))
  local goodsBox2 = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Right_gbox"))
  for i = 0, 9 do
    local icon_type, info1, info2 = GetActionBtnInfoByIndex(i + 10 * (gUI.ActBr.CurMainBarPage - 1))
    if icon_type == AOT_EMPTY then
      targetX = goodsBox:GetItemRect(i):get_left()
      targetY = goodsBox:GetItemRect(i):get_top()
      pos = i + 10 * (gUI.ActBr.CurMainBarPage - 1)
      ActBr_SetEffDataInfo(sourceX, sourceY, speeds, pic1, effect, skillId, targetX, targetY, pos)
      return
    end
  end
  for i = 0, 9 do
    local icon_type, info1, info2 = GetActionBtnInfoByIndex(i + 30)
    if icon_type == AOT_EMPTY then
      targetX = goodsBox1:GetItemRect(i):get_left()
      targetY = goodsBox1:GetItemRect(i):get_top()
      pos = i + 30
      ActBr_SetEffDataInfo(sourceX, sourceY, speeds, pic1, effect, skillId, targetX, targetY, pos)
      return
    end
  end
  for i = 0, 9 do
    local icon_type, info1, info2 = GetActionBtnInfoByIndex(i + 40)
    if icon_type == AOT_EMPTY then
      targetX = goodsBox2:GetItemRect(i):get_left()
      targetY = goodsBox2:GetItemRect(i):get_top()
      pos = i + 40
      ActBr_SetEffDataInfo(sourceX, sourceY, speeds, pic1, effect, skillId, targetX, targetY, pos)
      return
    end
  end
end
function ActBr_SetEffDataInfo(sourceX, sourceY, speeds, pic1, effect, skillId, targetX, targetY, pos)
  local pic = WindowSys_Instance:GetWindow("Scroll_bg")
  if pos ~= -1 then
    pic:SetProperty("Visible", "true")
    pic:SetProperty("Left", tostring(sourceX))
    pic:SetProperty("Top", tostring(sourceY))
    pic:SetProperty("SourcePosX", tostring(sourceX))
    pic:SetProperty("SourcePosY", tostring(sourceY))
    pic:SetProperty("TargetPosX", tostring(targetX))
    pic:SetProperty("TargetPosY", tostring(targetY))
    pic:SetProperty("MoveSpeed", tostring(speeds))
    pic:SetProperty("BackImage", tostring(pic1))
    pic:SetProperty("EffectFile", tostring(effect))
    SKILLID = skillId
    STRUTPOS = pos
    TARGETPOSX = targetX
    TARGETPOSY = targetY
  end
end
function _OnActBr_PlayerRelive()
  gUI.ActBr.PlayerDead = false
  UpdateAllActionBtnSet()
  WorldStage:playSoundByID(0)
end
function _OnActBr_PlayerLevelChanged()
  _ActBr_UpdataAllIcon()
end
function _OnActBr_SetMainBtnActive()
  local wnd
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  wnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Pet_btn")
  wnd:SetEnable(true)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Mount_btn")
  wnd:SetEnable(GetMountCnt() > 0)
  wnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Amass_Btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.AmassLevel)
  wnd = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Blend_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.BlendLevel)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Blend_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.BlendLevel)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Resolve_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.ResovleLevel)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Manufacture_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.ManufaceLevel)
  wnd = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Check_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.ActiveCheckLev)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Sign_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.SignatureLevel)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Ineffective_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.IneffLevel)
  wnd = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.Forge_btn")
  wnd:SetEnable(level >= gUI_MainBtn_ActiveFlag.ForgeLevel)
end
function Lua_ActBr_SetTimer()
  AddTimerEvent(2, "RefshEff_Time", _RefshEff_TimeEvent)
end
function _OnActBr_UseItemByIndex(bagType, from_off)
  Lua_Bag_UseItemCheck(bagType, from_off)
end
function Script_ActBr_OnEvent(event)
  if "ACTION_BTN_ADD" == event then
    _OnActBr_AddIcon(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
  elseif "ACTION_BTN_COOLDOWN" == event then
    _OnActBr_CoolDown(arg1, arg2, arg3)
  elseif "ACTION_BTN_DELETED" == event then
    _OnActBr_DelIcon(arg1)
  elseif "PLAYER_DEAD" == event then
    _OnActBr_PlayerDead()
  elseif "PLAYER_RELIVED" == event then
    _OnActBr_PlayerRelive()
  elseif "PLAYER_LEVEL_CHANGED" == event then
    _OnActBr_PlayerLevelChanged()
  elseif "PLAYER_COMBAT_FORM_CHANGED" == event then
    _OnActBr_ChangeCombatForm(arg1, arg2)
  elseif "LEARNTECH_TEMPORARYSTUDY" == event then
    _OnActBr_TemporarySkillStudy(arg1, arg2, arg3, arg4)
  elseif "LEARNTECH_TEMPORARYREMOVE" == event then
    _OnActBr_TemporarySkillRemove(arg1, arg2)
  elseif "TEMPORARY_COOLDOWN" == event then
    _OnActBr_CoolDownTemporary(arg1, arg2, arg3, arg4)
  elseif "ACTION_BTN_MAINBTN_ACTIVE" == event then
    _OnActBr_SetMainBtnActive()
  elseif "BAG_USE_ITEM" == event then
    _OnActBr_UseItemByIndex(arg1, arg2)
  elseif "USER_GUIDE_REMOVESTRUCTEFFECT" == event then
    _OnAct_RemoveSkillEffect(arg1)
  end
end
function Script_ActBr_OnLoad()
  _OnActBr_CoolDown = _OnActBr_CoolDownNormal
  gUI.ActBr.Lock = false
  gUI.ActBr.LockByShift = false
  gUI.ActBr.MainRoot = WindowSys_Instance:GetWindow("ActionBar_frm")
  gUI.ActBr.Main_bg = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg"))
  gUI.ActBr.Main = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Main_gbox"))
  gUI.ActBr.Side1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Top_gbox"))
  gUI.ActBr.Side2 = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Right_gbox"))
  gUI.ActBr.Vehiclegb = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.Vehicle_bg.Vehicle_gbox"))
  gUI.ActBr.RightEff = WindowSys_Instance:GetWindow("ActionBar_frm.RightNew_bg")
  gUI.ActBr.Temporary = WindowToGoodsBox(WindowSys_Instance:GetWindow("ActionBar_frm.DldSkill_gbox"))
  gUI.ActBr.Temporary:SetVisible(false)
  gUI.ActBr.CurMainBarPage = 1
  gUI.ActBr.PlayerDead = false
  gUI.ActBr.Side1:OnFadeOut()
  gUI.ActBr.Side2:OnFadeOut()
  gUI.ActBr.Side1:SetIconFadeWithWindow(false)
  gUI.ActBr.Side2:SetIconFadeWithWindow(false)
  gUI.ActBr.Main:SetItemCoolCenter(true)
  gUI.ActBr.Side1:SetItemCoolCenter(true)
  gUI.ActBr.Side2:SetItemCoolCenter(true)
  gUI.ActBr.Temporary:SetItemCoolCenter(true)
  gUI.ActBr.DragIconFlag = false
  _ActBr_UpdatePageLabel(1)
  if GetActionBarLockState() == true then
    UI_ActBr_Lock()
  end
  local _, _, gender = GetMyPlayerStaticInfo()
  if gender then
    local charInfoMan_btn = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Character_btn")
    local charInfoWoman_btn = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Character02_Btn")
    if gender == 0 then
      charInfoMan_btn:SetProperty("Visible", "false")
      charInfoWoman_btn:SetProperty("Visible", "true")
    else
      charInfoMan_btn:SetProperty("Visible", "true")
      charInfoWoman_btn:SetProperty("Visible", "false")
    end
  end
  local wndNew = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Signature_btn.SignEff_bg")
  wndNew:SetVisible(false)
  wndNew = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Stall_btn.StallEff_bg")
  wndNew:SetVisible(false)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.Struct_SetEff")
  if wnd then
    wnd:RemoveAllChild()
  end
  AddTimerEvent(2, "RefshEff_Time", _RefshEff_TimeEvent)
  Act_Struct_ItemTemp = {}
  Act_Struct_ItemWndList = {}
  ActBr_SetEffDataInfo(0, 0, 0, "", "", -1, 0, 0, 0)
end
