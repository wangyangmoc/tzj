if gUI and not gUI.Amass then
  gUI.Amass = {}
end
AMASS_STATE_AMASSING = 0
AMASS_STATE_UNAMASSING = 1
AMASS_STATE_AMASSMAX = 1
AMASS_SNED_MSGFLAG_STAET = 10
AMASS_SEND_MSGFLAG_PAUSE = 11
AMASS_SEND_MSGFLAG_OVERCLICK = 12
AMASS_SEND_MSGFLAG_UNOVERCLICK = 13
AMASS_STONE_ID = 300002
local _Amass_State_Keep = 1
function Lua_Amass_Close_Open_Panel()
  local b_flagOpening = "1"
  if not gUI.Amass.Root:IsVisible() then
    b_flagOpening = "1"
    Lua_Amass_InitNeedItems()
  else
    b_flagOpening = "0"
  end
  gUI.Amass.Root:SetVisible("1" == b_flagOpening)
  return "1" == b_flagOpening
end
function Lua_Amass_InitData()
  Lua_AmassSet_ShowData(0, 0, 0, 0, 0, 0, 0)
end
function Lua_Amass_UpdateData()
  local playLevel = gUI_MainPlayerAttr.Level
  local NibLevel, RemainTimes, TotalTimes, currentClrPoint, ExpPoint, SpritePoint = AmassInfosData()
  Lua_AmassSet_ShowData(playLevel, NibLevel, RemainTimes, TotalTimes, currentClrPoint, ExpPoint, SpritePoint)
end
function OnAmass_UpdateData_Info()
  if gUI.Amass.Root:IsVisible() then
    Lua_Amass_UpdateData()
    Lua_Amass_InitNeedItems()
    Lua_Amass_UpdateInfosForOtherInterface()
  end
end
function Lua_Amass_UpdateInfosForOtherInterface()
  if gUI.Refine.Root:IsVisible() then
    Lua_Refine_SetData()
  end
end
function Lua_AmassSet_ShowData(charLev, nibLev, remainTimes, totalTimes, curClr, expPoint, spritePoint)
  gUI.Amass.CharLev:SetProperty("Text", tostring(charLev))
  gUI.Amass.NibLev:SetProperty("Text", tostring(nibLev))
  gUI.Amass.RemainTime:SetProperty("Text", tostring(remainTimes) .. "/" .. tostring(totalTimes))
  gUI.Amass.CurClr:SetProperty("Text", tostring(curClr))
  gUI.Amass.ExpLab:SetProperty("Text", tostring(expPoint))
  gUI.Amass.Burnt:SetProperty("Text", tostring(spritePoint))
end
function Lua_Amass_InitNeedItems()
  local strIconName, ItemCount = AmassGetITEMInfo()
  gUI.Amass.AmassItem:SetItemGoods(0, strIconName, Lua_Bag_GetQuality(-1, AMASS_STONE_ID))
  gUI.Amass.AmassItem:SetProperty("Text", tostring(ItemCount))
  gUI.Amass.RemainLab:SetProperty("Text", tostring(ItemCount))
  local _, _, _, m_isCancel = GetDanTianDataInfoForUse()
  if m_isCancel == 0 then
    gUI.Amass.MaxExit:SetChecked(false)
  else
    gUI.Amass.MaxExit:SetChecked(true)
  end
  gUI.Amass.AutoRevert:SetChecked(UIConfig:getAmassRevert())
end
function Lua_Amass_UpdateBtnUI()
  local strAmass = "开始聚灵"
  if _Amass_State_Keep == AMASS_STATE_AMASSING then
    strAmass = "取消聚灵"
  elseif _Amass_State_Keep == AMASS_STATE_UNAMASSING then
    strAmass = "开始聚灵"
  end
  gUI.Amass.AmassBtn:SetProperty("Text", strAmass)
end
function Lua_Amass_SendMsgToSerStartPause(B_Flag)
  AmassStartCloseHang(B_Flag)
end
function Lua_Amass_SendMsgToSerOverEND(B_Flag)
  AmassOverEndClick(B_Flag)
end
function Lua_Amass_SendMsgs(UI_Amass_MSG_flag)
  if UI_Amass_MSG_flag == AMASS_SNED_MSGFLAG_STAET then
    Lua_Amass_SendMsgToSerStartPause(true)
  elseif UI_Amass_MSG_flag == AMASS_SEND_MSGFLAG_PAUSE then
    Lua_Amass_SendMsgToSerStartPause(false)
  elseif UI_Amass_MSG_flag == AMASS_SEND_MSGFLAG_OVERCLICK then
    Lua_Amass_SendMsgToSerOverEND(true)
  elseif UI_Amass_MSG_flag == AMASS_SEND_MSGFLAG_UNOVERCLICK then
    Lua_Amass_SendMsgToSerOverEND(false)
  end
end
function Lua_Amass_InitAmassState()
  local Amass_CurrentState = AmassGetCurrentAmassState()
  if Amass_CurrentState then
    _Amass_State_Keep = AMASS_STATE_AMASSING
  else
    _Amass_State_Keep = AMASS_STATE_UNAMASSING
  end
  Lua_Amass_UpdateBtnUI()
end
function Lua_Amass_LogicAll()
  if gUI.Amass.Root:IsVisible() then
    Lua_Amass_InitAmassState()
    Lua_Amass_InitData()
    Lua_Amass_UpdateData()
  end
end
function On_Amass_BeNofityUpdate()
  Lua_Amass_UpdateData()
end
function Lua_Amass_SendMsgInterface()
  if _Amass_State_Keep == AMASS_STATE_AMASSING then
    Lua_Amass_SendMsgs(AMASS_SEND_MSGFLAG_PAUSE)
  elseif _Amass_State_Keep == AMASS_STATE_UNAMASSING then
    Lua_Amass_SendMsgs(AMASS_SNED_MSGFLAG_STAET)
  end
end
function UI_Amass_OpenClick(msg)
  local playLevel = gUI_MainPlayerAttr.Level
  if playLevel < 30 then
    Lua_Chat_ShowOSD("AMASS_LEVEL_LIMIT")
    return
  end
  if Lua_Amass_Close_Open_Panel() then
    Lua_Amass_LogicAll()
  end
end
function UI_Amass_ExitClick(msg)
  Lua_Amass_Close_Open_Panel()
end
function UI_Amass_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Amass_AmassClick(msg)
  Lua_Amass_SendMsgInterface()
end
function UI_Amass_CanclClick(msg)
  UI_Amass_ExitClick(msg)
end
function UI_Amass_Over_Click(msg)
  if msg == nil then
    return
  end
  local CheckBtn = msg:get_window()
  CheckBtn = WindowToCheckButton(CheckBtn)
  local curName = CheckBtn:GetProperty("WindowName")
  if CheckBtn:IsChecked() then
    Lua_Amass_SendMsgs(AMASS_SEND_MSGFLAG_OVERCLICK)
  else
    Lua_Amass_SendMsgs(AMASS_SEND_MSGFLAG_UNOVERCLICK)
  end
end
function UI_Amass_Revert_Click(msg)
  if msg == nil then
    return
  end
  local CheckBtn = msg:get_window()
  CheckBtn = WindowToCheckButton(CheckBtn)
  UIConfig:setAmassRevert(CheckBtn:IsChecked())
end
function UI_Amass_ShowTips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, AMASS_STONE_ID)
end
function UI_Tip_ShowTextAmass(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_ShowInterfaceCommon(tooltip, "灵力在聚灵时可以获得，每分钟获得1点灵力,灵力上限为1000点,到达上限后，聚灵只能获得经验不能获得灵力。", 0, 1, colorYellow, "")
  Lua_Tip_ShowInterfaceCommon(tooltip, "灵力作用：", 1, 1, colorDarkBlue, "kaiti_13")
  Lua_Tip_ShowInterfaceCommon(tooltip, "暂未开放，敬请期待。", 1, 0, colorYellow, "")
end
function On_Amass_Update()
  Lua_Amass_InitAmassState()
end
function On_Amass_UpdateClose()
  gUI.Amass.Root:SetVisible(false)
end
function Script_Amass_OnEvent(event)
  if event == "AMASS_UPDATE_INFO" then
    OnAmass_UpdateData_Info()
  elseif event == "AMASS_UPDATE_BTNS" then
    On_Amass_Update()
  elseif event == "AMASS_UPDATE_BTNS_CLOSE" then
    On_Amass_UpdateClose()
  end
end
function Script_Amass_OnLoad()
  gUI.Amass.Root = WindowSys_Instance:GetWindow("Amass")
  gUI.Amass.CharLev = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.Level_dlab"))
  gUI.Amass.NibLev = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.AmassLevel_dlab"))
  gUI.Amass.RemainTime = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.Remain_dlab"))
  gUI.Amass.AmassItem = WindowToGoodsBox(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.RemainGbox_gbox"))
  gUI.Amass.CurClr = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.Vigour_dlab"))
  gUI.Amass.MaxExit = WindowToCheckButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.ContentCbtn_cbtn"))
  gUI.Amass.AutoRevert = WindowToCheckButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.RevertTo_cbtn"))
  gUI.Amass.ExpLab = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Down_bg.Experience_dlab"))
  gUI.Amass.RemainLab = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Up_bg.RemainNum_dlab"))
  gUI.Amass.Burnt = WindowToLabel(WindowSys_Instance:GetWindow("Amass.Amass_bg.Image_bg.Down_bg.Vigour_dlab"))
  gUI.Amass.AmassBtn = WindowToButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Amass_btn"))
  gUI.Amass.CancelBtn = WindowToButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Cancel_btn"))
  gUI.Amass.HelpBtn = WindowToButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Help_btn"))
  gUI.Amass.ExitBtn = WindowToButton(WindowSys_Instance:GetWindow("Amass.Amass_bg.Close_btn"))
end
function Script_Amass_OnHide()
end

