function _AreaMap_IsCurrentMap()
  _, CurrentMapID = GetCurrentMiniMapInfo()
  return CurrentMapID == gUI.AreaMap.MapObj:GetCurrentMapID()
end
function Script_AreaMap_OnLoad()
  _AreaMap_InitUI()
end
function Script_AreaMap_OnEvent(event)
  if event == "AREA_MAP_UPDATE_CUROSR_POS" then
    _AreaMap_UpdateCursorPos(arg1, arg2)
  elseif event == "AREA_MAP_HIDE_TIP_INFO" then
    _AreaMap_HideTipInfo()
  elseif event == "AREA_MAP_SHOW_NPC_TIP" then
    _AreaMap_UpdateNpcTip(arg1, arg2, arg3, arg4)
  elseif event == "AREA_MAP_SHOW_TASK_TIP" then
    _AreaMap_UpdateTaskTip(arg1, arg2, arg3)
  elseif event == "AREA_MAP_SHOW_TRANS_TIP" then
    _AreaMap_UpdateTransTip(arg1, arg2, arg3)
  elseif event == "AREA_MAP_DRAG_STATE" then
  elseif event == "AREA_MAP_SHOW_PLAYER_TIP" then
    _AreaMap_UpdatePlayerTip(arg1, arg2)
  elseif event == "AREA_MAP_SHOW_TIP" then
    _AreaMap_ShowTip()
  end
end
function _AreaMap_InitUI()
  gUI.AreaMap = {}
  gUI.AreaMap.Root = WindowSys_Instance:GetWindow("AreaMap")
  gUI.AreaMap.Map = WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.PicMap_Pic")
  gUI.AreaMap.MapList = WindowToComboBox(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.Pic1_bg.MapList_cbox"))
  gUI.AreaMap.NpcList = WindowToListBox(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.Pic2_bg.Pic_bg.NPCList_lbox"))
  gUI.AreaMap.MonList = WindowToListBox(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.Pic3_bg.Pic_bg.MonsterList_lbox"))
  gUI.AreaMap.Title = WindowToLabel(WindowSys_Instance:GetWindow("AreaMap.Title_dlab"))
  gUI.AreaMap.CursorPos = WindowToLabel(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.CursorPos_dlab"))
  gUI.AreaMap.AllCheck = WindowToCheckButton(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.AllCkBtn_cbtn"))
  gUI.AreaMap.FuncCheck = WindowToCheckButton(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.CkBtn1_cbtn"))
  gUI.AreaMap.OtherCheck = WindowToCheckButton(WindowSys_Instance:GetWindow("AreaMap.PicAreaMap_bg.CkBtn2_cbtn"))
  gUI.AreaMap.MapToolTip = WindowToTooltip(gUI.AreaMap.Map:GetToolTipWnd(0))
  gUI.AreaMap.MapObj = UIInterface:GetAreaMap()
  UIInterface:BindAreaMap(gUI.AreaMap.Map, false)
  FilterFlag = 3
  CursorNpcID = 0
  CursorX = 0
  CursorY = 0
  TipType = 0
  CursorMapID = 0
  _AreaMap_UpdateFilterFlag()
  gUI.AreaMap.AllMapList = {}
end
function UI_AreaMap_Show()
  if not gUI.AreaMap.Root:IsVisible() then
    _AreaMap_InitMapList()
    _, CurrentMapID = GetCurrentMiniMapInfo()
    _AreaMap_ChangeMap(CurrentMapID)
    gUI.AreaMap.MapObj:SetVisible(true)
    gUI.AreaMap.Root:SetProperty("Visible", "true")
    local AreaRootWnd = gUI.AreaMap.Root:GetChildByName("PicAreaMap_bg")
    local WorldRootWnd = gUI.AreaMap.Root:GetChildByName("PicWorldMap_pic")
    AreaRootWnd:SetVisible(true)
    WorldRootWnd:SetVisible(false)
    UIInterface:BindAreaMap(gUI.AreaMap.Map, false)
  else
    gUI.AreaMap.MapObj:SetVisible(false)
    _AreaMap_HideTipInfo()
    gUI.AreaMap.Root:SetProperty("Visible", "false")
  end
end
function UI_AreaMap_CloseBtnClick(msg)
  UI_AreaMap_Show()
end
function UI_AreaMap_MapChanged(msg)
  local MapID = msg:get_lparam()
  local item = gUI.AreaMap.AllMapList[MapID]
  if item and item.ShowInList then
    _AreaMap_ChangeMap(MapID)
  end
end
function _AreaMap_InitMapList()
  gUI.AreaMap.AllMapList = {}
  gUI.AreaMap.MapList:RemoveAllItems()
  gUI.AreaMap.AllMapList = GetMapList()
  local nPos = 0
  for index, item in spairs(gUI.AreaMap.AllMapList) do
    if item.MapID and item.ShowInList then
      local node
      if not item.IsInstance then
        node = gUI.AreaMap.MapList:InsertString(item.MapName, -1)
      else
        node = gUI.AreaMap.MapList:InsertString("(副本)" .. item.MapName, -1)
      end
      node:SetUserData(item.MapID)
      item.Index = nPos
      nPos = nPos + 1
    end
  end
end
function _AreaMap_InitTargetList(MapID)
  gUI.AreaMap.NpcList:RemoveAllItems()
  gUI.AreaMap.MonList:RemoveAllItems()
  TargetList = GetTargetList(MapID)
  for index, item in spairs(TargetList) do
    if item.ID then
      if item.Kind == 1 or item.Kind == 13 then
        local node
        if item.Title and string.len(item.Title) > 0 then
          node = gUI.AreaMap.NpcList:InsertString("<" .. item.Title .. ">" .. item.Name, -1)
        else
          node = gUI.AreaMap.NpcList:InsertString(item.Name, -1)
        end
        node:SetUserData(item.ID)
      else
        local node
        node = gUI.AreaMap.MonList:InsertString(item.Name .. "(" .. tostring(math.floor(item.X)) .. "," .. tostring(math.floor(item.Y)) .. ")", -1)
        node:SetUserData(item.ID)
      end
    end
  end
end
function _AreaMap_ChangeMap(MapID)
  local item = gUI.AreaMap.AllMapList[MapID]
  if item then
    gUI.AreaMap.MapObj:SetMapID(MapID)
    gUI.AreaMap.MapObj:SetBackgroundTexture(item.AreaMap, item.MapSize)
    gUI.AreaMap.Title:SetProperty("Text", item.MapName)
    _AreaMap_InitTargetList(MapID)
    if item.ShowInList then
      gUI.AreaMap.MapList:SetSelectItem(item.Index)
    end
  end
end
function UI_AreaMap_CurrentMapBtnClicked(msg)
  _, CurrentMapID = GetCurrentMiniMapInfo()
  _AreaMap_ChangeMap(CurrentMapID)
end
function UI_AreaMap_WorldMapBtnClicked()
  local AreaRootWnd = gUI.AreaMap.Root:GetChildByName("PicAreaMap_bg")
  local WorldRootWnd = gUI.AreaMap.Root:GetChildByName("PicWorldMap_pic")
  AreaRootWnd:SetVisible(false)
  WorldRootWnd:SetVisible(true)
  UIInterface:BindAreaMap(gUI.AreaMap.Map, true)
end
function UI_AreaMap_WorldMap_AreaBtnClicked(msg)
  local AreaMapBtn = msg:get_window()
  local MapID = AreaMapBtn:GetCustomUserData(0)
  _AreaMap_ChangeMap(MapID)
  local AreaRootWnd = gUI.AreaMap.Root:GetChildByName("PicAreaMap_bg")
  local WorldRootWnd = gUI.AreaMap.Root:GetChildByName("PicWorldMap_pic")
  AreaRootWnd:SetVisible(true)
  WorldRootWnd:SetVisible(false)
  UIInterface:BindAreaMap(gUI.AreaMap.Map, false)
end
function _AreaMap_UpdateCursorPos(x, y)
  gUI.AreaMap.CursorPos:SetProperty("Text", "鼠标坐标 {#G^X:}{#W^" .. tostring(x) .. "} {#G^Y:}{#W^" .. tostring(y) .. "}")
  CursorX = x
  CursorY = y
end
function _AreaMap_ShowTip()
  if gUI.AreaMap.Map:IsVisible() then
    if gUI.AreaMap.MapToolTip == nil then
      gUI.AreaMap.MapToolTip = WindowToTooltip(gUI.AreaMap.Map:GetToolTipWnd(0))
    end
    Lua_Tip_ShowMapTip(gUI.AreaMap.MapToolTip)
    local posx, posy = GetMousePos()
    gUI.AreaMap.MapToolTip:SetLeft(posx + 0.01)
    gUI.AreaMap.MapToolTip:SetTop(posy - 0.05)
  end
end
function _AreaMap_UpdateNpcTip(Name, Title, NpcID, IdInMap)
  if gUI.AreaMap.Map:IsVisible() then
    TipType = 1
    Lua_Tip_ShowMapNpcTip(gUI.AreaMap.MapToolTip, NpcID, Name, Title)
    local posx, posy = GetMousePos()
    gUI.AreaMap.MapToolTip:SetLeft(posx + 0.01)
    gUI.AreaMap.MapToolTip:SetTop(posy - 0.05)
    CursorNpcID = IdInMap
  end
end
function _AreaMap_UpdatePlayerTip(name, menpaiId)
  if name == gUI_MainPlayerAttr.Name then
    return
  end
  if gUI.AreaMap.Map:IsVisible() then
    TipType = 1
    local infos = "--"
    if gUI_MenPaiName[menpaiId] then
      infos = gUI_MenPaiName[menpaiId]
    end
    Lua_Tip_ShowAraMapPlayerTip(gUI.AreaMap.MapToolTip, name, infos, "FF6A5ACD")
    local posx, posy = GetMousePos()
    gUI.AreaMap.MapToolTip:SetLeft(posx + 0.01)
    gUI.AreaMap.MapToolTip:SetTop(posy - 0.05)
  end
end
function _AreaMap_UpdateTaskTip(Title, Desc, curValue)
  if gUI.AreaMap.Map:IsVisible() then
    TipType = 2
    Lua_Tip_ShowAreaMapTaskTip(gUI.AreaMap.MapToolTip, Title, Desc, curValue)
    local posx, posy = GetMousePos()
    gUI.AreaMap.MapToolTip:SetLeft(posx + 0.01)
    gUI.AreaMap.MapToolTip:SetTop(posy - 0.05)
  end
end
function _AreaMap_UpdateTransTip(Desc, LevelLimited, TargetMapID)
  if gUI.AreaMap.Map:IsVisible() then
    TipType = 3
    CursorMapID = TargetMapID
    Lua_Tip_ShowMapTransTip(gUI.AreaMap.MapToolTip, LevelLimited, Desc)
    local posx, posy = GetMousePos()
    gUI.AreaMap.MapToolTip:SetLeft(posx + 0.01)
    gUI.AreaMap.MapToolTip:SetTop(posy - 0.05)
  end
end
function _AreaMap_HideTipInfo()
  CursorNpcID = 0
  TipType = 0
  gUI.AreaMap.MapToolTip:SetProperty("Visible", "false")
end
function UI_AreaMap_NpcDblClicked(msg)
  local wnd = WindowToListBox(msg:get_window())
  local item = wnd:GetSelectedItem()
  if item then
    local NpcID = item:GetUserData()
    if _AreaMap_IsCurrentMap() then
      MoveToNpc(NpcID)
    else
      MoveToTargetLocation(TargetList[NpcID].X, TargetList[NpcID].Y, gUI.AreaMap.MapObj:GetCurrentMapID())
    end
  end
  WorldStage:playSoundByID(25)
end
function UI_AreaMap_MonDblClicked(msg)
  local wnd = WindowToListBox(msg:get_window())
  local item = wnd:GetSelectedItem()
  if item then
    local NpcID = item:GetUserData()
    local TargetInfo = TargetList[NpcID]
    if TargetInfo then
      MoveToTargetLocation(TargetInfo.X, TargetInfo.Y, gUI.AreaMap.MapObj:GetCurrentMapID())
    end
  end
end
function UI_AreaMap_MapRClicked(msg)
  if not CursorNpcID or CursorNpcID == 0 then
    MoveToTargetLocation(CursorX, CursorY, gUI.AreaMap.MapObj:GetCurrentMapID())
  elseif TargetList[CursorNpcID].Kind == 1 then
    if _AreaMap_IsCurrentMap() then
      MoveToNpc(CursorNpcID)
    else
      MoveToTargetLocation(TargetList[CursorNpcID].X, TargetList[CursorNpcID].Y, gUI.AreaMap.MapObj:GetCurrentMapID())
    end
  else
    MoveToTargetLocation(CursorX, CursorY, gUI.AreaMap.MapObj:GetCurrentMapID())
  end
  WorldStage:playSoundByID(25)
end
function UI_AreaMap_MapClicked(msg)
  if TipType == 3 then
    _AreaMap_HideTipInfo()
    _AreaMap_ChangeMap(CursorMapID)
  end
end
function _AreaMap_UpdateFilterFlag()
  if FilterFlag == 3 then
    gUI.AreaMap.AllCheck:SetChecked(true)
    gUI.AreaMap.FuncCheck:SetChecked(false)
    gUI.AreaMap.OtherCheck:SetChecked(false)
    gUI.AreaMap.MapObj:SetFilterFlag(0, true)
    gUI.AreaMap.MapObj:SetFilterFlag(1, true)
  elseif FilterFlag == 2 then
    gUI.AreaMap.AllCheck:SetChecked(false)
    gUI.AreaMap.FuncCheck:SetChecked(true)
    gUI.AreaMap.OtherCheck:SetChecked(false)
    gUI.AreaMap.MapObj:SetFilterFlag(0, true)
    gUI.AreaMap.MapObj:SetFilterFlag(1, false)
  elseif FilterFlag == 1 then
    gUI.AreaMap.AllCheck:SetChecked(false)
    gUI.AreaMap.FuncCheck:SetChecked(false)
    gUI.AreaMap.OtherCheck:SetChecked(true)
    gUI.AreaMap.MapObj:SetFilterFlag(0, false)
    gUI.AreaMap.MapObj:SetFilterFlag(1, true)
  end
end
function UI_AreaMap_FilterOtherClicked(mag)
  FilterFlag = 1
  _AreaMap_UpdateFilterFlag()
end
function UI_AreaMap_FilterFuncClicked(mag)
  FilterFlag = 2
  _AreaMap_UpdateFilterFlag()
end
function UI_AreaMap_FilterAllClicked(mag)
  FilterFlag = 3
  _AreaMap_UpdateFilterFlag()
end
function UI_AreaMap_NpcClicked(msg)
  local wnd = WindowToListBox(msg:get_window())
  local item = wnd:GetSelectedItem()
  if item then
    local NpcID = item:GetUserData()
    local TargetInfo = TargetList[NpcID]
    if TargetInfo then
      gUI.AreaMap.MapObj:SetFlashPos(TargetInfo.X, TargetInfo.Y, 1.5)
    end
  end
end
function UI_AreaMap_MonClicked(msg)
  local wnd = WindowToListBox(msg:get_window())
  local item = wnd:GetSelectedItem()
  if item then
    local MonID = item:GetUserData()
    local TargetInfo = TargetList[MonID]
    if TargetInfo then
      gUI.AreaMap.MapObj:SetFlashPos(TargetInfo.X, TargetInfo.Y, 1.5)
    end
  end
end
function Script_AreaMap_OnHide()
  gUI.AreaMap.MapObj:SetVisible(false)
  _AreaMap_HideTipInfo()
end
se)
  _AreaMap_HideTipInfo()
end
