if gUI and not gUI.Bag then
  gUI.Bag = {}
end
local _Bag_IsInSplitItem = false
local _Bag_SplitWnd
local _Bag_SplitBagType = -1
local _Bag_SplitCurrentSlot = 0
local _Bag_SplitItemCount = 0
local _Bag_ContainerBaseSize = 40
local _Bag_ContainerExtSize = 10
local _Bag_ContainerExtNum = 8
local _Bag_ContainerMaxSize = _Bag_ContainerBaseSize + _Bag_ContainerExtSize * _Bag_ContainerExtNum
local _Bag_ContainerMaxSizeTask = 40
local _Bag_ExtSlotQualityID = 20
function _Bag_GetBagSlotClientToUI(slot)
  return 0, slot
end
function _Bag_SplitItemCallback(num)
  _Bag_IsInSplitItem = false
  if _Bag_SplitWnd then
    local totalslot = 0
    local dstslot = -1
    local slottocheck = 0
    local next = 0
    _Bag_SplitItemCount = num
    local bag, slot = _Bag_GetBagSlotClientToUI(_Bag_SplitCurrentSlot)
    _Bag_SplitWnd:StartSplitItem(slot, num)
  else
    LogMsg("_Bag_SplitItemCallback: _Bag_SplitWnd is nil.")
  end
  _Bag_IsInSplitItem = false
end
function _Bag_InsertBag()
  gUI.Bag.WndRoot = WindowSys_Instance:GetWindow("Bag_frm")
  gUI.Bag.ContainerBag = WindowToContainer(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Bag_bg.BagMain_cnt"))
  gUI.Bag.ContainerBag:SetVisible(true)
  gUI.Bag.GboxExtBags = WindowToGoodsBox(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.KuoChongDi_bg.GoodsBox1_gbox"))
  if nil == gUI.Bag.GoodsBoxBag then
    gUI.Bag.ContainerBag:setEnableHeightCal(false)
    gUI.Bag.GoodsBoxBag = WindowToGoodsBox(gUI.Bag.ContainerBag:GetChildByName("GoodsBox1"))
    gUI.Bag.GoodsBoxBag:SetItemColumn(10)
    gUI.Bag.GoodsBoxBag:SetItemCount(_Bag_ContainerMaxSize)
    gUI.Bag.GoodsBoxBag:SetCustomUserData(0, 0)
  end
  gUI.Bag.ContainerBag:SizeSelf()
end
function _Bag_InsertTaskBag()
  gUI.Bag.ContainerTask = WindowToContainer(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Bag_bg.BagTask_cnt"))
  gUI.Bag.ContainerTask:SetVisible(false)
  if nil == gUI.Bag.GoodsBoxTask then
    gUI.Bag.ContainerTask:setEnableHeightCal(false)
    gUI.Bag.GoodsBoxTask = WindowToGoodsBox(gUI.Bag.ContainerTask:GetChildByName("GoodsBox1"))
    gUI.Bag.GoodsBoxTask:SetItemColumn(10)
    gUI.Bag.GoodsBoxTask:SetItemCount(_Bag_ContainerMaxSizeTask)
  end
  gUI.Bag.ContainerTask:SizeSelf()
end
function _Bag_ShowNewState()
  for i = 0, gUI.Bag.BagSlotCount - 1 do
    gUI.Bag.GoodsBoxBag:SetIconState(i, 8, false)
  end
  for i = 0, gUI.Bag.TaskSlotCount - 1 do
    gUI.Bag.GoodsBoxTask:SetIconState(i, 8, false)
  end
  for i, n in pairs(gUI.Bag.NewItemMark) do
    if n.type == 1 then
      gUI.Bag.GoodsBoxBag:SetIconState(n.slot, 8, true)
    else
      gUI.Bag.GoodsBoxTask:SetIconState(n.slot, 8, true)
    end
  end
  _Bag_ClearNewMarkList()
end
function _Bag_UsePetSkillScroll(bag, skillName, from_off, skillParam, needLevel)
  local curPetIndex = Lua_Pet_GetCurrentPetIndex()
  local petName = GetPetName(curPetIndex)
  local petLevel = GetPetLevel(curPetIndex)
  if petName == nil then
    ShowErrorMessage("使用失败，你当前没有出战的宠物")
    return
  end
  if GetIsPetHasSameSkillID(curPetIndex, skillParam) then
    ShowErrorMessage("已经有了相同的技能,不能重复学习")
    return
  end
  local bSame, newSlot, oldSkillName = GetIsPetHasSameSkillGroupID(curPetIndex, skillParam)
  if bSame then
    local name, icon, category, Type, desc = GetSkillBaseInfo(skillParam)
    Messagebox_Show("REPLACE_PET_SKILL", petName, oldSkillName, skillName, bag, from_off, newSlot)
    return
  end
  if GetIsPetSkillFull(curPetIndex) then
    Lua_Pet_ShowReplaceWnd(skillParam, bag, from_off)
    return
  end
  if needLevel > petLevel then
    ShowErrorMessage("使用失败，【" .. petName .. "】需要" .. tostring(needLevel) .. "级后才能学习")
    return
  end
  Messagebox_Show("USE_PET_STUDY_SKILL", petName, skillName, bag, from_off, newSlot)
end
function _Bag_UseFaerieSkillScroll(bag, skillName, from_off, skillParam, needLevel)
  local _, petLevel = GetFaerieBase()
  if GetIsFaerieHasSameSkillID(skillParam) then
    ShowErrorMessage("已经有了相同的技能,不能重复学习")
    return
  end
  local bSame, newSlot, oldSkillName = GetIsFaerieHasSameSkillGroupID(skillParam)
  if bSame then
    local name, icon, category, Type, desc = GetSkillBaseInfo(skillParam)
    Messagebox_Show("REPLACE_PET_SKILL", oldSkillName, skillName, bag, from_off, newSlot)
    return
  end
  if GetIsFaerieSkillFull() then
    Lua_Faerie_ShowReplaceWnd(skillParam, bag, from_off)
    return
  end
  if needLevel > petLevel * 10 then
    ShowErrorMessage("使用失败, 需要" .. tostring(needLevel) .. "级后才能学习")
    return
  end
  Messagebox_Show("USE_PET_STUDY_SKILL", skillName, bag, from_off, newSlot)
end
function _Bag_ClearNewMarkList()
  gUI.Bag.NewItemMark = {}
end
function _Bag_PlaceBag()
  local xRoot = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg"):GetRight() + 0.4
  local yRoot = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg"):GetTop() + 0.015
  local mainBag = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg")
  local mainBagWidth = mainBag:GetWidth()
  local mainBagHeight = mainBag:GetHeight()
  xRoot = xRoot - mainBagWidth
  yRoot = yRoot - mainBagHeight
  gUI.Bag.WndRoot:SetTop(yRoot)
  gUI.Bag.WndRoot:SetLeft(xRoot)
end
function _Bag_AdjustWndSize()
  local widthWnd = RenderSys_Instance:GetPixelWidth()
  local heightWnd = RenderSys_Instance:GetPixelHeight()
  local itemBoxHeight = gUI.Bag.GoodsBoxBag:GetHeight()
  local picBagBox = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Bag_bg")
  local picItemBox = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg")
  local picBack = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg")
  if itemBoxHeight * heightWnd < 145 then
    itemBoxHeight = 145 / heightWnd
  end
  local picItemBoxHeight = picItemBox:GetHeight()
  local picBagBoxHeight = itemBoxHeight + 12 / heightWnd
  picItemBox:SetTop(picBagBoxHeight + 1 / heightWnd)
  picBagBox:SetHeight(picItemBoxHeight + picBagBoxHeight + -4 / heightWnd)
  picBack:SetHeight(picBagBox:GetHeight() + 10 / heightWnd)
  gUI.Bag.ContainerBag:SetHeight(picBagBoxHeight)
  gUI.Bag.WndRoot:SetHeight(picBack:GetHeight() + 50 / heightWnd)
end
function _Bag_SetShowTypeBtnState(typeIndex)
  local BtnShowAll = WindowToButton(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Btn_bg.All_btn"))
  local BtnShowEquip = WindowToButton(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Btn_bg.Equip_btn"))
  local BtnShowGem = WindowToButton(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Btn_bg.Gem_btn"))
  local BtnShowQuest = WindowToButton(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Btn_bg.Quest_btn"))
  if typeIndex == 0 then
    gUI.Bag.ContainerBag:SetVisible(true)
    gUI.Bag.ContainerTask:SetVisible(false)
    gUI.Bag.GoodsBoxBag:SetItemHideFlags(0)
    BtnShowAll:SetStatus("selected")
  else
    BtnShowAll:SetStatus("normal")
  end
  if typeIndex == 1 then
    gUI.Bag.ContainerBag:SetVisible(true)
    gUI.Bag.ContainerTask:SetVisible(false)
    gUI.Bag.GoodsBoxBag:SetItemHideFlags(GetBagShowTypeEquip())
    BtnShowEquip:SetStatus("selected")
  else
    BtnShowEquip:SetStatus("normal")
  end
  if typeIndex == 2 then
    gUI.Bag.ContainerBag:SetVisible(true)
    gUI.Bag.ContainerTask:SetVisible(false)
    gUI.Bag.GoodsBoxBag:SetItemHideFlags(GetBagShowTypeGem())
    BtnShowGem:SetStatus("selected")
  else
    BtnShowGem:SetStatus("normal")
  end
  if typeIndex == 3 then
    gUI.Bag.ContainerBag:SetVisible(false)
    gUI.Bag.ContainerTask:SetVisible(true)
    BtnShowQuest:SetStatus("selected")
  else
    BtnShowQuest:SetStatus("normal")
  end
  _Bag_AdjustWndSize()
end
function Lua_Bag_GetQualityColor(quality)
  return gUI_GOODS_QUALITY_COLOR_PREFIX[quality]
end
function Lua_Bag_IsActionbtn(type)
  local pos = string.find(type, "action")
  return pos ~= nil
end
function Lua_Bag_IsTemp(type)
  local pos = string.find(type, "temp")
  return pos ~= nil
end
function Lua_Bag_IsSkillBar(type)
  local pos = string.find(type, "skill")
  return pos ~= nil
end
function Lua_Bag_IsMerchantBar(type)
  local pos = string.find(type, "merchant")
  return pos ~= nil
end
function Lua_Bag_IsEquipbox(type)
  local pos = string.find(type, "equipbox")
  return pos ~= nil
end
function Lua_Bag_IsBackPackbox(type)
  local pos = string.find(type, "backpack")
  return pos ~= nil
end
function Lua_Bag_IsBankBagbox(type)
  local pos = string.find(type, "bankbag")
  return pos ~= nil
end
function Lua_Bag_GetRealSlot(win, slot)
  return slot
end
function Lua_Bag_CancelSplitItem()
  _Bag_IsInSplitItem = false
  SetMouseState(0)
end
function Lua_Bag_GetBagWinAndItemId(slot)
  bagId, index = _Bag_GetBagSlotClientToUI(slot)
  if bagId == 0 then
    return gUI.Bag.GoodsBoxBag, index
  end
  return nil, nil
end
function Lua_Bag_ClearToolTip()
  HideTooltip()
  Lua_Tip_HideObjTooltip()
end
function Lua_Bag_GetBindInfo(bag, slot)
  local _, bindtype = GetItemUsingInfo(bag, slot)
  if bindtype ~= nil and bindtype == 1 then
    return 1
  else
    return 0
  end
end
function Lua_Bag_GetStarInfo(bag, slot, resver)
  local intenLev = GetItemForgeLevel(bag, slot, resver)
  if intenLev == nil then
    return 0
  else
    return intenLev
  end
end
function Lua_Bag_GetQuality(bag, slot)
  local _, _, Quality = GetItemBaseInfoBySlot(bag, slot)
  if Quality == nil then
    Quality = 0
  end
  return Quality
end
function Lua_Bag_SetSplitBtnEnable(bVal)
  WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Lab_bg.Chaifen_btn"):SetEnable(bVal)
end
function Lua_Bag_NewItemExceptBaseContainer(bag, slot)
  if bag == gUI_GOODSBOX_BAG.Horse then
    if gUI.Mounts.wndRoot:IsVisible() then
      _Mounts_UpdateAll()
    end
    if not IsUserGuideStepIsComplete(18) then
      TriggerUserGuide(34, -1, -1)
    else
      UI_Mounts_Open()
    end
  elseif bag == gUI_GOODSBOX_BAG.pet1 then
    if not IsUserGuideStepIsComplete(14) then
      TriggerUserGuide(28, -1, -1)
    else
      UI_Pet_Open()
    end
  end
end
function Lua_Bag_ShowUI(show)
  if show then
    _OnBag_UpdateMoney()
    _OnBag_UpdateMoneyBind()
    _Bag_SetShowTypeBtnState(0)
    gUI.Bag.WndRoot:SetVisible(true)
    _Bag_ShowNewState()
    RefreshCurrentUserGuide()
    _Bag_PlaceBag()
  else
    gUI.Bag.WndRoot:SetVisible(false)
    if _Bag_IsInSplitItem then
      _Bag_IsInSplitItem = false
      SetMouseState(0)
    end
  end
end
function Lua_Bag_UseItemCheck(bag, from_off)
  local bUsingHorse, bindtype, hasStallArua, id, skillId, skillParam, isPetItem, strPrompt = GetItemUsingInfo(bag, from_off)
  local _, _, _, name, _, _, _, _ = GetItemBaseInfoBySlot(bag, from_off)
  if id == nil then
    return
  elseif id == 308100002 then
    Guild_ChangeName()
  elseif id >= 300047 and id <= 300056 then
    HelpBook_Show(101000000 + (id - 300047) * 1000000)
  elseif id == 370004 then
    Messagebox_Show("CONFIRM_USE_ITEM", bag, from_off, "您是否确定重置天赋？")
  elseif skillId == 3101 then
    Messagebox_Show("CONFIRM_USE_ITEM", bag, from_off, "是否确认需要扩充仓库？")
  elseif skillId == 135 then
    Messagebox_Show("CONFIRM_USE_ITEM", bag, from_off, "确定使用符文页开启第二符文页？")
  elseif bUsingHorse and bag == gUI_GOODSBOX_BAG.backpack0 and bindtype == 3 then
    Messagebox_Show("USE_MOUNT", bag, from_off, name)
  elseif bindtype == 3 then
    local _, _, _, minLevelC, jobC, sexC = GetItemInfoBySlot(bag, from_off, 0)
    local playName = gUI_MainPlayerAttr.Name
    local playJob = gUI_MainPlayerAttr.MenPai
    local playSex = gUI_MainPlayerAttr.Sex
    local playLevel = gUI_MainPlayerAttr.Level
    if minLevelC then
      if minLevelC and minLevelC ~= -1 and minLevelC > playLevel then
        Lua_Chat_ShowOSD("BAG_LEV_LOW")
        return
      end
      if jobC ~= -1 and not checkJobKindIsCanUse(playJob, jobC) then
        Lua_Chat_ShowOSD("BAG_JOB_WRONG")
        return
      end
      if sexC ~= -1 and playSex ~= sexC then
        Lua_Chat_ShowOSD("BAG_SEX_WRONG")
        return
      end
      if bag == gUI_GOODSBOX_BAG.backpack0 then
        Messagebox_Show("CONFIRM_USE_BIND", bag, from_off)
      end
    end
  elseif skillId == 55 and WindowSys_Instance:GetWindow("Pet.Feed_bg"):IsVisible() then
    Lua_Pet_SetAutoFeedItem(bag, from_off)
  elseif skillId == 56 then
    local _, _, _, needLevel, _, _, _, _ = GetItemInfoBySlot(bag, from_off)
    _Bag_UseFaerieSkillScroll(bag, name, from_off, skillParam, needLevel)
  elseif skillId == 66 then
    Messagebox_Show("USE_PET_EGG", name, bag, from_off)
  elseif isPetItem and bindtype == 0 then
    Messagebox_Show("USE_PET_ITEM", name, bag, from_off)
  elseif bUsingHorse and isAutoVehicleMoving() then
    Lua_UI_ShowMessageInfo("不能在飞行状态下使用坐骑")
  elseif skillId == 124 then
    local HasUse = GetDanTianDataInfoForUse()
    if HasUse == 1 then
      Lua_Chat_ShowOSD("BAG_IN_HASUSE")
    else
      local _, currentTime, TotalTime = GetDanTianDataInfoForUse()
      if TotalTime < currentTime + skillParam then
        Messagebox_Show("USE_DANTIANITEM", bag, from_off)
      else
        UseItem(bag, from_off)
      end
    end
  else
    UseItem(bag, from_off)
  end
end
function _OnBag_ItemAdd(bag, slot, icon, count, locked, quality, duration, rest, userlock, lefttime, hideflag, canUse)
  local gSlot = slot
  if slot ~= nil then
    local win
    if bag == 1 then
      win = gUI.Bag.GoodsBoxBag
      if gUI.Amass.Root:IsVisible() then
        Lua_Amass_InitNeedItems()
      end
      win:SetItemHideFlag(slot, hideflag)
    elseif bag == 2 then
      win = gUI.Bag.GoodsBoxTask
    end
    if win then
      win = WindowToGoodsBox(win)
      win:SetItemGoods(slot, icon, quality)
      local intenLev = Lua_Bag_GetStarInfo(bag, gSlot)
      if ForgeLevel_To_Stars[intenLev] > 0 then
        win:SetItemStarState(slot, ForgeLevel_To_Stars[intenLev])
      else
        win:SetItemStarState(slot, 0)
      end
      win:SetItemData(slot, quality)
      win:SetItemBindState(slot, Lua_Bag_GetBindInfo(bag, gSlot))
      win:SetIconState(slot, 8, false)
      win:SetIconState(slot, 7, canUse == 0)
      if count > 1 then
        win:SetItemSubscript(slot, tostring(count))
      end
      win:SetItemsLock(slot, 1, locked > 0)
      if rest == 0 then
        win:ClearItemCoolTime(slot)
      else
        win:SetItemCoolTime(slot, duration, rest)
      end
      win:SetItemUserLocked(slot, userlock > 0)
      win:SetItemOverdue(slot, lefttime <= 0)
    end
  end
end
function _OnBag_ItemDelete(bag, slot)
  if slot ~= nil then
    local win
    if bag == 1 then
      local bagindex
      bagindex, slot = _Bag_GetBagSlotClientToUI(slot)
      if 0 == bagindex then
        win = gUI.Bag.GoodsBoxBag
      end
      if gUI.Amass.Root:IsVisible() then
        Lua_Amass_InitNeedItems()
      end
    elseif bag == 2 then
      win = gUI.Bag.GoodsBoxTask
    end
    if win then
      win = WindowToGoodsBox(win)
      win:ClearGoodsItem(slot)
      win:SetItemData(slot, -1)
      win:SetItemsLock(slot, 1, false)
      win:SetItemBindState(slot, 0)
      win:SetIconState(slot, 8, false)
      win:SetIconState(slot, 7, false)
      win:SetItemHideFlag(slot, 0)
    end
    for index = 1, #gUI.Bag.NewItemMark do
      local newItem = gUI.Bag.NewItemMark[index]
      if bag == newItem.type and slot == newItem.slot then
        table.remove(gUI.Bag.NewItemMark, index)
        return
      end
    end
    RefreshCurrentUserGuide()
  end
end
function _OnBag_BagExtended()
  UpdateBagSlotCount()
  if gUI.Bag.WndRoot:IsVisible() then
    _Bag_PlaceBag()
  end
end
function _OnBag_BagSlotUpdate()
  UpdateBagSlotCount()
end
function _OnBag_ItemUse(arg1)
end
function _OnBag_SetBagItemCoolDown(slot1, duration, rest, isTask)
  local bag, slot = _Bag_GetBagSlotClientToUI(slot1)
  local wnd = gUI.Bag.GoodsBoxBag
  if isTask then
    wnd = gUI.Bag.GoodsBoxTask
    slot = slot1
    bag = 2
  end
  if bag then
    if rest == 0 then
      wnd:ClearItemCoolTime(slot)
    else
      wnd:SetItemCoolTime(slot, duration, rest)
    end
  end
end
function _OnBag_SetBag(index, name, ItemCount, maxCount, icon)
  if icon ~= "" then
    gUI.Bag.GboxExtBags:SetItemGoods(index, icon, -1)
    gUI.Bag.GboxExtBags:SetItemSubscript(index, ItemCount .. "/" .. maxCount)
  else
    gUI.Bag.GboxExtBags:ClearGoodsItem(index)
  end
  if maxCount <= ItemCount then
    gUI.Bag.GboxExtBags:SetItemSubcolor(index, 4294901760)
  else
    gUI.Bag.GboxExtBags:SetItemSubcolor(index, 4294967295)
  end
  local beginPos = _Bag_ContainerBaseSize + _Bag_ContainerExtSize * index
  local endPos = _Bag_ContainerBaseSize + _Bag_ContainerExtSize * (index + 1)
  for i = beginPos, endPos - 1 do
    gUI.Bag.GoodsBoxBag:SetItemVisible(i, true)
  end
  beginPos = beginPos + maxCount
  if endPos > beginPos and endPos <= _Bag_ContainerMaxSize then
    for i = beginPos, endPos - 1 do
      gUI.Bag.GoodsBoxBag:SetItemVisible(i, false)
    end
  end
end
function _OnBag_SetTotalBag(name, ItemCount, maxCount, extCount)
  gUI.Bag.GoodsBoxBag:SetItemCount(_Bag_ContainerMaxSize)
  gUI.Bag.BagSlotCount = _Bag_ContainerMaxSize
  gUI.Bag.GoodsBoxBag:ResetItemRect()
  gUI.Bag.ContainerBag:SizeSelf()
  _Bag_AdjustWndSize()
  local countLab = WindowToLabel(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Lab_bg.Bulk_dlab"))
  countLab:SetProperty("Text", tostring(ItemCount) .. "/" .. tostring(maxCount))
  gUI.Bag.BagExtNumCur = extCount
  local kuoLab = WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Lab_bg.Kuozhan_btn")
  kuoLab:SetVisible(extCount < _Bag_ContainerExtNum)
end
function _OnBag_SetTaskBag(name, ItemCount, maxCount)
  gUI.Bag.GoodsBoxTask:SetItemCount(_Bag_ContainerMaxSizeTask)
  gUI.Bag.TaskSlotCount = _Bag_ContainerMaxSizeTask
  gUI.Bag.GoodsBoxTask:ResetItemRect()
  gUI.Bag.ContainerTask:SizeSelf()
end
function _OnBag_UpdateMoney(arg1, arg2)
  if not arg1 then
    _, arg1, _ = GetBankAndBagMoney()
  end
  local goldLabel = WindowToLabel(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.MoneyDi_bg.MoneyAu_dlab"))
  goldLabel:SetProperty("Text", Lua_UI_Money2String(arg1))
  arg2 = arg2 or -1
  if arg2 == -1 then
    return
  elseif arg2 > 0 then
    WorldStage:playSoundByID(1)
  elseif arg2 < 0 then
    WorldStage:playSoundByID(2)
  else
    return
  end
end
function _OnBag_UpdateMoneyBind(arg1, arg2)
  if not arg1 then
    _, _, arg1 = GetBankAndBagMoney()
  end
  local goldLabel = WindowToLabel(WindowSys_Instance:GetWindow("Bag_frm.MainBag_bg.Di_bg.MoneyBindDi_bg.MoneyAuBind_dlab"))
  goldLabel:SetProperty("Text", Lua_UI_Money2String(arg1))
  arg2 = arg2 or -1
  if arg2 == -1 then
    return
  elseif arg2 > 0 then
    WorldStage:playSoundByID(1)
  elseif arg2 < 0 then
    WorldStage:playSoundByID(2)
  else
    return
  end
end
function _OnBag_MarkNewItem(bagType, slot)
  if not gUI.Bag.WndRoot:IsVisible() then
    local item = {type = bagType, slot = slot}
    table.insert(gUI.Bag.NewItemMark, item)
  end
end
function UI_Bag_Show()
  if not gUI.Bag.WndRoot:IsVisible() then
    Lua_Bag_ShowUI(true)
  else
    Lua_Bag_ShowUI(false)
  end
  WorldStage:playSoundByID(34)
end
function UI_Bag_ShowTip(msg)
  local goodsbox = msg:get_window()
  local box_type = goodsbox:GetProperty("BoxType")
  local slot = 0
  local tooltip = goodsbox:GetToolTipWnd(0)
  local bag = gUI_GOODSBOX_BAG[box_type]
  goodsbox = WindowToGoodsBox(goodsbox)
  if goodsbox:GetItemIcon(msg:get_wparam()) == "" then
    return
  end
  slot = Lua_Bag_GetRealSlot(goodsbox, msg:get_wparam())
  Lua_Tip_Item(tooltip, slot, bag, nil, 0, nil)
  Lua_Tip_ShowEquipCompare(goodsbox, bag, slot)
end
function UI_Bag_ShowTipExt(msg)
  local goodsbox = WindowToGoodsBox(msg:get_window())
  local tooltip = goodsbox:GetToolTipWnd(0)
  if goodsbox:GetItemIcon(msg:get_wparam()) == "" then
    tooltip = WindowToTooltip(tooltip)
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText("扩充栏", colorDarkBlue, "kaiti_13", 0, 0)
    tooltip:InsertLeftText("使用扩充类道具可以增加背包的容量。", colorWhite, "", 0, 0)
    Lua_Tip_End(tooltip)
    return
  else
    UI_Bag_ShowTip(msg)
  end
  local selected = goodsbox:GetActiveItemIndex()
  local bagBegin = _Bag_ContainerBaseSize + selected * _Bag_ContainerExtSize
  for index = 0, _Bag_ContainerMaxSize - 1 do
    if index >= bagBegin and index < bagBegin + _Bag_ContainerExtSize then
      gUI.Bag.GoodsBoxBag:SetItemQuality(index, _Bag_ExtSlotQualityID)
    else
      gUI.Bag.GoodsBoxBag:SetItemQuality(index, gUI.Bag.GoodsBoxBag:GetItemData(index))
    end
  end
end
function UI_Bag_Close()
  Lua_Bag_ShowUI(false)
end
function UI_Bag_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Bag_frm.TTitle_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Bag_SetShowTypeAll(msg)
  _Bag_SetShowTypeBtnState(0)
end
function UI_Bag_SetShowTypeEquip(msg)
  _Bag_SetShowTypeBtnState(1)
end
function UI_Bag_SetShowTypeGem(msg)
  _Bag_SetShowTypeBtnState(2)
end
function UI_Bag_SetShowTypeQuest(msg)
  _Bag_SetShowTypeBtnState(3)
end
function UI_Bag_SplitBtnClick(msg)
  local actionbar = WindowToGoodsBox(gUI.Bag.GoodsBoxBag:GetParent())
  actionbar:ResetDragItem()
  _Bag_IsInSplitItem = true
  SetMouseState(4)
  WorldStage:playSoundByID(24)
end
function UI_Bag_ExtendBtnClick(msg)
  if gUI.Bag.BagExtNumCur >= _Bag_ContainerExtNum then
    Lua_Chat_ShowOSD("BAG_EXT_NUM_FULL")
    return
  end
  local itemID = 361643
  local bag, slot = GetItemInBagIndex(itemID)
  if bag == gUI_GOODSBOX_BAG.backpack0 and slot ~= -1 then
    Messagebox_Show("KUOCHONG_BAG", itemID, false)
  else
    local itemIcon, itemName = GetItemInfosByTable(itemID)
    Lua_Chat_ShowOSD("BAG_EXT_NUM_NOITEM", itemName)
  end
end
function UI_Bag_SplitItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local from_off, to_off
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, msg:get_wparam())
  end
  if to_type == "backpack0" then
    to_off = Lua_Bag_GetRealSlot(to_win, msg:get_lparam())
  end
  SplitItem(gUI_GOODSBOX_BAG[to_type], to_off, gUI_GOODSBOX_BAG[from_type], from_off, _Bag_SplitItemCount)
end
function UI_Bag_SortBtnClick(msg)
  TidyBag(gUI_GOODSBOX_BAG.backpack0)
  TidyBag(gUI_GOODSBOX_BAG.backpack1)
  local wnd = msg:get_window()
  wnd:SetProperty("Enable", "false")
  wnd:SetProperty("Text", "&10&")
  WorldStage:playSoundByID(23)
end
function UI_Bag_GetItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  local from_win = WindowToGoodsBox(to_win:GetDragItemParent())
  local from_type = from_win:GetProperty("BoxType")
  local to_off = msg:get_lparam()
  local form_typeIndex = from_win:GetProperty("GoodBoxIndex")
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_bag = gUI_GOODSBOX_BAG[to_type]
  if string.find(from_type, "action_") then
    return
  end
  if to_type == "backpack1" then
    return
  end
  if from_bag == 1 then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
  end
  if to_bag == 1 then
    to_off = Lua_Bag_GetRealSlot(to_win, to_off)
  end
  if from_bag == 4 then
  end
  if to_bag == 4 then
  end
  if from_type == "exchange" then
    SwapClearTradeItem(from_off)
  elseif Lua_Bag_IsEquipbox(from_type) then
    from_off = from_win:GetItemData(from_off)
    MoveItem(from_bag, from_off, to_bag, to_off)
  elseif Lua_Bag_IsBackPackbox(from_type) then
    if form_typeIndex == "GB_Forge" then
      Lua_Forge_DragItem(from_win, gUI_GOODSBOX_BAG.backpack0)
      return
    elseif form_typeIndex == "GB_Blend" then
      UI_Blend_SideEquipRClick(msg)
    elseif form_typeIndex == "GB_Blend0" then
      UI_Blend_MainEquipRClick(msg)
    end
    MoveItem(from_bag, from_off, to_bag, to_off)
  elseif from_type == "Gbankbag" then
    from_off = Lua_GBank_ConvertLocalSlotToRealSlot(from_off)
    MoveItem(from_bag, from_off, to_bag, to_off)
  elseif Lua_Bag_IsBankBagbox(from_type) then
    MoveItem(from_bag, from_off, to_bag, to_off)
  elseif Lua_Bag_IsActionbtn(from_type) then
    WorldStage:delActionBtn(mainmenubar_GetActionbtnIndex(from_type, from_off))
  elseif form_typeIndex == "GB_GetMail" then
    UI_Email_GetRecvtem()
  elseif form_typeIndex == "GB_SetMail" then
    from_win:ClearGoodsItem(0)
    ItemRemoveSendMail()
  elseif form_typeIndex == "GB_GemBlend" then
    local index = tonumber(from_win:GetProperty("BoxType"))
    if index == 0 then
      GemCompose_RomoveAllItem()
    else
    end
  elseif form_typeIndex == "GB_GemEmbed" then
    _GemBeset_Cancel()
    SetGemBeset(1, 0, OperateMode.Cancel, GEMBESETSLOTCOMP.EQUIPSLOT)
  elseif form_typeIndex == "GB_GemExtraction" then
    RemoveSplitItem()
    _GemEx_ClearUI()
  elseif form_typeIndex == "GB_Extend" then
    local index = tonumber(from_win:GetProperty("BoxType"))
    InheritRemoveEquip(index)
  elseif form_typeIndex == "GB_Stall" then
    if not IsStall() then
      ClearStallItem(from_off)
    end
  elseif form_typeIndex == "GB_Scroll" then
    CancleStoremapItem()
    Lua_Scroll_DelItem()
  end
  WorldStage:playSoundByID(31)
end
function UI_Bag_ItemLbtndown(msg)
  local from_type = msg:get_window():GetProperty("BoxType")
  local from_off = msg:get_lparam()
  local bag = gUI_GOODSBOX_BAG[from_type]
  local mouse_state = GetMouseState()
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(msg:get_window(), from_off)
  end
  if IsKeyPressed(29) then
    Lua_Chat_AddLinkItem(bag, from_off)
    WindowToGoodsBox(msg:get_window()):ResetDragItem()
  end
  if _Bag_IsInSplitItem or UIInterface:IsShiftPressed() then
    local actionbar = WindowToGoodsBox(gUI.Bag.GoodsBoxBag:GetParent())
    actionbar:ResetDragItem()
    local num, id, quality, name, _, _, _, locked = GetItemBaseInfoBySlot(bag, from_off)
    if num and num > 1 and locked == false then
      _Bag_SplitCurrentSlot = from_off
      _Bag_SplitWnd = WindowToGoodsBox(msg:get_window())
      _Bag_SplitBagType = from_type
      Lua_Split_Inidata(num, _Bag_SplitItemCallback)
      Lua_Split_Show(true)
      _Bag_IsInSplitItem = false
      SetMouseState(0)
    else
      Lua_Chat_ShowOSD("BAG_SPLITER_DISABLE")
    end
  end
  WorldStage:playSoundByID(30)
end
function UI_Bag_ItemRbtndown(msg)
  local from_type = msg:get_window():GetProperty("BoxType")
  local from_off = msg:get_lparam()
  local slot = from_off
  local bag = gUI_GOODSBOX_BAG[from_type]
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(msg:get_window(), from_off)
  end
  if gUI.GemCompose.Root:IsVisible() then
    GemCompose_SendAddGemToServer(gUI_GOODSBOX_BAG[from_type], from_off)
    return
  end
  if gUI.GemAdorn.Root:IsVisible() and gUI.GemAdorn.bRoot:IsVisible() then
    local name, icon, quality, minLevel, _, _, _, _, _, _, _, _, _, itemType, _, _, _, _, _, _, equipKind = GetItemInfoBySlot(bag, from_off, 0)
    if itemType == 2 then
      SetNpcFunctionEquip(gUI_GOODSBOX_BAG[from_type], from_off, OperateMode.Add, GEMADORNSLOT.EQUIPSLOT, NpcFunction.NPC_FUNC_GEM_EMBED)
    elseif itemType == 8 then
      SetNpcFunctionEquip(gUI_GOODSBOX_BAG[from_type], from_off, OperateMode.Add, GEMADORNSLOT.MATERSLOT, NpcFunction.NPC_FUNC_GEM_EMBED)
    end
    return
  end
  if gUI.GemAdorn.Root:IsVisible() and gUI.GemAdorn.eRoot:IsVisible() then
    SetSplitItem(gUI_GOODSBOX_BAG[from_type], from_off)
    return
  end
  if gUI.GemIneff.Root:IsVisible() then
    GemIneff_SetGemToNpcSer(gUI_GOODSBOX_BAG[from_type], from_off)
    return
  end
  if gUI.GemChange.Root:IsVisible() then
    Lua_GemChange_PlaceMainSlot(gUI_GOODSBOX_BAG[from_type], from_off)
    return
  end
  if gUI.InheritNew.Root:IsVisible() then
    local mBag, mSlot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_EQUIP_SMRITI, INHERITNEW_SLOT_COLLATE.MAINEQUIP)
    if mBag and mSlot then
      Lua_InheritNew_CheckSubEquip(from_off)
    else
      Lua_InheritNew_CheckMainEquip(from_off)
    end
    return
  end
  if gUI.Ineffective.Root:IsVisible() then
    local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType, _, _, _, _, _, _, equipKind = GetItemInfoBySlot(bag, from_off, 0)
    if itemType == 2 then
      Ineffective_SendAddEquipToServer(gUI_GOODSBOX_BAG.backpack0, msg:get_lparam())
    elseif itemType == 8 then
      Ineffective_SendAddMaterToServer(gUI_GOODSBOX_BAG.backpack0, msg:get_lparam())
    else
      Lua_Chat_ShowOSD("INEFFECTIVE_CANACC_EQUIPGEM")
    end
    return
  end
  if gUI.SignItem.Root:IsVisible() then
    SignItem_PlaceSlot(msg:get_window(), msg:get_lparam())
    return
  end
  if gUI.GemSeniorBlend.Root:IsVisible() then
    local index = GetCurrGenSeniorBlendIndex()
    GemSen_PlaceSlot(msg:get_lparam(), index)
    return
  end
  if gUI.Authenticate.Root:IsVisible() then
    local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType, _, _, _, _, _, _, equipKind = GetItemInfoBySlot(bag, from_off, 0)
    if itemType == 2 then
      Authenticate_SendAddActionToServer(gUI_GOODSBOX_BAG.backpack0, msg:get_lparam())
    else
      Authenticate_SendAddMaterToServer(gUI_GOODSBOX_BAG.backpack0, msg:get_lparam())
    end
    return
  end
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    local mainbag, mainslot = GetEnhanceSlotInfo(FORGE_SLOT_COLLATE.EQUIPSLOT)
    local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType, _, _, _, _, _, _, equipKind = GetItemInfoBySlot(bag, from_off, 0)
    if itemType == 2 then
      Forge_CheckEquip(from_off, bag)
      return
    else
      if not mainbag then
        Lua_Chat_ShowOSD("FORGE_NO_EQUIPMENT")
        return
      end
      local currentSlot = Forge_GetCurrentSlotCanPlaced()
      Forge_CheckLuckyStone(from_off, currentSlot, gUI_GOODSBOX_BAG.backpack0)
      return
    end
  end
  local wnd = WindowSys_Instance:GetWindow("MountForge")
  if wnd:IsVisible() and Lua_MountForge_IsMount(from_off, bag) then
    local mountLev = GetMountEnhanceLevel(from_off, gUI_GOODSBOX_BAG.backpack0)
    local mountMaxLev = GetMountMaxLevel(from_off, gUI_GOODSBOX_BAG.backpack0)
    if mountLev >= mountMaxLev then
      Lua_Chat_ShowOSD("MOUNTFORGE_MAXLEVEL")
      return
    end
    SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, from_off, OperateMode.Add, MOUNTFORGE_SLOT_COLLATE.EQUIPSLOT, NpcFunction.NPC_FUNC_MOUNT_INTENSIFY)
    return
  end
  local wnd = WindowSys_Instance:GetWindow("Blend")
  if wnd:IsVisible() then
    local materialbind, materialunbind, name = Lua_BlendStoneId()
    local itemId = GetTableIdBySlot(gUI_GOODSBOX_BAG.backpack0, from_off)
    local _, _, _, _, _, _, _, _, _, _, _, _, _, itemType, _, _, _, _, _, _, equipKind = GetItemInfoBySlot(bag, from_off, 0)
    if itemType ~= 2 then
      local mbag, mslot = GetBlendSlotInfo(BLEND_SLOT_COLLATE.MAINEQUIP)
      if not mslot then
        Lua_Chat_ShowOSD("BLEND_NOT_RIGHT_EQUIPED")
        return
      else
        Lua_Chat_ShowOSD("BLEND_NOT_RIGHT_SUB")
        return
      end
    else
      local dir = Lua_Blend_GetRBtnTargetSlot()
      if dir == "AddMainEquip" then
        Lua_Blend_CheckMainEquip(from_off)
      elseif dir == "AddSideEquip" then
        Lua_Blend_CheckSideEquip(from_off)
      end
      return
    end
  end
  if gUI.Stall.Root:IsVisible() and gUI.Stall.Goods_gbox:IsVisible() then
    Lua_Stall_AddItem(msg:get_window(), slot)
    return
  end
  if gUI.Exchange.Root:IsVisible() then
    Lua_Exchange_ApplyAddItem(msg:get_window(), slot)
    return
  end
  if gUI.Resolve.Root:IsVisible() then
    local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, msg:get_lparam(), 0)
    ResolveAddOrDel(guid)
    return
  end
  if gUI.Faerie_Income.Reduce:IsVisible() then
    local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, msg:get_lparam(), 0)
    ResolveItemAddOrDel(guid)
    return
  end
  local wnd = WindowSys_Instance:GetWindow("EmailWrite")
  if wnd:IsVisible() then
    Lua_Email_AddItem(bag, from_off)
    return
  end
  if gUI.Bank.Root:IsVisible() then
    Lua_Bank_StoreItemByRClick(from_off, from_type)
    return
  end
  if gUI.GBank.Root:IsVisible() then
    Lua_GBank_StoreItemByRClick(from_off, from_type)
    return
  end
  if gUI.NpcShop.Root:IsVisible() then
    Lua_Shop_SellItem(gUI_GOODSBOX_BAG[from_type], from_off)
    return
  end
  if gUI.CGScroll.Root:IsVisible() then
    Lua_Scroll_DropItem(msg:get_window(), msg:get_lparam())
    return
  end
  local msState = GetMouseState()
  if wndSplitItem and wndSplitItem:IsSplitItem() then
    SetMouseState(0)
    _Bag_IsInSplitItem = false
  elseif msState == 6 or msState == 4 then
    SetMouseState(0)
  else
    Lua_Bag_UseItemCheck(bag, from_off)
  end
end
function UI_Bag_ItemRbtndownExt(msg)
  local from_type = msg:get_window():GetProperty("BoxType")
  local from_off = msg:get_lparam()
  local slot = from_off
  local bag = gUI_GOODSBOX_BAG[from_type]
  UseItem(bag, from_off)
end
function UI_Bag_UnSelectItem(msg)
  WorldStage:SetItemCursor(0)
end
function UI_Bag_UnSelectItemExt(msg)
  WorldStage:SetItemCursor(0)
  for index = 0, _Bag_ContainerMaxSize - 1 do
    gUI.Bag.GoodsBoxBag:SetItemQuality(index, gUI.Bag.GoodsBoxBag:GetItemData(index))
  end
end
function UI_Bag_TimeUpSortBtn(msg)
  local wnd = msg:get_window()
  wnd:SetProperty("Enable", "true")
  wnd:SetProperty("Text", "整理")
end
function UI_Manufacture_Tip(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  if level < gUI_MainBtn_ActiveFlag.ManufaceLevel then
    local strTooltip = "30级可使用制造功能"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_Resolve_TipNew(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  if level < gUI_MainBtn_ActiveFlag.ResovleLevel then
    local strTooltip = "30级可使用装备分解功能"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_IneffBtn_Tip(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  if level < gUI_MainBtn_ActiveFlag.IneffLevel then
    local strTooltip = "0级可使用附魔"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_BlendBtn_TipNew(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  if level < gUI_MainBtn_ActiveFlag.BlendLevel then
    local strTooltip = "12级可使用装备融魂"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_ForgeBtn_TipNew(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  if level < gUI_MainBtn_ActiveFlag.ForgeLevel then
    local strTooltip = "20级可使用装备锻造"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_Signture_TipNew(msg)
  local wnd = msg:get_window()
  local tooltip = wnd:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local _, _, _, _, level, level_nimbus = GetPlaySelfProp(4)
  if level < gUI_MainBtn_ActiveFlag.SignatureLevel then
    local strTooltip = "10级可使用装备签名"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function Script_Bag_OnLoad()
  _Bag_InsertBag()
  _Bag_InsertTaskBag()
  gUI.Bag.ItemGUIDList = {}
  gUI.Bag.ItemGUIDListOld = {}
  gUI.Bag.NewItemMark = {}
  gUI.Bag.BagExtNumCur = 0
end
function Script_Bag_OnEvent(event)
  if event == "BAG_EXTENDED" then
    _OnBag_BagExtended()
  elseif event == "BAG_SLOTUPDATE" then
    _OnBag_BagSlotUpdate()
  elseif event == "ITEM_ADD_TO_BAG" then
    _OnBag_ItemAdd(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12)
  elseif event == "ITEM_DEL_FROM_BAG" then
    _OnBag_ItemDelete(arg1, arg2)
  elseif event == "ITEM_USE_ITEM_BAG" then
    _OnBag_ItemUse(arg1)
  elseif event == "ITEM_ADD_GET_ITEM_LOG" then
    Lua_Pick_OnGetNewItem(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "ITEM_REMOVE_ITEM_LOG" then
    Lua_Pick_OnRemoveItem(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "ITEM_ADD_EXCEPT_BASE_CONTAINER" then
    Lua_Bag_NewItemExceptBaseContainer(arg1, arg2)
  elseif event == "ITEM_DROP_ITEM_LOG" then
    Lua_Pick_OnDropItem()
  elseif event == "ITEM_REMOVE_LOG" then
  elseif event == "BAG_SET_BAG" then
    _OnBag_SetBag(arg1, arg2, arg3, arg4, arg5)
  elseif event == "BAG_SET_TOTAL_BAG" then
    _OnBag_SetTotalBag(arg1, arg2, arg3, arg4)
  elseif event == "BAG_SET_TASK_BAG" then
    _OnBag_SetTaskBag(arg1, arg2, arg3)
  elseif event == "PLAYER_MONEY_CHANGED" then
    _OnBag_UpdateMoney(arg1, arg2)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    _OnBag_UpdateMoneyBind(arg1, arg2)
  elseif event == "BAG_ITEM_COOLDOWN" then
    _OnBag_SetBagItemCoolDown(arg1, arg2, arg3, arg4)
  elseif event == "CANCEL_OPERATOR" then
    Lua_Bag_CancelSplitItem()
  elseif event == "ITEM_BAG_NEW" then
    _OnBag_MarkNewItem(arg1, arg2)
  end
end
function Script_Bag_OnHide()
  _Bag_IsInSplitItem = false
  WorldStage:SetItemCursor(0)
  _Bag_PlaceBag()
end

