if gUI and not gUI.Bank then
  gUI.Bank = {}
end
local _Bank_ContainerBaseSize = 40
local _Bank_ContainerExtSize = 10
local _Bank_ContainerExtNum = 8
local _Bank_ContainerMaxSize = _Bank_ContainerBaseSize + _Bank_ContainerExtSize * _Bank_ContainerExtNum
local _Bank_ExtSlotQualityID = 20
function _Bank_Close()
  if gUI.Bank.Root:IsVisible() then
    gUI.Bank.Root:SetProperty("Visible", "false")
    local wndRoot = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(wndRoot)
  end
end
function UI_Bank_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Bank.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _Bank_AdjustWndSize()
  local widthWnd = RenderSys_Instance:GetPixelWidth()
  local heightWnd = RenderSys_Instance:GetPixelHeight()
  local itemBoxHeight = gUI.Bank.GoodsBox:GetHeight()
  local picItemBox = WindowSys_Instance:GetWindow("Bank.Pic1_bg")
  local picItemBoxHeight = itemBoxHeight + 12 / heightWnd
  picItemBox:SetHeight(picItemBoxHeight)
  gUI.Bank.Root:SetHeight(picItemBox:GetHeight() + 95 / heightWnd)
end
function _OnBank_Extend()
  UpdateBankSlotCount()
  if gUI.Bank.Root:IsVisible() then
    _Bank_AdjustWndSize()
  end
end
function _OnBank_AddItem(index, icon, count)
  local quality = Lua_Bag_GetQuality(gUI_GOODSBOX_BAG[gUI.Bank.GoodsBox:GetProperty("BoxType")], index)
  local bindinfo = Lua_Bag_GetBindInfo(gUI_GOODSBOX_BAG[gUI.Bank.GoodsBox:GetProperty("BoxType")], index)
  gUI.Bank.GoodsBox:SetItemGoods(index, icon, quality)
  local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG[gUI.Bank.GoodsBox:GetProperty("BoxType")], index)
  if ForgeLevel_To_Stars[intenLev] > 0 then
    gUI.Bank.GoodsBox:SetItemStarState(index, ForgeLevel_To_Stars[intenLev])
  else
    gUI.Bank.GoodsBox:SetItemStarState(index, 0)
  end
  gUI.Bank.GoodsBox:SetItemBindState(index, bindinfo)
  gUI.Bank.GoodsBox:SetItemData(index, quality)
  if count > 1 then
    gUI.Bank.GoodsBox:SetItemSubscript(index, tostring(count))
  end
end
function _OnBank_DelItem(index)
  gUI.Bank.GoodsBox:ClearGoodsItem(index)
  gUI.Bank.GoodsBox:SetItemBindState(index, 0)
  gUI.Bank.GoodsBox:SetItemData(index, -1)
end
function _OnBank_Show()
  if not gUI.Bank.Root:IsVisible() then
    Lua_Bag_ShowUI(true)
    _Bank_AdjustWndSize()
    gUI.Bank.Root:SetVisible(true)
  end
end
function _OnBank_Close()
  _Bank_Close()
end
function _OnBank_SetBag(index, name, ItemCount, maxCount, icon)
  if icon ~= "" then
    gUI.Bank.GboxExtBags:SetItemGoods(index, icon, -1)
    gUI.Bank.GboxExtBags:SetItemSubscript(index, ItemCount .. "/" .. maxCount)
  else
    gUI.Bank.GboxExtBags:ClearGoodsItem(index)
  end
  if maxCount <= ItemCount then
    gUI.Bank.GboxExtBags:SetItemSubcolor(index, 4294901760)
  else
    gUI.Bank.GboxExtBags:SetItemSubcolor(index, 4294967295)
  end
  local beginPos = _Bank_ContainerBaseSize + _Bank_ContainerExtSize * index
  local endPos = _Bank_ContainerBaseSize + _Bank_ContainerExtSize * (index + 1)
  for i = beginPos, endPos - 1 do
    gUI.Bank.GoodsBox:SetItemVisible(i, true)
  end
  beginPos = beginPos + maxCount
  if endPos > beginPos and endPos <= _Bank_ContainerMaxSize then
    for i = beginPos, endPos - 1 do
      gUI.Bank.GoodsBox:SetItemVisible(i, false)
    end
  end
end
function _OnBank_SetTotalBag(name, ItemCount, maxCount, extCount)
  gUI.Bank.GoodsBox:SetItemCount(_Bank_ContainerMaxSize)
  gUI.Bank.GoodsBox:ResetItemRect()
  _Bank_AdjustWndSize()
  local countLab = WindowToLabel(WindowSys_Instance:GetWindow("Bank.Bulk_dlab"))
  countLab:SetProperty("Text", tostring(ItemCount) .. "/" .. tostring(maxCount))
  gUI.Bank.BankExtNumCur = extCount
  local kuoLab = WindowSys_Instance:GetWindow("Bank.KuoZhan_btn")
  kuoLab:SetVisible(extCount < _Bank_ContainerExtNum)
end
function UI_Bank_Drop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  if Lua_Bag_IsBackPackbox(from_type) then
    from_off = Lua_Bag_GetRealSlot(from_win, from_off)
    local tooffset = msg:get_lparam()
    MoveItem(gUI_GOODSBOX_BAG[from_type], from_off, gUI_GOODSBOX_BAG[to_type], tooffset)
  elseif Lua_Bag_IsBankBagbox(from_type) then
    local tooffset = msg:get_lparam()
    MoveItem(gUI_GOODSBOX_BAG[from_type], from_off, gUI_GOODSBOX_BAG[to_type], tooffset)
  end
end
function UI_Bank_ShowTooltip(msg)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  local slot = msg:get_wparam()
  Lua_Tip_Item(wnd:GetToolTipWnd(0), slot, gUI_GOODSBOX_BAG.bankbag, nil, nil, nil)
  Lua_Tip_ShowEquipCompare(wnd, gUI_GOODSBOX_BAG.bankbag, slot)
end
function UI_Bank_ShowTooltipExt(msg)
  local goodsbox = WindowToGoodsBox(msg:get_window())
  local tooltip = goodsbox:GetToolTipWnd(0)
  local wnd = msg:get_window()
  wnd = WindowToGoodsBox(wnd)
  tooltip = WindowToTooltip(tooltip)
  if goodsbox:GetItemIcon(msg:get_wparam()) == "" then
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText("扩充栏", colorDarkBlue, "kaiti_13", 0, 0)
    tooltip:InsertLeftText("使用扩充类道具可以增加仓库的容量。", colorWhite, "", 0, 0)
    Lua_Tip_End(tooltip)
    return
  else
    local slot = msg:get_wparam()
    Lua_Tip_Item(tooltip, slot, gUI_GOODSBOX_BAG.bankbagex1, nil, nil, nil)
  end
  local selected = goodsbox:GetActiveItemIndex()
  local bagBegin = _Bank_ContainerBaseSize + selected * _Bank_ContainerExtSize
  for index = 0, _Bank_ContainerMaxSize - 1 do
    if index >= bagBegin and index < bagBegin + _Bank_ContainerExtSize then
      gUI.Bank.GoodsBox:SetItemQuality(index, _Bank_ExtSlotQualityID)
    else
      gUI.Bank.GoodsBox:SetItemQuality(index, gUI.Bank.GoodsBox:GetItemData(index))
    end
  end
end
function UI_Bank_SortBtn()
  SortBank()
  gUI.Bank.SortBtn:SetProperty("Enable", "false")
  gUI.Bank.SortBtn:SetProperty("Text", "整理&10&")
end
function UI_Bank_RClick(msg)
  local from_type = msg:get_window():GetProperty("BoxType")
  local from_off = msg:get_lparam()
  local bag = gUI_GOODSBOX_BAG[from_type]
  MoveItem(bag, from_off, gUI_GOODSBOX_BAG.backpack0, -1)
end
function UI_Bank_RClickExt(msg)
end
function UI_Bank_CloseBtn()
  _Bank_Close()
end
function UI_Bank_TimeUpSortBtn(msg)
  local wnd = msg:get_window()
  wnd:SetProperty("Enable", "true")
  wnd:SetProperty("Text", "整理仓库")
end
function UI_Bank_UnSelectItemExt(msg)
  WorldStage:SetItemCursor(0)
  for index = 0, _Bank_ContainerMaxSize - 1 do
    gUI.Bank.GoodsBox:SetItemQuality(index, gUI.Bank.GoodsBox:GetItemData(index))
  end
end
function UI_Bank_ExtendBtnClick(msg)
  if gUI.Bank.BankExtNumCur >= _Bank_ContainerExtNum then
    Lua_Chat_ShowOSD("BAG_EXT_NUM_FULL")
    return
  end
  local itemID = 361643
  local bag, slot = GetItemInBagIndex(itemID)
  if bag == gUI_GOODSBOX_BAG.backpack0 and slot ~= -1 then
    Messagebox_Show("KUOCHONG_BAG", itemID, true)
  else
    local itemIcon, itemName = GetItemInfosByTable(itemID)
    Lua_Chat_ShowOSD("BAG_EXT_NUM_NOITEM", itemName)
  end
end
function Lua_Bank_StoreItemByRClick(from_off, from_type)
  MoveItem(gUI_GOODSBOX_BAG[from_type], from_off, gUI_GOODSBOX_BAG.bankbag, -1)
end
function Lua_Bank_CloseBank()
  _Bank_Close()
end
function Script_Bank_OnLoad()
  gUI.Bank.Root = WindowSys_Instance:GetWindow("Bank")
  gUI.Bank.GoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Bank.Pic1_bg.GoodsBox1_gbox"))
  gUI.Bank.GboxExtBags = WindowToGoodsBox(WindowSys_Instance:GetWindow("Bank.Pic2_bg.GoodsBox2_gbox"))
  gUI.Bank.SortBtn = WindowSys_Instance:GetWindow("Bank.TidyUp_btn")
  gUI.Bank.BagIcon = "tg_item01:05"
  gUI.Bank.BankExtNumCur = 0
end
function Script_Bank_OnHide()
  _Bank_Close()
end
function Script_Bank_OnEvent(event)
  if event == "BANK_EXTENDED" then
    _OnBank_Extend()
  elseif event == "ITEM_ADD_TO_BANK" then
    _OnBank_AddItem(arg1, arg2, arg3)
  elseif event == "ITEM_DEL_FROM_BANK" then
    _OnBank_DelItem(arg1)
  elseif event == "BANK_SHOW_FRAME" then
    _OnBank_Show()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    _OnBank_Close()
  elseif event == "BANK_SET_BAG" then
    _OnBank_SetBag(arg1, arg2, arg3, arg4, arg5)
  elseif event == "BANK_SET_TOTAL_BAG" then
    _OnBank_SetTotalBag(arg1, arg2, arg3, arg4)
  end
end
    _OnBank_SetBag(arg1, arg2, arg3, arg4, arg5)
  elseif event == "BANK_SET_TOTAL_BAG" then
    _OnBank_SetTotalBag(arg1, arg2, arg3, arg4)
  end
end
