if gUI and not gUI.CGPlacing then
  gUI.CGPlacing = {}
end
local PlacingOpen = 1
local PlacingClose = 0
local PlacingOuter = 100
function UI_Placing_Click(msg)
  UI_Placing_OnPanelOpenClose()
end
function UI_Placing_OnPanelOpenClose()
  if not gUI.CGPlacing.Root:IsVisible() then
    gUI.CGPlacing.Root:SetVisible(true)
    NotifyServerOpenPlacingPanel(PlacingOpen)
  else
    gUI.CGPlacing.Root:SetVisible(false)
    NotifyServerOpenPlacingPanel(PlacingClose)
  end
end
function Lua_Placing_InitDataBegin()
  Lua_Placing_ClearData()
end
function Lua_Placing_SetDatasAdding(m_nIndex, m_nPlayerName, m_sPlayerJob, m_nKill, m_nVictim)
  local str_index
  if m_nIndex == 1 or m_nIndex == 2 or m_nIndex == 3 then
    str_index = tostring(" ")
  else
    str_index = tostring(m_nIndex)
  end
  local strShowContent
  if m_nPlayerName == "--" then
    strShowContent = string.format("%s|%s|%s|%s|%s", tostring(str_index), tostring(m_nPlayerName), tostring("--"), tostring(m_nKill), tostring(m_nVictim))
  else
    strShowContent = string.format("%s|%s|%s|%s|%s", tostring(str_index), tostring(m_nPlayerName), tostring(gUI_MenPaiName[m_sPlayerJob]), tostring(m_nKill), tostring(m_nVictim))
  end
  gUI.CGPlacing.ShowList:InsertString(strShowContent, -1)
end
function Lua_Placing_DataEnd()
end
function Lua_Placing_UpdateCurrentRanking(currRankings)
  local rankStr
  if currRankings == 1 then
    rankStr = " 恭喜荣登榜首！笑傲群雄！ "
  elseif currRankings == 2 then
    rankStr = "恭喜荣登榜眼！"
  elseif currRankings == 3 then
    rankStr = "恭喜晋级三甲！"
  elseif currRankings > 3 and currRankings < 11 then
    rankStr = string.format("你当前排名为第%d名，非常不错！", currRankings)
  else
    rankStr = string.format("你没有进入当前排名，请继续努力!")
  end
  gUI.CGPlacing.CurrRanking:SetProperty("Text", tostring(rankStr))
end
function Lua_Placing_ClearData()
  gUI.CGPlacing.ShowList:RemoveAllItems()
end
function _OnPlacing_UpdateDataBegin()
  Lua_Placing_InitDataBegin()
end
function _OnPlacing_UpdateDataAdding(m_nIndex, m_nPlayerName, m_sPlayerJob, m_nKill, m_nVictim)
  Lua_Placing_SetDatasAdding(m_nIndex, m_nPlayerName, m_sPlayerJob, m_nKill, m_nVictim)
end
function _OnPlacing_UpdateDataEnd()
  Lua_Placing_DataEnd()
end
function _OnPlacing_UpdateCurrentRank(currRankings)
  Lua_Placing_UpdateCurrentRanking(currRankings)
end
function UI_Placing_CloseClick(msg)
  UI_Placing_OnPanelOpenClose()
end
function Script_Placing_OnEvent(event)
  if event == "PLACING_BEGIN_UPDATEDATD" then
    _OnPlacing_UpdateDataBegin()
  elseif event == "PLACING_ADDING_UPDATEDATD" then
    _OnPlacing_UpdateDataAdding(arg1, arg2, arg3, arg4, arg5)
  elseif event == "PLACING_END_UPDATEDATD" then
    _OnPlacing_UpdateDataEnd()
  elseif event == "PLACING_CURR_UPDATEDATA" then
    _OnPlacing_UpdateCurrentRank(arg1)
  end
end
function Script_Placing_OnLoad()
  gUI.CGPlacing.Root = WindowSys_Instance:GetWindow("CGPlacing")
  gUI.CGPlacing.ShowList = WindowToListBox(WindowSys_Instance:GetWindow("CGPlacing.RankingImagePic_bg.LevelImage_bg.LevelBox_lbox"))
  gUI.CGPlacing.CloseBtn = WindowToButton(WindowSys_Instance:GetWindow("CGPlacing.RankingImagePic_bg.Cancel_btn"))
  gUI.CGPlacing.CurrRanking = WindowToLabel(WindowSys_Instance:GetWindow("CGPlacing.RankingImagePic_bg.RankImage_bg.NumberImage_bg.ChannelLab_dlab"))
end
function Script_Placing_OnHide()
end
function test()
  Lua_Placing_InitDataBegin()
  for i = 0, 9 do
    Lua_Placing_SetDatasAdding(i + 1, "admin", "神将", "dddd", "dddddd")
  end
  Lua_Placing_DataEnd()
  Lua_Placing_UpdateCurrentRanking(1)
end
