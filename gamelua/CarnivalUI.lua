if gUI and not gUI.CarnivalUI then
  gUI.CarnivalUI = {}
end
local _CarnivalUI_NumPic = {
  [0] = "{ImAgE^UiTitle002:Title_0}",
  [1] = "{ImAgE^UiTitle002:Title_1}",
  [2] = "{ImAgE^UiTitle002:Title_2}",
  [3] = "{ImAgE^UiTitle002:Title_3}",
  [4] = "{ImAgE^UiTitle002:Title_4}",
  [5] = "{ImAgE^UiTitle002:Title_5}",
  [6] = "{ImAgE^UiTitle002:Title_6}",
  [7] = "{ImAgE^UiTitle002:Title_7}",
  [8] = "{ImAgE^UiTitle002:Title_8}",
  [9] = "{ImAgE^UiTitle002:Title_9}"
}
local _CarnivalUI_End = {
  [1] = "UiBtn001:Image_Win",
  [2] = "UiBtn001:Image_succeed"
}
local _CarnivalUI_Di = "{ImAgE^UiTitle002:Title_di}"
local _CarnivalUI_Guan = "{ImAgE^UiTitle002:Title_guan}"
local _CarnivalUI_OSDTime = 2
local _CarnivalUI_PointTime = 0
local _CarnivalUI_Lv = 1
local _CarnivalUI_IsEnd = false
local _CarnivalUI_Stimer = 900
local _CarnivalUI_MaxMun = 0
local _CarnivalUI_STRONGHOLD_STATUS = {
  [0] = "摧毁",
  [1] = "占领",
  [2] = "偷袭"
}
local _CarnivalUI_RESPAWN_STATUS = {
  [0] = "占领",
  [1] = "突袭"
}
local _CarnivalUI_TEAM = {
  [0] = "蓝方",
  [1] = "红方"
}
local _CarnivalUI_MINE_STATUS = {
  [0] = "未占领",
  [1] = "蓝方占领",
  [2] = "红方占领"
}
local _CarnivalUI_BOSS_STATUS = {
  [0] = "未出生",
  [1] = "存活",
  [2] = "蓝方击倒",
  [3] = "红方击倒"
}
local _CarnivalUI_BGHieghtOpen = 255
local _CarnivalUI_BGHieghtClose = 25
local _CarnivalUI_BGMapID = 130
local _CarnivalUI_BGMapID1 = 131
function _CarnivalUI_GetLvPic(lv)
  local strPic = ""
  if lv >= 10 and lv < 100 then
    local nlv1 = math.floor(lv / 10)
    local nlv2 = lv - nlv1 * 10
    strPic = _CarnivalUI_Di .. _CarnivalUI_NumPic[nlv1] .. _CarnivalUI_NumPic[nlv2] .. _CarnivalUI_Guan
  else
    strPic = _CarnivalUI_Di .. _CarnivalUI_NumPic[lv] .. _CarnivalUI_Guan
  end
  return strPic
end
function _CarnivalUI_OSDShow(timer)
  _CarnivalUI_OSDTime = _CarnivalUI_OSDTime - timer:GetRealInterval()
  if _CarnivalUI_OSDTime < 0.1 then
    gUI.CarnivalUI.RewardOSD:SetVisible(false)
    DelTimerEvent(2, "ShowOSD")
  end
end
function _CarnivalUI_PointShow(timer)
  _CarnivalUI_PointTime = _CarnivalUI_PointTime + 1
  if _CarnivalUI_PointTime == 30 then
    gUI.CarnivalUI.Reward:SetVisible(true)
  elseif _CarnivalUI_PointTime == 45 then
    gUI.CarnivalUI.GainExp:SetVisible(true)
    gUI.CarnivalUI.GainVig:SetVisible(true)
  elseif _CarnivalUI_PointTime == 50 then
    gUI.CarnivalUI.AllExp:SetVisible(true)
    gUI.CarnivalUI.AllVig:SetVisible(true)
  elseif _CarnivalUI_PointTime == 55 then
    gUI.CarnivalUI.Succeed:SetVisible(true)
    DelTimerEvent(2, "ShowPoint")
  end
end
function _CarnivalUI_UpdateBGinfo()
  local buleSore, buleLeaderAlive, redSore, redLeaderAlive, respawnState1, respawnTeam1, respawnState2, respawnTeam2 = GuildContestBGinfo()
  gUI.CarnivalUI.Bule:GetChildByName("Ji_dlab"):SetProperty("Text", tostring(buleSore))
  gUI.CarnivalUI.Red:GetChildByName("Ji_dlab"):SetProperty("Text", tostring(redSore))
  if buleLeaderAlive then
    gUI.CarnivalUI.Bule:GetChildByName("Shou_dlab"):SetProperty("Text", "存活")
  else
    gUI.CarnivalUI.Bule:GetChildByName("Shou_dlab"):SetProperty("Text", "死亡")
  end
  if redLeaderAlive then
    gUI.CarnivalUI.Red:GetChildByName("Shou_dlab"):SetProperty("Text", "存活")
  else
    gUI.CarnivalUI.Red:GetChildByName("Shou_dlab"):SetProperty("Text", "死亡")
  end
  local strRe1 = _CarnivalUI_TEAM[respawnTeam1] .. _CarnivalUI_RESPAWN_STATUS[respawnState1]
  gUI.CarnivalUI.Bule:GetChildByName("Ju_dlab"):SetProperty("Text", strRe1)
  local strRe2 = _CarnivalUI_TEAM[respawnTeam2] .. _CarnivalUI_RESPAWN_STATUS[respawnState2]
  gUI.CarnivalUI.Red:GetChildByName("Ju_dlab"):SetProperty("Text", strRe2)
end
function _CarnivalUI_UpdateBGResult(mapID, bWin)
  if mapID ~= _CarnivalUI_BGMapID and mapID ~= _CarnivalUI_BGMapID1 then
    return
  end
  local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.BG_pic")
  if bWin == 0 then
    wnd:setEffectFile("efxc_ui_xianshi_shibai")
  else
    wnd:setEffectFile("efxc_ui_xianshi_shengli")
  end
  wnd:SetVisible(true)
  gUI.CarnivalUI.BGTimer:Restart()
end
function _CarnivalUI_Timer()
  local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.BG_pic")
  wnd:SetVisible(false)
  gUI.CarnivalUI.BGTimer:Stop()
end
function _OnCarnivalUI_ShowOsd(lv)
  if not lv then
    return
  end
  _CarnivalUI_Lv = lv
  local strPic = _CarnivalUI_GetLvPic(lv)
  gUI.CarnivalUI.ReviveTitle:SetProperty("Text", strPic)
  gUI.CarnivalUI.RewardOSD:SetVisible(true)
  gUI.CarnivalUI.RewardOSD:GetChildByName("Label1"):SetProperty("Text", strPic)
  _CarnivalUI_OSDTime = 2
  AddTimerEvent(2, "ShowOSD", _CarnivalUI_OSDShow)
end
function _OnCarnivalUI_ShowPoint(show, current_lv, max_lv, obtain_Exp, obtain_Vig, gain_Exp, gain_Vig)
  if show then
    if current_lv == max_lv then
      _CarnivalUI_IsEnd = true
    else
      _CarnivalUI_IsEnd = false
    end
    local strPic = _CarnivalUI_GetLvPic(_CarnivalUI_Lv)
    gUI.CarnivalUI.RewardTitle:SetProperty("Text", strPic)
    gUI.CarnivalUI.Succeed:SetVisible(false)
    gUI.CarnivalUI.ObtainExp:SetProperty("Text", tostring(obtain_Exp))
    gUI.CarnivalUI.ObtainVig:SetProperty("Text", tostring(obtain_Vig))
    gUI.CarnivalUI.ObtainExp:SetVisible(true)
    gUI.CarnivalUI.ObtainVig:SetVisible(true)
    gUI.CarnivalUI.GainExp:SetProperty("Text", tostring(gain_Exp))
    gUI.CarnivalUI.GainVig:SetProperty("Text", tostring(gain_Vig))
    gUI.CarnivalUI.GainExp:SetVisible(false)
    gUI.CarnivalUI.GainVig:SetVisible(false)
    gUI.CarnivalUI.AllExp:SetProperty("Text", tostring(obtain_Exp + gain_Exp))
    gUI.CarnivalUI.AllVig:SetProperty("Text", tostring(obtain_Vig + gain_Vig))
    gUI.CarnivalUI.AllExp:SetVisible(false)
    gUI.CarnivalUI.AllVig:SetVisible(false)
    _CarnivalUI_PointTime = 0
    gUI.CarnivalUI.Keep:SetVisible(true)
    if not _CarnivalUI_IsEnd then
      gUI.CarnivalUI.Remind:SetVisible(true)
      gUI.CarnivalUI.Exit:SetVisible(true)
      gUI.CarnivalUI.Keep:SetLeft(gUI.CarnivalUI.Reward:GetWndRect():get_width() * 0.5 + gUI.CarnivalUI.Keep:GetWndRect():get_width() * 0.5)
      gUI.CarnivalUI.Keep:SetProperty("Text", "继续挑战")
      gUI.CarnivalUI.Keep:AddScriptEvent("wm_mouseclick", "UI_CarnivalUI_GoOn")
      gUI.CarnivalUI.Succeed:SetProperty("BackImage", _CarnivalUI_End[1])
    else
      gUI.CarnivalUI.Remind:SetVisible(false)
      gUI.CarnivalUI.Exit:SetVisible(false)
      gUI.CarnivalUI.Keep:SetLeft(gUI.CarnivalUI.Reward:GetWndRect():get_width() * 0.5 - gUI.CarnivalUI.Keep:GetWndRect():get_width() * 0.5)
      gUI.CarnivalUI.Keep:SetProperty("Text", "结束挑战")
      gUI.CarnivalUI.Keep:AddScriptEvent("wm_mouseclick", "UI_CarnivalUI_Exit")
      gUI.CarnivalUI.Succeed:SetProperty("BackImage", _CarnivalUI_End[2])
    end
    AddTimerEvent(2, "ShowPoint", _CarnivalUI_PointShow)
  else
    gUI.CarnivalUI.Reward:SetVisible(false)
    _CarnivalUI_IsEnd = false
  end
end
function _OnCarnivalUI_ShowSpecial(bshow, binit, bStartTime, hpProgress, currentNum, maxNum, useTime)
  if bshow then
    gUI.CarnivalUI.Special:SetVisible(true)
    if binit then
      _CarnivalUI_MaxMun = maxNum
      gUI.CarnivalUI.Numpbar:SetProperty("Progress", tostring(currentNum / maxNum))
      gUI.CarnivalUI.Numdlab:SetProperty("Text", tostring(currentNum) .. "/" .. tostring(maxNum))
      gUI.CarnivalUI.HPpbar:SetProperty("Progress", tostring(hpProgress / 100))
      gUI.CarnivalUI.HPdlab:SetProperty("Text", tostring(hpProgress) .. "%")
      gUI.CarnivalUI.Time:SetProperty("Text", tostring(Lua_Tip_ConvertTime(_CarnivalUI_Stimer * 1000)))
      return
    end
    if bStartTime and useTime then
      gUI.CarnivalUI.Time:SetProperty("Text", tostring("&" .. tostring(useTime) .. "&"))
    end
    if maxNum and maxNum ~= -1 then
      _CarnivalUI_MaxMun = maxNum
    end
    if currentNum and currentNum ~= -1 then
      gUI.CarnivalUI.Numpbar:SetProperty("Progress", tostring(currentNum / _CarnivalUI_MaxMun))
      gUI.CarnivalUI.Numdlab:SetProperty("Text", tostring(currentNum) .. "/" .. tostring(_CarnivalUI_MaxMun))
    end
    if hpProgress and hpProgress ~= -1 then
      gUI.CarnivalUI.HPpbar:SetProperty("Progress", tostring(hpProgress / 100))
      gUI.CarnivalUI.HPdlab:SetProperty("Text", tostring(hpProgress) .. "%")
    end
  else
    gUI.CarnivalUI.Special:SetVisible(false)
  end
end
function _OnCarnivalUI_TemporarySkillStudy(icon, skillId, enable, category, name)
  if category == 7 then
    gUI.CarnivalUI.Temporary:SetVisible(true)
    gUI.CarnivalUI.Temporary:SetItemGoods(0, icon, -1)
    gUI.CarnivalUI.Temporary:SetItemData(0, skillId)
    gUI.CarnivalUI.Temporary:SetItemEnable(0, enable)
    gUI.CarnivalUI.SkillName:SetProperty("Text", name)
    gUI.CarnivalUI.Temporary:ClearItemCoolTime(0)
  end
end
function _OnCarnivalUI_TemporarySkillRemove(category, skillId)
  if category == 7 then
    local spellid = gUI.CarnivalUI.Temporary:GetItemData(0)
    if spellid == skillid then
      gUI.CarnivalUI.Temporary:ResetAllGoods(true)
      gUI.CarnivalUI.SkillName:SetProperty("Text", "")
    end
  end
end
function _OnCarnivalUI_CoolDownTemporary(skillid, category, duration, rest)
  local spellid = gUI.CarnivalUI.Temporary:GetItemData(0)
  local action_bar = gUI.CarnivalUI.Temporary
  if category == 7 and spellid == skillid then
    if rest == 0 then
      action_bar:ClearItemCoolTime(0)
    else
      action_bar:SetItemCoolTime(0, duration, rest)
    end
  end
end
function _OnCarnivalUI_ShowBattleGround(BattleType, bShow, useTime)
  if tonumber(BattleType) == 1 then
    if bShow then
      gUI.CarnivalUI.Battle:SetVisible(true)
      _CarnivalUI_UpdateBGinfo()
      gUI.CarnivalUI.BattleInfo:SetProperty("Visible", "true")
      gUI.CarnivalUI.BattleTitle:SetProperty("Height", "p" .. tostring(_CarnivalUI_BGHieghtOpen))
      if useTime > 0 then
        gUI.CarnivalUI.BGTime:SetVisible(true)
        gUI.CarnivalUI.BGTime:GetChildByName("Time_dlab"):SetProperty("Text", tostring("&" .. tostring(useTime) .. "&"))
      else
        gUI.CarnivalUI.BGTime:SetVisible(false)
      end
    else
      gUI.CarnivalUI.Battle:SetVisible(false)
    end
  end
end
function UI_CarnivalUI_Relive_Show(time, v1, v2)
  gUI.CarnivalUI.Revive:SetVisible(true)
  gUI.CarnivalUI.ReviveBtn:SetCustomUserData(0, v1)
  if time > 0 then
    local wnd = WindowSys_Instance:GetWindow("Revive.Revive_bg.Down_bg.Time_dlab")
    wnd:SetProperty("Text", "&" .. 100 .. "&")
  end
  local strPic = _CarnivalUI_GetLvPic(_CarnivalUI_Lv)
  gUI.CarnivalUI.ReviveTitle:SetProperty("Text", strPic)
end
function UI_CarnivalUI_Relive_ReliveBtnClick(msg)
  local btn = msg:get_window()
  local revive_type = btn:GetCustomUserData(0)
  Revive(revive_type)
  gUI.CarnivalUI.Revive:SetProperty("Visible", "false")
end
function UI_CarnivalUI_Timeout(msg)
  local revive_type = gUI.CarnivalUI.ReviveBtn:GetCustomUserData(0)
  Revive(revive_type)
  gUI.CarnivalUI.Revive:SetProperty("Visible", "false")
end
function UI_CarnivalUI_GoOn(msg)
  ChooseFloor(1)
  gUI.CarnivalUI.Reward:SetVisible(false)
end
function UI_CarnivalUI_Exit(msg)
  Messagebox_Show("CARNIVAL_EXIT")
end
function UI_CarnivalUI_CastSkill(msg)
  local index = msg:get_lparam()
  local spellid = gUI.CarnivalUI.Temporary:GetItemData(index)
  CastSkill(spellid)
end
function UI_CarnivalUI_ShowSkillTips(msg)
  local index = msg:get_wparam()
  local spellid = gUI.CarnivalUI.Temporary:GetItemData(index)
  local goodsbox = msg:get_window()
  local tooltip = goodsbox:GetToolTipWnd(0)
  local currentlv = GetPlayerSkillLv(spellid)
  Lua_Tip_Skill(tooltip, spellid, currentlv, false, false)
end
function UI_CarnivalUI_BGClick(msg)
  if gUI.CarnivalUI.BattleInfo:IsVisible() then
    gUI.CarnivalUI.BattleInfo:SetProperty("Visible", "false")
    gUI.CarnivalUI.BattleTitle:SetProperty("Height", "p" .. tostring(_CarnivalUI_BGHieghtClose))
  else
    gUI.CarnivalUI.BattleInfo:SetProperty("Visible", "true")
    gUI.CarnivalUI.BattleTitle:SetProperty("Height", "p" .. tostring(_CarnivalUI_BGHieghtOpen))
  end
end
function Lua_CarnivalUI_Timer_Reset()
  gUI.CarnivalUI.Special:SetVisible(false)
end
function Lua_CarnivalUI_CloseSpecial()
  gUI.CarnivalUI.Special:SetVisible(false)
  gUI.CarnivalUI.Temporary:ResetAllGoods(true)
  gUI.CarnivalUI.SkillName:SetProperty("Text", "")
  gUI.CarnivalUI.Reward:SetVisible(false)
end
function Lua_CarnivalUI_CloseBattle()
  gUI.CarnivalUI.Battle:SetVisible(false)
end
function Script_CarnivalUI_OnEvent(event)
  if "INSTANCE_BOARD" == event then
    _OnCarnivalUI_ShowPoint(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif "SHOW_SPECIL_OSD" == event then
    _OnCarnivalUI_ShowOsd(arg1)
  elseif "LEARNTECH_TEMPORARYSTUDY" == event then
    _OnCarnivalUI_TemporarySkillStudy(arg1, arg2, arg3, arg4, arg5)
  elseif "LEARNTECH_TEMPORARYREMOVE" == event then
    _OnCarnivalUI_TemporarySkillRemove(arg1, arg2)
  elseif "INSTANCE_INFO1" == event then
    _OnCarnivalUI_ShowSpecial(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif "TEMPORARY_COOLDOWN" == event then
    _OnCarnivalUI_CoolDownTemporary(arg1, arg2, arg3, arg4)
  elseif "SHOW_BATTLEGROUND" == event then
    _OnCarnivalUI_ShowBattleGround(arg1, arg2, arg3)
  elseif "GUILDCONTEST_UPDATEBGINFO" == event then
    _CarnivalUI_UpdateBGinfo()
  elseif "GUILDCONTEST_RESULT" == event then
    _CarnivalUI_UpdateBGResult(arg1, arg2)
  end
end
function Script_CarnivalUI_OnLoad(event)
  gUI.CarnivalUI.Revive = WindowSys_Instance:GetWindow("Revive")
  gUI.CarnivalUI.ReviveBtn = WindowSys_Instance:GetWindow("Revive.Revive_bg.Down_bg.Keep_btn")
  gUI.CarnivalUI.ReviveTitle = WindowSys_Instance:GetWindow("Revive.Revive_bg.Title_bg.Title_lb")
  gUI.CarnivalUI.RewardOSD = WindowSys_Instance:GetWindow("CarnvialLv_pic")
  gUI.CarnivalUI.Reward = WindowSys_Instance:GetWindow("Reward")
  gUI.CarnivalUI.RewardTitle = WindowSys_Instance:GetWindow("Reward.Reward_bg.Title_bg.Title_lb")
  gUI.CarnivalUI.Succeed = WindowSys_Instance:GetWindow("Reward.Reward_bg.Succeed_bg")
  gUI.CarnivalUI.ObtainExp = WindowSys_Instance:GetWindow("Reward.Reward_bg.Up_bg.ObtainExp_dlab")
  gUI.CarnivalUI.ObtainVig = WindowSys_Instance:GetWindow("Reward.Reward_bg.Up_bg.ObtainVig_dlab")
  gUI.CarnivalUI.GainExp = WindowSys_Instance:GetWindow("Reward.Reward_bg.Up_bg.GainExp_dlab")
  gUI.CarnivalUI.GainVig = WindowSys_Instance:GetWindow("Reward.Reward_bg.Up_bg.GainVig_dlab")
  gUI.CarnivalUI.AllExp = WindowSys_Instance:GetWindow("Reward.Reward_bg.Down_bg.AllExp_dlab")
  gUI.CarnivalUI.AllVig = WindowSys_Instance:GetWindow("Reward.Reward_bg.Down_bg.AllVig_dlab")
  gUI.CarnivalUI.Remind = WindowSys_Instance:GetWindow("Reward.Reward_bg.Down_bg.Remind_slab")
  gUI.CarnivalUI.Keep = WindowSys_Instance:GetWindow("Reward.Reward_bg.Down_bg.Keep_btn")
  gUI.CarnivalUI.Exit = WindowSys_Instance:GetWindow("Reward.Reward_bg.Down_bg.Exit_btn")
  gUI.CarnivalUI.Special = WindowSys_Instance:GetWindow("Special")
  gUI.CarnivalUI.Time = WindowSys_Instance:GetWindow("Special.Special_bg.Top_bg.Insist_dlab")
  gUI.CarnivalUI.Numpbar = WindowToProgressBar(WindowSys_Instance:GetWindow("Special.Special_bg.Center_bg.ProgressBar1"))
  gUI.CarnivalUI.Numdlab = WindowToLabel(WindowSys_Instance:GetWindow("Special.Special_bg.Center_bg.ProgressBar1.Label2_dlab"))
  gUI.CarnivalUI.HPpbar = WindowToProgressBar(WindowSys_Instance:GetWindow("Special.Special_bg.Center_bg.ProgressBar2"))
  gUI.CarnivalUI.HPdlab = WindowToLabel(WindowSys_Instance:GetWindow("Special.Special_bg.Center_bg.ProgressBar2.Label2_dlab"))
  gUI.CarnivalUI.Temporary = WindowToGoodsBox(WindowSys_Instance:GetWindow("Special.Special_bg.Skill_bg.Skill_gbox"))
  gUI.CarnivalUI.SkillName = WindowSys_Instance:GetWindow("Special.Special_bg.Skill_bg.Skill_dlab")
  gUI.CarnivalUI.Temporary:SetItemCoolCenter(true)
  gUI.CarnivalUI.Battle = WindowSys_Instance:GetWindow("GuildContestInf")
  gUI.CarnivalUI.BattleTitle = WindowSys_Instance:GetWindow("GuildContestInf.Inf_bg")
  gUI.CarnivalUI.BattleInfo = WindowSys_Instance:GetWindow("GuildContestInf.Inf_bg.Image_bg")
  gUI.CarnivalUI.Bule = WindowSys_Instance:GetWindow("GuildContestInf.Inf_bg.Image_bg.Left_bg")
  gUI.CarnivalUI.Red = WindowSys_Instance:GetWindow("GuildContestInf.Inf_bg.Image_bg.Right_bg")
  gUI.CarnivalUI.BGTime = WindowSys_Instance:GetWindow("GuildContestInf.Inf_bg.Image_bg.Time_bg")
  gUI.CarnivalUI.BGTimer = TimerSys_Instance:CreateTimerObject("bg_blink", 8, 1, "_CarnivalUI_Timer", false, false)
end

