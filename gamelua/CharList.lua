local SelectedChar = -1
local _CurrSelectGuid = -1
local CharCount = 0
local HaveCreated = false
local TOTALTIMES = 0
local Gender = {
  [1] = "女",
  [2] = "男"
}
function _CharList_Select(index)
  local root = WindowSys_Instance:GetWindow("CharList.Main_bg")
  if SelectedChar ~= index then
    if SelectedChar ~= -1 then
      local wnd = WindowToButton(root:GetChildByName(string.format("Button%d_btn", SelectedChar)))
      wnd:SetStatus("normal")
    end
    SelectedChar = index
  end
  if CharCount > 0 then
    local wnd = WindowToButton(root:GetChildByName(string.format("Button%d_btn", SelectedChar)))
    wnd:SetStatus("selected")
  end
  local show_box = UIInterface:getShowbox("selectShowbox")
  if index > 0 then
    local guid, name, _, gender, pro = GetAccountCharInfo(index - 1)
    if guid then
      if not show_box:isAvatarExist(name) then
        show_box:createSelectedAvatar(index - 1, "", -1)
      end
      show_box:setCurrentAvatar(name)
    end
    local selectShare = gUI_PROFESSION_INTRODUCE.selectShare
    show_box:setAvatarPosition(selectShare.rx, selectShare.ry, selectShare.rz)
    show_box:setAvatarOrientation(selectShare.rotation)
    show_box:setAvatarScale(selectShare.scale)
    show_box:setCameraPos(selectShare.cx, selectShare.cy, selectShare.cz)
    show_box:setCameraDir(selectShare.dx, selectShare.dy, selectShare.dz)
  else
    show_box:clearAllAvatars()
  end
end
function _CharList_Update()
  local root = WindowSys_Instance:GetWindow("CharList")
  local main = root:GetChildByName("Main_bg")
  local count = 0
  for index = 1, 3 do
    local guid, name, level, gender, pro, needChange = GetAccountCharInfo(index - 1)
    local char = main:GetChildByName(string.format("Button%d_btn", index))
    if guid and gUI_MenPaiName[pro] then
      char:GetChildByName("Name_dlab"):SetProperty("Text", name)
      char:GetChildByName("Level_dlab"):SetProperty("Text", tostring(level) .. "级")
      char:GetChildByName("Profession_dlab"):SetProperty("Text", gUI_MenPaiName[pro])
      char:GetChildByName("Create_dlab"):SetVisible(false)
      char:GetChildByName("NeedChange_pic"):SetVisible(needChange)
      count = count + 1
    else
      char:GetChildByName("Name_dlab"):SetProperty("Text", "")
      char:GetChildByName("Level_dlab"):SetProperty("Text", "")
      char:GetChildByName("Profession_dlab"):SetProperty("Text", "")
      char:GetChildByName("Create_dlab"):SetVisible(true)
      char:GetChildByName("NeedChange_pic"):SetVisible(false)
    end
    char:SetEnable(true)
    char = WindowToButton(char)
    char:SetStatus("normal")
  end
  CharCount = count
  if count > 0 then
    if HaveCreated then
      _CharList_Select(count)
    elseif SelectedChar > 0 then
      _CharList_Select(SelectedChar)
    else
      _CharList_Select(1)
    end
    root:SetProperty("Visible", "true")
    local wnd = WindowSys_Instance:GetWindow("QueueAccount")
    if wnd ~= nil and wnd:IsVisible() then
      wnd:SetProperty("Visible", "false")
    end
    WindowSys_Instance:GetWindow("AllStars"):SetVisible(false)
    WindowSys_Instance:GetWindow("CharConfigure"):SetVisible(false)
  else
    root:SetProperty("Visible", "false")
    Lua_ProPreview_AllShow()
  end
  HaveCreated = false
end
function _CharList_SetEnterDisable()
  local wnd = WindowSys_Instance:GetWindow("CharList.Picture1_bg.Enter_btn")
  wnd:SetEnable(false)
  wnd = WindowSys_Instance:GetWindow("CharList.Main_bg.Button1_btn")
  wnd:SetEnable(false)
  wnd = WindowSys_Instance:GetWindow("CharList.Main_bg.Button2_btn")
  wnd:SetEnable(false)
  wnd = WindowSys_Instance:GetWindow("CharList.Main_bg.Button3_btn")
  wnd:SetEnable(false)
  wnd = WindowSys_Instance:GetWindow("CharList.Direction_bg.ZoomOut")
  wnd:SetEnable(false)
  wnd = WindowSys_Instance:GetWindow("CharList.Direction_bg.ZoomIn")
  wnd:SetEnable(false)
end
function Lua_CharList_HaveCreatedNewChar(have)
  HaveCreated = have
end
function Lua_CharList_Return()
  local root = WindowSys_Instance:GetWindow("CharList")
  root:SetVisible(true)
  if SelectedChar ~= -1 then
    _CharList_Select(SelectedChar)
  elseif CharCount > 0 then
    _CharList_Select(1)
  else
    _CharList_Select(-1)
  end
  local wnd = WindowSys_Instance:GetWindow("CharList.Picture1_bg.Enter_btn")
  if CharCount > 0 then
    wnd:SetEnable(true)
  else
    wnd:SetEnable(false)
  end
  local wnd = WindowSys_Instance:GetWindow("CharList.Direction_bg.ZoomOut")
  if CharCount > 0 then
    wnd:SetEnable(true)
  else
    wnd:SetEnable(false)
  end
  local wnd = WindowSys_Instance:GetWindow("CharList.Direction_bg.ZoomIn")
  if CharCount > 0 then
    wnd:SetEnable(true)
  else
    wnd:SetEnable(false)
  end
end
function Lua_CharList_AccountGrp_Succ()
  _OnAccountGrp_Succ()
end
function _OnCharList_OnOpen()
  local account_info = Lua_LoginGrp_GetAccountInfo()
  UIConfig:setAccount(account_info.remember, account_info.name)
  local wnd = WindowSys_Instance:GetWindow("CharList.Picture1_bg.Enter_btn")
  if wnd then
    wnd:SetEnable(true)
  end
  local wnd = WindowSys_Instance:GetWindow("CharList.Direction_bg.ZoomOut")
  if wnd then
    wnd:SetEnable(true)
  end
  local wnd = WindowSys_Instance:GetWindow("CharList.Direction_bg.ZoomIn")
  if wnd then
    wnd:SetEnable(true)
  end
  _CharList_Update()
end
function _OnCharList_SetData(lastIndex, lastLogoutTime, nowTime, guid)
  if _CurrSelectGuid == guid and _CurrSelectGuid ~= -1 then
    return
  end
  _CurrSelectGuid = guid
  SelectedChar = lastIndex + 1
  local lab1 = WindowSys_Instance:GetWindow("CharList.Picture1_bg.Label1_dlab")
  lab1:SetProperty("Text", string.format("上次退出:%s", lastLogoutTime))
  lab2 = WindowSys_Instance:GetWindow("CharList.Picture1_bg.Label2_dlab")
  lab2:SetProperty("Text", string.format("本次登陆:%s", nowTime))
end
function _OnCharList_SatgeChange()
  local wnd = WindowSys_Instance:GetWindow("Messagebox")
  if wnd then
    wnd:SetVisible(false)
  end
  ResetTimer()
end
function _OnCharList_EnterToGame()
  local charList = WindowSys_Instance:GetWindow("CharList")
  if charList:IsVisible() then
    UI_CharList_EnterIntoGame()
  end
end
function UI_CharList_EnterIntoGame(msg)
  if SelectedChar ~= -1 then
    EnterIntoGameWorld(SelectedChar - 1)
    _CharList_SetEnterDisable()
  else
    MessageboxTypes.NOTIFY_MSG.has_button1 = true
    MessageboxTypes.NOTIFY_MSG.text = "您需要先创建角色"
    MessageboxTypes.NOTIFY_MSG.text1 = ""
    MessageboxTypes.NOTIFY_MSG.text2 = ""
    MessageboxTypes.NOTIFY_MSG.text3 = ""
    Messagebox_Show("NOTIFY_MSG")
  end
end
function UI_CharList_BackToLogin(msg)
  local wnd = WindowSys_Instance:GetWindow("QueueAccount")
  if wnd ~= nil then
    wnd:SetProperty("Visible", "false")
  end
  BackToLogin()
end
function UI_CharList_CreateNewChar(msg)
  if CharCount >= 3 then
    MessageboxTypes.NOTIFY_MSG.has_button1 = true
    MessageboxTypes.NOTIFY_MSG.text = "创建的角色数量已满"
    MessageboxTypes.NOTIFY_MSG.text1 = ""
    MessageboxTypes.NOTIFY_MSG.text2 = ""
    MessageboxTypes.NOTIFY_MSG.text3 = ""
    Messagebox_Show("NOTIFY_MSG")
    return
  end
  local root = WindowSys_Instance:GetWindow("CharList")
  root:SetVisible(false)
  local show_box = UIInterface:getShowbox("selectShowbox")
  show_box:setCurrentAvatar("")
  Lua_ProPreview_AllShow()
end
function UI_CharList_Delete(msg)
  DeleteChar(SelectedChar - 1)
end
function UI_RotationBegin(msg)
  local button = msg:get_window()
  local avatar = UIInterface:getShowbox("selectShowbox")
  if button:GetProperty("WindowName") == "RotationL" then
    AddTimerEvent(2, "login_char_rotation", function()
      avatar:onCharacterLeft()
    end)
  else
    AddTimerEvent(2, "login_char_rotation", function()
      avatar:onCharacterRight()
    end)
  end
end
function UI_RotationEnd(msg)
  DelTimerEvent(2, "login_char_rotation")
end
function UI_ZoomBegin(msg)
  local button = msg:get_window()
  local avatar = UIInterface:getShowbox("selectShowbox")
  if button:GetProperty("WindowName") == "ZoomIn" then
    avatar:onCameraZoomIn(true)
  else
    avatar:onCameraZoomOut(true)
  end
end
function UI_ZoomEnd(msg)
  local button = msg:get_window()
  local avatar = UIInterface:getShowbox("selectShowbox")
  if button:GetProperty("WindowName") == "ZoomIn" then
    avatar:onCameraZoomIn(false)
  else
    avatar:onCameraZoomOut(false)
  end
end
function UI_CharList_Select(msg)
  local btn = msg:get_window()
  local index = string.match(btn:GetProperty("WindowName"), "%d")
  if btn:GetChildByName("Create_dlab"):IsVisible() then
    UI_CharList_CreateNewChar()
  else
    _CharList_Select(tonumber(index))
  end
end
function UI_CharList_SelectAndEnter(msg)
  if SelectedChar ~= -1 then
    EnterIntoGameWorld(SelectedChar - 1)
    _CharList_SetEnterDisable()
  end
end
function _OnAccountGrp_LindQueInfo(text)
  local wnd = WindowSys_Instance:GetWindow("QueueAccount")
  if wnd ~= nil then
    wnd:SetVisible(true)
    local queueInfoWnd = wnd:GetChildByName("Quantity")
    queueInfoWnd:SetProperty("Text", tostring(text))
  end
end
function UI_LoadingGrp_QiutQueue1(msg)
  local wnd = WindowSys_Instance:GetWindow("QueueAccount")
  if wnd ~= nil then
    wnd:SetProperty("Visible", "false")
  end
  BackToLogin()
  Disconnect()
end
function _OnAccountGrp_Succ()
  local wnd = WindowSys_Instance:GetWindow("QueueAccount")
  if wnd ~= nil then
    wnd:SetProperty("Visible", "false")
  end
end
function _On_ShowChangeName(arg1, arg2)
  if arg1 == 2 then
    Messagebox_Hide("CHANGENAME")
  else
    local changeWin
    if TOTALTIMES % 2 == 0 then
      changeWin = Messagebox_Show("CHANGENAMENEW", arg1, arg2)
    else
      changeWin = Messagebox_Show("CHANGENAME", arg1, arg2)
    end
    TOTALTIMES = TOTALTIMES + 1
    local changeEdi = WindowToEditBox(changeWin.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
    WindowToEditBox(changeEdi):SetProperty("Text", "")
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowToEditBox(changeEdi))
    WindowSys_Instance:SetFocusWindow(changeEdi)
  end
end
function Script_CharList_OnLoad()
  CharCount = 0
  HaveCreated = false
  SelectedChar = -1
  WorldStage:playMusic("0017.ogg", true)
end
function Script_CharList_OnEvent(event)
  if event == "PLAYER_CHARLIST_ONOPEN" then
    _OnCharList_OnOpen()
  elseif event == "PLAYER_CHARLIST_SETDATA" then
    _OnCharList_SetData(arg1, arg2, arg3, arg4)
  elseif event == "STAGE_CHANGED" then
    _OnCharList_SatgeChange()
  elseif event == "DEL_CHAR_RESULT" then
    SelectedChar = -1
  elseif event == "PLAYER_CHARLIST_FROMGAME" then
    _CharList_SetEnterDisable()
  elseif event == "PLAYER_CHARLIST_ENTERCLICK" then
    _OnCharList_EnterToGame()
  elseif event == "ACCOUNT_LINE_QUEUE_INFO" then
    _OnAccountGrp_LindQueInfo(arg1)
  elseif event == "ACCOUNT_SUCC" then
    _OnAccountGrp_Succ()
  elseif event == "LOGIN_CHANGE_NAME" then
    _On_ShowChangeName(arg1, arg2)
  end
end
