_Chara_MenpaiElement = {
  [0] = 1,
  [1] = 3,
  [2] = 2,
  [3] = 5,
  [4] = 4,
  [255] = 0
}
_Chara_MenpaiElementName = {
  [0] = "火",
  [1] = "雷",
  [2] = "水",
  [3] = "灵",
  [4] = "毒",
  [255] = 0
}
local _Chara_EQUIPSLOT_MAP = {
  [0] = {5, "武器"},
  [1] = {1, "衣服"},
  [2] = {3, "手套"},
  [3] = {8, "手镯"},
  [4] = {2, "肩饰"},
  [5] = {4, "鞋子"},
  [6] = {7, "项链"},
  [7] = {9, "戒指"},
  [8] = {6, "耳饰"},
  [9] = {0, "头盔"},
  [10] = {10, "时装"}
}
local _Chara_fashion_show = 10
local _Chara_EquipGoodsItemNum = 5
if not gUI then
  gUI = {}
end
if not gUI.Chara then
  gUI.Chara = {}
end
if not gUI.Chara.Obj then
  gUI.Chara.Obj = {introduce = -1}
  gUI.Chara.Obj.__index = gUI.Chara.Obj
end
function gUI.Chara.Obj:Init(root_name)
  self.root = WindowSys_Instance:GetWindow(root_name)
  self[1] = self.root:GetChildByName("Left_bg")
  self[2] = self.root:GetChildByName("Right_bg")
  self.name_lab = self[1]:GetGrandChild("CharName_bg.CharName_dlab")
  self.type_lab = self[1]:GetGrandChild("MenPai_bg.MenPai_dlab")
  self.level_lab = self[1]:GetGrandChild("Level_dlab")
  self.Exp_lab = self[1]:GetGrandChild("Exp_pbar.ExpNum_dlab")
  self.EquipPoint_lab = self[1]:GetGrandChild("Choose_bg.EquipValue_dlab")
  self.xiuwei_lab = self[1]:GetGrandChild("Xiuwei_dlab")
  local avatar = self[1]:GetChildByName("Avatar_pic")
  self.left_equip_gb = WindowToGoodsBox(avatar:GetGrandChild("AvatarLeft_bg.EquipLeft_gbox"))
  self.left_equip_gb:ResetAllGoods(true)
  self.right_equip_gb = WindowToGoodsBox(avatar:GetGrandChild("AvatarRight_bg.EquipRight_gbox"))
  self.right_equip_gb:ResetAllGoods(true)
  self.fashion_equip_gb = WindowToGoodsBox(self[1]:GetGrandChild("Fashion_bg.Fashion_gbox"))
  self.fashion_equip_gb:ResetAllGoods(true)
  local showbox = UIInterface:getShowbox(self.showbox_name)
  showbox = showbox or UIInterface:createShowbox(self.showbox_name, avatar)
  self.showbox = showbox
  self.root:GetGrandChild("Title_bg.Close_btn"):SetCustomUserData(0, self.target_type)
  avatar:GetChildByName("RotateLeft_btn"):SetCustomUserData(0, self.target_type)
  avatar:GetChildByName("RotateRight_btn"):SetCustomUserData(0, self.target_type)
end
function gUI.Chara.Obj:StartRendering()
  self.showbox:createAvatar(self.object_id)
  self.showbox:show()
  self.showbox:revertCamera()
  self.showbox:setAvatarScale(0.8)
end
function gUI.Chara.Obj:EndRendering()
  self.showbox:hide()
end
function gUI.Chara.Obj:Hide()
  self.root:SetProperty("Visible", "false")
  self:EndRendering()
end
function gUI.Chara.Obj:Rotate(is_left)
  local showbox = self.showbox
  if is_left then
    AddTimerEvent(2, self.event_name, function()
      showbox:onCharacterLeft()
    end)
  else
    AddTimerEvent(2, self.event_name, function()
      showbox:onCharacterRight()
    end)
  end
end
function gUI.Chara.Obj:StopRotate()
  DelTimerEvent(2, self.event_name)
end
function gUI.Chara.Obj:UpdateEquip(equip_type, equipped)
  if equip_type >= gUI_HUMAN_EQUIP.HEQUIP_NUMBER then
    return
  end
  local offset = _Chara_EQUIPSLOT_MAP[equip_type][1]
  local gb
  if offset < _Chara_EquipGoodsItemNum then
    gb = self.left_equip_gb
  elseif offset < gUI_HUMAN_EQUIP.HEQUIP_NUMBER - 1 then
    gb = self.right_equip_gb
    offset = offset - _Chara_EquipGoodsItemNum
  elseif offset == _Chara_fashion_show then
    gb = self.fashion_equip_gb
    offset = offset - _Chara_fashion_show
  else
    return
  end
  local icon, _, left_time = GetPlayerEquipInfo(self.equip_container, equip_type, self.guid)
  if icon and equipped then
    gb:SetItemGoods(offset, icon, -1)
    gb:SetItemGoods(offset, icon, Lua_Bag_GetQuality(self.equip_container, equip_type))
    local intenLev = Lua_Bag_GetStarInfo(self.equip_container, equip_type)
    if ForgeLevel_To_Stars[intenLev] > 0 then
      gb:SetItemStarState(offset, ForgeLevel_To_Stars[intenLev])
    else
      gb:SetItemStarState(offset, 0)
    end
    gb:SetItemOverdue(offset, left_time <= 0)
  else
    gb:ClearGoodsItem(offset)
  end
  gb:SetItemData(offset, equip_type)
end
function gUI.Chara.Obj:UpdateEquipScore()
  local equipScore = 0
  for i = 0, gUI_HUMAN_EQUIP.HEQUIP_NUMBER - 1 do
    local equipScoreItem = GetEquipScore(self.equip_container, i)
    if equipScoreItem then
      equipScore = equipScore + equipScoreItem
    end
  end
  self.EquipPoint_lab:SetProperty("Text", tostring(equipScore))
end
if not gUI.Chara.MyObj then
  gUI.Chara.MyObj = {
    attrs = {},
    potentials = {
      0,
      0,
      0,
      0,
      0,
      0
    },
    bonus = 0,
    used = 0,
    object_id = -1,
    guid = -1,
    target_type = 0,
    equip_container = 0,
    showbox_name = "SelfAvatar",
    event_name = "mychar_rotation",
    reputation_lists = {
      {
        widget = -1,
        count = 0,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1
      },
      {
        widget = -1,
        count = 0,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1,
        -1
      }
    }
  }
  gUI.Chara.MyObj = setmetatable(gUI.Chara.MyObj, gUI.Chara.Obj)
end
function gUI.Chara.MyObj:Init(root_name)
  gUI.Chara.Obj.Init(self, root_name)
  self.equip_container = gUI_GOODSBOX_BAG.myequipbox
  local avatar = self[1]:GetChildByName("Avatar_pic")
  local name, menpai = GetMyPlayerStaticInfo()
  local _, _, _, _, level, NimbusLevel = GetPlaySelfProp(4)
  if name then
    self.name_lab:SetProperty("Text", name)
    self.type_lab:SetProperty("Text", gUI_MenPaiName[menpai])
    self.level_lab:SetProperty("Text", tostring(level))
    self.xiuwei_lab:SetProperty("Text", tostring(NimbusLevel))
  end
end
function gUI.Chara.MyObj:Show()
  UI_Def_CheckBtnClick()
  UI_Other_CheckBtnClick()
  local root = self.root
  if not root:IsVisible() then
    self:UpdateAll()
    self:StartRendering()
    WorldStage:UpdateValue(root, -1, -1)
    root:SetProperty("Visible", "true")
    local isHideArmet = UIConfig:getHideArmet()
    local isHideFashion = UIConfig:gerHideFashion()
    local CheckAremt = WindowToCheckButton(WindowSys_Instance:GetWindow("Character.Left_bg.Choose_bg.Headwear_cbtn"))
    local CheckFashion = WindowToCheckButton(WindowSys_Instance:GetWindow("Character.Left_bg.Choose_bg.Fashion_cbtn"))
    CheckAremt:SetChecked(isHideArmet)
    CheckFashion:SetChecked(isHideFashion)
  else
    self:EndRendering()
    root:SetProperty("Visible", "false")
  end
end
function DisSetup_CheckHideArmet(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setHideArmet(bState)
end
function DisSetup_CheckHideFashion(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setHideFashion(bState)
end
function gUI.Chara.MyObj:UpdateEquip(equip_type, equipped)
  gUI.Chara.Obj.UpdateEquip(self, equip_type, equipped)
end
function gUI.Chara.MyObj:UpdateAll()
  local maxhp, hp, maxmp, mp, maxrage, rage, strike_point = GetSelfEaseChangeInfo()
  local exp, maxexp, Nimbus, maxNimbus, level, NimbusLevel, pkpt = GetPlaySelfProp(4)
  local menpai = GetPlaySelfProp(5)
  self.level_lab:SetProperty("Text", tostring(level))
  self.Exp_lab:SetProperty("Text", tostring(exp) .. "/" .. tostring(maxexp))
  self:UpdateEquipScore()
  self:UpdateAttBase()
  self:UpdateAttDef()
  self:UpdateOther()
end
function gUI.Chara.MyObj:UpdateAttBase()
  local baseAttrPer = self.root:GetGrandChild("Right_bg.Property_bg.Property_cnt.Base_bg")
  local baseAttr = baseAttrPer:GetChildByName("Up_bg")
  local STRValue, CONValue, INTValue, DEXValue = GetPlaySelfProp(0)
  baseAttr:GetChildByName("STRNum_dlab"):SetProperty("Text", tostring(STRValue))
  baseAttr:GetChildByName("CONNum_dlab"):SetProperty("Text", tostring(CONValue))
  baseAttr:GetChildByName("INTNum_dlab"):SetProperty("Text", tostring(INTValue))
  baseAttr:GetChildByName("DEXNum_dlab"):SetProperty("Text", tostring(DEXValue))
  local r1, r2, r3, r4 = GetCurTargetBaseInfo()
  if STRValue - r1 > 0 then
    baseAttr:GetChildByName("STRNum_dlab"):SetProperty("FontColor", tostring("FF00FF40"))
  else
    baseAttr:GetChildByName("STRNum_dlab"):SetProperty("FontColor", tostring("FFC0C0C0"))
  end
  if CONValue - r2 > 0 then
    baseAttr:GetChildByName("CONNum_dlab"):SetProperty("FontColor", tostring("FF00FF40"))
  else
    baseAttr:GetChildByName("CONNum_dlab"):SetProperty("FontColor", tostring("FFC0C0C0"))
  end
  if INTValue - r3 > 0 then
    baseAttr:GetChildByName("INTNum_dlab"):SetProperty("FontColor", tostring("FF00FF40"))
  else
    baseAttr:GetChildByName("INTNum_dlab"):SetProperty("FontColor", tostring("FFC0C0C0"))
  end
  if DEXValue - r4 > 0 then
    baseAttr:GetChildByName("DEXNum_dlab"):SetProperty("FontColor", tostring("FF00FF40"))
  else
    baseAttr:GetChildByName("DEXNum_dlab"):SetProperty("FontColor", tostring("FFC0C0C0"))
  end
  local baseAttr2 = baseAttrPer:GetChildByName("Center_bg")
  local maxhp, hp, maxmp, mp, maxrage, rage, strike_point = GetSelfEaseChangeInfo()
  local exp, maxexp, Nimbus, maxNimbus, level, NimbusLevel, pkpt = GetPlaySelfProp(4)
  baseAttr2:GetChildByName("HPNum_dlab"):SetProperty("Text", tostring(hp) .. "/" .. tostring(maxhp))
  local menpai = GetPlaySelfProp(5)
  if menpai == gUI_MenPaiID.CK then
    baseAttr2:GetGrandChild("Bar_sLab"):SetProperty("Text", "精力值")
  elseif menpai == gUI_MenPaiID.ZS then
    baseAttr2:GetGrandChild("Bar_sLab"):SetProperty("Text", "怒气值")
  elseif menpai == gUI_MenPaiID.GS then
    baseAttr2:GetGrandChild("Bar_sLab"):SetProperty("Text", "箭枝")
  else
    baseAttr2:GetGrandChild("Bar_sLab"):SetProperty("Text", "法力值")
  end
  baseAttr2:GetChildByName("Bar_dLab"):SetProperty("Text", tostring(mp) .. "/" .. tostring(maxmp))
  baseAttr2:GetChildByName("Nimbus_dlab"):SetProperty("Text", tostring(Nimbus))
  local maxAttack, minAttack, absDmg, def, absImDmg, parry, critical, miss, deadly, tenacity, absor = GetPlaySelfProp(1)
  local ATTR_PARRY_RATE, ATTR_CRITICAL_RATE, ATTR_MISS_RATE, ATTR_DEADLY_RATE = GetPlaySelfProp(8)
  maxAttack = math.floor(maxAttack)
  minAttack = math.floor(minAttack)
  local _, menpai = GetMyPlayerStaticInfo()
  local _, _, _, _, level = GetPlaySelfProp(4)
  local rate1, rate2 = GetSelfPropTipInfo(1010, critical, level, menpai)
  local rate11, rate21 = GetSelfPropTipInfo(1012, deadly, level, menpai)
  local attrCritly = rate1 / 100 + rate2 / 100
  local attrDeadly = rate11 / 100 + rate21 / 100
  local baseAttr3 = baseAttrPer:GetChildByName("Down_bg")
  baseAttr3:GetChildByName("DamagePhyNum_dlab"):SetProperty("Text", tostring(maxAttack) .. "～" .. tostring(minAttack))
  baseAttr3:GetChildByName("ViolentRate_dlab"):SetProperty("Text", tostring(attrCritly) .. "%")
  baseAttr3:GetChildByName("Rate_dlab"):SetProperty("Text", tostring(attrDeadly) .. "%")
  baseAttr3:GetChildByName("Harm_dlab"):SetProperty("Text", tostring(absDmg))
  local element = {}
  element[0] = 0
  local re_element = {}
  re_element[1], re_element[2], re_element[3], re_element[4], re_element[5], element[1], element[2], element[3], element[4], element[5] = GetPlaySelfProp(2)
  local _, menpai = GetMyPlayerStaticInfo()
  local index = _Chara_MenpaiElement[menpai]
  baseAttr3:GetChildByName("Violent_slab"):SetProperty("Text", string.format("%s属性伤害", _Chara_MenpaiElementName[menpai]))
  baseAttr3:GetChildByName("Violent_dlab"):SetProperty("Text", tostring(element[index]))
end
function gUI.Chara.MyObj:UpdateAttDef()
  local baseDefPer = self.root:GetGrandChild("Right_bg.Property_bg.Property_cnt.Defense_bg")
  local maxAttack, minAttack, absDmg, def, absImDmg, parry, critical, miss, deadly, tenacity, absor = GetPlaySelfProp(1)
  local ATTR_PARRY_RATE, ATTR_CRITICAL_RATE, ATTR_MISS_RATE, ATTR_DEADLY_RATE = GetPlaySelfProp(8)
  local re_element = {}
  re_element[1], re_element[2], re_element[3], re_element[4], re_element[5] = GetPlaySelfProp(2)
  local _, menpai = GetMyPlayerStaticInfo()
  local _, _, _, _, level = GetPlaySelfProp(4)
  local rate1, rate2 = GetSelfPropTipInfo(1011, miss, level, menpai)
  local rate11, rate21 = GetSelfPropTipInfo(1009, parry, level, menpai)
  local rate12, rate22 = GetSelfPropTipInfo(1013, tenacity, level, menpai)
  local attrMiss = rate1 / 100 + rate2 / 100
  local attrParry = rate11 / 100 + rate21 / 100
  local attrtenac = rate12 / 100 + rate22 / 100
  baseDefPer:GetChildByName("DEFNum_dlab"):SetProperty("Text", tostring(def))
  baseDefPer:GetChildByName("Label2_dlab"):SetProperty("Text", tostring(attrtenac) .. "%")
  baseDefPer:GetChildByName("Label4_dlab"):SetProperty("Text", tostring(attrMiss) .. "%")
  baseDefPer:GetChildByName("Label6_dlab"):SetProperty("Text", tostring(attrParry) .. "%")
  baseDefPer:GetChildByName("Abs_dlab"):SetProperty("Text", tostring(absor))
  baseDefPer:GetChildByName("AbsImDmgNum_dlab"):SetProperty("Text", tostring(absImDmg))
  baseDefPer:GetChildByName("ResistNum2_dlab"):SetProperty("Text", tostring(re_element[2]))
  baseDefPer:GetChildByName("ResistNum1_dlab"):SetProperty("Text", tostring(re_element[1]))
  baseDefPer:GetChildByName("ResistNum3_dlab"):SetProperty("Text", tostring(re_element[3]))
  baseDefPer:GetChildByName("ResistNum4_dlab"):SetProperty("Text", tostring(re_element[4]))
  baseDefPer:GetChildByName("ResistNum5_dlab"):SetProperty("Text", tostring(re_element[5]))
end
function gUI.Chara.MyObj:UpdateOther()
  local baseOtherPer = self.root:GetGrandChild("Right_bg.Property_bg.Property_cnt.Other_bg")
  local _, _, _, _, _, _, pkpt, LilingPoint, XiulianPoint, HuoliPoint, XiayiPoint, TianZhuPoint, DayKill, TotalKill, Rongyu, BattleWeek = GetPlaySelfProp(4)
  local nianliPt = GetFaeriePsychokinesis()
  local baseOther1 = baseOtherPer:GetChildByName("Up_bg")
  baseOther1:GetChildByName("PK_dlab"):SetProperty("Text", tostring(TianZhuPoint))
  baseOther1:GetChildByName("PKpoint_dlab"):SetProperty("Text", tostring(pkpt))
  baseOther1:GetChildByName("huoliPoint_dlab"):SetProperty("Text", tostring(HuoliPoint))
  baseOther1:GetChildByName("LilingPoint_dlab"):SetProperty("Text", tostring(LilingPoint))
  baseOther1:GetChildByName("XiulianPoint_dlab"):SetProperty("Text", tostring(XiulianPoint))
  baseOther1:GetChildByName("Xiayi_dlab"):SetProperty("Text", tostring(XiayiPoint))
  baseOther1:GetChildByName("Lianli_dlab"):SetProperty("Text", tostring(nianliPt))
  baseOther1:GetChildByName("ShaRen_dlab"):SetProperty("Text", tostring(DayKill))
  baseOther1:GetChildByName("ZongShu_dlab"):SetProperty("Text", tostring(TotalKill))
  baseOther1:GetChildByName("RongYu_dlab"):SetProperty("Text", tostring(Rongyu))
  baseOther1:GetChildByName("BattleWeek_dlab"):SetProperty("Text", tostring(BattleWeek))
  local baseOther2 = baseOtherPer:GetChildByName("Center_bg")
  local baseOther3 = baseOtherPer:GetChildByName("Down_bg")
  local baseOther4 = baseOtherPer:GetChildByName("Other_bg")
  local _, _, _, _, _, _, _, my_guid = GetMyPlayerStaticInfo()
  local hisPoint, dailyPoint, weekPros, hisPros = GetGuildMemberExInfo(my_guid)
  local _, guid_name = GetGuildBaseInfo()
  if dailyPoint ~= nil then
    baseOther2:GetChildByName("GuildName_dlab"):SetProperty("Text", tostring(guid_name))
    baseOther2:GetChildByName("Guildpoint_dlab"):SetProperty("Text", tostring(dailyPoint))
    baseOther2:GetChildByName("GuildHisDona_dlab"):SetProperty("Text", tostring(hisPoint))
    baseOther3:GetChildByName("GuildActive_dlab"):SetProperty("Text", tostring(weekPros))
    baseOther3:GetChildByName("GuildHisActive_dlab"):SetProperty("Text", tostring(hisPros))
  else
    local curPoint, allPoint = GetPlaySelfProp(10)
    baseOther2:GetChildByName("GuildName_dlab"):SetProperty("Text", "未加入帮会")
    baseOther2:GetChildByName("Guildpoint_dlab"):SetProperty("Text", tostring(curPoint))
    baseOther2:GetChildByName("GuildHisDona_dlab"):SetProperty("Text", tostring(allPoint))
    baseOther3:GetChildByName("GuildActive_dlab"):SetProperty("Text", tostring(0))
    baseOther3:GetChildByName("GuildHisActive_dlab"):SetProperty("Text", tostring(0))
  end
  local curTimes, maxTimes = GetBlendTimes()
  baseOther4:GetChildByName("Blend_dlab"):SetProperty("Text", tostring(curTimes))
end
if not gUI.Chara.OthObj then
  gUI.Chara.OthObj = {
    object_id = -1,
    guid = -1,
    target_type = 1,
    showbox_name = "OtherAvatar",
    event_name = "othchar_rotation",
    runelist = {}
  }
  gUI.Chara.OthObj = setmetatable(gUI.Chara.OthObj, gUI.Chara.Obj)
end
function gUI.Chara.OthObj:Init(root_name)
  gUI.Chara.Obj.Init(self, root_name)
end
function gUI.Chara.OthObj:ShowTargetChar(object_id, name, level, menpai, nimbus_level)
  WindowSys_Instance:GetWindow("CharacterOther.Page_tctl"):SetVisible(true)
  self.object_id = object_id
  self.equip_container = gUI_GOODSBOX_BAG.othequipbox
  self.name_lab:SetProperty("Text", name)
  self.type_lab:SetProperty("Text", gUI_MenPaiName[menpai])
  self.level_lab:SetProperty("Text", tostring(level))
  self.xiuwei_lab:SetProperty("Text", tostring(nimbus_level))
  for i = 0, gUI_HUMAN_EQUIP.HEQUIP_WEAPON2 do
    self:UpdateEquip(i, true)
  end
  self:StartRendering()
  self.root:SetProperty("Visible", "true")
  self:UpdateEquipScore()
end
function gUI.Chara.OthObj:ShowRankChar(guid, name, level, menpai)
  WindowSys_Instance:GetWindow("CharacterOther.Page_tctl"):SetVisible(false)
  self:ShowOtherTabCtrl(true)
  self.guid = guid
  self.equip_container = gUI_GOODSBOX_BAG.rankequipbox
  self.name_lab:SetProperty("Text", name)
  self.type_lab:SetProperty("Text", gUI_MenPaiName[menpai])
  self.level_lab:SetProperty("Text", tostring(level))
  self.xiuwei_lab:SetProperty("Text", "--")
  self.showbox:createRankAvatar(guid)
  self.showbox:setAvatarScale(0.8)
  self.showbox:revertCamera()
  self.showbox:show()
  for i = 0, gUI_HUMAN_EQUIP.HEQUIP_WEAPON2 do
    self:UpdateEquip(i, true)
  end
  self.root:SetProperty("Visible", "true")
  self:UpdateEquipScore()
end
function gUI.Chara.OthObj:ShowOtherTabCtrl(bShowEquip)
  if bShowEquip then
    self[1]:SetVisible(true)
    self.root:GetGrandChild("Skill_bg"):SetVisible(false)
  else
    self[1]:SetVisible(false)
    RequestTargetSkillRune(self.object_id)
  end
end
function gUI.Chara.OthObj:ShowTargetCharRune(object_id, pageNum, startNum, rune1, rune2, rune3, rune4, rune5, rune6, rune7, rune8, rune9, rune10)
  if self.object_id ~= object_id then
    return
  else
    if pageNum > 0 then
      self.root:GetGrandChild("Skill_bg.Skill_bg.Next_btn"):SetVisible(true)
    else
      self.root:GetGrandChild("Skill_bg.Skill_bg.Next_btn"):SetVisible(false)
    end
    self.runelist = {}
    self.runelist[1] = {}
    self.runelist[1][1] = rune1
    self.runelist[1][2] = rune2
    self.runelist[1][3] = rune3
    self.runelist[1][4] = rune4
    self.runelist[1][5] = rune5
    self.runelist[2] = {}
    self.runelist[2][1] = rune6
    self.runelist[2][2] = rune7
    self.runelist[2][3] = rune8
    self.runelist[2][4] = rune9
    self.runelist[2][5] = rune10
    self.selecPage = 1
    self.startNum = startNum
    self:UpdateRuneList(self.selecPage, self.startNum)
  end
end
function gUI.Chara.OthObj:UpdateRuneList(page, startNum)
  local root = self.root:GetGrandChild("Skill_bg")
  for index = 1, 5 do
    local partname = "Skill_bg.Symbol" .. index .. "_gbox"
    local wndparent = root:GetGrandChild(partname)
    local wnd = WindowToGoodsBox(wndparent)
    wnd:ResetAllGoods(true)
    local runeId = self.runelist[page][index]
    local icon = GetRuneIcon(runeId)
    if icon ~= "" then
      wnd:SetItemGoods(0, icon, -1)
    end
    if index <= startNum then
      if runeId > 0 then
        wnd:SetCustomUserData(1, runeId)
      else
        wnd:SetCustomUserData(1, 0)
      end
    else
      wnd:SetItemGoods(0, "UiIamge013:Image_BagFrameLock", -1)
      wnd:SetCustomUserData(1, -1)
    end
  end
  root:SetVisible(true)
end
function gUI.Chara.OthObj:RunePageChange(page)
  self:UpdateRuneList(page, self.startNum)
end
function _Chara_ShowPage(index)
  local page = {}
  page[0] = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Base_bg")
  page[1] = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Element_bg")
  page[2] = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Other_bg")
  for i = 0, 2 do
    page[i]:SetVisible(false)
  end
  page[index]:SetVisible(true)
end
function _OnChara_UpdatePlayerInfo()
  if gUI.Chara.MyObj.root:IsVisible() then
    gUI.Chara.MyObj:UpdateAll()
  end
end
function _OnChara_UpdatePlayerEquip(arg1, arg2)
  gUI.Chara.MyObj:UpdateEquip(arg1, arg2)
  Lua_Char_UpdateInfos()
end
function Lua_Char_UpdateInfos()
  if gUI.Amass.Root:IsVisible() then
    Lua_Amass_UpdateData()
  end
end
function _OnChara_UpdateOtherPlayerEquip(arg1, arg2, arg3, arg4, arg5)
  gUI.Chara.OthObj:ShowTargetChar(arg1, arg2, arg3, arg4, arg5)
  local tablectrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("CharacterOther.Page_tctl"))
  if arg3 < 30 then
    tablectrl:SetProperty("ItemCount", "1")
    tablectrl:SetProperty("ItemsText", "装备")
  else
    tablectrl:SetProperty("ItemCount", "2")
    tablectrl:SetProperty("ItemsText", "装备|符文")
  end
  tablectrl:SetSelectedState(0)
  gUI.Chara.OthObj:ShowOtherTabCtrl(true)
end
function _OnChara_UpdateOtherPlayerRune(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13)
  gUI.Chara.OthObj:ShowTargetCharRune(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13)
end
function UI_Chara_RotationBegin(msg)
  local button = msg:get_window()
  local character = button:GetCustomUserData(0) == 0 and gUI.Chara.MyObj or gUI.Chara.OthObj
  character:Rotate(button:GetProperty("WindowName") == "RotateLeft_btn")
end
function UI_Chara_RotationEnd(msg)
  local button = msg:get_window()
  local character = button:GetCustomUserData(0) == 0 and gUI.Chara.MyObj or gUI.Chara.OthObj
  character:StopRotate()
end
function UI_Chara_Close(msg)
  local wnd = msg:get_window()
  local character = wnd:GetCustomUserData(0) == 0 and gUI.Chara.MyObj or gUI.Chara.OthObj
  character:Hide()
end
function UI_Chara_SelfShow(msg)
  gUI.Chara.MyObj:Show()
  WorldStage:playSoundByID(33)
end
function UI_Chara_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Character.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Chara_ChooseInfoType(msg)
  local pageList = WindowToTableCtrl(WindowSys_Instance:GetWindow("Character.Right_bg.Property_tctl"))
  index = pageList:GetSelectItem()
  _Chara_ShowPage(index)
end
function UI_Chara_PlaceEquip(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local from_off, to_off
  local to_type = to_win:GetProperty("BoxType")
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_bag = gUI_GOODSBOX_BAG[to_type]
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, msg:get_wparam())
    to_off = to_win:GetItemData(msg:get_lparam())
    local equipType = GetItemExtraInfoBySlot(from_off, from_bag, 0)
    if equipType ~= to_off then
      if equipType ~= 7 then
        Lua_UI_ShowMessageInfo("装备类型不正确！")
        return
      elseif to_off ~= 8 then
        Lua_UI_ShowMessageInfo("装备类型不正确！")
        return
      end
    end
  elseif Lua_Bag_IsEquipbox(from_type) then
    from_off = to_win:GetItemData(msg:get_wparam())
    to_off = to_win:GetItemData(msg:get_lparam())
  else
    return
  end
  local bUsingHorse, bindtype, hasStallArua, id, skillId, skillParam, isPetItem, strPrompt = GetItemUsingInfo(from_bag, from_off)
  if bindtype == 3 then
    Messagebox_Show("CONFIRM_USE_BIND1", from_bag, from_off, to_bag, to_off)
  else
    MoveItem(from_bag, from_off, to_bag, to_off)
  end
end
function UI_Def_CheckBtnClick(msg)
  local checkBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt.Defense_cbtn"))
  local win = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt.Defense_bg")
  win:setEnableHeightCal(false)
  win:SetProperty("Height", "p0")
  if checkBtn:IsChecked() then
    win:setEnableHeightCal(true)
    win:SetProperty("Height", "p195")
  else
  end
  local winCtn = WindowToContainer((WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt")))
end
function UI_Other_CheckBtnClick(msg)
  local checkBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt.Other_cbtn"))
  local win = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt.Other_bg")
  win:setEnableHeightCal(false)
  win:SetProperty("Height", "p0")
  if checkBtn:IsChecked() then
    win:setEnableHeightCal(true)
    win:SetProperty("Height", "p220")
  else
  end
  local winCtn = WindowToContainer((WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt")))
end
function UI_Chara_UnMountEquip(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local from_type = gb:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_off = -1
  MoveItem(from_bag, from_off, gUI_GOODSBOX_BAG.backpack0, to_off)
end
function UI_Chara_SwapWeapon(msg)
  PlayerSwapWeapon(1)
end
function UI_Chara_ShowEquipTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  local template_id = msg:get_lparam()
  local offset = msg:get_wparam()
  Lua_Tip_EquipSlot(tooltip, _Chara_EQUIPSLOT_MAP[template_id][2])
  local bag = gUI_GOODSBOX_BAG[wnd:GetProperty("BoxType")]
  Lua_Tip_Item(tooltip, template_id, bag, true, nil, nil)
end
function UI_Chara_ShowEquipTipNew(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  local template_id = msg:get_lparam()
  local offset = msg:get_wparam()
  Lua_Tip_EquipSlot(tooltip, _Chara_EQUIPSLOT_MAP[template_id][2])
  local bag = gUI_GOODSBOX_BAG[wnd:GetProperty("BoxType")]
  Lua_Tip_Item(tooltip, template_id, bag, true, nil, nil)
end
function UI_Chara_ShowPropertyTip(msg)
  local wnd = msg:get_window()
  local cnt = WindowToContainer(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt"))
  if not cnt:IsMouseIn() then
    return
  end
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local value = tonumber(wnd:GetProperty("Text"))
  local _, menpai = GetMyPlayerStaticInfo()
  local _, _, _, _, level = GetPlaySelfProp(4)
  Chara_Tip_Common(wndUserdata, value, level, menpai, tooltip)
end
function UI_Chara_ShowPropertyTipNew(msg)
  local wnd = msg:get_window()
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local value = tonumber(wnd:GetProperty("Text"))
  local _, menpai = GetMyPlayerStaticInfo()
  local _, _, _, _, level = GetPlaySelfProp(4)
  Chara_Tip_Common(wndUserdata, value, level, menpai, tooltip)
end
function UI_Chara_OtherTabCtrlClick(msg)
  local tablectrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("CharacterOther.Page_tctl"))
  if tablectrl:GetSelectItem() == 0 then
    gUI.Chara.OthObj:ShowOtherTabCtrl(true)
  elseif tablectrl:GetSelectItem() == 1 then
    gUI.Chara.OthObj:ShowOtherTabCtrl(false)
  end
end
function UI_Chara_OtherRuneChangePage(msg)
  local wnd = msg:get_window()
  local selectPage = wnd:GetCustomUserData(0)
  gUI.Chara.OthObj:RunePageChange(selectPage)
end
function Chara_Tip_Common(wndUserdata, value, level, menpai, tooltip)
  local desc = ""
  local re1, re2 = GetSelfPropTipInfo(wndUserdata, value, level, menpai)
  if 1001 == wndUserdata then
    desc = string.format("修为等级会影响装备对技能使用的效果")
  elseif 1002 == wndUserdata then
    desc = string.format("神将的基础伤害增加%d并且增加神将的最大气血值", re1)
  elseif 1003 == wndUserdata then
    desc = string.format("七杀和飞翎的基础伤害增加%d并且增加暴击率，增加所有职业的闪避率", re1)
  elseif 1004 == wndUserdata then
    desc = string.format("灵霄和沉香的基础伤害增加%d并且增加法力值上限和暴击率", re1)
  elseif 1005 == wndUserdata then
    desc = string.format("生命增加%d点", re1)
  elseif 1006 == wndUserdata then
    desc = string.format("忽视目标的物理和属性防御，直接造成伤害")
  elseif 1056 == wndUserdata then
    desc = string.format("对目标造成%d-%d点伤害。", re1, re2)
  elseif 1007 == wndUserdata then
    desc = string.format("物理伤害减免%.2f%%", re1 / 100)
  elseif 1008 == wndUserdata then
    desc = string.format("直接降低绝对伤害造成的伤害效果")
  elseif 1009 == wndUserdata then
    desc = string.format("有%.2f%%+%.2f%%几率招架", re1 / 100, re2 / 100)
  elseif 1010 == wndUserdata then
    desc = string.format("有%.2f%%+%.2f%%几率造成爆击。", re1 / 100, re2 / 100)
  elseif 1011 == wndUserdata then
    desc = string.format("有%.2f%%+%.2f%%几率闪避", re1 / 100, re2 / 100)
  elseif 1012 == wndUserdata then
    desc = string.format("有%.2f%%+%.2f%%几率出现无视对方所有防御的攻击", re1 / 100, re2 / 100)
  elseif 1013 == wndUserdata then
    desc = string.format("爆击伤害减免%.2f%%+%.2f%%", re1 / 100, re2 / 100)
  elseif 1014 == wndUserdata then
    desc = string.format("技能攻击时附加%d点%s属性伤害", value, _Chara_MenpaiElementName[menpai])
  elseif 1015 == wndUserdata then
    desc = string.format("火属性伤害减免%.2f%%", re1 / 100)
  elseif 1016 == wndUserdata then
    desc = string.format("水属性伤害减免%.2f%%", re1 / 100)
  elseif 1017 == wndUserdata then
    desc = string.format("雷属性伤害减免%.2f%%", re1 / 100)
  elseif 1018 == wndUserdata then
    desc = string.format("毒属性伤害减免%.2f%%", re1 / 100)
  elseif 1019 == wndUserdata then
    desc = string.format("灵属性伤害减免%.2f%%", re1 / 100)
  elseif 1020 == wndUserdata then
    desc = string.format("炎属性伤害减免%.2f%%", re1 / 100)
  elseif 1021 == wndUserdata then
    desc = string.format("冰属性伤害减免%.2f%%", re1 / 100)
  elseif 1022 == wndUserdata then
    desc = string.format("电属性伤害减免%.2f%%", re1 / 100)
  elseif 1023 == wndUserdata then
    desc = string.format("地属性伤害减免%.2f%%", re1 / 100)
  elseif 1024 == wndUserdata then
    desc = string.format("风属性伤害减免%.2f%%", re1 / 100)
  elseif 1025 == wndUserdata then
    desc = string.format("受到敌人攻击时，直接减免伤害")
  elseif 26 == wndUserdata then
    desc = string.format("可以通过异境试练活动获得，精魄值可以用来升级技能。")
  end
  if desc ~= "" then
    Lua_Tip_Property(tooltip, desc)
  end
end
function UI_CharaAttack_Tip(msg)
  local maxAttack, minAttack, absDmg, def, absImDmg, parry, critical, miss, deadly, tenacity, absor = GetPlaySelfProp(1)
  local wnd = msg:get_window()
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  local desc = string.format("对目标造成%d-%d点伤害。", maxAttack, minAttack)
  tooltip:InsertLeftText(tostring(desc), colorUserDefine1, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function UI_Chara_ShowToolTip(msg)
  local win = msg:get_window()
  local cnt = WindowToContainer(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt"))
  if not cnt:IsMouseIn() then
    return
  end
  local tooltip = win:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local ID = tonumber(win:GetProperty("CustomUserData"))
  if ID ~= nil and ID > 0 and ID <= 16 then
    Chara_Common_Tips(ID, tooltip)
  end
end
function UI_Chara_ShowToolTipNew(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local ID = tonumber(win:GetProperty("CustomUserData"))
  if ID ~= nil and ID > 0 and ID <= 16 then
    Chara_Common_Tips(ID, tooltip)
  end
end
function Chara_Common_Tips(ID, tooltip)
  if ID == -1 then
    return
  else
    if gUI_Chara_AttrTip[ID].des1 ~= "" then
    end
    Lua_Tip_ShowInterfaceCommon(tooltip, gUI_Chara_AttrTip[ID].des2, 0, 0, colorUserDefine1, "")
  end
end
function UI_Chara_Rate(msg)
  local win = msg:get_window()
  local tooltip = win:GetToolTipWnd(0)
  tooltip = WindowToTooltip(tooltip)
  local ID = tonumber(win:GetProperty("CustomUserData"))
  Lua_Tip_PropertyNew(tooltip, ID)
end
function UI_Chara_ShowOthEquipTip(msg)
  local slot = msg:get_lparam()
  local goodsbox = WindowToGoodsBox(msg:get_window())
  local tooltip = goodsbox:GetToolTipWnd(0)
  Lua_Tip_EquipSlot(tooltip, _Chara_EQUIPSLOT_MAP[slot][2])
  Lua_Tip_Item(tooltip, slot, gUI.Chara.OthObj.equip_container, nil, gUI.Chara.OthObj.guid, nil)
  Lua_Tip_ShowEquipCompare(goodsbox, gUI.Chara.OthObj.equip_container, slot)
end
function Script_Chara_OnLoad1()
  gUI.Chara.MyObj:Init("Character")
  local cnt = WindowToContainer(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt"))
  cnt:DeleteAll()
  local srcbtndef = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Defense_cbtn")
  local srcbtnoth = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Other_cbtn")
  local srcdef = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Defense_bg")
  local srcBase = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Base_bg")
  local srcoth = WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Other_bg")
  local newItem = cnt:Insert(-1, srcBase)
  newItem:SetProperty("WindowName", "Base_bg")
  local newItem1 = cnt:Insert(-1, srcbtndef)
  newItem1:SetProperty("WindowName", "Defense_cbtn")
  local newItem2 = cnt:Insert(-1, srcdef)
  newItem2:SetProperty("WindowName", "Defense_bg")
  local newItem3 = cnt:Insert(-1, srcbtnoth)
  newItem3:SetProperty("WindowName", "Other_cbtn")
  local newItem4 = cnt:Insert(-1, srcoth)
  newItem4:SetProperty("WindowName", "Other_bg")
  local defCheckBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt.Defense_cbtn"))
  local othCheckBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Character.Right_bg.Property_bg.Property_cnt.Other_cbtn"))
  defCheckBtn:SetChecked(true)
  othCheckBtn:SetChecked(true)
end
function Script_Chara_OnLoad2()
  gUI.Chara.OthObj:Init("CharacterOther")
end
function Script_Chara_OnEvent(event)
  if event == "PLAYER_INFO_CHANGED" then
    _OnChara_UpdatePlayerInfo()
  elseif event == "ITEM_EQUIP_UPDATE" then
    _OnChara_UpdatePlayerEquip(arg1, arg2)
  elseif event == "ITEM_OTHEREQUIP_UPDATE" then
    _OnChara_UpdateOtherPlayerEquip(arg1, arg2, arg3, arg4, arg5)
  elseif event == "PLAYER_LEVEL_CHANGED" then
    _OnChara_UpdatePlayerInfo()
  elseif event == "ITEM_RANKEQUIP_UPDATE" then
    gUI.Chara.OthObj:ShowRankChar(arg1, arg2, arg3, arg4)
  elseif event == "SPELL_OTHER_RUNELIST" then
    _OnChara_UpdateOtherPlayerRune(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13)
  end
end
function Script_Chara_OnHide1()
  gUI.Chara.MyObj:Hide()
end
function Script_Chara_OnHide2()
  gUI.Chara.OthObj:Hide()
end
