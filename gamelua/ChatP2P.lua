if gUI and not gUI.Cp2p then
  gUI.Cp2p = {}
end
function _Cp2p_SendMsg()
  local _, wnd, name = _Cp2p_GetDataByIndex(0)
  local online = true
  if online then
    local context = gUI.Cp2p.EditBox:GetProperty("Text")
    if context ~= "" then
      SendChatMessage(g_CHAT_TYPE.ONEBYONE, context, name)
      gUI.Cp2p.EditBox:SetProperty("Text", "")
    end
  else
    Lua_Chat_AddSysLog("对方不在线")
  end
end
function _Cp2p_GetDataByName(name)
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name == name then
      return i, gUI.Cp2p.ChatList[i].wnd, gUI.Cp2p.ChatList[i].index
    end
  end
  return nil
end
function _Cp2p_GetDataByIndex(index)
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].index == index then
      return i, gUI.Cp2p.ChatList[i].wnd, gUI.Cp2p.ChatList[i].name
    end
  end
  return nil
end
function _Cp2p_InitAllWND()
  local wndTemplate = WindowSys_Instance:GetWindow("ChatP2P.Pic2_bg.Picture1_bg.ChatInfo_dbox")
  local templateRoot = WindowSys_Instance:GetWindow("ChatP2P.Pic2_bg.Picture1_bg")
  if gUI.Cp2p.ChatList == nil then
    gUI.Cp2p.ChatList = {}
    for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
      gUI.Cp2p.ChatList[i] = {
        wnd = nil,
        name = nil,
        index = -1,
        hasNewMsg = false
      }
      gUI.Cp2p.ChatList[i].wnd = WindowToDisplayBox(WindowSys_Instance:CreateWndFromTemplate(wndTemplate, templateRoot))
      gUI.Cp2p.ChatList[i].wnd:SetProperty("Visible", "false")
    end
  else
    for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
      gUI.Cp2p.ChatList[i].wnd:ClearAllContent()
      gUI.Cp2p.ChatList[i].wnd:SetProperty("Visible", "false")
      gUI.Cp2p.ChatList[i].name = nil
      gUI.Cp2p.ChatList[i].index = -1
      gUI.Cp2p.ChatList[i].hasNewMsg = false
    end
  end
end
function _Cp2p_ShowText(isTo, context, tarName, wnd, guid, menpaiID)
  local tData, tTime = GetDateAndTime()
  local colorTo = 4281597951
  local colorFrom = 4294967040
  local colorText = 0
  wnd:InsertBack(" ", colorText, 0, 0)
  if isTo then
    local nameStr = gUI.Cp2p.PlayerName .. " " .. tData .. " " .. tTime
    PushToNewList(tarName, "1" .. nameStr)
    PushToNewList(tarName, "3" .. context)
    wnd:InsertBack(nameStr, colorTo, 0, 0)
    wnd:InsertBack(context, colorText, 0, 0)
  else
    local nameStr = string.format("{PlAyEr^%s^%s^%s^3} %s %s", tarName, guid, gUI_MenPaiColor[menpaiID], tData, tTime)
    PushToNewList(tarName, "2" .. nameStr)
    PushToNewList(tarName, "3" .. context)
    wnd:InsertBack(nameStr, colorFrom, 0, 0)
    wnd:InsertBack(context, colorText, 0, 0)
  end
end
function _Cp2p_ShowChatWND(oldWnd, newWnd)
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    gUI.Cp2p.ChatList[i].wnd:SetProperty("Visible", "false")
  end
  newWnd:MoveToEnd()
  newWnd:UpdateScrollBarConfig()
  newWnd:SetProperty("Visible", "true")
end
function _Cp2p_UpdataCurWND(name)
  local _, oldWnd, oldName = _Cp2p_GetDataByIndex(0)
  local _, newWnd, _ = _Cp2p_GetDataByName(name)
  if oldName ~= name then
    _Cp2p_ShowChatWND(oldWnd, newWnd)
  end
end
function _Cp2p_UpdataIndex(name)
  local id, wnd, index = _Cp2p_GetDataByName(name)
  if wnd == nil then
    return nil
  end
  if gUI.Cp2p.ChatList[id].index == -1 then
    for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
      if gUI.Cp2p.ChatList[i].index ~= -1 then
        gUI.Cp2p.ChatList[i].index = gUI.Cp2p.ChatList[i].index + 1
      end
    end
  else
    for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
      if gUI.Cp2p.ChatList[i].index ~= -1 and gUI.Cp2p.ChatList[i].index < gUI.Cp2p.ChatList[id].index then
        gUI.Cp2p.ChatList[i].index = gUI.Cp2p.ChatList[i].index + 1
      end
    end
  end
  gUI.Cp2p.ChatList[id].index = 0
end
function _Cp2p_ClearLastWnd()
  return _Cp2p_ClearWnd(gUI.Cp2p.MaxChatWnd - 1)
end
function _Cp2p_ClearWnd(n)
  if n >= 0 then
    local id, _, _ = _Cp2p_GetDataByIndex(n)
    if id ~= nil then
      if gUI.Cp2p.ChatList[id].hasNewMsg == true then
        local re = _Cp2p_ClearWnd(n - 1)
        gUI.Cp2p.ChatList[id].index = gUI.Cp2p.ChatList[id].index - 1
        return re
      else
        gUI.Cp2p.ChatList[id].name = nil
        gUI.Cp2p.ChatList[id].index = -1
        gUI.Cp2p.ChatList[id].wnd:ClearAllContent()
        return id
      end
    end
  end
  return nil
end
function _Cp2p_GetWaitingNameList(list)
  local count = 0
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].index == -1 and gUI.Cp2p.ChatList[i].name ~= nil then
      count = count + 1
      list[count - 1] = gUI.Cp2p.ChatList[i].name
    end
  end
  return count
end
function _Cp2p_GetExNameList(list, speList)
  local count = 0
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].index >= 3 and gUI.Cp2p.ChatList[i].name ~= nil then
      count = count + 1
      list[count - 1] = gUI.Cp2p.ChatList[i].name
      speList[count - 1] = gUI.Cp2p.ChatList[i].hasNewMsg
    end
  end
  return count
end
function _Cp2p_IsChating(name)
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name == name and gUI.Cp2p.ChatList[i] ~= -1 then
      return true
    end
  end
  return false
end
function _Cp2p_IsWaiting(name)
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name == name and gUI.Cp2p.ChatList[i] == -1 then
      return true
    end
  end
  return false
end
function _Cp2p_IsInList(name)
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name == name then
      return true
    end
  end
  return false
end
function _Cp2p_UpdataNameBtn()
  for i = 0, 2 do
    local id, wnd, name = _Cp2p_GetDataByIndex(i)
    if id ~= nil then
      gUI.Cp2p.NameBtnList[i]:SetProperty("Visible", "true")
      gUI.Cp2p.NameBtnList[i]:SetProperty("Text", name)
      gUI.Cp2p.NameBtnList[i]:SetProperty("Enable", "true")
    else
      gUI.Cp2p.NameBtnList[i]:SetProperty("Visible", "false")
      gUI.Cp2p.NameBtnList[i]:SetProperty("Text", "")
      gUI.Cp2p.NameBtnList[i]:SetProperty("Enable", "false")
    end
  end
  local id, wnd, name = _Cp2p_GetDataByIndex(3)
  if id ~= nil then
    gUI.Cp2p.MoreBtn:SetProperty("Visible", "true")
    gUI.Cp2p.MoreBtn:SetProperty("Enable", "true")
  else
    gUI.Cp2p.MoreBtn:SetProperty("Visible", "false")
    gUI.Cp2p.MoreBtn:SetProperty("Enable", "false")
  end
end
function _Cp2p_GetFreeWnd()
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name == nil then
      return i
    end
  end
  local re = _Cp2p_ClearLastWnd()
  return re
end
function _Cp2p_AddNewTarget(name)
  local wndId = _Cp2p_GetFreeWnd()
  if wndId ~= nil then
    gUI.Cp2p.ChatList[wndId].name = name
    gUI.Cp2p.ChatList[wndId].index = -1
  end
end
function _Cp2p_Updata1v1Btn()
  local waitingNameList = {}
  local count = _Cp2p_GetWaitingNameList(waitingNameList)
  if count <= 0 then
  else
  end
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name ~= nil then
      gUI.Cp2p.Cp2pBtn:SetProperty("Visible", "true")
      return
    end
  end
  gUI.Cp2p.Cp2pBtn:SetProperty("Visible", "false")
end
function _Cp2p_UpdataBlinkBtn()
  local flag = 0
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].index >= 3 and gUI.Cp2p.ChatList[i].hasNewMsg then
      flag = 1
    end
    if gUI.Cp2p.ChatList[i].index == 1 and gUI.Cp2p.ChatList[i].hasNewMsg then
      gUI.Cp2p.NameBtnList[1]:GetParent():SetProperty("Blink", "true")
    elseif gUI.Cp2p.ChatList[i].index == 1 and not gUI.Cp2p.ChatList[i].hasNewMsg then
      gUI.Cp2p.NameBtnList[1]:GetParent():SetProperty("Blink", "false")
    end
    if gUI.Cp2p.ChatList[i].index == 2 and gUI.Cp2p.ChatList[i].hasNewMsg then
      gUI.Cp2p.NameBtnList[2]:GetParent():SetProperty("Blink", "true")
    elseif gUI.Cp2p.ChatList[i].index == 2 and not gUI.Cp2p.ChatList[i].hasNewMsg then
      gUI.Cp2p.NameBtnList[2]:GetParent():SetProperty("Blink", "false")
    end
  end
  if flag == 0 then
    gUI.Cp2p.MoreBtn:GetParent():SetProperty("Blink", "false")
  else
    gUI.Cp2p.MoreBtn:GetParent():SetProperty("Blink", "true")
  end
end
function _Cp2p_ShowHistory(name, page)
  local colorTo = 4281597951
  local colorFrom = 4294967040
  local colorText = 0
  SaveToHistory(name)
  local s = {}
  s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], s[12], s[13], s[14], s[15], s[16], s[17], s[18], s[19] = ReadHistory(page * gUI.Cp2p.LogPerPage * 2, gUI.Cp2p.LogPerPage * 2, gUI.Cp2p.NameBtnList[0]:GetProperty("Text"))
  gUI.Cp2p.LogDisplayBox:ClearAllContent()
  gUI.Cp2p.LogDisplayBox:InsertBack(" ", 4281597951, 0, 0)
  for i = 0, gUI.Cp2p.LogPerPage * 2 do
    if s[i] ~= nil then
      local flag = tonumber(string.sub(s[i], 1, 1))
      local str = string.sub(s[i], 2, -1)
      if flag == 1 then
        gUI.Cp2p.LogDisplayBox:InsertBack(str, colorTo, 0, 0)
      elseif flag == 2 then
        gUI.Cp2p.LogDisplayBox:InsertBack(str, colorFrom, 0, 0)
      else
        gUI.Cp2p.LogDisplayBox:InsertBack(str, colorText, 0, 0)
      end
    end
  end
  gUI.Cp2p.LogDisplayBox:MoveToEnd()
end
function _Cp2p_ClearOneTarget(id)
  gUI.Cp2p.ChatList[id].wnd:ClearAllContent()
  gUI.Cp2p.ChatList[id].name = nil
  gUI.Cp2p.ChatList[id].index = -1
  gUI.Cp2p.ChatList[id].hasNewMsg = false
end
function _Cp2p_CloseHistoryWnd()
  gUI.Cp2p.LogDisplayBox:ClearAllContent()
  gUI.Cp2p.History:SetProperty("Visible", "false")
end
function _Cp2p_UpdataPageLable()
  local str = tostring(gUI.Cp2p.MaxPage - gUI.Cp2p.CurPage + 1) .. "/" .. tostring(gUI.Cp2p.MaxPage + 1)
  gUI.Cp2p.PageLable:SetProperty("Text", str)
end
function _Cp2p_UpdataPageBtn()
  if gUI.Cp2p.MaxPage == 0 or gUI.Cp2p.MaxPage == -1 then
    gUI.Cp2p.NextBtn:SetProperty("Enable", "false")
    gUI.Cp2p.LastBtn:SetProperty("Enable", "false")
  elseif gUI.Cp2p.CurPage == 0 then
    gUI.Cp2p.NextBtn:SetProperty("Enable", "true")
    gUI.Cp2p.LastBtn:SetProperty("Enable", "false")
  elseif gUI.Cp2p.CurPage == gUI.Cp2p.MaxPage then
    gUI.Cp2p.NextBtn:SetProperty("Enable", "false")
    gUI.Cp2p.LastBtn:SetProperty("Enable", "true")
  else
    gUI.Cp2p.NextBtn:SetProperty("Enable", "true")
    gUI.Cp2p.LastBtn:SetProperty("Enable", "true")
  end
end
function _Cp2p_ResetInputFocus()
  local wnd = WindowSys_Instance:GetWindow("ActionBar")
  WindowSys_Instance:SetKeyboardCaptureWindow(wnd)
end
function _Cp2p_AddTextToInputBox(str)
  local str1 = gUI.Cp2p.EditBox:GetProperty("Text")
  if Lua_Chat_CountItem(str1) >= 3 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_ITEM_LIMITED)
    return
  end
  local count = select(2, string.gsub(str, "%{##%^%d%d%}", "{##%^%d%d%}"))
  if count > 6 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_FACE_LIMITED)
    return
  end
  gUI.Cp2p.EditBox:InsertText(str)
end
function Lua_Cp2p_Show()
  gUI.Cp2p.root:SetProperty("Visible", "true")
  gUI.Cp2p.Cp2pBg:SetVisible(false)
end
function Lua_Cp2p_AddTargetToCur(name)
  if _Cp2p_IsInList(name) then
  else
    _Cp2p_AddNewTarget(name)
  end
  local id, _, _ = _Cp2p_GetDataByName(name)
  if id ~= nil then
    _Cp2p_UpdataCurWND(name)
    _Cp2p_UpdataIndex(name)
    _Cp2p_UpdataNameBtn()
    _Cp2p_Updata1v1Btn()
    _Cp2p_UpdataBlinkBtn()
    gUI.Cp2p.ChatList[id].hasNewMsg = false
    Lua_Cp2p_Show()
  end
  _Cp2p_Updata1v1Btn()
end
function UI_Cp2p_DelLog()
  local name = gUI.Cp2p.NameBtnList[0]:GetProperty("Text")
  DelLog(name)
  UI_Cp2p_OpenLogBtnClick()
end
function UI_Cp2p_EditBoxEnter()
  _Cp2p_SendMsg()
end
function UI_Cp2p_CloseLogWnd()
  _Cp2p_CloseHistoryWnd()
end
function UI_Cp2p_FaceBtnClick(msg)
  Lua_ChatC_ShowMotionWnd(msg:get_window(), gUI.Cp2p.EditBox)
end
function UI_Cp2p_CloseOne()
  local id, wnd, name = _Cp2p_GetDataByIndex(1)
  SaveToHistory(gUI.Cp2p.NameBtnList[0]:GetProperty("Text"))
  if id == nil then
    gUI.Cp2p.root:SetProperty("Visible", "false")
    _Cp2p_ResetInputFocus()
    id = _Cp2p_GetDataByIndex(0)
    _Cp2p_ClearOneTarget(id)
    _Cp2p_Updata1v1Btn()
  else
    Lua_Cp2p_AddTargetToCur(name)
    id, _, _ = _Cp2p_GetDataByIndex(1)
    _Cp2p_ClearOneTarget(id)
    for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
      if 0 < gUI.Cp2p.ChatList[i].index then
        gUI.Cp2p.ChatList[i].index = gUI.Cp2p.ChatList[i].index - 1
      end
    end
    _Cp2p_UpdataNameBtn()
  end
  gUI.Cp2p.EditBox:SetProperty("Text", "")
  _Cp2p_CloseHistoryWnd()
  Lua_CFace_CloseFaceWnd()
end
function UI_Cp2p_ItemDrop(msg)
  local seletedId = 0
  if gUI.Bag.ContainerBag:IsVisible() == false then
    seletedId = 1
  end
  local num, id, quality, name, IdTable, _, _, _, _, _, _, strGuid = GetItemBaseInfoBySlot(seletedId + 1, msg:get_wparam())
  if strGuid then
    local str = "{ItEm^" .. "0^" .. strGuid .. "^" .. tostring(IdTable) .. "^" .. tostring(quality) .. "}"
    _Cp2p_AddTextToInputBox(str)
  end
end
function UI_Cp2p_CloseAll()
  for i = 0, gUI.Cp2p.MaxChatWnd - 1 do
    if gUI.Cp2p.ChatList[i].name ~= nil and gUI.Cp2p.ChatList[i].index == -1 then
    else
      _Cp2p_ClearOneTarget(i)
    end
  end
  gUI.Cp2p.root:SetProperty("Visible", "false")
  _Cp2p_ResetInputFocus()
  _Cp2p_UpdataNameBtn()
  _Cp2p_Updata1v1Btn()
  _Cp2p_CloseHistoryWnd()
  gUI.Cp2p.EditBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
  Lua_CFace_CloseFaceWnd()
end
function UI_Cp2p_OpenLogBtnClick()
  if gUI.Cp2p.History:GetProperty("Visible") == "true" then
    _Cp2p_CloseHistoryWnd()
    return
  end
  local name = gUI.Cp2p.NameBtnList[0]:GetProperty("Text")
  gUI.Cp2p.LogDisplayBox:UpdateScrollBarConfig()
  gUI.Cp2p.CurPage = 0
  _Cp2p_ShowHistory(name, gUI.Cp2p.CurPage)
  local count = GetLogCount(name)
  gUI.Cp2p.MaxPage = math.ceil(count / 2 / gUI.Cp2p.LogPerPage) - 1
  _Cp2p_UpdataPageLable()
  _Cp2p_UpdataPageBtn()
  gUI.Cp2p.History:SetProperty("Visible", "true")
end
function UI_Cp2p_NextBtnClick()
  local name = gUI.Cp2p.NameBtnList[0]:GetProperty("Text")
  gUI.Cp2p.CurPage = gUI.Cp2p.CurPage + 1
  if gUI.Cp2p.CurPage > gUI.Cp2p.MaxPage then
    gUI.Cp2p.CurPage = gUI.Cp2p.MaxPage
  end
  _Cp2p_UpdataPageLable()
  _Cp2p_UpdataPageBtn()
  _Cp2p_ShowHistory(name, gUI.Cp2p.CurPage)
end
function UI_Cp2p_LastBtnClick()
  gUI.Cp2p.CurPage = gUI.Cp2p.CurPage - 1
  if gUI.Cp2p.CurPage < 0 then
    gUI.Cp2p.CurPage = 0
  end
  _Cp2p_UpdataPageLable()
  _Cp2p_UpdataPageBtn()
  _Cp2p_ShowHistory(name, gUI.Cp2p.CurPage)
end
function UI_Cp2p_ChooseExList(msg)
  local i = msg:get_lparam()
  local nameList = {}
  local speList = {}
  local count = _Cp2p_GetExNameList(nameList, speList)
  local name = nameList[i]
  Lua_Cp2p_AddTargetToCur(name)
  _Cp2p_Updata1v1Btn()
  _Cp2p_UpdataBlinkBtn()
  if gUI.Cp2p.History:GetProperty("Visible") == "true" then
    UI_Cp2p_OpenLogBtnClick()
  end
end
function UI_Cp2p_ChooseWaitingWnd(msg)
  local i = msg:get_lparam()
  local waitingNameList = {}
  local count = _Cp2p_GetWaitingNameList(waitingNameList)
  local name = waitingNameList[i]
  Lua_Cp2p_AddTargetToCur(name)
end
function UI_Cp2p_NameBtnClick(msg)
  local wnd = msg:get_window()
  local name = wnd:GetProperty("Text")
  wnd:GetParent():SetProperty("Blink", "false")
  Lua_Cp2p_AddTargetToCur(name)
  if gUI.Cp2p.History:GetProperty("Visible") == "true" then
    UI_Cp2p_OpenLogBtnClick()
  end
end
function UI_Cp2p_MoreBtnClick(msg)
  local colorNormal = "0xFFFFFFFF"
  local colorSpe = "0xFF33FFFF"
  local wnd = msg:get_window()
  local nameList = {}
  local speList = {}
  local count = _Cp2p_GetExNameList(nameList, speList)
  if count > 0 then
    for i = 0, count - 1 do
      local name = nameList[i]
      Lua_Cp2p_AddTargetToCur(name)
      _Cp2p_Updata1v1Btn()
      _Cp2p_UpdataBlinkBtn()
      if gUI.Cp2p.History:GetProperty("Visible") == "true" then
        UI_Cp2p_OpenLogBtnClick()
      end
    end
  end
end
function UI_Cp2p_1v1BtnClick(msg)
  local waitingNameList = {}
  local count = _Cp2p_GetWaitingNameList(waitingNameList)
  local id = _Cp2p_GetDataByIndex(0)
  if id ~= nil then
    Lua_Cp2p_Show()
  end
  for i = 0, count - 1 do
    local name = waitingNameList[i]
    Lua_Cp2p_AddTargetToCur(name)
  end
end
function UI_Cp2p_MinWnd()
  gUI.Cp2p.root:SetProperty("Visible", "false")
  gUI.Cp2p.Cp2pBtn:SetProperty("Visible", "true")
  Lua_Fri_SetFoucsToWorld()
end
function UI_Cp2p_SendBtnClick()
  _Cp2p_SendMsg()
end
function _OnCp2p_GetMsgSelf(context, name)
  if _Cp2p_IsInList(name) then
    local id, wnd, index = _Cp2p_GetDataByName(name)
    if id ~= nil then
      _Cp2p_ShowText(true, context, name, wnd, 0, 0)
    end
  end
end
function _OnCp2p_GetMsg(contend, name, guid, menpaiID)
  if gUI.Cp2p.root:IsVisible() then
    gUI.Cp2p.Cp2pBg:SetProperty("Visible", "false")
  else
    gUI.Cp2p.Cp2pBg:SetProperty("Visible", "true")
  end
  local color = 4281597951
  if _Cp2p_IsInList(name) then
    local id, wnd, index = _Cp2p_GetDataByName(name)
    gUI.Cp2p.ChatList[id].hasNewMsg = true
    if id ~= nil then
      _Cp2p_ShowText(false, contend, name, wnd, guid, menpaiID)
    end
    if gUI.Cp2p.NameBtnList[0]:GetProperty("Text") == name then
    elseif gUI.Cp2p.NameBtnList[1]:GetProperty("Text") == name then
      gUI.Cp2p.NameBtnList[1]:GetParent():SetProperty("Blink", "true")
    elseif gUI.Cp2p.NameBtnList[2]:GetProperty("Text") == name then
      gUI.Cp2p.NameBtnList[2]:GetParent():SetProperty("Blink", "true")
    elseif _Cp2p_IsChating(name) then
      gUI.Cp2p.MoreBtn:GetParent():SetProperty("Blink", "true")
    elseif _Cp2p_IsWaiting(name) then
    end
  else
    _Cp2p_AddNewTarget(name)
    local id, wnd, index = _Cp2p_GetDataByName(name)
    if id ~= nil then
      gUI.Cp2p.ChatList[id].hasNewMsg = true
      _Cp2p_ShowText(false, contend, name, wnd, guid, menpaiID)
    end
  end
  _Cp2p_Updata1v1Btn()
  if AmassGetCurrentAmassState() and UIConfig:getAmassRevert() then
    SendChatMessage(g_CHAT_TYPE.ONEBYONE, "正处于聚灵中..", name)
  end
  WorldStage:playSoundByID(15)
end
function Script_Cp2p_OnEvent(event)
  if "NEW_1V1_CHAT_MSG" == event then
    _OnCp2p_GetMsg(arg1, arg2, arg3, arg4)
  elseif "NEW_1V1_CHAT_MSG_SELF" == event then
    _OnCp2p_GetMsgSelf(arg1, arg2)
  end
end
function Script_Cp2p_OnLoad()
  gUI.Cp2p.MaxChatWnd = 21
  _Cp2p_InitAllWND()
  gUI.Cp2p.root = WindowSys_Instance:GetWindow("ChatP2P")
  gUI.Cp2p.root:SetProperty("Visible", "false")
  gUI.Cp2p.History = WindowSys_Instance:GetWindow("ChatP2P.Pic1_bg")
  gUI.Cp2p.History:SetProperty("Visible", "false")
  local root = WindowSys_Instance:GetWindow("MainMenu")
  gUI.Cp2p.ListMenu = WindowToRightClickMenu(root:GetChildByName("ListMenu"))
  gUI.Cp2p.Cp2pBtn = WindowToButton(WindowSys_Instance:GetWindow("ActionBar_frm.ChatP2p_slab.ChatP2p_btn"))
  gUI.Cp2p.Cp2pBg = WindowToPicture(WindowSys_Instance:GetWindow("ActionBar_frm.ChatP2p_slab.ChatP2p_btn.chatEff_bg"))
  gUI.Cp2p.NameBtnList = {}
  gUI.Cp2p.NameBtnList[0] = WindowToButton(WindowSys_Instance:GetWindow("ChatP2P.Pic3_bg.name1_btn"))
  gUI.Cp2p.NameBtnList[1] = WindowToButton(WindowSys_Instance:GetWindow("ChatP2P.Pic3_bg.name2_bg.name2_btn"))
  gUI.Cp2p.NameBtnList[2] = WindowToButton(WindowSys_Instance:GetWindow("ChatP2P.Pic3_bg.name3_bg.name3_btn"))
  gUI.Cp2p.MoreBtn = WindowToButton(WindowSys_Instance:GetWindow("ChatP2P.Pic3_bg.more_bg.more_btn"))
  gUI.Cp2p.EditBox = WindowToEditBox(WindowSys_Instance:GetWindow("ChatP2P.Pic2_bg.Picture2_bg.EditBox_ebox"))
  gUI.Cp2p.LogDisplayBox = WindowToDisplayBox(WindowSys_Instance:GetWindow("ChatP2P.Pic1_bg.Picture1_bg.InfoLog_dbox"))
  gUI.Cp2p.NextBtn = WindowToButton(WindowSys_Instance:GetWindow("ChatP2P.Pic1_bg.Next_btn"))
  gUI.Cp2p.LastBtn = WindowToButton(WindowSys_Instance:GetWindow("ChatP2P.Pic1_bg.Last_btn"))
  gUI.Cp2p.PlayerName = GetMyPlayerStaticInfo()
  gUI.Cp2p.PageLable = WindowSys_Instance:GetWindow("ChatP2P.Pic1_bg.Picture2_bg.Page_dlab")
  _Cp2p_Updata1v1Btn()
  gUI.Cp2p.CurPage = 0
  gUI.Cp2p.LogPerPage = 10
  gUI.Cp2p.MaxPage = 0
end
age = 0
  gUI.Cp2p.LogPerPage = 10
  gUI.Cp2p.MaxPage = 0
end
