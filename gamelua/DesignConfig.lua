HINT_MAX_PLYAE_LEVEL = 19
INEFFECT_COST = 1000
AUTHMAXNORMAL = 5
AUTHMINNORMAL = 4
AUTHMAXBATTER = 8
AUTHMINBATTER = 7
AUTHMAXEXCELLENCE = 12
AUTHMINEXCELLENCE = 11
AUTHMAXPERF = 16
AUTHMINPERF = 15
AUTHMAXTOP = 20
AUTHMINTOP = 19
G_ALLJOBFOREQUIP = {
  [1] = 450550,
  [2] = 451009,
  [3] = 450660,
  [4] = 450569,
  [5] = 450770,
  [6] = 450679,
  [7] = 450880,
  [8] = 450789,
  [9] = 450990,
  [10] = 450899,
  [11] = 497006,
  [12] = 497007,
  [13] = 497008,
  [14] = 497009,
  [15] = 497010,
  [16] = 497001,
  [17] = 497002,
  [18] = 497003,
  [19] = 497004,
  [20] = 497005
}
ITEM_OPENPANEL_FORMAUL_ID = 369001
ITEM_OPENPANEL_BACKOUT_ID = 369000
CURRENCY_UNIT = {
  CU_NONE = -1,
  CU_BEGIN = 0,
  CU_UNBINDMONEY = 1,
  CU_INGOT = 2,
  CU_GUILD_SCORE = 4,
  CU_REPUTATION = 5,
  CU_TOKEN = 6,
  CU_VIGOR = 7,
  CU_GARDEN = 8,
  CU_LOVEPOINT = 9,
  CU_PKVALUE = 10,
  CU_SPIRIT = 11,
  CU_MORALPOINT = 12,
  CU_GOODBAD = 13,
  CU_SPIRITVALUE = 14,
  CU_NIMBUS = 15,
  CU_ZUANSHI = 16,
  CU_SHUIJING = 17,
  CU_ACTIVE = 18,
  CU_MOB_ILLUSTRATION = 19,
  CU_PSYCHOKINESIS = 20,
  CU_BGHONOR = 21,
}

TokenTypeName = {
  [-1] = {TokenIcon = "", TokenName = ""},
  [0] = {TokenIcon = "", TokenName = "绑金"},
  [1] = {TokenIcon = "", TokenName = "金币"},
  [2] = {
    TokenIcon = "UiBtn001:Image_shop",
    TokenName = "福利点"
  },
  [3] = {TokenIcon = "", TokenName = ""},
  [4] = {
    TokenIcon = "UiBtn001:Image_bgong",
    TokenName = "帮贡点"
  },
  [5] = {
    TokenIcon = "UiBtn001:Image_swang",
    TokenName = "声望点"
  },
  [6] = {
    TokenIcon = "UiBtn001:Image_sdao",
    TokenName = "物品"
  },
  [7] = {TokenIcon = "", TokenName = ""},
  [8] = {TokenIcon = "", TokenName = ""},
  [9] = {TokenIcon = "", TokenName = ""},
  [10] = {TokenIcon = "", TokenName = ""},
  [11] = {TokenIcon = "", TokenName = ""},
  [12] = {TokenIcon = "", TokenName = ""},
  [13] = {
    TokenIcon = "UiBtn001:Image_xyi",
    TokenName = "侠义值"
  },
  [14] = {
    TokenIcon = "UiBtn001:Image_lli",
    TokenName = "灵力值"
  },
  [15] = {
    TokenIcon = "tg_activity01:07",
    TokenName = "精魄值"
  },
  [16] = {TokenIcon = "", TokenName = ""},
  [17] = {TokenIcon = "", TokenName = ""},
  [18] = {TokenIcon = "", TokenName = ""},
  [19] = {TokenIcon = "", TokenName = ""},
  [20] = {TokenIcon = "", TokenName = "念力"},
  [21] = {
	TokenIcon = "UiBtn001:Image_Rong",
	TokenName = "战场荣誉值"
  },
 [22] = {
    TokenIcon = "",
    TokenName = "物品策划类型"
  }
}

GUIDE_TITLE_DES = {
  [30] = "撇嘴 ",
  [31] = "呕吐 ",
  [32] = "我晕 ",
  [33] = "暗送秋波 ",
  [34] = "再见 ",
  [35] = "爱心",
  [36] = "被你打败了 ",
  [37] = "膜拜 ",
  [38] = "路过 ",
  [39] = "暴怒 ",
  [40] = "你来打我啊 ",
  [41] = "伤心 ",
  [42] = "大哭 ",
  [43] = "抠鼻 ",
  [44] = "勾引 ",
  [45] = "色色的 ",
  [46] = "发呆 ",
  [47] = "萌萌哒 ",
  [48] = "吃饭 ",
  [49] = "睡觉 ",
  [50] = "我看好你 ",
  [51] = "就是这么自信 ",
  [52] = "无话可说 ",
  [53] = "你好 ",
  [54] = "画圈圈诅咒你 ",
  [55] = "委屈 ",
  [56] = "什么？ ",
  [57] = "就是你了 ",
  [58] = "钱 ",
  [59] = "敲你头 ",
  [101] = "我来了 ",
  [102] = "害羞 ",
  [103] = "胜利 ",
  [104] = "鼓掌 ",
  [105] = "石化 ",
  [106] = "抱抱 ",
  [107] = "跪求 ",
  [108] = "舔舔 ",
  [109] = "掀桌 ",
  [110] = "无辜 ",
  [111] = "偷看 ",
  [112] = "扭扭 ",
  [113] = "潜水 ",
  [114] = "吃撑了 ",
  [115] = "被吓到了 ",
  [116] = "祷告 ",
  [117] = "汗 ",
  [118] = "倒 ",
  [119] = "跳舞 ",
  [120] = "下班啦 ",
  [121] = "切 ",
  [122] = "酷 ",
  [123] = "没钱 ",
  [124] = "快跑 ",
  [125] = "贱贱的笑 ",
  [126] = "洗澡 ",
  [127] = "WC ",
  [128] = "幸福 ",
  [129] = "玫瑰 ",
  [130] = "乌鸦 ",
  [229] = "仙药被偷走了 ",
  [230] = "仙灵芝成熟了 ",
}