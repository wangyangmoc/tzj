if gUI and not gUI.Exchanges then
  gUI.Exchanges = {}
end
if gUI and not gUI.Detail then
  gUI.Detail = {}
end
local _Detail_Str = {
  "你同意决斗！%d秒后开始！",
  "[%s]同意了你的切磋邀请，%d秒后开始！",
  "你离开了擂台，%d秒后判负！",
  ""
}
local USER_GUIDE_HAD_REWARDS = 44
local USER_GUIDE_HAD_REWARDSID = 22
local ZeroNumber = 0
function Lua_Detail_ShowChildWnd(WndName, visible)
  local target = WindowSys_Instance:GetWindow("Detail." .. WndName)
  if visible then
    local wnd = WindowSys_Instance:GetWindow("Detail")
    local childIndex = 0
    while true do
      local childWnd = wnd:GetChildByIndex(childIndex)
      childIndex = childIndex + 1
      if childWnd == nil or IsUserdataNull(childWnd) then
        break
      end
      if childWnd:IsVisible() then
        childWnd:SetProperty("Visible", "false")
      end
    end
    target:SetProperty("Visible", "true")
  else
    target:SetProperty("Visible", "false")
  end
  return target
end
function _OnDetail_ShowSummonPlayerInputName(visible)
  Lua_Detail_ShowChildWnd("InputName", visible)
end
function _OnDetail_DuelInvitationReceive(arg1, arg2)
  if arg1 then
    Messagebox_Show("DUEL_REQUEST", arg2)
  end
end
function _OnDetail_DuelCountDown(arg1, arg2, arg3)
  local _Duel_NumPic = {
    [1] = "efxc_ui_xianshi_qiecuokaishi",
    [2] = "efxc_ui_xianshi_qiecuokaishi_er",
    [3] = "efxc_ui_xianshi_qiecuokaishi_san"
  }
  local index = arg1
  local countdown = arg2 / 1000
  local wnd = WindowSys_Instance:GetWindow("EfxNoLoop_pic.Duel_pic")
  if countdown > 0 then
    do
      local str = _Detail_Str[index]
      AddTimerEvent(1, "duel_prompt", function()
        local prompt = ""
        if index == 2 or index == 1 then
          wnd:SetVisible(true)
          if countdown > 1 then
            wnd:setEffectFile(_Duel_NumPic[countdown])
          end
        else
          prompt = string.format(str, countdown)
        end
        if prompt ~= "" then
          ShowErrorMessage(prompt)
        end
        countdown = countdown - 1
        if countdown < 1 and (index == 2 or index == 1) then
          wnd:setEffectFile(_Duel_NumPic[1])
        end
        if countdown < 0 then
          DelTimerEvent(1, "duel_prompt")
          wnd:SetVisible(false)
        end
      end)
    end
  else
    wnd:SetVisible(false)
    DelTimerEvent(1, "duel_prompt")
  end
end
function _OnDetail_DuelAutoCanel()
  local wnd = WindowSys_Instance:GetWindow("Detail.Duel")
  if wnd:IsVisible() then
    Lua_Detail_ShowChildWnd("Duel", false)
    WorldStage:CancelDuel()
  end
end
function _OnDetail_Invite_MultiMount(name, objId)
  Messagebox_Show("INVITE_MULTIMOUNT", name, objId)
end
function _OnDetail_MultiMountShowLeaveBtn(show)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Ride_btn")
  wnd:SetVisible(show)
end
function UI_Detail_ReplyInvitation()
  WorldStage:CancelDuel()
  Lua_Detail_ShowChildWnd("Duel", false)
end
function UI_Detail_ReplyInvitation(msg)
  local wnd = msg:get_window()
  if wnd:GetProperty("WindowName") == "Confirm" then
    WorldStage:AcceptDuel()
  else
    WorldStage:CancelDuel()
  end
  Lua_Detail_ShowChildWnd("Duel", false)
end
function UI_Detail_PartnerChangeConfirm(msg)
  local wnd = msg:get_window():GetParent()
  local name = wnd:GetGrandChild("Pic.Edit"):GetProperty("Text")
  if WorldStage:isValidString(name) == 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_INVALID)
    return
  end
  RenamePartner(PartnerType, PartnerIndex, name)
  Lua_UI_Close(msg)
end
function UI_Detail_SummonPlayerOk()
  local edit = WindowSys_Instance:GetWindow("Detail.InputName.Picture2_bg.EditBox1_ebox")
  local strName = edit:GetProperty("Text")
  AskSummonPlayer(strName)
  Lua_Detail_ShowChildWnd("InputName", false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Detail_SummonPlayerCancel()
  AskSummonPlayer("")
  Lua_Detail_ShowChildWnd("InputName", false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Detail_DialogBtn(msg)
  local btn = msg:get_window()
  if btn then
    local SelectIndex = btn:GetCustomUserData(0)
    SetExchangeMsg(SelectIndex)
  end
end
function UI_Detail_OpenExchangePanel()
  local wnd = WindowSys_Instance:GetWindow("Detail.Exchange")
  if not wnd:IsVisible() then
    Lua_Detail_ShowChildWnd("Exchange", true)
    UI_Detail_ExchangeSetDatas()
  end
end
function UI_Detail_ExchangeSetDatas()
  gUI.Exchanges.TextDbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Detail.Exchange.Center_pic.Exchange_dbox"))
  gUI.Exchanges.TextDbox:ClearAllContent()
  local str
  local dataList = ExchangesInfosData()
  if table.maxn(dataList) <= ZeroNumber then
    str = "您没有可领取的奖励！"
    gUI.Exchanges.TextDbox:InsertBack(str, ZeroNumber, ZeroNumber, 0)
    return
  end
  gUI.Exchanges.TextDbox:InsertBack("您可以领取以下奖励！", ZeroNumber, ZeroNumber, 0)
  for i = 1, table.maxn(dataList) do
    str = dataList[i].m_sdec
    index = dataList[i].m_index
    str = string.format("{NpCdLg^UiBtn001:Image_npctalk^%s^%d^%d^%d^%d}", str, UNKOWNNUM, index, BtnTypeDig.Exchanges, 2)
    gUI.Exchanges.TextDbox:InsertBack(str, ZeroNumber, index, 0)
  end
end
function ExchangeTest()
  gUI.Exchanges.TextDbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Detail.Exchange.Center_pic.Exchange_dbox"))
  gUI.Exchanges.TextDbox:ClearAllContent()
  gUI.Exchanges.TextDbox:InsertBack("你今天还有没有兑换是奖品哦！！！", 125125125, ZeroNumber, 0)
  gUI.Exchanges.TextDbox:InsertBack(" ", 10, ZeroNumber + 10, 0)
  local str
  for i = 0, 5 do
    str = string.format("{NpCdLg^UiBtn001:Image_npctalk^%s%d^%d^%d^%d^%d}", "兑换奖品", i + 1, UNKOWNNUM, i + 1, BtnTypeDig.Exchanges, 2)
    gUI.Exchanges.TextDbox:InsertBack(str, 125125100, i + 1, 0)
  end
end
function On_Detail_Close(msg)
  gUI.Exchanges.Root = WindowSys_Instance:GetWindow("Detail.Exchange")
  if gUI.Exchanges.Root then
    gUI.Exchanges.Root:SetProperty("Visible", "false")
  end
end
function _OnDetail_OpenExchangePanel()
  UI_Detail_OpenExchangePanel()
end
function _OnDetail_UpdateExchangeData()
  UI_Detail_ExchangeSetDatas()
end
function _OnDetail_SummonRequst()
end
function Guide_OnGetReward(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  DeleteHint(USER_GUIDE_HAD_REWARDSID)
  GuidePathFinder(1)
end
function _OnMsgShowSummonRequest(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  Messagebox_Show("SUMMON_REQUEST", arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  if SummonIsAttack() == 1 then
    Messagebox_EnableMsgTypeOkBtn("SUMMON_REQUEST", false)
  else
    Messagebox_EnableMsgTypeOkBtn("SUMMON_REQUEST", true)
  end
end
function _OnMsgShowSummonRequestNew(placeName, summonName, Guid, sceneKey, posX, posZ, dir)
  local str = ""
  local times = GetCurrSysTime()
  str = "各位大侠，小弟在" .. "[" .. placeName .. "]" .. "(" .. string.format("%d", (tonumber(posX))) .. "," .. string.format("%d", (tonumber(posZ))) .. ")" .. "陷入了困境，速来救援啊。"
  str = "{HaLpCaLl^" .. str .. "}" .. "{LaBeL^1000^" .. tostring(times) .. "^" .. Guid .. "^" .. sceneKey .. "^" .. tostring(posX) .. "^" .. tostring(posZ) .. "^" .. tostring(dir) .. "} " .. "{NoRmAlLa^1000^" .. tostring(times) .. "}"
  _Chat_AddChatText(g_CHAT_TYPE.GUILD, str, tostring(summonName), "", nil, nil, nil, nil)
end
function Detail_OnEvent(event)
  if event == "DUEL_INVITATION_RECEIVE" then
    _OnDetail_DuelInvitationReceive(arg1, arg2)
  elseif event == "DUEL_COUNTDOWN" then
    _OnDetail_DuelCountDown(arg1, arg2, arg3)
  elseif event == "DUEL_AUTO_CANCLE" then
    _OnDetail_DuelAutoCanel()
  elseif event == "SUMMON_REQUEST" then
    _OnMsgShowSummonRequest(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "SUMMON_CALL" then
    _OnMsgShowSummonRequestNew(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "SUMMON_REQUEST_SCENE" then
    Messagebox_Show("SUMMON_REQUEST_SCENE", arg1, arg2, arg3, arg4)
  elseif event == "SUMMON_INPUTNAME_SHOW" then
    _OnDetail_ShowSummonPlayerInputName(arg1)
  elseif event == "MULTIMOUNT_INVITE" then
    _OnDetail_Invite_MultiMount(arg1, arg2)
  elseif event == "MULTIMOUNT_SHOW_LEAVEBTN" then
    _OnDetail_MultiMountShowLeaveBtn(arg1)
  elseif event == "EXCHANEGS_SHOW_PANEL" then
    _OnDetail_OpenExchangePanel()
  elseif event == "EXCHANGES_UPDATE_DATA" then
    _OnDetail_UpdateExchangeData()
  end
end
function Detail_OnEscape()
  local wnd = WindowSys_Instance:GetWindow("Detail")
  local childIndex = 0
  while true do
    local childWnd = wnd:GetChildByIndex(childIndex)
    childIndex = childIndex + 1
    if childWnd == nil or IsUserdataNull(childWnd) then
      break
    end
    if childWnd:IsVisible() then
      childWnd:SetProperty("Visible", "false")
      if childWnd:GetProperty("WindowName") == "Horn" then
        CancelHorn()
      end
      if childWnd:GetProperty("WindowName") == "InputName" then
        AskSummonPlayer("")
      end
    end
  end
end
nction Detail_OnEscape()
  local wnd = WindowSys_Instance:GetWindow("Detail")
  local childIndex = 0
  while true do
    local childWnd = wnd:GetChildByIndex(childIndex)
    childIndex = childIndex + 1
    if childWnd == nil or IsUserdataNull(childWnd) then
      break
    end
    if childWnd:IsVisible() then
      childWnd:SetProperty("Visible", "false")
      if childWnd:GetProperty("WindowName") == "Horn" then
        CancelHorn()
      end
      if childWnd:GetProperty("WindowName") == "InputName" then
        AskSummonPlayer("")
      end
    end
  end
end
