if gUI and not gUI.DisplayAdjust then
  gUI.DisplayAdjust = {}
end
local defaultCharLightFac = 0.23
local defaultSceneContrast = 1.1
local defaultCharContrast = 1.12
local defaultAmbientImpactFac = 0.5
local defaultFollowLightColorR = 0
local defaultFollowLightColorG = 0
local defaultFollowLightColorB = 0
local defaultFollowLightAtten = 1
local defaultFollowLightPower = 0.01
local defaultShadowDepth = 0.9
local defaultSceneFocalRange = 2600
local defaultSceneFocalNearRange = 8000
local defaultSceneFocalFarRange = 21000
local defaultSceneAdjustFac = 0.9
local defaultCharFocalRange = 2600
local defaultCharFocalNearRange = 8000
local defaultCharFocalFarRange = 21000
local defaultCharAdjustFac = 0.9
local defaultSceneExposal = 0
local defaultSceneToneRed = 0
local defaultSceneToneBlue = 0
local defaultCharExposal = 0
local defaultCharToneRed = 0
local defaultCharToneBlue = 0
local defaultPetNumber = 16
local Configure = {
  [1] = {
    false,
    false,
    false,
    false,
    false,
    false,
    _,
    _,
    _,
    _,
    0,
    1
  },
  [2] = {
    true,
    false,
    true,
    true,
    false,
    true,
    _,
    _,
    _,
    _,
    1,
    1
  },
  [3] = {
    true,
    true,
    true,
    true,
    true,
    true,
    _,
    _,
    _,
    _,
    2,
    3
  }
}
local Configure1 = {
  [1] = {
    true,
    false,
    false,
    _,
    _,
    _,
    _,
    0,
    1
  },
  [2] = {
    false,
    true,
    false,
    _,
    _,
    _,
    _,
    1,
    1
  },
  [3] = {
    false,
    false,
    true,
    _,
    _,
    _,
    _,
    2,
    3
  }
}
local Expert = {
  CheckButtonWidgets = {
    CheckBtn01FullScreenLight = {
      set = function(self, val, effect)
        UIConfig:setShowHDR(val, effect)
      end,
      curr = -1,
      effect = true
    },
    CheckBtn02SceneObjectLight = {
      set = function(self, val)
        SetFlowLightEnable(val)
      end,
      curr = -1
    },
    CheckBtn03EffectSoftEdges = {
      set = function(self, val)
        SetEffectFadeEnable(val)
      end,
      curr = -1
    },
    CheckBtn04ScenePointLight = {
      set = function(self, val)
        SetPointLightEnable(val)
      end,
      curr = -1
    },
    CheckBtn05SceneObjectReflection = {
      set = function(self, val)
        SetReflectionEnable(val)
      end,
      curr = -1
    },
    checkBtn06Play = {
      set = function(self, val)
        SetSceneAnimation(val)
      end,
      curr = -1
    }
  },
  RadioButtonWidgets1 = {
    ckBtn00ShadowLow = {
      set = function(self, val, effect)
        UIConfig:setShadowLevel(val, effect)
      end,
      curr = -1,
      effect = true,
      Name = "ckBtn00ShadowLow"
    },
    ckBtn01ShadowMedium = {
      set = function(self, val, effect)
        UIConfig:setShadowLevel(val, effect)
      end,
      curr = -1,
      effect = true,
      Name = "ckBtn01ShadowMedium"
    },
    ckBtn02ShadowHigh = {
      set = function(self, val, effect)
        UIConfig:setShadowLevel(val, effect)
      end,
      curr = -1,
      effect = true,
      Name = "ckBtn02ShadowHigh"
    }
  },
  RadioButtonWidgets2 = {
    ckBtn01GrassLow = {
      set = function(self, val, effect)
        UIConfig:setGrassDensity(val, effect)
      end,
      curr = -1,
      effect = true,
      Name = "ckBtn01GrassLow"
    },
    ckBtn02GrassMedium = {
      set = function(self, val, effect)
        UIConfig:setGrassDensity(val, effect)
      end,
      curr = -1,
      effect = true,
      Name = "ckBtn02GrassMedium"
    },
    ckBtn03GrassHigh = {
      set = function(self, val, effect)
        UIConfig:setGrassDensity(val, effect)
      end,
      curr = -1,
      effect = true,
      Name = "ckBtn03GrassHigh"
    }
  }
}
function DisplayAdjust_Show()
  gUI.DisplayAdjust.Root:SetProperty("Visible", "true")
  _InitializeAllDofSettings()
  _InitializeAllHdrSettings()
  _InitializeAllContrastSettings()
end
function DisplayAdjust_SelectPage(msg)
  local tc = WindowToTableCtrl(msg:get_window())
  local curr = msg:get_wparam() + 1
  for i, name in ipairs({
    "Dof_bg",
    "HDR_bg",
    "Contrast_bg",
    "PerformanceParameter"
  }) do
    gUI.DisplayAdjust.Root:GetChildByName(name):SetProperty("Visible", tostring(curr == i))
  end
end
function UI_DisplayAdjust_Close(msg)
end
function UI_DisplayAdjust_Cancel(msg)
end
function UI_DisplayAdjust_Confirm(msg)
  gUI.DisplayAdjust.Root:SetProperty("Visible", "false")
  DisplayAdjustSave()
end
function _InitializeAllDofSettings()
  local bEnableDof = GetDofEnable()
  local sceneFocalRange, sceneFocalNearRange, sceneFocalFarRange, sceneAdjustFac, charFocalRange, charFocalNearRange, charFocalFarRange, charAdjustFac = GetDisplayAdjustAllDofSettings()
  gUI.DisplayAdjust.SceneFocalRange:SetProperty("Text", string.format("%4.2f", sceneFocalRange))
  gUI.DisplayAdjust.SceneFocalNearRangeEbox:SetProperty("Text", string.format("%4.2f", sceneFocalNearRange))
  gUI.DisplayAdjust.SceneFocalFarRangeEbox:SetProperty("Text", string.format("%4.2f", sceneFocalFarRange))
  gUI.DisplayAdjust.SceneAdjustFacEbox:SetProperty("Text", string.format("%4.2f", sceneAdjustFac))
  gUI.DisplayAdjust.CharFocalRange:SetProperty("Text", string.format("%4.2f", charFocalRange))
  gUI.DisplayAdjust.CharFocalNearRangeEbox:SetProperty("Text", string.format("%4.2f", charFocalNearRange))
  gUI.DisplayAdjust.CharFocalFarRangeEbox:SetProperty("Text", string.format("%4.2f", charFocalFarRange))
  gUI.DisplayAdjust.CharAdjustFacEbox:SetProperty("Text", string.format("%4.2f", charAdjustFac))
  if not bEnableDof then
    WindowToCheckButton(gUI.DisplayAdjust.EnableDof):SetChecked(false)
    _EnableAllSceneSettingsUnderDof(false)
    _EnableAllCharSettingsUnderDof(false)
  else
    WindowToCheckButton(gUI.DisplayAdjust.EnableDof):SetChecked(true)
    _EnableAllSceneSettingsUnderDof(true)
    _EnableAllCharSettingsUnderDof(true)
  end
end
function _InitializeAllHdrSettings()
  local bEnableHDR = GetHDREnable()
  local sceneExposal, sceneToneRed, sceneToneBlue, charExposal, charToneRed, charToneBlue = GetDisplayAdjustAllHdrSettings()
  gUI.DisplayAdjust.HdrSceneExposalEbox:SetProperty("Text", string.format("%4.2f", sceneExposal))
  gUI.DisplayAdjust.HdrSceneToneRedEbox:SetProperty("Text", string.format("%4.2f", sceneToneRed))
  gUI.DisplayAdjust.HdrSceneToneBlueEbox:SetProperty("Text", string.format("%4.2f", sceneToneBlue))
  gUI.DisplayAdjust.HdrCharExposalEbox:SetProperty("Text", string.format("%4.2f", charExposal))
  gUI.DisplayAdjust.HdrCharToneRedEbox:SetProperty("Text", string.format("%4.2f", charToneRed))
  gUI.DisplayAdjust.HdrCharToneBlueEbox:SetProperty("Text", string.format("%4.2f", charToneBlue))
  if not bEnableHDR then
    WindowToCheckButton(gUI.DisplayAdjust.EnableHdr):SetChecked(false)
    _EnableAllSceneSettingsUnderHdr(false)
    _EnableAllCharSettingsUnderHdr(false)
  else
    WindowToCheckButton(gUI.DisplayAdjust.EnableHdr):SetChecked(true)
    _EnableAllSceneSettingsUnderHdr(true)
    _EnableAllCharSettingsUnderHdr(true)
  end
end
function _InitializeAllContrastSettings()
  local charLightFac, sceneContrastRatio, charContrastRatio, ambientImpactFac, followLightR, followLightG, followLightB, followLightAtten, followLightPower, shadowDepth = GetDisplayAdjustAllContrastSettings()
  gUI.DisplayAdjust.SceneContrastEbox:SetProperty("Text", string.format("%4.2f", sceneContrastRatio))
  gUI.DisplayAdjust.CharContrastEbox:SetProperty("Text", string.format("%4.2f", charContrastRatio))
  gUI.DisplayAdjust.CharLightFacEbox:SetProperty("Text", string.format("%4.2f", charLightFac))
  gUI.DisplayAdjust.AmbientImpactFacEbox:SetProperty("Text", string.format("%4.2f", ambientImpactFac))
  gUI.DisplayAdjust.FollowLightColorREbox:SetProperty("Text", string.format("%4.2f", followLightR))
  gUI.DisplayAdjust.FollowLightColorGEbox:SetProperty("Text", string.format("%4.2f", followLightG))
  gUI.DisplayAdjust.FollowLightColorBEbox:SetProperty("Text", string.format("%4.2f", followLightB))
  gUI.DisplayAdjust.FollowLightAttenEbox:SetProperty("Text", string.format("%4.2f", followLightAtten))
  gUI.DisplayAdjust.FollowLightPowerEbox:SetProperty("Text", string.format("%4.2f", followLightPower))
  gUI.DisplayAdjust.ShadowDepthEbox:SetProperty("Text", string.format("%4.2f", shadowDepth))
  if charLightFac == 0 and charContrastRatio == 1 then
    WindowToCheckButton(gUI.DisplayAdjust.ContrastEnableChar):SetChecked(false)
    _EnableAllCharSettingsUnderContrast(false)
  else
    WindowToCheckButton(gUI.DisplayAdjust.ContrastEnableChar):SetChecked(true)
    _EnableAllCharSettingsUnderContrast(true)
  end
  if sceneContrastRatio == 1 then
    WindowToCheckButton(gUI.DisplayAdjust.ContrastEnableScene):SetChecked(false)
    _EnableAllSceneSettingsUnderContrast(false)
  else
    WindowToCheckButton(gUI.DisplayAdjust.ContrastEnableScene):SetChecked(true)
    _EnableAllSceneSettingsUnderContrast(true)
  end
end
function UI_SceneFocalRange_Change(msg)
  local value = gUI.DisplayAdjust.SceneFocalRange:GetProperty("Text")
  SetSceneFocalRange(tonumber(value))
end
function UI_SceneFocalNearRange_Change(msg)
  local value = gUI.DisplayAdjust.SceneFocalNearRangeEbox:GetProperty("Text")
  SetSceneFocalNearRange(tonumber(value))
end
function UI_SceneFocalFarRange_Change(msg)
  local value = gUI.DisplayAdjust.SceneFocalFarRangeEbox:GetProperty("Text")
  SetSceneFocalFarRange(tonumber(value))
end
function UI_SceneAdjustFac_Change(msg)
  local value = gUI.DisplayAdjust.SceneAdjustFacEbox:GetProperty("Text")
  SetSceneAdjustFac(tonumber(value))
end
function UI_CharFocalRange_Change(msg)
  local value = gUI.DisplayAdjust.CharFocalRange:GetProperty("Text")
  SetCharFocalRange(tonumber(value))
end
function UI_CharFocalNearRange_Change(msg)
  local value = gUI.DisplayAdjust.CharFocalNearRangeEbox:GetProperty("Text")
  SetCharFocalNearRange(tonumber(value))
end
function UI_CharFocalFarRange_Change(msg)
  local value = gUI.DisplayAdjust.CharFocalFarRangeEbox:GetProperty("Text")
  SetCharFocalFarRange(tonumber(value))
end
function UI_CharAdjustFac_Change(msg)
  local value = gUI.DisplayAdjust.CharAdjustFacEbox:GetProperty("Text")
  SetCharAdjustFac(tonumber(value))
end
function UI_Dof_SceneReset(msg)
  gUI.DisplayAdjust.SceneFocalRange:SetProperty("Text", string.format("%4.2f", defaultSceneFocalRange))
  gUI.DisplayAdjust.SceneFocalNearRangeEbox:SetProperty("Text", string.format("%4.2f", defaultSceneFocalNearRange))
  gUI.DisplayAdjust.SceneFocalFarRangeEbox:SetProperty("Text", string.format("%4.2f", defaultSceneFocalFarRange))
  gUI.DisplayAdjust.SceneAdjustFacEbox:SetProperty("Text", string.format("%4.2f", defaultSceneAdjustFac))
  SetSceneFocalRange(defaultSceneFocalRange)
  SetSceneFocalNearRange(defaultSceneFocalNearRange)
  SetSceneFocalFarRange(defaultSceneFocalFarRange)
  SetSceneAdjustFac(defaultSceneAdjustFac)
end
function UI_Dof_CharReset(msg)
  gUI.DisplayAdjust.CharFocalRange:SetProperty("Text", string.format("%4.2f", defaultCharFocalRange))
  gUI.DisplayAdjust.CharFocalNearRangeEbox:SetProperty("Text", string.format("%4.2f", defaultCharFocalNearRange))
  gUI.DisplayAdjust.CharFocalFarRangeEbox:SetProperty("Text", string.format("%4.2f", defaultCharFocalFarRange))
  gUI.DisplayAdjust.CharAdjustFacEbox:SetProperty("Text", string.format("%4.2f", defaultCharAdjustFac))
  SetCharFocalRange(defaultCharFocalRange)
  SetCharFocalNearRange(defaultCharFocalNearRange)
  SetCharFocalFarRange(defaultCharFocalFarRange)
  SetCharAdjustFac(defaultCharAdjustFac)
end
function UI_SceneExposal_Change(msg)
  local value = gUI.DisplayAdjust.HdrSceneExposalEbox:GetProperty("Text")
  SetSceneExposal(tonumber(value))
end
function UI_SceneToneRed_Change(msg)
  local value = gUI.DisplayAdjust.HdrSceneToneRedEbox:GetProperty("Text")
  SetSceneToneRed(tonumber(value))
end
function UI_SceneToneBlue_Change(msg)
  local value = gUI.DisplayAdjust.HdrSceneToneBlueEbox:GetProperty("Text")
  SetSceneToneBlue(tonumber(value))
end
function UI_CharExposal_Change(msg)
  local value = gUI.DisplayAdjust.HdrCharExposalEbox:GetProperty("Text")
  SetCharExposal(tonumber(value))
end
function UI_CharToneRed_Change(msg)
  local value = gUI.DisplayAdjust.HdrCharToneRedEbox:GetProperty("Text")
  SetCharToneRed(tonumber(value))
end
function UI_CharToneBlue_Change(msg)
  local value = gUI.DisplayAdjust.HdrCharToneBlueEbox:GetProperty("Text")
  SetCharToneBlue(tonumber(value))
end
function UI_HDR_SceneReset(msg)
  gUI.DisplayAdjust.HdrSceneExposalEbox:SetProperty("Text", string.format("%4.2f", defaultSceneExposal))
  gUI.DisplayAdjust.HdrSceneToneRedEbox:SetProperty("Text", string.format("%4.2f", defaultSceneToneRed))
  gUI.DisplayAdjust.HdrSceneToneBlueEbox:SetProperty("Text", string.format("%4.2f", defaultSceneToneBlue))
  SetSceneExposal(defaultSceneExposal)
  SetSceneToneRed(defaultSceneToneRed)
  SetSceneToneBlue(defaultSceneToneBlue)
end
function UI_HDR_CharReset(msg)
  gUI.DisplayAdjust.HdrCharExposalEbox:SetProperty("Text", string.format("%4.2f", defaultCharExposal))
  gUI.DisplayAdjust.HdrCharToneRedEbox:SetProperty("Text", string.format("%4.2f", defaultCharToneRed))
  gUI.DisplayAdjust.HdrCharToneBlueEbox:SetProperty("Text", string.format("%4.2f", defaultCharToneBlue))
  SetCharExposal(defaultCharExposal)
  SetCharToneRed(defaultCharToneRed)
  SetCharToneBlue(defaultCharToneBlue)
end
function UI_SceneContrast_Change(msg)
  local value = gUI.DisplayAdjust.SceneContrastEbox:GetProperty("Text")
  SetSceneContrastRatio(tonumber(value))
end
function UI_CharContrast_Change(msg)
  local value = gUI.DisplayAdjust.CharContrastEbox:GetProperty("Text")
  SetCharContrastRatio(tonumber(value))
end
function UI_CharLightFac_Change(msg)
  local value = gUI.DisplayAdjust.CharLightFacEbox:GetProperty("Text")
  SetCharacterLightFactor(tonumber(value))
end
function UI_AmbientImpactFac_Change(msg)
  local value = gUI.DisplayAdjust.AmbientImpactFacEbox:GetProperty("Text")
  SetAmbientImpactFactor(tonumber(value))
end
function UI_FollowLightCol_R_Change(msg)
  local value = gUI.DisplayAdjust.FollowLightColorREbox:GetProperty("Text")
  SetCharacterFollowLightColorR(tonumber(value))
end
function UI_FollowLightCol_G_Change(msg)
  local value = gUI.DisplayAdjust.FollowLightColorGEbox:GetProperty("Text")
  SetCharacterFollowLightColorG(tonumber(value))
end
function UI_FollowLightCol_B_Change(msg)
  local value = gUI.DisplayAdjust.FollowLightColorBEbox:GetProperty("Text")
  SetCharacterFollowLightColorB(tonumber(value))
end
function UI_FollowLightAtten_Change(msg)
  local value = gUI.DisplayAdjust.FollowLightAttenEbox:GetProperty("Text")
  SetCharacterFollowLightAtten(tonumber(value))
end
function UI_FollowLightPower_Change(msg)
  local value = gUI.DisplayAdjust.FollowLightPowerEbox:GetProperty("Text")
  SetCharacterFollowLightPower(tonumber(value))
end
function UI_ShadowDepth_Change(msg)
  local value = gUI.DisplayAdjust.ShadowDepthEbox:GetProperty("Text")
  SetShadowDepth(tonumber(value))
end
function UI_Contrast_SceneReset(msg)
  gUI.DisplayAdjust.SceneContrastEbox:SetProperty("Text", string.format("%4.2f", defaultSceneContrast))
  SetSceneContrastRatio(defaultSceneContrast)
end
function UI_Contrast_CharReset(msg)
  gUI.DisplayAdjust.CharContrastEbox:SetProperty("Text", string.format("%4.2f", defaultCharContrast))
  gUI.DisplayAdjust.CharLightFacEbox:SetProperty("Text", string.format("%4.2f", defaultCharLightFac))
  gUI.DisplayAdjust.AmbientImpactFacEbox:SetProperty("Text", string.format("%4.2f", defaultAmbientImpactFac))
  gUI.DisplayAdjust.FollowLightColorREbox:SetProperty("Text", string.format("%4.2f", defaultFollowLightColorR))
  gUI.DisplayAdjust.FollowLightColorGEbox:SetProperty("Text", string.format("%4.2f", defaultFollowLightColorG))
  gUI.DisplayAdjust.FollowLightColorBEbox:SetProperty("Text", string.format("%4.2f", defaultFollowLightColorB))
  gUI.DisplayAdjust.FollowLightAttenEbox:SetProperty("Text", string.format("%4.2f", defaultFollowLightAtten))
  gUI.DisplayAdjust.FollowLightPowerEbox:SetProperty("Text", string.format("%4.2f", defaultFollowLightPower))
  gUI.DisplayAdjust.ShadowDepthEbox:SetProperty("Text", string.format("%4.2f", defaultShadowDepth))
  SetCharacterLightFactor(defaultCharLightFac)
  SetCharContrastRatio(defaultCharContrast)
  SetAmbientImpactFactor(defaultAmbientImpactFac)
  SetCharacterFollowLightColorR(defaultFollowLightColorR)
  SetCharacterFollowLightColorG(defaultFollowLightColorG)
  SetCharacterFollowLightColorB(defaultFollowLightColorB)
  SetCharacterFollowLightAtten(defaultFollowLightAtten)
  SetCharacterFollowLightPower(defaultFollowLightPower)
  SetShadowDepth(defaultShadowDepth)
end
function _EnableAllSceneSettingsUnderDof(bEnable)
  gUI.DisplayAdjust.SceneFocalRange:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.SceneFocalNearRangeEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.SceneFocalFarRangeEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.SceneAdjustFacEbox:SetProperty("Enable", tostring(bEnable))
end
function _EnableAllCharSettingsUnderDof(bEnable)
  gUI.DisplayAdjust.CharFocalRange:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.CharFocalNearRangeEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.CharFocalFarRangeEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.CharAdjustFacEbox:SetProperty("Enable", tostring(bEnable))
end
function _EnableAllSceneSettingsUnderHdr(bEnable)
  gUI.DisplayAdjust.HdrSceneExposalEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.HdrSceneToneRedEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.HdrSceneToneBlueEbox:SetProperty("Enable", tostring(bEnable))
end
function _EnableAllCharSettingsUnderHdr(bEnable)
  gUI.DisplayAdjust.HdrCharExposalEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.HdrCharToneRedEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.HdrCharToneBlueEbox:SetProperty("Enable", tostring(bEnable))
end
function _EnableAllSceneSettingsUnderContrast(bEnable)
  gUI.DisplayAdjust.SceneContrastEbox:SetProperty("Enable", tostring(bEnable))
end
function _EnableAllCharSettingsUnderContrast(bEnable)
  gUI.DisplayAdjust.CharLightFacEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.CharContrastEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.AmbientImpactFacEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.FollowLightColorREbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.FollowLightColorGEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.FollowLightColorBEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.FollowLightAttenEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.FollowLightPowerEbox:SetProperty("Enable", tostring(bEnable))
  gUI.DisplayAdjust.ShadowDepthEbox:SetProperty("Enable", tostring(bEnable))
end
function UI_EnableDof_Checked(msg)
  local wnd = WindowToCheckButton(gUI.DisplayAdjust.EnableDof)
  if wnd:IsChecked() then
    EnableDof(true)
    local value
    value = gUI.DisplayAdjust.SceneFocalRange:GetProperty("Text")
    SetSceneFocalRange(tonumber(value))
    value = gUI.DisplayAdjust.SceneFocalNearRangeEbox:GetProperty("Text")
    SetSceneFocalNearRange(tonumber(value))
    value = gUI.DisplayAdjust.SceneFocalFarRangeEbox:GetProperty("Text")
    SetSceneFocalFarRange(tonumber(value))
    value = gUI.DisplayAdjust.SceneAdjustFacEbox:GetProperty("Text")
    SetSceneAdjustFac(tonumber(value))
    _EnableAllSceneSettingsUnderDof(true)
    value = gUI.DisplayAdjust.CharFocalRange:GetProperty("Text")
    SetCharFocalRange(tonumber(value))
    value = gUI.DisplayAdjust.CharFocalNearRangeEbox:GetProperty("Text")
    SetCharFocalNearRange(tonumber(value))
    value = gUI.DisplayAdjust.CharFocalFarRangeEbox:GetProperty("Text")
    SetCharFocalFarRange(tonumber(value))
    value = gUI.DisplayAdjust.CharAdjustFacEbox:GetProperty("Text")
    SetCharAdjustFac(tonumber(value))
    _EnableAllCharSettingsUnderDof(true)
  else
    EnableDof(false)
    SetSceneFocalRange(defaultSceneFocalRange)
    SetSceneFocalNearRange(defaultSceneFocalNearRange)
    SetSceneFocalFarRange(defaultSceneFocalFarRange)
    SetSceneAdjustFac(defaultSceneAdjustFac)
    _EnableAllSceneSettingsUnderDof(false)
    SetCharFocalRange(defaultCharFocalRange)
    SetCharFocalNearRange(defaultCharFocalNearRange)
    SetCharFocalFarRange(defaultCharFocalFarRange)
    SetCharAdjustFac(defaultCharAdjustFac)
    _EnableAllCharSettingsUnderDof(false)
  end
end
function UI_EnableHDR_Checked(msg)
  local wnd = WindowToCheckButton(gUI.DisplayAdjust.EnableHdr)
  if wnd:IsChecked() then
    EnableHDR(true)
    local value
    value = gUI.DisplayAdjust.HdrSceneExposalEbox:GetProperty("Text")
    SetSceneExposal(tonumber(value))
    value = gUI.DisplayAdjust.HdrSceneToneRedEbox:GetProperty("Text")
    SetSceneToneRed(tonumber(value))
    value = gUI.DisplayAdjust.HdrSceneToneBlueEbox:GetProperty("Text")
    SetSceneToneBlue(tonumber(value))
    _EnableAllSceneSettingsUnderHdr(true)
    value = gUI.DisplayAdjust.HdrCharExposalEbox:GetProperty("Text")
    SetCharExposal(tonumber(value))
    value = gUI.DisplayAdjust.HdrCharToneRedEbox:GetProperty("Text")
    SetCharToneRed(tonumber(value))
    value = gUI.DisplayAdjust.HdrCharToneBlueEbox:GetProperty("Text")
    SetCharToneBlue(tonumber(value))
    _EnableAllCharSettingsUnderHdr(true)
  else
    EnableHDR(false)
    SetSceneExposal(defaultSceneExposal)
    SetSceneToneRed(defaultSceneToneRed)
    SetSceneToneBlue(defaultSceneToneBlue)
    _EnableAllSceneSettingsUnderHdr(false)
    SetCharExposal(defaultCharExposal)
    SetCharToneRed(defaultCharToneRed)
    SetCharToneBlue(defaultCharToneBlue)
    _EnableAllCharSettingsUnderHdr(false)
  end
end
function UI_Contrast_EnableScene_Checked(msg)
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  if wnd:IsChecked() then
    local value
    value = gUI.DisplayAdjust.SceneContrastEbox:GetProperty("Text")
    SetSceneContrastRatio(tonumber(value))
    _EnableAllSceneSettingsUnderContrast(true)
  else
    SetSceneContrastRatio(1)
    _EnableAllSceneSettingsUnderContrast(false)
  end
end
function UI_Contrast_EnableChar_Checked(msg)
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  if wnd:IsChecked() then
    local value
    value = gUI.DisplayAdjust.CharLightFacEbox:GetProperty("Text")
    SetCharacterLightFactor(tonumber(value))
    value = gUI.DisplayAdjust.CharContrastEbox:GetProperty("Text")
    SetCharContrastRatio(tonumber(value))
    value = gUI.DisplayAdjust.AmbientImpactFacEbox:GetProperty("Text")
    SetAmbientImpactFactor(tonumber(value))
    value = gUI.DisplayAdjust.FollowLightColorREbox:GetProperty("Text")
    SetCharacterFollowLightColorR(tonumber(value))
    value = gUI.DisplayAdjust.FollowLightColorGEbox:GetProperty("Text")
    SetCharacterFollowLightColorG(tonumber(value))
    value = gUI.DisplayAdjust.FollowLightColorBEbox:GetProperty("Text")
    SetCharacterFollowLightColorB(tonumber(value))
    value = gUI.DisplayAdjust.FollowLightAttenEbox:GetProperty("Text")
    SetCharacterFollowLightAtten(tonumber(value))
    value = gUI.DisplayAdjust.FollowLightPowerEbox:GetProperty("Text")
    SetCharacterFollowLightPower(tonumber(value))
    value = gUI.DisplayAdjust.ShadowDepthEbox:GetProperty("Text")
    SetShadowDepth(tonumber(value))
    _EnableAllCharSettingsUnderContrast(true)
  else
    SetCharacterLightFactor(0)
    SetCharContrastRatio(1)
    SetAmbientImpactFactor(0.5)
    SetCharacterFollowLightColorR(0)
    SetCharacterFollowLightColorG(0)
    SetCharacterFollowLightColorB(0)
    SetCharacterFollowLightAtten(1)
    SetCharacterFollowLightPower(0.01)
    SetShadowDepth(0.9)
    _EnableAllCharSettingsUnderContrast(false)
  end
end
function UI_PetLodNumEdit_Change()
  local value = gUI.DisplayAdjust.performanceParametersEdit:GetProperty("Text")
  if value == "" then
    gUI.DisplayAdjust.performanceParametersEdit:SetProperty("Text", "16")
    SetPetLodNum(defaultPetNumber)
  else
    gUI.DisplayAdjust.performanceParametersEdit:SetProperty("Text", value)
    SetPetLodNum(tonumber(value))
  end
end
function UI_SceneTotalNum_Change()
  local value = gUI.DisplayAdjust.editSceneToatalNum:GetProperty("Text")
  if value == "" then
    gUI.DisplayAdjust.editSceneToatalNum:SetProperty("Text", "50")
    SetSceneEffectTotalNum(50)
  else
    gUI.DisplayAdjust.editSceneToatalNum:SetProperty("Text", value)
    SetSceneEffectTotalNum(tonumber(value))
  end
end
function UI_effecTotalNum_Change()
  local value = gUI.DisplayAdjust.editEffectToatalNum:GetProperty("Text")
  if value == "" then
    gUI.DisplayAdjust.editEffectToatalNum:SetProperty("Text", "100")
    SetEquipEffectTotalNum(100)
  else
    gUI.DisplayAdjust.editEffectToatalNum:SetProperty("Text", value)
    SetEquipEffectTotalNum(tonumber(value))
  end
end
function ebxShadowNumChange(msg)
  local value = gUI.DisplayAdjust.editShadowNumber:GetProperty("Text")
  if value == "" then
    gUI.DisplayAdjust.editShadowNumber:SetProperty("Text", "16")
    SetShadowLodNum(16)
  else
    gUI.DisplayAdjust.editShadowNumber:SetProperty("Text", value)
    SetShadowLodNum(tonumber(value))
  end
end
function _UpdateRadioButton2(button, checked)
  button = WindowToCheckButton(button)
  button:SetCheckedState(checked)
end
function DefaultSettings()
  gUI.DisplayAdjust.checkBtnadvancedSet:SetCheckedState(false)
  gUI.DisplayAdjust.checkBtncommonSet:SetCheckedState(false)
  gUI.DisplayAdjust.checkBtnmediumSet:SetCheckedState(false)
end
function SettingsHighRadioBtn(msg)
  local wnd = WindowToCheckButton(msg:get_window())
  local parent = wnd:GetParent()
  local cur_name = wnd:GetProperty("WindowName")
  for key, name in pairs({
    [1] = "CommonSettings",
    [2] = "MediumSettings",
    [3] = "AdvancedSettings"
  }) do
    if name ~= cur_name then
      local button = parent:GetChildByName(name)
      _UpdateRadioButton2(button, false)
    else
      gUI.DisplayAdjust.CurrLevel = key
      local button = parent:GetChildByName(name)
      _UpdateRadioButton2(button, true)
    end
  end
  DisplayAdjust_UpdateConfigure(gUI.DisplayAdjust.CurrLevel)
end
function RoleShadowSetRadioBtn(msg)
  DefaultSettings()
  local wnd = WindowToCheckButton(msg:get_window())
  local parent = wnd:GetParent()
  local cur_name = wnd:GetProperty("WindowName")
  for key, object in pairs(Expert.RadioButtonWidgets1) do
    local index = tonumber(string.sub(key, 6, 7))
    if object.Name ~= cur_name then
      local button = parent:GetChildByName(object.Name)
      _UpdateRadioButton2(button, false)
    else
      gUI.DisplayAdjust.CurrLevel = index
      local num = gUI.DisplayAdjust.CurrLevel
      local button = parent:GetChildByName(object.Name)
      _UpdateRadioButton2(button, true)
      object:set(tonumber(gUI.DisplayAdjust.CurrLevel), true)
    end
  end
end
function GrassRadioBtn(msg)
  DefaultSettings()
  local wnd = WindowToCheckButton(msg:get_window())
  local parent = wnd:GetParent()
  local cur_name = wnd:GetProperty("WindowName")
  for key, object in pairs(Expert.RadioButtonWidgets2) do
    local index = tonumber(string.sub(key, 6, 7))
    if object.Name ~= cur_name then
      local button = parent:GetChildByName(object.Name)
      _UpdateRadioButton2(button, false)
    else
      gUI.DisplayAdjust.CurrLevel = index
      local num = gUI.DisplayAdjust.CurrLevel
      local button = parent:GetChildByName(object.Name)
      _UpdateRadioButton2(button, true)
      object:set(tonumber(gUI.DisplayAdjust.CurrLevel), true)
    end
  end
end
function CheckBtnAllSettings(msg)
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  local arg = wnd:IsChecked()
  local name = wnd:GetProperty("WindowName")
  local widget = Expert.CheckButtonWidgets[name]
  widget.curr = arg
  widget:set(arg, true)
  DefaultSettings()
end
function DisplayAdjust_UpdateConfigure(level)
  local parent = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.AdvancedsettingsPic")
  local conf = Configure[level]
  local conf1 = Configure1[level]
  for name, widget in pairs(Expert.CheckButtonWidgets) do
    local index = tonumber(string.sub(name, 9, 10))
    if widget.curr ~= conf[index] then
      widget.curr = conf[index]
      local button = parent:GetChildByName(name)
      if widget.curr == true or widget.curr == 1 then
        _UpdateRadioButton2(button, true)
      else
        _UpdateRadioButton2(button, false)
      end
      widget:set(widget.curr, true)
    end
  end
  for name, widget in pairs(Expert.RadioButtonWidgets1) do
    local index = tonumber(string.sub(name, 6, 7))
    if widget.curr ~= conf1[index + 1] then
      widget.curr = conf1[index + 1]
      local button = parent:GetChildByName(name)
      if widget.curr == true or widget.curr == 1 then
        _UpdateRadioButton2(button, true)
      else
        _UpdateRadioButton2(button, false)
      end
      widget:set(level - 1, true)
    end
  end
  for name, widget in pairs(Expert.RadioButtonWidgets2) do
    local index = tonumber(string.sub(name, 6, 7))
    if widget.curr ~= conf1[index] then
      widget.curr = conf1[index]
      local button = parent:GetChildByName(name)
      if widget.curr == true or widget.curr == 1 then
        _UpdateRadioButton2(button, true)
      else
        _UpdateRadioButton2(button, false)
      end
      widget:set(level, true)
    end
  end
end
function DisplayAdjust_OnLoad()
  gUI.DisplayAdjust.Root = WindowSys_Instance:GetWindow("DisplayAdjust")
  gUI.DisplayAdjust.EnableDof = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.EnableDof_cbtn")
  gUI.DisplayAdjust.SceneFocalRange = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.SceneFocalRange_ebox")
  gUI.DisplayAdjust.SceneFocalNearRangeEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.SceneFocalNearRange_ebox")
  gUI.DisplayAdjust.SceneFocalFarRangeEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.SceneFocalFarRange_ebox")
  gUI.DisplayAdjust.SceneAdjustFacEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.SceneAdjustFac_ebox")
  gUI.DisplayAdjust.CharFocalRange = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.CharFocalRange_ebox")
  gUI.DisplayAdjust.CharFocalNearRangeEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.CharFocalNearRange_ebox")
  gUI.DisplayAdjust.CharFocalFarRangeEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.CharFocalFarRange_ebox")
  gUI.DisplayAdjust.CharAdjustFacEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Dof_bg.CharAdjustFac_ebox")
  gUI.DisplayAdjust.EnableHdr = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.EnableHDR_cbtn")
  gUI.DisplayAdjust.HdrSceneExposalEbox = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.SceneExposal_ebox")
  gUI.DisplayAdjust.HdrSceneToneRedEbox = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.SceneToneRed_ebox")
  gUI.DisplayAdjust.HdrSceneToneBlueEbox = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.SceneToneBlue_ebox")
  gUI.DisplayAdjust.HdrCharExposalEbox = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.CharExposal_ebox")
  gUI.DisplayAdjust.HdrCharToneRedEbox = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.CharToneRed_ebox")
  gUI.DisplayAdjust.HdrCharToneBlueEbox = WindowSys_Instance:GetWindow("DisplayAdjust.HDR_bg.CharToneBlue_ebox")
  gUI.DisplayAdjust.ContrastEnableChar = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.EnableChar_cbtn")
  gUI.DisplayAdjust.ContrastEnableScene = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.EnableScene_cbtn")
  gUI.DisplayAdjust.SceneContrastEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.SceneContrast_ebox")
  gUI.DisplayAdjust.CharContrastEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.CharContrast_ebox")
  gUI.DisplayAdjust.CharLightFacEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.CharLightFac_ebox")
  gUI.DisplayAdjust.AmbientImpactFacEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.AmbientImpactFac_ebox")
  gUI.DisplayAdjust.FollowLightColorREbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.FollowLightColR_ebox")
  gUI.DisplayAdjust.FollowLightColorGEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.FollowLightColG_ebox")
  gUI.DisplayAdjust.FollowLightColorBEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.FollowLightColB_ebox")
  gUI.DisplayAdjust.FollowLightAttenEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.FollowLightAtten_ebox")
  gUI.DisplayAdjust.FollowLightPowerEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.FollowLightPower_ebox")
  gUI.DisplayAdjust.ShadowDepthEbox = WindowSys_Instance:GetWindow("DisplayAdjust.Contrast_bg.ShadowDepth_ebox")
  gUI.DisplayAdjust.performanceParametersEdit = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.ebxNumber")
  gUI.DisplayAdjust.performanceParametersLab = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.labNum")
  gUI.DisplayAdjust.checkSceneAnimation = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.AdvancedsettingsPic.checkBtnPlay")
  gUI.DisplayAdjust.editSceneToatalNum = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.SceneToatalNum")
  gUI.DisplayAdjust.editEffectToatalNum = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.EquipTotalNum")
  gUI.DisplayAdjust.checkBtnFullScreen = WindowToCheckButton(WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.AdvancedsettingsPic.CheckBtn01FullScreenLight"))
  gUI.DisplayAdjust.editShadowNumber = WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.ebxShadowNum")
  gUI.DisplayAdjust.checkBtnadvancedSet = WindowToCheckButton(WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.AdvancedsettingsPic.AdvancedSettings"))
  gUI.DisplayAdjust.checkBtncommonSet = WindowToCheckButton(WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.AdvancedsettingsPic.CommonSettings"))
  gUI.DisplayAdjust.checkBtnmediumSet = WindowToCheckButton(WindowSys_Instance:GetWindow("DisplayAdjust.PerformanceParameter.AdvancedsettingsPic.MediumSettings"))
end
