if gUI and not gUI.DisSetup then
  gUI.DisSetup = {}
end
local _DisVideo = {
  ComboWidgets = {
    Option1_cbox = {
      set = function(self, val, effect)
        UIConfig:setDispResolution(val, effect)
      end,
      get = function(self, val, effect)
        return UIConfig:getDispResolution()
      end,
      effect = false,
      curr = -1,
      prev = -1,
      default = function(self)
        return GetDisplayModes() - 1
      end
    },
    Option3_cbox = {
      set = function(self, val, effect)
        UIConfig:setDispFrequency(val, effect)
      end,
      get = function(self)
        return UIConfig:getDispFrequency()
      end,
      effect = false,
      curr = -1,
      prev = -1,
      default = 0
    }
  },
  RadioWidgets = {
    Radio1_cbtn = {
      set = function(self, val, effect)
        UIConfig:setDispFullScreen(val, effect)
      end,
      get = function(self)
        return UIConfig:getDispFullScreen()
      end,
      effect = false,
      curr = -1,
      prev = -1,
      default = false
    },
    Radio2_cbtn = {
      set = function(self, val, effect)
        UIConfig:setDispWindow(val, effect)
      end,
      get = function(self)
        return UIConfig:getDispWindow()
      end,
      effect = false,
      curr = -1,
      prev = -1,
      default = true
    }
  },
  SliderWidgets = {
    UISize_sld = {
      set = function(self, val, effect)
        UIConfig:setSizeAdaptBase(val, effect)
      end,
      get = function(self)
        return UIConfig:getSizeAdaptBase()
      end,
      effect = false,
      curr = -1,
      prev = -1,
      default = 0.75,
      offset = 0,
      scale = 1
    },
    Dist1_sld = {
      set = function(self, val, effect)
        UIConfig:setViewDist(val, effect)
      end,
      get = function(self)
        return UIConfig:getViewDist()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = 120,
      offset = 80,
      scale = 100
    },
    Bright_sld = {
      set = function(self, val, effect)
        UIConfig:setCustomBrightness(val, effect)
      end,
      get = function(self)
        return UIConfig:getCustomBrightness()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = 0,
      offset = -0.5,
      scale = 1
    },
    Number_sld = {
      set = function(self, val, effect)
        UIConfig:setViewPlayerCount(val, effect)
      end,
      get = function(self)
        return UIConfig:getViewPlayerCount()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = 45,
      offset = 0,
      scale = 1
    }
  }
}
local _DisExpert2 = {
  RadioWidgets1 = {
    CheckBtn01_cbtn = {
      set = function(self, val, effect)
        UIConfig:setShowEquipEff(val, effect)
      end,
      get = function(self)
        return UIConfig:getShowEquipEff()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn02_cbtn = {
      set = function(self, val, effect)
        UIConfig:setWaterLevel(val, effect)
      end,
      get = function(self)
        return UIConfig:getWaterLevel()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn03_cbtn = {
      set = function(self, val, effect)
        UIConfig:setShowSceneEffect(val, effect)
      end,
      get = function(self)
        return UIConfig:getShowSceneEffect()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn04_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnableFSAA(val, effect)
      end,
      get = function(self)
        return UIConfig:getEnableFSAA()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn05_cbtn = {
      set = function(self, val, effect)
        UIConfig:setRealShadow(val, effect)
      end,
      get = function(self)
        return UIConfig:getRealShadow()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn06_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnableDOF(val, effect)
      end,
      get = function(self)
        return UIConfig:getEnableDOF()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn07_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnableSSAO(val, effect)
      end,
      get = function(self)
        return UIConfig:getEnableSSAO()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn08_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnableContrast(val, effect)
      end,
      get = function(self)
        return UIConfig:getEnableContrast()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn09_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnableOutline(val, effect)
      end,
      get = function(self)
        return UIConfig:getEnableOutline()
      end,
      effect = true,
      curr = -1
    },
    CheckBtn10_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnableFog(val, effect)
      end,
      get = function(self)
        return UIConfig:getEnableFog()
      end,
      effect = true,
      curr = -1
    }
  },
  RadioWidgets2 = {
    Option11 = {
      set = function(self, val, effect)
        UIConfig:setOtherWithoutUI(val, effect)
      end,
      get = function(self)
        return UIConfig:getOtherWithoutUI()
      end,
      effect = true,
      curr = -1
    },
    Option12 = {
      set = function(self, val, effect)
        UIConfig:setShadowLevel(val, effect)
      end,
      get = function(self)
        return UIConfig:getShadowLevel()
      end,
      effect = true,
      curr = -1
    },
    Option13 = {
      set = function(self, val, effect)
        UIConfig:setGrassDensity(val, effect)
      end,
      get = function(self)
        return UIConfig:getGrassDensity()
      end,
      effect = true,
      curr = -1
    }
  }
}
local _DisSound = {
  SliderWidgets = {
    Volume1_sld = {
      set = function(self, val, effect)
        UIConfig:setSoundVolume(val, effect)
      end,
      get = function(self)
        return UIConfig:getSoundVolume()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = 100,
      offset = 1.0E-4,
      scale = 100
    },
    Volume2_sld = {
      set = function(self, val, effect)
        UIConfig:setMusicVolume(val, effect)
      end,
      get = function(self)
        return UIConfig:getMusicVolume()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = 100,
      offset = 1.0E-4,
      scale = 100
    }
  },
  CheckWidgets = {
    Mute1_cbtn = {
      set = function(self, val, effect)
        UIConfig:setSoundOpen(val, effect)
      end,
      get = function(self)
        return UIConfig:getSoundOpen()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = true
    },
    Mute2_cbtn = {
      set = function(self, val, effect)
        UIConfig:setMusicOpen(val, effect)
      end,
      get = function(self)
        return UIConfig:getMusicOpen()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = true
    },
    Mute3_cbtn = {
      set = function(self, val, effect)
        UIConfig:setEnvSoundOpen(not val, effect)
      end,
      get = function(self)
        return not UIConfig:getEnvSoundOpen()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = true
    },
    Mute4_cbtn = {
      set = function(self, val, effect)
        UIConfig:setUISoundOpen(not val, effect)
      end,
      get = function(self)
        return not UIConfig:getUISoundOpen()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = true
    },
    Mute5_cbtn = {
      set = function(self, val, effect)
        UIConfig:setCharacterSoundOpen(not val, effect)
      end,
      get = function(self)
        return not UIConfig:getCharacterSoundOpen()
      end,
      effect = true,
      curr = -1,
      prev = -1,
      default = true
    }
  }
}
local _DisExpertLevConfig = {
  [1] = {
    nil,
    false,
    true,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    1,
    0,
    1
  },
  [2] = {
    nil,
    false,
    true,
    false,
    false,
    false,
    false,
    true,
    false,
    false,
    2,
    0,
    1
  },
  [3] = {
    nil,
    false,
    true,
    true,
    false,
    true,
    true,
    true,
    true,
    false,
    3,
    1,
    2
  },
  [4] = {
    nil,
    true,
    true,
    true,
    true,
    true,
    true,
    true,
    true,
    false,
    4,
    2,
    3
  },
  [5] = {
    nil,
    false,
    true,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    5,
    0,
    1
  }
}
local DisMode =  {
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1
}
local _DisFreq = {
  -1,
  -1,
  -1,
  -1,
  -1,
  -1
}
local _DisDisplayBits = {
  hpmine_cbtn = 1,
  hpmon_e_cbtn = 2,
  hpmon_f_cbtn = 4,
  hpteam_cbtn = 8,
  hpplayer_e_cbtn = 16,
  hpplayer_f_cbtn = 32,
  hpall_cbtn = 255,
  petname_cbtn = 256,
  monname_cbtn = 1024,
  guildname_cbtn = 4194304,
  title_cbtn = 32768,
  montalk_cbtn = 16777216
}
local _Display_Move_Style = {
  [0] = "键盘+鼠标",
  "键盘",
  "鼠标"
}
local _Dis_GetShowFuncs = {
  function()
    return UIConfig:getShowTeamHp()
  end,
  function()
    return UIConfig:getShowSelfHp()
  end,
  function()
    return UIConfig:getShowMonEHp()
  end,
  function()
    return UIConfig:getShowMonFHp()
  end,
  function()
    return UIConfig:getShowPlayerEHp()
  end,
  function()
    return UIConfig:getShowPlayerFHp()
  end,
  function()
    return UIConfig:getShowMonsterName()
  end,
  function()
    return UIConfig:getShowTitle()
  end,
  function()
    return UIConfig:getShowMonsterTalk()
  end,
  function()
    return UIConfig:getShowGuildName()
  end,
  function()
    return UIConfig:getShowPetName()
  end
}
local _Dis_SetShowFuncs = {
  function(bVal)
    UIConfig:setShowTeamHp(bVal)
  end,
  function(bVal)
    UIConfig:setShowSelfHp(bVal)
  end,
  function(bVal)
    UIConfig:setShowMonEHp(bVal)
  end,
  function(bVal)
    UIConfig:setShowMonFHp(bVal)
  end,
  function(bVal)
    UIConfig:setShowPlayerEHp(bVal)
  end,
  function(bVal)
    UIConfig:setShowPlayerFHp(bVal)
  end,
  function(bVal)
    UIConfig:setShowMonsterName(bVal)
  end,
  function(bVal)
    UIConfig:setShowTitle(bVal)
  end,
  function(bVal)
    UIConfig:setShowMonsterTalk(bVal)
  end,
  function(bVal)
    UIConfig:setShowGuildName(bVal)
  end,
  function(bVal)
    UIConfig:setShowPetName(bVal)
  end
}
function _DisSetup_InitBasicCombo(ui_group, widgets, text)
  local wnd = WindowToComboBox(ui_group)
  wnd:RemoveAllItems()
  if text[1] then
    local count = text[1] + 1
    for i = 2, count do
      wnd:InsertString(tostring(text[i]), -1)
    end
    local name = wnd:GetProperty("WindowName")
    local widget = widgets[name]
    local val = widget:get()
    widget.prev = val
    widget.curr = val
    if wnd:GetProperty("WindowName") == "Option1_cbox" then
      gUI.DisSetup.Resolution = val
    end
    wnd:SetProperty("Text", "")
    wnd:SetProperty("Text", tostring(text[val + 2]))
  end
end
function _DisSetup_InitExpertRadio1(ui_group, widgets)
  local val, parent, level
  parent = ui_group
  level = gUI.DisSetup.CurrLevel
  for name, widget in pairs(widgets) do
    local index = tonumber(string.sub(name, 9, 10))
    val = widget:get()
    if nil ~= _DisExpertLevConfig[5][index] then
      _DisExpertLevConfig[5][index] = val
      if level ~= 5 then
        val = _DisExpertLevConfig[level][index]
      end
    end
    widget.curr = val
    local button = parent:GetChildByName(name)
    if val == 1 or val == true then
      _DisSetup_UpdateRadioButton2(button, true)
    else
      _DisSetup_UpdateRadioButton2(button, false)
    end
  end
end
function _DisSetup_InitExpertRadio2(ui_group, widgets)
  local val, parent, level
  parent = ui_group
  level = gUI.DisSetup.CurrLevel
  for name, widget in pairs(widgets) do
    local index = tonumber(string.sub(name, 7, 8))
    val = widget:get()
    if nil ~= _DisExpertLevConfig[5][index] then
      _DisExpertLevConfig[5][index] = val
      if level ~= 5 then
        val = _DisExpertLevConfig[level][index]
      end
    end
    widget.curr = val
    if name == "Option12" then
      val = val + 1
    end
    local str1, str2, str3
    if val == 1 then
      str1 = "_Low"
      str2 = "_Medium"
      str3 = "_High"
    elseif val == 2 then
      str1 = "_Medium"
      str2 = "_Low"
      str3 = "_High"
    else
      str1 = "_High"
      str2 = "_Low"
      str3 = "_Medium"
    end
    local button = parent:GetChildByName(name .. str1 .. "_cbtn")
    _DisSetup_UpdateRadioButton2(button, true)
    button = parent:GetChildByName(name .. str2 .. "_cbtn")
    _DisSetup_UpdateRadioButton2(button, false)
    button = parent:GetChildByName(name .. str3 .. "_cbtn")
    _DisSetup_UpdateRadioButton2(button, false)
  end
end
function _DisSetup_InitPateRadioBtn()
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.Pate_bg")
  local names = {
    "hpall_cbtn",
    "hpteam_cbtn",
    "hpmine_cbtn",
    "hpmon_e_cbtn",
    "hpmon_f_cbtn",
    "hpplayer_e_cbtn",
    "hpplayer_f_cbtn",
    "monname_cbtn",
    "title_cbtn",
    "montalk_cbtn",
    "guildname_cbtn",
    "petname_cbtn"
  }
  local checked = {
    UIConfig:getShowAllHp(),
    UIConfig:getShowTeamHp(),
    UIConfig:getShowSelfHp(),
    UIConfig:getShowMonEHp(),
    UIConfig:getShowMonFHp(),
    UIConfig:getShowPlayerEHp(),
    UIConfig:getShowPlayerFHp(),
    UIConfig:getShowMonsterName(),
    UIConfig:getShowTitle(),
    UIConfig:getShowMonsterTalk(),
    UIConfig:getShowGuildName(),
    UIConfig:getShowPetName()
  }
  local displayBit = 4294967295
  local removeBit = 0
  for i = 1, table.maxn(names) do
    local btn = WindowToCheckButton(wnd:GetChildByName(names[i]))
    btn:SetCheckedState(checked[i])
    if checked[i] or i == 1 then
    else
      removeBit = removeBit + _DisDisplayBits[names[i]]
    end
  end
  UIConfig:setNameboardDisplayOption(displayBit - removeBit)
  if checked[1] then
    local parent = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.Pate_bg")
    parent:GetChildByName("hpteam_cbtn"):SetProperty("Enable", "false")
    parent:GetChildByName("hpmine_cbtn"):SetProperty("Enable", "false")
    parent:GetChildByName("hpmon_e_cbtn"):SetProperty("Enable", "false")
    parent:GetChildByName("hpmon_f_cbtn"):SetProperty("Enable", "false")
    parent:GetChildByName("hpplayer_e_cbtn"):SetProperty("Enable", "false")
    parent:GetChildByName("hpplayer_f_cbtn"):SetProperty("Enable", "false")
  end
end
function _DisSetup_InitGameSetBtn()
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.GameSet_bg")
  local names = {
    "Option01_cbtn",
    "Option06_cbtn",
    "Option07_cbtn",
    "Option08_cbtn",
    "Option09_cbtn",
    "Option10_cbtn",
    "CheckButton1_cbtn",
    "CheckButton2_cbtn",
    "CheckButton4_cbtn",
    "CheckButton5_cbtn",
    "CheckButton6_cbtn",
    "TradeMini_cbtn",
    "Faerie_cbtn"
  }
  local b1, b2, _, b4, b5, b6 = GetSetting()
  local checked = {
    UIConfig:getShowTargetOfTarget(),
    UIConfig:getHidePlayerInMinimap(),
    UIConfig:getHideMonsterInMinimap(),
    UIConfig:getAutoDisMount(),
    UIConfig:getDeathBlink(),
    UIConfig:getCameraShock(),
    b1,
    b2,
    b4,
    b5,
    b6,
    UIConfig:getTradeMini(),
    UIConfig:getHideFaerie()
  }
  for i = 1, table.maxn(names) do
    local btn = WindowToCheckButton(wnd:GetChildByName(names[i]))
    btn:SetCheckedState(checked[i])
  end
  UIInterface:GetMiniMap():SetFilterFlag(0, UIConfig:getHidePlayerInMinimap())
  UIInterface:GetMiniMap():SetFilterFlag(1, UIConfig:getHideMonsterInMinimap())
end
function _DisSetup_SaveConfig(widgets)
  for name, widget in pairs(widgets) do
    if widget.curr ~= widget.prev and not widget.effect then
      widget:set(widget.curr, true)
    end
    widget.prev = widget.curr
  end
end
function _DisSetup_InitSlider(ui_group, widgets)
  local parent = ui_group
  for name, widget in pairs(widgets) do
    widget.prev = widget:get()
    widget.curr = widget.prev
    parent:GetChildByName(name):SetProperty("CurrVal", tostring((widget.curr - widget.offset) / widget.scale))
  end
end
function _DisSetup_InitCheckButton(ui_group, widgets)
  local parent = ui_group
  for name, widget in pairs(widgets) do
    local button = WindowToCheckButton(parent:GetChildByName(name))
    widget.prev = widget:get()
    widget.curr = widget.prev
    button:SetCheckedState(not widget.curr)
  end
end
function _DisSetup_InitRadioButton(ui_group, widgets)
  local parent = ui_group
  for name, widget in pairs(widgets) do
    widget.prev = widget:get()
    widget.curr = widget.prev
    local button = WindowToCheckButton(parent:GetChildByName(name))
    button:SetCheckedState(widget.curr)
  end
end
function _DisSetup_SetComboBox(ui_group, widgets, reset)
  local parent = ui_group
  for name, widget in pairs(widgets) do
    local combo = WindowToComboBox(parent:GetChildByName(name))
    local arg
    if reset and widget.curr ~= widget.default then
      if name == "Option1_cbox" then
        arg = widget.default()
      else
        arg = widget.default
      end
      if arg == true then
        arg = 1
      elseif arg == false then
        arg = 0
      end
      combo:SetSelectItem(arg)
    end
    if not reset and widget.curr ~= widget.prev then
      arg = widget.prev
      if arg == true then
        arg = 1
      elseif arg == false then
        arg = 0
      end
      combo:SetSelectItem(arg)
    end
  end
end
function _DisSetup_SetSlider(ui_group, widgets, reset)
  local parent = ui_group
  local abs = math.abs
  for name, widget in pairs(widgets) do
    if reset and widget.curr ~= widget.default then
      parent:GetChildByName(name):SetProperty("CurrVal", tostring((widget.default - widget.offset) / widget.scale))
    elseif not reset and abs(widget.curr - widget.prev) > 1.0E-4 then
      parent:GetChildByName(name):SetProperty("CurrVal", tostring((widget.prev - widget.offset) / widget.scale))
    end
  end
end
function _DisSetup_SetCheckButton(ui_group, widgets, reset)
  local parent = ui_group
  for name, widget in pairs(widgets) do
    if reset and widget.curr ~= widget.default then
      local button = parent:GetChildByName(name)
      button = WindowToCheckButton(button)
      button:SetChecked(not widget.default)
    elseif not reset and widget.curr ~= widget.prev then
      local button = parent:GetChildByName(name)
      button = WindowToCheckButton(button)
      button:SetChecked(not widget.prev)
    end
  end
end
function _DisSetup_SetRadioButton(ui_group, widgets, reset)
  local parent = ui_group
  for name, widget in pairs(widgets) do
    if reset and widget.curr ~= widget.default then
      local button = parent:GetChildByName(name)
      button = WindowToCheckButton(button)
      button:SetChecked(widget.default)
    elseif not reset and widget.curr ~= widget.prev then
      local button = parent:GetChildByName(name)
      button = WindowToCheckButton(button)
      button:SetChecked(widget.prev)
    end
  end
end
function _DisSetup_UpdateRadioButton2(button, checked)
  if button == nil then
    return
  end
  button = WindowToCheckButton(button)
  button:SetCheckedState(checked)
end
function _DisSetup_OptionChange(strText, bState)
  local parent = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.Pate_bg")
  local names = {
    "hpteam_cbtn",
    "hpmine_cbtn",
    "hpmon_e_cbtn",
    "hpmon_f_cbtn",
    "hpplayer_e_cbtn",
    "hpplayer_f_cbtn",
    "monname_cbtn",
    "title_cbtn",
    "montalk_cbtn",
    "guildname_cbtn",
    "petname_cbtn"
  }
  for index, name in ipairs(names) do
    if name == strText then
      local showValue = not _Dis_GetShowFuncs[index]()
      if bState ~= nil then
        showValue = bState
      end
      _Dis_SetShowFuncs[index](showValue)
      local button = WindowToCheckButton(parent:GetChildByName(name))
      button:SetChecked(showValue)
      local nValue = _DisDisplayBits[strText]
      UIConfig:DisplayOptionChange(nValue, showValue)
    end
  end
end
function _DisSetup_CloseToolTips()
  WindowSys_Instance:GetWindow("TooltipWindows.Tooltip1"):SetProperty("Visible", "false")
  RightClickMeun_HideAll()
end
function DisSetup_PromptShow(show)
  local wnd = Lua_Detail_ShowChildWnd("DisplayMessage", show)
  local combo = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg.Option1_cbox")
  combo = WindowToComboBox(combo)
  if show then
    do
      local text = string.format("当前分辨率为{#G^%s}", combo:GetItemText(_DisVideo.ComboWidgets.Option1_cbox.curr))
      wnd:GetChildByName("Label1"):SetProperty("Text", text)
      local counter = 10
      AddTimerEvent(1, "display_resolution_changed", function()
        if counter > 0 then
          local label = wnd:GetChildByName("Label2")
          label:SetProperty("Text", "是否确认修改？")
          label = wnd:GetChildByName("Label3")
          label:SetProperty("Text", string.format("{#G^%d}秒后恢复原分辨率。", counter))
          counter = counter - 1
        else
          DisSetup_PromptCancel()
        end
      end)
    end
  else
    combo:SetSelectItem(gUI.DisSetup.Resolution)
  end
end
function DisSetup_PromptConfirm()
  DelTimerEvent(1, "display_resolution_changed")
  gUI.DisSetup.Resolution = _DisVideo.ComboWidgets.Option1_cbox.curr
  DisSetup_PromptShow(false)
  if gUI.Bag.WndRoot:IsVisible() then
    _Bag_PlaceBag()
    _DisSetup_CloseToolTips()
  end
end
function DisSetup_PromptCancel()
  DelTimerEvent(1, "display_resolution_changed")
  _DisVideo.ComboWidgets.Option1_cbox.prev = gUI.DisSetup.Resolution
  DisSetup_PromptShow(false)
  UIConfig:setDispResolution(gUI.DisSetup.Resolution, true)
end
function DisSetup_ShowAllHP(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  local strState = tostring(not bState)
  local parent = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.Pate_bg")
  local btn = WindowToCheckButton(parent:GetChildByName("hpteam_cbtn"))
  _DisSetup_OptionChange(btn:GetProperty("WindowName"), bState)
  btn:SetProperty("Enable", strState)
  btn = WindowToCheckButton(parent:GetChildByName("hpmine_cbtn"))
  _DisSetup_OptionChange(btn:GetProperty("WindowName"), bState)
  btn:SetProperty("Enable", strState)
  btn = WindowToCheckButton(parent:GetChildByName("hpmon_e_cbtn"))
  _DisSetup_OptionChange(btn:GetProperty("WindowName"), bState)
  btn:SetProperty("Enable", strState)
  btn = WindowToCheckButton(parent:GetChildByName("hpmon_f_cbtn"))
  _DisSetup_OptionChange(btn:GetProperty("WindowName"), bState)
  btn:SetProperty("Enable", strState)
  btn = WindowToCheckButton(parent:GetChildByName("hpplayer_e_cbtn"))
  _DisSetup_OptionChange(btn:GetProperty("WindowName"), bState)
  btn:SetProperty("Enable", strState)
  btn = WindowToCheckButton(parent:GetChildByName("hpplayer_f_cbtn"))
  _DisSetup_OptionChange(btn:GetProperty("WindowName"), bState)
  btn:SetProperty("Enable", strState)
  UIConfig:setShowAllHp(bState)
end
function DisSetup_DisplayOptionChange(msg)
  local button = msg:get_window()
  local strText = button:GetProperty("WindowName")
  _DisSetup_OptionChange(strText)
end
function DisSetup_SelectPage(cur)
  local root = WindowSys_Instance:GetWindow("DisplaySetup")
  for i, name in ipairs({
    "Video_bg",
    "Sound_bg",
    "Pic_bg",
    "Advan_bg"
  }) do
    root:GetChildByName(name):SetProperty("Visible", tostring(cur == i))
  end
end
function DisSetup_Show()
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup")
  wnd:SetProperty("Visible", "true")
  local tc = WindowToTableCtrl(wnd:GetChildByName("Page_tctl"))
  if not tc:SelectItem(0) then
    for i, name in ipairs({
      "Video_bg",
      "Sound_bg",
      "Pic_bg"
    }) do
      wnd:GetChildByName(name):SetProperty("Visible", tostring(1 == i))
    end
  end
end
function DisSetup_Close(msg)
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup")
  wnd:SetProperty("Visible", "false")
  local video = wnd:GetGrandChild("Video_bg")
  local basic = video:GetGrandChild("Basic_bg")
  local expert = video:GetGrandChild("Expert_bg")
  local sound = wnd:GetChildByName("Sound_bg")
  _DisSetup_SetComboBox(basic, _DisVideo.ComboWidgets, false)
  _DisSetup_SetSlider(basic, _DisVideo.SliderWidgets, false)
  _DisSetup_SetRadioButton(basic, _DisVideo.RadioWidgets, false)
  _DisSetup_SetSlider(sound, _DisSound.SliderWidgets, false)
  _DisSetup_SetCheckButton(sound, _DisSound.CheckWidgets, false)
  if gUI.DisSetup.CurrLevel ~= gUI.DisSetup.PrevLevel then
    local button = expert:GetGrandChild("RadioMode" .. tostring(gUI.DisSetup.CurrLevel) .. "_cbtn")
    _DisSetup_UpdateRadioButton2(button, false)
    button = expert:GetGrandChild("RadioMode" .. tostring(gUI.DisSetup.PrevLevel) .. "_cbtn")
    _DisSetup_UpdateRadioButton2(button, true)
    gUI.DisSetup.CurrLevel = gUI.DisSetup.PrevLevel
  end
  DisSetup_UpdateExpertLevConfig(gUI.DisSetup.PrevLevel)
end
function DisSetup_Confirm(msg)
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup")
  wnd:SetProperty("Visible", "false")
  DisSetup_Apply(msg)
end
function DisSetup_Apply(msg)
  gUI.DisSetup.Count = 0
  DisSetup_SaveBasicRadio(_DisVideo.RadioWidgets)
  DisSetup_SaveBasicCombo(_DisVideo.ComboWidgets)
  if gUI.DisSetup.CurrLevel == 5 then
    for name, widget in pairs(_DisExpert2.RadioWidgets1) do
      local index = tonumber(string.sub(name, 9, 10))
      if nil ~= _DisExpertLevConfig[gUI.DisSetup.CurrLevel][index] then
        _DisExpertLevConfig[gUI.DisSetup.CurrLevel][index] = widget.curr
      end
    end
    for name, widget in pairs(_DisExpert2.RadioWidgets2) do
      local index = tonumber(string.sub(name, 7, 8))
      if nil ~= _DisExpertLevConfig[gUI.DisSetup.CurrLevel][index] then
        _DisExpertLevConfig[gUI.DisSetup.CurrLevel][index] = widget.curr
      end
    end
  end
  gUI.DisSetup.PrevLevel = gUI.DisSetup.CurrLevel
  UIConfig:setConfigureLevel(gUI.DisSetup.CurrLevel, false)
  _DisSetup_SaveConfig(_DisVideo.SliderWidgets)
  _DisSetup_SaveConfig(_DisSound.SliderWidgets)
  _DisSetup_SaveConfig(_DisSound.CheckWidgets)
  UIConfig:saveConfig()
  gUI.DisSetup.Count = 0
  _Bag_PlaceBag()
  _DisSetup_CloseToolTips()
end
function DisSetup_SaveBasicCombo(widgets)
  SetWindowsCommond()
  for name, widget in pairs(widgets) do
    if widget.curr ~= widget.prev and gUI.DisSetup.Count == 0 then
      widget:set(widget.curr, true)
      if name == "Option1_cbox" then
        DisSetup_PromptShow(true)
        gUI.DisSetup.Count = gUI.DisSetup.Count + 1
      end
    end
    widget.prev = widget.curr
  end
end
function DisSetup_SaveBasicRadio(widgets)
  local parent = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg")
  local change = 0
  for name, widget in pairs(widgets) do
    if widget.curr ~= widget.prev and gUI.DisSetup.Count == 0 and widget.curr then
      widget:set(true, true)
    end
    widget.prev = widget.curr
  end
end
function DisSetup_ResetConfig()
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup")
  local basic = wnd:GetChildByName("Video_bg")
  local sound = wnd:GetChildByName("Sound_bg")
  local gameset = wnd:GetChildByName("Pic_bg")
  if basic:IsVisible() then
    local parent = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg")
    _DisSetup_SetComboBox(parent, _DisVideo.ComboWidgets, true)
    _DisSetup_SetSlider(parent, _DisVideo.SliderWidgets, true)
    _DisSetup_SetRadioButton(parent, _DisVideo.RadioWidgets, true)
    parent = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Expert_bg")
    for key, name in pairs({
      [1] = "RadioMode1_cbtn",
      [2] = "RadioMode2_cbtn",
      [3] = "RadioMode3_cbtn",
      [4] = "RadioMode4_cbtn",
      [5] = "RadioMode5_cbtn"
    }) do
      if key ~= 2 then
        local button = parent:GetChildByName(name)
        _DisSetup_UpdateRadioButton2(button, false)
      else
        gUI.DisSetup.PrevLevel = gUI.DisSetup.CurrLevel
        gUI.DisSetup.CurrLevel = key
        local button = parent:GetChildByName(name)
        _DisSetup_UpdateRadioButton2(button, true)
      end
    end
    DisSetup_UpdateExpertLevConfig(gUI.DisSetup.CurrLevel)
  elseif sound:IsVisible() then
    _DisSetup_SetSlider(sound, _DisSound.SliderWidgets, true)
    _DisSetup_SetCheckButton(sound, _DisSound.CheckWidgets, true)
  elseif gameset:IsVisible() then
    local pate_reset = {
      hpall_cbtn = false,
      hpteam_cbtn = true,
      hpmine_cbtn = true,
      hpmon_e_cbtn = false,
      hpmon_f_cbtn = false,
      hpplayer_e_cbtn = false,
      hpplayer_f_cbtn = false,
      monname_cbtn = true,
      title_cbtn = true,
      montalk_cbtn = true,
      guildname_cbtn = true,
      petname_cbtn = false
    }
    local gameset_reset = {
      Option01_cbtn = true,
      Option06_cbtn = false,
      Option07_cbtn = false,
      Option08_cbtn = true,
      Option09_cbtn = true,
      Option10_cbtn = false,
      CheckButton1_cbtn = false,
      CheckButton2_cbtn = false,
      CheckButton4_cbtn = false,
      TradeMini_cbtn = false,
      Faerie_cbtn = false
    }
    local parent = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.Pate_bg")
    for name, value in pairs(pate_reset) do
      local button = WindowToCheckButton(parent:GetChildByName(name))
      if name == "hpall_cbtn" and button:IsChecked() then
        UIConfig:setShowAllHp(button:IsChecked())
        button:SetCheckedState(value)
        parent:GetChildByName("hpteam_cbtn"):SetProperty("Enable", "true")
        parent:GetChildByName("hpmine_cbtn"):SetProperty("Enable", "true")
        parent:GetChildByName("hpmon_e_cbtn"):SetProperty("Enable", "true")
        parent:GetChildByName("hpmon_f_cbtn"):SetProperty("Enable", "true")
        parent:GetChildByName("hpplayer_e_cbtn"):SetProperty("Enable", "true")
        parent:GetChildByName("hpplayer_f_cbtn"):SetProperty("Enable", "true")
      else
        button:SetChecked(value)
        _DisSetup_OptionChange(button:GetProperty("WindowName"), value)
      end
    end
    local parent = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.GameSet_bg")
    for name, value in pairs(gameset_reset) do
      local button = WindowToCheckButton(parent:GetChildByName(name))
      button:SetChecked(value)
    end
  end
end
function DisSetup_ComboBoxHandler(msg)
  local wnd = msg:get_window()
  wnd = WindowToComboBox(wnd)
  local parent = wnd:GetParent()
  local widgets = _DisVideo.ComboWidgets
  local name = wnd:GetProperty("WindowName")
  local widget = widgets[name]
  local val = wnd:GetSelectItem()
  local str = wnd:GetSelectItemText()
  widget.curr = val
  widget:set(val, false)
end
function DisSetup_CheckHandler(msg)
  local wnd = WindowToCheckButton(msg:get_window())
  local name = wnd:GetProperty("WindowName")
  local widget = _DisSound.CheckWidgets[name]
  local val = wnd:IsChecked()
  val = not val
  widget.curr = val
  if widget.effect then
    widget:set(val, true)
  end
end
function DisSetup_SliderHandler1(msg)
  local wnd = WindowToSlider(msg:get_window())
  local name = wnd:GetProperty("WindowName")
  local widget
  if wnd:GetParent():GetProperty("WindowName") == "Basic_bg" then
    widget = _DisVideo.SliderWidgets[name]
  else
    widget = _DisSound.SliderWidgets[name]
  end
  local val = wnd:GetCurrVal()
  widget.curr = val * widget.scale + widget.offset
  widget:set(widget.curr, true)
end
function DisSetup_SliderHandler2(msg)
  local wnd = msg:get_window()
  wnd = WindowToSlider(wnd)
  local name = wnd:GetProperty("WindowName")
  local widget = _DisVideo.SliderWidgets[name]
  local val = wnd:GetCurrVal()
  val = val * widget.scale
  widget.curr = val + widget.offset
  local lab
  if name == "Bright_sld" then
    lab = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg.Label34_dlab")
    val = val * 100
    if val < 10 then
      val = 10
    end
    lab:SetProperty("Text", string.format("%3.1d", val))
    widget:set(widget.curr, true)
  elseif name == "Number_sld" then
    lab = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg.Label39_dlab")
    val = widget.curr
    if val < 45 then
      val = 45
    end
    lab:SetProperty("Text", string.format("%3.1d", val))
    widget:set(widget.curr, true)
  elseif name == "UISize_sld" then
    lab = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg.Label40_dlab")
    local val1 = 0.75 + widget.curr * 0.25
    lab:SetProperty("Text", string.format("%.2f", val1))
  end
end
function DisSetup_BasicRadioHandler(msg)
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  if wnd:IsChecked() then
    local parent = wnd:GetParent()
    local widgets = _DisVideo.RadioWidgets
    local cur_name = wnd:GetProperty("WindowName")
    for name, widget in pairs(widgets) do
      if name ~= cur_name then
        local button = parent:GetChildByName(name)
        button = WindowToCheckButton(button)
        button:SetCheckedState(false)
        widget.curr = false
        widget:set(false, false)
      else
        widget:set(true, false)
        widget.curr = true
      end
    end
  else
    wnd:SetCheckedState(true)
  end
end
function DisSetup_SwitchToCustom()
  local expert = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Expert_bg")
  local button = expert:GetChildByName("RadioMode" .. tostring(gUI.DisSetup.CurrLevel) .. "_cbtn")
  _DisSetup_UpdateRadioButton2(button, false)
  gUI.DisSetup.CurrLevel = 5
  button = expert:GetChildByName("RadioMode5_cbtn")
  _DisSetup_UpdateRadioButton2(button, true)
end
function DisSetup_ExpertRadioHandler1(msg)
  local wnd = msg:get_window()
  wnd = WindowToCheckButton(wnd)
  local arg = wnd:IsChecked()
  local name = wnd:GetProperty("WindowName")
  local widget = _DisExpert2.RadioWidgets1[name]
  widget.curr = arg
  widget:set(arg, true)
  local index = tonumber(string.sub(name, 9, 10))
  if gUI.DisSetup.CurrLevel ~= 5 and nil ~= _DisExpertLevConfig[5][index] then
    DisSetup_SwitchToCustom()
  end
end
function DisSetup_ExpertRadioHandler2(msg)
  local wnd = WindowToCheckButton(msg:get_window())
  if wnd:IsChecked() then
    local parent = wnd:GetParent()
    local str = wnd:GetProperty("WindowName")
    local name = string.sub(str, 1, 8)
    local switch1, switch2, switch3, arg
    switch1 = string.sub(str, 9, -1)
    local widget = _DisExpert2.RadioWidgets2[name]
    if switch1 == "_Low_cbtn" then
      switch2 = "_Medium_cbtn"
      switch3 = "_High_cbtn"
      arg = 1
    elseif switch1 == "_Medium_cbtn" then
      switch2 = "_Low_cbtn"
      switch3 = "_High_cbtn"
      arg = 2
    else
      switch2 = "_Low_cbtn"
      switch3 = "_Medium_cbtn"
      arg = 3
    end
    button = parent:GetChildByName(name .. switch2)
    _DisSetup_UpdateRadioButton2(button, false)
    button = parent:GetChildByName(name .. switch3)
    _DisSetup_UpdateRadioButton2(button, false)
    if name == "Option12" then
      arg = arg - 1
    end
    widget.curr = arg
    widget:set(arg, true)
    local index = tonumber(string.sub(name, 7, 8))
    if gUI.DisSetup.CurrLevel ~= 5 and nil ~= _DisExpertLevConfig[5][index] then
      DisSetup_SwitchToCustom()
    end
  else
    wnd:SetCheckedState(true)
  end
end
function DisSetup_UpdateExpertLevConfig(level)
  local parent = WindowSys_Instance:GetWindow("DisplaySetup.Advan_bg.Expert_bg")
  local conf = _DisExpertLevConfig[level]
  for name, widget in pairs(_DisExpert2.RadioWidgets1) do
    local index = tonumber(string.sub(name, 9, 10))
    if nil ~= conf[index] then
      widget.curr = conf[index]
      local button = parent:GetChildByName(name)
      if widget.curr == true or widget.curr == 1 then
        _DisSetup_UpdateRadioButton2(button, true)
      else
        _DisSetup_UpdateRadioButton2(button, false)
      end
      widget:set(widget.curr, true)
    end
  end
  for name, widget in pairs(_DisExpert2.RadioWidgets2) do
    local index = tonumber(string.sub(name, 7, 8))
    if nil ~= conf[index] then
      widget.curr = conf[index]
      local val = widget.curr
      if name == "Option12" then
        val = val + 1
      end
      local str1, str2, str3
      if val == 1 then
        str1 = "_Low"
        str2 = "_Medium"
        str3 = "_High"
      elseif val == 2 then
        str1 = "_Medium"
        str2 = "_Low"
        str3 = "_High"
      else
        str1 = "_High"
        str2 = "_Low"
        str3 = "_Medium"
      end
      local button = parent:GetChildByName(name .. str1 .. "_cbtn")
      _DisSetup_UpdateRadioButton2(button, true)
      button = parent:GetChildByName(name .. str2 .. "_cbtn")
      _DisSetup_UpdateRadioButton2(button, false)
      button = parent:GetChildByName(name .. str3 .. "_cbtn")
      _DisSetup_UpdateRadioButton2(button, false)
      widget:set(widget.curr, true)
    end
  end
end
function DisSetup_SelectConfigure(msg)
  local wnd = WindowToCheckButton(msg:get_window())
  local parent = wnd:GetParent()
  local cur_name = wnd:GetProperty("WindowName")
  for key, name in pairs({
    [1] = "RadioMode1_cbtn",
    [2] = "RadioMode2_cbtn",
    [3] = "RadioMode3_cbtn",
    [4] = "RadioMode4_cbtn",
    [5] = "RadioMode5_cbtn"
  }) do
    if name ~= cur_name then
      local button = parent:GetChildByName(name)
      _DisSetup_UpdateRadioButton2(button, false)
    else
      gUI.DisSetup.CurrLevel = key
      local button = parent:GetChildByName(name)
      _DisSetup_UpdateRadioButton2(button, true)
    end
  end
  DisSetup_UpdateExpertLevConfig(gUI.DisSetup.CurrLevel)
end
function DisSetup_CheckDeathBlink(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setDeathBlink(bState)
end
function DisSetup_CheckCameraShock(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setCameraShock(bState)
end
function DisSetup_CheckHidePlayerInMinimap(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setHidePlayerInMinimap(bState)
  UIInterface:GetMiniMap():SetFilterFlag(0, bState)
end
function DisSetup_CheckHideMonsterInMinimap(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setHideMonsterInMinimap(bState)
  UIInterface:GetMiniMap():SetFilterFlag(1, bState)
end
function DisSetup_CheckHideFaerie(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  SetHideFaerie(bState)
end
function DisSetup_CheckTradeMini(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setTradeMini(bState)
end
function DisSetup_CheckShowTargetOfTarget(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setShowTargetOfTarget(bState)
  _OnPlays_TargetChangedInfo(true)
end
function DisSetup_CheckAutoDisMount(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  UIConfig:setAutoDisMount(bState, true)
end
function DisSetup_OnlineInfoChecked(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  ModifySetting(1, bState)
end
function DisSetup_RefuseAddChecked(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  ModifySetting(2, bState)
end
function DisSetup_CannotMallRequest(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  ModifySetting(5, bState)
end
function DisSetup_GuildOnlineRequest(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  ModifySetting(6, bState)
end
function DisSetup_RefuseChatChecked(msg)
  local button = msg:get_window()
  button = WindowToCheckButton(button)
  local bState = button:IsChecked()
  ModifySetting(4, bState)
end
function DisSetup_ChangeMoveStyle(msg)
  local style = msg:get_wparam()
  SetMoveStyle(style)
end
function UI_DisSetup_KeyDown(msg)
  if msg:get_wparam() == 1 then
    DisSetup_PromptCancel()
  end
end
function UI_DisSetup_SelectPage(msg)
  local cur = msg:get_wparam() + 1
  DisSetup_SelectPage(cur)
end
function _OnDisSetup_CheckAutoDisMount()
  if GetAutoMountState() ~= UIConfig:getAutoDisMount() then
    UIConfig:setAutoDisMount(UIConfig:getAutoDisMount(), true)
  end
end
function _OnDisSetup_CheckFriend()
  local wnd = WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.GameSet_bg")
  local names = {
    "CheckButton1_cbtn",
    "CheckButton2_cbtn",
    "CheckButton4_cbtn",
    "CheckButton5_cbtn",
    "CheckButton6_cbtn"
  }
  local b1, b2, _, b4, b5, b6 = GetSetting()
  local checked = {
    b1,
    b2,
    b4,
    b5,
    b6
  }
  for i = 1, table.maxn(names) do
    local btn = WindowToCheckButton(wnd:GetChildByName(names[i]))
    btn:SetCheckedState(checked[i])
  end
end
function _OnDisSetup_MoveStyle()
  local combo_box = WindowToComboBox(WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.GameSet_bg.Move_cbox"))
  local key, mouse = GetMoveStyle()
  if key == 1 and mouse == 1 then
    combo_box:SetSelectItem(0)
  elseif key == 1 then
    combo_box:SetSelectItem(1)
  elseif mouse == 1 then
    combo_box:SetSelectItem(2)
  end
end
function _OnSetup_DisPlayModeMax()
  local wnd = WindowToComboBox(WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg.Option1_cbox"))
  local nSelItem = wnd:GetItemCount() - 1
  if nSelItem >= 0 then
    UIConfig:setDispResolution(nSelItem, true)
    _Bag_PlaceBag()
    _DisSetup_CloseToolTips()
  end
end
function _OnSetup_DisPlayModeMin()
  _DisSetup_CloseToolTips()
end
function _OnSetup_DisPlayModeRestore()
  local wnd = WindowToComboBox(WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg.Option1_cbox"))
  local nSelItem = wnd:GetSelectItem()
  if nSelItem >= 0 then
    UIConfig:setDispResolution(nSelItem, true)
    _DisSetup_CloseToolTips()
  end
end
function Script_DisSetup_OnLoad()
  gUI.DisSetup.Resolution = 0
  gUI.DisSetup.Count = -1
  local basic = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Basic_bg")
  local expert1 = WindowSys_Instance:GetWindow("DisplaySetup.Video_bg.Expert_bg")
  local expert2 = WindowSys_Instance:GetWindow("DisplaySetup.Advan_bg.Expert_bg")
  local sound = WindowSys_Instance:GetWindow("DisplaySetup.Sound_bg")
  _DisMode[1], _DisMode[2], _DisMode[3], _DisMode[4], _DisMode[5], _DisMode[6], _DisMode[7], _DisMode[8], _DisMode[9], _DisMode[10], _DisMode[11], _DisMode[12], _DisMode[13], _DisMode[14], _DisMode[15], _DisMode[16], _DisMode[17], _DisMode[18], _DisMode[19], _DisMode[20], _DisMode[21] = GetDisplayModes()
  if _DisMode[1] > 20 then
    _DisMode[1] = 20
  end
  _DisSetup_InitBasicCombo(basic:GetChildByName("Option1_cbox"), _DisVideo.ComboWidgets, _DisMode)
  _DisFreq[1], _DisFreq[2], _DisFreq[3], _DisFreq[4], _DisFreq[5], _DisFreq[6] = GetDisplayFrequencys()
  if _DisFreq[1] > 5 then
    _DisFreq[1] = 5
  end
  _DisSetup_InitBasicCombo(basic:GetChildByName("Option3_cbox"), _DisVideo.ComboWidgets, _DisFreq)
  _DisSetup_InitSlider(basic, _DisVideo.SliderWidgets)
  _DisSetup_InitRadioButton(basic, _DisVideo.RadioWidgets)
  local level = UIConfig:getConfigureLevel()
  if level > 4 or level < 1 then
    level = 5
  end
  gUI.DisSetup.CurrLevel = level
  gUI.DisSetup.PrevLevel = level
  for i = 1, 5 do
    local button = expert1:GetChildByName("RadioMode" .. tostring(i) .. "_cbtn")
    _DisSetup_UpdateRadioButton2(button, i == level)
  end
  _DisSetup_InitExpertRadio1(expert2, _DisExpert2.RadioWidgets1)
  _DisSetup_InitExpertRadio2(expert2, _DisExpert2.RadioWidgets2)
  _DisSetup_InitSlider(sound, _DisSound.SliderWidgets)
  _DisSetup_InitCheckButton(sound, _DisSound.CheckWidgets)
  _DisSetup_InitPateRadioBtn()
  _DisSetup_InitGameSetBtn()
  local Display_Move = WindowToComboBox(WindowSys_Instance:GetWindow("DisplaySetup.Pic_bg.GameSet_bg.Move_cbox"))
  Display_Move:RemoveAllItems()
  for i = 0, #_Display_Move_Style do
    Display_Move:InsertString(_Display_Move_Style[i], i)
  end
  DisSetup_SelectPage(1)
end
function Script_DisSetup_OnEvent(event)
  if event == "PLAYER_CLIENTDATA" then
    _OnDisSetup_CheckAutoDisMount()
    _OnDisSetup_CheckFriend()
    _OnDisSetup_MoveStyle()
  elseif event == "DISPLAY_MODE_RESTORE" then
    _OnSetup_DisPlayModeRestore()
  elseif event == "DISPLAY_MODE_MAX" then
    _OnSetup_DisPlayModeMax()
  elseif event == "DISPLAY_MODE_MIN" then
    _OnSetup_DisPlayModeMin()
  end
end
function Script_DisSetup_OnEscape()
  DisSetup_Close()
end
