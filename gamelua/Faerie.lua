if gUI and not gUI.Faerie then
  gUI.Faerie = {}
end
local _Faerie_ShowSkillLv = 2
local _Faerie_ShowBlendLv = 3
local _Faerie_BaseHieght = 280
local _Faerie_BlendHieght = 265
local _Faerie_SkillHieght = 75
local _Faerie_ShowBoxName = "FAERIEBOX"
local _Faerie_OtherShowBoxName = "FAERIEOTHERBOX"
local _Faerie_SkillCntMax = 10
local _Faerie_TempName = ""
local _Faerie_ActiveModel = -1
local _Faerie_SelectModel = -1
local _Faerie_SelectFaerie = -1
local _Faerie_ChangeNumOnePage = 24
local _Faerie_CurPage = 0
local _Faerie_NewSkillBag = 0
local _Faerie_NewSkillFromOff = 0
local _Faerie_NewSkillSlotToReplace = 0
local _Faerie_CriticalRate1 = 0.02
local _Faerie_CriticalRate2 = 300
local _Faerie_BaseInfoDesc = {
  [1] = "体质",
  [2] = "力量",
  [3] = "敏捷",
  [4] = "智力"
}
function _Faerie_SkillInfo()
  local gb = gUI.Faerie.wndSkillBx
  gb:ResetAllGoods(true)
  for skillPos = 1, _Faerie_SkillCntMax do
    local skillId = GetFaerieSkillId(skillPos)
    local skillIcon = GetFaerieSkillIcon(skillPos)
    local index = skillPos - 1
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(index)
      else
        gb:SetItemGoods(index, skillIcon, -1)
        gb:SetItemData(index, skillId)
      end
    else
      gb:ClearGoodsItem(index)
      gb:SetItemData(index, 0)
    end
  end
end
function _Faerie_OtherSkillInfo()
  local gb = gUI.Faerie.wndOtherSkillBx
  gb:ResetAllGoods(true)
  for skillPos = 1, _Faerie_SkillCntMax do
    local skillId, skillIcon = GetOtherFaerieSkill(skillPos)
    local index = skillPos - 1
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(index)
      else
        gb:SetItemGoods(index, skillIcon, -1)
        gb:SetItemData(index, skillId)
      end
    else
      gb:ClearGoodsItem(index)
      gb:SetItemData(index, 0)
    end
  end
end
function _Faerie_BlendInfo()
  local blendList = GetFaerieBlendInfo()
  if blendList[0] == nil then
    return
  end
  for i = 0, table.maxn(blendList) do
    local wnd = gUI.Faerie.wndFaerieBlendList[i]
    local wndEnc = wnd:GetChildByName("Xian_slab")
    local wndEncValue = wnd:GetChildByName("Xian_dlab")
    local wndBlend = wnd:GetChildByName("XianAdd_slab")
    local wndBlendValue = wnd:GetChildByName("XianAdd_dlab")
    local wndBase = wnd:GetChildByName("XianBase_slab")
    local wndBaseValue = wnd:GetChildByName("XianBase_dlab")
    local wndAttr = wnd:GetChildByName("Promote_btn")
    wndEnc:SetProperty("Text", "总" .. blendList[i].Desc)
    wndEncValue:SetProperty("Text", tostring(blendList[i].ValueRate))
    wndBase:SetProperty("Text", "基础" .. blendList[i].Desc)
    wndBaseValue:SetProperty("Text", tostring(blendList[i].Value))
    wndBlend:SetProperty("Text", blendList[i].Desc .. "加成")
    wndBlendValue:SetProperty("Text", tostring(blendList[i].Rate) .. "%")
    wndAttr:SetCustomUserData(0, tonumber(blendList[i].Attr))
  end
end
function _Faerie_OtherBlendInfo()
  local blendList = GetOtherFaerieBlendInfo()
  if blendList[0] == nil then
    return
  end
  for i = 0, table.maxn(blendList) do
    local wnd = gUI.Faerie.wndOtherFaerieBlendList[i]
    local wndEnc = wnd:GetChildByName("Xian_slab")
    local wndEncValue = wnd:GetChildByName("Xian_dlab")
    local wndBlend = wnd:GetChildByName("XianAdd_slab")
    local wndBlendValue = wnd:GetChildByName("XianAdd_dlab")
    local wndBase = wnd:GetChildByName("XianBase_slab")
    local wndBaseValue = wnd:GetChildByName("XianBase_dlab")
    wndEnc:SetProperty("Text", "总" .. blendList[i].Desc)
    wndEncValue:SetProperty("Text", tostring(blendList[i].ValueRate))
    wndBase:SetProperty("Text", "基础" .. blendList[i].Desc)
    wndBaseValue:SetProperty("Text", tostring(blendList[i].Value))
    wndBlend:SetProperty("Text", blendList[i].Desc .. "加成")
    wndBlendValue:SetProperty("Text", tostring(blendList[i].Rate) .. "%")
  end
end
function _Faerie_BaseInfo()
  local FaerieId, nLv, train, trainMax, str, dex, int, con, modelId, name, nextLv, hasNext, score, lvDesc = GetFaerieBase()
  local _, _, _, _, level = GetPlaySelfProp(4)
  if FaerieId == -1 or nLv < 1 then
    gUI.Faerie.Faerieshowbox:hide()
    Lua_Chat_ShowOSD("OPERATE_RESULT", "仙灵未激活")
    return
  end
  gUI.Faerie.Faerieshowbox:show()
  gUI.Faerie.wndName:SetProperty("Text", name)
  if nLv <= #gUI_Faerie_LvName then
    gUI.Faerie.wndLv:SetProperty("Text", gUI_Faerie_LvName[nLv])
  end
  gUI.Faerie.wndCon:SetProperty("Text", tostring(con))
  gUI.Faerie.wndStr:SetProperty("Text", tostring(str))
  gUI.Faerie.wndDex:SetProperty("Text", tostring(dex))
  gUI.Faerie.wndInt:SetProperty("Text", tostring(int))
  gUI.Faerie.wndScore:SetProperty("Text", tostring(score))
  gUI.Faerie.eboxPetName:SetProperty("Text", name)
  gUI.Faerie.wndTrain:SetProperty("Text", tostring(train) .. "/" .. tostring(trainMax))
  _Faerie_TempName = name
  local wndHeight = _Faerie_BaseHieght
  if nLv >= _Faerie_ShowSkillLv then
    wndHeight = wndHeight + _Faerie_SkillHieght
    gUI.Faerie.wndFaerieSkill:setEnableHeightCal(true)
    _Faerie_SkillInfo()
  else
    gUI.Faerie.wndFaerieSkill:setEnableHeightCal(false)
  end
  if nLv >= _Faerie_ShowBlendLv then
    wndHeight = wndHeight + _Faerie_BlendHieght
    gUI.Faerie.wndFaerieBlend:setEnableHeightCal(true)
    _Faerie_BlendInfo()
  else
    gUI.Faerie.wndFaerieBlend:setEnableHeightCal(false)
  end
  gUI.Faerie.wndBg:SetProperty("Height", "p" .. tostring(wndHeight))
  gUI.Faerie.wndContainer:SizeSelf()
  if modelId > 0 then
    if not gUI.Faerie.Faerieshowbox:isAvatarExist("Faerie" .. tostring(modelId)) then
      gUI.Faerie.Faerieshowbox:createMountAvatar(modelId, "Faerie")
    end
    gUI.Faerie.Faerieshowbox:setCurrentAvatar("Faerie" .. tostring(modelId))
    _Faerie_ActiveModel = modelId
  end
  gUI.Faerie.TrainDesc:SetProperty("TooltipTextDes", lvDesc)
  local bHasFaerie = GetCurFaerie()
  if bHasFaerie then
    gUI.Faerie.btnBattle:SetProperty("Text", "收回")
    gUI.Faerie.btnBattle:AddScriptEvent("wm_mouseclick", "UI_Faerie_Callback")
  else
    gUI.Faerie.btnBattle:SetProperty("Text", "出战")
    gUI.Faerie.btnBattle:AddScriptEvent("wm_mouseclick", "UI_Faerie_Callup")
  end
  gUI.Faerie.wndRoot:SetVisible(true)
end
function _Faerie_OtherBaseInfo()
  local FaerieId, nLv, train, trainMax, str, dex, int, con, modelId, name, score = GetOtherFaerieBase()
  gUI.Faerie.OtherFaerieshowbox:show()
  gUI.Faerie.wndOtherName:SetProperty("Text", name)
  if nLv <= #gUI_Faerie_LvName then
    gUI.Faerie.wndOtherLv:SetProperty("Text", gUI_Faerie_LvName[nLv])
  end
  gUI.Faerie.wndOtherCon:SetProperty("Text", tostring(con))
  gUI.Faerie.wndOtherStr:SetProperty("Text", tostring(str))
  gUI.Faerie.wndOtherDex:SetProperty("Text", tostring(dex))
  gUI.Faerie.wndOtherInt:SetProperty("Text", tostring(int))
  gUI.Faerie.wndOtherScore:SetProperty("Text", tostring(score))
  gUI.Faerie.wndOtherTrain:SetProperty("Text", tostring(train) .. "/" .. tostring(trainMax))
  local wndHeight = _Faerie_BaseHieght
  if nLv >= _Faerie_ShowSkillLv then
    wndHeight = wndHeight + _Faerie_SkillHieght
    gUI.Faerie.wndOtherFaerieSkill:setEnableHeightCal(true)
    _Faerie_OtherSkillInfo()
  else
    gUI.Faerie.wndOtherFaerieSkill:setEnableHeightCal(false)
  end
  if nLv >= _Faerie_ShowBlendLv then
    wndHeight = wndHeight + _Faerie_BlendHieght
    gUI.Faerie.wndOtherFaerieBlend:setEnableHeightCal(true)
    _Faerie_OtherBlendInfo()
  else
    gUI.Faerie.wndOtherFaerieBlend:setEnableHeightCal(false)
  end
  gUI.Faerie.wndOtherBg:SetProperty("Height", "p" .. tostring(wndHeight))
  gUI.Faerie.wndOtherContainer:SizeSelf()
  if modelId > 0 then
    if not gUI.Faerie.OtherFaerieshowbox:isAvatarExist("FaerieOther" .. tostring(modelId)) then
      gUI.Faerie.OtherFaerieshowbox:createMountAvatar(modelId, "FaerieOther")
    end
    gUI.Faerie.OtherFaerieshowbox:setCurrentAvatar("FaerieOther" .. tostring(modelId))
  end
end
function UI_Faerie_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _Faerie_OpenChange()
  gUI.Faerie.ChangeGb:ResetAllGoods(true)
  local activeList = GetActivedFaerieList()
  if activeList[0] == nil then
    return
  end
  local num = table.maxn(activeList)
  gUI.Faerie.dlabPage:SetProperty("Text", tostring(_Faerie_CurPage + 1) .. "/" .. tostring(math.floor(num / _Faerie_ChangeNumOnePage) + 1))
  if _Faerie_CurPage >= math.floor(num / _Faerie_ChangeNumOnePage) then
    gUI.Faerie.btnNextPage:SetEnable(false)
  else
    gUI.Faerie.btnNextPage:SetEnable(true)
  end
  if _Faerie_CurPage <= 0 then
    gUI.Faerie.btnLastPage:SetEnable(false)
  else
    gUI.Faerie.btnLastPage:SetEnable(true)
  end
  if num > _Faerie_CurPage * _Faerie_ChangeNumOnePage + _Faerie_ChangeNumOnePage - 1 then
    num = _Faerie_CurPage * _Faerie_ChangeNumOnePage + _Faerie_ChangeNumOnePage - 1
  end
  local index = 0
  for i = _Faerie_CurPage * _Faerie_ChangeNumOnePage, num do
    local icon = activeList[i].Icon
    if icon ~= "" then
      if activeList[i].Active then
        gUI.Faerie.ChangeGb:SetItemGoods(index, icon, -1)
        gUI.Faerie.ChangeGb:SetIconState(index, 9, true)
      else
        gUI.Faerie.ChangeGb:SetItemGoods(index, icon, -1)
        gUI.Faerie.ChangeGb:SetIconState(index, 9, false)
      end
      gUI.Faerie.ChangeGb:SetItemData(index, activeList[i].Id)
    end
    index = index + 1
  end
end
function _Faerie_CloseChange()
  if _Faerie_ActiveModel ~= _Faerie_SelectModel and _Faerie_ActiveModel > 0 then
    if not gUI.Faerie.Faerieshowbox:isAvatarExist("Faerie" .. tostring(_Faerie_ActiveModel)) then
      gUI.Faerie.Faerieshowbox:createMountAvatar(_Faerie_ActiveModel, "Faerie")
    end
    gUI.Faerie.Faerieshowbox:setCurrentAvatar("Faerie" .. tostring(_Faerie_ActiveModel))
  end
end
function _Faerie_ShowSkillReplace(skillId, bag, from_off)
  if not gUI.Pet.wndRoot2:IsVisible() then
    gUI.Pet.wndRoot2:SetVisible(true)
    _Faerie_UpdateSkillReplace(skillId, bag, from_off)
  else
    UI_Pet_CloseSkillReplace()
  end
end
function _Faerie_UpdateSkillReplace(skillIdNew, bag, from_off)
  _Faerie_NewSkillBag = bag
  _Faerie_NewSkillFromOff = from_off
  local gb = gUI.Pet.gboxPetSkillReplace
  gb:ResetAllGoods(true)
  for skillPos = 1, _Faerie_SkillCntMax do
    local skillId = GetFaerieSkillId(skillPos)
    local skillIcon = GetFaerieSkillIcon(skillPos)
    local index = skillPos - 1
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(index)
      else
        gb:SetItemGoods(index, skillIcon, -1)
        gb:SetItemData(index, skillId)
      end
    else
      gb:ClearGoodsItem(index)
    end
  end
  gb = gUI.Pet.gboxPetSkillNew
  gb:ResetAllGoods(true)
  local name, icon, category, Type, desc = GetSkillBaseInfo(skillIdNew)
  gb:SetItemGoods(0, icon, -1)
  gb:SetItemData(0, skillIdNew)
  gUI.Pet.slabPetSkillName:SetProperty("Text", name)
  gUI.Pet.btnPetSkillReplace:SetEnable(false)
end
function _Faerie_BlendTip(wndUserdata, tooltip)
  local blendList = GetFaerieBlendInfo()
  if blendList[0] == nil then
    return
  end
  Lua_Tip_Begin(tooltip)
  local desc = ""
  if 100 == wndUserdata then
    tooltip:InsertLeftText("仙攻", colorUserDefine, "kaiti_13", 0, 0)
    local itemId = tooltip:InsertLeftText("总仙攻: ", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText(tostring(blendList[0].ValueRate), colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
    local itemId1 = tooltip:InsertLeftText("通过基础仙攻获得: " .. tostring(blendList[0].Value) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
    local itemId2 = tooltip:InsertLeftText("通过仙攻加成获得: " .. tostring(blendList[0].Rate) .. "%", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText("(" .. tostring(blendList[0].ValueRate - blendList[0].Value) .. ")", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
    desc = string.format("仙灵攻击目标时可以造成%d点伤害", blendList[0].ValueRate)
    tooltip:InsertLeftText(desc, colorYellow, "fzheiti_11", 0, 0)
  elseif 101 == wndUserdata then
    tooltip:InsertLeftText("仙防", colorUserDefine, "kaiti_13", 0, 0)
    local itemId = tooltip:InsertLeftText("总仙防: ", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText(tostring(blendList[1].ValueRate), colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
    local itemId1 = tooltip:InsertLeftText("通过基础仙防获得: " .. tostring(blendList[1].Value) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
    local itemId2 = tooltip:InsertLeftText("通过仙防加成获得: " .. tostring(blendList[1].Rate) .. "%", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText("(" .. tostring(blendList[1].ValueRate - blendList[1].Value) .. ")", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
    desc = string.format("人物角色受到仙灵攻击时伤害减少%d点", blendList[1].ValueRate)
    tooltip:InsertLeftText(desc, colorYellow, "fzheiti_11", 0, 0)
  elseif 102 == wndUserdata then
    tooltip:InsertLeftText("重击", colorUserDefine, "kaiti_13", 0, 0)
    local itemId = tooltip:InsertLeftText("总重击: ", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText(tostring(blendList[2].ValueRate), colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
    local itemId1 = tooltip:InsertLeftText("通过基础重击获得: " .. tostring(blendList[2].Value) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
    local itemId2 = tooltip:InsertLeftText("通过重击加成获得: " .. tostring(blendList[2].Rate) .. "%", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText("(" .. tostring(blendList[2].ValueRate - blendList[2].Value) .. ")", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
    local value = (_Faerie_CriticalRate1 + blendList[2].ValueRate / (blendList[2].ValueRate + _Faerie_CriticalRate2)) * 100
    desc = string.format("仙灵攻击目标时有%d%%几率造成重击", value)
    tooltip:InsertLeftText(desc, colorYellow, "fzheiti_11", 0, 0)
  elseif 103 == wndUserdata then
    tooltip:InsertLeftText("韧性", colorUserDefine, "kaiti_13", 0, 0)
    local itemId = tooltip:InsertLeftText("总韧性: ", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText(tostring(blendList[3].ValueRate), colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
    local itemId1 = tooltip:InsertLeftText("通过基础韧性获得: " .. tostring(blendList[3].Value) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
    local itemId2 = tooltip:InsertLeftText("通过韧性加成获得: " .. tostring(blendList[3].Rate) .. "%", colorUserDefine1, "fzheiti_11", 0, 0)
    tooltip:InsertText("(" .. tostring(blendList[3].ValueRate - blendList[3].Value) .. ")", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
    desc = string.format("被仙灵重击时减少%d%%重击伤害", math.floor(blendList[3].ValueRate / 6))
    tooltip:InsertLeftText(desc, colorYellow, "fzheiti_11", 0, 0)
  end
  Lua_Tip_End(tooltip, true)
end
function Lua_Faerie_ShowReplaceWnd(skillId, bag, from_off)
  _Faerie_ShowSkillReplace(skillId, bag, from_off)
end
function _OnFaerie_NameUpdate(name)
  if gUI.Faerie.wndRoot:IsVisible() then
    gUI.Faerie.wndName:SetProperty("Text", name)
    gUI.Faerie.eboxPetName:SetProperty("Text", name)
    _Faerie_TempName = name
  end
end
function _OnFaerie_LvUp(times)
  _Faerie_BaseInfo()
  local upList = GetFaerieLvupInfo(times)
  if upList[0] == nil then
    return
  end
  for i = 0, table.maxn(upList) do
    local nType = upList[i].Type
    if nType >= 0 and nType < 4 then
      local wnd = gUI.Faerie.wndLvAttrList[nType]
      wnd:SetProperty("Text", "+ " .. tostring(upList[i].Value))
    end
  end
  gUI.Faerie.wndOther:SetVisible(true)
end
function _OnFaerie_Update()
  if gUI.Faerie.wndRoot:IsVisible() then
    _Faerie_BaseInfo()
  end
  if gUI.Faerie.wndChange:IsVisible() then
    _Faerie_OpenChange()
  end
end
function _OnFaerie_ShowOther()
  _Faerie_OtherBaseInfo()
  gUI.Faerie.wndOtherRoot:SetVisible(true)
end
function _OnFaerie_CallFaerie(bCall)
  if bCall then
    gUI.Faerie.btnBattle:SetProperty("Text", "收回")
    gUI.Faerie.btnBattle:AddScriptEvent("wm_mouseclick", "UI_Faerie_Callback")
  else
    gUI.Faerie.btnBattle:SetProperty("Text", "出战")
    gUI.Faerie.btnBattle:AddScriptEvent("wm_mouseclick", "UI_Faerie_Callup")
  end
end
function _OnFaerie_CallOutBattle(bCall)
  if bCall then
    gUI.Faerie.BattleBtn:SetVisible(false)
  else
    gUI.Faerie.BattleBtn:SetVisible(true)
  end
end
function _OnFaerie_CanProvier(bProvier)
  if bProvier then
    gUI.Faerie.ProvierEff:SetVisible(true)
    gUI.Faerie.Surpass_eff:SetVisible(true)
  else
    gUI.Faerie.ProvierEff:SetVisible(false)
    gUI.Faerie.Surpass_eff:SetVisible(false)
  end
end
function UI_Faerie_Show()
  if not gUI.Faerie.wndRoot:IsVisible() then
    UI_Faerie_Open()
    gUI.Faerie.wndChange:SetVisible(false)
    gUI.Faerie.wndOther:SetVisible(false)
  else
    UI_Faerie_Close()
  end
end
function UI_Faerie_Open()
  _Faerie_BaseInfo()
end
function UI_Faerie_Close()
  gUI.Faerie.wndRoot:SetVisible(false)
  gUI.Faerie.wndChange:SetVisible(false)
  gUI.Faerie.wndOther:SetVisible(false)
  gUI.Faerie.Faerieshowbox:hide()
  _Faerie_CurPage = 0
end
function UI_Faerie_OtherClose()
  gUI.Faerie.wndOtherRoot:SetVisible(false)
  gUI.Faerie.OtherFaerieshowbox:hide()
end
function UI_Faerie_ShowSkillTips(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local skill_id = msg:get_lparam()
  local index = msg:get_wparam()
  local tooltip = WindowToTooltip(gb:GetToolTipWnd(0))
  if skill_id ~= 0 then
    Lua_Tip_Skill(tooltip, skill_id, 1, false, false)
  else
    local text = "仙灵技能可以通过仙灵技能书获得"
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(text, "FFFAFAFA", "", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_Faerie_Rename()
  local edit_box = WindowToEditBox(gUI.Faerie.eboxPetName)
  if not edit_box:IsKeyboardCaptureWindow() then
    WindowSys_Instance:SetFocusWindow(gUI.Faerie.eboxPetName)
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.Faerie.eboxPetName)
  else
    local strRename = gUI.Faerie.eboxPetName:GetProperty("Text")
    if string.len(strRename) == 0 then
      ShowErrorMessage("宠物名称不能为空")
      gUI.Faerie.eboxPetName:SetProperty("Text", _Faerie_TempName)
      return
    elseif string.len(strRename) > 12 then
      ShowErrorMessage("宠物名称不能超过6个字")
      return
    elseif WorldStage:isValidString(strRename) == 0 then
      WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_INVALID)
      return
    end
    if strRename ~= _Faerie_TempName then
      AskFaerieRename(strRename)
    end
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    WindowSys_Instance:SetFocusWindow(WindowSys_Instance:GetRootWindow())
  end
end
function UI_Faerie_OpenChange()
  if gUI.Faerie.wndChange:IsVisible() then
    gUI.Faerie.wndChange:SetVisible(false)
    _Faerie_CloseChange()
    _Faerie_CurPage = 0
  else
    _Faerie_OpenChange()
    gUI.Faerie.wndChange:SetVisible(true)
  end
end
function UI_Faerie_Lvup()
  local FaerieId, nLv, train, trainMax, str, dex, int, con, modelId, name, nextLv, hasNext, score = GetFaerieBase()
  local _, _, _, _, level = GetPlaySelfProp(4)
  if nextLv > level then
    Lua_Chat_ShowOSD("FAERIE_LEVEL_UP_ERR", nextLv)
  else
    AskFaerieLvup()
  end
end
function UI_Faerie_LvUpClose()
  gUI.Faerie.wndOther:SetVisible(false)
end
function UI_Faerie_SelectFaerie(msg)
  local nFaerie = msg:get_wparam()
  _Faerie_SelectFaerie = nFaerie
  local nModelId = GetFaerieTmpByIndex(nFaerie)
  if nModelId > 0 then
    if not gUI.Faerie.Faerieshowbox:isAvatarExist("Faerie" .. tostring(nModelId)) then
      gUI.Faerie.Faerieshowbox:createMountAvatar(nModelId, "Faerie")
    end
    gUI.Faerie.Faerieshowbox:setCurrentAvatar("Faerie" .. tostring(nModelId))
    _Faerie_SelectModel = nModelId
  end
end
function UI_Faerie_OpenPanel(msg)
  Faerie_IncomeOpen()
end
function UI_OpenFaerie_Intensify(msg)
  local wnd = msg:get_window()
  local index = tonumber(wnd:GetProperty("CustomUserData"))
  Faerie_IntensifyOpen(index)
end
function UI_Faerie_Change()
  if _Faerie_SelectFaerie > 0 then
    ChangeFaerie(_Faerie_SelectFaerie)
  end
end
function UI_Faerie_LastPage(msg)
  _Faerie_CurPage = _Faerie_CurPage - 1
  _Faerie_OpenChange()
end
function UI_Faerie_NextPage(msg)
  _Faerie_CurPage = _Faerie_CurPage + 1
  _Faerie_OpenChange()
end
function UI_Faerie_Change_Tip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local offset = msg:get_wparam()
  if wnd:IsItemHasIcon(offset) then
    local nFaerie = msg:get_lparam()
    local name, icon, list = GetFaerieCompareTip(nFaerie)
    if name == nil then
      return
    end
    local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
    Lua_Tip_Begin(tooltip)
    local itemid = tooltip:InsertLeftText(tostring(name), colorWhite, "kaiti_13", 0, 0)
    tooltip:InsertImage(tostring(icon), 49, 49, itemid, "RIGHT", 0, 0)
    tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
    local FaerieId = GetFaerieBase()
    if FaerieId == nFaerie then
      tooltip:InsertLeftText("当前正在使用", colorWhite, "", 0, 0)
    elseif list[0] ~= nil then
      local str = string.format("使用%s外观时", name)
      tooltip:InsertLeftText(str, colorGreen, "", 0, 0)
      for i = 0, table.maxn(list) do
        local color = colorGreen
        local text = "%s  +%d"
        if 0 > list[i].Value then
          color = colorRed
          text = "%s  %d"
        end
        local str = string.format(text, list[i].Desc, list[i].Value)
        tooltip:InsertLeftText(str, color, "", 0, 0)
      end
    else
      local str = string.format("点击确认使用%s外观", name)
      tooltip:InsertLeftText(str, colorGreen, "", 0, 0)
    end
    Lua_Tip_End(tooltip)
  end
end
function UI_Faerie_SkillReplaceOk()
  UseItemPetSkill(_Faerie_NewSkillBag, _Faerie_NewSkillFromOff, _Faerie_NewSkillSlotToReplace)
  gUI.Pet.wndRoot2:SetVisible(false)
end
function UI_Faerie_SelectSkill(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local from_type = gb:GetProperty("BoxType")
  local slot = msg:get_lparam()
  gUI.Pet.btnPetSkillReplace:SetEnable(true)
  _Faerie_NewSkillSlotToReplace = slot + 1
end
function UI_Faerie_ShowBaseTip(msg)
  local wnd = msg:get_window()
  local num = wnd:GetProperty("Text")
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  Lua_Tip_Begin(tooltip)
  local str = string.format("仙灵出战时增加主人{#G^%s}点%s", num, _Faerie_BaseInfoDesc[wndUserdata])
  tooltip:InsertLeftPraseText(str, colorWhite, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function UI_Faerie_ShowBlendTip(msg)
  local wnd = msg:get_window()
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  _Faerie_BlendTip(wndUserdata, tooltip)
end
function UI_Faerie_ShowLvTip(msg)
  local FaerieId, nLv = GetFaerieBase()
  if nLv > #gUI_Faerie_LvName then
    return
  end
  local str = "已到最高境界"
  if nLv ~= #gUI_Faerie_LvName then
    str = string.format("下一境界:%s", gUI_Faerie_LvName[nLv + 1])
  end
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(str, colorWhite, "", 0, 0)
  tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 3, 0, 0, false)
  str = "仙灵境界说明#r筑基境界：可以学习仙灵技能#r结丹境界：开启兽魂属性，宠物可以进行攻击"
  tooltip:InsertLeftText(str, colorWhite, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function UI_Faerie_Callback()
  AskCallbackFaerie()
end
function UI_Faerie_Callup()
  AskCallupFaerie()
end
function UI_Faerie_BlendUp(msg)
  local wnd = msg:get_window()
  local attr = wnd:GetCustomUserData(0)
  local itemGuid, itemName, itemQuality, Desc, nMax, nBaseValue = GetCanFaerieBlandItem(attr)
  if itemGuid then
    Messagebox_Show("USE_FAERIE_BLEND", itemName, itemQuality, itemGuid, Desc, nMax, nBaseValue)
  else
    ShowErrorMessage("背包内没有符合条件的魂力道具")
  end
end
function Script_Faerie_OnLoad()
  gUI.Faerie.wndRoot = WindowSys_Instance:GetWindow("Faerie")
  gUI.Faerie.wndFaerieBase = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg")
  gUI.Faerie.wndFaerieSkill = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Skill_bg")
  gUI.Faerie.wndFaerieBlend = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Blend_bg")
  gUI.Faerie.wndBg = WindowSys_Instance:GetWindow("Faerie.Faerie_bg")
  gUI.Faerie.wndContainer = WindowToContainer(WindowSys_Instance:GetWindow("Faerie.Faerie_ctn"))
  gUI.Faerie.wndChange = WindowSys_Instance:GetWindow("Faerie.FaerieChange_bg")
  gUI.Faerie.wndOther = WindowSys_Instance:GetWindow("Faerie.FaerieOther_bg")
  gUI.Faerie.wndName = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Left_bg.Name_dlab")
  gUI.Faerie.wndLv = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Jing_dlab")
  gUI.Faerie.wndTrain = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Pei_dlab")
  gUI.Faerie.wndCon = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Ti_dlab")
  gUI.Faerie.wndStr = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Li_dlab")
  gUI.Faerie.wndDex = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Min_dlab")
  gUI.Faerie.wndInt = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Zhi_dlab")
  gUI.Faerie.wndScore = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Ping_dlab")
  gUI.Faerie.eboxPetName = WindowToEditBox(WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Left_bg.Replace_bg.Replace_ebox"))
  gUI.Faerie.wndSkillBx = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Skill_bg.Faerie_gbox"))
  gUI.Faerie.wndFaerieBlendList = {}
  gUI.Faerie.wndFaerieBlendList[0] = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Blend_bg.LeftTop_bg")
  gUI.Faerie.wndFaerieBlendList[1] = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Blend_bg.RightTop_bg")
  gUI.Faerie.wndFaerieBlendList[2] = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Blend_bg.Left_bg")
  gUI.Faerie.wndFaerieBlendList[3] = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Blend_bg.Right_bg")
  gUI.Faerie.Faerieshowbox = WindowToPicture(WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Left_bg.Mode_pic"))
  local showbox = UIInterface:getShowbox(_Faerie_ShowBoxName)
  showbox = showbox or UIInterface:createShowbox(_Faerie_ShowBoxName, gUI.Faerie.Faerieshowbox)
  gUI.Faerie.Faerieshowbox = showbox
  gUI.Faerie.ChangeGb = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie.FaerieChange_bg.Center_bg.Faerie_gbox"))
  gUI.Faerie.btnNextPage = WindowSys_Instance:GetWindow("Faerie.FaerieChange_bg.Down_bg.Page_bg.Next_btn")
  gUI.Faerie.btnLastPage = WindowSys_Instance:GetWindow("Faerie.FaerieChange_bg.Down_bg.Page_bg.Last_btn")
  gUI.Faerie.dlabPage = WindowSys_Instance:GetWindow("Faerie.FaerieChange_bg.Down_bg.Page_bg.Page_dlab")
  _Faerie_CurPage = 0
  gUI.Faerie.wndLvAttrList = {}
  gUI.Faerie.wndLvAttrList[0] = WindowSys_Instance:GetWindow("Faerie.FaerieOther_bg.Image_bg.Li_dlab")
  gUI.Faerie.wndLvAttrList[1] = WindowSys_Instance:GetWindow("Faerie.FaerieOther_bg.Image_bg.Min_dlab")
  gUI.Faerie.wndLvAttrList[2] = WindowSys_Instance:GetWindow("Faerie.FaerieOther_bg.Image_bg.Zhi_dlab")
  gUI.Faerie.wndLvAttrList[3] = WindowSys_Instance:GetWindow("Faerie.FaerieOther_bg.Image_bg.Ti_dlab")
  gUI.Faerie.TrainDesc = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Pei_dlab")
  gUI.Faerie.wndOtherRoot = WindowSys_Instance:GetWindow("Faerie_Other")
  gUI.Faerie.wndOtherFaerieBase = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg")
  gUI.Faerie.wndOtherFaerieSkill = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Skill_bg")
  gUI.Faerie.wndOtherFaerieBlend = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Blend_bg")
  gUI.Faerie.wndOtherBg = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_bg")
  gUI.Faerie.wndOtherContainer = WindowToContainer(WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn"))
  gUI.Faerie.wndOtherName = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Left_bg.Name_dlab")
  gUI.Faerie.wndOtherLv = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Jing_dlab")
  gUI.Faerie.wndOtherTrain = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Pei_dlab")
  gUI.Faerie.wndOtherCon = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Ti_dlab")
  gUI.Faerie.wndOtherStr = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Li_dlab")
  gUI.Faerie.wndOtherDex = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Min_dlab")
  gUI.Faerie.wndOtherInt = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Zhi_dlab")
  gUI.Faerie.wndOtherScore = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Right_bg.Lab_bg.Ping_dlab")
  gUI.Faerie.wndOtherSkillBx = WindowToGoodsBox(WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Skill_bg.Faerie_gbox"))
  gUI.Faerie.wndOtherFaerieBlendList = {}
  gUI.Faerie.wndOtherFaerieBlendList[0] = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Blend_bg.LeftTop_bg")
  gUI.Faerie.wndOtherFaerieBlendList[1] = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Blend_bg.RightTop_bg")
  gUI.Faerie.wndOtherFaerieBlendList[2] = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Blend_bg.Left_bg")
  gUI.Faerie.wndOtherFaerieBlendList[3] = WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Blend_bg.Right_bg")
  gUI.Faerie.OtherFaerieshowbox = WindowToPicture(WindowSys_Instance:GetWindow("Faerie_Other.Faerie_ctn.Base_bg.Faerie_bg.Left_bg.Mode_pic"))
  local showboxOther = UIInterface:getShowbox(_Faerie_OtherShowBoxName)
  showboxOther = showboxOther or UIInterface:createShowbox(_Faerie_OtherShowBoxName, gUI.Faerie.OtherFaerieshowbox)
  gUI.Faerie.OtherFaerieshowbox = showboxOther
  gUI.Faerie.btnBattle = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Battle_btn")
  gUI.Faerie.BattleBtn = WindowSys_Instance:GetWindow("FaerieBattle")
  gUI.Faerie.ProvierEff = WindowSys_Instance:GetWindow("ActionBar_frm.frame3_bg.Pet_btn.pet_eff")
  gUI.Faerie.ProvierEff:SetVisible(false)
  gUI.Faerie.Surpass_eff = WindowSys_Instance:GetWindow("Faerie.Faerie_ctn.Base_bg.Faerie_bg.Left_bg.Surpass_btn.surpass_eff")
  gUI.Faerie.Surpass_eff:SetVisible(false)
end
function Script_Faerie_OnEvent(event)
  if event == "ON_FAERIE_RENMAE" then
    _OnFaerie_NameUpdate(arg1)
  elseif event == "ON_FAERIE_LVUP" then
    _OnFaerie_LvUp(arg1)
  elseif event == "ON_FAERIE_UPDATE" then
    _OnFaerie_Update()
  elseif event == "ON_ACTIVE_FAERIE" then
    UI_Faerie_Show()
  elseif event == "ON_SHOW_OTHERFAERIE" then
    _OnFaerie_ShowOther()
  elseif event == "ON_CURFAERIE_CALL" then
    _OnFaerie_CallFaerie(arg1)
  elseif event == "ON_CURFAERIE_CALLOUTBATTLE" then
    _OnFaerie_CallOutBattle(arg1)
  elseif event == "ON_CURFAERIE_CANPROVIER" then
    _OnFaerie_CanProvier(arg1)
  end
end

