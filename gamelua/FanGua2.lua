function FanGua2_Close_Click(msg)
  local wnd = WindowSys_Instance:GetWindow("Verification")
  wnd:SetProperty("Visible", "false")
end
function FanGua2_OnRefresh_timerstop(msg)
  local wnd = msg:get_window()
  wnd:SetProperty("Text", "换一张")
end
function FanGua2_OnRefreshBtnClick(msg)
  ImageCheckRefreshQuestion()
  local wnd = msg:get_window()
  wnd:SetProperty("Enable", "false")
  wnd:SetProperty("Text", "换一张(&4&)")
end
function FanGua2_OnOkBtnClick(msg)
  local wnd = WindowSys_Instance:GetWindow("Verification.EditBox1")
  local input = wnd:GetProperty("Text")
  ImageCheckAnswerQuestion(input)
  FanGua2_Close_Click(msg)
end
function FanGua2_OnCloseBtnClick(msg)
  FanGua2_Close_Click(msg)
end
function FanGua2_OnStart(countTime)
  local wnd = WindowSys_Instance:GetWindow("Verification.Error.Time")
  wnd:SetProperty("Visible", "true")
  wnd:SetProperty("Text", "&" .. countTime / 1000 .. "&")
  wnd = WindowSys_Instance:GetWindow("Verification")
  local x = math.random(25, 75)
  local y = math.random(25, 75)
  wnd:SetLeft(x / 100)
  wnd:SetTop(y / 100)
  wnd:SetProperty("Visible", "true")
end
function FanGua2_OnEvent(event)
  if event == "IMAGECHECK2_SHOW" then
    FanGua2_OnStart(arg1)
  end
end