if gUI and not gUI.Fri then
  gUI.Fri = {}
end
local gFri_JobPic = "UiIamge004:Image_Friendleave"
local _FriAddList = {}
local USER_GUIDE_ADDFRI_UI = 89
local _Fri_Friend_Type = 0
local _Fri_Friend_Index = 0
local _Fri_CurrentSel = -1
local _Fri_FriendCount = -1
local _Fri_FriendUpdateIndex = -1
local _Fri_FriendUpdateNum = 13
function _Fri_GetFreeGroupId()
  for i = 1, 10 do
    if gUI.Fri.FriGroup[i] == nil then
      return i
    end
  end
  return nil
end
function _Fri_CheckGroupName(str)
  if str == "在线好友" then
    return false
  end
  local i, j = string.find(str, "%s+")
  if i ~= nil then
    return false
  end
  if 0 == WorldStage:isValidString(str) then
    return false
  end
  return true
end
function _Fri_ShowChannelByPageBtn(selected)
  for i = 0, 1 do
    gUI.Fri.ChannelList[i]:SetVisible(false)
  end
  for i = 0, _Fri_Friend_Index do
    local itemTmp = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChildByData1(i)
    if itemTmp then
      local wnd = WindowToButton(itemTmp)
      wnd:SetStatus("normal")
    end
  end
  if selected == 0 then
    gUI.Fri.BlackMember:SetVisible(false)
    gUI.Fri.EnemyMember:SetVisible(false)
    gUI.Fri.WrathMember:SetVisible(false)
    gUI.Fri.Search:SetVisible(true)
    gUI.Fri.ClearBtn:SetEnable(false)
    gUI.Fri.SearchBox:SetProperty("Text", "")
    Lua_Fri_SetFoucsToWorld()
    _Fri_GetFriendGroupList()
    if _Fri_Friend_Type == 0 then
      gUI.Fri.ChannelList[0]:SetVisible(true)
    else
      gUI.Fri.ChannelList[1]:SetVisible(true)
    end
    gUI.Fri.DelFri:SetVisible(true)
    gUI.Fri.DelFri:SetEnable(true)
    gUI.Fri.DelFri:SetProperty("Text", "删除好友")
    gUI.Fri.DelFri:AddScriptEvent("wm_mouseclick", "UI_Fri_DelFri")
    gUI.Fri.FunctionBtn:SetProperty("Text", "添加好友")
    gUI.Fri.FunctionBtn:SetVisible(true)
    gUI.Fri.FunctionBtn:SetEnable(true)
    gUI.Fri.FunctionBtn:AddScriptEvent("wm_mouseclick", "UI_Fri_AddBtnClick")
  elseif selected == 1 then
    gUI.Fri.Search:SetVisible(false)
    gUI.Fri.EnemyMember:SetVisible(true)
    gUI.Fri.BlackMember:SetVisible(false)
    gUI.Fri.WrathMember:SetVisible(false)
    gUI.Fri.DelFri:SetProperty("Text", "删除仇人")
    gUI.Fri.DelFri:SetVisible(true)
    local list = WindowToListBox(gUI.Fri.EnemyMember:GetGrandChild("Enemy_lbox"))
    if list:GetItemCount() == 0 then
      gUI.Fri.DelFri:SetEnable(false)
    else
      gUI.Fri.DelFri:SetEnable(true)
    end
    gUI.Fri.DelFri:AddScriptEvent("wm_mouseclick", "UI_Fri_DelEnemy")
    gUI.Fri.FunctionBtn:SetProperty("Text", "添加仇人")
    gUI.Fri.FunctionBtn:SetVisible(true)
    gUI.Fri.FunctionBtn:SetEnable(true)
    gUI.Fri.FunctionBtn:AddScriptEvent("wm_mouseclick", "UI_Fri_AddEnemyClick")
  elseif selected == 2 then
    gUI.Fri.Search:SetVisible(false)
    gUI.Fri.BlackMember:SetVisible(true)
    gUI.Fri.EnemyMember:SetVisible(false)
    gUI.Fri.WrathMember:SetVisible(false)
    gUI.Fri.DelFri:SetVisible(false)
    gUI.Fri.FunctionBtn:SetProperty("Text", "取消屏蔽")
    gUI.Fri.FunctionBtn:SetVisible(true)
    local list = WindowToListBox(gUI.Fri.BlackMember:GetGrandChild("Black_lbox"))
    if list:GetItemCount() == 0 then
      gUI.Fri.FunctionBtn:SetEnable(false)
    else
      gUI.Fri.FunctionBtn:SetEnable(true)
    end
    gUI.Fri.FunctionBtn:AddScriptEvent("wm_mouseclick", "UI_Fri_DelBlack")
  elseif selected == 3 then
    gUI.Fri.Search:SetVisible(false)
    gUI.Fri.BlackMember:SetVisible(false)
    gUI.Fri.EnemyMember:SetVisible(false)
    gUI.Fri.DelFri:SetVisible(false)
    gUI.Fri.FunctionBtn:SetVisible(false)
    gUI.Fri.WrathMember:SetVisible(true)
    AskWrathList()
  end
end
function _Fri_AddRelation2(typeId, name, level, guid, job, region)
  local color = "ffffffff"
  if region == "" then
    color = "ffa9a9a9"
  end
  if typeId == 103 then
    local list = WindowToListBox(gUI.Fri.BlackMember:GetGrandChild("Black_lbox"))
    local str = string.format("{#C^%s^%s}|{#C^%s^%s}|{#C^%s^%d}", color, name, color, gUI_MenPaiName[job], color, level)
    local item = list:InsertString(str, -1)
    item:SetUserData64(guid)
    list:SetCustomUserData(0, typeId)
    gUI.Fri.FunctionBtn:SetEnable(true)
  elseif typeId == 104 then
    local kill, avenge = GetEnemyKill(guid)
    local list = WindowToListBox(gUI.Fri.EnemyMember:GetGrandChild("Enemy_lbox"))
    local str = string.format("{#C^%s^%s}|{#C^%s^%s}|{#C^%s^%d}|{#C^%s^%d}|{#C^%s^%d}", color, name, color, gUI_MenPaiName[job], color, level, color, kill, color, avenge)
    local item = list:InsertString(str, -1)
    item:SetUserData64(guid)
    list:SetCustomUserData(0, typeId)
    gUI.Fri.DelFri:SetEnable(true)
  end
end
function _Fri_AddRelation(typeId, name, level, guid, job, region, friLv, curFriExp, maxFriExp, index)
  if type(index) ~= "number" then
    return
  end
  local color = "ffffffff"
  if region == "" then
    color = "ffa9a9a9"
  end
  local newitem, FriLvSlab, progress, percent
  if typeId == 101 then
    newitem = gUI.Fri.ChannelList[0]:GetChildByData1(index)
    newitem = newitem or gUI.Fri.ChannelList[0]:Insert(-1, gUI.Fri.templateItem)
    FriLvSlab = newitem:GetChildByName("FriendLev_dlab")
    progress = WindowToProgressBar(newitem:GetChildByName("ProgBar_pbar"))
    percent = newitem:GetChildByName("Percent_dlab")
  else
    newitem = gUI.Fri.ChannelList[1]:GetChildByData1(index)
    newitem = newitem or gUI.Fri.ChannelList[1]:Insert(-1, gUI.Fri.templateItem2)
  end
  _Fri_Friend_Index = index
  newitem:SetVisible(true)
  newitem:SetEnable(true)
  local nameSlab = newitem:GetChildByName("Name_dlab")
  local icon = WindowToPicture(newitem:GetChildByName("Pic1_bg"))
  local levelSlab = newitem:GetChildByName("Lev_dlab")
  local menpai = newitem:GetChildByName("Type_dlab")
  nameSlab:SetProperty("Text", tostring(name))
  nameSlab:SetProperty("FontColor", tostring(color))
  newitem:SetCustomUserData(0, typeId)
  newitem:SetCustomUserData(1, _Fri_Friend_Index)
  if typeId == 101 then
    gUI.Fri.ChannelList[0]:SetAllChildVisible(true, 1, _Fri_Friend_Index)
  else
    gUI.Fri.ChannelList[1]:SetAllChildVisible(true, 1, _Fri_Friend_Index)
  end
  if guid ~= nil then
    newitem:SetUserData64(guid)
  end
  if typeId == 101 or typeId == 102 then
    icon:SetVisible(true)
    local _, head_id, _, _, _, _, _, _, delTime, _, _, _, _, Sex = GetFriendDetailInfo(guid)
    if typeId == 101 then
      if region == "" then
        icon:SetProperty("BackImage", gUI_PLAYER_ICON_LEAVE[job][Sex])
      else
        icon:SetProperty("BackImage", gUI_PLAYER_ICON[job][Sex][head_id])
      end
    end
    menpai:SetProperty("Text", gUI_MenPaiName[job])
    if typeId == 101 then
      levelSlab:SetProperty("Text", tostring(level))
    else
      levelSlab:SetProperty("Text", tostring(level) .. "级")
    end
    levelSlab:SetProperty("FontColor", tostring(color))
    if typeId == 101 then
      FriLvSlab:SetProperty("Text", "好友度：" .. tostring(friLv) .. "级")
      local rate = curFriExp / maxFriExp
      percent:SetProperty("Text", string.format("%0.1f%%", rate * 100))
      progress:SetProgress(rate)
      local bShowDelBtn = delTime > 0
      local delBtn = newitem:GetChildByName("Back_btn")
      delBtn:SetProperty("Text", string.format("恢复（剩余%d小时）", delTime))
      delBtn:SetVisible(bShowDelBtn)
      progress:SetVisible(not bShowDelBtn)
      percent:SetVisible(false)
      FriLvSlab:SetVisible(not bShowDelBtn)
    elseif typeId == 102 then
      local bShowDelBtn = delTime > 0
      local delBtn = newitem:GetChildByName("Back_btn")
      delBtn:SetVisible(bShowDelBtn)
      icon:SetVisible(not bShowDelBtn)
      delBtn:SetCustomUserData(0, delTime)
    end
  else
    levelSlab:SetProperty("Text", "")
  end
end
function _Fri_IsRelationExist(groupId, guid)
  local item = WindowToTreeItem(gUI.Fri.FriGroup[groupId]):FindItemByData(guid, nil)
  if item ~= nil then
    return true
  else
    return false
  end
end
function _Fri_ShowInfoWnd(name, level, job, guild, secene, friPoint, isOnline)
  WindowSys_Instance:GetWindow("FriendsInfo.Pic3_bg.Name_dlab"):SetProperty("Text", "名字：" .. name)
  WindowSys_Instance:GetWindow("FriendsInfo.Pic6_bg.Lev_dlab"):SetProperty("Text", "等级：" .. tostring(level))
  WindowSys_Instance:GetWindow("FriendsInfo.Pic4_bg.Metier_dlab"):SetProperty("Text", "职业：" .. gUI_MenPaiName[job])
  WindowSys_Instance:GetWindow("FriendsInfo.Pic5_bg.Friend_dlab"):SetProperty("Text", "帮会：" .. guild)
  WindowSys_Instance:GetWindow("FriendsInfo.Pic8_bg.Place_dlab"):SetProperty("Text", "所在地：" .. secene)
  WindowSys_Instance:GetWindow("FriendsInfo.Pic7_bg.FriendValue_dlab"):SetProperty("Text", "好友度：" .. tostring(friPoint))
  if isOnline then
    WindowSys_Instance:GetWindow("FriendsInfo.Pic2_bg.Away_dlab"):SetProperty("Text", "在线")
  else
    WindowSys_Instance:GetWindow("FriendsInfo.Pic2_bg.Away_dlab"):SetProperty("Text", "离线")
  end
  gUI.Fri.DetailInfo:SetProperty("Visible", "true")
end
function _Fri_UpdateTime()
  if _Fri_FriendUpdateIndex > _Fri_FriendCount then
    gUI.Fri.UpdateTime:Stop()
    return
  end
  local endIndex = _Fri_FriendUpdateIndex + _Fri_FriendUpdateNum
  GetFriendGroupList(_Fri_FriendUpdateIndex, endIndex)
  _Fri_FriendUpdateIndex = endIndex
end
function _Fri_GetFriendGroupList()
  _Fri_FriendCount = SortRelationList()
  if _Fri_FriendCount > 0 then
    _Fri_FriendUpdateIndex = 0
    GetFriendGroupList(_Fri_FriendUpdateIndex, _Fri_FriendUpdateNum)
    _Fri_FriendUpdateIndex = _Fri_FriendUpdateNum
    gUI.Fri.UpdateTime:Restart()
  end
end
function _Fri_GetEnemyGroupList()
  GetEnemyGroupList()
end
function _Fri_GetBlackGroupList()
  GetBlackGroupList()
end
function UI_Fri_AddBtnClick()
  if not gUI.Fri.TempChatCI:IsVisible() then
    gUI.Fri.AddFriI:SetProperty("Visible", "true")
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.Fri.FindInptNameBox)
  else
    Lua_Chat_ShowOSD("FRIEND_ADD_GROUP_OPEN")
  end
end
function UI_Fri_AddEnemyClick()
  gUI.Fri.AddEnemy:SetProperty("Visible", "true")
  WindowSys_Instance:SetKeyboardCaptureWindow(gUI.Fri.EnemyInptNameBox)
end
function UI_Fri_MClick(msg)
  local item = msg:get_window()
  local userdata = item:GetCustomUserData(0)
  if userdata == 101 or userdata == 102 then
    local guid = item:GetUserData64()
    local target_name, group_id = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    Lua_MenuShow("ON_FRIENDS", guid, target_name)
  elseif userdata == 103 then
    local index = msg:get_wparam()
    item = WindowToListBox(item)
    if item then
      local guid = item:GetItemData64(index)
      local target_name, group_id = GetRelationInfo(guid, gUI_RELATION_TYPE.blacklist)
      Lua_MenuShow("ON_BLACK", guid, target_name)
    end
  elseif userdata == 104 then
    local index = msg:get_wparam()
    item = WindowToListBox(item)
    if item then
      local guid = item:GetItemData64(index)
      local target_name, group_id = GetRelationInfo(guid, gUI_RELATION_TYPE.enemy)
      Lua_MenuShow("ON_ENEMY", guid, target_name)
    end
  end
end
function UI_Fri_InviteClick()
  if gUI.Fri.FindInptNameBox:GetProperty("Text") ~= "" then
    local textlen = string.len(gUI.Fri.FindInptNameBox:GetProperty("Text"))
    if textlen >= 4 then
      AddToFriend(gUI.Fri.FindInptNameBox:GetProperty("Text"))
      UI_Fri_AddFriCloseBtnClick()
    else
      Lua_Chat_ShowOSD("FRI_FINDFRI_NAME_TOO_SHORT")
    end
  else
    ShowErrorMessage("请输入玩家名")
  end
end
function UI_Fri_EnemyClick()
  if gUI.Fri.EnemyInptNameBox:GetProperty("Text") ~= "" then
    local textlen = string.len(gUI.Fri.EnemyInptNameBox:GetProperty("Text"))
    if textlen >= 4 then
      AddToEnemy(gUI.Fri.EnemyInptNameBox:GetProperty("Text"))
      UI_Fri_AddEnemyCloseBtnClick()
    else
      Lua_Chat_ShowOSD("FRI_FINDFRI_NAME_TOO_SHORT")
    end
  else
    ShowErrorMessage("请输入玩家名")
  end
end
function UI_Fri_CancelDelFri(msg)
  local guid = msg:get_window():GetParent():GetUserData64()
  CancelDelFriend(guid)
end
function UI_Fri_AddFriCloseBtnClick()
  gUI.Fri.AddFriI:SetProperty("Visible", "false")
  gUI.Fri.FindInptNameBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
end
function UI_Fri_AddEnemyCloseBtnClick()
  gUI.Fri.AddEnemy:SetProperty("Visible", "false")
  gUI.Fri.EnemyInptNameBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
end
function UI_Fri_ShowMainUI()
  if gUI.Fri.Root:IsVisible() then
    UI_Fri_MainCloseBtn()
    return
  end
  gUI.Fri.BlackMember:SetVisible(false)
  gUI.Fri.EnemyMember:SetVisible(false)
  gUI.Fri.WrathMember:SetVisible(false)
  gUI.Fri.Search:SetVisible(true)
  _Fri_SetPageSelect(0)
  gUI.Fri.ClearBtn:SetEnable(false)
  gUI.Fri.SearchBox:SetProperty("Text", "")
  if _Fri_Friend_Type == 0 then
    gUI.Fri.ChannelList[1]:SetVisible(false)
    gUI.Fri.ChannelList[0]:SetVisible(true)
  else
    gUI.Fri.ChannelList[0]:SetVisible(false)
    gUI.Fri.ChannelList[1]:SetVisible(true)
  end
  for i = 0, _Fri_Friend_Index do
    local itemTmp = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChildByData1(i)
    if itemTmp then
      local wnd = WindowToButton(itemTmp)
      wnd:SetStatus("normal")
    end
  end
  _Fri_CurrentSel = -1
  RequestRelationList()
  Lua_Fri_UpdataChatGroup()
  gUI.Fri.Root:SetProperty("Visible", "true")
  gUI.Fri.DelFri:SetVisible(true)
  gUI.Fri.DelFri:SetEnable(true)
  gUI.Fri.DelFri:SetProperty("Text", "删除好友")
  gUI.Fri.DelFri:AddScriptEvent("wm_mouseclick", "UI_Fri_DelFri")
  gUI.Fri.FunctionBtn:SetEnable(true)
  gUI.Fri.FunctionBtn:SetProperty("Text", "添加好友")
  gUI.Fri.FunctionBtn:SetVisible(true)
  gUI.Fri.FunctionBtn:AddScriptEvent("wm_mouseclick", "UI_Fri_AddBtnClick")
  WorldStage:playSoundByID(39)
  AskHomeInfo(0, -1)
end
function UI_Fri_ShowDetail(msg)
  local item = msg:get_window()
  if item == nil then
    return
  end
  local guid = item:GetUserData64()
  local userdata = item:GetCustomUserData(0)
  if userdata == 101 or userdata == 102 then
    RequestFriendDetailInfo(guid)
    gUI.Fri.GetDetailLock = item
  end
end
function UI_Fri_ShowDelTime(msg)
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local delTime = wnd:GetCustomUserData(0)
  local desc = ""
  if delTime > 0 then
    desc = "恢复好友（剩余" .. tostring(delTime) .. "小时）"
  end
  if desc ~= "" then
    Lua_Tip_Property(tooltip, desc)
  end
end
function UI_Fri_DetailCloseBtn()
  gUI.Fri.DetailInfo:SetProperty("Visible", "false")
end
function UI_Fri_RequestPartyBtn()
  InviteJoinTeamByGUID(gUI.Fri.DetailInfoName, gUI.Fri.DetailInfoGuid)
end
function UI_Fri_1v1ChatBtn()
  Lua_Cp2p_AddTargetToCur(gUI.Fri.DetailInfoName)
end
function UI_Fri_1v1ChatDBClick(msg)
  local item = msg:get_window()
  if not item then
    return
  end
  local guid = item:GetUserData64()
  local userdata = item:GetCustomUserData(0)
  if userdata == 101 or userdata == 102 then
    local name = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    Lua_Cp2p_AddTargetToCur(name)
  elseif userdata == 106 then
    Lua_ChatC_ShowChatC()
  end
end
function UI_Fri_CreateTempBtn()
  if not gUI.Fri.AddFriI:IsVisible() then
    gUI.Fri.TempChatCBox:SetProperty("Text", "")
    gUI.Fri.TempChatCI:SetProperty("Visible", "true")
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.Fri.TempChatCBox)
  else
    Lua_Chat_ShowOSD("FRIEND_ADD_FRI_OPEN")
  end
end
function UI_Fri_CloseTempBtn()
  gUI.Fri.TempChatCI:SetProperty("Visible", "false")
  gUI.Fri.TempChatCBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
end
function UI_Fri_CreateTempGroup()
  gUI.Fri.CreateTempGroupName = gUI.Fri.TempChatCBox:GetProperty("Text")
  if gUI.Fri.CreateTempGroupName ~= nil and _Fri_CheckGroupName(gUI.Fri.CreateTempGroupName) then
    UI_Fri_CloseTempBtn()
    Lua_ChatC_ShowNameListI()
  else
    WorldGroup_ShowNewStr(UI_SYS_MSG.CHAT_INVALID_GROUP)
  end
end
function UI_Fri_MainCloseBtn()
  gUI.Fri.UpdateTime:Stop()
  gUI.Fri.SearchBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
  gUI.Fri.Root:SetProperty("Visible", "false")
end
function UI_Friends_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Friends.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Fri_FindBoxChange(msg)
  gUI.Fri.ChannelList[0]:ResetScrollBar()
  gUI.Fri.ChannelList[0]:SetAllChildVisible(false, 0, 101)
  gUI.Fri.ChannelList[0]:SizeSelf()
  gUI.Fri.ChannelList[1]:ResetScrollBar()
  gUI.Fri.ChannelList[1]:SetAllChildVisible(false, 0, 102)
  gUI.Fri.ChannelList[1]:SizeSelf()
  _Fri_CurrentSel = -1
  gUI.Fri.ClearBtn:SetEnable(true)
  local str = msg:get_window():GetProperty("Text")
  if str ~= "" then
    local reList = GetSearchFriendList(str)
    if reList[0] ~= nil then
      local i = 0
      for index, item in pairs(reList) do
        local friLevel, curPoint, maxPoint = GetFriendPointDetail(item.GUID)
        _Fri_AddRelation(101, item.name, item.Lev, item.GUID, item.Job, item.Secene, friLevel, curPoint, maxPoint, item.Index)
        _Fri_AddRelation(102, item.name, item.Lev, item.GUID, item.Job, item.Secene, friLevel, curPoint, maxPoint, item.Index)
      end
    end
  else
    _Fri_GetFriendGroupList()
  end
end
function UI_Fri_SearchListClick(msg)
  local root = WindowSys_Instance:GetWindow("MainMenu")
  local menu = WindowToRightClickMenu(root:GetChildByName("ListMenu"))
  local index = msg:get_wparam()
  local guid = menu:GetItemData64(index)
  if guid ~= nil then
    local name = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    if name ~= nil then
      if menu:GetItemData(index) == 1 then
        local townd = WindowSys_Instance:GetWindow("EmailWrite.Write_bg.Picture1_bg.WriterName_ebox")
        townd:SetProperty("Text", name)
      else
        Lua_Cp2p_AddTargetToCur(name)
      end
    end
  end
end
function UI_Fri_PageSelectChange(msg)
  local index = WindowToButton(msg:get_window()):GetProperty("CustomUserData")
  if tonumber(index) >= 0 and tonumber(index) <= #gUI.Fri.TypeBtns then
    _Fri_SetPageSelect(tonumber(index))
  end
end
function _Fri_SetPageSelect(index)
  for i = 0, #gUI.Fri.TypeBtns do
    gUI.Fri.TypeBtns[i]:SetEnable(true)
  end
  gUI.Fri.TypeBtns[index]:SetEnable(false)
  _Fri_ShowChannelByPageBtn(index)
end
function UI_Fri_Selected(msg)
  local item = msg:get_window()
  local index = item:GetCustomUserData(1)
  for i = 0, _Fri_Friend_Index do
    local itemTmp = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChildByData1(i)
    if itemTmp then
      local wnd = WindowToButton(itemTmp)
      if itemTmp:GetCustomUserData(1) == index then
        wnd:SetStatus("selected")
        _Fri_CurrentSel = i
      else
        wnd:SetStatus("normal")
      end
    end
  end
end
function UI_Fri_FriendTypeChange(msg)
  for i = 0, _Fri_Friend_Index do
    local itemTmp = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChildByData1(i)
    if itemTmp then
      local wnd = WindowToButton(itemTmp)
      wnd:SetStatus("normal")
    end
  end
  _Fri_CurrentSel = -1
  local wnd = msg:get_window()
  if _Fri_Friend_Type == 0 then
    wnd:SetProperty("Text", "经典显示")
    _Fri_Friend_Type = 1
  else
    wnd:SetProperty("Text", "简化显示")
    _Fri_Friend_Type = 0
  end
  if _Fri_Friend_Type == 0 then
    gUI.Fri.ChannelList[1]:SetVisible(false)
    gUI.Fri.ChannelList[0]:SetVisible(true)
  else
    gUI.Fri.ChannelList[0]:SetVisible(false)
    gUI.Fri.ChannelList[1]:SetVisible(true)
  end
end
function UI_Fri_SearchClear(msg)
  gUI.Fri.ClearBtn:SetEnable(false)
  gUI.Fri.SearchBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
  _Fri_GetFriendGroupList()
end
function UI_Fri_DelBlack(msg)
  local list = WindowToListBox(gUI.Fri.BlackMember:GetGrandChild("Black_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("需要先选择一个玩家")
    return
  end
  local guid = item:GetUserData64()
  DeleteRelation(gUI_RELATION_TYPE.blacklist, guid)
end
function UI_Fri_DelEnemy(msg)
  local list = WindowToListBox(gUI.Fri.EnemyMember:GetGrandChild("Enemy_lbox"))
  local item = list:GetSelectedItem()
  if not item then
    ShowErrorMessage("需要先选择一个玩家")
    return
  end
  local guid = item:GetUserData64()
  Messagebox_Show("ENEMY_DEL_RELATION", gUI_RELATION_TYPE.enemy, guid)
end
function UI_Fri_DelFri(msg)
  local itemTmp = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChildByData1(_Fri_CurrentSel)
  if itemTmp then
    local guid = itemTmp:GetUserData64()
    local target_name, group_id = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    Messagebox_Show("FRIENDS_DEL_RELATION", gUI_RELATION_TYPE.friend, guid, target_name)
  else
    ShowErrorMessage("需要先选择一个玩家")
    return
  end
end
function Lua_Fri_UpdataChatGroup()
  local hasGroup = GetGroupInfo()
  if hasGroup then
    gUI.Fri.CreateChatGroupBtn:AddScriptEvent("wm_mouseclick", "UI_ChatC_ChatCBtnClick")
  else
    gUI.Fri.CreateChatGroupBtn:AddScriptEvent("wm_mouseclick", "UI_Fri_CreateTempBtn")
  end
end
function Lua_Fri_SetFoucsToWorld()
  local win = WindowSys_Instance:GetRootWindow()
  WindowSys_Instance:SetFocusWindow(win)
  WindowSys_Instance:SetKeyboardCaptureWindow(win)
end
function Lua_Fri_DelBlack(guid, name)
  DeleteRelation(gUI_RELATION_TYPE.blacklist, guid)
end
function Lua_Fri_ShowEnemyPos(guid, name)
  GetEnemyPos(guid)
end
function Lua_Fri_AddBlackByGUID(guid, name)
  AddBlackByGUID(guid)
end
function Lua_Fri_DelRelation(guid, name)
  Messagebox_Show("FRIENDS_DEL_RELATION", gUI_RELATION_TYPE.friend, guid, name)
end
function Lua_Fri_AddFriend(name, objId)
  if name then
    ApplyRelationByName(name)
  elseif objId ~= -1 then
    ApplyRelationByObjId(objId)
  end
end
function Lua_Fri_AddBlack(name, objId)
  AddToBlack(name)
end
function Lua_Fri_RequestDetail(guid, name)
  gUI.Fri.ReqestWinInfo = 1
  gUI.Fri.DetailInfoGuid = guid
  gUI.Fri.DetailInfoName = name
  RequestFriendDetailInfo(guid)
end
function _OnFri_AddRelation(relation_type, guid, level, name, region, job, index)
  if not gUI.Fri.Root:IsVisible() then
    return
  end
  if relation_type == gUI_RELATION_TYPE.friend then
    local friLevel, curPoint, maxPoint = GetFriendPointDetail(guid)
    _Fri_AddRelation(101, name, level, guid, job, region, friLevel, curPoint, maxPoint, index)
    _Fri_AddRelation(102, name, level, guid, job, region, friLevel, curPoint, maxPoint, index)
  elseif relation_type == gUI_RELATION_TYPE.blacklist then
    local _, _, job = GetRelationInfo(guid, gUI_RELATION_TYPE.blacklist)
    _Fri_AddRelation2(103, name, level, guid, job, region)
  elseif relation_type == gUI_RELATION_TYPE.enemy then
    local _, _, job = GetRelationInfo(guid, gUI_RELATION_TYPE.enemy)
    _Fri_AddRelation2(104, name, level, guid, job, region)
  end
end
function _OnFri_UpdateRelation(reType)
  if not gUI.Fri.Root:IsVisible() then
    return
  end
  local selected = gUI.Fri.ChannelBtn:GetSelectItem()
  if reType == gUI_RELATION_TYPE.friend then
    gUI.Fri.ChannelList[0]:ResetScrollBar()
    gUI.Fri.ChannelList[0]:SetAllChildVisible(false, 0, 101)
    gUI.Fri.ChannelList[0]:SizeSelf()
    gUI.Fri.ChannelList[1]:ResetScrollBar()
    gUI.Fri.ChannelList[1]:SetAllChildVisible(false, 0, 102)
    gUI.Fri.ChannelList[1]:SizeSelf()
    _Fri_CurrentSel = -1
    _Fri_GetFriendGroupList()
  elseif reType == gUI_RELATION_TYPE.enemy then
    local list = WindowToListBox(gUI.Fri.EnemyMember:GetGrandChild("Enemy_lbox"))
    list:RemoveAllItems()
    if selected == 1 then
      gUI.Fri.DelFri:SetEnable(false)
    end
    _Fri_GetEnemyGroupList()
  elseif reType == gUI_RELATION_TYPE.blacklist then
    local list = WindowToListBox(gUI.Fri.BlackMember:GetGrandChild("Black_lbox"))
    list:RemoveAllItems()
    if selected == 2 then
      gUI.Fri.FunctionBtn:SetEnable(false)
    end
    _Fri_GetBlackGroupList()
  end
end
function _OnFri_GetDetail(guid)
  local isOnline, headid, name, level, job, guild, _, _, _, secene, _, friPoint, lineId = GetFriendDetailInfo(guid)
  if gUI.Fri.ReqestWinInfo == 1 then
    _Fri_ShowInfoWnd(name, level, job, guild, secene, friPoint, isOnline)
    gUI.Fri.ReqestWinInfo = 0
  elseif gUI.Fri.GetDetailLock ~= nil then
    local hoverGuid = gUI.Fri.GetDetailLock:GetUserData64()
    if hoverGuid == guid then
      local tipwnd = gUI.Fri.GetDetailLock:GetCurrentTips()
      tipwnd = WindowToTooltip(tipwnd)
      if tipwnd and tipwnd:IsVisible() and tipwnd:GetTargetWindow() ~= gUI.Fri.GetDetailLock then
        return
      end
      local tooltip = gUI.Fri.GetDetailLock:GetToolTipWnd(0)
      Lua_Tip_FriendDetail(tooltip, name, level, job, guild, secene, friPoint, lineId)
      gUI.Fri.GetDetailLock = nil
      return
    end
  end
end
function _OnFri_AskAcceptApply(guid)
  local name, level, job = GetPlayerInfoByGUID(guid)
  for key, value in pairs(_FriAddList) do
    if value and value.Guid == guid then
      return
    end
  end
  table.insert(_FriAddList, 1, {
    Guid = guid,
    Name = name,
    Level = level,
    Job = job
  })
  WorldStage:playSoundByID(14)
end
function _OnFri_AskAddFriend(guid)
  local name, level, job = GetPlayerInfoByGUID(guid)
  Messagebox_Show("COMMUNITY_ASK_ADD_FRIEND", guid, name, level)
end
function _OnFri_ShowEnemyPos(name, mapName, x, y)
  local str = string.format("%s：%s", name, mapName)
  Lua_Chat_AddSysLog(str)
end
function _OnFri_AddFriendNotify()
end
function _OnFri_FriendOnline(online)
  if online then
    WorldStage:playSoundByID(16)
  end
  if gUI.Fri.Root:IsVisible() then
    RequestRelationList()
  end
  local wnd = WindowSys_Instance:GetWindow("ChatAddMember")
  if wnd:IsVisible() then
    Lua_ChatC_ShowNameListI()
  end
end
function Guide_OnFriAdd(keepSelect)
  gUI.Fri.Invite:SetVisible(true)
  local oldSelect, ScrollBarPos, ScrollBar
  if keepSelect == true then
    oldSelect = gUI.Fri.InviteList:GetSelectedItemIndex()
    ScrollBar = WindowToScrollBar(gUI.Fri.InviteList:GetChildByName("vertSB_"))
    if ScrollBar:IsVisible() then
      ScrollBarPos = ScrollBar:GetCurrentPos()
    end
  end
  local oldCount = gUI.Fri.InviteList:GetItemCount()
  gUI.Fri.InviteList:RemoveAllItems()
  for key, value in pairs(_FriAddList) do
    if value then
      local str = string.format("%s|%d|%s", value.Name, value.Level, gUI_MenPaiName[value.Job])
      local item = gUI.Fri.InviteList:InsertString(str, -1)
      item:SetUserData64(value.Guid)
    end
  end
  local newCount = gUI.Fri.InviteList:GetItemCount()
  if keepSelect == true and oldSelect >= 0 then
    gUI.Fri.InviteList:SetSelectItem(oldSelect + newCount - oldCount)
    if ScrollBarPos and ScrollBar and ScrollBar:IsVisible() then
      ScrollBar:SetCurrentPos(ScrollBarPos)
    end
  else
    gUI.Fri.InviteList:SetSelectItem(0)
  end
end
function UI_Fri_Guide_AddFri_Close()
  gUI.Fri.Invite:SetVisible(false)
end
function _Fri_Guide_SetItemNilByGuid(guid)
  for key, value in pairs(_FriAddList) do
    if value and value.Guid == guid then
      value = nil
      return
    end
  end
end
function UI_Fri_Guide_AddFri_Refuse()
  local selItem = gUI.Fri.InviteList:GetSelectedItem()
  if selItem then
    local guid = selItem:GetUserData64()
    AcceptOrRefuseApply(false, guid)
    gUI.Fri.InviteList:RemoveItemByData64(guid)
    _Fri_Guide_SetItemNilByGuid(guid)
  end
  if gUI.Fri.InviteList:GetItemCount() == 0 then
    _FriAddList = {}
    UI_Fri_Guide_AddFri_Close()
    DeleteHint(USER_GUIDE_ADDFRI_UI)
  end
end
function UI_Fri_Guide_AddFri_Accept()
  local selItem = gUI.Fri.InviteList:GetSelectedItem()
  if selItem then
    local guid = selItem:GetUserData64()
    AcceptOrRefuseApply(true, guid)
    gUI.Fri.InviteList:RemoveItemByData64(guid)
    _Fri_Guide_SetItemNilByGuid(guid)
  end
  if gUI.Fri.InviteList:GetItemCount() == 0 then
    _FriAddList = {}
    UI_Fri_Guide_AddFri_Close()
    DeleteHint(USER_GUIDE_ADDFRI_UI)
  end
end
function UI_Fri_Guide_AddFri_AllAccept()
  for key, value in pairs(_FriAddList) do
    if value then
      AcceptOrRefuseApply(true, value.Guid)
    end
  end
  gUI.Fri.InviteList:RemoveAllItems()
  _FriAddList = {}
  UI_Fri_Guide_AddFri_Close()
  DeleteHint(USER_GUIDE_ADDFRI_UI)
end
function _OnFri_Guide_HintUpdate(guideID)
  if guideID == USER_GUIDE_ADDFRI_UI and gUI.Fri.Invite:IsVisible() then
    Guide_OnFriAdd(true)
  end
end
function _OnFri_HomeLandUpdate()
  local showHome = IsActiveHome()
  if showHome then
    gUI.Fri.HomeLandBtn:SetEnable(true)
  else
    gUI.Fri.HomeLandBtn:SetEnable(false)
  end
end
function _OnFri_Update_Wrath()
  gUI.Fri.WrathList:RemoveAllItems()
  local wrathList = GetWrathList()
  local beWrath = 0
  local wrath = 0
  if wrathList[0] == nil then
    local strbewrath = string.format("当月被天诛总次数%s次", beWrath)
    local strwrath = string.format("当月天诛总次数%s次", wrath)
    gUI.Fri.AllWrath:SetProperty("Text", strwrath)
    gUI.Fri.AllBeWrath:SetProperty("Text", strbewrath)
    return
  end
  for i = 0, table.maxn(wrathList) do
    local str = string.format("%s|%d|%d", wrathList[i].Name, wrathList[i].BeWrath, wrathList[i].Wrath)
    local item = gUI.Fri.WrathList:InsertString(str, -1)
    gUI.Fri.WrathList:SetProperty("TextHorzAlign", "Left")
    gUI.Fri.WrathList:SetProperty("TextHorzAlign", "HCenter")
    beWrath = beWrath + wrathList[i].BeWrath
    wrath = wrath + wrathList[i].Wrath
  end
  local strbewrath = string.format("当月被天诛总次数%s次", beWrath)
  local strwrath = string.format("当月天诛总次数%s次", wrath)
  gUI.Fri.AllWrath:SetProperty("Text", strwrath)
  gUI.Fri.AllBeWrath:SetProperty("Text", strbewrath)
end
function _OnFri_Update_Relation_Point(targetGuid, friLv, curFriExp, maxFriExp)
  if _Fri_Friend_Type == 0 then
    local qCount = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChildCount()
    for i = 0, qCount - 1 do
      local itemTmp = gUI.Fri.ChannelList[_Fri_Friend_Type]:GetChild(i)
      local guid = itemTmp:GetUserData64()
      if guid == targetGuid then
        local FriLvSlab = itemTmp:GetChildByName("FriendLev_dlab")
        local progress = WindowToProgressBar(itemTmp:GetChildByName("ProgBar_pbar"))
        FriLvSlab:SetProperty("Text", "好友度：" .. tostring(friLv) .. "级")
        local rate = curFriExp / maxFriExp
        progress:SetProgress(rate)
      end
    end
  end
end
function _OnFri_CancelDel_Fri(deletGuid)
  local qCount = gUI.Fri.ChannelList[0]:GetChildCount()
  for i = 0, qCount - 1 do
    local itemTmp = gUI.Fri.ChannelList[0]:GetChild(i)
    local guid = itemTmp:GetUserData64()
    if guid == deletGuid then
      local delBtn = itemTmp:GetChildByName("Back_btn")
      delBtn:SetVisible(false)
      local FriLvSlab = itemTmp:GetChildByName("FriendLev_dlab")
      local progress = WindowToProgressBar(itemTmp:GetChildByName("ProgBar_pbar"))
      progress:SetVisible(true)
      FriLvSlab:SetVisible(true)
      local itemTmp2 = gUI.Fri.ChannelList[1]:GetChild(i)
      local delBtn2 = itemTmp2:GetChildByName("Back_btn")
      delBtn2:SetVisible(false)
    end
  end
end
function _OnFri_WaitDel_Fri(deletGuid)
  local qCount = gUI.Fri.ChannelList[0]:GetChildCount()
  for i = 0, qCount - 1 do
    local itemTmp = gUI.Fri.ChannelList[0]:GetChild(i)
    local guid = itemTmp:GetUserData64()
    if guid == deletGuid then
      local _, _, _, _, _, _, _, _, delTime = GetFriendDetailInfo(guid)
      local bShowDelBtn = delTime > 0
      local delBtn = itemTmp:GetChildByName("Back_btn")
      delBtn:SetProperty("Text", string.format("恢复（剩余%d小时）", delTime))
      delBtn:SetVisible(bShowDelBtn)
      local FriLvSlab = itemTmp:GetChildByName("FriendLev_dlab")
      local progress = WindowToProgressBar(itemTmp:GetChildByName("ProgBar_pbar"))
      progress:SetVisible(not bShowDelBtn)
      FriLvSlab:SetVisible(not bShowDelBtn)
      local itemTmp2 = gUI.Fri.ChannelList[1]:GetChild(i)
      local delBtn2 = itemTmp2:GetChildByName("Back_btn")
      delBtn2:SetVisible(bShowDelBtn)
    end
  end
end
function Script_Fri_OnEvent(event)
  if event == "COMMUNITY_UPDATE_RELATION" then
    _OnFri_UpdateRelation(arg1, arg2)
  elseif event == "COMMUNITY_ADD_RELATION" then
    _OnFri_AddRelation(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "COMMUNITY_DEL_RELATION" then
    _OnFri_UpdateRelation(arg1, arg2)
  elseif event == "COMMUNITY_FRIEND_DETAIL" then
    _OnFri_GetDetail(arg1)
  elseif event == "COMMUNITY_ASK_ACCEPT_APPLY" then
    _OnFri_AskAcceptApply(arg1)
  elseif event == "COMMUNITY_ASK_ADD_FRIEND" then
    _OnFri_AskAddFriend(arg1)
  elseif event == "COMMUNITY_SHOW_ENEMY_POS" then
    _OnFri_ShowEnemyPos(arg1, arg2, arg3, arg4)
  elseif event == "COMMUNITY_ADD_FRIEND_NOTIFY" then
    _OnFri_AddFriendNotify()
  elseif event == "COMMUNITY_FRIEND_ONLINE" then
    _OnFri_FriendOnline(arg1)
  elseif event == "GUIDE_HINT_UPDATE_UI" then
    _OnFri_Guide_HintUpdate(arg1)
  elseif event == "HOME_UPDATE_FRI_LAND" then
    _OnFri_HomeLandUpdate(arge1)
  elseif event == "COMMUNITY_UPDATE_WRATHLIST" then
    _OnFri_Update_Wrath()
  elseif event == "COMMUNITY_RELATION_POINT_UPDATE" then
    _OnFri_Update_Relation_Point(arg1, arg2, arg3, arg4)
  elseif event == "COMMUNITY_CANCELDEL_FRIEND" then
    _OnFri_CancelDel_Fri(arg1)
  elseif event == "COMMUNITY_WAITDEL_FRIEND" then
    _OnFri_WaitDel_Fri(arg1)
  end
end
function Script_Fri_OnLoad()
  gUI.Fri.Root = WindowSys_Instance:GetWindow("Friends")
  gUI.Fri.Root:SetProperty("Visible", "false")
  gUI.Fri.FunctionBtn = WindowToButton(WindowSys_Instance:GetWindow("Friends.Function_btn"))
  gUI.Fri.Module = WindowSys_Instance:GetWindow("FriendsModule")
  gUI.Fri.AddFriI = WindowSys_Instance:GetWindow("FriendsModule.AddFriend_bg")
  gUI.Fri.TempChatCBox = WindowSys_Instance:GetWindow("FriendsModule.CreateColony_bg.Pic1_bg.EditBox_ebox")
  gUI.Fri.TempChatCI = WindowSys_Instance:GetWindow("FriendsModule.CreateColony_bg")
  gUI.Fri.FindInptNameBox = WindowToEditBox(WindowSys_Instance:GetWindow("FriendsModule.AddFriend_bg.Pic1_bg.EditBox_ebox"))
  gUI.Fri.DetailInfo = WindowSys_Instance:GetWindow("FriendsInfo")
  gUI.Fri.SearchBox = WindowSys_Instance:GetWindow("Friends.Pic1_bg.Search_ebox")
  gUI.Fri.CreateChatGroupBtn = WindowSys_Instance:GetWindow("Friends.CreateGroup_btn")
  gUI.Fri.AddEnemy = WindowSys_Instance:GetWindow("FriendsModule.AddEnemy_bg")
  gUI.Fri.EnemyInptNameBox = WindowToEditBox(WindowSys_Instance:GetWindow("FriendsModule.AddEnemy_bg.Pic1_bg.EditBox_ebox"))
  gUI.Fri.DelFri = WindowSys_Instance:GetWindow("Friends.DelFri_btn")
  local template = WindowSys_Instance:GetWindow("Friends.Pic2_bg.Container_cnt")
  local templateRoot = WindowSys_Instance:GetWindow("Friends.Pic2_bg")
  if gUI.Fri.ChannelList ~= nil then
    for i = 0, 1 do
      gUI.Fri.ChannelList[i]:DeleteAll()
    end
  else
    gUI.Fri.ChannelList = {}
    for i = 0, 1 do
      gUI.Fri.ChannelList[i] = WindowToContainer(WindowSys_Instance:CreateWndFromTemplate(template, templateRoot))
      gUI.Fri.ChannelList[i]:SetVisible(true)
    end
  end
  gUI.Fri.templateItem = WindowSys_Instance:GetWindow("Friends.Template_btn1")
  gUI.Fri.templateItem2 = WindowSys_Instance:GetWindow("Friends.Template_btn2")
  gUI.Fri.ChannelBtn = WindowToTableCtrl(WindowSys_Instance:GetWindow("Friends.TableCtrl_tctl"))
  gUI.Fri.EnemyMember = WindowSys_Instance:GetWindow("Friends.Pic3_bg")
  gUI.Fri.BlackMember = WindowSys_Instance:GetWindow("Friends.Pic4_bg")
  gUI.Fri.WrathMember = WindowSys_Instance:GetWindow("Friends.Pic5_bg")
  gUI.Fri.WrathList = WindowToListBox(WindowSys_Instance:GetWindow("Friends.Pic5_bg.Punish_lbox"))
  gUI.Fri.AllWrath = WindowSys_Instance:GetWindow("Friends.Pic5_bg.PunishAll_dlab")
  gUI.Fri.AllBeWrath = WindowSys_Instance:GetWindow("Friends.Pic5_bg.All_dlab")
  gUI.Fri.TypeBtns = {}
  gUI.Fri.TypeBtns[0] = WindowToButton(WindowSys_Instance:GetWindow("Friends.Page_bg.PageFri_btn"))
  gUI.Fri.TypeBtns[1] = WindowToButton(WindowSys_Instance:GetWindow("Friends.Page_bg.PageFoe_btn"))
  gUI.Fri.TypeBtns[2] = WindowToButton(WindowSys_Instance:GetWindow("Friends.Page_bg.PageHide_btn"))
  gUI.Fri.TypeBtns[3] = WindowToButton(WindowSys_Instance:GetWindow("Friends.Page_bg.PagePunish_btn"))
  gUI.Fri.modifyId = -1
  gUI.Fri.FriList = nil
  gUI.Fri.FriGroup = {}
  gUI.Fri.GetDetailLock = nil
  gUI.Fri.DetailInfoGuid = 0
  gUI.Fri.DetailInfoName = 0
  gUI.Fri.ReqestWinInfo = 0
  gUI.Fri.CreateTempGroupName = ""
  gUI.Fri.Invite = WindowSys_Instance:GetWindow("FriendsInvite")
  gUI.Fri.InviteList = WindowToListBox(WindowSys_Instance:GetWindow("FriendsInvite.Invite_bg.List_bg.List_lbox"))
  _FriAddList = {}
  gUI.Fri.Search = WindowSys_Instance:GetWindow("Friends.Pic1_bg")
  gUI.Fri.FriendType = WindowSys_Instance:GetWindow("Friends.Pic1_bg.Simple_btn")
  gUI.Fri.ClearBtn = WindowSys_Instance:GetWindow("Friends.Pic1_bg.Clear_btn")
  gUI.Fri.FriendType:SetProperty("Text", "简化显示")
  gUI.Fri.ClearBtn:SetEnable(false)
  gUI.Fri.SearchBox:SetProperty("Text", "")
  _Fri_CurrentSel = -1
  gUI.Fri.HomeLandBtn = WindowSys_Instance:GetWindow("Friends.Home_btn")
  _Fri_Friend_Type = 0
  gUI.Fri.UpdateTime = TimerSys_Instance:CreateTimerObject("FriUpdateTime", 1, -1, "_Fri_UpdateTime", false, false)
end
function Script_Fri_OnHide()
end

