if gUI and not gUI.GemCompose then
  gUI.GemCompose = {}
end
GEMCOMPOSESLOTCOMP = {EQUIPSLOT = 0, MATERIALSLOT = 1}
function _GemCompose_Show()
  gUI.GemCompose.Root:SetProperty("Visible", "true")
  Lua_Bag_ShowUI(true)
end
function _GemCompose_Close()
  gUI.GemCompose.Root:SetProperty("Visible", "false")
end
function UI_GemCompose_Close()
  GemCompose_RomoveAllItem()
  _GemCompose_Close()
end
function _GemCompose_ClearUI()
  gUI.GemCompose.MainGemBox:ClearGoodsItem(0)
  gUI.GemCompose.SubGemBox:ClearGoodsItem(0)
  gUI.GemCompose.PrevGemBox:ClearGoodsItem(0)
  _GemCompose_SetMoney(0, 0)
end
function _GemCompose_SetMoney(money, sprite)
  if sprite < 0 then
    sprite = 0
  end
  if money < 0 then
    money = 0
  end
  gUI.GemCompose.CostLab:SetProperty("Text", Lua_UI_Money2String(money))
  gUI.GemCompose.SpriteLab:SetProperty("Text", tostring(sprite))
end
function GemCompose_RomoveAllItem()
  SetGemCompNeedItems(1, 1, OperateMode.Cancel, GEMCOMPOSESLOTCOMP.EQUIPSLOT)
  _GemCompose_ClearUI()
end
function _OnGemCompose_Show()
  _GemCompose_ClearUI()
  _GemCompose_Show()
end
function GemCompose_SendAddGemToServer(srcBag, srcSlot)
  if srcBag == gUI_GOODSBOX_BAG.backpack0 then
    SetGemCompNeedItems(srcBag, srcSlot, OperateMode.Add, GEMCOMPOSESLOTCOMP.EQUIPSLOT)
  end
end
function GemCompose_SendAddMaterToServer(srcBag, srcSlot)
end
function _OnGemCompose_AddItem(index, bag, slot, icon)
  if index == 0 then
    gUI.GemCompose.MainGemBox:SetItemGoods(0, icon, Lua_Bag_GetQuality(bag, slot))
    local perItemId, perItemIcon, needItemNums, hasNums, needMoney, needsprite, needItemID1, needItemID2, nums1, IconMater, MaterCount = GetComposeInfo()
    if perItemId then
      gUI.GemCompose.PrevGemBox:SetItemGoods(0, perItemIcon, Lua_Bag_GetQuality(-1, perItemId))
      gUI.GemCompose.MainGemBox:SetItemSubscript(0, tostring(hasNums) .. "/" .. tostring(needItemNums))
      _GemCompose_SetMoney(needMoney, needsprite)
      gUI.GemCompose.SubGemBox:SetItemGoods(0, IconMater, Lua_Bag_GetQuality(-1, needItemID2))
      gUI.GemCompose.SubGemBox:SetItemSubscript(0, tostring(MaterCount) .. "/" .. tostring(nums1))
    end
  elseif index == 1 then
  end
end
function _OnGemCompose_Del(index)
  if index == GEMCOMPOSESLOTCOMP.EQUIPSLOT then
    _GemCompose_ClearUI()
  elseif index == GEMCOMPOSESLOTCOMP.MATERIALSLOT then
    gUI.GemCompose.SubGemBox:ClearGoodsItem(0)
  end
end
function UI_GemCompose_Comfirm(msg)
  if not gUI.GemCompose.MainGemBox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("GEMCOMPOSE_TAKEGEM_FIRST")
  else
    DoUpradeItem()
  end
end
function UI_GemComp_GemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local toName = to_win:GetProperty("WindowName")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    if toName == "GoodsBox1_gbox" then
      GemCompose_SendAddGemToServer(gUI_GOODSBOX_BAG[from_type], slot)
    elseif toName == "GoodsBox3_gbox" then
      GemCompose_SendAddMaterToServer(gUI_GOODSBOX_BAG[from_type], slot)
    end
  end
end
function UI_GemCompose_ItemRClick(msg)
  local toName = msg:get_window():GetProperty("WindowName")
  if toName == "GoodsBox1_gbox" then
    SetGemCompNeedItems(1, 1, OperateMode.Remove, GEMCOMPOSESLOTCOMP.EQUIPSLOT)
  elseif toName == "GoodsBox3_gbox" then
  end
end
function _OnGemComposeCloseOrder()
  GemCompose_RomoveAllItem()
  _GemCompose_Close()
end
function UI_GemCompose_ShowGemTooltip(msg)
  local tooltip = msg:get_window():GetToolTipWnd(0)
  local toName = msg:get_window():GetProperty("WindowName")
  if toName == "GoodsBox1_gbox" and gUI.GemCompose.MainGemBox:IsItemHasIcon(0) then
    local name, bag, slot = GetGemSacrifyInfo(GEMCOMPOSESLOTCOMP.EQUIPSLOT)
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  elseif toName == "GoodsBox3_gbox" and gUI.GemCompose.SubGemBox:IsItemHasIcon(0) then
    local perItemId, perItemIcon, needItemNums, hasNums, needMoney, needsprite, needItemID1, needItemID2 = GetComposeInfo()
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, needItemID1)
  elseif toName == "GoodsBox2_gbox" and gUI.GemCompose.PrevGemBox:IsItemHasIcon(0) then
    local perItemId, perItemIcon, needItemNums, hasNums, needMoney, needsprite, needItemID1, needItemID2 = GetComposeInfo()
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, perItemId)
  end
end
function _OnGemCompose_Result()
  Lua_Chat_ShowOSD("GEM_BLEND_SUCC")
  _OnUpdateGemItems()
  WorldStage:playSoundByID(8)
end
function _OnUpdateGemItems()
  local perItemId, perItemIcon, needItemNums, hasNums, needMoney, needsprite, needItemID1, needItemID2, _, _, _, Icon = GetComposeInfo()
  if Icon then
    _OnGemCompose_AddItem(0, 0, 0, Icon)
  else
    _GemCompose_ClearUI()
  end
end
function Script_GemCompose_OnEvent(event)
end
function Script_GemCompose_OnHide()
  _OnGemComposeCloseOrder()
end
function Script_GemCompose_OnLoad()
  gUI.GemCompose.Root = WindowSys_Instance:GetWindow("GemCompose")
  gUI.GemCompose.MainGemBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemCompose.Picture2_bg.GoodsBox1_gbox"))
  gUI.GemCompose.SubGemBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemCompose.Picture2_bg.GoodsBox3_gbox"))
  gUI.GemCompose.PrevGemBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemCompose.Picture2_bg.GoodsBox2_gbox"))
  gUI.GemCompose.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("GemCompose.Label4_slab.Label2_dlab"))
  gUI.GemCompose.SpriteLab = WindowToLabel(WindowSys_Instance:GetWindow("GemCompose.Label4_slab.Label4_dlab"))
end
