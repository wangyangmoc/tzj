if gUI and not gUI.GemEx then
  gUI.GemEx = {}
end
local GEMSLEECT = -1
function _GemEx_ClearUI()
  gUI.GemEx.EquipBox:ClearGoodsItem(0)
  gUI.GemEx.MaterBox:ClearGoodsItem(0)
  for i = 0, 2 do
    gUI.GemEx.GemBox[i]:ClearGoodsItem(0)
  end
  _GemEx_ClearComsumeMoney()
end
function _GemEx_ShowComsumeMoney()
  local money = GetSplitMoney()
  _GemEx_ShowMoney(money)
end
function _GemEx_ClearComsumeMoney()
  _GemEx_ShowMoney(0)
end
function _GemEx_ShowMoney(money)
  win = WindowSys_Instance:GetWindow("GemExtraction.Picture4_bg.Label1_dlab")
  win:SetProperty("Text", Lua_UI_Money2String(money))
end
function UI_GemEx_ItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "myequipbox" then
    return
  end
  if from_win:GetProperty("BoxType") == "skill1" then
    return
  end
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    SetSplitItem(gUI_GOODSBOX_BAG[from_type], slot, 0)
    local name, icon = GetEquipGemInfo(gUI_GOODSBOX_BAG[from_type], slot, 0)
  end
end
function UI_GemEx_Close()
  RemoveSplitItem()
  _GemEx_ClearUI()
  if gUI.GemEx.Root:IsVisible() then
    gUI.GemEx.Root:SetProperty("Visible", "false")
  end
end
function UI_GemEx_HelpClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GemExtraction.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_GemEx_Comfirm()
  local flag = false
  local _, bag, slot, isBind = GetEquipExtractionInfo()
  if isBind == nil then
    Lua_Chat_ShowOSD("GEM_EXTRAC_NOEQUIP")
    return
  end
  if GEMSLEECT ~= -1 then
    SplitGem(GEMSLEECT)
  else
    Lua_Chat_ShowOSD("GEM_EXTRAC_CANEX")
    return
  end
end
function UI_GemEx_Cancel()
  RemoveSplitItem()
  _GemEx_ClearUI()
end
function UI_GemEx_EquipRC()
  RemoveSplitItem()
  _GemEx_ClearUI()
end
function UI_GemEx_ShowEquipTooltip(msg)
  local _, bag, slot = GetEquipExtractionInfo()
  local tooltip = gUI.GemEx.EquipBox:GetToolTipWnd(0)
  Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
  Lua_Tip_ShowEquipCompare(gUI.GemEx.EquipBox, bag, slot)
end
function UI_GemEx_ShowGemTooltip(msg)
  local goodsbox = msg:get_window()
  goodsbox = WindowToGoodsBox(goodsbox)
  if goodsbox:GetItemIcon(0) ~= "" then
    local tooltip = goodsbox:GetToolTipWnd(0)
    local slot = goodsbox:GetItemData(0)
    local itemid = goodsbox:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemid, nil, nil, nil, true)
  end
end
function _OnGemEx_Show()
  gUI.GemEx.Root:SetProperty("Visible", "true")
  Lua_Bag_ShowUI(true)
  for i = 0, 2 do
    gUI.GemEx.GemBox[i]:SetVisible(false)
  end
end
function _OnGemEx_Updata(typeId, slot)
  if typeId == 1 then
    local icon, bag, slot = GetEquipExtractionInfo()
    local attrName, value, rate, gemIcon, gemName = GetGemInfoBySlot(bag, slot, 0)
    gUI.GemEx.EquipBox:SetItemGoods(0, icon, Lua_Bag_GetQuality(bag, slot))
    GemId0, GemIcon0, GemType0, GemValue0, GemName0, GemNa0, GemId1, GemIcon1, GemType1, GemValue1, GemName1, GemNa1, GemId2, GemIcon2, GemType2, GemValue2, GemName2, GemNa2 = GetEquipHasGemInfo(1)
    for i = 0, 2 do
      gUI.GemEx.GemBox[i]:SetVisible(false)
    end
    if GemId2 ~= 0 then
      gUI.GemEx.GemBox[2]:SetItemGoods(0, GemIcon2, Lua_Bag_GetQuality(-1, GemId2))
      gUI.GemEx.GemBox[2]:ClearSelectedItem()
      gUI.GemEx.GemBox[2]:SetCustomUserData(0, GemId2)
      gUI.GemEx.GemBox[2]:SetVisible(true)
      GEMSLEECT = 2
    end
    if GemId1 ~= 0 then
      gUI.GemEx.GemBox[1]:SetItemGoods(0, GemIcon1, Lua_Bag_GetQuality(-1, GemId1))
      gUI.GemEx.GemBox[1]:ClearSelectedItem()
      gUI.GemEx.GemBox[1]:SetCustomUserData(0, GemId1)
      gUI.GemEx.GemBox[1]:SetVisible(true)
      GEMSLEECT = 1
    end
    if GemId0 ~= 0 then
      gUI.GemEx.GemBox[0]:SetItemGoods(0, GemIcon0, Lua_Bag_GetQuality(-1, GemId0))
      gUI.GemEx.GemBox[0]:ClearSelectedItem()
      gUI.GemEx.GemBox[0]:SetCustomUserData(0, GemId0)
      gUI.GemEx.GemBox[0]:SetVisible(true)
      GEMSLEECT = 0
    end
    gUI.GemEx.GemBox[GEMSLEECT]:SetSelectedItem(0)
    UI_GemEx_SetSelectItem()
  elseif typeId == 2 then
    _GemEx_ClearUI()
    GEMSLEECT = -1
    for i = 0, 2 do
      gUI.GemEx.GemBox[i]:ClearSelectedItem()
    end
  end
end
function UI_SelectItem_Click(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_name = to_win:GetProperty("WindowName")
  for i = 0, 2 do
    gUI.GemEx.GemBox[i]:ClearSelectedItem()
  end
  if to_name == "GoodsBox2_gbox" then
    gUI.GemEx.GemBox[0]:SetSelectedItem(0)
    GEMSLEECT = 0
  elseif to_name == "GoodsBox3_gbox" then
    gUI.GemEx.GemBox[1]:SetSelectedItem(0)
    GEMSLEECT = 1
  elseif to_name == "GoodsBox4_gbox" then
    gUI.GemEx.GemBox[2]:SetSelectedItem(0)
    GEMSLEECT = 2
  end
  if GEMSLEECT == -1 then
    return
  end
  UI_GemEx_SetSelectItem()
end
function UI_GemEx_SetSelectItem()
  if GEMSLEECT == -1 then
    return
  end
  if gUI.GemEx.GemBox[GEMSLEECT]:IsItemHasIcon(0) then
    local gemid = gUI.GemEx.GemBox[GEMSLEECT]:GetCustomUserData(0)
    local money, materialId, materialNum, materialToNum, materialIcon = GetGemExtarInfos(gemid)
    gUI.GemEx.MaterBox:SetItemGoods(0, materialIcon, Lua_Bag_GetQuality(-1, gemid))
    gUI.GemEx.MaterBox:SetItemSubscript(0, tostring(materialToNum) .. "/" .. tostring(materialNum))
    gUI.GemEx.MaterBox:SetCustomUserData(0, materialId)
    _GemEx_ShowMoney(money)
  end
end
function UI_GemEx_CheckBtnClick(msg)
  if msg == nil then
    return
  end
  local checkBtn = msg:get_window()
end
function _OnGemEx_Result()
  UI_GemEx_Cancel()
  Lua_Chat_ShowOSD("GEM_EXTRAC_SUCC")
  WorldStage:playSoundByID(7)
end
function _OnAddItem()
  UI_GemEx_SetSelectItem()
end
function _OnGemEx_Close()
  UI_GemEx_Close()
end
function Script_GemEx_OnLoad()
  gUI.GemEx.Root = WindowSys_Instance:GetWindow("GemExtraction")
  gUI.GemEx.EquipBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemExtraction.GoodsBox1_gbox"))
  gUI.GemEx.MaterBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemExtraction.Picture6.GoodsBox1_gbox"))
  gUI.GemEx.GemBox = {}
  gUI.GemEx.GemBox[0] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemExtraction.GoodsBox2_gbox"))
  gUI.GemEx.GemBox[1] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemExtraction.GoodsBox3_gbox"))
  gUI.GemEx.GemBox[2] = WindowToGoodsBox(WindowSys_Instance:GetWindow("GemExtraction.GoodsBox4_gbox"))
  gUI.GemEx.GemCheckBtn = {}
  gUI.GemEx.GemCheckBtn[0] = WindowToCheckButton(WindowSys_Instance:GetWindow("GemExtraction.Check_bg.CheckButton1_cbtn"))
  gUI.GemEx.GemCheckBtn[1] = WindowToCheckButton(WindowSys_Instance:GetWindow("GemExtraction.Check_bg.CheckButton2_cbtn"))
  gUI.GemEx.GemCheckBtn[2] = WindowToCheckButton(WindowSys_Instance:GetWindow("GemExtraction.Check_bg.CheckButton3_cbtn"))
  gUI.GemEx.CheckBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("GemExtraction.CheckButton1_cbtn"))
  _GemEx_ClearComsumeMoney()
  _GemEx_ClearUI()
end
function Script_GemEx_OnHide()
  UI_GemEx_Close()
  GEMSLEECT = -1
end
function Script_GemEx_OnEvent(event)
end
