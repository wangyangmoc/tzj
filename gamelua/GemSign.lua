if gUI and not gUI.SignItem then
  gUI.SignItem = {}
end
GEMSIGN_STATE = {Sign = 0, UnSign = 1}
SIGN_SLOT_COLLATE = {MAINSLOT = 0}
function On_SignItem_OpenPanel()
  if gUI.SignItem.Root:IsVisible() then
    gUI.SignItem.Root:SetVisible(false)
  else
    gUI.SignItem.Root:SetVisible(true)
    SignItem_InitData()
    SignItem_Guide()
  end
end
function SignItem_SignGetCurrMode()
  if gUI.SignItem.SignCtrl:GetSelectItem() == GEMSIGN_STATE.Sign then
    return GEMSIGN_STATE.Sign
  else
    return GEMSIGN_STATE.UnSign
  end
end
function SignItem_InitData()
  gUI.SignItem.SignCtrl:SetSelectedState(GEMSIGN_STATE.Sign)
  SignItem_SignRefreshUI()
end
function SignItem_Guide()
  UserGuideUpdateCustomData(21)
  local wnd = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Signature_btn.SignEff_bg")
  wnd:SetVisible(false)
end
function SignItem_SignRefreshUI()
  local mode = SignItem_SignGetCurrMode()
  if mode == GEMSIGN_STATE.Sign then
    gUI.SignItem.OkBtn:SetProperty("Text", "签名")
    gUI.SignItem.SignLabelText:SetProperty("Text", "道具签名介绍：\n1.可以对装备和大于等于3级的神玉进行签名。\n2.道具签名后变不可转移状态、不能销售、摧毁。\n3.道具签名不收取任何费用。\n4.每件装备签名后会增加10点体质奖励，神玉签名没有奖励。\n5.可以通过取消签名，解除道具的不可转移状态。\n6.取消签名状态中的道具可以随时重新签名。")
    gUI.SignItem.SignLabelInfo:SetProperty("Text", "请放入需要签名的道具")
  elseif mode == GEMSIGN_STATE.UnSign then
    gUI.SignItem.OkBtn:SetProperty("Text", "解签")
    gUI.SignItem.SignLabelText:SetProperty("Text", "道具取消签名介绍：\n1.取消签名需要等待48小时。\n2.取消签名不会收取任何费用。\n3.取消签名后装备可以转移。\n4.取消签名后，装备附加的10点体质也将取消。")
    gUI.SignItem.SignLabelInfo:SetProperty("Text", "请放入需要取消签名的道具")
  end
end
function SignItem_PlaceSlot(from_win, slot)
  local mode = SignItem_SignGetCurrMode()
  slot = Lua_Bag_GetRealSlot(from_win, slot)
  local canSign, bSigned, bUnsigned, unsignTime, nType = GetEquipSignInfo(1, slot, 0)
  if canSign == nil then
    Lua_Chat_ShowOSD("SIGN_NOT_EQUIP")
    return
  end
  if mode == GEMSIGN_STATE.Sign then
    if canSign == false then
      if nType == 0 then
        Lua_Chat_ShowOSD("SIGN_CANNOT")
        return
      elseif nType == 1 then
        Lua_Chat_ShowOSD("SIGN_CANNOTGEM")
        return
      end
    end
    if bSigned == true and bUnsigned == false then
      if nType == 0 then
        Lua_Chat_ShowOSD("SIGN_SIGNED")
        return
      elseif nType == 1 then
        Lua_Chat_ShowOSD("SIGN_SIGNEDGEM")
        return
      end
    end
  elseif mode == GEMSIGN_STATE.UnSign then
    if bSigned == false then
      if nType == 0 then
        Lua_Chat_ShowOSD("SIGN_UNSIGN")
        return
      elseif nType == 1 then
        Lua_Chat_ShowOSD("SIGN_UNSIGNGEM")
        return
      end
    end
    if bUnsigned == true then
      if nType == 0 then
        Lua_Chat_ShowOSD("SIGN_UNSIGNED")
        return
      elseif nType == 1 then
        Lua_Chat_ShowOSD("SIGN_UNSIGNEDGEM")
        return
      end
    end
  end
  SignItem_AddItem(gUI_GOODSBOX_BAG.backpack0, slot)
end
function On_SignItem_UpdateItem()
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_ITEM_SIGN, SIGN_SLOT_COLLATE.MAINSLOT)
  if not slot then
    gUI.SignItem.SignGoodsBox:ClearGoodsItem(0)
    gUI.SignItem.SignGoodsBox:SetItemBindState(0, 0)
  else
    local _, icon, quality = GetItemInfoBySlot(bag, slot, 0)
    if not icon then
      return
    end
    gUI.SignItem.SignGoodsBox:SetItemGoods(0, icon, quality)
    local bindinfo = Lua_Bag_GetBindInfo(bag, slot)
    gUI.SignItem.SignGoodsBox:SetItemBindState(0, bindinfo)
  end
end
function SignItem_AddItem(bag, slot)
  SetNpcFunctionEquip(bag, slot, OperateMode.Add, SIGN_SLOT_COLLATE.MAINSLOT, NpcFunction.NPC_FUNC_ITEM_SIGN)
end
function SignItem_DelItem()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, 0, OperateMode.Remove, SIGN_SLOT_COLLATE.MAINSLOT, NpcFunction.NPC_FUNC_ITEM_SIGN)
end
function SignItem_CancelItem()
  SetNpcFunctionEquip(gUI_GOODSBOX_BAG.backpack0, 0, OperateMode.Cancel, GEMINEFF_SLOT_COLLATE.MAINGEMSLOT, NpcFunction.NPC_FUNC_ITEM_SIGN)
end
function UI_DargItemToSignItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack0" then
    slot = Lua_Bag_GetRealSlot(from_win, slot)
    SignItem_PlaceSlot(from_win, slot)
  end
end
function UI_SignItemRClick(msg)
  SignItem_CancelItem()
end
function UI_SafeSetup_OkSign(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_ITEM_SIGN, SIGN_SLOT_COLLATE.MAINSLOT)
  DoSignEquip(SignItem_SignGetCurrMode())
end
function UI_Tip_SignItem(msg)
  local bag, slot = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_ITEM_SIGN, SIGN_SLOT_COLLATE.MAINSLOT)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if slot == nil then
    return
  end
  Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
end
function OnSignItem_Sign_Res(ntype, bag, slot)
  SignItem_CancelItem()
  if ntype == GEMSIGN_STATE.Sign then
    Lua_Chat_ShowOSD("SIGN_SUCC")
  elseif ntype == GEMSIGN_STATE.UnSign then
    Lua_Chat_ShowOSD("SIGN_UNSING_SUCC")
  end
end
function UI_SafeSetup_CtrlClickSign(msg)
  SignItem_SignRefreshUI()
  SignItem_CancelItem()
end
function UI_SafeSetup_CloseSign()
  SignItem_SignRefreshUI()
  gUI.SignItem.Root:SetVisible(false)
end
function UI_SafeSetup_HelpSign(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Signature.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function Script_SignItem_OnLoad()
  gUI.SignItem.Root = WindowSys_Instance:GetWindow("Signature")
  gUI.SignItem.OkBtn = WindowSys_Instance:GetWindow("Signature.Button1_btn")
  gUI.SignItem.SignLabelText = WindowToLabel(WindowSys_Instance:GetWindow("Signature.Picture3.Label1_slab"))
  gUI.SignItem.SignCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("Signature.TableCtrl_tctl"))
  gUI.SignItem.SignClose = WindowSys_Instance:GetWindow("Signature.Close_btn")
  gUI.SignItem.SignGoodsBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Signature.Picture2_bg.GoodsBox1_gbox"))
  gUI.SignItem.SignLabelInfo = WindowToLabel(WindowSys_Instance:GetWindow("Signature.Label1_slab"))
end
function Script_SignItem_OnHide()
  SignItem_CancelItem()
end
function Script_SignItem_OnEvent(event)
  if event == "SIGN_SHOW_WINDOW" then
    On_SignItem_OpenPanel()
  elseif event == "SIGN_ADD_ITEM" then
    On_SignItem_UpdateItem()
  elseif event == "SIGN_DEL_ITEM" then
    On_SignItem_UpdateItem()
  elseif event == "SIGN_RES" then
    OnSignItem_Sign_Res(arg1, arg2, arg3)
  end
end

