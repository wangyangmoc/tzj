if gUI and not gUI.GuildContest then
  gUI.GuildContest = {}
end
local GuildContestFunc = 28
local GuildContestMaxTurnRun = 5
gUI_GuildContest_Time = {
  "周二20点",
  "周三20点",
  "周四20点",
  "周五20点",
  "周日20点"
}
local gUI_GuildContest_RunTime = {}
local GuildContest_Member = 1
local GuildContest_World = 2
local GuildContest_MaxMemberText = "/50"
local GuildContest_LevelLimit = 30
function GuildContest_WroldCapOpen()
  if gUI.GuildContest.WroldCapRoot:IsVisible() then
    gUI.GuildContest.WroldCapRoot:SetVisible(false)
  else
    GuildContestBaseInfo(GuildContestFunc)
  end
end
function UI_GuildContest_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function GuildContest_WroldCapRule(Param)
  if gUI.GuildContest.WroldCapRuleRoot:IsVisible() and Param == nil then
    gUI.GuildContest.WroldCapRuleRoot:SetVisible(false)
  elseif Param == GuildContest_Member then
    gUI.GuildContest.WroldCapRuleRoot:SetVisible(true)
    gUI.GuildContest.WroldCapRule:SetVisible(false)
    gUI.GuildContest.MemberRule:SetVisible(true)
  elseif Param == GuildContest_World then
    gUI.GuildContest.WroldCapRuleRoot:SetVisible(true)
    gUI.GuildContest.WroldCapRule:SetVisible(true)
    gUI.GuildContest.MemberRule:SetVisible(false)
  end
end
function GuildContest_WorldCapInitData(CurrToTurnNum)
  if CurrToTurnNum >= 6 then
    for i = 0, #gUI.GuildContest.WroldCapLeft_32 do
      gUI.GuildContest.WroldCapLeft_32[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_32[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_32[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapRight_32[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapLeft_32[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail01")
      gUI.GuildContest.WroldCapRight_32[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail01")
      gUI.GuildContest.WroldCapLeft_32[i]:SetProperty("Visible", "true")
      gUI.GuildContest.WroldCapRight_32[i]:SetProperty("Visible", "true")
    end
  else
    for i = 0, #gUI.GuildContest.WroldCapLeft_32 do
      gUI.GuildContest.WroldCapLeft_32[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_32[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_32[i]:SetProperty("Visible", "false")
      gUI.GuildContest.WroldCapRight_32[i]:SetProperty("Visible", "false")
    end
  end
  if CurrToTurnNum >= 5 then
    for i = 0, #gUI.GuildContest.WroldCapLeft_16 do
      gUI.GuildContest.WroldCapLeft_16[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_16[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_16[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapRight_16[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapLeft_16[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail02")
      gUI.GuildContest.WroldCapRight_16[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail02")
      gUI.GuildContest.WroldCapLeft_16[i]:SetProperty("Visible", "true")
      gUI.GuildContest.WroldCapRight_16[i]:SetProperty("Visible", "true")
    end
  else
    for i = 0, #gUI.GuildContest.WroldCapLeft_16 do
      gUI.GuildContest.WroldCapLeft_16[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_16[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_16[i]:SetProperty("Visible", "false")
      gUI.GuildContest.WroldCapRight_16[i]:SetProperty("Visible", "false")
    end
  end
  if CurrToTurnNum >= 4 then
    for i = 0, #gUI.GuildContest.WroldCapLeft_8 do
      gUI.GuildContest.WroldCapLeft_8[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_8[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_8[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapRight_8[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapLeft_8[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail03")
      gUI.GuildContest.WroldCapRight_8[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail03")
      gUI.GuildContest.WroldCapLeft_8[i]:SetProperty("Visible", "true")
      gUI.GuildContest.WroldCapRight_8[i]:SetProperty("Visible", "true")
    end
  else
    for i = 0, #gUI.GuildContest.WroldCapLeft_8 do
      gUI.GuildContest.WroldCapLeft_8[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_8[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_8[i]:SetProperty("Visible", "false")
      gUI.GuildContest.WroldCapRight_8[i]:SetProperty("Visible", "false")
    end
  end
  if CurrToTurnNum >= 3 then
    for i = 0, #gUI.GuildContest.WroldCapLeft_4 do
      gUI.GuildContest.WroldCapLeft_4[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_4[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_4[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapRight_4[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapLeft_4[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail04")
      gUI.GuildContest.WroldCapRight_4[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail04")
      gUI.GuildContest.WroldCapLeft_4[i]:SetProperty("Visible", "true")
      gUI.GuildContest.WroldCapRight_4[i]:SetProperty("Visible", "true")
    end
  else
    for i = 0, #gUI.GuildContest.WroldCapLeft_4 do
      gUI.GuildContest.WroldCapLeft_4[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_4[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_4[i]:SetProperty("Visible", "false")
      gUI.GuildContest.WroldCapRight_4[i]:SetProperty("Visible", "false")
    end
  end
  if CurrToTurnNum >= 2 then
    for i = 0, #gUI.GuildContest.WroldCapLeft_2 do
      gUI.GuildContest.WroldCapLeft_2[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_2[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_2[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapRight_2[i]:SetProperty("BackImage", "UiIamge017:Image_Name01")
      gUI.GuildContest.WroldCapLeft_2[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail05")
      gUI.GuildContest.WroldCapRight_2[i]:GetGrandChild("Line_pic"):SetProperty("BackImage", "UiIamge017:Image_Fail05")
      gUI.GuildContest.WroldCapLeft_2[i]:SetProperty("Visible", "true")
      gUI.GuildContest.WroldCapRight_2[i]:SetProperty("Visible", "true")
    end
  else
    for i = 0, #gUI.GuildContest.WroldCapLeft_2 do
      gUI.GuildContest.WroldCapLeft_2[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapRight_2[i]:SetProperty("Text", "")
      gUI.GuildContest.WroldCapLeft_2[i]:SetProperty("Visible", "false")
      gUI.GuildContest.WroldCapRight_2[i]:SetProperty("Visible", "false")
    end
  end
  if CurrToTurnNum >= 1 then
    gUI.GuildContest.WorldCap_1:SetProperty("Text", "")
    gUI.GuildContest.WorldCap_1:SetProperty("Visible", "true")
  else
    gUI.GuildContest.WorldCap_1:SetProperty("Text", "")
    gUI.GuildContest.WorldCap_1:SetProperty("Visible", "false")
  end
  gUI.GuildContest.WroldInterZoneTime:SetProperty("Text", "--")
  for i = 0, #gUI.GuildContest.WroldCapTimeLeft do
    gUI.GuildContest.WroldCapTimeLeft[i]:SetVisible(false)
    gUI.GuildContest.WroldCapTimeRight[i]:SetVisible(false)
  end
end
function _OnGuildContestTurnData(CurrToTurnNum)
  gUI.GuildContest.WroldCapRoot:SetVisible(true)
  GuildContest_WorldCapInitData(CurrToTurnNum)
  local BlankNum = GuildContestMaxTurnRun - CurrToTurnNum + 1
  gUI_GuildContest_RunTime = {}
  for i = 0, BlankNum - 1 do
    gUI_GuildContest_RunTime[i] = ""
  end
  for i = BlankNum, #gUI_GuildContest_Time - 1 do
    gUI_GuildContest_RunTime[i] = gUI_GuildContest_Time[i - BlankNum + 1]
  end
  for i = 0, #gUI.GuildContest.WroldCapTimeLeft do
    if gUI_GuildContest_RunTime[i] ~= "" then
      gUI.GuildContest.WroldCapTimeLeft[i]:SetVisible(true)
      gUI.GuildContest.WroldCapTimeLeft[i]:SetProperty("Text", tostring(gUI_GuildContest_RunTime[i]))
      gUI.GuildContest.WroldCapTimeRight[i]:SetVisible(true)
      gUI.GuildContest.WroldCapTimeRight[i]:SetProperty("Text", tostring(gUI_GuildContest_RunTime[i]))
    end
  end
end
function _OnGuildContestGuildInfo(GuildTNum, GuildName1, GuildName2, BeatReslut, GuildRealPos)
  local guildName = GetGuildName()
  if guildName == GuildName1 then
    GuildName1 = string.format("{#G^%s}", GuildName1)
  end
  if guildName == GuildName2 then
    GuildName2 = string.format("{#G^%s}", GuildName2)
  end
  local wndMain, wndAdversary, WndScoreMainLine, WndScoreMain, WndScoreAdversary, WndScoreAdversaryLine, winLine, loseLine
  if GuildTNum >= 32 then
    winLine = "UiIamge017:Image_Win01"
    loseLine = "UiIamge017:Image_Fail01"
    if GuildRealPos <= 15 then
      wndMain = gUI.GuildContest.WroldCapLeft_32[GuildRealPos]
      wndAdversary = gUI.GuildContest.WroldCapLeft_32[GuildRealPos + 1]
    else
      if GuildRealPos <= 31 then
        wndMain = gUI.GuildContest.WroldCapRight_32[GuildRealPos - 16]
        wndAdversary = gUI.GuildContest.WroldCapRight_32[GuildRealPos + 1 - 16]
      else
      end
    end
  elseif GuildTNum >= 16 then
    winLine = "UiIamge017:Image_Win02"
    loseLine = "UiIamge017:Image_Fail02"
    if GuildRealPos <= 7 then
      wndMain = gUI.GuildContest.WroldCapLeft_16[GuildRealPos]
      wndAdversary = gUI.GuildContest.WroldCapLeft_16[GuildRealPos + 1]
    else
      if GuildRealPos <= 15 then
        wndMain = gUI.GuildContest.WroldCapRight_16[GuildRealPos - 8]
        wndAdversary = gUI.GuildContest.WroldCapRight_16[GuildRealPos + 1 - 8]
      else
      end
    end
  elseif GuildTNum >= 8 then
    winLine = "UiIamge017:Image_Win03"
    loseLine = "UiIamge017:Image_Fail03"
    if GuildRealPos <= 3 then
      wndMain = gUI.GuildContest.WroldCapLeft_8[GuildRealPos]
      wndAdversary = gUI.GuildContest.WroldCapLeft_8[GuildRealPos + 1]
    else
      if GuildRealPos <= 7 then
        wndMain = gUI.GuildContest.WroldCapRight_8[GuildRealPos - 4]
        wndAdversary = gUI.GuildContest.WroldCapRight_8[GuildRealPos + 1 - 4]
      else
      end
    end
  elseif GuildTNum >= 4 then
    winLine = "UiIamge017:Image_Win04"
    loseLine = "UiIamge017:Image_Fail04"
    if GuildRealPos <= 1 then
      wndMain = gUI.GuildContest.WroldCapLeft_4[GuildRealPos]
      wndAdversary = gUI.GuildContest.WroldCapLeft_4[GuildRealPos + 1]
    else
      if GuildRealPos <= 3 then
        wndMain = gUI.GuildContest.WroldCapRight_4[GuildRealPos - 2]
        wndAdversary = gUI.GuildContest.WroldCapRight_4[GuildRealPos + 1 - 2]
      else
      end
    end
  elseif GuildTNum >= 2 then
    winLine = "UiIamge017:Image_Win05"
    loseLine = "UiIamge017:Image_Fail05"
    if GuildRealPos <= 1 then
      wndMain = gUI.GuildContest.WroldCapLeft_2[GuildRealPos]
      wndAdversary = gUI.GuildContest.WroldCapRight_2[GuildRealPos]
    else
    end
  else
    if GuildTNum >= 1 then
      winLine = ""
      loseLine = ""
      if GuildRealPos <= 0 then
        wndMain = gUI.GuildContest.WorldCap_1
        wndAdversary = nil
      end
    else
    end
  end
  if wndMain ~= nil then
    wndMain:SetProperty("Text", GuildName1)
    wndMain:SetProperty("Visible", "true")
    WndScoreMain = wndMain:GetGrandChild("Score_pic")
    WndScoreMainLine = wndMain:GetGrandChild("Line_pic")
    if WndScoreMain ~= nil then
    end
    if WndScoreMainLine ~= nil then
    end
    wndMain:SetProperty("FontColor", "FFFAFAFA")
  end
  if wndAdversary ~= nil then
    wndAdversary:SetProperty("Text", GuildName2)
    wndAdversary:SetProperty("Visible", "true")
    WndScoreAdversary = wndAdversary:GetGrandChild("Score_pic")
    WndScoreAdversaryLine = wndAdversary:GetGrandChild("Line_pic")
    if WndScoreAdversary ~= nil then
    end
    if WndScoreAdversaryLine ~= nil then
    end
    wndAdversary:SetProperty("FontColor", "FFFAFAFA")
  end
  if BeatReslut == 0 then
    if WndScoreAdversaryLine ~= nil then
      WndScoreAdversaryLine:SetProperty("BackImage", tostring(loseLine))
    end
    if WndScoreMainLine ~= nil then
      WndScoreMainLine:SetProperty("BackImage", tostring(loseLine))
    end
  elseif BeatReslut == 1 then
    if WndScoreMain ~= nil then
      WndScoreMain:SetProperty("BackImage", "UiIamge017:Image_Name03")
      WndScoreMain:SetProperty("Visible", "true")
    end
    if WndScoreMainLine ~= nil then
      WndScoreMainLine:SetProperty("BackImage", tostring(loseLine))
      wndMain:SetProperty("FontColor", "FFA9A9A9")
    end
    if WndScoreAdversary ~= nil then
      WndScoreAdversary:SetProperty("BackImage", "UiIamge017:Image_Name01")
      WndScoreAdversary:SetProperty("Visible", "true")
    end
    if WndScoreAdversaryLine ~= nil then
      WndScoreAdversaryLine:SetProperty("BackImage", tostring(winLine))
      wndAdversary:SetProperty("FontColor", "FFFFFF00")
    end
  elseif BeatReslut == 2 then
    if WndScoreMain ~= nil then
      WndScoreMain:SetProperty("BackImage", "UiIamge017:Image_Name01")
      WndScoreMain:SetProperty("Visible", "true")
    end
    if WndScoreMainLine ~= nil then
      WndScoreMainLine:SetProperty("BackImage", tostring(winLine))
      wndMain:SetProperty("FontColor", "FFFFFF00")
    end
    if WndScoreAdversary ~= nil then
      WndScoreAdversary:SetProperty("BackImage", "UiIamge017:Image_Name03")
    end
    if WndScoreAdversaryLine ~= nil then
      WndScoreAdversaryLine:SetProperty("BackImage", tostring(loseLine))
      wndAdversary:SetProperty("FontColor", "FFA9A9A9")
    end
  elseif BeatReslut == -1 then
    if WndScoreAdversaryLine ~= nil then
      WndScoreAdversaryLine:SetProperty("BackImage", tostring(loseLine))
    end
    if WndScoreMainLine ~= nil then
      WndScoreMainLine:SetProperty("BackImage", tostring(loseLine))
    end
  end
end
function _OnGuildContestGuildTime(year1, month1, day1, year2, month2, day2)
  local strTime = string.format("%d.%d.%d -- %d.%d.%d", year1, month1, day1, year2, month2, day2)
  gUI.GuildContest.WroldInterZoneTime:SetProperty("Text", tostring(strTime))
end
function _OnGuildContestGuildMember()
  gUI.GuildContest.MemberMain:SetVisible(true)
  gUI.GuildContest.MemberCntBg:DeleteAll()
  gUI.GuildContest.MemberNumber:SetProperty("Text", tostring(GuildContestMemberNumber()) .. GuildContest_MaxMemberText)
  local memberTmp = WindowSys_Instance:GetWindow("GuildContestMember.Member_bg.Center_bg.ChooseTemp_cbtn")
  local cntMemList = {}
  local population = GetGuildPopulation() - 1
  local indexTable = 1
  for i = 0, population do
    local guid, name, _, _, _, _, is_online, level, menpai, last_time = GetGuildMemberInfo(nil, i)
    local need_insert = false
    local view_offline = not gUI.GuildContest.HideOfflineButton:IsChecked()
    local color
    if is_online then
      last_time = 0
    end
    if not is_online and last_time < 1 then
      last_time = 1
    end
    if is_online then
      color = "0xFFFFFFFF"
      need_insert = true
    elseif view_offline then
      color = "0xFF808080"
      need_insert = true
    end
    if need_insert and level >= GuildContest_LevelLimit then
      local onlineText = _GuildM_ConvertTimeToStr(last_time, is_online)
      local menpaiText = gUI_MenPaiName[menpai]
      local str1 = string.format("{#C^%s^%s}", color, name)
      local str2 = string.format("{#C^%s^%s}", color, menpaiText)
      local str3 = string.format("{#C^%s^%d}", color, level)
      local str4 = string.format("{#C^%s^%s}", color, onlineText)
      if last_time > 1000 then
        last_time = 1000
      end
      local power = last_time * 1000 - level * 10 + menpai
      cntMemList[indexTable] = {
        guid,
        str1,
        str2,
        str3,
        str4
      }
      cntMemList[indexTable].power = power
      indexTable = indexTable + 1
    end
  end
  table.sort(cntMemList, function(v1, v2)
    return v1.power < v2.power
  end)
  for key, valueTab in pairs(cntMemList) do
    local newItem = gUI.GuildContest.MemberCntBg:Insert(-1, memberTmp)
    newItem:SetProperty("Visible", "true")
    newItem:SetUserData64(valueTab[1])
    newItem:SetProperty("Text", valueTab[2])
    newItem:GetChildByName("Menpai_slab"):SetProperty("Text", valueTab[3])
    newItem:GetChildByName("Level_slab"):SetProperty("Text", valueTab[4])
    newItem:GetChildByName("State_slab"):SetProperty("Text", valueTab[5])
    if GuildContestMemberInfo(valueTab[1]) then
      WindowToCheckButton(newItem):SetCheckedState(true)
    else
      WindowToCheckButton(newItem):SetCheckedState(false)
    end
  end
end
function UI_GuildContest_Close(msg)
  GuildContest_WroldCapOpen()
end
function UI_GuildContest_Click(msg)
  GuildContest_WroldCapOpen()
end
function UI_GuildContest_Rule(msg)
  if gUI.GuildContest.WroldCapRuleRoot:IsVisible() then
    GuildContest_WroldCapRule()
  else
    GuildContest_WroldCapRule(GuildContest_World)
  end
end
function UI_GuildContest_RuleMember(msg)
  if gUI.GuildContest.WroldCapRuleRoot:IsVisible() then
    GuildContest_WroldCapRule()
  else
    GuildContest_WroldCapRule(GuildContest_Member)
  end
end
function UI_GuildContest_CloseRuleClick(msg)
  GuildContest_WroldCapRule()
end
function UI_GuildContest_CloseMemberUI(msg)
  gUI.GuildContest.MemberMain:SetVisible(false)
end
function UI_GuildContest_EnsureMemberUI(msg)
  GuildContestEnsureMember()
end
function UI_GuildContest_CheckedHideButton(msg)
  _OnGuildContestGuildMember()
end
function UI_GuildContest_CheckedMemberButton(msg)
  local wnd = msg:get_window()
  local checkBtn = WindowToCheckButton(wnd)
  local guid = wnd:GetUserData64()
  local selRes = GuildContestSelectMember(guid, checkBtn:IsChecked())
  if not selRes and checkBtn:IsChecked() then
    checkBtn:SetCheckedState(false)
  end
  gUI.GuildContest.MemberNumber:SetProperty("Text", tostring(GuildContestMemberNumber()) .. GuildContest_MaxMemberText)
end
function Script_GuildContest_OnLoad()
  gUI.GuildContest.WroldCapRoot = WindowSys_Instance:GetWindow("GuildContest")
  gUI.GuildContest.WroldCapLeft_32 = {}
  gUI.GuildContest.WroldCapRight_32 = {}
  for i = 0, 15 do
    gUI.GuildContest.WroldCapLeft_32[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Left_bg.Pic1_bg.Label" .. tostring(i + 1) .. "_dlab"))
    gUI.GuildContest.WroldCapRight_32[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Right_bg.Pic1_bg.Label" .. tostring(i + 1) .. "_dlab"))
  end
  gUI.GuildContest.WroldCapLeft_16 = {}
  gUI.GuildContest.WroldCapRight_16 = {}
  for i = 0, 7 do
    gUI.GuildContest.WroldCapLeft_16[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Left_bg.Pic2_bg.Label" .. tostring(i + 1) .. "_dlab"))
    gUI.GuildContest.WroldCapRight_16[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Right_bg.Pic2_bg.Label" .. tostring(i + 1) .. "_dlab"))
  end
  gUI.GuildContest.WroldCapLeft_8 = {}
  gUI.GuildContest.WroldCapRight_8 = {}
  for i = 0, 3 do
    gUI.GuildContest.WroldCapLeft_8[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Left_bg.Pic3_bg.Label" .. tostring(i + 1) .. "_dlab"))
    gUI.GuildContest.WroldCapRight_8[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Right_bg.Pic3_bg.Label" .. tostring(i + 1) .. "_dlab"))
  end
  gUI.GuildContest.WroldCapLeft_4 = {}
  gUI.GuildContest.WroldCapRight_4 = {}
  for i = 0, 1 do
    gUI.GuildContest.WroldCapLeft_4[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Left_bg.Pic4_bg.Label" .. tostring(i + 1) .. "_dlab"))
    gUI.GuildContest.WroldCapRight_4[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Right_bg.Pic4_bg.Label" .. tostring(i + 1) .. "_dlab"))
  end
  gUI.GuildContest.WroldCapLeft_2 = {}
  gUI.GuildContest.WroldCapRight_2 = {}
  gUI.GuildContest.WroldCapLeft_2[0] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Left_bg.Pic5_bg.Label1_dlab"))
  gUI.GuildContest.WroldCapRight_2[0] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Right_bg.Pic5_bg.Label1_dlab"))
  gUI.GuildContest.WorldCap_1 = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Pic1_bg.Name_slab"))
  gUI.GuildContest.WorldCapPic_1 = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Pic1_bg.Label1_slab"))
  gUI.GuildContest.WroldInterZoneTime = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Down_bg.Time_dlab"))
  gUI.GuildContest.WroldCapTimeLeft = {}
  gUI.GuildContest.WroldCapTimeRight = {}
  for i = 0, 4 do
    gUI.GuildContest.WroldCapTimeLeft[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Left_bg.Pic" .. tostring(i + 1) .. "_bg.Time_dlab"))
    gUI.GuildContest.WroldCapTimeRight[i] = WindowToLabel(WindowSys_Instance:GetWindow("GuildContest.GuildContest_bg.Center_bg.Right_bg.Pic" .. tostring(i + 1) .. "_bg.Time_dlab"))
  end
  gUI.GuildContest.WroldCapRuleRoot = WindowSys_Instance:GetWindow("GuildContestRule")
  gUI.GuildContest.WroldCapRule = WindowSys_Instance:GetWindow("GuildContestRule.GuildContestRule_bg.Image_bg.Contest_bg")
  gUI.GuildContest.MemberRule = WindowSys_Instance:GetWindow("GuildContestRule.GuildContestRule_bg.Image_bg.Member_bg")
  gUI_GuildContest_RunTime = {}
  gUI.GuildContest.MemberMain = WindowSys_Instance:GetWindow("GuildContestMember")
  gUI.GuildContest.MemberCntBg = WindowToContainer(WindowSys_Instance:GetWindow("GuildContestMember.Member_bg.Center_bg.ImageBg_cnt"))
  gUI.GuildContest.MemberCntBg:DeleteAll()
  gUI.GuildContest.HideOfflineButton = WindowToCheckButton(WindowSys_Instance:GetWindow("GuildContestMember.Member_bg.Center_bg.Title_bg.Sta_cbtn"))
  gUI.GuildContest.HideOfflineButton:SetCheckedState(false)
  gUI.GuildContest.MemberNumber = WindowSys_Instance:GetWindow("GuildContestMember.Member_bg.Center_bg.Title_bg.Cho_dlab")
end
function Script_GuildContest_OnHide()
end
function Script_GuildContest_OnEvent(event)
  if event == "GUILDCONTEST_TURNDATA" then
    _OnGuildContestTurnData(arg1)
  elseif event == "GUILDCONTEST_GUILDINFO" then
    _OnGuildContestGuildInfo(arg1, arg2, arg3, arg4, arg5)
  elseif event == "GUILDCONTEST_GUILDMEMBER" then
    _OnGuildContestGuildMember()
  elseif event == "GUILDCONTEST_GUILDTIME" then
    _OnGuildContestGuildTime(arg1, arg2, arg3, arg4, arg5, arg6)
  end
end

