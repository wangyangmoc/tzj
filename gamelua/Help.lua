if gUI and not gUI.Help then
  gUI.Help = {}
end
local _Help_Current_Open_Path = ""
local _Help_Current_Open_ID = -1
local _Help_Current_Open_TabID = -1
local _Help_Current_Select_TreeID = -1
local _Help_Current_Select_TreeParentID = -1
HELP_DEFLUT_SELECTITEM = 0
HELP_UPDATETYPE_SUB = 10
HELP_UPDATETYPE_TAB = 20
function UI_Help_Click(msg)
  if not gUI.Help.Root:IsVisible() then
    local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Help_btn")):GetProperty("CustomUserData"))
    Lua_Help_ShowUI_ByPath(ID)
  else
    gUI.Help.Root:SetVisible(false)
  end
end
function Lua_Help_ShowUI_ByPath(SubID)
  if Lua_Help_Close_Open_Panel("1") then
    UpdateCurrentByPath_TabData(SubID, HELP_UPDATETYPE_SUB)
  end
  Lua_Help_InitData()
end
function Lua_Help_ShowUI_ByTab(Tab)
  UpdateCurrentByPath_TabData(Tab, HELP_UPDATETYPE_TAB)
end
function Lua_Help_Close_Open_Panel(str)
  local b_flagOpen = str
  if not gUI.Help.Root:IsVisible() then
  else
  end
  gUI.Help.Root:SetVisible("1" == b_flagOpen)
  return "1" == b_flagOpen
end
function Lua_Help_InitData()
  gUI.Help.EditBox:SetProperty("Text", "")
end
function Lua_Help_ClearData()
end
function _OnHelp_SetCurrentSelectTableItem(curTab)
  gUI.Help.TableCtrl:SelectItem(curTab)
  _Help_Current_Open_TabID = curTab
end
function _OnHelp_UpdateLeftTreeListAndPosStart()
  Lua_Help_InitParams_Start()
  gUI.Help.TreeList:DeleteAllItems()
  gUI.Help.TreeList:LockUpdate(true)
end
function _OnHelp_UpdateLeftTreeListAndPos(subId, MainId, Title)
  local parent = gUI.Help.TreeList:FindItemByData(MainId)
  if MainId == 0 then
    parent = gUI.Help.TreeList:InsertItem(Title, nil, "kaiti_12", 4286248381)
    parent:SetUserData(subId)
    parent:SetShrink(true)
  else
    local subItem = gUI.Help.TreeList:InsertItem(Title, parent, "fzheiti_11", 4294638330)
    subItem:SetUserData(subId)
  end
end
function _OnHelp_SetCurrentSelectItemById(ItemId)
  if ItemId == HELP_DEFLUT_SELECTITEM then
    Lua_Help_TreeListSelectFirstChild()
    return
  end
  local currentSel = gUI.Help.TreeList:SearchItemByData(ItemId)
  if currentSel then
    gUI.Help.TreeList:SetSelectedItem(currentSel)
  end
end
function _OnHelp_UpdateLeftTreeListAndPosEnd()
  gUI.Help.TreeList:LockUpdate(false)
end
function _OnHelp_UpdateRightContainListAndPosStart()
  gUI.Help.Contain:DeleteAll()
end
function _OnHelp_UpdateRightContainListAndPos(title, Description, subId, keyWord, begIn, endIn, begTi, endTi)
  if keyWord == "" then
    local strtitle = title
    local strdescri = Description
    if strtitle ~= "" then
      strtitle = string.format("{HeLp^%s^%s^%s^%s}\n", strtitle, "kaiti_13", tostring(subId), "1")
    end
    if strdescri ~= "" then
      strdescri = string.format("%s\n", strdescri)
    end
    local tempDisplayBox = gUI.Help.Contain:Insert(-1, gUI.Help.TempDisPlayBox)
    tempDisplayBox = WindowToDisplayBox(tempDisplayBox)
    tempDisplayBox:ClearAllContent()
    tempDisplayBox:InsertBack(strtitle, 4294956800, subId, 0)
    tempDisplayBox:InsertBack(strdescri, 4292927712, subId, 0)
    tempDisplayBox:SetCustomUserData(1, subId)
  else
    if begTi ~= "" then
      strtitle = string.format("%s{HeLp^%s^%s^%s^%s}%s\n", begTi, keyWord, "kaiti_13", tostring(subId), "0", endTi)
    else
      strtitle = string.format("{HeLp^%s^%s^%s^%s}\n", title, "kaiti_13", tostring(subId), "1")
    end
    if begIn ~= "" or endIn ~= "" then
      Description = string.format("%s{HeLp^%s^%s^%s^%s}%s\n", begIn, keyWord, "songti_8", tostring(subId), "0", endIn)
    else
      Description = string.format("%s\n", " ")
    end
    local tempDisplayBox = gUI.Help.Contain:Insert(-1, gUI.Help.TempDisPlayBox)
    tempDisplayBox = WindowToDisplayBox(tempDisplayBox)
    tempDisplayBox:ClearAllContent()
    tempDisplayBox:InsertBack(strtitle, 4294956800, subId, 0)
    tempDisplayBox:InsertBack(Description, 4292927712, subId, 0)
    tempDisplayBox:SetCustomUserData(1, subId)
  end
end
function _OnHelp_UpdataRightContainListAndPosEnd(CurrUpdateItemId)
  Lua_Help_UpdateRightEnd()
  Lua_Help_UpdateCurrentRightScrollBarPosition(CurrUpdateItemId)
end
function Lua_Help_UpdateRightEnd()
  gUI.Help.Contain:SizeSelf()
end
function Lua_Help_TreeListSelectFirstChild()
  gUI.Help.TreeList:SelectFirstChild(1)
end
function Lua_Help_InitParams_Start()
end
function Lua_Help_UpdateCurrentRightScrollBarPosition(CurrUpdateItemId)
  if CurrUpdateItemId ~= 0 then
    gUI.Help.Contain:SetChildVisible(1, CurrUpdateItemId)
  end
end
function UI_Help_TableCtrlClick(msg)
  local wnd = WindowToTableCtrl(msg:get_window())
  local selectTab = wnd:GetSelectItem()
  if selectTab ~= _Help_Current_Open_TabID then
    Lua_Help_ShowUI_ByTab(selectTab)
  end
end
function UI_Help_HideClose(msg)
  Lua_Help_Close_Open_Panel("0")
end
function UI_Help_TreeListClick(msg)
  local CurrentItemId = msg:get_lparam()
  UpdateMainInfosBySelIndex(CurrentItemId)
end
function _On_Help_ContectClick(ItemID)
  Lua_Help_ShowUI_ByPath(ItemID)
end
function UI_Help_SearchClick(msg)
  local strKeyWord = gUI.Help.EditBox:GetProperty("Text")
  if strKeyWord ~= "" and strKeyWord ~= "}" then
    SearchInfoByStr(strKeyWord)
  end
end
function UI_Help_Bbs(msg)
  OpenWebPage("http://60.12.209.75/bbs/forum.php")
end
function Script_Help_OnEvent(event)
  if event == "HELP_LEFTLISTANDPOS_START" then
    _OnHelp_UpdateLeftTreeListAndPosStart()
  elseif event == "HELP_LEFTLISTANDPOS_UPDAYE" then
    _OnHelp_UpdateLeftTreeListAndPos(arg1, arg2, arg3)
  elseif event == "HELP_LEFTLISTANDPOS_END" then
    _OnHelp_UpdateLeftTreeListAndPosEnd()
  elseif event == "HELP_CURRENTSELECT_TAB" then
    _OnHelp_SetCurrentSelectTableItem(arg1)
  elseif event == "HELP_CURRENTSELECT_TREELIST" then
    _OnHelp_SetCurrentSelectItemById(arg1)
  elseif event == "HELP_RIGHTCONTAIN_START" then
    _OnHelp_UpdateRightContainListAndPosStart()
  elseif event == "HELP_RIGHTCONTAIN_UPDATE" then
    _OnHelp_UpdateRightContainListAndPos(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "HELP_RIGHTCONTAIN_END" then
    _OnHelp_UpdataRightContainListAndPosEnd(arg1)
  elseif event == "HELP_CLICKCONTENT" then
    _On_Help_ContectClick(arg1)
  end
end
function Script_Help_OnLoad()
  gUI.Help.Root = WindowSys_Instance:GetWindow("Help_frm")
  gUI.Help.TableCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("Help_frm.Di_bg.HelpText_bg.TableCtrl1"))
  gUI.Help.TreeList = WindowToTreeCtrl(WindowSys_Instance:GetWindow("Help_frm.Di_bg.HelpText_bg.LeftDi_bg.TitleList_tctrl"))
  gUI.Help.Contain = WindowToContainer(WindowSys_Instance:GetWindow("Help_frm.Di_bg.HelpText_bg.RightDi_bg.Text_ctn"))
  gUI.Help.TempDisPlayBox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Help_frm.Di_bg.HelpText_bg.RightDi_bg.Temp_dbox"))
  gUI.Help.BtnBbs = WindowToButton(WindowSys_Instance:GetWindow("Help_frm.Di_bg.BBS_btn"))
  gUI.Help.EditBox = WindowToEditBox(WindowSys_Instance:GetWindow("Help_frm.Di_bg.Edit_bg.Edit_ebox"))
  gUI.Help.BtnSearch = WindowToButton(WindowSys_Instance:GetWindow("Help_frm.Di_bg.Edit_bg.Search_btn"))
end
function Script_Help_OnHide()
end
