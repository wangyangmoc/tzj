if gUI and not gUI.HomeLandFri then
  gUI.HomeLandFri = {}
end
local _HomeLandFri = false
local _HomeLandFri_MAXNUM = 10
function _HomeLandFri_Update()
  if gUI.HomeLandFri.FriHomeRoot:IsVisible() then
    _HomeLandFri_UpdateFriList()
  end
  if gUI.HomeLandFri.AccessRoot:IsVisible() then
    _HomeLandFri_UpdateAccessList()
  end
end
function UI_HomeLand_Friend_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Homeland_Friend.Friend_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _HomeLandFri_UpdateFriList()
  gUI.HomeLandFri.FriHomeList:RemoveAllItems()
  local friList = GetFriendHomeLandList()
  if friList[0] == nil then
    gUI.HomeLandFri.FriHomeNotice:SetVisible(true)
    return
  else
    gUI.HomeLandFri.FriHomeNotice:SetVisible(false)
  end
  for i = 0, table.maxn(friList) do
    local str = friList[i].Name
    local item = gUI.HomeLandFri.FriHomeList:InsertString(str, -1)
    gUI.HomeLandFri.FriHomeList:SetProperty("TextHorzAlign", "Left")
    gUI.HomeLandFri.FriHomeList:SetProperty("TextHorzAlign", "HCenter")
    item:SetUserData64(friList[i].Guid)
  end
end
function _HomeLandFri_UpdateAccessList()
  local nCoun = 0
  gUI.HomeLandFri.NameListL:RemoveAllItems()
  gUI.HomeLandFri.NameListR:RemoveAllItems()
  local list = UpdateHomeFriendList()
  if list[0] == nil then
    gUI.HomeLandFri.NumberAccess:SetProperty("Text", tostring(0) .. "/" .. tostring(_HomeLandFri_MAXNUM))
    return
  end
  for i = 0, table.maxn(list) do
    local friLev = list[i].FriLev
    if friLev >= 2 then
      local str = list[i].Name
      if list[i].CanAdd then
        local item = gUI.HomeLandFri.NameListL:InsertString(str, -1)
        gUI.HomeLandFri.NameListL:SetProperty("TextHorzAlign", "Left")
        gUI.HomeLandFri.NameListL:SetProperty("TextHorzAlign", "HCenter")
        item:SetUserData64(list[i].Guid)
      else
        local item = gUI.HomeLandFri.NameListR:InsertString(str, -1)
        gUI.HomeLandFri.NameListR:SetProperty("TextHorzAlign", "Left")
        gUI.HomeLandFri.NameListR:SetProperty("TextHorzAlign", "HCenter")
        item:SetUserData64(list[i].Guid)
        nCoun = nCoun + 1
      end
    end
  end
  gUI.HomeLandFri.NumberAccess:SetProperty("Text", tostring(nCoun) .. "/" .. tostring(_HomeLandFri_MAXNUM))
end
function _OnHomeLandFri_ShowFriHomeLand(bHasFri)
  gUI.HomeLandFri.FriHomeRoot:SetVisible(true)
  if bHasFri then
    _HomeLandFri_UpdateFriList()
  else
    gUI.HomeLandFri.FriHomeList:RemoveAllItems()
    gUI.HomeLandFri.FriHomeNotice:SetVisible(true)
  end
  AskHomeInfo(0, -1)
end
function _OnHomeLandFri_ShowAccess()
  gUI.HomeLandFri.SearchBox:SetProperty("Text", "")
  gUI.HomeLandFri.ClearBtn:SetEnable(false)
  gUI.HomeLandFri.AccessRoot:SetVisible(true)
  gUI.HomeLandFri.AddAccessBtn:SetEnable(false)
  gUI.HomeLandFri.DelAccessBtn:SetEnable(false)
  gUI.HomeLandFri.NameListL:RemoveAllItems()
  gUI.HomeLandFri.NameListR:RemoveAllItems()
  AskHomeInfo(0, -1)
end
function UI_HomeLandFri_ShowFriHomeLand()
  gUI.HomeLandFri.FriHomeList:RemoveAllItems()
  gUI.HomeLandFri.FriHomeRoot:SetVisible(false)
end
function UI_HomeLandFri_EnterFriHomeLand()
  local item = gUI.HomeLandFri.FriHomeList:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择一个好友再点击确定")
    return
  end
  if item then
    local target_guid = item:GetUserData64()
    EnterFriendFarm(target_guid)
  end
end
function UI_HomeLandFri_CloseAccess()
  gUI.HomeLandFri.AccessRoot:SetVisible(false)
end
function UI_HomeLandFri_NameListRSelect()
  local wnd = gUI.HomeLandFri.NameListR:GetSelectedItem()
  if wnd then
    gUI.HomeLandFri.DelAccessBtn:SetEnable(true)
  end
end
function UI_HomeLandFri_NameListLSelect()
  local item = gUI.HomeLandFri.NameListL:GetSelectedItem()
  if item then
    gUI.HomeLandFri.AddAccessBtn:SetEnable(true)
  end
end
function UI_HomeLandFri_AddBtnClick()
  local item = gUI.HomeLandFri.NameListL:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择一个好友")
    return
  end
  if item then
    local target_guid = item:GetUserData64()
    AddManorPermission(target_guid)
  end
end
function UI_HomeLandFri_DelBtnClick()
  local item = gUI.HomeLandFri.NameListR:GetSelectedItem()
  if not item then
    ShowErrorMessage("请选择一个好友")
    return
  end
  if item then
    local target_guid = item:GetUserData64()
    RemoveManorPermission(target_guid)
  end
end
function UI_HomeLandFri_FindBoxChange(msg)
  local name = msg:get_window():GetProperty("Text")
  if name ~= "" then
    gUI.HomeLandFri.ClearBtn:SetEnable(true)
    gUI.HomeLandFri.NameListL:RemoveAllItems()
    local reList = SearchHomeFriendList(name)
    if reList[0] ~= nil then
      for i = 0, table.maxn(reList) do
        local str = reList[i].Name
        local item = gUI.HomeLandFri.NameListL:InsertString(str, -1)
        gUI.HomeLandFri.NameListL:SetProperty("TextHorzAlign", "Left")
        gUI.HomeLandFri.NameListL:SetProperty("TextHorzAlign", "HCenter")
        item:SetUserData64(reList[i].Guid)
      end
    end
  else
    _HomeLandFri_Update()
  end
end
function UI_HomeLandFri_SearchClear(msg)
  gUI.HomeLandFri.ClearBtn:SetEnable(false)
  gUI.HomeLandFri.SearchBox:SetProperty("Text", "")
  Lua_Fri_SetFoucsToWorld()
  _HomeLandFri_Update()
end
function Script_HomeLandFri_OnEvent(event)
  if event == "HOME_SHOW_FRI_LAND" then
    _OnHomeLandFri_ShowFriHomeLand(arg1)
  elseif event == "HOME_UPDATE_FRI_LAND" then
    _HomeLandFri_Update()
  elseif event == "HOME_SHOW_HOMELAND_ACCESS" then
    _OnHomeLandFri_ShowAccess()
  end
end
function Script_HomeLandFri_OnLoad(event)
  gUI.HomeLandFri.FriHomeRoot = WindowSys_Instance:GetWindow("Homeland_Friend")
  gUI.HomeLandFri.FriHomeList = WindowToListBox(WindowSys_Instance:GetWindow("Homeland_Friend.Friend_bg.Image_bg.Enter_bg.Enter_lbox"))
  gUI.HomeLandFri.FriHomeNotice = WindowSys_Instance:GetWindow("Homeland_Friend.Friend_bg.Image_bg.Enter_bg.Unable_slab")
  gUI.HomeLandFri.GoHomeBtn = WindowSys_Instance:GetWindow("Homeland_Friend")
  gUI.HomeLandFri.FriHomeList:RemoveAllItems()
  gUI.HomeLandFri.AccessRoot = WindowSys_Instance:GetWindow("HomeLand_FriendRight")
  gUI.HomeLandFri.NameListL = WindowToListBox(WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Left_bg.Friend_lbox"))
  gUI.HomeLandFri.NameListR = WindowToListBox(WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Right_bg.Friend_lbox"))
  gUI.HomeLandFri.AddAccessBtn = WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Add_btn")
  gUI.HomeLandFri.DelAccessBtn = WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Reduce_btn")
  gUI.HomeLandFri.NumberAccess = WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Right_bg.Label2_dlab")
  gUI.HomeLandFri.ClearBtn = WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Left_bg.Clear_btn")
  gUI.HomeLandFri.SearchBox = WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Left_bg.Name_bg.Name_ebox")
end
t_bg.Left_bg.Clear_btn")
  gUI.HomeLandFri.SearchBox = WindowSys_Instance:GetWindow("HomeLand_FriendRight.FriendRight_bg.Left_bg.Name_bg.Name_ebox")
end
