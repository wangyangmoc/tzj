if gUI and not gUI.Ineffective then
  gUI.Ineffective = {}
end
INEFFECTIVESLOTCOMP = {EQUIPSLOT = 0, MATERIALSLOT = 1}
function UI_Ineffective_OnPanelOpen(msg)
  if gUI.Ineffective.Root:IsVisible() then
    UI_Ineffective_ClosePanel()
  else
    _Ineffective_OpenPanel()
  end
end
function UI_Ineffective_ClosePanel()
  gUI.Ineffective.Root:SetVisible(false)
end
function UI_Ineffective_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Ineffective.Ineffective_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _Ineffective_OpenPanel()
  gUI.Ineffective.Root:SetVisible(true)
end
function UI_Ineffective_EquipOnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_slot = msg:get_wparam()
  local from_TypeIndex = to_win:GetProperty("GoodBoxIndex")
  if from_TypeIndex == "GB_ItemMainBag" then
    from_slot = Lua_Bag_GetRealSlot(from_win, from_slot)
    Ineffective_SendAddEquipToServer(gUI_GOODSBOX_BAG.backpack0, from_slot)
  end
end
function UI_Ineffective_MaterOnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_slot = msg:get_wparam()
  local from_TypeIndex = to_win:GetProperty("GoodBoxIndex")
  if from_TypeIndex == "GB_ItemMainBag" then
    from_slot = Lua_Bag_GetRealSlot(from_win, from_slot)
    Ineffective_SendAddMaterToServer(gUI_GOODSBOX_BAG.backpack0, from_slot)
  end
end
function UI_Ineffective_OnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_slot = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local boxName = to_win:GetProperty("WindowName")
  local from_TypeIndex = from_win:GetProperty("GoodBoxIndex")
  if from_TypeIndex == "GB_ItemMainBag" then
    from_slot = Lua_Bag_GetRealSlot(from_win, from_slot)
    if boxName == "Equip_gbox" then
      Ineffective_SendAddEquipToServer(gUI_GOODSBOX_BAG.backpack0, from_slot)
    elseif boxName == "Ineffective_gbox" then
      Ineffective_SendAddMaterToServer(gUI_GOODSBOX_BAG.backpack0, from_slot)
    end
  end
end
function Ineffective_SendAddEquipToServer(srcBag, srcSlot)
  if NPC_IsEquip(srcSlot) == false then
    Lua_Chat_ShowOSD("INEFFECTIVE_DOACT_ONLYEQUIP")
    return false
  end
  local IneffectiveAttr = GetInEffectiveInfos(srcBag, srcSlot, 0)
  if IneffectiveAttr < 1 then
    Lua_Chat_ShowOSD("INEFFECTIVE_CANNOT_DOACTION")
    return false
  end
  SetInEffectiveEquip(srcSlot, OperateMode.Add, INEFFECTIVESLOTCOMP.EQUIPSLOT)
end
function Ineffective_SendAddMaterToServer(srcBag, srcSlot)
  if not gUI.Ineffective.MainEquipGbox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("AUTHENT_ENTER_EQUIPFIRST")
    return
  else
    local attrName, value, rate, gemIcon, gemName, quality, tableId = GetGemInfoBySlot(bag, slot, 0)
    local canPut = GetIneffectiveGemInfos(srcBag, srcSlot, 0)
    if canPut == 2 then
      Lua_Chat_ShowOSD("INEFFECTIVE_CANACC_EQUIPGEM")
    elseif canPut == 0 then
      Lua_Chat_ShowOSD("INEFFECTIVE_CANPUT_ONLYLI")
    elseif canPut == 1 then
      SetInEffectiveEquip(srcSlot, OperateMode.Add, INEFFECTIVESLOTCOMP.MATERIALSLOT)
    end
  end
end
function UI_Ineffective_EquipOnRClick(msg)
  local winBox = WindowToGoodsBox(msg:get_window())
  local boxName = winBox:GetProperty("WindowName")
  if boxName == "Equip_gbox" then
    SetInEffectiveEquip(0, OperateMode.Cancel)
  elseif boxName == "Ineffective_gbox" then
    SetInEffectiveEquip(0, OperateMode.Remove, INEFFECTIVESLOTCOMP.MATERIALSLOT)
  end
end
function UI_Ineffective_DoAction(msg)
  if not gUI.Ineffective.MainEquipGbox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("AUTHENT_ENTER_EQUIPFIRST")
    return
  end
  if not gUI.Ineffective.SubMaterGbox:IsItemHasIcon(0) then
    Lua_Chat_ShowOSD("AUTHENT_ENTER_MATERFIRST")
    return
  end
  local bagsub, slotsub = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.MATERIALSLOT)
  local canDo, levGem, levEquip
  if bagsub then
    canDo, levGem, levEquip = GetIneffectiveGemInfos(bagsub, slotsub, 0)
    if levGem > levEquip then
      Lua_Chat_ShowOSD("INEFFECTIVE_LEVEL_OVERFLOW")
      return
    end
  end
  local bag, slot = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.EQUIPSLOT)
  if bag then
    local Time, deltaTime, des, value = GetIneffectiveGemInfosEx(bag, slot, 0)
    if Time == 0 or deltaTime <= 0 then
      Messagebox_Show("CONFIRM_BIND_INEFF")
    elseif deltaTime > 0 then
      Messagebox_Show("CONFIRM_REPLACE_INEFF")
    end
  end
end
function Ineffective_UpdateItems(srcBag, srcSlot, NpcSlot)
  if not gUI.Ineffective.Root:IsVisible() then
    return
  end
  MaterName, MaterIconName, quality, _, _, _, _, _, _, _, _, MaterItemNum, _, _, _, _, _, _, _, _, _, _, _, _, _, _, ItemIds = GetItemInfoBySlot(srcBag, srcSlot, 0)
  if NpcSlot == INEFFECTIVESLOTCOMP.EQUIPSLOT then
    gUI.Ineffective.MainEquipGbox:SetItemGoods(0, MaterIconName, quality)
    local intenLev = Lua_Bag_GetStarInfo(srcBag, srcSlot)
    if 0 < ForgeLevel_To_Stars[intenLev] then
      gUI.Ineffective.MainEquipGbox:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
    else
      gUI.Ineffective.MainEquipGbox:SetItemStarState(0, 0)
    end
    gUI.Ineffective.SubMaterGbox:ClearGoodsItem(0)
  elseif NpcSlot == INEFFECTIVESLOTCOMP.MATERIALSLOT then
    gUI.Ineffective.SubMaterGbox:SetItemGoods(0, MaterIconName, quality)
  end
end
function Ineffective_Sue()
  Lua_Chat_ShowOSD("INEFFECTIVE_LEVEL_SUCC")
  local bag, slot = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.MATERIALSLOT)
  if bag then
  else
    gUI.Ineffective.SubMaterGbox:ClearGoodsItem(0)
  end
end
function Ineffective_DelItems(NpcSlot)
  if NpcSlot == INEFFECTIVESLOTCOMP.EQUIPSLOT then
    gUI.Ineffective.MainEquipGbox:ClearGoodsItem(0)
    gUI.Ineffective.SubMaterGbox:ClearGoodsItem(0)
  elseif NpcSlot == INEFFECTIVESLOTCOMP.MATERIALSLOT then
    gUI.Ineffective.SubMaterGbox:ClearGoodsItem(0)
  end
end
function UI_IneffEquipsShowItem_Tips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  local bag, slot = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.EQUIPSLOT)
  if not gUI.Ineffective.Root:IsVisible() or not gUI.Ineffective.MainEquipGbox:IsItemHasIcon(0) then
    return
  elseif bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(win, bag, slot)
  end
end
function UI_IneffMaterial_ShowTips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if not gUI.Ineffective.Root:IsVisible() or not gUI.Ineffective.SubMaterGbox:IsItemHasIcon(0) then
    return
  else
    local bag, slot = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.EQUIPSLOT)
    local bagsub, slotsub = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.MATERIALSLOT)
    if bagsub then
      Lua_Tip_Item(tooltip, slotsub, bagsub, nil, nil, nil)
    end
  end
end
function DoIneffctive()
  local bagsub, slotsub = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.MATERIALSLOT)
  local bag, slot = GetIneffCurrentItem(INEFFECTIVESLOTCOMP.EQUIPSLOT)
  if bagsub then
    local bindInfo = GetItemBindInfos(bag, slot, 0)
    local bindInfoTools = GetItemBindInfos(bagsub, slotsub, 0)
    if not bindInfo and bindInfoTools then
      Messagebox_Show("CONFIRM_BINDITEM_INEFF")
    else
      DoIneffctiveAction()
    end
  end
end
function Script_Ineffective_OnLoad()
  gUI.Ineffective.Root = WindowSys_Instance:GetWindow("Ineffective")
  gUI.Ineffective.ABtn = WindowToButton(WindowSys_Instance:GetWindow("Ineffective.Ineffective_bg.Down_bg.Ineffective_btn"))
  gUI.Ineffective.FeeLab = WindowToLabel(WindowSys_Instance:GetWindow("Ineffective.Ineffective_bg.Down_bg.Spend_bg.Spend_dlab"))
  gUI.Ineffective.MainEquipGbox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Ineffective.Ineffective_bg.Explain_bg.Equip_bg.Equip_gbox"))
  gUI.Ineffective.SubMaterGbox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Ineffective.Ineffective_bg.Explain_bg.Ineffective_bg.Ineffective_gbox"))
  gUI.Ineffective.FeeLab:SetProperty("Text", Lua_UI_Money2String(INEFFECT_COST))
end
function Script_Ineffective_OnHide()
  SetInEffectiveEquip(0, OperateMode.Cancel)
end
function Script_Ineffective_OnEvent(event)
  if "INEFF_ADD_ITEM" == event then
    Ineffective_UpdateItems(arg1, arg2, arg3)
  elseif "INEFF_DEL_ITEM" == event then
    Ineffective_DelItems(arg1)
  elseif "INEFF_RES_SUESS" == event then
    Ineffective_Sue()
  end
end
