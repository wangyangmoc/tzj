if gUI and not gUI.JeweBlend then
  gUI.JeweBlend = {}
end
local MAXJEWEBLENDNUM = 99
local JEWEBLEND_SLEID = -1
local IS_FLASFLAG = true
function JewelBlend_OpenPanel()
  if gUI.JeweBlend.Root:IsVisible() then
    gUI.JeweBlend.Root:SetVisible(false)
  else
    gUI.JeweBlend.Root:SetVisible(true)
    IS_FLASFLAG = true
    JewelBlend_ClearInfos()
    Notity_JewelBlend_Open()
  end
end
function Notity_JewelBlend_Open()
  GemCom_PanelOpen()
end
function JewelBlend_UpdateList()
  if gUI.JeweBlend.Root:IsVisible() then
    if IS_FLASFLAG then
      IS_FLASFLAG = false
      gUI.JeweBlend.JewTime:Restart()
    else
      gUI.JeweBlend.JewTime:Restart()
    end
  end
end
function JewelBlend_ClearInfos()
  gUI.JeweBlend.MoneyLab:SetProperty("Text", Lua_UI_Money2String(0))
  gUI.JeweBlend.MoneyLab:SetCustomUserData(0, 0)
  gUI.JeweBlend.NimbusLab:SetProperty("Text", "0")
  gUI.JeweBlend.Nimbus:SetProperty("Visible", "false")
  gUI.JeweBlend.GemItems:SetVisible(false)
  gUI.JeweBlend.NeedItems1:SetVisible(false)
  gUI.JeweBlend.NeedItems2:SetVisible(false)
  gUI.JeweBlend.NumEdit:SetProperty("Text", "0")
end
function JewelBlend_UpdateInfos(mainId, contect, subId, num)
  local parent = gUI.JeweBlend.TreeList:FindItemByData(mainId)
  if mainId == 0 then
    parent = gUI.JeweBlend.TreeList:InsertItem(contect, nil, "kaiti_13", 4282962380)
    parent:SetUserData(subId)
    parent:SetShrink(true)
  else
    local subItem
    if num > 0 then
      contect = contect .. " (" .. tostring(num) .. ")"
      subItem = gUI.JeweBlend.TreeList:InsertItem(contect, parent, "fzheiti_11", 4278255360)
    else
      subItem = gUI.JeweBlend.TreeList:InsertItem(contect, parent, "fzheiti_11", 4294903313)
    end
    subItem:SetUserData(subId)
  end
end
function JewelBlend_UpdateInfos_Begin()
  gUI.JeweBlend.TreeList:DeleteAllItems()
end
function UI_JewelBlend_Close(msg)
  gUI.JeweBlend.Root:SetVisible(false)
end
function UI_JewelBlend_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function JewelBlend_UpdateInfos_End()
  gUI.JeweBlend.TreeList:LockUpdate(false)
  if JEWEBLEND_SLEID ~= -1 then
    local currentSel = gUI.JeweBlend.TreeList:SearchItemByData(JEWEBLEND_SLEID)
    if currentSel then
      gUI.JeweBlend.TreeList:SetSelectedItem(currentSel)
    end
  else
    gUI.JeweBlend.TreeList:SelectFirstChild(1)
  end
end
function UI_JewelBlend_TreeListClick(msg)
  local currentSelect = msg:get_lparam()
  if currentSelect >= 3400 and currentSelect <= 4000 or currentSelect == 100 then
    JewelBlend_ClearInfos()
    JEWEBLEND_SLEID = -1
  else
    JEWEBLEND_SLEID = currentSelect
    JewelBlend_UpdateSelInfos()
    local money, maxNum, nimbus = GetManufactureCostAndMaxNum(JEWEBLEND_SLEID, 1)
    if maxNum > 0 then
      gUI.JeweBlend.NumEdit:SetProperty("Text", "1")
    else
      gUI.JeweBlend.NumEdit:SetProperty("Text", "1")
    end
    local maxNumText = gUI.JeweBlend.NumEdit:GetProperty("Text")
    gUI.JeweBlend.MoneyLab:SetProperty("Text", Lua_UI_Money2String(money * tonumber(maxNumText), true, false, false, true))
    gUI.JeweBlend.MoneyLab:SetCustomUserData(0, money * tonumber(maxNumText))
    gUI.JeweBlend.NimbusLab:SetProperty("Text", Lua_UI_Niubus2String(nimbus * tonumber(maxNumText), true))
    gUI.JeweBlend.NimbusLab:SetCustomUserData(0, nimbus * tonumber(maxNumText))
    if nimbus == 0 then
      gUI.JeweBlend.Nimbus:SetProperty("Visible", "false")
    else
      gUI.JeweBlend.Nimbus:SetProperty("Visible", "true")
    end
  end
end
function JewelBlend_UpdateSelInfos()
  if JEWEBLEND_SLEID ~= -1 then
    JewelBlend_Notity_SelInfo(JEWEBLEND_SLEID, 1)
  end
end
function JewelBlend_UpdateGoodsBox(itemId1, itemId2, NeedNums1, NeedNums2, gb)
  if gb == 0 then
    JewelBlend_UpdateGB1(itemId1)
  elseif gb == 1 then
    JewelBlend_UpdateGB2(itemId1, itemId2, NeedNums1, gb)
  elseif gb == 2 then
    JewelBlend_UpdateGB2(itemId1, itemId2, NeedNums1, gb)
  end
end
function JewelBlend_UpdateGB1(itemId1)
  gUI.JeweBlend.GemItems:SetVisible(true)
  JewelBlend_UpdateGbByItemId(itemId1, gUI.JeweBlend.GemItems, 0)
end
function JewelBlend_UpdateGB2(itemId1, itemId2, NeedNums1, gb)
  if gb == 1 then
    gUI.JeweBlend.NeedItems1:SetVisible(true)
    JewelBlend_UpdateGbByItemId(itemId1, gUI.JeweBlend.NeedItems1, NeedNums1, itemId2)
  elseif gb == 2 then
    gUI.JeweBlend.NeedItems2:SetVisible(true)
    JewelBlend_UpdateGbByItemId(itemId1, gUI.JeweBlend.NeedItems2, NeedNums1, itemId2)
  end
end
function UI_JewelBlend_NumChange(msg)
  local maxNumText = gUI.JeweBlend.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    maxNumText = "1"
  end
  local nums = JewelBlend_GetRealNum(maxNumText)
  gUI.JeweBlend.NumEdit:SetProperty("Text", tostring(nums))
end
function UI_JewelBlend_ReduceClick(msg)
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  local maxNumText = gUI.JeweBlend.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    maxNumText = 1
  end
  local MaxNumText = tonumber(maxNumText) - 1
  local nums = JewelBlend_GetRealNum(MaxNumText)
  gUI.JeweBlend.NumEdit:SetProperty("Text", tostring(nums))
end
function UI_JewelBlend_AddClick(msg)
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  local maxNumText = gUI.JeweBlend.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    maxNumText = 1
  end
  local MaxNumText = tonumber(maxNumText) + 1
  local nums = JewelBlend_GetRealNum(MaxNumText)
  gUI.JeweBlend.NumEdit:SetProperty("Text", tostring(nums))
end
function JewelBlend_GetRealNum(maxNumTextNew)
  if JEWEBLEND_SLEID ~= -1 then
    local money, maxNum, niubus = GetManufactureCostAndMaxNum(JEWEBLEND_SLEID, 1)
    local maxNumText = tonumber(maxNumTextNew)
    if maxNum < tonumber(maxNumTextNew) then
      maxNumText = maxNum
      Lua_Chat_ShowOSD("JEWELBLEND_MAX")
    end
    if maxNumText > MAXJEWEBLENDNUM then
      maxNumText = MAXJEWEBLENDNUM
    end
    if 1 > tonumber(maxNumText) then
      maxNumText = 1
    end
    if 1 > tonumber(maxNumTextNew) then
      maxNumText = 1
    end
    gUI.JeweBlend.MoneyLab:SetProperty("Text", Lua_UI_Money2String(money * tonumber(maxNumText), true, false, false, true))
    gUI.JeweBlend.MoneyLab:SetCustomUserData(0, money * tonumber(maxNumText))
    gUI.JeweBlend.NimbusLab:SetProperty("Text", Lua_UI_Niubus2String(niubus * tonumber(maxNumText), true))
    gUI.JeweBlend.NimbusLab:SetCustomUserData(0, niubus * tonumber(maxNumText))
    return maxNumText
  end
  return 0
end
function UI_JewelBlend_DoAction(msg)
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  local maxNumText = gUI.JeweBlend.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    Lua_Chat_ShowOSD("JEWELBLEND_CANDO")
    return
  end
  local money, maxNum = GetManufactureCostAndMaxNum(JEWEBLEND_SLEID, 1)
  if JEWEBLEND_SLEID > 0 then
    if 0 < tonumber(maxNum) then
      local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
      local moneyNeed = money * tonumber(maxNumText)
      if moneyNeed > nonBindemoney + bindMoney then
        Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
        return
      end
      local count1, count2 = JewelBlend_SelInfo_BindId(JEWEBLEND_SLEID, 1)
      if count1 > 0 or count2 > 0 then
        Messagebox_Show("CONFIRM_JEWELBLEND", tonumber(maxNumText), JEWEBLEND_SLEID)
      else
        Manufacture_StartMake(JEWEBLEND_SLEID, tonumber(maxNumText))
      end
    else
      Lua_Chat_ShowOSD("JEWELBLEND_CANDO1")
    end
  else
    Lua_Chat_ShowOSD("JEWELBLEND_CANDO3")
  end
end
function JewelBlend_UpdateGbByItemId(itemId, GB, NeedNum, itemId2)
  local goodbox = WindowToGoodsBox(GB:GetGrandChild("Material_gbox"))
  local namelab = WindowToLabel(GB:GetGrandChild("Material_dlab"))
  local numlab = WindowToLabel(GB:GetGrandChild("Count_dlab"))
  local szicon, szname, count, quilty = GetItemInfosByTable(itemId, itemId2)
  goodbox:SetCustomUserData(0, itemId)
  goodbox:SetItemGoods(0, szicon, quilty)
  namelab:SetProperty("Text", "{#C^" .. tostring(gUI_GOODS_QUALITY_COLOR_PREFIX[quilty]) .. "^" .. szname .. "}")
  local color = colorGreen
  if NeedNum > count then
    color = colorRed
  end
  numlab:SetProperty("Text", "{#C^" .. color .. "^" .. tostring(count) .. "/" .. tostring(NeedNum) .. "}")
end
function JewelBlend_ChangeNiubus()
  if gUI.JeweBlend.Root:IsVisible() then
    local itemMoney = tonumber(gUI.JeweBlend.NimbusLab:GetCustomUserData(0))
    gUI.JeweBlend.NimbusLab:SetProperty("Text", Lua_UI_Niubus2String(itemMoney, true))
  end
end
function JewelBlend_SetMoney(newMoney, changeMoney, event)
  if gUI.JeweBlend.Root:IsVisible() then
    local itemMoney = tonumber(gUI.JeweBlend.MoneyLab:GetCustomUserData(0))
    gUI.JeweBlend.MoneyLab:SetProperty("Text", Lua_UI_Money2String(itemMoney, true, false, false, true, newMoney, event))
  end
end
function Script_JewelBlend_OnEvent(event)
  if event == "GEMCOM_UPDATEINFOS" then
    JewelBlend_UpdateInfos(arg1, arg2, arg3, arg4)
  elseif event == "GEMCOM_UPDATEINFOS_BEGIN" then
    JewelBlend_UpdateInfos_Begin()
  elseif event == "GEMCOM_UPDATEINFOS_END" then
    JewelBlend_UpdateInfos_End()
  elseif event == "GEMCOM_UPDATE_GOODBOXS" then
    JewelBlend_UpdateGoodsBox(arg1, arg2, arg3, arg4, arg5)
  elseif event == "GEM_UPGRADE_SHOWEX" then
    JewelBlend_OpenPanel()
  elseif event == "PLAYER_INFO_CHANGED" then
    JewelBlend_ChangeNiubus()
  elseif event == "PLAYER_MONEY_CHANGED" then
    JewelBlend_SetMoney(arg1, arg2, event)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    JewelBlend_SetMoney(arg1, arg2, event)
  elseif event == "ITEM_ADD_TO_BAG" then
    JewelBlend_UpdateList()
  elseif event == "ITEM_DEL_FROM_BAG" then
    JewelBlend_UpdateList()
  elseif "CLOSE_NPC_RELATIVE_DIALOG" == event then
    UI_JewelBlend_Close()
  end
end
function _JeweBlend_JewTime(timer)
  IS_FLASFLAG = true
  GemCom_PanelOpen()
  gUI.JeweBlend.JewTime:Stop()
end
function Script_JewelBlend_OnHide()
end
function Script_JewelBlend_OnLoad()
  gUI.JeweBlend.Root = WindowSys_Instance:GetWindow("JewelBlend")
  gUI.JeweBlend.TreeList = WindowToTreeCtrl(WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Top_bg.List_tctl"))
  gUI.JeweBlend.GemItems = WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Center_bg.Need_bg")
  gUI.JeweBlend.NeedItems1 = WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Center_bg.MaterialMain1_bg.Material1_bg")
  gUI.JeweBlend.NeedItems2 = WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Center_bg.MaterialMain1_bg.Material2_bg")
  gUI.JeweBlend.NimbusLab = WindowToLabel(WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Down_bg.Nimbus_bg.Nimbus_dlab"))
  gUI.JeweBlend.MoneyLab = WindowToLabel(WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Down_bg.Money_bg.Money_dlab"))
  gUI.JeweBlend.Nimbus = WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Down_bg.Nimbus_bg")
  gUI.JeweBlend.NumEdit = WindowToEditBox(WindowSys_Instance:GetWindow("JewelBlend.GemCompose_bg.Down_bg.Pag_bg.Num_ebox"))
  gUI.JeweBlend.JewTime = TimerSys_Instance:CreateTimerObject("JewTime", 1, -1, "_JeweBlend_JewTime", false, false)
end
