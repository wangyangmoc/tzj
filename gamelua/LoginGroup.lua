if gUI then
  gUI.account_name = ""
end
local _LoginGrp_SoftRandNumberKeyNum = 13
local _LoginGrp_SoftRandKeyNumber = 47
local _LoginGrp_SoftKeyBoardLower = {
  "`",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "0",
  "-",
  "=",
  "q",
  "w",
  "e",
  "r",
  "t",
  "y",
  "u",
  "i",
  "o",
  "p",
  "[",
  "]",
  "\\",
  "a",
  "s",
  "d",
  "f",
  "g",
  "h",
  "j",
  "k",
  "l",
  ";",
  "'",
  "z",
  "x",
  "c",
  "v",
  "b",
  "n",
  "m",
  ",",
  ".",
  "/",
  "\b",
  "\r",
  " "
 }
local _LoginGrp_SoftKeyBoardUpper = {
  "~",
  "!",
  "@",
  "#",
  "$",
  "%",
  "^",
  "&",
  "*",
  "(",
  ")",
  "_",
  "+",
  "Q",
  "W",
  "E",
  "R",
  "T",
  "Y",
  "U",
  "I",
  "O",
  "P",
  "{",
  "}",
  "|",
  "A",
  "S",
  "D",
  "F",
  "G",
  "H",
  "J",
  "K",
  "L",
  ":",
  [[\]],
  "Z",
  "X",
  "C",
  "V",
  "B",
  "N",
  "M",
  "<",
  ">",
  "?",
  " ",
}
local _LoginGrp_WebsiteFlag = {
  [0] = "找回密码",
  "官网首页",
  "账号充值"
}
function _LoginGrp_MapKeyDataToUI()
  for i = 1, _LoginGrp_SoftRandKeyNumber do
    local keyBtnWndName = "LoginGroup.Keyboard_bg.Button" .. tostring(i)
    local keyWindow = WindowSys_Instance:GetWindow(keyBtnWndName)
    keyWindow:SetProperty("Text", tostring(g_currentSoftKeyBoard[i]))
  end
end
function _LoginGrp_RandSoftKeyBoard()
  local softRandKeyNumber = _LoginGrp_SoftRandKeyNumber
  local softRandNumberKeyNum = _LoginGrp_SoftRandNumberKeyNum
  local lowerKeyBoardTemp = {}
  local upperKeyBoardTemp = {}
  for i = 1, softRandKeyNumber do
    lowerKeyBoardTemp[i] = _LoginGrp_SoftKeyBoardLower[i]
    upperKeyBoardTemp[i] = _LoginGrp_SoftKeyBoardUpper[i]
  end
  for i = 1, softRandNumberKeyNum do
    local rdIndex = math.random(1, softRandNumberKeyNum - i + 1)
    _LoginGrp_SoftKeyBoardLower[i] = lowerKeyBoardTemp[rdIndex]
    _LoginGrp_SoftKeyBoardUpper[i] = upperKeyBoardTemp[rdIndex]
    table.remove(lowerKeyBoardTemp, rdIndex)
    table.remove(upperKeyBoardTemp, rdIndex)
  end
  local leftSoftRandKeyNumber = softRandKeyNumber - softRandNumberKeyNum
  for i = 1, leftSoftRandKeyNumber do
    local rdIndex = math.random(1, leftSoftRandKeyNumber - i + 1)
    _LoginGrp_SoftKeyBoardLower[i + softRandNumberKeyNum] = lowerKeyBoardTemp[rdIndex]
    _LoginGrp_SoftKeyBoardUpper[i + softRandNumberKeyNum] = upperKeyBoardTemp[rdIndex]
    table.remove(lowerKeyBoardTemp, rdIndex)
    table.remove(upperKeyBoardTemp, rdIndex)
  end
  _LoginGrp_MapKeyDataToUI()
end
local g_currentSoftKeyBoard = _LoginGrp_SoftKeyBoardLower
local AccountInfo = {remember = false, account = ""}
function UI_LoginGrp_Clicked(msg)
  local root = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
  local rem = WindowToCheckButton(root:GetGrandChild("Account_bg.RemAcc_cbtn"))
  local acc = WindowToEditBox(root:GetGrandChild("Account_bg.Edit_ebox"))
  local account_name = acc:GetProperty("Text")
  local pw = WindowToEditBox(root:GetGrandChild("Password_bg.Edit_ebox"))
  local password = pw:GetProperty("Text")
  local loginBtn = WindowToButton(root:GetGrandChild("Accept_btn"))
  if loginBtn:IsEnable() == false then
    return
  end
  loginBtn:beginClickCDShow(true)
  if string.len(account_name) ~= 0 and string.len(password) ~= 0 then
    if false == Login(account_name, "") then
      MessageboxTypes.NOTIFY_MSG.has_button1 = true
      MessageboxTypes.NOTIFY_MSG.text = "账号名含有非法字符"
      MessageboxTypes.NOTIFY_MSG.text1 = ""
      MessageboxTypes.NOTIFY_MSG.text2 = ""
      MessageboxTypes.NOTIFY_MSG.text3 = ""
      Messagebox_Show("NOTIFY_MSG")
    end
    AccountInfo.name = account_name
    AccountInfo.remember = rem:IsChecked()
    gUI.account_name = account_name
  elseif string.len(account_name) == 0 then
    WindowSys_Instance:SetKeyboardCaptureWindow(acc)
    MessageboxTypes.NOTIFY_MSG.has_button1 = true
    MessageboxTypes.NOTIFY_MSG.text = "请先输入帐号后登录"
    MessageboxTypes.NOTIFY_MSG.text1 = ""
    MessageboxTypes.NOTIFY_MSG.text2 = ""
    MessageboxTypes.NOTIFY_MSG.text3 = ""
    Messagebox_Show("NOTIFY_MSG")
  elseif string.len(password) == 0 then
    WindowSys_Instance:SetKeyboardCaptureWindow(pw)
    MessageboxTypes.NOTIFY_MSG.has_button1 = true
    MessageboxTypes.NOTIFY_MSG.text = "请先输入密码后登录"
    MessageboxTypes.NOTIFY_MSG.text1 = ""
    MessageboxTypes.NOTIFY_MSG.text2 = ""
    MessageboxTypes.NOTIFY_MSG.text3 = ""
    Messagebox_Show("NOTIFY_MSG")
  end
end
function UI_LoginGrp_QuitClicked(msg)
  Messagebox_Show("WILL_NOT_LOGIN")
end
function UI_LoginGrp_KeyDown(msg)
  if msg:get_wparam() == 28 then
    UI_LoginGrp_Clicked(0)
  end
end
function UI_LoginGrp_Enter(msg)
  UI_LoginGrp_Clicked(0)
end
function UI_LoginGrp_InputPassword(msg)
  local editbox = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg.Password_bg.Edit_ebox")
  WindowSys_Instance:SetKeyboardCaptureWindow(editbox)
end
function UI_LoginGrp_InputAccount(msg)
  local editbox = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg.Account_bg.Edit_ebox")
  WindowSys_Instance:SetKeyboardCaptureWindow(editbox)
end
function UI_LoadingGrp_QiutQueue(msg)
  local wnd = WindowSys_Instance:GetWindow("Queue")
  wnd:SetProperty("Visible", "false")
  Disconnect()
end
function UI_LoginGrp_RemAcc(msg)
  local wnd = msg:get_window()
  local cbtn = WindowToCheckButton(wnd)
  if cbtn:IsChecked() then
    Messagebox_Show("REMACC")
  end
  cbtn:SetChecked(false)
end
function UI_LoginGrp_SoftCharKey_Press(msg)
  local keyIndex = msg:get_window():GetCustomUserData(0)
  local keyVaule = g_currentSoftKeyBoard[keyIndex]
  if keyVaule then
    Lua_InjectChar(keyVaule)
  end
end
function UI_LoginGrp_SwitchKey_Press(msg)
  if g_currentSoftKeyBoard == _LoginGrp_SoftKeyBoardLower then
    g_currentSoftKeyBoard = _LoginGrp_SoftKeyBoardUpper
  else
    g_currentSoftKeyBoard = _LoginGrp_SoftKeyBoardLower
  end
  _LoginGrp_MapKeyDataToUI()
end
function UI_LoginGrp_ShowRegisterInterface(msg)
  OpenWebPage("http://passport.iwgame.com/reg/account/regpage.do?type=p")
end
function UI_LoginGrp_OpenWebsite(msg)
  OpenWebPage("http://pay.iwgame.com/")
end
function UI_LoginGrp_ForgetPassword(msg)
  OpenWebPage("http://cs.iwgame.com/cs/index.do")
end
function UI_LoginGrp_AccountPay(msg)
  OpenWebPage("http://tzj.iwgame.com/")
end
function UI_LoginGrp_SelectServerBtnClick(msg)
  Lua_LoginGrp_ShowServerUI(true)
  LoadServerList(false)
  StartDownLoadServerInfo()
end
function Lua_LoginGrp_SendActivePasswordMsgToServer(msg)
  SendMegToServerPassword(msg)
end
function Lua_LoginGrp_GetAccountInfo()
  return AccountInfo
end
function Lua_LoginGrp_ShowServerUI(bShow)
  local show_str = true
  if not bShow then
    show_str = false
  end
  WindowSys_Instance:GetWindow("Server"):SetProperty("Visible", tostring(show_str))
  WindowSys_Instance:GetWindow("LoginGroup.BtnLeft_bg"):SetProperty("Visible", tostring(not show_str))
  WindowSys_Instance:GetWindow("LoginGroup.BtnRight_bg"):SetProperty("Visible", tostring(not show_str))
end
function _OnLoginGrp_OpenMessboxActivePassword()
  local passWin = Messagebox_Show("EDITPASSWORD")
  local pass = WindowToEditBox(passWin.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
  WindowToEditBox(pass):SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowToEditBox(pass))
  WindowSys_Instance:SetFocusWindow(pass)
end
function _OnLoginGrp_OpenLogin(isShowError)
  local passWin = Messagebox_Show("EDITPASSWORD1", isShowError)
  local pass = WindowToEditBox(passWin.binded_widget:GetGrandChild("passwords_bg.Edit_ebox"))
  WindowToEditBox(pass):SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowToEditBox(pass))
  WindowSys_Instance:SetFocusWindow(pass)
end
function _OnLoginGrp_ShowLast(groupName, serverName)
  local wnd = WindowSys_Instance:GetWindow("LoginGroup.BtnLeft_bg.Area_dlab")
  if string.len(groupName) > 0 and string.len(serverName) > 0 then
    wnd:SetProperty("Text", groupName .. "   " .. serverName)
  end
end
function _OnLoginGrp_LineQueInfo(name)
  local wnd = WindowSys_Instance:GetWindow("Queue")
  if not wnd:IsVisible() then
    wnd:SetVisible(true)
  end
  local queueInfoWnd = wnd:GetChildByName("Quantity")
  queueInfoWnd:SetProperty("Text", tostring(name))
end
function Script_LoginGrp_OnLoad(this)
  gUI_CurrentStage = 1
  local root = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
  local rem = WindowToCheckButton(root:GetGrandChild("Account_bg.RemAcc_cbtn"))
  local acc = WindowToEditBox(root:GetGrandChild("Account_bg.Edit_ebox"))
  local pw = WindowToEditBox(root:GetGrandChild("Password_bg.Edit_ebox"))
  local loginBtn = WindowToButton(root:GetGrandChild("Accept_btn"))
  loginBtn:beginClickCDShow(false)
  local remember = UIConfig:getAccountOption()
  local name = UIConfig:getAccountName()
  if remember then
    acc:SetProperty("Text", name)
    AccountInfo.name = name
    WindowSys_Instance:SetKeyboardCaptureWindow(pw)
  else
    acc:SetProperty("Text", "")
    AccountInfo.name = ""
    WindowSys_Instance:SetKeyboardCaptureWindow(acc)
  end
  rem:SetCheckedState(remember)
  AccountInfo.remember = remember
  pw:SetProperty("Text", "")
  WorldStage:playMusic("0017.ogg", true)
  Lua_LoginGrp_ShowServerUI(true)
end
function Script_LoginGrp_OnEvent(event)
  if event == "LOGIN_LINE_QUEUE_INFO" then
    _OnLoginGrp_LineQueInfo(arg1)
  elseif event == "LOGIN_GET_LAST_SERVER" then
    _OnLoginGrp_ShowLast(arg1, arg2)
  elseif event == "LOGIN_SHOWPASS_ACTIVE" then
    _OnLoginGrp_OpenMessboxActivePassword()
  elseif event == "LOGIN_SHOW_LOGIN" then
    _OnLoginGrp_OpenLogin(arg1)
  end
end
