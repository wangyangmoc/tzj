if gUI and not gUI.Manufacture then
  gUI.Manufacture = {}
end
local MANUFACETURE_SLEID = -1
local MAXMANUFATERNUM = 99
function Manufacture_OpenPanel()
  if gUI.Manufacture.Root:IsVisible() then
    gUI.Manufacture.Root:SetVisible(false)
  else
    gUI.Manufacture.Root:SetVisible(true)
    Manufacture_ClearInfos()
    Notity_Manufacture_Open()
  end
end
function Notity_Manufacture_Open()
  Manufacture_PanelOpen()
end
function Manufacture_UpdateBegin()
  gUI.Manufacture.TreeList:DeleteAllItems()
end
function UI_Manufacture_Close(msg)
  gUI.Manufacture.Root:SetVisible(false)
end
function UI_Manufacture_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function Manufacture_UpdateList()
  if gUI.Manufacture.Root:IsVisible() then
    Manufacture_PanelOpen()
  end
end
function Manufacture_UpdateInfos(mainId, contect, subId, num)
  local parent = gUI.Manufacture.TreeList:FindItemByData(mainId)
  if mainId == 0 then
    parent = gUI.Manufacture.TreeList:InsertItem(contect, nil, "kaiti_13", 4282962380)
    parent:SetUserData(subId)
    parent:SetShrink(true)
  else
    local subItem
    if num > 0 then
      contect = contect .. " (" .. tostring(num) .. ")"
      subItem = gUI.Manufacture.TreeList:InsertItem(contect, parent, "songti_9", 4278255360)
    else
      subItem = gUI.Manufacture.TreeList:InsertItem(contect, parent, "songti_9", 4294903313)
    end
    subItem:SetUserData(subId)
  end
end
function Manufacture_UpdateEnd()
  gUI.Manufacture.TreeList:LockUpdate(false)
  if MANUFACETURE_SLEID ~= -1 then
    local currentSel = gUI.Manufacture.TreeList:SearchItemByData(MANUFACETURE_SLEID)
    if currentSel then
      gUI.Manufacture.TreeList:SetSelectedItem(currentSel)
    end
  else
    gUI.Manufacture.TreeList:SelectFirstChild(1)
  end
end
function UI_Manufacture_TreeListClick(msg)
  local currentSelect = msg:get_lparam()
  if currentSelect >= 3000 and currentSelect <= 3100 then
    Manufacture_ClearInfos()
    MANUFACETURE_SLEID = -1
  else
    MANUFACETURE_SLEID = currentSelect
    Manufacture_UpdateSelInfos()
    local money, maxNum = GetManufactureCostAndMaxNum(MANUFACETURE_SLEID, 0)
    if maxNum > 0 then
      gUI.Manufacture.NumEdit:SetProperty("Text", "1")
    else
      gUI.Manufacture.NumEdit:SetProperty("Text", "1")
    end
    local maxNumText = gUI.Manufacture.NumEdit:GetProperty("Text")
    gUI.Manufacture.CostLab:SetProperty("Text", Lua_UI_Money2String(money * tonumber(maxNumText), true))
  end
end
function Manufacture_ClearInfos()
  gUI.Manufacture.CostLab:SetProperty("Text", Lua_UI_Money2String(0))
  gUI.Manufacture.GenItems:SetVisible(false)
  gUI.Manufacture.GenItems:setEnableHeightCal(false)
  gUI.Manufacture.GenItems:SetProperty("Height", "p0")
  for i = 0, 2 do
    gUI.Manufacture.MaterialGoodsBox[i]:SetVisible(false)
    gUI.Manufacture.MaterialGoodsBox[i]:setEnableHeightCal(false)
    gUI.Manufacture.MaterialGoodsBox[i]:SetProperty("Height", "p0")
  end
  gUI.Manufacture.NumEdit:SetProperty("Text", "0")
end
function Manufacture_UpdateSelInfos()
  if MANUFACETURE_SLEID ~= -1 then
    Manufacture_Notity_SelInfo(MANUFACETURE_SLEID, 0)
  end
end
function UI_Manufacture_NumChange(msg)
  local maxNumText = gUI.Manufacture.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    maxNumText = "1"
  end
  local nums = Maunfacture_GetRealNum(maxNumText)
  gUI.Manufacture.NumEdit:SetProperty("Text", tostring(nums))
end
function UI_Manufacture_ReduceClick(msg)
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  local maxNumText = gUI.Manufacture.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    maxNumText = 1
  end
  local MaxNumText = tonumber(maxNumText) - 1
  local nums = Maunfacture_GetRealNum(MaxNumText)
  gUI.Manufacture.NumEdit:SetProperty("Text", tostring(nums))
end
function UI_Manufacture_AddClick(msg)
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  local maxNumText = gUI.Manufacture.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    maxNumText = 1
  end
  local MaxNumText = tonumber(maxNumText) + 1
  local nums = Maunfacture_GetRealNum(MaxNumText)
  gUI.Manufacture.NumEdit:SetProperty("Text", tostring(nums))
end
function Maunfacture_GetRealNum(maxNumTextNew)
  if MANUFACETURE_SLEID ~= -1 then
    local money, maxNum = GetManufactureCostAndMaxNum(MANUFACETURE_SLEID, 0)
    local maxNumText = tonumber(maxNumTextNew)
    if maxNum < tonumber(maxNumTextNew) then
      maxNumText = maxNum
      Lua_Chat_ShowOSD("MANUFACTURE_MAX")
    end
    if maxNumText > MAXMANUFATERNUM then
      maxNumText = MAXMANUFATERNUM
    end
    if tonumber(maxNumText) < 1 then
      maxNumText = 1
    end
    if tonumber(maxNumTextNew) < 1 then
      maxNumText = 1
    end
    gUI.Manufacture.CostLab:SetProperty("Text", Lua_UI_Money2String(money * tonumber(maxNumText), true))
    return maxNumText
  end
  return 0
end
function UI_Manufacture_DoAction(msg)
  WindowSys_Instance:SetKeyboardCaptureWindow(nil)
  local maxNumText = gUI.Manufacture.NumEdit:GetProperty("Text")
  if maxNumText == "" then
    Lua_Chat_ShowOSD("MANUFACTURE_CANDO")
    return
  end
  local money, maxNum = GetManufactureCostAndMaxNum(MANUFACETURE_SLEID, 0)
  if 0 < tonumber(maxNum) then
    if MANUFACETURE_SLEID > 0 then
      local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
      local moneyNeed = money * tonumber(maxNumText)
      if nonBindemoney < moneyNeed then
        Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
        return
      end
      Manufacture_StartMake(MANUFACETURE_SLEID, tonumber(maxNumText))
    end
  else
    Lua_Chat_ShowOSD("MANUFACTURE_CANDO1")
  end
end
function Maunfacture_UpdateGoodsBox(itemId1, itemId2, NeedNums1, NeedNums2, gb)
  if gb == 0 then
    Maunfaceture_UpdateGb1(itemId1)
  else
    Maunfaceture_UpdateGb2(itemId1, itemId2, NeedNums1, NeedNums2, gb)
  end
end
function Maunfaceture_UpdateGb1(itemId1)
  gUI.Manufacture.GenItems:SetVisible(false)
  gUI.Manufacture.GenItems:setEnableHeightCal(false)
  gUI.Manufacture.GenItems:SetProperty("Height", "p0")
  for i = 0, 2 do
    gUI.Manufacture.MaterialGoodsBox[i]:SetVisible(false)
    gUI.Manufacture.MaterialGoodsBox[i]:setEnableHeightCal(false)
    gUI.Manufacture.MaterialGoodsBox[i]:SetProperty("Height", "p0")
  end
  gUI.Manufacture.GenItems:SetVisible(true)
  gUI.Manufacture.GenItems:setEnableHeightCal(true)
  gUI.Manufacture.GenItems:SetProperty("Height", "p66")
  Manufaceture_UpdateGbByItemId(itemId1, gUI.Manufacture.GenItems, 0)
end
function Maunfaceture_UpdateGb2(itemId1, itemId2, NeedNums1, NeedNums2, gb)
  local gbItem1 = gUI.Manufacture.MaterialGoodsBox[gb - 1]:GetGrandChild("Material1_bg")
  local gbItem2 = gUI.Manufacture.MaterialGoodsBox[gb - 1]:GetGrandChild("Material2_bg")
  if itemId1 > 0 then
    gUI.Manufacture.MaterialGoodsBox[gb - 1]:SetVisible(true)
    gUI.Manufacture.MaterialGoodsBox[gb - 1]:setEnableHeightCal(true)
    gUI.Manufacture.MaterialGoodsBox[gb - 1]:SetProperty("Height", "p49")
    gbItem1:SetVisible(false)
    gbItem2:SetVisible(false)
    gbItem1:SetVisible(true)
    Manufaceture_UpdateGbByItemId(itemId1, gbItem1, NeedNums1)
  end
  if itemId2 > 0 then
    gbItem2:SetVisible(true)
    Manufaceture_UpdateGbByItemId(itemId2, gbItem2, NeedNums2)
  end
end
function Manufaceture_UpdateGbByItemId(itemId, GB, NeedNum)
  local goodbox = WindowToGoodsBox(GB:GetGrandChild("Material_gbox"))
  local namelab = WindowToLabel(GB:GetGrandChild("Material_dlab"))
  local numlab = WindowToLabel(GB:GetGrandChild("Count_dlab"))
  local szicon, szname, count, quilty = GetItemInfosByTable(itemId)
  goodbox:SetCustomUserData(0, itemId)
  goodbox:SetItemGoods(0, szicon, quilty)
  namelab:SetProperty("Text", "{#C^" .. tostring(gUI_GOODS_QUALITY_COLOR_PREFIX[quilty]) .. "^" .. szname .. "}")
  local color = colorGreen
  if NeedNum > count then
    color = colorRed
  end
  numlab:SetProperty("Text", "{#C^" .. color .. "^" .. tostring(count) .. "/" .. tostring(NeedNum) .. "}")
end
function UI_ShowItemTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  if win:IsItemHasIcon(0) then
    local itemId = win:GetCustomUserData(0)
    Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemId)
  end
end
function Script_Manufacture_OnEvent(event)
  if event == "MANUFACTURE_UPDATEINFOS_BEGIN" then
    Manufacture_UpdateBegin()
  elseif event == "MANUFACTURE_UPDATEINFOS" then
    Manufacture_UpdateInfos(arg1, arg2, arg3, arg4)
  elseif event == "MANUFACTURE_UPDATEINFOS_END" then
    Manufacture_UpdateEnd()
  elseif event == "ITEM_ADD_TO_BAG" then
    Manufacture_UpdateList()
  elseif event == "ITEM_DEL_FROM_BAG" then
    Manufacture_UpdateList()
  elseif event == "MANUFACTURE_UPDATE_GOODBOXS" then
    Maunfacture_UpdateGoodsBox(arg1, arg2, arg3, arg4, arg5)
  end
end
function Script_Manufacture_OnLoad()
  gUI.Manufacture.Root = WindowSys_Instance:GetWindow("Manufacture")
  gUI.Manufacture.TreeList = WindowToTreeCtrl(WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Top_bg.List_tctl"))
  gUI.Manufacture.GenItems = WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Center_bg.Need_cnt.Need_bg")
  gUI.Manufacture.MaterialGoodsBox = {}
  gUI.Manufacture.MaterialGoodsBox[0] = WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Center_bg.Need_cnt.MaterialMain1_bg")
  gUI.Manufacture.MaterialGoodsBox[1] = WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Center_bg.Need_cnt.MaterialMain2_bg")
  gUI.Manufacture.MaterialGoodsBox[2] = WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Center_bg.Need_cnt.MaterialMain3_bg")
  gUI.Manufacture.CostLab = WindowToLabel(WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Down_bg.Money_bg.Money_dlab"))
  gUI.Manufacture.NumEdit = WindowToEditBox(WindowSys_Instance:GetWindow("Manufacture.Manufacture_bg.Down_bg.Pag_bg.Num_ebox"))
end
function Script_Manufacture_OnHide()
end
