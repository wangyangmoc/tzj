function Script_MiniMap_OnLoad()
  _MiniMap_InitUI()
end
function Script_MiniMap_OnShow()
end
function UI_MiniMap_MapClicked(msg)
  UI_AreaMap_Show()
end
function UI_MiniMap_HideMap(msg)
  local is_show = gUI.MiniMap.MiniMap:IsVisible()
  gUI.MiniMap.MiniMap:SetProperty("Visible", tostring(not is_show))
  gUI.MiniMap.HideMap:SetCheckedState(not is_show)
end
function Script_MiniMap_OnEvent(event)
  if event == "MINI_MAP_UPDATE_POS" then
    _MiniMap_UpdatePos(arg1, arg2, arg3, arg4, arg5)
  elseif event == "MINI_MAP_HIDE_TIP_INFO" then
    _MiniMap_HideTipInfo()
  elseif event == "MINI_MAP_SHOW_NPC_TIP" then
    _MiniMap_UpdateNpcTip(arg1, arg2, arg3)
  elseif event == "MINI_MAP_SHOW_TRANS_TIP" then
    _MiniMap_UpdateTransTip(arg1, arg2)
  elseif event == "MAIL_RECV_NEW" then
    _MiniMap_RecvNewMail(arg1)
  elseif event == "SYSMSG_RECV_NEW" then
    _MiniMap_RecvSysMail(arg1)
  elseif event == "MAIL_TOTAL_COUNT" then
    _MiniMap_TotalCount(arg1, arg2)
  elseif event == "MINI_MAP_SHOW_PLAYER_TIP" then
    _MiniMap_UpdatePlayerTip(arg1, arg2)
  elseif event == "MINI_CANSHOW_PLCAING" then
    _OnNotifty_ShowHideBtn(arg1)
  elseif event == "MAIL_EXPIRE_COUNT" then
    _MiniMap_ExpireCount(arg1, arg2)
  elseif event == "MINIMAP_UPDATELINE" then
    _MiniMap_UpdateLine()
  end
end
function _MiniMap_UpdateLine()
  local lineList = GetSenceLineData()
  if lineList[1] ~= nil then
    gUI.MiniMap.Junction:RemoveAllItems()
    if 1 < lineList[1].count then
      gUI.MiniMap.Junction:SetVisible(true)
      for i = 0, lineList[1].count - 1 do
        local curLine = lineList[1][i].LineFlag
        gUI.MiniMap.Junction:InsertString(tostring(curLine + 1) .. "线", -1)
        gUI.MiniMap.Junction:SetItemData(i, curLine)
      end
      gUI.MiniMap.Junction:SetEditBoxText(tostring(lineList[1].CurrentLine + 1) .. "线")
    else
      gUI.MiniMap.Junction:SetVisible(false)
    end
  end
end
function UI_MiniMap_JunctionSelect(msg)
  local Index = msg:get_wparam()
  local lineList = GetSenceLineData()
  local lineId = gUI.MiniMap.Junction:GetItemData(Index)
  gUI.MiniMap.Junction:SetEditBoxText(tostring(lineList[1].CurrentLine + 1) .. "线")
  if Index ~= -1 and lineList[1] ~= nil and lineId ~= lineList[1].CurrentLine then
    SetSenceLine(tonumber(lineId))
  end
end
function UI_MiniMap_BBS(msg)
  local account_name = ""
  if gUI.account_name then
    account_name = gUI.account_name
  end
  OpenWebPage("http://bbs.tzj.iwgame.com")
end
function _MiniMap_UpdatePos(MapName, SceneArea, X, Z, Time)
  local strPos = string.format("(%d,%d)", X, Z)
  local strUsed
  if string.len(SceneArea) ~= 0 then
    strUsed = SceneArea
  else
    strUsed = MapName
  end
  gUI.MiniMap.MapName:SetProperty("Text", strUsed)
  gUI.MiniMap.PosName:SetProperty("Text", strPos)
  if locLastSceneArea ~= SceneArea then
    locLastSceneArea = SceneArea
    if string.len(SceneArea) ~= 0 then
      Lua_UI_ShowMessageInfo("进入" .. SceneArea)
    end
  end
end
function _MiniMap_HideTipInfo()
  Lua_Tip_HideObjTooltip()
end
function _MiniMap_UpdateTransTip(Desc, LevelLimited)
  if gUI.MiniMap.MapPic:IsVisible() then
    Lua_Tip_ShowMapTransTip(gUI.MiniMap.MapToolTip, LevelLimited, Desc)
    local posx, posy = GetMousePos()
    gUI.MiniMap.MapToolTip:SetLeft(posx - 0.01 - string.len(Desc) * 0.0075)
    gUI.MiniMap.MapToolTip:SetTop(posy + 0.01)
  end
end
function _MiniMap_UpdatePlayerTip(name, relation)
  if gUI.MiniMap.MapPic:IsVisible() then
    local Text = ""
    local color = "FFFAFAFA"
    if relation == 1 then
      Text = name
      color = "FF6A5ACD"
    elseif relation == 2 then
      Text = string.format("%s<仇人>", name)
      color = "FFFF0611"
    else
      Text = name
    end
    Lua_Tip_ShowMiniMapTip(gUI.MiniMap.MapToolTip, Text, color)
    local posx, posy = GetMousePos()
    gUI.MiniMap.MapToolTip:SetLeft(posx - 0.01 - string.len(Text) * 0.0075)
    gUI.MiniMap.MapToolTip:SetTop(posy + 0.01)
  end
end
function _MiniMap_UpdateNpcTip(Name, Title, NpcID)
  if gUI.MiniMap.MapPic:IsVisible() then
    local Text = Name .. "<" .. Title .. ">"
    if Title == "" then
      Text = Name
    end
    Lua_Tip_ShowMiniMapTip(gUI.MiniMap.MapToolTip, Text)
    local posx, posy = GetMousePos()
    gUI.MiniMap.MapToolTip:SetLeft(posx - 0.01 - string.len(Text) * 0.0075)
    gUI.MiniMap.MapToolTip:SetTop(posy + 0.01)
  end
end
function _OnNotifty_ShowHideBtn(b_show)
  gUI.MiniMap.RankDark:SetVisible(b_show)
end
function _MiniMap_InitUI()
  gUI.MiniMap = {}
  gUI.MiniMap.MapName = WindowSys_Instance:GetWindow("MiniMap.Text_bg.MapName_dlab")
  gUI.MiniMap.PosName = WindowSys_Instance:GetWindow("MiniMap.Text_bg.Pos_dlab")
  gUI.MiniMap.MiniMap = WindowSys_Instance:GetWindow("MiniMap.Show_bg")
  gUI.MiniMap.HideMap = WindowToCheckButton(WindowSys_Instance:GetWindow("MiniMap.Text_bg.HideMap_Cbtn"))
  gUI.MiniMap.MapPic = WindowSys_Instance:GetWindow("MiniMap.Show_bg.Showmap_pic.Pic1_bg")
  gUI.MiniMap.MapObj = UIInterface:GetMiniMap()
  gUI.MiniMap.MapToolTip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  gUI.MiniMap.Email = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Mail_pic")
  gUI.MiniMap.RankDark = WindowSys_Instance:GetWindow("MiniMap.Show_bg.Placing_btn")
  gUI.MiniMap.ServerTime = WindowSys_Instance:GetWindow("MiniMap.ServerTime_dlab")
  UIInterface:BindMiniMap(gUI.MiniMap.MapPic)
  locLastSceneArea = ""
  locUnreadNormalCount = 0
  locUnreadSystemCount = 0
  locNormalCount = 0
  locSystemCount = 0
  locExpireCount = 0
  locMiniMapFilterFlag = 0
  gUI.MiniMap.MiniMap:SetProperty("Visible", "true")
  gUI.MiniMap.HideMap:SetCheckedState(true)
  gUI.MiniMap.EmailTimer = TimerSys_Instance:CreateTimerObject("mail_blink", 5, 1, "_MiniMap_Timer", false, false)
  gUI.MiniMap.Junction = WindowToComboBox(WindowSys_Instance:GetWindow("MiniMap.Text_bg.Junction_Cbox"))
end
function _OnMiniMap_SetTime()
  _MiniMap_ServerTimeEvent()
end
function _MiniMap_ServerTimeEvent()
  local tData, tTime, tWeek = GetTimeString()
  gUI.MiniMap.ServerTime:SetProperty("Text", tTime)
end
function _MiniMap_FlahMailIcon()
  FlashSeconds = 5
  gUI.MiniMap.Email:SetProperty("Blink", "true")
  Lua_Chat_AddSysLog("您有新的邮件！")
  gUI.MiniMap.EmailTimer:Restart()
  gUI.MiniMap.Email:setEffectFile("efxc_ui_mail_tishi")
  WorldStage:playSoundByID(13)
end
function _MiniMap_Timer()
  gUI.MiniMap.Email:SetProperty("Blink", "false")
  gUI.MiniMap.EmailTimer:Stop()
end
function _MiniMap_RecvNewMail(Count)
  if Count ~= nil then
    if Count > locUnreadNormalCount then
      _MiniMap_FlahMailIcon()
    end
    locUnreadNormalCount = Count
    _MiniMap_UpdateMailTipInfo()
  end
end
function _MiniMap_RecvSysMail(Count)
  if Count ~= nil then
    if Count > locUnreadSystemCount then
      _MiniMap_FlahMailIcon()
    end
    locUnreadSystemCount = Count
    _MiniMap_UpdateMailTipInfo()
  end
end
function _MiniMap_ExpireCount(NormalCount, SystemCount)
  locExpireCount = NormalCount + SystemCount
  _MiniMap_UpdateMailTipInfo()
end
function _MiniMap_TotalCount(NormalCount, SystemCount)
  locNormalCount = NormalCount
  locSystemCount = SystemCount
  _MiniMap_UpdateMailTipInfo()
end
function _MiniMap_UpdateMailTipInfo()
  cMailFull = 400
  cMailAlmostFull = 380
  if locSystemCount + locNormalCount > cMailFull then
    gUI.MiniMap.Email:SetProperty("TooltipText", "有" .. locUnreadSystemCount + locUnreadNormalCount .. "封邮件未阅读 ，邮箱已满，接收不到邮件")
  elseif locExpireCount > 0 then
    gUI.MiniMap.Email:SetProperty("TooltipText", "有邮件即将到期")
  elseif locSystemCount + locNormalCount > cMailAlmostFull then
    gUI.MiniMap.Email:SetProperty("TooltipText", "有" .. locUnreadSystemCount + locUnreadNormalCount .. "封邮件未阅读，邮箱快满了，满了以后将收不到邮件")
  elseif locUnreadSystemCount + locUnreadNormalCount > 0 then
    gUI.MiniMap.Email:SetProperty("TooltipText", "有" .. locUnreadSystemCount + locUnreadNormalCount .. "封邮件未阅读")
  else
    gUI.MiniMap.Email:SetProperty("TooltipText", "查看邮箱")
    gUI.MiniMap.Email:setEffectFile("")
  end
end

