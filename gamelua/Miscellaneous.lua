if gUI and not gUI.Miscellaneous then
  gUI.Miscellaneous = {}
end
local USER_GUIDE_FLOWERSHINT = 45
local OPENFINDEX = 93
local KILLPLAYER = 102
local GROW = 205
local BEWATER = 221
local HOME_BRON_GUIDE = 229
local FACILITY_BRON_SELF = 230
local FACILITY_BRON_OTHER = 231
local USER_GUIDE_RANSOM = 51
local RANSOMINDEX = 103
local URLINDEX = 104
local USER_GUIDE_GAMESURERY = 52
local USER_GUIDE_WALLOW = 54
local USER_GUIDE_ALLOWCONSTEST = 55
local USER_GUIDE_BEWATER = 57
local USER_GUIDE_FACILITY_BRON = 59
local WALLOWINDEX1 = 215
local WALLOWINDEX2 = 216
local WALLOWINDEX3 = 217
local ALLOWCONTSTE = 218
local PKVALUEOVER500 = 220
local _Miscell_Guide_FlowersList = {}
local itemName = ""
local itemColor = "FFFF8C00"
local _Miscell_Guide_WaterName = {}
local _Miscell_FacilityBorn_Name = {}
function _OnMiscell_GetFlowers(guid, playerName, playerLev, menpai)
  local info_f = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
  if info_f == nil then
    TriggerUserGuide(USER_GUIDE_FLOWERSHINT, -1, -1, guid)
    _Miscell_Guide_FlowersList[guid] = {
      Name = playerName,
      Lev = playerLev,
      Job = menpai
    }
  end
end
function _OnMiscell_OnBeWater(guid, playerName)
  TriggerUserGuide(USER_GUIDE_BEWATER, -1, -1, guid)
  _Miscell_Guide_WaterName[guid] = playerName
end
function _OnMiscell_HomeFacilityBron(guid, playerName)
  if guid == -1 then
    TriggerUserGuide(USER_GUIDE_FACILITY_BRON, 1, -1)
  else
    TriggerUserGuide(USER_GUIDE_FACILITY_BRON, 2, -1, guid)
    _Miscell_FacilityBorn_Name[guid] = playerName
  end
end
function _OnMiscell_GetRanSomEquip(name, quailty)
  TriggerUserGuide(USER_GUIDE_RANSOM, -1, -1)
  itemName = name
  itemColor = gUI_GOODS_QUALITY_COLOR[quailty]
end
function UI_Ransom_Equip(msg)
  GuidePathFinder(2)
end
local WALLOWSTANDRAND_TIME = 3600
function _OnMisscell_Wallow(timer)
  if timer >= WALLOWSTANDRAND_TIME * 3 then
    TriggerUserGuide(USER_GUIDE_WALLOW, 3, -1)
    Lua_Chat_ShowOSD("SAFE_WALLOW_OSD3")
  elseif timer >= WALLOWSTANDRAND_TIME * 2 then
    TriggerUserGuide(USER_GUIDE_WALLOW, 2, -1)
    Lua_Chat_ShowOSD("SAFE_WALLOW_OSD2")
  elseif timer >= WALLOWSTANDRAND_TIME then
    TriggerUserGuide(USER_GUIDE_WALLOW, 1, -1)
    Lua_Chat_ShowOSD("SAFE_WALLOW_OSD1")
  end
end
function _OnMisscell_AllowContest()
  TriggerUserGuide(USER_GUIDE_ALLOWCONSTEST, -1, -1)
end
function UI_OpenUrl_OnClose(msg)
  OpenWebPage("http://act.tzj.iwgame.com/special/survey1/")
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(URLINDEX)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_CloseAllowContest(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(ALLOWCONTSTE)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_CloseWallow(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(WALLOWINDEX1)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_CloseWallow1(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(WALLOWINDEX2)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_CloseWallow2(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(WALLOWINDEX3)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_OnMiscell_OnCloseNew(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  DeleteHint(OPENFINDEX, guid)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_ClosePKValueOver500(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(PKVALUEOVER500)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_OnMiscell_OnCloseNew2(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(RANSOMINDEX)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_OnMiscell_OnCloseNew3(msg)
  local rootwnd = msg:get_window():GetParent()
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_OnMiscell_OnWaterClose(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  DeleteHint(BEWATER, guid)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function UI_OnMiscell_OnHomeBronOther(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  DeleteHint(FACILITY_BRON_OTHER, guid)
  rootwnd:SetVisible(false)
  rootwnd:CloseGuide()
end
function Guide_OnGetFlower(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local guid = GoodsBox:GetUserData64()
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local rootwnd = WindowSys_Instance:GetWindow(guideWindowName)
  rootwnd:SetUserData64(guid)
  for key, value in pairs(_Miscell_Guide_FlowersList) do
    if key == guid then
      strText = string.format(strText, value.Name, value.Lev, gUI_MenPaiName[value.Job])
    end
  end
  local displayBox = WindowToDisplayBox(rootwnd:GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local btn = rootwnd:GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  local btn1 = WindowToButton(rootwnd:GetChildByName("Close_btn"))
  if btn1 then
    btn1:SetVisible(true)
    btn1:AddScriptEvent("wm_mouseclick", "UI_OnMiscell_OnCloseNew")
  end
  local titleWnd = rootwnd:GetChildByName("Label1_dlab")
  titleWnd:SetProperty("Text", "赠送鲜花提示")
  rootwnd:SetVisible(true)
end
function Guide_RansomEquip(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local rootwnd = WindowSys_Instance:GetWindow(guideWindowName)
  local strTemp = "{#C^%s^[%s]}"
  strTemp = string.format(strTemp, itemColor, itemName)
  strText = string.format(strText, strTemp)
  local displayBox = WindowToDisplayBox(rootwnd:GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local btn = rootwnd:GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  local btn1 = WindowToButton(rootwnd:GetChildByName("Close_btn"))
  if btn1 then
    btn1:SetVisible(true)
    btn1:AddScriptEvent("wm_mouseclick", "UI_OnMiscell_OnCloseNew2")
  end
  local titleWnd = rootwnd:GetChildByName("Label1_dlab")
  titleWnd:SetProperty("Text", "装备赎回")
  rootwnd:SetVisible(true)
end
function Guide_GameSurvey(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local rootwnd = WindowSys_Instance:GetWindow(guideWindowName)
  local displayBox = WindowToDisplayBox(rootwnd:GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local btn = rootwnd:GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  local btn1 = WindowToButton(rootwnd:GetChildByName("Close_btn"))
  if btn1 then
    btn1:SetVisible(true)
    btn1:AddScriptEvent("wm_mouseclick", "UI_OnMiscell_OnCloseNew3")
  end
  local titleWnd = rootwnd:GetChildByName("Label1_dlab")
  titleWnd:SetProperty("Text", "提示")
  rootwnd:SetVisible(true)
end
function Guide_OnAskAddFirend(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  local name, level, job = GetPlayerInfoByGUID(guid)
  Messagebox_Show("ASK_ADD_FRIEND_FLOWER", guid, name, level)
  DeleteHint(OPENFINDEX, guid)
  rootwnd:CloseGuide()
  rootwnd:SetVisible(false)
end
function Guide_OnBeWater(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local guid = GoodsBox:GetUserData64()
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local rootwnd = WindowSys_Instance:GetWindow(guideWindowName)
  rootwnd:SetUserData64(guid)
  for key, value in pairs(_Miscell_Guide_WaterName) do
    if key == guid then
      strText = string.format(strText, value)
    end
  end
  local displayBox = WindowToDisplayBox(rootwnd:GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local btn = rootwnd:GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  local btn1 = WindowToButton(rootwnd:GetChildByName("Close_btn"))
  if btn1 then
    btn1:SetVisible(true)
    btn1:AddScriptEvent("wm_mouseclick", "UI_OnMiscell_OnWaterClose")
  end
  local titleWnd = rootwnd:GetChildByName("Label1_dlab")
  titleWnd:SetProperty("Text", "浇水提示")
  rootwnd:SetVisible(true)
end
function Guide_OnFaciliteBron(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local guid = GoodsBox:GetUserData64()
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local rootwnd = WindowSys_Instance:GetWindow(guideWindowName)
  rootwnd:SetUserData64(guid)
  for key, value in pairs(_Miscell_FacilityBorn_Name) do
    if key == guid then
      strText = string.format(strText, value)
    end
  end
  local displayBox = WindowToDisplayBox(rootwnd:GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local btn = rootwnd:GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  local titleWnd = rootwnd:GetChildByName("Label1_dlab")
  titleWnd:SetProperty("Text", "神灵草成熟了")
  rootwnd:SetVisible(true)
end
function UI_OnMiscell_OnKillPlayer(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(KILLPLAYER)
  rootwnd:SetVisible(false)
end
function UI_OnMiscell_OnGrow(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(GROW)
  rootwnd:SetVisible(false)
end
function UI_OnMiscell_OnWater(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  DeleteHint(BEWATER, guid)
  rootwnd:SetVisible(false)
  UI_HomeLandCash_ShowMain()
end
function UI_OnMiscell_OnHomeBronGuide(msg)
  local rootwnd = msg:get_window():GetParent()
  DeleteHint(HOME_BRON_GUIDE)
  rootwnd:SetVisible(false)
end
function UI_OnMiscell_OnHomeBronSelf(msg)
  local parent = msg:get_window():GetParent()
  parent:SetVisible(false)
  local state = GetQuestState(8100)
  if state == QUEST_STATUS_AVAILABLE then
    _Guide_ShowHintMission(8100)
  else
  end
  DeleteHint(FACILITY_BRON_SELF)
end
