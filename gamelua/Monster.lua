if gUI and not gUI.Monster then
  gUI.Monster = {}
end
ConMonsterStateMyCard = 0
ConMonsterStateSuitCard = 1
ConMonsterStateExchange = 2
mMonsterCurrentState = -1
mCURPAGECARD = -1
mCURPAGESUIT = -1
mCURPAGEEXCHANGE = -1
mBelongCard = -1
mCurrMaxPage = -1
CurrentState = 0
CHANGEState = 1
NUCHANGEState = 0
function Lua_Monster_CloseOpen_Panel()
  if gUI.Monster.Root:IsVisible() then
    gUI.Monster.Root:SetVisible(false)
    mMonsterCurrentState = -1
    for i = 0, #gUI.Monster.MonCheckBtns do
      gUI.Monster.MonCheckBtns[i]:SetProperty("Checked", "false")
    end
    mCURPAGECARD = -1
    mCURPAGESUIT = -1
    mCURPAGEEXCHANGE = -1
    CurrentState = NUCHANGEState
  else
    gUI.Monster.Root:SetVisible(true)
    mMonsterCurrentState = ConMonsterStateMyCard
    gUI.Monster.MonCheckBtns[mMonsterCurrentState]:SetProperty("Checked", "true")
    CurrentState = NUCHANGEState
    Lua_Monster_UpdateCardInfo()
    mCURPAGECARD = 0
  end
end
function UI_Monster_OnOpenClick(msg)
  Lua_Monster_CloseOpen_Panel()
end
function UI_Monster_Search(msg)
  local strKeyWord = gUI.Monster.CardSearchEidt:GetProperty("Text")
  if strKeyWord ~= "" then
    MonsterSearchInfoByKeyWord(strKeyWord)
  end
end
function UI_Monster_ShowTips(msg)
  local win = msg:get_window()
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  local itemIds = win:GetCustomUserData(0)
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, itemIds)
end
function UI_GainItems(msg)
  if mCURPAGESUIT > -1 then
    MonsterManualGainReward(mCURPAGESUIT)
  end
end
function UI_ExchangeItem(msg)
  if mCURPAGEEXCHANGE > -1 then
    MonsterManualExchange(mCURPAGEEXCHANGE)
  end
end
function UI_Monster_Suit(msg)
  if mBelongCard ~= -1 then
    mCURPAGESUIT = GetMonsterManualItemIndex(mBelongCard)
    mMonsterCurrentState = ConMonsterStateSuitCard
    CurrentState = CHANGEState
    Lua_Monster_ShowState(mMonsterCurrentState)
    gUI.Monster.ListsRoot[1]:SetSelectItem(mCURPAGESUIT)
  end
end
function UI_MonsterState_BtnClick(msg)
  if msg == nil then
    return
  end
  local CheckBtn = msg:get_window()
  CheckBtn = WindowToCheckButton(CheckBtn)
  local curIndex = CheckBtn:GetProperty("CustomUserData")
  CurrentState = NUCHANGEState
  Lua_Monster_ShowState(curIndex)
end
function UI_Monster_PageUp(msg)
  gUI.Monster.FrameItem:SetProperty("AnimateId", "87")
  if mMonsterCurrentState == ConMonsterStateMyCard then
    mCURPAGECARD = mCURPAGECARD - 1
    Lua_Monster_UpdateCurCardInfos()
    gUI.Monster.ListsRoot[0]:SetSelectItem(mCURPAGECARD)
  elseif mMonsterCurrentState == ConMonsterStateSuitCard then
    mCURPAGESUIT = mCURPAGESUIT - 1
    Lua_Monster_UpdateCurSuitInfos()
    gUI.Monster.ListsRoot[1]:SetSelectItem(mCURPAGESUIT)
  elseif mMonsterCurrentState == ConMonsterStateExchange then
    mCURPAGEEXCHANGE = mCURPAGEEXCHANGE - 1
    Lua_Monster_UpdateCurExchangeInfos()
    gUI.Monster.ListsRoot[2]:SetSelectItem(mCURPAGEEXCHANGE)
  end
end
function UI_Monster_PageDown(msg)
  gUI.Monster.FrameItem:SetProperty("AnimateId", "86")
  if mMonsterCurrentState == ConMonsterStateMyCard then
    mCURPAGECARD = mCURPAGECARD + 1
    Lua_Monster_UpdateCurCardInfos()
    gUI.Monster.ListsRoot[0]:SetSelectItem(mCURPAGECARD)
  elseif mMonsterCurrentState == ConMonsterStateSuitCard then
    mCURPAGESUIT = mCURPAGESUIT + 1
    Lua_Monster_UpdateCurSuitInfos()
    gUI.Monster.ListsRoot[1]:SetSelectItem(mCURPAGESUIT)
  elseif mMonsterCurrentState == ConMonsterStateExchange then
    mCURPAGEEXCHANGE = mCURPAGEEXCHANGE + 1
    Lua_Monster_UpdateCurExchangeInfos()
    gUI.Monster.ListsRoot[2]:SetSelectItem(mCURPAGEEXCHANGE)
  end
end
function Lua_Monster_ShowState(curIndex)
  for i = 0, #gUI.Monster.MonCheckBtns do
    gUI.Monster.MonCheckBtns[i]:SetProperty("Checked", "false")
  end
  if mMonsterCurrentState == tonumber(curIndex) then
    Lua_Monster_UpdateCardInfo()
  else
    mMonsterCurrentState = tonumber(curIndex)
    Lua_Monster_UpdateCardInfo()
  end
  gUI.Monster.MonCheckBtns[tonumber(curIndex)]:SetProperty("Checked", "true")
end
function Lua_Monster_UpdateCardInfo()
  if mMonsterCurrentState == -1 then
    return
  end
  for i = 0, #gUI.Monster.Roots do
  end
  if mMonsterCurrentState == ConMonsterStateMyCard and CurrentState == NUCHANGEState then
    mCURPAGECARD = 0
  elseif mMonsterCurrentState == ConMonsterStateSuitCard and CurrentState == NUCHANGEState then
    mCURPAGESUIT = 0
  elseif mMonsterCurrentState == ConMonsterStateExchange and CurrentState == NUCHANGEState then
    mCURPAGEEXCHANGE = 0
  end
  Lua_Monster_UpdateListInfo()
end
function Lua_Monster_UpdateListInfo()
  if mMonsterCurrentState == ConMonsterStateMyCard then
    MonsterManualListInfosData1()
  elseif mMonsterCurrentState == ConMonsterStateSuitCard then
    MonsterManualListInfosData2()
  elseif mMonsterCurrentState == ConMonsterStateExchange then
    MonsterManualListInfosData3()
  end
end
function Lua_Monster_UpdateCurInfos()
  if mMonsterCurrentState == ConMonsterStateMyCard then
    Lua_Monster_UpdateCurCardInfos()
  elseif mMonsterCurrentState == ConMonsterStateSuitCard then
    Lua_Monster_UpdateCurSuitInfos()
  elseif mMonsterCurrentState == ConMonsterStateExchange then
    Lua_Monster_UpdateCurExchangeInfos()
  end
end
function Lua_Monster_UpdateCurCardInfos()
  local iconName, itemName, descript, ownNum, ActTime, SuitName, SuitId, GainMoth = MonsterManualContentInfosData1(mCURPAGECARD)
  if ActTime == "" then
    ActTime = " 未获得 "
    gUI.Monster.CardItemIcon:SetProperty("GrayImage", "1")
  else
    gUI.Monster.CardItemIcon:SetProperty("GrayImage", "0")
  end
  gUI.Monster.CardNameLab:SetProperty("Text", tostring(itemName))
  gUI.Monster.CardDesLab:SetProperty("Text", tostring(descript))
  gUI.Monster.CardGetMothLab:SetProperty("Text", tostring(ActTime))
  gUI.Monster.CardGainLab:ClearAllContent()
  gUI.Monster.CardGainLab:InsertBack(tostring(GainMoth), 0, 0, 0)
  gUI.Monster.CardBelongLab:SetProperty("Text", tostring(SuitName))
  gUI.Monster.CardNumLab:SetProperty("Text", tostring(ownNum))
  gUI.Monster.CurPageLab:SetProperty("Text", tostring(mCURPAGECARD + 1))
  gUI.Monster.CardItemIcon:SetProperty("BackImage", tostring(iconName))
  mBelongCard = SuitId
  if mBelongCard == -1 then
    gUI.Monster.CardSuitBtn:SetProperty("Enable", "false")
  else
    gUI.Monster.CardSuitBtn:SetProperty("Enable", "true")
  end
  if mCURPAGECARD == 0 then
    gUI.Monster.PerBtn:SetProperty("Visible", "false")
  else
    gUI.Monster.PerBtn:SetProperty("Visible", "true")
  end
  if mCURPAGECARD == mCurrMaxPage - 1 then
    gUI.Monster.NextBtn:SetProperty("Visible", "false")
  else
    gUI.Monster.NextBtn:SetProperty("Visible", "true")
  end
  if SuitId > 0 then
    local _, _, _, _, _, _, activeState = MonsterManualContentInfosData2(SuitId - 1)
    local ItemInfos = MonsterManualSuitInfos(SuitId - 1)
    local IsActiveAll = 1
    for i = 0, #ItemInfos do
      if ItemInfos[i].Active ~= "1" then
        IsActiveAll = 0
      end
    end
    if activeState == 0 and IsActiveAll == 1 then
      TriggerUserGuide(46, -1, -1)
    end
  end
end
function Lua_Monster_UpdateCurSuitInfos()
  if mCURPAGESUIT < 0 then
    return
  end
  local totalLen, goodsBoxIcon, ItemCount, GainTitle, Descript, SuitName, activeState, ItemIDs, BackImage = MonsterManualContentInfosData2(mCURPAGESUIT)
  local ItemInfos = MonsterManualSuitInfos(mCURPAGESUIT)
  for i = 0, #gUI.Monster.MonActiveImages do
    gUI.Monster.MonActiveImages[i]:SetProperty("Visible", "false")
  end
  if BackImage == "" then
  else
    gUI.Monster.MonActiveBackImage:SetProperty("BackImage", tostring(BackImage))
  end
  local strItemName = ""
  local intActive = 0
  local IsActiveAll = 1
  local AllCardNums = 0
  for i = 0, #ItemInfos do
    gUI.Monster.MonActiveImages[i]:SetProperty("BackImage", tostring(ItemInfos[i].Icon))
    strItemName = strItemName .. tostring(ItemInfos[i].ItemName) .. ";" .. " "
    if ItemInfos[i].Active == "1" then
      gUI.Monster.MonActiveImages[i]:SetProperty("GrayImage", "0")
      intActive = intActive + 1
    else
      IsActiveAll = 0
      gUI.Monster.MonActiveImages[i]:SetProperty("GrayImage", "1")
    end
    gUI.Monster.MonActiveImages[i]:SetProperty("Visible", "true")
    AllCardNums = AllCardNums + 1
  end
  if activeState == 0 then
    gUI.Monster.GetRewards:SetProperty("Text", "领取奖励")
    if IsActiveAll == 0 then
      gUI.Monster.GetRewards:SetProperty("Enable", "false")
    else
      gUI.Monster.GetRewards:SetProperty("Enable", "true")
      TriggerUserGuide(47, -1, -1)
    end
  else
    gUI.Monster.GetRewards:SetProperty("Text", "已领取")
    gUI.Monster.GetRewards:SetProperty("Enable", "false")
  end
  gUI.Monster.RewardDBox:ClearAllContent()
  gUI.Monster.RewardDBox:InsertBack(strItemName, 0, 0, 0)
  gUI.Monster.PresentLab:SetProperty("Text", tostring(intActive) .. "/" .. tostring(AllCardNums))
  gUI.Monster.ReWardGoodBox:ResetAllGoods(true)
  gUI.Monster.ReWardGoodBox:SetItemGoods(0, goodsBoxIcon, -1)
  gUI.Monster.ReWardGoodBox:SetItemSubscript(0, tostring(ItemCount))
  gUI.Monster.ReWardGoodBox:SetCustomUserData(0, ItemIDs)
  gUI.Monster.DescriptLabel:SetProperty("Text", tostring(Descript))
  gUI.Monster.NameLabel:SetProperty("Text", tostring(SuitName))
  gUI.Monster.RewardTitle:SetProperty("Text", tostring(GainTitle))
  if mCURPAGESUIT == 0 then
    gUI.Monster.PerBtn:SetProperty("Visible", "false")
  else
    gUI.Monster.PerBtn:SetProperty("Visible", "true")
  end
  if mCURPAGESUIT == mCurrMaxPage - 1 then
    gUI.Monster.NextBtn:SetProperty("Visible", "false")
  else
    gUI.Monster.NextBtn:SetProperty("Visible", "true")
  end
  gUI.Monster.CurPageLab:SetProperty("Text", tostring(mCURPAGESUIT + 1))
end
function Lua_Monster_UpdateCurExchangeInfos()
  local RewardIcon, Rewardname, RewardCount, ExItemId = MonsterManualContentInfosData3(mCURPAGEEXCHANGE)
  if mCURPAGEEXCHANGE == 0 then
    gUI.Monster.PerBtn:SetProperty("Visible", "false")
  else
    gUI.Monster.PerBtn:SetProperty("Visible", "true")
  end
  if mCURPAGEEXCHANGE == mCurrMaxPage - 1 then
    gUI.Monster.NextBtn:SetProperty("Visible", "false")
  else
    gUI.Monster.NextBtn:SetProperty("Visible", "true")
  end
  for i = 0, #gUI.Monster.MonsterPics do
    gUI.Monster.MonsterPics[i]:SetProperty("Visible", "false")
  end
  local States = 1
  local RewardList = MonsterManualExchangeItems(mCURPAGEEXCHANGE)
  for i = 0, #RewardList do
    gUI.Monster.MonsterPics[i]:GetChildByName("Label1_dlab"):SetProperty("Text", tostring(RewardList[i].ItemName))
    gUI.Monster.MonsterPics[i]:GetChildByName("Label2_dlab"):SetProperty("Text", tostring(RewardList[i].ItemHasCount) .. "/" .. tostring(RewardList[i].ItemNeedCount))
    gUI.Monster.MonsterPics[i]:SetProperty("BackImage", tostring(RewardList[i].ItemIcon))
    gUI.Monster.MonsterPics[i]:SetProperty("Visible", "true")
    if RewardList[i].ItemHasCount >= RewardList[i].ItemNeedCount then
      gUI.Monster.MonsterPics[i]:GetChildByName("Label2_dlab"):SetProperty("FontColor", "FFFFFFFF")
    else
      States = 0
      gUI.Monster.MonsterPics[i]:GetChildByName("Label2_dlab"):SetProperty("FontColor", "FFFF0000")
    end
  end
  gUI.Monster.ItemName1:SetProperty("Text", tostring(Rewardname))
  gUI.Monster.ItemName2:SetProperty("Text", tostring(Rewardname))
  gUI.Monster.ItemCount:SetProperty("Text", tostring(RewardCount))
  gUI.Monster.RewardGoods:ResetAllGoods(true)
  gUI.Monster.RewardGoods:SetItemGoods(0, RewardIcon, -1)
  gUI.Monster.RewardGoods:SetCustomUserData(0, ExItemId)
  if States == 0 then
    gUI.Monster.RewardBtns:SetProperty("Enable", "false")
  else
    gUI.Monster.RewardBtns:SetProperty("Enable", "true")
  end
  gUI.Monster.CurPageLab:SetProperty("Text", tostring(mCURPAGEEXCHANGE + 1))
end
function _OnMonsterCardListBoxInfos(CardId, CardName, CardNum, CardIndex, CardState)
  local strCard
  if CardState == 0 then
    strCard = "{#R^" .. tostring(CardIndex + 1) .. ". " .. tostring(CardName) .. "(" .. tostring(CardNum) .. ")" .. "}"
  else
    strCard = "{#G^" .. tostring(CardIndex + 1) .. ". " .. tostring(CardName) .. "(" .. tostring(CardNum) .. ")" .. "}"
  end
  gUI.Monster.ListsRoot[0]:InsertString(strCard, -1)
end
function _OnMonsterSuitListBoxInfos(SuitId, SuitName, SuitState, SuitIndex)
  local strSuit
  if SuitState == 1 then
    strSuit = "{#R^" .. tostring(SuitIndex + 1) .. ". " .. tostring(SuitName) .. "}"
  elseif SuitState == 2 then
    strSuit = "{#Y^" .. tostring(SuitIndex + 1) .. ". " .. tostring(SuitName) .. "}"
  elseif SuitState == 3 then
    strSuit = "{#G^" .. tostring(SuitIndex + 1) .. ". " .. tostring(SuitName) .. "}"
  end
  gUI.Monster.ListsRoot[1]:InsertString(strSuit, -1)
end
function _OnMonsterExchangeListBoxInfos(GiftName, GiftIndex, GiftState)
  local strGift
  if GiftState == 1 then
    strGift = "{#G^" .. tostring(GiftIndex + 1) .. ". " .. tostring(GiftName) .. "}"
  elseif GiftState == 2 then
    strGift = "{#R^" .. tostring(GiftIndex + 1) .. ". " .. tostring(GiftName) .. "}"
  end
  gUI.Monster.ListsRoot[2]:InsertString(strGift, -1)
end
function _OnMonsterCardListBoxInfosBegin(MaxPage)
  for i = 0, #gUI.Monster.Roots do
    gUI.Monster.Roots[i]:SetVisible(false)
    gUI.Monster.ListsRoot[i]:RemoveAllItems()
  end
  gUI.Monster.Roots[mMonsterCurrentState]:SetVisible(true)
  mCurrMaxPage = MaxPage
end
function _OnMonsterCardListBoxInfosEnd()
  if mMonsterCurrentState == ConMonsterStateMyCard then
    gUI.Monster.ListsRoot[0]:SetSelectItem(mCURPAGECARD)
  elseif mMonsterCurrentState == ConMonsterStateSuitCard then
    gUI.Monster.ListsRoot[1]:SetSelectItem(mCURPAGESUIT)
  elseif mMonsterCurrentState == ConMonsterStateExchange then
    gUI.Monster.ListsRoot[2]:SetSelectItem(mCURPAGEEXCHANGE)
  end
  Lua_Monster_UpdateCurInfos()
end
function UI_Monster_SelListBox(msg)
  local List, selectId
  if mMonsterCurrentState == ConMonsterStateMyCard then
    List = gUI.Monster.ListsRoot[0]
    selectId = List:GetSelectedItemIndex()
    if mCURPAGECARD ~= selectId then
      if selectId < mCURPAGECARD then
        gUI.Monster.FrameItem:SetProperty("AnimateId", "87")
      else
        gUI.Monster.FrameItem:SetProperty("AnimateId", "86")
      end
      mCURPAGECARD = selectId
      Lua_Monster_UpdateCurCardInfos()
    end
  elseif mMonsterCurrentState == ConMonsterStateSuitCard then
    List = gUI.Monster.ListsRoot[1]
    selectId = List:GetSelectedItemIndex()
    if mCURPAGESUIT ~= selectId then
      if selectId < mCURPAGESUIT then
        gUI.Monster.FrameItem:SetProperty("AnimateId", "87")
      else
        gUI.Monster.FrameItem:SetProperty("AnimateId", "86")
      end
      mCURPAGESUIT = selectId
      Lua_Monster_UpdateCurSuitInfos()
    end
  elseif mMonsterCurrentState == ConMonsterStateExchange then
    List = gUI.Monster.ListsRoot[2]
    selectId = List:GetSelectedItemIndex()
    if mCURPAGEEXCHANGE ~= selectId then
      if selectId < mCURPAGEEXCHANGE then
        gUI.Monster.FrameItem:SetProperty("AnimateId", "87")
      else
        gUI.Monster.FrameItem:SetProperty("AnimateId", "86")
      end
      mCURPAGEEXCHANGE = selectId
      Lua_Monster_UpdateCurExchangeInfos()
    end
  end
end
function _OnMonsterCardPageChange(page)
  if mCURPAGECARD == page then
  elseif page < mCURPAGECARD then
    gUI.Monster.FrameItem:SetProperty("AnimateId", "87")
  elseif page > mCURPAGECARD then
    gUI.Monster.FrameItem:SetProperty("AnimateId", "86")
  end
  mCURPAGECARD = page
  gUI.Monster.ListsRoot[0]:SetSelectItem(mCURPAGECARD)
  Lua_Monster_UpdateCurCardInfos()
end
function _OnCardNumChange(Index, state)
  if nStateU == 1 then
    Lua_Monster_UpdateCurInfos()
  end
  local nStateU = 1
  if state == 1 then
    mMonsterCurrentState = ConMonsterStateMyCard
    mCURPAGECARD = Index
  elseif state == 0 then
    if not gUI.Monster.Root:IsVisible() then
      gUI.Monster.Root:SetVisible(true)
    end
    mMonsterCurrentState = ConMonsterStateMyCard
    mCURPAGECARD = Index
  elseif state == 2 then
    nStateU = 2
  end
  CurrentState = CHANGEState
  Lua_Monster_ShowState(mMonsterCurrentState)
  if nStateU == 1 then
    Lua_Monster_UpdateCurInfos()
  end
end
function Script_Monster_OnLoad()
  gUI.Monster.Root = WindowSys_Instance:GetWindow("Monster")
  gUI.Monster.Roots = {}
  gUI.Monster.Roots[0] = WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg")
  gUI.Monster.Roots[1] = WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg")
  gUI.Monster.Roots[2] = WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg")
  gUI.Monster.ListsRoot = {}
  gUI.Monster.ListsRoot[0] = WindowToListBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Left_bg.List_lbox"))
  gUI.Monster.ListsRoot[1] = WindowToListBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Left_bg.List_lbox"))
  gUI.Monster.ListsRoot[2] = WindowToListBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Left__bg.List_lbox"))
  gUI.Monster.PerBtn = WindowToButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Last_btn"))
  gUI.Monster.NextBtn = WindowToButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Next_btn"))
  gUI.Monster.MonCheckBtns = {}
  gUI.Monster.MonCheckBtns[0] = WindowToCheckButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_cbtn"))
  gUI.Monster.MonCheckBtns[1] = WindowToCheckButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_cbtn"))
  gUI.Monster.MonCheckBtns[2] = WindowToCheckButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_cbtn"))
  gUI.Monster.CurPageLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Page_dlab"))
  gUI.Monster.CardNameLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Label1_dlab"))
  gUI.Monster.CardDesLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Label2_dlab"))
  gUI.Monster.CardItemIcon = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Monster_pic"))
  gUI.Monster.CardGetMothLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Gain_cnt.Present_bg.Present_dlab"))
  gUI.Monster.CardGainLab = WindowToDisplayBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Gain_cnt.Gain_dbox"))
  gUI.Monster.CardBelongLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Gain_cnt.Belong_bg.Belong_dlab"))
  gUI.Monster.CardNumLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Gain_cnt.Have_bg.Have_dlab"))
  gUI.Monster.CardSuitBtn = WindowToButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Right_bg.Gain_cnt.Belong_bg.Check_btn"))
  gUI.Monster.CardSearchBtn = WindowToButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Left_bg.Search_bg.Search_btn"))
  gUI.Monster.CardSearchEidt = WindowToEditBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Card_bg.Left_bg.Search_bg.Search_ebox"))
  gUI.Monster.MonActiveImages = {}
  gUI.Monster.MonActiveBackImage = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic"))
  gUI.Monster.MonActiveImages[0] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture1_pic"))
  gUI.Monster.MonActiveImages[1] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture2_pic"))
  gUI.Monster.MonActiveImages[2] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture3_pic"))
  gUI.Monster.MonActiveImages[3] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture4_pic"))
  gUI.Monster.MonActiveImages[4] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture5_pic"))
  gUI.Monster.MonActiveImages[5] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture6_pic"))
  gUI.Monster.MonActiveImages[6] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture7_pic"))
  gUI.Monster.MonActiveImages[7] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture8_pic"))
  gUI.Monster.MonActiveImages[8] = WindowToPicture(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Monster_pic.Picture9_pic"))
  gUI.Monster.GetRewards = WindowToButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Reward_cnt.Choose_bg.Reward_btn"))
  gUI.Monster.RewardDBox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Reward_cnt.Count_dbox"))
  gUI.Monster.PresentLab = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Reward_cnt.Present_bg.Present_dlab"))
  gUI.Monster.ReWardGoodBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Reward_cnt.Choose_bg.Reward_gbox"))
  gUI.Monster.DescriptLabel = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Label2_dlab"))
  gUI.Monster.NameLabel = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Label1_dlab"))
  gUI.Monster.RewardTitle = WindowToDisplayBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Suit_bg.Right_bg.Reward_cnt.Reward_bg.Reward_dlab"))
  gUI.Monster.MonsterPics = {}
  gUI.Monster.MonsterPics[0] = WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Monster1_pic")
  gUI.Monster.MonsterPics[1] = WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Monster2_pic")
  gUI.Monster.MonsterPics[2] = WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Monster3_pic")
  gUI.Monster.ItemName1 = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Label1_dlab"))
  gUI.Monster.ItemName2 = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Name_dlab"))
  gUI.Monster.ItemCount = WindowToLabel(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Label2_dlab"))
  gUI.Monster.RewardGoods = WindowToGoodsBox(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Exchange_gbox"))
  gUI.Monster.RewardBtns = WindowToButton(WindowSys_Instance:GetWindow("Monster.Monster_bg.Exchange_bg.Right_bg.Exchange_btn"))
  gUI.Monster.FrameItem = WindowSys_Instance:GetWindow("Monster.Monster_bg.Frames_bg")
end
function Script_Monster_OnEvent(event)
  if event == "MONSTER_CARDS_LISTINFOS" then
    _OnMonsterCardListBoxInfos(arg1, arg2, arg3, arg4, arg5)
  elseif event == "MONSTER_CARDS_LISTINFOSBEGIN" then
    _OnMonsterCardListBoxInfosBegin(arg1)
  elseif event == "MONSTER_CARDS_LISTINFOSEND" then
    _OnMonsterCardListBoxInfosEnd()
  elseif event == "MONSTER_SUITS_LISTINFOS" then
    _OnMonsterSuitListBoxInfos(arg1, arg2, arg3, arg4)
  elseif event == "MONSTER_SUITS_LISTINFOSBEGIN" then
    _OnMonsterCardListBoxInfosBegin(arg1)
  elseif event == "MONSTER_SUITS_LISTINFOSEND" then
    _OnMonsterCardListBoxInfosEnd()
  elseif event == "MONSTER_EXCHANGE_LISTINFOS" then
    _OnMonsterExchangeListBoxInfos(arg1, arg2, arg3)
  elseif event == "MONSTER_EXCHANGE_LISTINFOSBEGIN" then
    _OnMonsterCardListBoxInfosBegin(arg1)
  elseif event == "MONSTER_EXCHANGE_LISTINFOSEND" then
    _OnMonsterCardListBoxInfosEnd()
  elseif event == "MONSTER_CARD_CONTENT" then
  elseif event == "MONSTER_SUIT_CONTENT" then
  elseif event == "MONSTER_EXCHANGE_CONTENT" then
  elseif event == "MONSTER_CHANGECARD_PAGE" then
    _OnMonsterCardPageChange(arg1)
  elseif event == "MONSTER_NUMCHANGE" then
    _OnCardNumChange(arg1, arg2)
  end
end

