if gUI and not gUI.Mounts then
  gUI.Mounts = {}
end
local _Mounts_CurSelMount = -1
local _Mounts_ShowBoxName = "MountsAvatar"
local _Mounts_NumInOnePage = 10
local _Mounts_DefaultIndex = -1
local _Mounts_CurPage = 0
local _Mounts_Avatarotate = "Mounts_Rotate"
function _Mounts_InitCtrl()
  gUI.Mounts.wndRoot = WindowSys_Instance:GetWindow("Mounts")
  gUI.Mounts.dlabName = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.Label3"))
  gUI.Mounts.dlabScore = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.Picture3_bg.Label1_dlab"))
  gUI.Mounts.dlabCurLevel = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.Picture3_bg.Label2_dlab"))
  gUI.Mounts.dlabHistroyMaxLevel = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.Picture3_bg.Label3_dlab"))
  gUI.Mounts.dlabPasNum = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.Picture3_bg.Label4_dlab"))
  gUI.Mounts.picAvatar = WindowToPicture(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.Picture1_bg.MountModle_pic"))
  gUI.Mounts.cbtnDefault = WindowToCheckButton(WindowSys_Instance:GetWindow("Mounts.Picture2_bg.CheckButton_cbtn"))
  local showbox = UIInterface:getShowbox(_Mounts_ShowBoxName)
  showbox = showbox or UIInterface:createShowbox(_Mounts_ShowBoxName, gUI.Mounts.picAvatar)
  gUI.Mounts.showbox = showbox
  gUI.Mounts.dlabTotalScore = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture3_bg.Count_dlab"))
  gUI.Mounts.gboxAllMounts = WindowToGoodsBox(WindowSys_Instance:GetWindow("Mounts.Picture3_bg.GoodsBox_gbox"))
  gUI.Mounts.gboxAllMounts:SetItemDragType(1)
  gUI.Mounts.btnLastPage = WindowToButton(WindowSys_Instance:GetWindow("Mounts.Picture3_bg.LeftButton_btn"))
  gUI.Mounts.btnNextPage = WindowToButton(WindowSys_Instance:GetWindow("Mounts.Picture3_bg.RightButton_btn"))
  gUI.Mounts.dlabPage = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture3_bg.Page_dlab"))
  gUI.Mounts.gboxSkill1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GoodsBox1_gbox"))
  gUI.Mounts.gboxSkill2 = WindowToGoodsBox(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GoodsBox2_gbox"))
  gUI.Mounts.gboxSkill3 = WindowToGoodsBox(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GoodsBox3_gbox"))
  gUI.Mounts.dlabPoint = WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.Count_dlab"))
  gUI.Mounts.dlabPage:SetProperty("Text", "1")
end
function _Mounts_Clear()
  gUI.Mounts.dlabName:SetProperty("Text", "")
  gUI.Mounts.dlabScore:SetProperty("Text", "")
  gUI.Mounts.dlabCurLevel:SetProperty("Text", "")
  gUI.Mounts.dlabHistroyMaxLevel:SetProperty("Text", "")
  gUI.Mounts.dlabPasNum:SetProperty("Text", "")
  gUI.Mounts.cbtnDefault:SetVisible(false)
  gUI.Mounts.dlabTotalScore:SetProperty("Text", "")
  gUI.Mounts.gboxAllMounts:ResetAllGoods(true)
  gUI.Mounts.btnLastPage:SetEnable(false)
  gUI.Mounts.btnNextPage:SetEnable(false)
  gUI.Mounts.dlabPage:SetProperty("Text", "1")
  gUI.Mounts.gboxSkill1:ResetAllGoods(true)
  gUI.Mounts.gboxSkill2:ResetAllGoods(true)
  gUI.Mounts.gboxSkill3:ResetAllGoods(true)
  gUI.Mounts.dlabPoint:SetProperty("Text", "")
  _Mounts_CurSelMount = -1
  _Mounts_ShowBoxName = "MountsAvatar"
  _Mounts_NumInOnePage = 10
  _Mounts_DefaultIndex = -1
  _Mounts_CurPage = 0
end
function _Mounts_UpdateAll()
  _Mounts_UpdateMounts(false)
  _Mounts_UpdateSkill()
end
function _Mounts_UpdateMounts(updateDefault)
  gUI.Mounts.dlabTotalScore:SetProperty("Text", tostring(GetAllMark()))
  local totalMountCnt = GetMountCnt()
  if totalMountCnt <= 0 then
    gUI.Mounts.dlabPage:SetProperty("Text", "1/1")
    gUI.Mounts.cbtnDefault:SetVisible(false)
    return
  end
  gUI.Mounts.cbtnDefault:SetVisible(true)
  gUI.Mounts.dlabPage:SetProperty("Text", tostring(_Mounts_CurPage + 1) .. "/" .. tostring(math.floor((totalMountCnt - 1) / _Mounts_NumInOnePage) + 1))
  if _Mounts_CurPage >= math.floor((totalMountCnt - 1) / _Mounts_NumInOnePage) then
    gUI.Mounts.btnNextPage:SetEnable(false)
  else
    gUI.Mounts.btnNextPage:SetEnable(true)
  end
  if _Mounts_CurPage <= 0 then
    gUI.Mounts.btnLastPage:SetEnable(false)
  else
    gUI.Mounts.btnLastPage:SetEnable(true)
  end
  _Mounts_CurSelMount = _Mounts_CurPage * _Mounts_NumInOnePage
  local index = 0
  for i = _Mounts_CurPage * _Mounts_NumInOnePage, _Mounts_CurPage * _Mounts_NumInOnePage + _Mounts_NumInOnePage - 1 do
    local _, mark, EnhanceLv, HistoryEnhanceLv, pasNum, icon, modelId, bDefault = GetMountBaseInfo(i)
    if bDefault then
      _Mounts_DefaultIndex = i
    end
    if icon ~= nil then
      if bDefault then
        gUI.Mounts.gboxAllMounts:SetItemGoods(index, icon, 1)
      else
        gUI.Mounts.gboxAllMounts:SetItemGoods(index, icon, -1)
      end
      gUI.Mounts.gboxAllMounts:SetItemData(index, i)
    else
      gUI.Mounts.gboxAllMounts:ClearGoodsItem(index)
      gUI.Mounts.gboxAllMounts:SetItemData(index, -1)
    end
    index = index + 1
  end
  gUI.Mounts.dlabPoint:SetProperty("Text", tostring(GetSkillPoint()))
  if not updateDefault then
    gUI.Mounts.gboxAllMounts:SetSelectedItem(0)
    _Mounts_UpdateByIndex(_Mounts_CurPage * _Mounts_NumInOnePage)
  end
end
function _Mounts_UpdateSkill()
  local skillList = GetMountAllSkill()
  local giftList = GetMountAllGift()
  local index1 = 0
  local index2 = 0
  local index3 = 0
  gUI.Mounts.gboxSkill1:ResetAllGoods(true)
  gUI.Mounts.gboxSkill2:ResetAllGoods(true)
  gUI.Mounts.gboxSkill3:ResetAllGoods(true)
  for index, item in spairs(skillList) do
    if item.bActive then
      gUI.Mounts.gboxSkill1:SetItemGoods(index1, item.icon, -1)
      gUI.Mounts.gboxSkill1:SetItemData(index1, item.id)
      gUI.Mounts.gboxSkill1:SetItemEnable(index1, item.bEnable)
      gUI.Mounts.gboxSkill1:SetItemCanDrag(index1, item.bEnable)
      index1 = index1 + 1
    else
      gUI.Mounts.gboxSkill2:SetItemGoods(index2, item.icon, -1)
      gUI.Mounts.gboxSkill2:SetItemData(index2, item.id)
      gUI.Mounts.gboxSkill2:SetItemEnable(index2, item.bEnable)
      index2 = index2 + 1
    end
  end
  for i = 0, 5 do
    gUI.Mounts.gboxSkill3:SetItemData(i, -1)
    WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GiftNumb0" .. tostring(i + 1) .. "_dlab")):SetProperty("Text", "")
  end
  for index, item in spairs(giftList) do
    gUI.Mounts.gboxSkill3:SetItemGoods(index3, item.icon, -1)
    gUI.Mounts.gboxSkill3:SetItemData(index3, item.id)
    index3 = index3 + 1
    WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GiftNumb0" .. tostring(index3) .. "_dlab")):SetProperty("Text", tostring(item.level) .. "/" .. tostring(item.maxLevel))
  end
end
function _Mounts_UpdateByIndex(index)
  local name, mark, EnhanceLv, HistoryEnhanceLv, pasNum, icon, modelId, bDefault = GetMountBaseInfo(index)
  gUI.Mounts.dlabName:SetProperty("Text", name)
  gUI.Mounts.dlabScore:SetProperty("Text", tostring(mark))
  gUI.Mounts.dlabCurLevel:SetProperty("Text", tostring(EnhanceLv))
  gUI.Mounts.dlabHistroyMaxLevel:SetProperty("Text", tostring(HistoryEnhanceLv))
  gUI.Mounts.dlabPasNum:SetProperty("Text", tostring(pasNum))
  gUI.Mounts.cbtnDefault:SetChecked(index == _Mounts_DefaultIndex)
  gUI.Mounts.cbtnDefault:SetEnable(index ~= _Mounts_DefaultIndex)
  if not gUI.Mounts.showbox:isAvatarExist("Mount" .. modelId) then
    gUI.Mounts.showbox:onCharacterReset()
    gUI.Mounts.showbox:revertCamera()
    gUI.Mounts.showbox:createMountAvatar(modelId, "Mount")
    gUI.Mounts.showbox:setAvatarScale(0.7)
    gUI.Mounts.showbox:setrevertCamera(-50)
  end
  gUI.Mounts.showbox:setCurrentAvatar("Mount" .. modelId)
end
function Lua_Mounts_UpdateGift()
  local giftList = GetMountAllGift()
  local index3 = 0
  gUI.Mounts.gboxSkill3:ResetAllGoods(true)
  for i = 0, 5 do
    gUI.Mounts.gboxSkill3:SetItemData(i, -1)
    WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GiftNumb0" .. tostring(i + 1) .. "_dlab")):SetProperty("Text", "")
  end
  for index, item in spairs(giftList) do
    gUI.Mounts.gboxSkill3:SetItemGoods(index3, item.icon, -1)
    gUI.Mounts.gboxSkill3:SetItemData(index3, item.id)
    index3 = index3 + 1
    WindowToLabel(WindowSys_Instance:GetWindow("Mounts.Picture4_bg.GiftNumb0" .. tostring(index3) .. "_dlab")):SetProperty("Text", tostring(item.level) .. "/" .. tostring(item.maxLevel))
  end
  gUI.Mounts.dlabPoint:SetProperty("Text", tostring(GetSkillPoint()))
end
function Lua_Mount_GetRealSlot(slot)
  return _Mounts_CurPage * _Mounts_NumInOnePage + slot
end
function UI_Mounts_Show()
  if not gUI.Mounts.wndRoot:IsVisible() then
    UI_Mounts_Open()
  else
    UI_Mounts_Close()
  end
  WorldStage:playSoundByID(34)
end
function UI_Mounts_Open()
  gUI.Mounts.wndRoot:SetVisible(true)
  gUI.Mounts.showbox:show()
  gUI.Mounts.showbox:setrevertCamera(-50)
  _Mounts_UpdateAll()
end
function UI_Mounts_Close()
  gUI.Mounts.wndRoot:SetVisible(false)
  gUI.Mounts.showbox:hide()
end
function UI_Mounts_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Mounts.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Mounts_SetDefault()
  SetDefaultMount(_Mounts_CurSelMount)
  _Mounts_DefaultIndex = _Mounts_CurSelMount
  _Mounts_UpdateMounts(true)
  gUI.Mounts.cbtnDefault:SetEnable(false)
end
function UI_Mounts_DragDownItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  local from_win = WindowToGoodsBox(to_win:GetDragItemParent())
  local from_type = from_win:GetProperty("BoxType")
  local to_off = msg:get_lparam()
  local form_typeIndex = from_win:GetProperty("GoodBoxIndex")
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_bag = gUI_GOODSBOX_BAG[to_type]
  if form_typeIndex == "GB_Forge" then
    Lua_Forge_DragItem(from_win, gUI_GOODSBOX_BAG.Horse)
  elseif form_typeIndex == "GB_ItemMainBag" then
    local _, _, _, name, _, _, _, _ = GetItemBaseInfoBySlot(gUI_GOODSBOX_BAG.backpack0, from_off)
    local bUsingHorse = GetItemUsingInfo(gUI_GOODSBOX_BAG.backpack0, from_off)
    if bUsingHorse and isAutoVehicleMoving() then
      Lua_UI_ShowMessageInfo("不能在飞行状态下使用坐骑")
    elseif bUsingHorse then
    end
  end
end
function UI_Mounts_MountLClick(msg)
end
function UI_Mounts_MountRClick(msg)
  local index = msg:get_wparam()
  _Mounts_CurSelMount = index
  _Mounts_UpdateByIndex(index)
  local wnd = WindowSys_Instance:GetWindow("Forge")
  if wnd:IsVisible() then
    if Lua_Forge_CheckMount(index, 3) then
      SetEnhanceNeedItem(index, 1, 0, 3)
    end
    return
  end
  MountRide(index)
end
function UI_Mounts_ShowMountTips(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local index = msg:get_lparam()
  local tooltip = WindowToTooltip(gb:GetToolTipWnd(0))
  Lua_Tip_Item(tooltip, index, gUI_GOODSBOX_BAG.Horse, nil, nil, nil)
end
function UI_Mounts_UseSkill(msg)
  local skillId = msg:get_lparam()
end
function UI_Mounts_ShowSkillTips(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local skillId = msg:get_lparam()
  local tooltip = WindowToTooltip(gb:GetToolTipWnd(0))
  local currentlv = GetPlayerSkillLv(skillId)
  Lua_Tip_Skill(tooltip, skillId, currentlv, false, false)
end
function UI_Mounts_ShowGiftTips(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local giftId = msg:get_lparam()
  local tooltip = WindowToTooltip(gb:GetToolTipWnd(0))
  Lua_Tip_Gift(tooltip, giftId)
end
function UI_Mounts_AddGift(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local giftId = msg:get_wparam()
  MountAddGift(giftId)
end
function UI_Mounts_LastPage(msg)
  _Mounts_CurPage = _Mounts_CurPage - 1
  _Mounts_UpdateMounts(false)
  gUI.Mounts.gboxAllMounts:SetSelectedItem(0)
  _Mounts_UpdateByIndex(_Mounts_CurPage * _Mounts_NumInOnePage)
end
function UI_Mounts_NextPage(msg)
  _Mounts_CurPage = _Mounts_CurPage + 1
  _Mounts_UpdateMounts(false)
  gUI.Mounts.gboxAllMounts:SetSelectedItem(0)
  _Mounts_UpdateByIndex(_Mounts_CurPage * _Mounts_NumInOnePage)
end
function UI_Mounts_ModelLeftRotate()
  AddTimerEvent(2, _Mounts_Avatarotate, function()
    gUI.Mounts.showbox:onCharacterLeft()
  end)
end
function UI_Mounts_ModelRightRotate()
  AddTimerEvent(2, _Mounts_Avatarotate, function()
    gUI.Mounts.showbox:onCharacterRight()
  end)
end
function UI_Mounts_ModelStopRotate()
  DelTimerEvent(2, _Mounts_Avatarotate)
end
function UI_Mounts_ShowTip(msg)
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  local desc = ""
  if wndUserdata == 1 then
    desc = "坐骑评分可通过增加坐骑数量和锻造坐骑获得"
  elseif wndUserdata == 2 then
    desc = "坐骑锻造等级越高坐骑分数越高"
  end
  if desc ~= "" then
    Lua_Tip_Property(tooltip, desc)
  end
end
function Script_Mounts_OnLoad()
  _Mounts_InitCtrl()
  _Mounts_CurPage = 0
  _Mounts_Clear()
end
function Script_Mounts_OnEvent(event)
end
function Script_Mounts_OnHide()
end
