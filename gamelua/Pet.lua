if gUI and not gUI.Pet then
  gUI.Pet = {}
end
if gUI and not gUI.PetBlend then
  gUI.PetBlend = {}
end
if gUI and not gUI.PetCheck then
  gUI.PetCheck = {}
end
local _Pet_CanCarryMaxCnt = 5
local _Pet_CurrentPetIndex = -1
local _Pet_FriendlyMaxValue = 100
local _Pet_SkillCntMax = 10
local _Pet_EquipCntMax = 4
local _Pet_TempName = ""
local _Pet_ShowBoxName = "PetAvatar"
local _Pet_State1NeedLevel = 0
local _Pet_State2NeedLevel = 10
local _Pet_State3NeedLevel = 40
local _Pet_NewSkillSlotToReplace = 0
local _Pet_NewSkillBag = 0
local _Pet_NewSkillFromOff = 0
local _Pet_AutoFeedChecked = true
local _Pet_AutoFeedNum = 99
local _Pet_AutoFeedItemIndex = 0
local _Pet_BlendShowBoxName1 = "PetBlend1"
local _Pet_BlendShowBoxName2 = "PetBlend2"
local _Pet_BlendShowBoxName3 = "PetBlend3"
local _Pet_BlendMainSelectedIndex = -1
local _Pet_BlendSecondSelectedIndex = -1
local _Pet_OtherPetShowBoxName = "OtherPet"
local _Pet_OtherIndex = 10
local _Pet_CurrTime = 0
local _Pet_IntervaTime = 30
local _Pet_TipsCnt1 = 0
local _Pet_TipsCnt2 = 0
local _Pet_CurTipsType = 0
local _Pet_Avatarotate = "Pet_Rotate"
local _Pet_Avatarotate_Plend = "Pet_Rotate_Plend"
local _Pet_SkillLockIamge = "UiIamge004:PetSkillLock"
local _Pet_HostIndex = -1
local _Pet_SubIndex = -1
local _Pet_SelectIndex = -1
local USER_GUIDE_PLAYER_PETFEED = 43
local USER_GUIDE_PETFEED_UI = 92
local USER_GUIDE_PETFEED_UI_FRIEND = 100
local _Pet_Guide_PetFeedList = {}
function _Pet_InitPetCtrl()
  gUI.Pet.wndRoot = WindowSys_Instance:GetWindow("Pet")
  gUI.Pet.wndRoot2 = WindowSys_Instance:GetWindow("PetSkill")
  gUI.Pet.wndRoot3 = WindowSys_Instance:GetWindow("Pet.Feed_bg")
  gUI.PetBlend.wndRoot = WindowSys_Instance:GetWindow("PetBlend")
  gUI.PetCheck.wndRoot = WindowSys_Instance:GetWindow("CheckPet")
  gUI.Pet.btnPetRename = WindowToButton(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Di_bg.Rename_btn"))
  gUI.Pet.btnPetFree = WindowToButton(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Di_bg.Free_btn"))
  gUI.Pet.btnPetFed = WindowToButton(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Di_bg.Fed_btn"))
  gUI.Pet.btnPetBattle = WindowToButton(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Di_bg.Battle_btn"))
  gUI.Pet.dlabPetCnt = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Top_bg.List_bg.ListTitle_bg.Num_dlab"))
  gUI.Pet.lboxPet = WindowToListBox(WindowSys_Instance:GetWindow("Pet.Top_bg.List_bg.List_lbox"))
  gUI.Pet.gboxPetEquip = WindowToGoodsBox(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Equip_gbox"))
  gUI.Pet.eboxPetName = WindowToEditBox(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Name_bg.Name_ebox"))
  gUI.Pet.dlabPetLevel = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Level_bg.Level_dlab"))
  gUI.Pet.picAvatar = WindowToPicture(WindowSys_Instance:GetWindow("Pet.Top_bg.Avatar_bg.Avatar_pic"))
  gUI.Pet.AllPetgb = WindowToGoodsBox(WindowSys_Instance:GetWindow("Pet.Top_bg.List_bg.GoodsBox1"))
  gUI.Pet.AllPetgb:SetProperty("ItemCanDrag", "false")
  gUI.Pet.dlabPetStr = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Base_bg.Str_dlab"))
  gUI.Pet.dlabPetDex = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Base_bg.Dex_dlab"))
  gUI.Pet.dlabPetInt = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Base_bg.Int_dlab"))
  gUI.Pet.dlabPetCon = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Base_bg.Con_dlab"))
  gUI.Pet.dlabPetScore = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Base_bg.Grade_dlab"))
  gUI.Pet.dlabRank = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Base_bg.Ranking_dlab"))
  gUI.Pet.pbarPetExp = WindowToProgressBar(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Exp_pbar"))
  gUI.Pet.dlabPetExp = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Exp_pbar.Exp_dlab"))
  gUI.Pet.pbarPetFriendly = WindowToProgressBar(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Friendly_pbar"))
  gUI.Pet.dlabPetFirendlyUp = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Friendly_pbar.Friendly_dlab"))
  gUI.Pet.dlabPetStrUp = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Str_pbar.Str_dlab"))
  gUI.Pet.dlabPetDexUp = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Dex_pbar.Dex_dlab"))
  gUI.Pet.dlabPetIntUp = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Int_pbar.Int_dlab"))
  gUI.Pet.dlabPetConUp = WindowToLabel(WindowSys_Instance:GetWindow("Pet.Center_bg.Grow_bg.Con_pbar.Con_dlab"))
  gUI.Pet.gboxPetSkill = WindowToGoodsBox(WindowSys_Instance:GetWindow("Pet.Bottom_bg.Skill_gbox"))
  gUI.Pet.gboxPetSkillReplace = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetSkill.Center_bg.Skill_gbox"))
  gUI.Pet.gboxPetSkillNew = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetSkill.Center_bg.NewSkill_gbox"))
  gUI.Pet.slabPetSkillName = WindowToLabel(WindowSys_Instance:GetWindow("PetSkill.Center_bg.SkillName_slab"))
  gUI.Pet.dlabPetName = WindowToLabel(WindowSys_Instance:GetWindow("PetSkill.Center_bg.Name_bg.Name_dlab"))
  gUI.Pet.btnPetSkillReplace = WindowToButton(WindowSys_Instance:GetWindow("PetSkill.Center_bg.Sure_btn"))
  gUI.Pet.cbtnPetFeed = WindowToCheckButton(WindowSys_Instance:GetWindow("Pet.Feed_bg.Feed_cbtn"))
  gUI.Pet.eboxPetFeedNum = WindowToEditBox(WindowSys_Instance:GetWindow("Pet.Feed_bg.Num_bg.Num_ebox"))
  gUI.Pet.gboxPetFeed = WindowToGoodsBox(WindowSys_Instance:GetWindow("Pet.Feed_bg.item_bgox"))
  local showbox = UIInterface:getShowbox(_Pet_ShowBoxName)
  showbox = showbox or UIInterface:createShowbox(_Pet_ShowBoxName, gUI.Pet.picAvatar)
  gUI.Pet.showbox = showbox
  gUI.PetBlend.cboxPetBlendMain = WindowToComboBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.ComboBox_cbox"))
  gUI.PetBlend.cboxPetBlendSecond = WindowToComboBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.ComboBox_cbox"))
  gUI.Pet.gboxPetBlend1 = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture1_bg.GoodsBox1"))
  gUI.Pet.gboxPetBlend1:SetProperty("ItemCanDrag", "false")
  gUI.Pet.gboxPetBlend2 = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture1_bg.GoodsBox1"))
  gUI.Pet.gboxPetBlend2:SetProperty("ItemCanDrag", "false")
  gUI.Pet.dlabBlendPetExp1 = WindowToProgressBar(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Exp_pbar"))
  gUI.Pet.dlabBlendPetExp2 = WindowToProgressBar(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Exp_pbar"))
  gUI.Pet.dlabBlendPetExp3 = WindowToProgressBar(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture3_bg.Exp_pbar"))
  gUI.Pet.dlabBlendPetExplab = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture3_bg.Exp_pbar.Exp_dlab"))
  gUI.PetBlend.picPetBlend3 = WindowToPicture(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture1_bg.Avatar_pic"))
  gUI.PetBlend.dlabPet1Str = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label1_dlab"))
  gUI.PetBlend.dlabPet1Dex = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label2_dlab"))
  gUI.PetBlend.dlabPet1Int = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label3_dlab"))
  gUI.PetBlend.dlabPet1Con = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label4_dlab"))
  gUI.PetBlend.dlabPet1StrUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label5_dlab"))
  gUI.PetBlend.dlabPet1DexUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label6_dlab"))
  gUI.PetBlend.dlabPet1IntUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label7_dlab"))
  gUI.PetBlend.dlabPet1ConUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.Picture2_bg.Picture2_bg.Label8_dlab"))
  gUI.PetBlend.dlabPet2Str = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label1_dlab"))
  gUI.PetBlend.dlabPet2Dex = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label2_dlab"))
  gUI.PetBlend.dlabPet2Int = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label3_dlab"))
  gUI.PetBlend.dlabPet2Con = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label4_dlab"))
  gUI.PetBlend.dlabPet2StrUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label5_dlab"))
  gUI.PetBlend.dlabPet2DexUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label6_dlab"))
  gUI.PetBlend.dlabPet2IntUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label7_dlab"))
  gUI.PetBlend.dlabPet2ConUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label8_dlab"))
  gUI.PetBlend.gboxPet1Skill = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture1_bg.GoodsBox_gbox"))
  gUI.PetBlend.gboxPet2Skill = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture2_bg.GoodsBox_gbox"))
  gUI.PetBlend.dlabPetName = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture3_bg.PetName_dlab"))
  gUI.PetBlend.dlabPetLevel = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture3_bg.PetLevel_dlab"))
  gUI.PetBlend.dlabPetStr = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label1_dlab"))
  gUI.PetBlend.dlabPetDex = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label2_dlab"))
  gUI.PetBlend.dlabPetInt = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label3_dlab"))
  gUI.PetBlend.dlabPetCon = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label4_dlab"))
  gUI.PetBlend.dlabPetStrUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label5_dlab"))
  gUI.PetBlend.dlabPetDexUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label6_dlab"))
  gUI.PetBlend.dlabPetIntUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label7_dlab"))
  gUI.PetBlend.dlabPetConUp = WindowToLabel(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture2_bg.Picture2_bg.Label8_dlab"))
  gUI.PetBlend.gboxPetSkill = WindowToGoodsBox(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.GoodsBox_gbox"))
  gUI.Pet.dlabBlendPetExp1:SetProperty("Progress", "0")
  gUI.Pet.dlabBlendPetExp2:SetProperty("Progress", "0")
  showbox = UIInterface:getShowbox(_Pet_BlendShowBoxName3)
  showbox = showbox or UIInterface:createShowbox(_Pet_BlendShowBoxName3, gUI.PetBlend.picPetBlend3)
  gUI.PetBlend.showbox3 = showbox
  gUI.PetCheck.dlabTitle = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Title_dlab"))
  gUI.PetCheck.dlabPetName = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.PetName_dlab"))
  gUI.PetCheck.dlabPetLevel = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.PetLevel_btn"))
  gUI.PetCheck.picPet = WindowToPicture(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture1_bg.Avatar_pic"))
  gUI.PetCheck.gboxEquip = WindowToGoodsBox(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.GoodsBox1_gbox"))
  gUI.PetCheck.dlabStr = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label1_dlab"))
  gUI.PetCheck.dlabDex = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label2_dlab"))
  gUI.PetCheck.dlabInt = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label3_dlab"))
  gUI.PetCheck.dlabCon = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label4_dlab"))
  gUI.PetCheck.dlabStrUp = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label5_dlab"))
  gUI.PetCheck.dlabDexUp = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label6_dlab"))
  gUI.PetCheck.dlabIntUp = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label7_dlab"))
  gUI.PetCheck.dlabConUp = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label8_dlab"))
  gUI.PetCheck.dlabScore = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.Picture2_bg.Picture2_bg.Label9_dlab"))
  gUI.PetCheck.gboxSkill = WindowToGoodsBox(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.GoodsBox_gbox"))
  gUI.PetCheck.pbarPetFriendly = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.ProgressBar1"))
  gUI.PetCheck.dlabPetFirendlyUp = WindowToLabel(WindowSys_Instance:GetWindow("CheckPet.Picture2_bg.ProgressBar1.Label1"))
  showbox = UIInterface:getShowbox(_Pet_OtherPetShowBoxName)
  showbox = showbox or UIInterface:createShowbox(_Pet_OtherPetShowBoxName, gUI.PetCheck.picPet)
  gUI.PetCheck.showbox = showbox
  AddTimerEvent(2, "PetTimer", _Pet_TimeEvent)
end
function _Pet_TimeEvent(_timer)
  _Pet_CurrTime = _Pet_CurrTime + _timer:GetRealInterval()
  if _Pet_CurrTime <= _Pet_IntervaTime then
    return
  end
  _Pet_CurrTime = 0
  local index = Lua_Pet_GetCurrentPetIndex()
  if index == -1 then
    return
  end
  if _Pet_TipsCnt1 < 84 and GetPetFriendly(index) < 100 then
    _Pet_TipsCnt1 = _Pet_TipsCnt1 + 1
    PetPaoPao("晒晒太阳吃吃饱，抠抠脚丫洗洗澡，生活真美好{##^40}")
  end
  if _Pet_TipsCnt1 < 5 and GetPetFriendly(index) < 30 then
    _Pet_TipsCnt1 = _Pet_TipsCnt1 + 1
    PetPaoPao("主人快乐度下降会影响我对你加成啦，快让我吃饱点{##^38}")
  end
  if _Pet_TipsCnt2 < 5 and GetPetFriendly(index) == 0 then
    _Pet_TipsCnt2 = _Pet_TipsCnt2 + 1
    PetPaoPao("主人，我要饿死了{##^36}")
  end
end
function _Pet_Clear()
  gUI.Pet.btnPetRename:SetEnable(false)
  gUI.Pet.btnPetFree:SetEnable(false)
  gUI.Pet.btnPetFed:SetEnable(false)
  gUI.Pet.btnPetBattle:SetEnable(false)
  gUI.Pet.dlabPetCnt:SetProperty("Text", "")
  gUI.Pet.eboxPetName:SetProperty("Text", "")
  gUI.Pet.dlabPetLevel:SetProperty("Text", "")
  gUI.Pet.gboxPetEquip:ResetAllGoods(true)
  gUI.Pet.pbarPetExp:SetProperty("Progress", "0")
  gUI.Pet.dlabPetExp:SetProperty("Text", "")
  gUI.Pet.dlabPetStr:SetProperty("Text", "")
  gUI.Pet.dlabPetDex:SetProperty("Text", "")
  gUI.Pet.dlabPetInt:SetProperty("Text", "")
  gUI.Pet.dlabPetCon:SetProperty("Text", "")
  gUI.Pet.dlabPetScore:SetProperty("Text", "")
  gUI.Pet.dlabRank:SetProperty("Text", "")
  gUI.Pet.pbarPetFriendly:SetProperty("Progress", "0")
  gUI.Pet.dlabPetFirendlyUp:SetProperty("Text", "")
  gUI.Pet.dlabPetStrUp:SetProperty("Text", "")
  gUI.Pet.dlabPetDexUp:SetProperty("Text", "")
  gUI.Pet.dlabPetIntUp:SetProperty("Text", "")
  gUI.Pet.dlabPetConUp:SetProperty("Text", "")
  gUI.Pet.gboxPetSkill:ResetAllGoods(true)
  gUI.Pet.picAvatar:SetVisible(false)
  gUI.Pet.gboxPetFeed:ClearGoodsItem(0)
  gUI.Pet.dlabBlendPetExp1:SetProperty("Progress", "0")
  gUI.Pet.dlabBlendPetExp2:SetProperty("Progress", "0")
  gUI.Pet.dlabBlendPetExp3:SetProperty("Progress", "0")
  gUI.Pet.dlabBlendPetExplab:SetProperty("Text", "")
end
function _Pet_Update()
  _Pet_UpdatePetFightInfo()
  if _Pet_CurrentPetIndex ~= -1 then
    _Pet_UpdateById(_Pet_CurrentPetIndex)
  else
    _Pet_Clear()
  end
end
function _Pet_UpdateById(index)
  gUI.Pet.picAvatar:SetVisible(true)
  gUI.Pet.btnPetRename:SetEnable(true)
  gUI.Pet.btnPetFree:SetEnable(true)
  gUI.Pet.btnPetFed:SetEnable(true)
  gUI.Pet.btnPetBattle:SetEnable(true)
  local CurPetCnt = gUI.Pet.lboxPet:GetItemCount()
  gUI.Pet.dlabPetCnt:SetProperty("Text", CurPetCnt .. "/" .. _Pet_CanCarryMaxCnt)
  _Pet_UpdateBaseInfoById(index)
  _Pet_UpdateAttrById(index)
  _Pet_UpdateSkillById(index)
  AskPetRank(index)
end
function _Pet_UpdateBaseInfoById(index)
  local name = GetPetName(index)
  local level = GetPetLevel(index)
  local edit = gUI.Pet.eboxPetName
  edit:SetProperty("Text", name)
  gUI.Pet.dlabPetLevel:SetProperty("Text", level .. "级")
  if not PetIsInFight(index) then
    gUI.Pet.btnPetBattle:SetProperty("Text", "出战")
  else
    gUI.Pet.btnPetBattle:SetProperty("Text", "收回")
  end
  for nCount = 0, _Pet_CanCarryMaxCnt - 1 do
    local icon = GetPetIcon(nCount)
    if icon ~= nil then
      if not PetIsInFight(nCount) then
        gUI.Pet.AllPetgb:SetItemGoods(nCount, icon, -1)
      else
        gUI.Pet.AllPetgb:SetItemGoods(nCount, icon, 20)
      end
    end
  end
  local state = 0
  if level >= _Pet_State1NeedLevel and level < _Pet_State2NeedLevel then
    gUI.Pet.gboxPetEquip:SetVisible(false)
    gUI.Pet.gboxPetEquip:SetEnable(false)
    state = 0
  elseif level >= _Pet_State2NeedLevel and level < _Pet_State3NeedLevel then
    gUI.Pet.gboxPetEquip:SetEnable(false)
    state = 1
  elseif level >= _Pet_State3NeedLevel then
    gUI.Pet.gboxPetEquip:SetEnable(true)
    if not GetIsPetEquipFull(_Pet_CurrentPetIndex) then
      state = 2
    else
      state = 3
    end
  end
  gUI.Pet.showbox:clearAvatar(_Pet_CurrentPetIndex .. tostring(state) .. "Normal")
  if not gUI.Pet.showbox:isAvatarExist(_Pet_CurrentPetIndex .. tostring(state) .. "Normal") then
    gUI.Pet.showbox:createPetAvatar(_Pet_CurrentPetIndex, state, "Normal")
  end
  gUI.Pet.showbox:setCurrentAvatar(_Pet_CurrentPetIndex .. tostring(state) .. "Normal")
  local gb = gUI.Pet.gboxPetEquip
  for equipPos = 0, _Pet_EquipCntMax do
    local equipIcon = GetPetEquipIcon(index, equipPos)
    local equipId = equipPos
    if equipIcon ~= nil then
      if string.len(equipIcon) == 0 then
        gb:ClearGoodsItem(equipPos)
      else
        gb:SetItemGoods(equipPos, equipIcon, -1)
        gb:SetItemData(equipPos, equipId)
      end
    else
      gb:ClearGoodsItem(equipPos)
    end
  end
end
function _Pet_GetAttrcolour(nNum)
  local str = ""
  if nNum > 1200 and nNum <= 1500 then
    str = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[1] .. "^" .. tostring(nNum) .. "}"
  elseif nNum > 1500 and nNum <= 2000 then
    str = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[2] .. "^" .. tostring(nNum) .. "}"
  elseif nNum > 2000 and nNum <= 2300 then
    str = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[3] .. "^" .. tostring(nNum) .. "}"
  elseif nNum > 2300 and nNum <= 3000 then
    str = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[4] .. "^" .. tostring(nNum) .. "}"
  elseif nNum > 3000 then
    str = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[5] .. "^" .. tostring(nNum) .. "}"
  else
    str = "{#C^" .. gUI_GOODS_QUALITY_COLOR_PREFIX[0] .. "^" .. tostring(nNum) .. "}"
  end
  return str
end
function _Pet_UpdateAttrById(index)
  gUI.Pet.pbarPetExp:SetProperty("Progress", tostring(GetPetExpCur(index) / GetPetExpMax(index)))
  gUI.Pet.dlabPetExp:SetProperty("Text", tostring(GetPetExpCur(index)) .. "/" .. tostring(GetPetExpMax(index)))
  gUI.Pet.dlabPetStr:SetProperty("Text", tostring(GetPetStr(index)))
  gUI.Pet.dlabPetDex:SetProperty("Text", tostring(GetPetDex(index)))
  gUI.Pet.dlabPetInt:SetProperty("Text", tostring(GetPetInt(index)))
  gUI.Pet.dlabPetCon:SetProperty("Text", tostring(GetPetCon(index)))
  gUI.Pet.pbarPetFriendly:SetProperty("Progress", tostring(GetPetFriendly(index) / _Pet_FriendlyMaxValue))
  gUI.Pet.dlabPetFirendlyUp:SetProperty("Text", tostring(GetPetFriendly(index)) .. "/" .. _Pet_FriendlyMaxValue)
  gUI.Pet.dlabPetStrUp:SetProperty("Text", _Pet_GetAttrcolour(GetPetStrUp(index)))
  gUI.Pet.dlabPetDexUp:SetProperty("Text", _Pet_GetAttrcolour(GetPetDexUp(index)))
  gUI.Pet.dlabPetIntUp:SetProperty("Text", _Pet_GetAttrcolour(GetPetIntUp(index)))
  gUI.Pet.dlabPetConUp:SetProperty("Text", _Pet_GetAttrcolour(GetPetConUp(index)))
  gUI.Pet.dlabPetScore:SetProperty("Text", tostring(PetGetScore(index)))
end
function _Pet_UpdateSkillById(index)
  local gb = gUI.Pet.gboxPetSkill
  for skillPos = 0, _Pet_SkillCntMax do
    gb:ClearGoodsItem(skillPos)
    gb:SetItemData(skillPos, 0)
  end
  for skillPos = 0, _Pet_SkillCntMax do
    local skillId = GetPetSkillId(index, skillPos)
    local skillIcon = GetPetSkillIcon(index, skillPos)
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(skillPos)
      else
        gb:SetItemGoods(skillPos, skillIcon, -1)
        gb:SetItemData(skillPos, skillId)
      end
    else
      gb:ClearGoodsItem(skillPos)
      gb:SetItemData(skillPos, 0)
    end
  end
  local slotCnt = GetPetSkillSlotCnt(index)
  for skillPos = slotCnt, _Pet_SkillCntMax do
    gb:SetItemGoods(skillPos, _Pet_SkillLockIamge, -1)
  end
end
function _Pet_ShowSkillReplace(skillId, bag, from_off)
  if not gUI.Pet.wndRoot2:IsVisible() then
    gUI.Pet.wndRoot2:SetVisible(true)
    _Pet_UpdateSkillReplace(_Pet_CurrentPetIndex, skillId, bag, from_off)
  else
    UI_Pet_CloseSkillReplace()
  end
end
function _Pet_UpdateSkillReplace(index, skillIdNew, bag, from_off)
  _Pet_NewSkillBag = bag
  _Pet_NewSkillFromOff = from_off
  local gb = gUI.Pet.gboxPetSkillReplace
  gb:ResetAllGoods(true)
  for skillPos = 0, _Pet_SkillCntMax do
    local skillId = GetPetSkillId(index, skillPos)
    local skillIcon = GetPetSkillIcon(index, skillPos)
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(skillPos)
      else
        gb:SetItemGoods(skillPos, skillIcon, -1)
        gb:SetItemData(skillPos, skillId)
      end
    else
      gb:ClearGoodsItem(skillPos)
    end
  end
  gb = gUI.Pet.gboxPetSkillNew
  gb:ResetAllGoods(true)
  local name, icon, category, Type, desc = GetSkillBaseInfo(skillIdNew)
  gb:SetItemGoods(0, icon, -1)
  gb:SetItemData(0, skillIdNew)
  gUI.Pet.slabPetSkillName:SetProperty("Text", name)
  gUI.Pet.dlabPetName:SetProperty("Text", GetPetName(index))
  gUI.Pet.btnPetSkillReplace:SetEnable(false)
end
function _Pet_StartRendering()
  gUI.Pet.showbox:show()
end
function _Pet_EndRendering()
  gUI.Pet.showbox:hide()
end
function _Pet_UpdateBlend()
  local combo1 = gUI.PetBlend.cboxPetBlendMain
  local combo2 = gUI.PetBlend.cboxPetBlendSecond
  combo1:RemoveAllItems()
  combo2:RemoveAllItems()
  local petIndex = 0
  for i = 0, gUI.Pet.lboxPet:GetItemCount() - 1 do
    petIndex = gUI.Pet.lboxPet:GetItemData(i)
    combo1:InsertString(GetPetName(petIndex) .. " " .. tostring(GetPetLevel(petIndex)) .. "级", 4294967295)
    combo1:SetItemData(i, petIndex)
  end
  combo1:SetEditBoxText("请选择主宠物")
  for i = 0, gUI.Pet.lboxPet:GetItemCount() - 1 do
    petIndex = gUI.Pet.lboxPet:GetItemData(i)
    combo2:InsertString(GetPetName(petIndex) .. " " .. tostring(GetPetLevel(petIndex)) .. "级", 4294967295)
    combo2:SetItemData(i, petIndex)
  end
  combo2:SetEditBoxText("请选择副宠物")
  local gb1 = gUI.PetBlend.gboxPet1Skill
  local gb2 = gUI.PetBlend.gboxPet2Skill
  gb1:SetVisibleSlotCount(10)
  gb2:SetVisibleSlotCount(10)
  for skillPos = 0, _Pet_SkillCntMax do
    gb1:ClearGoodsItem(skillPos)
    gb1:SetItemData(skillPos, 0)
    gb2:ClearGoodsItem(skillPos)
    gb2:SetItemData(skillPos, 0)
  end
end
function _Pet_UpdateBlendMainById(index)
  local name = GetPetName(index)
  local level = GetPetLevel(index)
  gUI.PetBlend.dlabPet1Str:SetProperty("Text", tostring(GetPetStr(index)))
  gUI.PetBlend.dlabPet1Dex:SetProperty("Text", tostring(GetPetDex(index)))
  gUI.PetBlend.dlabPet1Con:SetProperty("Text", tostring(GetPetCon(index)))
  gUI.PetBlend.dlabPet1Int:SetProperty("Text", tostring(GetPetInt(index)))
  gUI.PetBlend.dlabPet1StrUp:SetProperty("Text", tostring(GetPetStrUp(index)))
  gUI.PetBlend.dlabPet1DexUp:SetProperty("Text", tostring(GetPetDexUp(index)))
  gUI.PetBlend.dlabPet1ConUp:SetProperty("Text", tostring(GetPetConUp(index)))
  gUI.PetBlend.dlabPet1IntUp:SetProperty("Text", tostring(GetPetIntUp(index)))
  gUI.Pet.dlabBlendPetExp1:SetProperty("Progress", tostring(GetPetExpCur(index) / GetPetExpMax(index)))
  local state = 0
  if level >= _Pet_State1NeedLevel and level < _Pet_State2NeedLevel then
    gUI.Pet.gboxPetEquip:SetVisible(false)
    gUI.Pet.gboxPetEquip:SetEnable(false)
    state = 0
  elseif level >= _Pet_State2NeedLevel and level < _Pet_State3NeedLevel then
    gUI.Pet.gboxPetEquip:SetEnable(false)
    state = 1
  elseif level >= _Pet_State3NeedLevel then
    gUI.Pet.gboxPetEquip:SetEnable(true)
    if not GetIsPetEquipFull(index) then
      state = 2
    else
      state = 3
    end
  end
  gUI.Pet.gboxPetBlend1:ResetAllGoods(true)
  local icon = GetPetIcon(index)
  if icon ~= nil then
    gUI.Pet.gboxPetBlend1:SetItemGoods(0, icon, -1)
  end
  local gb = gUI.PetBlend.gboxPet1Skill
  for skillPos = 0, _Pet_SkillCntMax do
    gb:ClearGoodsItem(skillPos)
    gb:SetItemData(skillPos, 0)
  end
  for skillPos = 0, _Pet_SkillCntMax do
    local skillId = GetPetSkillId(index, skillPos)
    local skillIcon = GetPetSkillIcon(index, skillPos)
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(skillPos)
      else
        gb:SetItemGoods(skillPos, skillIcon, -1)
        gb:SetItemData(skillPos, skillId)
      end
    else
      gb:ClearGoodsItem(skillPos)
      gb:SetItemData(skillPos, 0)
    end
  end
  local slotCnt = GetPetSkillSlotCnt(index)
  for skillPos = slotCnt, _Pet_SkillCntMax do
    gb:SetItemGoods(skillPos, _Pet_SkillLockIamge, -1)
  end
end
function _Pet_UpdateBlendSecondById(index)
  local name = GetPetName(index)
  local level = GetPetLevel(index)
  gUI.PetBlend.dlabPet2Str:SetProperty("Text", tostring(GetPetStr(index)))
  gUI.PetBlend.dlabPet2Dex:SetProperty("Text", tostring(GetPetDex(index)))
  gUI.PetBlend.dlabPet2Con:SetProperty("Text", tostring(GetPetCon(index)))
  gUI.PetBlend.dlabPet2Int:SetProperty("Text", tostring(GetPetInt(index)))
  gUI.PetBlend.dlabPet2StrUp:SetProperty("Text", tostring(GetPetStrUp(index)))
  gUI.PetBlend.dlabPet2DexUp:SetProperty("Text", tostring(GetPetDexUp(index)))
  gUI.PetBlend.dlabPet2ConUp:SetProperty("Text", tostring(GetPetConUp(index)))
  gUI.PetBlend.dlabPet2IntUp:SetProperty("Text", tostring(GetPetIntUp(index)))
  gUI.Pet.dlabBlendPetExp2:SetProperty("Progress", tostring(GetPetExpCur(index) / GetPetExpMax(index)))
  local state = 0
  if level >= _Pet_State1NeedLevel and level < _Pet_State2NeedLevel then
    gUI.Pet.gboxPetEquip:SetVisible(false)
    gUI.Pet.gboxPetEquip:SetEnable(false)
    state = 0
  elseif level >= _Pet_State2NeedLevel and level < _Pet_State3NeedLevel then
    gUI.Pet.gboxPetEquip:SetEnable(false)
    state = 1
  elseif level >= _Pet_State3NeedLevel then
    gUI.Pet.gboxPetEquip:SetEnable(true)
    if not GetIsPetEquipFull(index) then
      state = 2
    else
      state = 3
    end
  end
  gUI.Pet.gboxPetBlend2:ResetAllGoods(true)
  local icon = GetPetIcon(index)
  if icon ~= nil then
    gUI.Pet.gboxPetBlend2:SetItemGoods(0, icon, -1)
  end
  local gb = gUI.PetBlend.gboxPet2Skill
  for skillPos = 0, _Pet_SkillCntMax do
    gb:ClearGoodsItem(skillPos)
    gb:SetItemData(skillPos, 0)
  end
  for skillPos = 0, _Pet_SkillCntMax do
    local skillId = GetPetSkillId(index, skillPos)
    local skillIcon = GetPetSkillIcon(index, skillPos)
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(skillPos)
      else
        gb:SetItemGoods(skillPos, skillIcon, -1)
        gb:SetItemData(skillPos, skillId)
      end
    else
      gb:ClearGoodsItem(skillPos)
      gb:SetItemData(skillPos, 0)
    end
  end
  local slotCnt = GetPetSkillSlotCnt(index)
  for skillPos = slotCnt, _Pet_SkillCntMax do
    gb:SetItemGoods(skillPos, _Pet_SkillLockIamge, -1)
  end
end
function _Pet_BlendStartRendering()
  gUI.PetBlend.showbox3:show()
end
function _Pet_BlendEndRendering()
  gUI.PetBlend.showbox3:hide()
end
function _Pet_UpdateBlendSecondList(index)
  local combo2 = gUI.PetBlend.cboxPetBlendSecond
  combo2:RemoveAllItems()
  local ctrlIndex = 0
  for i = 0, gUI.Pet.lboxPet:GetItemCount() - 1 do
    local petIndex = gUI.Pet.lboxPet:GetItemData(i)
    if _Pet_BlendMainSelectedIndex ~= petIndex then
      combo2:InsertString(GetPetName(petIndex) .. " " .. tostring(GetPetLevel(petIndex)) .. "级", 4294967295)
      combo2:SetItemData(ctrlIndex, petIndex)
      ctrlIndex = ctrlIndex + 1
    end
  end
  if index == _Pet_BlendSecondSelectedIndex or _Pet_BlendSecondSelectedIndex == -1 then
    combo2:SetEditBoxText("请选择副宠物")
    if index == _Pet_BlendSecondSelectedIndex then
      PetUnselectBlendPet(index, 1)
    end
    _Pet_ClearBlendSecond()
    _Pet_ClearBlend3()
    _Pet_BlendSecondSelectedIndex = -1
  else
    combo2:SetEditBoxText(GetPetName(_Pet_BlendSecondSelectedIndex) .. " " .. tostring(GetPetLevel(_Pet_BlendSecondSelectedIndex)) .. "级", 4294967295)
  end
end
function _Pet_ClearBlendMain()
  gUI.PetBlend.dlabPet1Str:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1Dex:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1Con:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1Int:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1StrUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1DexUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1ConUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPet1IntUp:SetProperty("Text", "")
  gUI.Pet.gboxPetBlend1:ResetAllGoods(true)
  gUI.PetBlend.gboxPet1Skill:ClearGoodsItem(0)
end
function _Pet_ClearBlendSecond()
  gUI.PetBlend.dlabPet2Str:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2Dex:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2Con:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2Int:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2StrUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2DexUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2ConUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPet2IntUp:SetProperty("Text", "")
  gUI.Pet.gboxPetBlend2:ResetAllGoods(true)
  gUI.PetBlend.gboxPet2Skill:ClearGoodsItem(0)
end
function _Pet_ClearBlend3()
  gUI.PetBlend.dlabPetName:SetProperty("Text", "")
  gUI.PetBlend.dlabPetLevel:SetProperty("Text", "")
  gUI.PetBlend.dlabPetStr:SetProperty("Text", "")
  gUI.PetBlend.dlabPetDex:SetProperty("Text", "")
  gUI.PetBlend.dlabPetCon:SetProperty("Text", "")
  gUI.PetBlend.dlabPetInt:SetProperty("Text", "")
  gUI.PetBlend.dlabPetStrUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPetDexUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPetConUp:SetProperty("Text", "")
  gUI.PetBlend.dlabPetIntUp:SetProperty("Text", "")
  gUI.Pet.dlabBlendPetExp3:SetProperty("Progress", "0")
  gUI.Pet.dlabBlendPetExplab:SetProperty("Text", "")
  gUI.PetBlend.picPetBlend3:SetVisible(false)
  for i = 0, 9 do
    local wnd = WindowToPicture(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture3_bg.Picture" .. tostring(i + 1) .. "_bg"))
    wnd:SetVisible(false)
  end
end
function _Pet_UpdateBlendResult()
  if _Pet_BlendMainSelectedIndex == -1 or _Pet_BlendSecondSelectedIndex == -1 then
    return
  end
  gUI.PetBlend.picPetBlend3:SetVisible(true)
  local name = GetPetName(_Pet_BlendMainSelectedIndex)
  local level1 = GetPetLevel(_Pet_BlendMainSelectedIndex)
  local level2 = GetPetLevel(_Pet_BlendSecondSelectedIndex)
  local exp1 = GetPetExp(_Pet_BlendMainSelectedIndex)
  local exp2 = GetPetExp(_Pet_BlendSecondSelectedIndex)
  local nIndex = _Pet_BlendMainSelectedIndex
  if level1 < level2 then
    level1 = level2
    exp1 = exp2
    nIndex = _Pet_BlendSecondSelectedIndex
  elseif level1 == level2 and exp2 > exp1 then
    exp1 = exp2
    nIndex = _Pet_BlendSecondSelectedIndex
  end
  gUI.PetBlend.dlabPetName:SetProperty("Text", tostring(name))
  gUI.PetBlend.dlabPetLevel:SetProperty("Text", tostring(level1) .. "级")
  gUI.Pet.dlabBlendPetExp3:SetProperty("Progress", tostring(GetPetExpCur(nIndex) / GetPetExpMax(nIndex)))
  gUI.Pet.dlabBlendPetExplab:SetProperty("Text", tostring(GetPetExpCur(nIndex)) .. "/" .. tostring(GetPetExpMax(nIndex)))
  local factor, value = GetPetStrUp(_Pet_BlendMainSelectedIndex)
  gUI.PetBlend.dlabPetStr:SetProperty("Text", tostring(PetCalculateAttribute(0, level1, factor, value)))
  factor, value = GetPetDexUp(_Pet_BlendMainSelectedIndex)
  gUI.PetBlend.dlabPetDex:SetProperty("Text", tostring(PetCalculateAttribute(1, level1, factor, value)))
  factor, value = GetPetConUp(_Pet_BlendMainSelectedIndex)
  gUI.PetBlend.dlabPetCon:SetProperty("Text", tostring(PetCalculateAttribute(2, level1, factor, value)))
  factor, value = GetPetIntUp(_Pet_BlendMainSelectedIndex)
  gUI.PetBlend.dlabPetInt:SetProperty("Text", tostring(PetCalculateAttribute(3, level1, factor, value)))
  gUI.PetBlend.dlabPetStrUp:SetProperty("Text", tostring(GetPetStrUp(_Pet_BlendMainSelectedIndex)))
  gUI.PetBlend.dlabPetDexUp:SetProperty("Text", tostring(GetPetDexUp(_Pet_BlendMainSelectedIndex)))
  gUI.PetBlend.dlabPetConUp:SetProperty("Text", tostring(GetPetConUp(_Pet_BlendMainSelectedIndex)))
  gUI.PetBlend.dlabPetIntUp:SetProperty("Text", tostring(GetPetIntUp(_Pet_BlendMainSelectedIndex)))
  local state = 0
  if level1 >= _Pet_State1NeedLevel and level1 < _Pet_State2NeedLevel then
    state = 0
  elseif level1 >= _Pet_State2NeedLevel and level1 < _Pet_State3NeedLevel then
    state = 1
  elseif level1 >= _Pet_State3NeedLevel then
    if not GetIsPetEquipFull(_Pet_BlendMainSelectedIndex) then
      state = 2
    else
      state = 3
    end
  end
  gUI.PetBlend.showbox3:clearAvatar(_Pet_BlendMainSelectedIndex .. tostring(state) .. "Diff")
  if not gUI.PetBlend.showbox3:isAvatarExist(_Pet_BlendMainSelectedIndex .. tostring(state) .. "Diff") then
    gUI.PetBlend.showbox3:createPetAvatar(_Pet_BlendMainSelectedIndex, state, "Diff")
  end
  gUI.PetBlend.showbox3:setCurrentAvatar(_Pet_BlendMainSelectedIndex .. tostring(state) .. "Diff")
  local slotCnt = GetPetSkillSlotCnt(_Pet_BlendMainSelectedIndex)
  for i = 0, 9 do
    local wnd = WindowToPicture(WindowSys_Instance:GetWindow("PetBlend.Pic_bg.Picture3_bg.Picture2_bg.Picture3_bg.Picture" .. tostring(i + 1) .. "_bg"))
    if i > slotCnt - 1 then
      wnd:SetVisible(false)
    else
      wnd:SetVisible(true)
    end
  end
end
function _Pet_UpdateOther(ownerName)
  local name = GetPetName(_Pet_OtherIndex)
  local level = GetPetLevel(_Pet_OtherIndex)
  gUI.PetCheck.dlabTitle:SetProperty("Text", tostring(ownerName) .. "的宠物")
  gUI.PetCheck.dlabPetName:SetProperty("Text", tostring(name))
  gUI.PetCheck.dlabPetLevel:SetProperty("Text", tostring(level) .. "级")
  gUI.PetCheck.dlabStr:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetStr(_Pet_OtherIndex))))
  gUI.PetCheck.dlabDex:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetDex(_Pet_OtherIndex))))
  gUI.PetCheck.dlabCon:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetCon(_Pet_OtherIndex))))
  gUI.PetCheck.dlabInt:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetInt(_Pet_OtherIndex))))
  gUI.PetCheck.dlabStrUp:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetStrUp(_Pet_OtherIndex))))
  gUI.PetCheck.dlabDexUp:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetDexUp(_Pet_OtherIndex))))
  gUI.PetCheck.dlabConUp:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetConUp(_Pet_OtherIndex))))
  gUI.PetCheck.dlabIntUp:SetProperty("Text", tostring(_Pet_GetAttrcolour(GetPetIntUp(_Pet_OtherIndex))))
  gUI.PetCheck.dlabScore:SetProperty("Text", tostring((PetGetScore(_Pet_OtherIndex))))
  gUI.PetCheck.pbarPetFriendly:SetProperty("Progress", tostring(GetPetFriendly(_Pet_OtherIndex) / _Pet_FriendlyMaxValue))
  gUI.PetCheck.dlabPetFirendlyUp:SetProperty("Text", tostring(GetPetFriendly(_Pet_OtherIndex)) .. "/" .. _Pet_FriendlyMaxValue)
  local state = 0
  if level >= _Pet_State1NeedLevel and level < _Pet_State2NeedLevel then
    state = 0
  elseif level >= _Pet_State2NeedLevel and level < _Pet_State3NeedLevel then
    state = 1
  elseif level >= _Pet_State3NeedLevel then
    if not GetIsPetEquipFull(_Pet_OtherIndex) then
      state = 2
    else
      state = 3
    end
  end
  gUI.PetCheck.showbox:clearAvatar(_Pet_OtherIndex .. tostring(state) .. "Normal")
  if not gUI.PetCheck.showbox:isAvatarExist(_Pet_OtherIndex .. tostring(state) .. "Normal") then
    gUI.PetCheck.showbox:createPetAvatar(_Pet_OtherIndex, state, "Normal")
  end
  gUI.PetCheck.showbox:setCurrentAvatar(_Pet_OtherIndex .. tostring(state) .. "Normal")
  for equipPos = 0, _Pet_EquipCntMax do
    local equipIcon = GetPetEquipIcon(_Pet_OtherIndex, equipPos)
    local equipId = equipPos
    if equipIcon ~= nil then
      if string.len(equipIcon) == 0 then
        gUI.PetCheck.gboxEquip:ClearGoodsItem(equipPos)
      else
        gUI.PetCheck.gboxEquip:SetItemGoods(equipPos, equipIcon, -1)
        gUI.PetCheck.gboxEquip:SetItemData(equipPos, equipId)
      end
    else
      gUI.PetCheck.gboxEquip:ClearGoodsItem(equipPos)
    end
  end
  local gb = gUI.PetCheck.gboxSkill
  for skillPos = 0, _Pet_SkillCntMax do
    gb:ClearGoodsItem(skillPos)
    gb:SetItemData(skillPos, 0)
  end
  for skillPos = 0, _Pet_SkillCntMax do
    local skillId = GetPetSkillId(_Pet_OtherIndex, skillPos)
    local skillIcon = GetPetSkillIcon(_Pet_OtherIndex, skillPos)
    if skillIcon ~= nil then
      if string.len(skillIcon) == 0 then
        gb:ClearGoodsItem(skillPos)
      else
        gb:SetItemGoods(skillPos, skillIcon, -1)
        gb:SetItemData(skillPos, skillId)
      end
    else
      gb:ClearGoodsItem(skillPos)
      gb:SetItemData(skillPos, 0)
    end
  end
  local slotCnt = GetPetSkillSlotCnt(_Pet_OtherIndex)
  for skillPos = slotCnt, _Pet_SkillCntMax do
    gb:SetItemGoods(skillPos, _Pet_SkillLockIamge, -1)
  end
end
function _Pet_OtherStartRendering()
  gUI.PetCheck.showbox:show()
end
function _Pet_OtherEndRendering()
  gUI.PetCheck.showbox:hide()
end
function _Pet_UpdatePetFightInfo()
  for index = 0, _Pet_CanCarryMaxCnt - 1 do
    if not PetIsInFight(index) then
    else
      gUI.Pet.AllPetgb:SetSelectedItem(index)
      _Pet_CurrentPetIndex = index
    end
  end
end
function Lua_Pet_GetCurrentPetIndex()
  for i = 0, _Pet_CanCarryMaxCnt - 1 do
    if PetIsInFight(i) then
      return i
    end
  end
  return -1
end
function Lua_Pet_ShowReplaceWnd(skillId, bag, from_off)
  _Pet_ShowSkillReplace(skillId, bag, from_off)
end
function Lua_Pet_SetAutoFeedItem(bag, from_off)
  local num, id, quality, name, IdTable, class, icon = GetItemBaseInfoBySlot(bag, from_off)
  local _, _, _, _, skillId, _, _, _ = GetItemUsingInfo(bag, from_off)
  local gb = gUI.Pet.gboxPetFeed
  if skillId == nil or skillId ~= 55 then
    return
  end
  local cnt = GetItemCountInBag(IdTable)
  gb:SetItemGoods(0, icon, -1)
  gb:SetItemData(0, IdTable)
  gb:SetItemSubscript(0, tostring(cnt))
  TriggerUserGuide(30, 345000, 0)
end
function Lua_Pet_Calculate1(param1, param2)
  local _, level = GetFaerieBase()
  local value = math.floor(level * 10 * param1 / 100 + param2)
  return value
end
function Lua_Pet_Calculate2(param1, param2)
  local level = 0
  if _Pet_CurTipsType == 0 then
    level = GetPetLevel(_Pet_CurrentPetIndex)
  elseif _Pet_CurTipsType == 1 then
    level = GetPetLevel(_Pet_BlendMainSelectedIndex)
  elseif _Pet_CurTipsType == 2 then
    level = GetPetLevel(_Pet_BlendSecondSelectedIndex)
  elseif _Pet_CurTipsType == 3 then
    level = GetPetLevel(_Pet_OtherIndex)
  end
  local value = math.floor((level + 5 + (level + 5) * (level + 5) / 30) * param1 / 100 + param2)
  return value
end
function Lua_Pet_BlendOk()
  PetBlendOk()
  gUI.PetBlend.wndRoot:SetVisible(false)
  _Pet_BlendEndRendering()
  _Pet_BlendMainSelectedIndex = -1
  _Pet_BlendSecondSelectedIndex = -1
  Lua_Pet_ClosePetBlend()
end
function Lua_Pet_ShowPetBlend()
  gUI.PetBlend.wndRoot:SetVisible(true)
  _Pet_UpdateBlend()
  _Pet_BlendStartRendering()
  gUI.PetBlend.picPetBlend3:SetVisible(false)
  _Pet_ClearBlendMain()
  _Pet_ClearBlendSecond()
  _Pet_ClearBlend3()
  gUI.Pet.gboxPetBlend1:ResetAllGoods(true)
  gUI.Pet.gboxPetBlend2:ResetAllGoods(true)
end
function Lua_Pet_ClosePetBlend()
  gUI.PetBlend.wndRoot:SetVisible(false)
  _Pet_BlendEndRendering()
  _Pet_BlendMainSelectedIndex = -1
  _Pet_BlendSecondSelectedIndex = -1
  gUI.Pet.dlabBlendPetExp1:SetProperty("Progress", "0")
  gUI.Pet.dlabBlendPetExp2:SetProperty("Progress", "0")
  PetBlendCancel()
end
function _OnPet_AddNewPet(index)
  local name = GetPetName(index)
  local level = GetPetLevel(index)
  local item = gUI.Pet.lboxPet:InsertColoredString(name .. "|" .. level .. "级", -1, 4278255360)
  item:SetUserData(index)
  gUI.Pet.lboxPet:SetProperty("TextHorzAlign", "Left")
  gUI.Pet.lboxPet:SetProperty("TextHorzAlign", "HCenter")
  local icon = GetPetIcon(index)
  if icon ~= nil then
    gUI.Pet.AllPetgb:SetItemGoods(index, icon, -1)
  else
    gUI.Pet.AllPetgb:ClearGoodsItem(index)
    gUI.Pet.AllPetgb:SetItemData(index, -1)
  end
  gUI.Pet.AllPetgb:SetSelectedItem(index)
  _Pet_CurrentPetIndex = index
  gUI.Pet.lboxPet:ClearAllSel()
  item:SetSelected(true)
  _Pet_Update()
end
function _OnPet_CallUp(index)
  _Pet_Update()
  if GetItemCountInBag(345000) > 0 and not IsUserGuideStepIsComplete(15) then
    TriggerUserGuide(29, -1, -1)
  end
end
function _OnPet_ReCall(index)
  _Pet_Update()
end
function _OnPet_Free(index)
  local list = gUI.Pet.lboxPet
  list:RemoveItemByData(index)
  gUI.Pet.AllPetgb:ResetAllGoods(true)
  for nCount = _Pet_CanCarryMaxCnt - 1, 0, -1 do
    local icon = GetPetIcon(nCount)
    if icon ~= nil then
      if not PetIsInFight(nCount) then
        gUI.Pet.AllPetgb:SetItemGoods(nCount, icon, -1)
      else
        gUI.Pet.AllPetgb:SetItemGoods(nCount, icon, 20)
      end
      _Pet_CurrentPetIndex = nCount
      gUI.Pet.AllPetgb:SetSelectedItem(_Pet_CurrentPetIndex)
    end
  end
  if list:GetItemCount() == 0 then
    _Pet_CurrentPetIndex = -1
  end
  _Pet_Update()
end
function _OnPet_UpdateAttr(index)
  if index == _Pet_CurrentPetIndex then
    _Pet_UpdateById(index)
  end
end
function _OnPet_UpdateAutoFeed()
  local set, happy, itemIndex = PetGetAutoFeed()
  local _, _, _, _, _, _, icon = GetItemBaseInfoBySlot(-1, itemIndex)
  local cnt = GetItemCountInBag(itemIndex)
  gUI.Pet.cbtnPetFeed:SetChecked(set)
  gUI.Pet.eboxPetFeedNum:SetProperty("Text", tostring(happy))
  if icon then
    gUI.Pet.gboxPetFeed:SetItemGoods(0, icon, -1)
    gUI.Pet.gboxPetFeed:SetItemData(0, itemIndex)
    gUI.Pet.gboxPetFeed:SetItemSubscript(0, tostring(cnt))
  else
    gUI.Pet.gboxPetFeed:ClearGoodsItem(0)
    gUI.Pet.gboxPetFeed:SetItemSubscript(0, tostring(cnt))
  end
  _Pet_AutoFeedChecked = set
  _Pet_AutoFeedNum = happy
  _Pet_AutoFeedItemIndex = itemIndex
end
function _OnPet_OtherFeedPet(guid, playerName, playerLev, menpai)
  local info_f = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
  if info_f == nil then
    TriggerUserGuide(USER_GUIDE_PLAYER_PETFEED, 1, -1, guid)
  else
    TriggerUserGuide(USER_GUIDE_PLAYER_PETFEED, 2, -1, guid)
  end
  _Pet_Guide_PetFeedList[guid] = {
    Name = playerName,
    Lev = playerLev,
    Job = menpai
  }
end
function _OnPet_RankUpadte(index)
  if index == _Pet_CurrentPetIndex then
    local nRank = GetPetRank()
    if nRank == -1 then
      gUI.Pet.dlabRank:SetProperty("Text", "未上榜")
    else
      gUI.Pet.dlabRank:SetProperty("Text", tostring(nRank))
    end
  end
end
function UI_Pet_Show()
  if not gUI.Pet.wndRoot:IsVisible() then
    UI_Pet_Open()
  else
    UI_Pet_Close()
  end
  WorldStage:playSoundByID(34)
end
function UI_Pet_Open()
  gUI.Pet.wndRoot:SetVisible(true)
  _Pet_Update()
  _Pet_StartRendering()
  _OnPet_UpdateAutoFeed()
end
function UI_Pet_Rename()
  local edit = gUI.Pet.eboxPetName
  if not edit:IsKeyboardCaptureWindow() then
    _Pet_TempName = edit:GetProperty("Text")
    edit:SetEnable(true)
    WindowSys_Instance:SetKeyboardCaptureWindow(edit)
    WindowSys_Instance:SetFocusWindow(edit)
  else
    if string.len(edit:GetProperty("Text")) == 0 then
      ShowErrorMessage("宠物名称不能为空")
      edit:SetProperty("Text", _Pet_TempName)
      return
    elseif string.len(edit:GetProperty("Text")) > 12 then
      ShowErrorMessage("宠物名称不能超过6个字")
      return
    end
    PetRename(_Pet_CurrentPetIndex, edit:GetProperty("Text"))
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
    WindowSys_Instance:SetFocusWindow(WindowSys_Instance:GetRootWindow())
  end
end
function UI_Pet_RemovePet()
  Messagebox_Show("REMOVE_PET", _Pet_CurrentPetIndex, GetPetName(_Pet_CurrentPetIndex), GetPetLevel(_Pet_CurrentPetIndex))
end
function UI_Pet_Feed()
  if not gUI.Pet.wndRoot3:IsVisible() then
    gUI.Pet.wndRoot3:SetVisible(true)
    Lua_Bag_ShowUI(true)
    local set, happy, itemIndex = PetGetAutoFeed()
    local _, _, _, _, _, _, icon = GetItemBaseInfoBySlot(-1, itemIndex)
    local cnt = GetItemCountInBag(itemIndex)
    if not icon then
      return
    end
    gUI.Pet.gboxPetFeed:SetItemGoods(0, icon, -1)
    gUI.Pet.gboxPetFeed:SetItemData(0, itemIndex)
    gUI.Pet.gboxPetFeed:SetItemSubscript(0, tostring(cnt))
  else
    UI_Pet_CloseFeed()
    Lua_Bag_ShowUI(false)
  end
end
function UI_Pet_Battle()
  if not PetIsInFight(_Pet_CurrentPetIndex) then
    PetCallup(_Pet_CurrentPetIndex)
  else
    PetRelax(_Pet_CurrentPetIndex)
  end
end
function UI_Pet_Close()
  gUI.Pet.wndRoot:SetVisible(false)
  Messagebox_Hide("REMOVE_PET")
  local edit = gUI.Pet.eboxPetName
  if edit:IsKeyboardCaptureWindow() then
    edit:SetProperty("Text", _Pet_TempName)
  end
  edit:SetEnable(false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  WindowSys_Instance:SetFocusWindow(WindowSys_Instance:GetRootWindow())
  _Pet_EndRendering()
  UI_Pet_CloseFeed()
end
function UI_Pet_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Pet.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Pet_ShowSkillTips(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local skill_id = msg:get_lparam()
  local index = msg:get_wparam()
  local tooltip = WindowToTooltip(gb:GetToolTipWnd(0))
  if gb:GetAbsWindowName() == "Pet.Bottom_bg.Skill_gbox" then
    _Pet_CurTipsType = 0
  elseif gb:GetAbsWindowName() == "PetBlend.Pic_bg.Picture1_bg.GoodsBox_gbox" then
    _Pet_CurTipsType = 1
  elseif gb:GetAbsWindowName() == "PetBlend.Pic_bg.Picture2_bg.GoodsBox_gbox" then
    _Pet_CurTipsType = 2
  elseif gb:GetAbsWindowName() == "CheckPet.Picture2_bg.GoodsBox_gbox" then
    _Pet_CurTipsType = 3
  end
  if skill_id ~= 0 then
    Lua_Tip_Skill(tooltip, skill_id, 1, false, false)
  elseif _Pet_CurTipsType == 0 then
    local text = ""
    if gb:GetItemIcon(index) == _Pet_SkillLockIamge then
      text = "宠物技能栏可以通过宠物合成获得"
    else
      text = "宠物技能可以通过宠物技能书获得，宠物技能书在帮会商店可以买到（帮会商店等级越高可买的技能书越多）"
    end
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText(text, "FFFAFAFA", "", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function UI_Pet_EquipOn(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_win = WindowToGoodsBox(to_win:GetDragItemParent())
  local from_type = from_win:GetProperty("BoxType")
  local from_off, to_off
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, msg:get_wparam())
    UseItem(gUI_GOODSBOX_BAG.backpack0, from_off)
  end
end
function UI_Pet_EquipOff(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local from_type = gb:GetProperty("BoxType")
  local from_off = msg:get_lparam()
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_off = -1
  MoveItemPet(gUI_GOODSBOX_BAG.backpack0, to_off, from_bag, from_off, 0, -1, _Pet_CurrentPetIndex)
end
function UI_Pet_ShowEquipTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  local template_id = msg:get_lparam()
  if wnd:GetItemIcon(template_id) ~= "" then
    local bag = gUI_GOODSBOX_BAG[wnd:GetProperty("BoxType")]
    Lua_Tip_Item(tooltip, template_id, bag, nil, nil, nil)
  end
end
function UI_Pet_ShowOtherEquipTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  local template_id = msg:get_lparam()
  if wnd:GetItemIcon(template_id) ~= "" then
    local bag = gUI_GOODSBOX_BAG[wnd:GetProperty("BoxType")]
    Lua_Tip_Item(tooltip, template_id, bag, nil, 10, nil)
  end
end
function UI_Pet_ShowPetTips(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local index = msg:get_wparam()
  local tooltip = WindowToTooltip(gb:GetToolTipWnd(0))
  Lua_Tip_ShowPetTip(tooltip, index)
end
function UI_Pet_ShowDescTip(msg)
  local wnd = msg:get_window()
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  local wndUserdata = tonumber(wnd:GetProperty("CustomUserData"))
  local desc = ""
  if wndUserdata == 1 then
    desc = "影响宠物升级时力量的增加值，成长越高增加的值越高"
  elseif wndUserdata == 2 then
    desc = "影响宠物升级时敏捷的增加值，成长越高增加的值越高"
  elseif wndUserdata == 3 then
    desc = "影响宠物升级时智力的增加值，成长越高增加的值越高"
  elseif wndUserdata == 4 then
    desc = "影响宠物升级时体质的增加值，成长越高增加的值越高"
  end
  if desc ~= "" then
    Lua_Tip_Property(tooltip, desc)
  end
end
function UI_Pet_ShowAutoFeedTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = wnd:GetToolTipWnd(0)
  local template_id = msg:get_lparam()
  local bag = gUI_GOODSBOX_BAG[wnd:GetProperty("BoxType")]
  Lua_Tip_Item(tooltip, nil, nil, nil, nil, template_id)
end
function UI_PetPlacedIn(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local to_type = to_win:GetProperty("BoxType")
  local from_off = msg:get_wparam()
  local from_win = WindowToGoodsBox(to_win:GetDragItemParent())
  local from_type = from_win:GetProperty("BoxType")
  local to_off = msg:get_lparam()
  local form_typeIndex = from_win:GetProperty("GoodBoxIndex")
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_bag = gUI_GOODSBOX_BAG[to_type]
  if form_typeIndex == "GB_ItemMainBag" then
    local _, bindtype, _, id, skillId, skillParam, isPetItem, strPrompt = GetItemUsingInfo(gUI_GOODSBOX_BAG.backpack0, from_off)
    local _, _, _, name, _, _, _, _ = GetItemBaseInfoBySlot(gUI_GOODSBOX_BAG.backpack0, from_off)
    if skillId ~= nil and skillId == 66 then
      Messagebox_Show("USE_PET_EGG", name, gUI_GOODSBOX_BAG.backpack0, from_off)
    elseif isPetItem and bindtype == 0 then
      Messagebox_Show("USE_PET_ITEM", name, gUI_GOODSBOX_BAG.backpack0, from_off)
    end
  end
end
function UI_Pet_CloseSkillReplace()
  gUI.Pet.wndRoot2:SetVisible(false)
end
function UI_Pet_SkillReplaceOk()
  UseItemPetSkill(_Pet_NewSkillBag, _Pet_NewSkillFromOff, _Pet_NewSkillSlotToReplace)
  gUI.Pet.wndRoot2:SetVisible(false)
end
function UI_Pet_SelectSkill(msg)
  local gb = WindowToGoodsBox(msg:get_window())
  local from_type = gb:GetProperty("BoxType")
  local slot = msg:get_lparam()
  gUI.Pet.btnPetSkillReplace:SetEnable(true)
  _Pet_NewSkillSlotToReplace = slot
end
function UI_Pet_CloseFeed()
  gUI.Pet.wndRoot3:SetVisible(false)
  local _, _, _, _, _, _, icon = GetItemBaseInfoBySlot(-1, _Pet_AutoFeedItemIndex)
  gUI.Pet.cbtnPetFeed:SetChecked(_Pet_AutoFeedChecked)
  gUI.Pet.eboxPetFeedNum:SetProperty("Text", tostring(_Pet_AutoFeedNum))
  if icon then
    gUI.Pet.gboxPetFeed:SetItemGoods(0, icon, -1)
    gUI.Pet.gboxPetFeed:SetItemData(0, _Pet_AutoFeedItemIndex)
  else
    gUI.Pet.gboxPetFeed:ClearGoodsItem(0)
    gUI.Pet.gboxPetFeed:SetItemData(0, 0)
  end
end
function UI_Pet_AutoFeed()
  _Pet_AutoFeedChecked = gUI.Pet.cbtnPetFeed:IsChecked()
  _Pet_AutoFeedNum = tonumber(gUI.Pet.eboxPetFeedNum:GetProperty("Text"))
  _Pet_AutoFeedItemIndex = gUI.Pet.gboxPetFeed:GetItemData(0)
  PetSetAutoFeed(_Pet_AutoFeedChecked, _Pet_AutoFeedNum, _Pet_AutoFeedItemIndex)
  gUI.Pet.wndRoot3:SetVisible(false)
end
function UI_Pet_AutoFeedItem(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_win = to_win:GetDragItemParent()
  local from_type = from_win:GetProperty("BoxType")
  local from_off, to_off
  local to_type = to_win:GetProperty("BoxType")
  local from_bag = gUI_GOODSBOX_BAG[from_type]
  local to_bag = gUI_GOODSBOX_BAG[to_type]
  if from_type == "backpack0" then
    from_off = Lua_Bag_GetRealSlot(from_win, msg:get_wparam())
    Lua_Pet_SetAutoFeedItem(from_bag, from_off)
  end
end
function UI_Pet_ShowPetBlend()
  Lua_Pet_ShowPetBlend()
end
function UI_Pet_ClosePetBlend()
  Lua_Pet_ClosePetBlend()
end
function UI_PetBlend_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("PetBlend.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Pet_PetBlendOk()
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  if _Pet_BlendMainSelectedIndex == -1 then
    Lua_Chat_ShowOSD("PET_CANNOT_BLEND_NOT_MAIN")
    return
  elseif _Pet_BlendSecondSelectedIndex == -1 then
    Lua_Chat_ShowOSD("PET_CANNOT_BLEND_NOT_SECOND")
    return
  end
  Messagebox_Show("PETBLEND")
end
function UI_Pet_SelectMainPet(msg)
  local petIndex = msg:get_lparam()
  _Pet_SelectIndex = petIndex
  if _Pet_HostIndex ~= -1 then
    gUI.PetBlend.cboxPetBlendMain:SetEditBoxText(GetPetName(_Pet_HostIndex) .. " " .. tostring(GetPetLevel(_Pet_HostIndex)) .. "级")
  else
    gUI.PetBlend.cboxPetBlendMain:SetEditBoxText("请选择主宠物")
  end
  if _Pet_IsInFighting() then
    return
  end
  _Pet_BlendMainSelectedIndex = petIndex
  _Pet_UpdateBlendMainById(petIndex)
  PetSelectBlendPet(petIndex, 0)
  _Pet_UpdateBlendResult()
end
function UI_Pet_SelectSecondPet(msg)
  local petIndex = msg:get_lparam()
  _Pet_SelectIndex = petIndex
  if _Pet_SubIndex ~= -1 then
    gUI.PetBlend.cboxPetBlendSecond:SetEditBoxText(GetPetName(_Pet_SubIndex) .. " " .. tostring(GetPetLevel(_Pet_SubIndex)) .. "级")
  else
    gUI.PetBlend.cboxPetBlendSecond:SetEditBoxText("请选择副宠物")
  end
  if _Pet_IsInFighting() then
    return
  end
  if _Pet_HasEquip() then
    return
  end
  _Pet_BlendSecondSelectedIndex = petIndex
  _Pet_UpdateBlendSecondById(petIndex)
  PetSelectBlendPet(petIndex, 1)
  _Pet_UpdateBlendResult()
end
function _Pet_ResetItemLists()
  local comboMain = gUI.PetBlend.cboxPetBlendMain
  local comboSub = gUI.PetBlend.cboxPetBlendSecond
  local ctrlIndex = 0
  comboMain:RemoveAllItems()
  comboSub:RemoveAllItems()
  if _Pet_HostIndex ~= -1 then
    comboMain:SetEditBoxText(GetPetName(_Pet_HostIndex) .. " " .. tostring(GetPetLevel(_Pet_HostIndex)) .. "级")
  else
    comboMain:SetEditBoxText("请选择主宠物")
  end
  if _Pet_SubIndex ~= -1 then
    comboSub:SetEditBoxText(GetPetName(_Pet_SubIndex) .. " " .. tostring(GetPetLevel(_Pet_SubIndex)) .. "级")
  else
    comboSub:SetEditBoxText("请选择副宠物")
  end
  for i = 0, gUI.Pet.lboxPet:GetItemCount() - 1 do
    local petIndex = gUI.Pet.lboxPet:GetItemData(i)
    if _Pet_HostIndex ~= petIndex and _Pet_SubIndex ~= petIndex then
      comboMain:InsertString(GetPetName(petIndex) .. " " .. tostring(GetPetLevel(petIndex)) .. "级", 4294967295)
      comboSub:InsertString(GetPetName(petIndex) .. " " .. tostring(GetPetLevel(petIndex)) .. "级", 4294967295)
      comboMain:SetItemData(ctrlIndex, petIndex)
      comboSub:SetItemData(ctrlIndex, petIndex)
      ctrlIndex = ctrlIndex + 1
    end
  end
end
function _Pet_IsInFighting()
  if PetIsInFight(_Pet_SelectIndex) then
    Lua_Chat_ShowOSD("PET_INFIGHT_CANNOT_BLEND")
    return true
  end
end
function _Pet_HasEquip()
  if GetIsPetHasEquip(_Pet_SelectIndex) then
    Lua_Chat_ShowOSD("PET_HASEQUIP_CANNOT_BLEND")
    return true
  end
end
function _OnPet_CancelInterface()
  _Pet_HostIndex = -1
  _Pet_SubIndex = -1
  _Pet_SelectIndex = -1
end
function _OnPet_AddPet(slot)
  if tostring(slot) == "0" then
    _Pet_HostIndex = _Pet_SelectIndex
    gUI.PetBlend.cboxPetBlendMain:SetEditBoxText(GetPetName(_Pet_HostIndex) .. " " .. tostring(GetPetLevel(_Pet_HostIndex)) .. "级")
  elseif tostring(slot) == "1" then
    _Pet_SubIndex = _Pet_SelectIndex
    gUI.PetBlend.cboxPetBlendSecond:SetEditBoxText(GetPetName(_Pet_SubIndex) .. " " .. tostring(GetPetLevel(_Pet_SubIndex)) .. "级")
  end
  _Pet_ResetItemLists()
end
function UI_Pet_OpenOther(ownerName)
  gUI.PetCheck.wndRoot:SetVisible(true)
  _Pet_OtherStartRendering()
  _Pet_UpdateOther(ownerName)
end
function UI_Pet_CloseOther()
  gUI.PetCheck.wndRoot:SetVisible(false)
  _Pet_OtherEndRendering()
end
function UI_Pet_ModelLeftRotate()
  AddTimerEvent(2, _Pet_Avatarotate, function()
    gUI.Pet.showbox:onCharacterLeft()
  end)
end
function UI_Pet_ModelRightRotate()
  AddTimerEvent(2, _Pet_Avatarotate, function()
    gUI.Pet.showbox:onCharacterRight()
  end)
end
function UI_Pet_ModelStopRotate()
  DelTimerEvent(2, _Pet_Avatarotate)
end
function UI_Pet_BlendModelLeftRotate()
  AddTimerEvent(2, _Pet_Avatarotate_Plend, function()
    gUI.PetBlend.showbox3:onCharacterLeft()
  end)
end
function UI_Pet_BlendModelRightRotate()
  AddTimerEvent(2, _Pet_Avatarotate_Plend, function()
    gUI.PetBlend.showbox3:onCharacterRight()
  end)
end
function UI_Pet_BlendModelStopRotate()
  DelTimerEvent(2, _Pet_Avatarotate_Plend)
end
function UI_Pet_PetLClick(msg)
end
function UI_Pet_FeedNumChanged(msg)
  local feednum = gUI.Pet.eboxPetFeedNum:GetProperty("Text")
  if feednum == "" then
    gUI.Pet.eboxPetFeedNum:SetProperty("Text", "0")
  end
end
function Guide_OnPetFeedShow(msg)
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = GoodsBox:GetItemData(0)
  local guid = GoodsBox:GetUserData64()
  local strText, strBtnText, luaEvent, guideWindowName = GetUserGuideInfo(index)
  local rootwnd = WindowSys_Instance:GetWindow(guideWindowName)
  rootwnd:SetUserData64(guid)
  for key, value in pairs(_Pet_Guide_PetFeedList) do
    if key == guid then
      strText = string.format(strText, value.Name, value.Lev, gUI_MenPaiName[value.Job])
    end
  end
  local displayBox = WindowToDisplayBox(rootwnd:GetChildByName("DisplayBox_dbox"))
  if displayBox then
    displayBox:ClearAllContent()
    displayBox:InsertBack(strText, 4294967295, 0, 0)
  end
  local btn = rootwnd:GetChildByName("Button3_btn")
  if btn then
    btn:SetProperty("Text", strBtnText)
    btn:AddScriptEvent("wm_mouseclick", luaEvent)
    btn:SetCustomUserData(0, index)
  end
  btn = rootwnd:GetChildByName("Close_btn")
  if btn then
    btn:SetVisible(true)
    btn:AddScriptEvent("wm_mouseclick", "UI_Pet_Guide_OnClose")
  end
  local titleWnd = rootwnd:GetChildByName("Label1_dlab")
  titleWnd:SetProperty("Text", "宠物喂养提示")
  rootwnd:SetVisible(true)
end
function Guide_OnAddFriend(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  AddRelation(guid)
  DeleteHint(USER_GUIDE_PETFEED_UI, guid)
  rootwnd:CloseGuide()
end
function UI_Pet_Guide_OnClose(msg)
  local rootwnd = msg:get_window():GetParent()
  local guid = rootwnd:GetUserData64()
  DeleteHint(USER_GUIDE_PETFEED_UI, guid)
  DeleteHint(USER_GUIDE_PETFEED_UI_FRIEND, guid)
  rootwnd:CloseGuide()
end
function Script_Pet_OnLoad()
  _Pet_InitPetCtrl()
  gUI.Pet.lboxPet:RemoveAllItems()
  gUI.Pet.AllPetgb:ResetAllGoods(true)
  local edit = gUI.Pet.eboxPetName
  edit:SetEnable(false)
  _Pet_CurrentPetIndex = -1
  _Pet_Guide_PetFeedList = {}
end
function Script_Pet_OnEvent(event)
  if event == "PARTNER_GET_NEW_ONE" then
    _OnPet_AddNewPet(arg1)
  elseif event == "PARTNER_CALLUP" then
    _OnPet_CallUp(arg1)
  elseif event == "PARTNER_RECALL" then
    _OnPet_ReCall(arg1)
  elseif event == "PARTNER_FREE" then
    _OnPet_Free(arg1)
  elseif event == "PARTNER_UPDATE" then
    _OnPet_UpdateAttr(arg1)
  elseif event == "PARTNER_AUTO_FEED" then
  elseif event == "PARTNER_BLEND_SHOWWINDOW" then
    Lua_Pet_ShowPetBlend()
  elseif event == "PARTNER_VIEW_TARGET" then
    UI_Pet_OpenOther(arg1)
  elseif event == "PARTNER_PET_FEED" then
    _OnPet_OtherFeedPet(arg1, arg2, arg3, arg4)
  elseif event == "PARTNER_PET_ADDPET" then
    _OnPet_AddPet(arg1)
  elseif event == "PARINER_PET_CANCELPET" then
    _OnPet_CancelInterface()
  elseif event == "PARINER_RANK" then
    _OnPet_RankUpadte(arg1)
  end
end
function Script_Pet_OnHide()
end
function Script_PetBlend_OnLoad()
end
function Script_PetBlend_OnEvent(event)
  if event == "CLOSE_NPC_RELATIVE_DIALOG" then
    Lua_Pet_ClosePetBlend()
  end
end
function Script_PetBlend_OnHide()
  PetBlendCancel()
end
