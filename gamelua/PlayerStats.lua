local _Plays_TargetObjID = -1
local _Plays_PKSTR = {
  "和平",
  "善恶",
  "帮会",
  "组队",
  "全体"
}
local spell_duration = 0
local next_Buff = 0
local target_level = 0
local debuff_num = 0
local _Plays_ViewPlayerGUID, _Plays_ViewPlayerName
local _Plays_ColorSJ = "UiIamge012:Image_SP"
local _Plays_ColorQS = "UiIamge012:Image_EP"
local _Plays_ColorNormal = "UiIamge012:Image_MP"
local _Plays_Bar
local _Plays_CurExp = 0
local _Plays_MaxExp = 1
local _Plays_CurTp = 0
function _Plays_LevelUp(level)
  local frame = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg")
  lab = frame:GetChildByName("Level_dlab")
  lab:SetProperty("Text", tostring(level))
  gUI_MainPlayerAttr.Level = level
  frame = WindowSys_Instance:GetWindow("TargetStats.Target_bg")
  lab = frame:GetGrandChild("Stats_bg.Level_dlab")
  if lab ~= nil then
    local text, color = _Plays_GetTargetLevelDegree(target_level)
    lab:SetProperty("FontColor", color)
    lab:SetProperty("Text", text)
  end
  Lua_PlayerStats_UpdateInfos()
end
function Lua_PlayerStats_UpdateInfos()
  if gUI.Refine.Root:IsVisible() then
    Lua_Refine_SetData()
  end
  if gUI.Amass.Root:IsVisible() then
    Lua_Amass_UpdateData()
  end
end
function _Plays_SelfChangedInfo()
  local maxhp, hp, maxmp, mp, maxrage, rage, strike_point, pk_value, name_color, _, tp, maxtp, yuanqi, maxyuanqi = GetSelfEaseChangeInfo()
  if maxhp then
    local progress = maxhp ~= 0 and hp / maxhp or 0
    local frame = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg")
    local frame2 = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg")
    local bar = frame:GetChildByName("HP_pbar")
    bar:SetProperty("Blink", tostring(hp < maxhp * 0.4))
    bar:SetProperty("Progress", tostring(progress))
    local lab = frame:GetChildByName("HP_dlab")
    lab:SetProperty("Text", tostring(hp) .. "/" .. tostring(maxhp))
    if UIConfig:getDeathBlink() then
      local agonal_wnd = WindowSys_Instance:GetWindow("EfxAgonal_pic")
      if hp < maxhp * 0.1 then
        agonal_wnd:SetProperty("Visible", "true")
        if hp <= 0 then
          agonal_wnd:SetProperty("Blink", "false")
        else
          agonal_wnd:SetProperty("Blink", "true")
        end
      else
        agonal_wnd:SetProperty("Visible", "false")
      end
    end
    if _Plays_Bar then
      progress = maxmp ~= 0 and mp / maxmp or 0
      _Plays_Bar:SetProperty("Progress", tostring(progress))
      lab = _Plays_Bar:GetChildByName("Bar_dLab")
      if lab then
        lab:SetProperty("Text", tostring(mp) .. "/" .. tostring(maxmp))
      end
    end
    for i = 1, 5 do
      lab = frame:GetGrandChild("SP_bg.SP" .. tostring(i))
      lab:SetProperty("Visible", tostring(strike_point >= i))
    end
    local pic = frame:GetGrandChild("SP_bg.Anim_pic")
    pic:SetProperty("Visible", tostring(5 == strike_point))
    progress = maxtp ~= 0 and tp / maxtp or 0
    progress = string.format("%f", progress)
    bar = frame2:GetChildByName("TP_pbar")
    bar:SetProperty("Visible", "true")
    bar:SetProperty("Progress", progress)
    _Plays_CurTp = tostring(tp) .. "/" .. tostring(maxtp)
  end
end
function _Plays_GetTargetLevelDegree(level)
  local text = tostring(level)
  local color
  local levelDiff = gUI_MainPlayerAttr.Level - level
  if levelDiff <= -10 then
    text = "??"
    color = "FFFF0611"
  elseif levelDiff <= -4 then
    color = "FFFF0611"
  elseif levelDiff <= 3 then
    color = "FFFFFF00"
  elseif levelDiff <= 9 then
    color = "FF00FF00"
  else
    color = "FFCACACA"
  end
  return text, color
end
function _Plays_UpdateBuffer(widget, aura_info, index_map, isDebuffer, isMine, notClear)
  local limit = widget:GetItemCount()
  local i = 0
  while limit > i do
    local icon, count, resttime, duration, _, bBlink = aura_info(i, isDebuffer, isMine)
    local index = index_map(i)
    if icon ~= nil then
      widget:SetItemGoods(index, icon, 2)
      if resttime > 0 and duration ~= 0 then
        widget:SetItemCoolTime(index, duration, resttime)
      else
        widget:SetItemCoolTime(index, 1, 0)
      end
      if not bBlink then
        widget:SetItemBlink(index, false)
      end
      if count > 1 then
        widget:SetItemSubscript(index, tostring(count))
      end
      widget:SetIconFadeWithWindow(false)
    else
      widget:ClearGoodsItem(index)
    end
    i = i + 1
  end
end
function _Plays_UpdateTargetBuffer(widget, aura_info, start, isDebuffer, isMine, needClear)
  local limit = widget:GetItemCount()
  local i = start
  local icon, count, resttime, duration
  while limit > i do
    icon, count, resttime, duration = aura_info(i, isDebuffer, isMine)
    if icon ~= nil then
      widget:SetItemGoods(next_Buff, icon, 2)
      if resttime > 0 and duration ~= 0 then
        widget:SetItemCoolTime(next_Buff, duration, resttime)
      end
      if count > 1 then
        widget:SetItemSubscript(next_Buff, tostring(count))
      end
      widget:SetIconFadeWithWindow(false)
      next_Buff = next_Buff + 1
    else
      widget:ClearGoodsItem(next_Buff)
    end
    i = i + 1
  end
end
function Lua_Plays_GetSelfAuraIndex(index)
  if index < 8 then
    index = 7 - index
  else
    index = 23 - index
  end
  return index
end
function Lua_Plays_ShowOtherBaseInfo(istarget, teammerber_name, target_guid, target_objid)
  local targetGuid = target_guid
  if istarget then
    local _, level, _, name, menpai, sexid, _, _, _, guildName = GetTargetBaseInfo()
    _OnPlays_ShowTargeInfo(target_guid, name, menpai, sexid, level, guildName)
  elseif teammerber_name ~= nil then
    local _, name, level, _, _, _, _, _, _, _, _, isoffline, menpai, _, sexid, _, guildName, mapId, _, _, _, _, minAtk, maxAtk, defence = GetTeamMemberInfo(teammerber_name, true)
    _OnPlays_ShowTargeInfo(target_guid, name, menpai, sexid, level, guildName)
  elseif target_guid ~= nil then
    RequestTargetInfoByGUID(target_guid)
  else
    local name, menpai, sexid, _, _, _, _, my_guid, my_objid = GetMyPlayerStaticInfo()
    local _, _, _, _, level = GetPlaySelfProp(4)
    local guildName = GetGuildName()
    targetGuid = my_guid
    _OnPlays_ShowTargeInfo(my_guid, name, menpai, sexid, level, guildName)
  end
  AskPlayerEquipMark(targetGuid)
end
function _OnPlays_ModeChange(index)
  for i = 1, table.maxn(_Plays_PKSTR) do
    local partname = "PlayerStats.PKStats_bg.PK" .. i .. "_btn"
    local wnd = WindowToButton(WindowSys_Instance:GetWindow(partname))
    if index + 1 ~= i then
      wnd:SetStatus("normal")
    else
      wnd:SetStatus("selected")
    end
  end
  if index >= 0 and index <= 4 then
    Lua_Chat_AddSysLog("当前PK模式为[" .. _Plays_PKSTR[index + 1] .. "模式]")
  end
  Lua_Rcm_SetPKMode(index)
end
function _OnPlays_TargetChanged(obj_id)
  if _Plays_TargetObjID ~= obj_id then
    _OnPlays_TargetBaseInfo(obj_id)
    _OnPlays_TargetChangedInfo(0, false)
    _OnPlays_TargetBuffUpdate()
    Lua_MenuHide("ON_TARGET")
    Lua_MenuHide("ON_NPC")
  end
end
function _OnPlays_TargetBaseInfo(obj_id)
  _Plays_TargetObjID = obj_id
  local target = WindowSys_Instance:GetWindow("TargetStats.Target_bg")
  local char_type, level, bHideName, name, menpai, gender, head_id, hair_model, hair_color, guildName = GetTargetBaseInfo(_Plays_TargetObjID)
  if char_type then
    target:SetProperty("Visible", "true")
    local frame = target:GetChildByName("Stats_bg")
    local lab = frame:GetChildByName("Name_dlab")
    lab:SetProperty("Text", name)
    lab:SetProperty("Visible", tostring(not bHideName))
    lab = frame:GetChildByName("Guild_dlab")
    if guildName == "" or guildName == nil then
      lab:SetProperty("Visible", "false")
    else
      lab:SetProperty("Visible", "true")
      lab:SetProperty("Text", "<" .. guildName .. ">")
    end
    lab = frame:GetChildByName("Level_dlab")
    target_level = level
    local text, color = _Plays_GetTargetLevelDegree(level)
    lab:SetProperty("FontColor", "ffff0000")
    lab:SetProperty("Text", text)
    local portrait = frame:GetChildByName("Portrait_pic")
    local menpaiImg = frame:GetChildByName("Menpai_pic")
    frame:SetProperty("BackImage", "UiIamge012:Image_Target")
    local menpaiPic = WindowSys_Instance:GetWindow("TargetStats.Target_bg.Stats_bg.Menpai_pic.Menpai_bg")
    if gUI_MenPaiName[menpai] then
      menpaiPic:SetProperty("TooltipText", gUI_MenPaiName[menpai])
    else
      menpaiPic:SetProperty("TooltipText", "--")
    end
    if char_type <= gUI_CharType.CHAR_PLAYER then
      local isInStall, ModelID = GetPlayerInfo(false, _Plays_TargetObjID)
      if not isInStall or not (ModelID >= -7) or ModelID <= -2 then
      else
      end
      menpaiImg:SetProperty("Visible", "true")
      menpaiImg:GetChildByName("Menpai_bg"):SetProperty("BackImage", gUI_PROFESSION_INTRODUCE[menpai].sign)
    else
      menpaiImg:SetProperty("Visible", "false")
      if gender == 0 then
        frame:SetProperty("BackImage", "UiIamge012:Image_Target")
      elseif gender == 1 or gender == 2 then
        frame:SetProperty("BackImage", "UiIamge012:Image_Ag")
      elseif gender == 3 or gender == 4 then
        frame:SetProperty("BackImage", "UiIamge012:Image_Gold")
      end
      portrait:SetProperty("Visible", "true")
    end
  else
    target:SetProperty("Visible", "false")
  end
end
function _OnPlays_TargetChangedInfo(bShowMode, bShowProgressChange)
  local target_type, _, _, _, menpai, _, _, showFourLayerHP = GetTargetBaseInfo()
  local hp, mp, guild, level = GetTargetEaseChangeInfo(0)
  local nameColor, guildColor, t_type, t_level, t_hp, t_name, t_head, _, t_menpai, _, _, _, t_objID = GetTargetEaseChangeInfo(1)
  local frame = WindowSys_Instance:GetWindow("TargetStats.Target_bg.Stats_bg")
  if hp and bShowMode >= 0 then
    local dlab = frame:GetChildByName("Level_dlab")
    local text, color = _Plays_GetTargetLevelDegree(level)
    target_level = level
    frame:GetChildByName("Name_dlab"):SetFontColor(nameColor)
    if guildColor ~= nil and frame:GetChildByName("Guild_dlab"):IsVisible() then
      frame:GetChildByName("Guild_dlab"):SetFontColor(guildColor)
    end
    dlab:SetProperty("FontColor", color)
    dlab:SetProperty("Text", text)
    dlab = frame:GetChildByName("HP_dlab")
    dlab:SetProperty("Text", tostring(hp) .. "%")
    local R_bar = frame:GetChildByName("HP_Red_pbar")
    local Y_bar = R_bar:GetChildByName("HP_Yellow_pbar")
    local G_bar = Y_bar:GetChildByName("HP_Green_pbar")
    local P_bar = G_bar:GetChildByName("HP_Purple_pbar")
    R_bar:SetProperty("ShowProgressChange", tostring(bShowProgressChange))
    Y_bar:SetProperty("ShowProgressChange", tostring(bShowProgressChange))
    G_bar:SetProperty("ShowProgressChange", tostring(bShowProgressChange))
    P_bar:SetProperty("ShowProgressChange", tostring(bShowProgressChange))
    if target_type == gUI_CharType.CHAR_NPC then
      if showFourLayerHP == 0 then
        showFourLayerHP = 1
      end
      local OneLayerMAXHP = 100 / showFourLayerHP
      if hp >= OneLayerMAXHP then
        R_bar:SetProperty("Progress", tostring(1))
        if hp >= OneLayerMAXHP * 2 then
          Y_bar:SetProperty("Progress", tostring(1))
          if hp >= OneLayerMAXHP * 3 then
            G_bar:SetProperty("Progress", tostring(1))
            P_bar:SetProperty("Progress", tostring(hp * 0.01 * showFourLayerHP - 3))
          else
            P_bar:SetProperty("Progress", "0")
            G_bar:SetProperty("Progress", tostring(hp * 0.01 * showFourLayerHP - 2))
          end
        else
          P_bar:SetProperty("Progress", "0")
          G_bar:SetProperty("Progress", "0")
          Y_bar:SetProperty("Progress", tostring(hp * 0.01 * showFourLayerHP - 1))
        end
      else
        Y_bar:SetProperty("Progress", "0")
        G_bar:SetProperty("Progress", "0")
        P_bar:SetProperty("Progress", "0")
        R_bar:SetProperty("Progress", tostring(hp * 0.01 * showFourLayerHP))
      end
    else
      R_bar:SetProperty("Progress", tostring(hp * 0.01))
      Y_bar:SetProperty("Progress", "0")
      G_bar:SetProperty("Progress", "0")
      P_bar:SetProperty("Progress", "0")
    end
  end
  local lab = frame:GetSibling("Target2")
  if t_type then
    lab:SetProperty("Visible", "true")
  else
    lab:SetProperty("Visible", "false")
  end
  if t_type and bShowMode <= 0 then
    if t_type <= 1 then
      local isInStall, modelID = GetPlayerInfo(false, t_objID)
      if isInStall and modelID >= -7 and modelID <= -2 then
        local p = lab:GetChildByName("Portrait_pic")
      else
        local p = lab:GetChildByName("Portrait_pic")
        if not (t_menpai >= 0) or not (t_menpai <= 4) or not (t_head >= 0) or t_head <= 1 then
        else
        end
      end
    else
      local p = lab:GetChildByName("Portrait_pic")
    end
    p = lab:GetChildByName("Name_dlab")
    p:SetProperty("Text", t_name)
    p = lab:GetChildByName("Level_dlab")
    local text, color = _Plays_GetTargetLevelDegree(t_level)
    p:SetProperty("Text", text)
    p:SetProperty("FontColor", color)
    p = lab:GetChildByName("HP_pbar")
    p:SetProperty("Progress", tostring(t_hp * 0.01))
  end
end
function _OnPlays_TargetFocusChangedInfo(objectID)
  local _, _, _, _name = GetTargetBaseInfo(objectID)
  local hp, mp, guild, level = GetTargetEaseChangeInfo(0, objectID)
  local nameColor = GetTargetEaseChangeInfo(1, objectID)
  local frame = WindowSys_Instance:GetWindow("TargetStats.TargetFocus_bg.Focus_bg")
  if hp ~= nil then
    local dlabName = frame:GetChildByName("Focus_dlab")
    dlabName:SetProperty("Text", _name)
    dlabName:SetFontColor(nameColor)
    local dlabLev = frame:GetChildByName("Lev_dlab")
    local text, color = _Plays_GetTargetLevelDegree(level)
    dlabLev:SetProperty("FontColor", color)
    dlabLev:SetProperty("Text", text)
    local barhp = frame:GetChildByName("Focus_pbar")
    barhp:SetProperty("Progress", tostring(hp * 0.01))
  end
end
function _OnPlays_SelfBuffUpdate()
  local win = WindowToGoodsBox(WindowSys_Instance:GetWindow("BufferBar.buffframe"))
  _Plays_UpdateBuffer(win, GetPlayerAuraInfo, Lua_Plays_GetSelfAuraIndex, false)
  win = WindowToGoodsBox(WindowSys_Instance:GetWindow("BufferBar.debuffframe"))
  _Plays_UpdateBuffer(win, GetPlayerAuraInfo, Lua_Plays_GetSelfAuraIndex, true)
end
function _OnPlays_TargetBuffUpdate()
  local target = WindowSys_Instance:GetWindow("TargetStats.Target_bg")
  local widget = WindowToGoodsBox(target:GetChildByName("Debuff_gbox"))
  local frontWnd = WindowToGoodsBox(target:GetChildByName("Mydebuff_gbox"))
  widget:ResetAllGoods(true)
  frontWnd:ResetAllGoods(true)
  if target:IsVisible() then
    next_Buff = 0
    debuff_num = 0
    _Plays_UpdateTargetBuffer(frontWnd, GetTargetAuraInfo, next_Buff, true, 1, false)
    _Plays_UpdateTargetBuffer(widget, GetTargetAuraInfo, next_Buff, true, -1, false)
    debuff_num = next_Buff
    _Plays_UpdateTargetBuffer(widget, GetTargetAuraInfo, 0, false, -1, true)
  end
end
function _OnPlays_RouterStateChange(is_show, map_id, npc_id, pos_x, pos_y)
  local button = WindowSys_Instance:GetWindow("RouterMaker_btn")
  button:SetProperty("Visible", tostring(is_show))
  if is_show then
    local map_name = "未知地点"
    if map_id ~= -1 then
      map_name = GetMapInfo(map_id)
      if npc_id ~= -1 then
        pos_x, pos_y = GetNpcCoordinatesById(npc_id, map_id)
      end
    end
    local name = ""
    if pos_x and pos_y then
      name = "正在前往 {#G^" .. map_name .. " " .. math.ceil(pos_x) .. "," .. math.ceil(pos_y) .. "}"
    else
      name = "正在前往 {#G^" .. map_name .. " 未知目标" .. "}"
    end
    button:SetProperty("Text", name)
  end
end
function _OnPlays_FollowStateChange(is_follow, objid_follow)
  local button = WindowSys_Instance:GetWindow("Follow_btn")
  button:SetProperty("Visible", tostring(is_follow))
  if is_follow then
    local _, _, _, name = GetTargetBaseInfo(objid_follow)
    name = "正在跟随{#G^" .. name .. "}"
    button:SetProperty("Text", name)
  end
end
function _OnPlays_ShowTargeInfo(guid, name, menpai, sexid, level, guildName)
  _Plays_ViewPlayerGUID = guid
  _Plays_ViewPlayerName = name
  local playerName = WindowSys_Instance:GetWindow("Detail.PlayerInformation.Information_bg.PlayerName_dlab")
  local levName = WindowSys_Instance:GetWindow("Detail.PlayerInformation.Information_bg.Level_dlab")
  local jobName = WindowSys_Instance:GetWindow("Detail.PlayerInformation.Information_bg.Menpai_dlab")
  local guildN = WindowSys_Instance:GetWindow("Detail.PlayerInformation.Information_bg.GuildName_dlab")
  playerName:SetProperty("Text", tostring(name))
  levName:SetProperty("Text", tostring(level))
  jobName:SetProperty("Text", tostring(gUI_MenPaiName[menpai]))
  if guildName == nil or guildName == "" then
    guildName = "未加入帮会"
  end
  guildN:SetProperty("Text", tostring(guildName))
  Lua_Detail_ShowChildWnd("PlayerInformation", true)
end
function _OnPlays_ShowTargeInfoEquipMark(equipMarks)
  local equipMark = WindowSys_Instance:GetWindow("Detail.PlayerInformation.Information_bg.Grade_dlab")
  equipMark:SetProperty("Text", tostring(equipMarks))
end
function UI_Plays_OnTarget_TargetBtnClick(msg)
  SetUITarget(-2)
end
function UI_Plays_SelfboxLClick(msg)
end
function UI_Plays_OnTargetIconShowBtnClick(msg)
  RequestTargetEquipList(-1)
end
function UI_Plays_BufferTooltip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  if tooltip then
    local box_type = wnd:GetProperty("BoxType")
    local index = msg:get_wparam()
    local isDebuffer = true
    if box_type == "buffer1" or box_type == "buffer9" then
      isDebuffer = false
    elseif box_type == "debuffer1" or box_type == "debuffer9" then
      isDebuffer = true
    end
    if box_type == "buffer1" or box_type == "debuffer1" then
      index = Lua_Plays_GetSelfAuraIndex(index)
    end
    local name, duration, remaintime, des
    if gUI_AURABOX_INDEXES[box_type] == 1 and index >= debuff_num then
      index = index - debuff_num
      isDebuffer = false
    end
    name, duration, remaintime, des, caster = GetAuraTipInfo(index, isDebuffer, gUI_AURABOX_INDEXES[box_type])
    if name then
      Lua_Tip_Begin(tooltip, nil, "false")
      local count = tonumber(wnd:GetItemSubscript(msg:get_wparam())) or 1
      tooltip:InsertLeftText(name, "1E90FF", "kaiti_12", 0, 0)
      if remaintime > 0 and duration ~= 0 then
        local resttime = string.format("剩余时间:$CT(40,%d,%d,%s)$", index, gUI_AURABOX_INDEXES[box_type], tostring(isDebuffer))
        itemid = tooltip:InsertLeftText(resttime, "FFFF00", "fzheiti_11", 0, 0)
      end
      tooltip:InsertLeftText(des, "FAFAFA", "fzheiti_11", 0, 0)
      if caster then
        local strCaster = string.format("%s对你施放", caster)
        tooltip:InsertLeftText(strCaster, "FFFF00", "fzheiti_11", 0, 0)
      end
      Lua_Tip_End(tooltip)
    end
  end
end
function UI_Plays_BuffIconRclick(msg)
  local index = Lua_Plays_GetSelfAuraIndex(msg:get_lparam())
  CancelAura(index)
end
function UI_Plays_OnSelfClick(msg)
  local wnd = msg:get_window()
  Lua_MenuShow("ON_SELF")
end
function UI_Plays_SelectClick(msg)
  local wnd = msg:get_window()
  SetUITarget(tonumber(wnd:GetProperty("Value")))
end
function UI_Plays_OnTargetClick(msg)
  local wnd = msg:get_window()
  local char_type, lv, _, name, _, _, _, _, name_owner = GetTargetBaseInfo()
  local _, _, _, _, _, _, IsGM = GetPlaySelfProp(6)
  if char_type == gUI_CharType.CHAR_MAIN_PLAYER then
    if IsGM == 1 then
      Lua_MenuShow("ON_TARGET", name, _Plays_TargetObjID, lv)
    end
  elseif char_type == gUI_CharType.CHAR_PLAYER then
    local my_name = GetMyPlayerStaticInfo()
    if my_name == name then
      Lua_MenuShow("ON_SELF")
    else
      Lua_MenuShow("ON_TARGET", name, _Plays_TargetObjID, lv)
    end
  elseif char_type == gUI_CharType.CHAR_NPC and IsGM == 1 then
    Lua_MenuShow("ON_NPC", name, _Plays_TargetObjID, lv)
  end
end
function UI_Plays_OnSelfMercenaryClick(msg)
  Lua_MenuShow("ON_PARTNER_SELF", 0, "")
end
function UI_Plays_OnSelfPkmodeClick(msg)
  local pk_btn = msg:get_window()
  local index = pk_btn:GetCustomUserData(0)
  SetPKMode(index)
end
function UI_Plays_OnRouterStateClick(msg)
  WorldStage:StopPlayerRouter()
end
function UI_Plays_OnFollowStateClick(msg)
  WorldStage:StopFollowTarget()
end
function UI_Plays_PlayerTip(msg)
  local char_type = GetTargetBaseInfo(_Plays_TargetObjID)
  Lua_Tip_ShowCharTooltip(_Plays_TargetObjID, char_type)
end
function UI_Plays_PlayerTipClose(msg)
  Lua_Tip_HideObjTooltip()
end
function UI_Plays_ShowTargetInfoInviteTeam(msg)
  if _Plays_ViewPlayerGUID and _Plays_ViewPlayerName then
    InviteJoinTeamByGUID(_Plays_ViewPlayerName, _Plays_ViewPlayerGUID)
  end
end
function UI_Plays_ShowTargetInfoAddFriend(msg)
  if _Plays_ViewPlayerName then
    Lua_Fri_AddFriend(_Plays_ViewPlayerName, -1)
  end
end
function UI_Plays_ShowTargetInfoClose(msg)
  Lua_Detail_ShowChildWnd("PlayerInformation", false)
end
function UI_Plays_SPCombat()
  CastSPCombat()
end
function UI_Plays_ShowExpTip(msg)
  Lua_Tip_ExpBar(_Plays_CurExp, _Plays_MaxExp)
end
function UI_Plays_ShowTpTip(msg)
  local wnd = WindowToGoodsBox(msg:get_window())
  local tooltip = WindowToTooltip(wnd:GetToolTipWnd(0))
  Lua_Tip_Begin(tooltip)
  local desc = string.format("当前体力为%s", _Plays_CurTp)
  tooltip:InsertLeftText(desc, "FFFAFAFA", "", 0, 0)
  Lua_Tip_End(tooltip)
end
function UI_Plays_OnFocusSelect(msg)
  local objectID = SetUITargetFocus(false)
  if objectID ~= nil and objectID ~= -1 then
    WindowSys_Instance:GetWindow("TargetStats.TargetFocus_bg"):SetVisible(true)
    _OnPlays_TargetFocusChangedInfo(objectID)
  end
end
function UI_Plays_OnFocusClose(msg)
  SetUITargetFocus(true)
  WindowSys_Instance:GetWindow("TargetStats.TargetFocus_bg"):SetVisible(false)
end
function UI_Plays_OnFocusClick(msg)
  SetUISelectFocus()
end
function _Plays_BreakTimer(timer)
  local alpha = tonumber(gUI.ActBr.SpellBarTar_Break:GetProperty("Alpha"))
  alpha = alpha - timer:GetRealInterval()
  if alpha > 0.1 then
    gUI.ActBr.SpellBarTar_Break:SetProperty("Alpha", tostring(alpha))
  else
    gUI.ActBr.SpellBarTar_Break:SetProperty("Visible", "false")
    DelTimerEvent(3, "target_chanting")
  end
end
function _Plays_OverTimer(timer)
  local alpha = tonumber(gUI.ActBr.SpellBarTar_Over:GetProperty("Alpha"))
  alpha = alpha - timer:GetRealInterval()
  if alpha > 0.1 then
    gUI.ActBr.SpellBarTar_Over:SetProperty("Alpha", tostring(alpha))
  else
    gUI.ActBr.SpellBarTar_Over:SetProperty("Visible", "false")
    DelTimerEvent(3, "target_overing")
  end
end
function _OnPlays_SpellStart(dura, stype, other, desc, flag)
  local target_type = GetTargetBaseInfo()
  if target_type == nil or target_type ~= flag then
    return
  end
  local spell_type = stype
  local skill_des = gUI_SKILL_PROGRESS_DES[other]
  spell_duration = dura / 1000
  gUI.ActBr.SpellBarTar:SetProperty("Visible", "true")
  gUI.ActBr.SpellBarTar:SetProperty("Text", skill_des or tostring(desc) or "")
  if spell_type == 0 then
    gUI.ActBr.SpellBarTar:SetProperty("Progress", "0")
  else
    gUI.ActBr.SpellBarTar:SetProperty("Progress", "1")
  end
end
function _OnPlays_SpellDelay(dura, flag)
  target_type = GetTargetBaseInfo()
  if flag == 1 or target_type == gUI_CharType.CHAR_MAIN_PLAYER then
    local spell_delay = dura ~= -1 and dura / (spell_duration * 1000) or dura
    if spell_delay == -1 and gUI.ActBr.SpellBarTar:IsVisible() then
      gUI.ActBr.SpellBarTar_Break:SetProperty("Visible", "true")
      gUI.ActBr.SpellBarTar_Break:SetProperty("Alpha", "1.0")
      AddTimerEvent(3, "target_chanting", _Plays_BreakTimer)
      gUI.ActBr.SpellBarTar_Break:SetProperty("Text", gUI.ActBr.SpellBarTar:GetProperty("Text"))
    end
  end
end
function _OnPlays_SpellOver(isleadtype, flag)
  if flag == 1 then
    gUI.ActBr.SpellBarTar_Over:SetProperty("Visible", "true")
    gUI.ActBr.SpellBarTar_Over:SetProperty("Alpha", "1.0")
    AddTimerEvent(3, "target_overing", _Plays_OverTimer)
    gUI.ActBr.SpellBarTar_Over:SetProperty("Text", gUI.ActBr.SpellBarTar:GetProperty("Text"))
    if isleadtype then
      gUI.ActBr.SpellBarTar_Over:SetProperty("Progress", "0")
    else
      gUI.ActBr.SpellBarTar_Over:SetProperty("Progress", "1")
    end
  end
end
function _OnPlays_SpellDisturb(dura)
  local progress = tonumber(gUI.ActBr.SpellBarTar:GetProperty("Progress"))
  if progress < 1 then
    progress = progress - dura / 1000 / spell_duration
    gUI.ActBr.SpellBarTar:SetProperty("Progress", tostring(progress))
  end
end
function _OnPlays_SpellUPDATE(lost, dura, stype, other, desc)
  if lost == -1 and dura == -1 then
    gUI.ActBr.SpellBarTar:SetProperty("Visible", "false")
    return
  end
  local skill_des = gUI_SKILL_PROGRESS_DES[other]
  spell_duration = dura / 1000
  gUI.ActBr.SpellBarTar:SetProperty("Visible", "true")
  gUI.ActBr.SpellBarTar:SetProperty("Text", skill_des or tostring(desc) or "")
  if stype == 0 then
    local progress = lost / dura
    gUI.ActBr.SpellBarTar:SetProperty("Progress", tostring(progress))
  else
    local progress = 1 - lost / dura
    gUI.ActBr.SpellBarTar:SetProperty("Progress", tostring(progress))
  end
end
function _OnPlays_ChangeMapImage(arg1)
  if arg1 == 0 then
    gUI.ActBr.SpellBarTar_Break:SetProperty("Visible", "false")
    gUI.ActBr.SpellBarTar_Over:SetProperty("Visible", "false")
    DelTimerEvent(3, "target_chanting")
    DelTimerEvent(3, "target_overing")
  end
  UI_Plays_OnFocusClose()
end
function _OnPlays_UpdateXP(cur_exp, max_exp)
  local progress = 0
  if max_exp ~= 0 then
    progress = cur_exp / max_exp
  end
  local bar = WindowSys_Instance:GetWindow("ActionBar_frm.Main_bg.Exp_bar")
  bar:SetProperty("Progress", tostring(progress))
  _Plays_CurExp = cur_exp
  _Plays_MaxExp = max_exp
end
function _OnPlays_SeverStatus(ping)
  local wnd = WindowSys_Instance:GetWindow("MiniMap.NetSpeed_pic")
  wnd:SetProperty("TooltipText", "延迟：" .. tostring(ping) .. "毫秒")
  if ping <= 100 then
    wnd:SetProperty("BackImage", "UiIamge012:Image_Green")
  elseif ping > 100 and ping <= 500 then
    wnd:SetProperty("BackImage", "UiIamge012:Image_Yellow")
  elseif ping > 500 then
    wnd:SetProperty("BackImage", "UiIamge012:Image_Red")
  end
end
function _OnPlays_MainPlayerDateUpdate()
  local name, menpai, gender, head, head_model, hair_model, hair_color = GetMyPlayerStaticInfo()
  if name then
    gUI_MainPlayerAttr.Name = name
    gUI_MainPlayerAttr.MenPai = menpai
    gUI_MainPlayerAttr.Sex = gender
    local frame = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg")
    frame:GetChildByName("Portrait_pic"):SetProperty("BackImage", gUI_PLAYER_ICON[menpai][gender][head])
  end
end
function Script_Plays_OnLoad()
  local name, menpai, gender, head, head_model, hair_model, hair_color = GetMyPlayerStaticInfo()
  if name then
    gUI_MainPlayerAttr.Name = name
    gUI_MainPlayerAttr.MenPai = menpai
    gUI_MainPlayerAttr.Sex = gender
    local frame = WindowSys_Instance:GetWindow("PlayerStats.TopSelf_bg.Stats_bg")
    frame:GetChildByName("Portrait_pic"):SetProperty("BackImage", gUI_PLAYER_ICON[menpai][gender][head])
    frame:GetChildByName("Name_dlab"):SetProperty("Text", name)
    local mpBar = frame:GetChildByName("MP_pbar")
    local epBar = frame:GetChildByName("EP_pbar")
    local dotback = frame:GetChildByName("SP_bg")
    if gUI_MainPlayerAttr.MenPai == 4 then
      mpBar:SetProperty("Visible", "false")
      epBar:SetProperty("Visible", "true")
      _Plays_Bar = epBar
    else
      mpBar:SetProperty("Visible", "true")
      epBar:SetProperty("Visible", "false")
      if gUI_MainPlayerAttr.MenPai == 1 then
        mpBar:SetProperty("FrontImage", _Plays_ColorQS)
        mpBar:SetProperty("TooltipText", "精力值")
      elseif gUI_MainPlayerAttr.MenPai == 0 then
        mpBar:SetProperty("FrontImage", _Plays_ColorSJ)
        mpBar:SetProperty("TooltipText", "怒气值，每次受到攻击可获得")
      else
        mpBar:SetProperty("FrontImage", _Plays_ColorNormal)
        mpBar:SetProperty("TooltipText", "法力值")
      end
      _Plays_Bar = mpBar
    end
    if gUI_MainPlayerAttr.MenPai == 1 then
      dotback:SetProperty("Visible", "true")
    else
      dotback:SetProperty("Visible", "false")
    end
  end
  local bufferWnd = WindowToGoodsBox(WindowSys_Instance:GetWindow("BufferBar.buffframe"))
  bufferWnd:ResetAllGoods(true)
  bufferWnd = WindowToGoodsBox(WindowSys_Instance:GetWindow("BufferBar.debuffframe"))
  bufferWnd:ResetAllGoods(true)
  bufferWnd = WindowToGoodsBox(WindowSys_Instance:GetWindow("TargetStats.Target_bg.Debuff_gbox"))
  bufferWnd:ResetAllGoods(true)
  bufferWnd = WindowToGoodsBox(WindowSys_Instance:GetWindow("TargetStats.Target_bg.Mydebuff_gbox"))
  bufferWnd:ResetAllGoods(true)
  local root = WindowSys_Instance:GetWindow("PlayerStats")
  root:GetChildByName("Self_bg"):SetProperty("Visible", "true")
  root = WindowSys_Instance:GetWindow("TargetStats")
  root:GetChildByName("Target_bg"):SetProperty("Visible", "false")
  local button = WindowSys_Instance:GetWindow("Follow_btn")
  button:SetProperty("Visible", "false")
  gUI.ActBr.SpellBarTar = WindowSys_Instance:GetWindow("TargetStats.Target_bg.Stats_bg.Skill_pbar")
  gUI.ActBr.SpellBarTar_Break = WindowSys_Instance:GetWindow("TargetStats.Target_bg.Stats_bg.SkillBreak_pbar")
  gUI.ActBr.SpellBarTar_Over = WindowSys_Instance:GetWindow("TargetStats.Target_bg.Stats_bg.SkillOver_pbar")
  gUI.ActBr.SpellBarTar:SetProperty("Visible", "false")
  gUI.ActBr.SpellBarTar_Break:SetProperty("Visible", "false")
  gUI.ActBr.SpellBarTar_Over:SetProperty("Visible", "false")
  DelTimerEvent(3, "target_chanting")
  DelTimerEvent(3, "target_overing")
  _Plays_CurExp = 0
  _Plays_MaxExp = 1
end
function Script_Plays_OnEvent(event)
  if event == "PLAYER_INFO_CHANGED" then
    _Plays_SelfChangedInfo()
  elseif event == "PLAYER_SPELL_START" then
    _OnPlays_SpellStart(arg1, arg2, arg3, arg4, arg5)
  elseif event == "PLAYER_SPELL_BREAK" then
    _OnPlays_SpellDelay(arg1, arg2)
  elseif event == "PLAYER_SPELL_OVER" then
    _OnPlays_SpellOver(arg1, arg2)
  elseif event == "PLAYER_SPELL_DISTURB" then
    _OnPlays_SpellDisturb(arg1)
  elseif event == "PLAYER_CHANGEMAP_IMAGE" then
    _OnPlays_ChangeMapImage(arg1)
  elseif event == "TARGET_INFO_CHANGED" then
    _OnPlays_TargetChangedInfo(arg1, true)
  elseif event == "TARGETFOCUS_INFO_CHANGED" then
    _OnPlays_TargetFocusChangedInfo(arg1)
  elseif event == "PLAYER_TARGET_CHANGED" then
    _OnPlays_TargetChanged(arg1)
  elseif event == "PLAYER_BUFF_UPDATE" then
    _OnPlays_SelfBuffUpdate()
  elseif event == "TARGET_BUFF_UPDATE" then
    _OnPlays_TargetBuffUpdate()
  elseif event == "PLAYER_LEVEL_CHANGED" then
    _Plays_LevelUp(arg1)
  elseif event == "PLAYER_PKMODE_CHANGE" then
    _OnPlays_ModeChange(arg1)
  elseif event == "PLAYER_ROUTER_CHANGED" then
    _OnPlays_RouterStateChange(arg1, arg2, arg3, arg4, arg5)
  elseif event == "PLAYER_FOLLOW_CHANGED" then
    _OnPlays_FollowStateChange(arg1, arg2)
  elseif event == "CHAT_TARGET_SHOW" then
    _OnPlays_ShowTargeInfo(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "CHAT_TARGET_SHOWSTAR" then
    _OnPlays_ShowTargeInfoEquipMark(arg1)
  elseif event == "PLAYER_SPELL_UPDATE" then
    _OnPlays_SpellUPDATE(arg1, arg2, arg3, arg4, arg5)
  elseif "PLAYER_SEVER_LATENCY" == event then
    _OnPlays_SeverStatus(arg1)
  elseif "PLAYER_EXP_CHANGED" == event then
    _OnPlays_UpdateXP(arg1, arg2)
  elseif "PLAYER_INFO_INIT" == event then
    _OnPlays_MainPlayerDateUpdate()
  end
end
