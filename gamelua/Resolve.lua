if gUI and not gUI.Resolve then
  gUI.Resolve = {}
end
local _MAX_SLOT = 12
local _SROLL_POS = 0
local _INDEX_GOL = 0
local IS_FLAG = true
function Resolve_OpenPanel()
  if gUI.Resolve.Root:IsVisible() then
    gUI.Resolve.Root:SetVisible(false)
  else
    gUI.Resolve.Root:SetVisible(true)
    Faerie_Income_ClosePanel()
    Resolve_InitData()
    ResolveRefushInfos(0)
  end
end
function Resolve_InitData()
  _Resolve_SetMoney(0, 0, "")
  gUI.Resolve.MainEquip:ResetAllGoods(true)
  gUI.Resolve.DesDBox:ClearAllContent()
  IS_FLAG = true
  for i = 0, 1 do
    gUI.Resolve.QCheckBtn[i]:SetProperty("Checked", "true")
  end
  gUI.Resolve.QCheckBtn[2]:SetProperty("Checked", "false")
  gUI.Resolve.Contain:DeleteAll()
end
function UI_Resolve_CloseClick(msg)
  Resolve_OpenPanel()
end
function UI_Resolve_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Title_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _Resolve_BeginUpdate(arg1)
  if arg1 == 0 then
  else
    local scrollbar = WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Resolve_cnt.vertSB_")
    if scrollbar then
      scrollbar = WindowToScrollBar(scrollbar)
      _SG_BarPos = scrollbar:GetCurrentPos()
    end
  end
  gUI.Resolve.Contain:DeleteAll()
  _INDEX_GOL = 0
end
function _Resolve_UpdateInfos(icon, name, quilty, job, lev, guid, Sel, index, EnchLev)
  if icon then
    if not gUI.Resolve.QCheckBtn[quilty - 1]:IsChecked() then
      return
    end
    if gUI.Resolve.PurBlendBtn:IsChecked() then
      local bag, slot = GetItemPosByGuid(guid)
      local attrList = GetBlendAttr(slot, bag, 0)
      for i = 0, table.maxn(attrList) do
        local _, _, _, _, IdTable, _ = GetItemBaseInfoBySlot(bag, slot)
        local blendLev = GetItemBlendColor(IdTable, attrList[i].typeId, attrList[i].value)
        if blendLev >= 3 then
          return
        end
      end
    end
    local Items = gUI.Resolve.Contain:Insert(-1, gUI.Resolve.Temp)
    Items:SetProperty("WindowName", "Pro_gbox_index" .. tostring(_INDEX_GOL))
    _INDEX_GOL = _INDEX_GOL + 1
    Items:SetProperty("Visible", "true")
    local ItemGb = WindowToGoodsBox(Items:GetGrandChild("Item_gbox"))
    local NameLb = WindowToLabel(Items:GetGrandChild("Name_dlab"))
    local JobLb = WindowToLabel(Items:GetGrandChild("Profession_dlab"))
    local LevLb = WindowToLabel(Items:GetGrandChild("Level_dlab"))
    local SelCbtn = WindowToCheckButton(Items:GetGrandChild("Choose_cbtn"))
    local Bg = WindowToCheckButton(Items:GetGrandChild("Sel_bg"))
    local color = colorWhite
    local jobnames = ""
    local playJob = gUI_MainPlayerAttr.MenPai
    if not checkJobKindIsCanUse(playJob, job) then
      color = colorRed
    end
    if job ~= -1 then
      local job1, job2, job3, job4, job5 = GetJobNamesByJobID(job)
      if job1 then
        jobnames = jobnames .. job1 .. " "
      end
      if job2 then
        jobnames = jobnames .. job2 .. " "
      end
      if job3 then
        jobnames = jobnames .. job3 .. " "
      end
      if job4 then
        jobnames = jobnames .. job4 .. " "
      end
      if job5 then
        jobnames = jobnames .. job5 .. " "
      end
    else
      jobnames = "ȫְҵ"
    end
    JobLb:SetProperty("Text", tostring(jobnames))
    JobLb:SetProperty("FontColor", color)
    local playLevel = gUI_MainPlayerAttr.Level
    LevLb:SetProperty("Text", tostring(lev))
    color = colorWhite
    if lev > playLevel then
      color = colorRed
    end
    LevLb:SetProperty("FontColor", color)
    if Sel == 0 then
      SelCbtn:SetProperty("Checked", "false")
      Bg:SetProperty("Visible", "false")
    else
      SelCbtn:SetProperty("Checked", "true")
      Bg:SetProperty("Visible", "true")
    end
    local strEnlev = ""
    if EnchLev > 0 then
      strEnlev = "+" .. tostring(EnchLev)
    end
    NameLb:SetProperty("Text", "{#C^" .. tostring(gUI_GOODS_QUALITY_COLOR_PREFIX[quilty]) .. "^" .. tostring(name) .. strEnlev .. "}")
    ItemGb:SetItemGoods(0, icon, quilty)
    local intenLev = EnchLev
    if 0 < ForgeLevel_To_Stars[intenLev] then
      ItemGb:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
    else
      ItemGb:SetItemStarState(0, 0)
    end
    Items:SetUserData64(guid)
  end
end
function _Resolve_EndInfos(arg1)
  if arg1 == 0 then
  else
    local scrollbar = WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Resolve_cnt.vertSB_")
    if scrollbar and scrollbar:IsVisible() and _SG_BarPos then
      scrollbar = WindowToScrollBar(scrollbar)
      scrollbar:SetCurrentPos(_SG_BarPos)
    end
  end
end
function _Resolve_Refresh_GbBegin()
  gUI.Resolve.MainEquip:ResetAllGoods(true)
end
function _Resolve_Refresh_Gb(index, Icon, quilty, intenLev)
  gUI.Resolve.MainEquip:SetItemGoods(index, Icon, quilty)
  gUI.Resolve.MainEquip:SetItemData(index, index)
  if ForgeLevel_To_Stars[intenLev] > 0 then
    gUI.Resolve.MainEquip:SetItemStarState(0, ForgeLevel_To_Stars[intenLev])
  else
    gUI.Resolve.MainEquip:SetItemStarState(0, 0)
  end
end
function _OnResolve_AddSlot(guid, index, bag, slot)
  local name, icon, quality, minLevel, job = GetItemInfoBySlot(bag, slot, -1)
  local intenLev = Lua_Bag_GetStarInfo(bag, slot)
  _Resolve_Refresh_Gb(index, icon, quality, intenLev)
  for i = 0, 200 do
    local item = gUI.Resolve.Contain:GetChildByIndex(i)
    if item then
      local guid1 = item:GetUserData64()
      if guid1 == guid then
        local SelCbtn = WindowToCheckButton(item:GetGrandChild("Choose_cbtn"))
        local Bg = WindowToCheckButton(item:GetGrandChild("Sel_bg"))
        SelCbtn:SetProperty("Checked", "true")
        Bg:SetProperty("Visible", "true")
        break
      end
    end
  end
  _Resolve_SetMoney()
end
function _OnResolve_DelSlot(index, guid)
  _OnResolve_Suess(index)
  for i = 0, 200 do
    local item = gUI.Resolve.Contain:GetChildByIndex(i)
    if item then
      local guid1 = item:GetUserData64()
      if guid1 == guid then
        local SelCbtn = WindowToCheckButton(item:GetGrandChild("Choose_cbtn"))
        local Bg = WindowToCheckButton(item:GetGrandChild("Sel_bg"))
        SelCbtn:SetProperty("Checked", "false")
        Bg:SetProperty("Visible", "false")
        break
      end
    end
  end
  _Resolve_SetMoney()
end
function _Resolve_Refresh_GbEnd()
end
function _Resolve_SetMoney(newMoney, changeMoney, event)
  if gUI.Resolve.Root:IsVisible() then
    local itemMoney = GetDecopMoney()
    if itemMoney then
      gUI.Resolve.MonLab:SetProperty("Text", Lua_UI_Money2String(itemMoney, true, false, false, false, newMoney, event))
    end
  end
end
function UI_Resolve_Tip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local boxName = win:GetProperty("WindowName")
  local tooltip = win:GetToolTipWnd(0)
  if not gUI.Resolve.Contain:IsMouseIn() then
    return
  end
  if win:IsItemHasIcon(0) then
    local guid = win:GetParent():GetUserData64()
    local bag, slot = GetItemPosByGuid(guid)
    if slot ~= -1 then
      Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
      Lua_Tip_ShowEquipCompare(win, bag, slot)
    end
  end
end
function UI_Resolve_ItemTip(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local tooltip = win:GetToolTipWnd(0)
  local from_off = msg:get_wparam()
  local bag, slot = GetResolveInfo(from_off, NpcFunction.NPC_FUNC_ITEM_DECOMPOSE)
  if bag then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil)
    Lua_Tip_ShowEquipCompare(win, bag, slot)
  end
end
function UI_Resolve_EquipOnRClick(msg)
  local win = WindowToGoodsBox(msg:get_window())
  local from_off = msg:get_wparam()
  local bag, slot = GetResolveInfo(from_off, NpcFunction.NPC_FUNC_ITEM_DECOMPOSE)
  local guid = GetItemGUIDByPos(bag, slot, 0)
  ResolveAddOrDel(guid)
end
function UI_Resolve_OnDraged(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local itemId = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  local form_type = from_win:GetProperty("BoxType")
  local bagType = gUI_GOODSBOX_BAG[form_type]
  local boxName = to_win:GetProperty("WindowName")
  if from_win:GetProperty("GoodBoxIndex") == "GB_ItemMainBag" then
    local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, itemId, 0)
    ResolveAddOrDel(guid)
  end
end
function UI_Resolve_DoAction(msg)
  local money, quilty = GetDecopMoney()
  local bankMoney, nonBindemoney, bindMoney = GetBankAndBagMoney()
  if money > nonBindemoney then
    Lua_Chat_ShowOSD("NOT_ENOUGH_NOBINDMONEY")
    return
  end
  if quilty >= 2 then
    Messagebox_Show("NPC_ITEM_DESCOMPSE")
  else
    DisassComfim()
  end
end
function UI_Resolve_CheckClick(msg)
  ResolveRefushInfos(1)
end
function UI_Resolve_CheckBtnClick(msg)
  local win = msg:get_window()
  local guid = win:GetParent():GetUserData64()
  win = WindowToCheckButton(win)
  if not gUI.Resolve.Contain:IsMouseIn() then
    return
  end
  ResolveAddOrDel(guid)
end
function _Resolve_Noti_Ref()
  if gUI.Resolve.Root:IsVisible() then
  end
end
function _Resolve_Add_Item(bag, slot, icon, count, locked, quality)
  if gUI.Resolve.Root:IsVisible() then
    local NeedAdd = true
    local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, slot, 0)
    for i = 0, 200 do
      local item = gUI.Resolve.Contain:GetChildByIndex(i)
      if item then
        local guid1 = item:GetUserData64()
        if guid1 == guid then
          NeedAdd = false
          break
        end
      end
    end
    if NeedAdd then
      if IS_FLAG then
        IS_FLAG = false
        gUI.Resolve.RefTime:Restart()
      else
        gUI.Resolve.RefTime:Restart()
      end
    end
  end
end
function _Resolve_Del_Item()
  if gUI.Resolve.Root:IsVisible() then
    if IS_FLAG then
      IS_FLAG = false
      gUI.Resolve.RefTime:Restart()
    else
      gUI.Resolve.RefTime:Restart()
    end
  end
end
function _Resolve_RefTime(timer)
  IS_FLAG = true
  ResolveRefushInfos(1)
  gUI.Resolve.RefTime:Stop()
end
function _OnResolve_Suess(slot)
  if gUI.Resolve.Root:IsVisible() and slot < _MAX_SLOT then
    gUI.Resolve.MainEquip:ClearGoodsItem(slot)
  end
end
function _OnResolve_Reslut(content)
  if gUI.Resolve.Root:IsVisible() then
    gUI.Resolve.DesDBox:InsertBack(content, 4294967295, 0, 0)
  end
end
function _OnResolve_Ref_All()
  if gUI.Resolve.Root:IsVisible() then
    if IS_FLAG then
      IS_FLAG = false
      gUI.Resolve.RefTime:Restart()
    else
      gUI.Resolve.RefTime:Restart()
    end
    _Resolve_SetMoney()
  end
end
function _OnResolve_Not_En()
  Lua_Chat_ShowOSD("DISASSEMBLE_NOEN")
  return
end
function _Resolve_Noti_Res(itemCanDis)
  if itemCanDis == ERRORRESLUT.NOTITEM then
    return
  elseif itemCanDis == ERRORRESLUT.NOTEQUIP then
    Lua_Chat_ShowOSD("DISASSEMBLE_NOEQUIP")
    return
  elseif itemCanDis == ERRORRESLUT.CANNOTDIS then
    Lua_Chat_ShowOSD("DISASSEMBLE_CANNOTDIS")
    return
  elseif itemCanDis == ERRORRESLUT.INFISMAX then
    Lua_Chat_ShowOSD("DISASSEMBLE_INFISMAX")
    return
  elseif itemCanDis == ERRORRESLUT.HASGEM then
    Lua_Chat_ShowOSD("DISASSENBLE_HASGEM")
    return
  elseif itemCanDis == ERRORRESLUT.HASSIGN then
    Lua_Chat_ShowOSD("DISASSENBLE_HASSIGN")
    return
  elseif itemCanDis == ERRORRESLUT.CANDIS then
    return
  end
end
function Script_Resolve_OnEvent(event)
  if event == "RESOLVE_UPDATEINFOBEGIN" then
    _Resolve_BeginUpdate(arg1)
  elseif event == "RESOLVE_UPDATEINFOS" then
    _Resolve_UpdateInfos(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
  elseif event == "RESOLVE_UPDATEINFOEND" then
    _Resolve_EndInfos(arg1)
  elseif event == "RESOLVE_UPDATESLOTBEGIN" then
    _Resolve_Refresh_GbBegin()
  elseif event == "RESOLVE_UPDATESLOTS" then
    _Resolve_Refresh_Gb(arg1, arg2, arg3, arg4)
  elseif event == "RESOLVE_UPDATESLOTEND" then
    _Resolve_Refresh_GbEnd()
  elseif event == "RESOLVE_NOTIFTY_UPDATE" then
    _Resolve_Noti_Ref()
  elseif event == "PLAYER_MONEY_CHANGED" then
    _Resolve_SetMoney(arg1, arg2, event)
  elseif event == "PLAYER_BIND_MONEY_CHANGED" then
    _Resolve_SetMoney(arg1, arg2, event)
  elseif event == "ITEM_ADD_TO_BAG" then
    _Resolve_Add_Item(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12)
  elseif event == "ITEM_DEL_FROM_BAG" then
    _Resolve_Del_Item(arg1, arg2)
  elseif event == "RESOLVE_NOTI_RES" then
    _Resolve_Noti_Res(arg1)
  elseif event == "DISASSEMBLE_RESLUTCONTENT" then
    _OnResolve_Reslut(arg1)
  elseif event == "DISASSEMBLE_SUESS" then
    _OnResolve_Suess(arg1)
  elseif event == "RESOLVE_BAG_ENOUGH" then
    _OnResolve_Not_En()
  elseif event == "RESOLVE_REF_ALL" then
    _OnResolve_Ref_All()
  elseif event == "RESOLVE_NOTIFTY_ADDSLOT" then
    _OnResolve_AddSlot(arg1, arg2, arg3, arg4)
  elseif event == "RESOLVE_NOTIFTY_DELSLOT" then
    _OnResolve_DelSlot(arg1, arg2)
  end
end
function Script_Resolve_OnLoad()
  gUI.Resolve.Root = WindowSys_Instance:GetWindow("Resolve")
  gUI.Resolve.MainEquip = WindowToGoodsBox(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Left_bg.Up_bg.Equip_bg.Equip_gbox"))
  gUI.Resolve.FirmBtn = WindowToButton(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Left_bg.Up_bg.Resolve_btn"))
  gUI.Resolve.DesDBox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Left_bg.Down_bg.List_bg.Record_dbox"))
  gUI.Resolve.MonLab = WindowToLabel(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Left_bg.Up_bg.Money_bg.Money_dlab"))
  gUI.Resolve.Temp = WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Item_bg")
  gUI.Resolve.Contain = WindowToContainer(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Resolve_cnt"))
  gUI.Resolve.QCheckBtn = {}
  gUI.Resolve.QCheckBtn[2] = WindowToCheckButton(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Choose_bg.Purple_cbtn"))
  gUI.Resolve.QCheckBtn[1] = WindowToCheckButton(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Choose_bg.Blue_cbtn"))
  gUI.Resolve.QCheckBtn[0] = WindowToCheckButton(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Choose_bg.Green_cbtn"))
  gUI.Resolve.PurBlendBtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Resolve.Resolve_bg.Right_bg.Choose_bg.PurpleBlend_cbtn"))
  gUI.Resolve.PurBlendBtn:SetProperty("Checked", "true")
  gUI.Resolve.RefTime = TimerSys_Instance:CreateTimerObject("RefTime", 1, -1, "_Resolve_RefTime", false, false)
end
function Script_Resolve_OnHide()
  ClearResolveSlot()
end
solveSlot()
end
