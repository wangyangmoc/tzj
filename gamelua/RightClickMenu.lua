local _Rcm_strLastWishperName = "姓名"
local _Rcm_strLastMenu = ""
local _Rcm_relation = 40
local _Rcm_move_to_group = _Rcm_relation + 15
local _Rcm_pkmode = _Rcm_move_to_group + 12
local _Rcm_chat = _Rcm_pkmode + 5
local _Rcm_ditribute_mode = _Rcm_chat + 10
local _Rcm_ditribute_level = _Rcm_ditribute_mode + 8
local _Rcm_instance = _Rcm_ditribute_level + 6
local _Rcm_GM = _Rcm_instance + 10
local _Rcm_Guild = _Rcm_GM + 10
local _Rcm_total = 199
if not gUI then
  gUI = {}
end
if not gUI.Rcm then
  gUI.Rcm = {}
end
if not gUI.Rcm.Menus then
  gUI.Rcm.Menus = {
    root = -1,
    menu = -1,
    child_menu = -1,
    cur_menu = -1
  }
  gUI.Rcm.Menus.__index = gUI.Rcm.Menus
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 5] = function(self)
    Lua_Cp2p_AddTargetToCur(self.name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 6] = function(self)
    Lua_Fri_RenameGroup(self.group_id)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 7] = function(self)
    Lua_Fri_DelGroup(self.group_id)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 8] = function(self)
    Lua_Fri_AddGroup()
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 9] = function(self)
    Lua_Fri_DelBlack(self.guid, self.name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 10] = function(self)
    Lua_Fri_ShowEnemyPos(self.guid, self.name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_relation + 11] = function(self)
    Lua_Fri_AddBlackByGUID(self.guid, self.name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_pkmode] = function(self)
    SetPKMode(0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_pkmode + 1] = function(self)
    SetPKMode(1)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_pkmode + 2] = function(self)
    SetPKMode(2)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_pkmode + 3] = function(self)
    SetPKMode(3)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_pkmode + 4] = function(self)
    SetPKMode(4)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Normal)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 1] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.World)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 2] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Team)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 3] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Alliance)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 4] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Guild)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 5] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 6] = function(self)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Map)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 7] = function(self)
    local name = Lua_Chat_GetLastWishperName(0)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 8] = function(self)
    local name = Lua_Chat_GetLastWishperName(1)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 9] = function(self)
    local name = Lua_Chat_GetLastWishperName(2)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 10] = function(self)
    local name = Lua_Chat_GetLastWishperName(3)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_chat + 11] = function(self)
    local name = Lua_Chat_GetLastWishperName(4)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_mode] = function(self)
    SetDistributeMode(0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_mode + 1] = function(self)
    SetDistributeMode(1)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_mode + 2] = function(self)
    SetDistributeMode(2)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_mode + 3] = function(self)
    SetDistributeMode(3)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_level] = function(self)
    SetDistributeLevel(0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_level + 1] = function(self)
    SetDistributeLevel(1)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_level + 2] = function(self)
    SetDistributeLevel(2)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_level + 3] = function(self)
    SetDistributeLevel(3)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_level + 4] = function(self)
    SetDistributeLevel(4)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_ditribute_level + 5] = function(self)
    SetDistributeLevel(5)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_instance + 0] = function(self)
    InstanceExit()
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_instance + 1] = function(self)
    InstanceReset()
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_GM] = function(self)
    ChatDialog_GMCommand(_Rcm_GM, 0, 0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_GM + 1] = function(self)
    ChatDialog_GMCommand(_Rcm_GM + 1, 0, 0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_GM + 2] = function(self)
    ChatDialog_GMCommand(_Rcm_GM + 2, 0, 0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_GM + 3] = function(self)
    ChatDialog_GMCommand(_Rcm_GM + 3, self.objTable_id, 0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_GM + 4] = function(self)
    ChatDialog_GMCommand(_Rcm_GM + 4, self.name, 0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_GM + 5] = function(self)
    ChatDialog_GMCommand(_Rcm_GM + 5, self.name, 0)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_Guild] = function(self)
    CopyDataToClipboard(self.name)
  end
  {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_Guild + 1] = function(self)
    CopyDataToClipboard(self.hostName)
  end
  gUI.Rcm.Menus.handlers, {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }[_Rcm_total] = {
    ["name"] = "",
    ["guid"] = -1,
    ["group_id"] = -1,
    [1] = function(self)
      CopyDataToClipboard(self.name)
    end,
    [2] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:FollowTarget(self.object_id)
      else
        Lua_Chat_ShowOSD("FOLLOW_TOO_FAR")
      end
    end,
    [3] = function(self)
      Lua_Plays_ShowOtherBaseInfo(true, nil, self.guid, self.object_id)
    end,
    [4] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, self.name, self.guid)
    end,
    [5] = function(self)
      RequestTargetEquipList(self.object_id)
    end,
    [6] = function(self)
      Lua_Plays_ShowOtherBaseInfo()
    end,
    [7] = function(self)
      InviteJoinTeam(self.name, self.object_id)
    end,
    [8] = function(self)
      AppointTeamLeader(self.name)
    end,
    [9] = function(self)
      Messagebox_Show("CONFIRM_KICK_TEAM", self.name)
    end,
    [10] = function(self)
      Messagebox_Show("CONFIRM_QUIT_TEAM")
    end,
    [11] = function(self)
      RecruitGuildMember(self.name)
    end,
    [12] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        if gUI.Exchange.ApplyWndState == false then
          SwapRequestTrade(self.object_id)
        else
          ShowErrorMessage(UI_SYS_MSG.EXCHANGE_APLLY_STATUE)
        end
      else
        Lua_Chat_ShowOSD("EXCHANGE_TOOFAR")
      end
    end,
    [13] = function(self)
      local distance = GetCharacterDistance(self.object_id)
      if self.object_id ~= -1 and distance and distance < 10 then
        WorldStage:InviteDuel(self.object_id)
      else
        Lua_Chat_ShowOSD("DUEL_TOOFAR")
      end
    end,
    [14] = function(self)
      _Rcm_strLastWishperName = self.name
      Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, _Rcm_strLastWishperName)
    end,
    [15] = function(self)
      InviteMultiMount()
    end,
    [16] = function(self)
      MountKickPas(self.object_id)
    end,
    [17] = function(self)
      LeaveMultiMount()
    end,
    [18] = function(self)
      ApplyJoinGuild(self.guild_id_target)
    end,
    [19] = function(self)
      Lua_Plays_ShowOtherBaseInfo(nil, nil, self.guid)
    end,
    [20] = function(self)
      if self.lv and self.lv < gUI_PK_Level then
        Lua_Chat_ShowOSD("TIANZHU_TAR_LV_LOW")
        return
      end
      UseWrathByRMenu(self.guid, self.name)
    end,
    [21] = function(self)
      AskOtherPlayerFaerie(self.object_id)
    end,
    [22] = function(self)
      PetFeedByOtherPlayer(self.guid)
    end,
    [23] = function(self)
      PrintPlayerDebugInfo(self.object_id)
    end,
    [24] = function(self)
      Lua_GuildM_LeaderAlter(self.guid, self.name)
    end,
    [25] = function(self)
      Lua_GuildM_KickMember(self.guid, self.name)
    end,
    [26] = function(self)
      UseWrathByRMenu(self.guid, self.name)
    end,
    [_Rcm_relation] = function(self)
      Lua_Fri_AddFriend(self.name, self.object_id)
    end,
    [_Rcm_relation + 1] = function(self)
      Lua_Fri_AddBlack(self.name, self.object_id)
    end,
    [_Rcm_relation + 2] = function(self)
      InviteJoinTeamByGUID(self.name, self.guid)
    end,
    [_Rcm_relation + 3] = function(self)
      Lua_Fri_DelRelation(self.guid, self.name)
    end,
    [_Rcm_relation + 4] = function(self)
      Lua_Fri_RequestDetail(self.guid, self.name)
    end
  }, function(self)
  end
end
function gUI.Rcm.Menus:Register(menu, name, data, color)
  if color == nil then
    color = ""
  end
  local item = menu:InsertItem(name, -1, nil, color)
  item:SetUserData(data)
end
function gUI.Rcm.Menus:OnSelected(msg)
  local index = msg:get_lparam()
  self.handlers[index](self.handlers)
end
gUI.Rcm.Menus.ON_SELF = {
  InitMenu = function(self)
    local menu = self.menu
    menu:Clear()
    local name, has_team, is_leader, _, _, multi_mount = GetPlaySelfProp(6)
    self.handlers.name = name
    self:Register(menu, "复制名称", 1)
    if has_team then
      if is_leader then
        local childmenu = self.child_menu
        childmenu:Clear()
        for i = 1, #gUI_GOODS_DISTRIBUTE_MODE do
          if gUI_GOODS_DISTRIBUTE_MODE[i] ~= "" then
            self:Register(childmenu, gUI_GOODS_DISTRIBUTE_MODE[i], _Rcm_ditribute_mode + i - 1)
          end
        end
        childmenu:SetProperty("CheckedMenuItem", tostring(gUI.Rcm.CurDistributeMode))
        menu:InsertItem("拾取  " .. gUI_GOODS_DISTRIBUTE_MODE[gUI.Rcm.CurDistributeMode + 1], -1, childmenu, "")
        local childmenu2 = self.child_menu2
        childmenu2:Clear()
        for i = 1, #gUI_GOODS_DISTRIBUTE_QUALITY do
          local item = childmenu2:InsertItemEx(gUI_GOODS_DISTRIBUTE_QUALITY[i], -1, gUI_GOODS_QUALITY_COLOR_PREFIX[i - 1], "", true)
          item:SetUserData(_Rcm_ditribute_level + i - 1)
        end
        childmenu2:SetProperty("CheckedMenuItem", tostring(gUI.Rcm.CurDistributeLevel))
        menu:InsertItemEx("分配  " .. gUI_GOODS_DISTRIBUTE_QUALITY[gUI.Rcm.CurDistributeLevel + 1], -1, gUI_GOODS_QUALITY_COLOR_PREFIX[gUI.Rcm.CurDistributeLevel], "", true, childmenu2)
      else
        self:Register(menu, "拾取  " .. gUI_GOODS_DISTRIBUTE_MODE[gUI.Rcm.CurDistributeMode + 1], _Rcm_total)
        item = menu:InsertItemEx("分配  " .. gUI_GOODS_DISTRIBUTE_QUALITY[gUI.Rcm.CurDistributeLevel + 1], -1, gUI_GOODS_QUALITY_COLOR_PREFIX[gUI.Rcm.CurDistributeLevel], "", true)
        item:SetUserData(_Rcm_total)
      end
      self:Register(menu, "离开队伍", 10)
    end
    local childmenu3 = self.child_menu3
    childmenu3:Clear()
    for i = 1, #gUI_GOODS_PK_MODE do
      self:Register(childmenu3, gUI_GOODS_PK_MODE[i], _Rcm_pkmode + i - 1)
    end
    childmenu3:SetProperty("CheckedMenuItem", tostring(gUI.Rcm.CurPKMode))
    menu:InsertItem("PK  " .. gUI_GOODS_PK_MODE[gUI.Rcm.CurPKMode + 1], -1, childmenu3, "")
    local bShowResetDun = false
    if has_team then
      if is_leader then
        bShowResetDun = true
      end
    else
      bShowResetDun = true
    end
    if bShowResetDun == true then
      self:Register(menu, "重置副本", _Rcm_instance + 1)
    end
    self:Register(menu, "查看信息", 6)
  end
}
gUI.Rcm.Menus.ON_SELF_PK_MODE = {
  InitMenu = function(self, ...)
    local menu = self.menu
    menu:Clear()
    for i = 1, #gUI_GOODS_PK_MODE do
      self:Register(menu, gUI_GOODS_PK_MODE[i], _Rcm_pkmode + i - 1)
    end
  end
}
gUI.Rcm.Menus.ON_TEAMMATE = {
  InitMenu = function(self, mate_index, mate_name)
    local menu = self.menu
    menu:Clear()
    self.handlers.mate_index = mate_index
    self.handlers.name = mate_name
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
    local _, _, _, guild_level = GetGuildBaseInfo()
    local _, _, level, _, _, _, _, _, _, _, _, isoffline, _, object_id, _, _, _, _, _, mate_guid, guild_id_target = GetTeamMemberInfo(mate_name, true)
    self.handlers.object_id = object_id
    self.handlers.guid = mate_guid
    menu:InsertItemEx(mate_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    local distance = GetCharacterDistance(object_id)
    if not isoffline then
      if object_id ~= -1 and distance and distance < 10 then
        self:Register(menu, "跟随", 2)
      end
      if object_id ~= -1 and distance and distance < 30 then
        self:Register(menu, "查看装备符文", 5)
        self:Register(menu, "交易", 12)
      end
      self:Register(menu, "悄悄话", 14)
      local info_f = GetRelationInfo(mate_guid, gUI_RELATION_TYPE.friend)
      if info_f ~= nil then
        self:Register(menu, "发送消息", _Rcm_relation + 5)
      else
        self:Register(menu, "加为好友", _Rcm_relation)
      end
    end
    if not isoffline then
      self:Register(menu, "查看信息", 4)
      if is_leader then
        self:Register(menu, "设为队长", 8)
      end
    end
    if is_leader then
      self:Register(menu, "移出队伍", 9)
    end
    if not isoffline then
      if guild_pos >= gUI_GUILD_POSITION.minister and guild_id_target < 0 and level >= gUI_GUILD_INVITE_LEVEL then
        if guild_level >= 1 then
          self:Register(menu, "邀请加入帮会", 11)
        elseif guild_level == 0 then
          self:Register(menu, "邀请帮会招募", 11)
        end
      end
      if guild_pos <= 0 and guild_id_target > 0 and level_self and level_self >= gUI_GUILD_INVITE_LEVEL then
        self:Register(menu, "申请加入帮会", 18)
      end
      if object_id ~= -1 and distance and distance < 10 then
        self:Register(menu, "邀请切磋", 13)
      end
    end
    local info_b = GetRelationInfo(mate_guid, gUI_RELATION_TYPE.blacklist)
    if info_b == nil then
      self:Register(menu, "添加屏蔽", _Rcm_relation + 1)
    end
  end
}
gUI.Rcm.Menus.ON_PET = {
  InitMenu = function(self, target_name, guid, name_owner)
    local menu = self.menu
    menu:Clear()
    self.handlers.guid = guid
    self.handlers.name = target_name
    local nameOwner = ""
    if name_owner == nil or name_owner == "" then
      nameOwner = "未知目标"
    else
      nameOwner = name_owner
    end
    menu:InsertItemEx(nameOwner .. "的" .. target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "查看仙灵", 21)
  end
}
gUI.Rcm.Menus.ON_TARGET = {
  InitMenu = function(self, target_name, object_id, lv)
    local menu = self.menu
    menu:Clear()
    self.handlers.name = target_name
    self.handlers.lv = lv
    self.handlers.object_id = object_id
    self.handlers.objTable_id = 0
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos, multi_mount, IsGM = GetPlaySelfProp(6)
    local _, _, _, guild_level = GetGuildBaseInfo()
    local is_target_teammember = false
    local mate_index = 0
    local _, _, guild_id_target, level, target_hasTeam, guid_target, target_hasGuild = GetTargetEaseChangeInfo(0)
    self.handlers.guid = guid_target
    while true do
      local _, _, _, _, _, _, _, mem_is_leader, _, _, _, _, _, mem_object_id = GetTeamMemberInfo(mate_index, false)
      if mem_object_id == nil then
        break
      end
      if object_id == mem_object_id then
        is_target_teammember = true
      end
      mate_index = mate_index + 1
    end
    self.handlers.guild_id_target = guild_id_target
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    local distance = GetCharacterDistance(object_id)
    if distance and distance < 30 then
      self:Register(menu, "跟随", 2)
      self:Register(menu, "查看装备符文", 5)
    end
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(guid_target, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    if level_self >= gUI_PK_Level and level >= gUI_PK_Level then
      self:Register(menu, "使用天诛令", 20)
    end
    self:Register(menu, "查看信息", 3)
    if distance and distance < 10 then
      self:Register(menu, "交易", 12)
    end
    if not is_target_teammember then
      if not has_team and target_hasTeam then
        self:Register(menu, "申请入队", 7)
      else
        self:Register(menu, "邀请组队", 7)
      end
    end
    if is_target_teammember and is_leader then
      self:Register(menu, "移出队伍", 9)
    end
    if multi_mount == 1 then
      if not IsOnSameMount() then
        self:Register(menu, "邀请同乘", 15)
      else
        self:Register(menu, "请下坐骑", 16)
      end
    end
    if guild_pos >= gUI_GUILD_POSITION.minister and not target_hasGuild and level and level >= gUI_GUILD_INVITE_LEVEL then
      if guild_level >= 1 then
        self:Register(menu, "邀请加入帮会", 11)
      elseif guild_level == 0 then
        self:Register(menu, "邀请帮会招募", 11)
      end
    end
    if guild_pos <= 0 and target_hasGuild and level_self and level_self >= gUI_GUILD_INVITE_LEVEL then
      self:Register(menu, "申请加入帮会", 18)
    end
    if distance and distance < 10 and level_self >= gUI_PK_Deul and level >= gUI_PK_Deul then
      self:Register(menu, "邀请切磋", 13)
    end
    local info_b = GetRelationInfo(guid_target, gUI_RELATION_TYPE.blacklist)
    if info_b == nil then
      self:Register(menu, "添加屏蔽", _Rcm_relation + 1)
    end
    self:Register(menu, "查看仙灵", 21)
    if IsGM == 1 then
      self:Register(menu, "删除NPC目标", _Rcm_GM, colorGold)
      self:Register(menu, "查看NPC信息", _Rcm_GM + 1, colorGold)
      self:Register(menu, "测试距离", _Rcm_GM + 2, colorGold)
      self:Register(menu, "查询掉落", _Rcm_GM + 3, colorGold)
      self:Register(menu, "查看角色信息", _Rcm_GM + 4, colorGold)
      self:Register(menu, "踢下线", _Rcm_GM + 5, colorGold)
      self:Register(menu, "（调试日志）", 23, colorRed)
    end
  end
}
gUI.Rcm.Menus.ON_TEAM_NEAR = {
  InitMenu = function(self, target_name, guid)
    local menu = self.menu
    menu:Clear()
    self.handlers.name = target_name
    self.handlers.guid = guid
    self.handlers.object_id = -1
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    local _, has_team = GetPlaySelfProp(6)
    if not has_team then
      self:Register(menu, "申请入队", _Rcm_relation + 2)
    end
  end
}
gUI.Rcm.Menus.ON_TEAM_NEAR1 = {
  InitMenu = function(self, target_name, guid)
    local menu = self.menu
    menu:Clear()
    self.handlers.name = target_name
    self.handlers.guid = guid
    self.handlers.object_id = -1
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    local _, has_team = GetPlaySelfProp(6)
    if not has_team then
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    else
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    end
  end
}
gUI.Rcm.Menus.ON_TEAM_NEAR2 = {
  InitMenu = function(self, target_name, guid)
    local menu = self.menu
    menu:Clear()
    self.handlers.name = target_name
    self.handlers.guid = guid
    self.handlers.object_id = -1
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
  end
}
gUI.Rcm.Menus.ON_RANKINFO = {
  InitMenu = function(self, target_name, guid, lv)
    local menu = self.menu
    menu:Clear()
    self.handlers.name = target_name
    self.handlers.lv = lv
    self.handlers.object_id = -1
    self.handlers.objTable_id = 0
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos, multi_mount, IsGM = GetPlaySelfProp(6)
    self.handlers.guid = guid
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    if level_self >= gUI_PK_Level then
      self:Register(menu, "使用天诛令", 20)
    end
    local _, name, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(target_name, true)
    if name == nil then
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    end
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    local info_b = GetRelationInfo(guid, gUI_RELATION_TYPE.blacklist)
    if info_b == nil then
      self:Register(menu, "添加屏蔽", _Rcm_relation + 1)
    end
  end
}
gUI.Rcm.Menus.ON_NPC = {
  InitMenu = function(self, target_name, object_id, lv)
    local menu = self.menu
    menu:Clear()
    self.handlers.name = target_name
    self.handlers.lv = lv
    self.handlers.object_id = object_id
    self.handlers.objTable_id = GetMonsterTableId(tonumber(object_id))
    local _, has_team, is_leader, _, guild_pos, multi_mount, IsGM = GetPlaySelfProp(6)
    if IsGM == 1 then
      menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
      self:Register(menu, "删除NPC目标", _Rcm_GM, colorGold)
      self:Register(menu, "查看NPC信息", _Rcm_GM + 1, colorGold)
      self:Register(menu, "测试距离", _Rcm_GM + 2, colorGold)
      self:Register(menu, "查询掉落", _Rcm_GM + 3, colorGold)
      self:Register(menu, "查看角色信息", _Rcm_GM + 4, colorGold)
      self:Register(menu, "踢下线", _Rcm_GM + 5, colorGold)
      self:Register(menu, "（调试日志）", 23, colorRed)
    end
  end
}
gUI.Rcm.Menus.ON_SPEAKER = {
  InitMenu = function(self, target_name, target_guid)
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self.handlers.name = target_name
    self.handlers.guid = target_guid
    self.handlers.object_id = -1
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
    local _, _, _, guild_level = GetGuildBaseInfo()
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(target_guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    if level_self >= gUI_PK_Level then
      self:Register(menu, "使用天诛令", 26)
    end
    self:Register(menu, "查看信息", 19)
    local _, name, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(target_name, true)
    if name == nil then
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    end
    if guild_pos >= gUI_GUILD_POSITION.minister then
      if guild_level >= 1 then
        self:Register(menu, "邀请加入帮会", 11)
      elseif guild_level == 0 then
        self:Register(menu, "邀请帮会招募", 11)
      end
    end
    local info_b = GetRelationInfo(target_guid, gUI_RELATION_TYPE.blacklist)
    if info_b == nil then
      self:Register(menu, "添加屏蔽", _Rcm_relation + 1)
    end
  end
}
gUI.Rcm.Menus.ON_SPEAKER_INFOS = {
  InitMenu = function(self, target_name, target_guid)
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self.handlers.name = target_name
    self.handlers.guid = target_guid
    self.handlers.object_id = -1
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
    local _, _, _, guild_level = GetGuildBaseInfo()
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(target_guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    if level_self >= gUI_PK_Level then
      self:Register(menu, "使用天诛令", 26)
    end
    self:Register(menu, "查看信息", 19)
  end
}
gUI.Rcm.Menus.ON_SPEAKER_STALL = {
  InitMenu = function(self, target_name, target_guid)
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self.handlers.name = target_name
    self.handlers.guid = target_guid
    self.handlers.object_id = -1
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local info_f = GetRelationInfo(target_guid, gUI_RELATION_TYPE.friend)
    if info_f ~= nil then
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    else
      self:Register(menu, "加为好友", _Rcm_relation)
    end
    self:Register(menu, "查看信息", 19)
    local _, name, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(target_name, true)
    if name == nil then
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    end
    local info_b = GetRelationInfo(target_guid, gUI_RELATION_TYPE.blacklist)
    if info_b == nil then
      self:Register(menu, "添加屏蔽", _Rcm_relation + 1)
    end
  end
}
gUI.Rcm.Menus.ON_SPEAKER_1V1 = {
  InitMenu = function(self, target_name, target_guid)
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self.handlers.name = target_name
    self.handlers.guid = target_guid
    self.handlers.object_id = -1
    local _, _, _, _, level_self = GetPlaySelfProp(4)
    local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "悄悄话", 14)
    local _, name, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(target_name, true)
    if name == nil then
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    end
  end
}
gUI.Rcm.Menus.ON_SPEAKER_GROUP = {
  InitMenu = function(self, target_name, target_guid)
    local _, _, _, _, _, _, _, myGuid = GetMyPlayerStaticInfo()
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self.handlers.name = target_name
    self.handlers.guid = target_guid
    self.handlers.object_id = -1
    if myGuid == target_guid then
      self:Register(menu, "复制名称", 1)
    else
      local _, _, _, _, level_self = GetPlaySelfProp(4)
      local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
      self:Register(menu, "复制名称", 1)
      self:Register(menu, "悄悄话", 14)
      local info_f = GetRelationInfo(target_guid, gUI_RELATION_TYPE.friend)
      if info_f ~= nil then
        self:Register(menu, "发送消息", _Rcm_relation + 5)
      else
        self:Register(menu, "加为好友", _Rcm_relation)
      end
      local _, name, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(target_name, true)
      if name == nil then
        self:Register(menu, "邀请组队", _Rcm_relation + 2)
      end
      local info_b = GetRelationInfo(target_guid, gUI_RELATION_TYPE.blacklist)
      if info_b == nil then
        self:Register(menu, "添加屏蔽", _Rcm_relation + 1)
      end
    end
  end
}
gUI.Rcm.Menus.ON_FRIENDS = {
  InitMenu = function(self, guid, target_name)
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self.handlers.guid = guid
    self.handlers.name = target_name
    self.handlers.object_id = -1
    local _, has_team, is_leader, _, guild_pos = GetPlaySelfProp(6)
    local _, _, _, guild_level = GetGuildBaseInfo()
    local isOnline, _, _, level = GetFriendDetailInfo(guid)
    self:Register(menu, "复制名称", 1)
    if isOnline then
      self:Register(menu, "悄悄话", 14)
      self:Register(menu, "发送消息", _Rcm_relation + 5)
    end
    local _, name_tmp, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(target_name, true)
    if name_tmp == nil and isOnline then
      self:Register(menu, "邀请组队", _Rcm_relation + 2)
    end
    if guild_pos >= gUI_GUILD_POSITION.minister and level >= gUI_GUILD_INVITE_LEVEL then
      if guild_level >= 1 then
        self:Register(menu, "邀请加入帮会", 11)
      elseif guild_level == 0 then
        self:Register(menu, "邀请帮会招募", 11)
      end
    end
    local isOnline, headid, name, _, job, guild, _, _, delTime, secene, _, friPoint = GetFriendDetailInfo(guid)
    if delTime == 0 then
      self:Register(menu, "删除好友", _Rcm_relation + 3)
    end
    self:Register(menu, "添加屏蔽", _Rcm_relation + 11)
  end
}
gUI.Rcm.Menus.ON_BLACK = {
  InitMenu = function(self, guid, target_name)
    local menu = self.menu
    menu:Clear()
    self.handlers.guid = guid
    self.handlers.name = target_name
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    self:Register(menu, "取消屏蔽", _Rcm_relation + 9)
  end
}
gUI.Rcm.Menus.ON_GUILD = {
  InitMenu = function(self, guid, targetGuild_name, hostName)
    local menu = self.menu
    menu:Clear()
    self.handlers.guid = guid
    self.handlers.name = targetGuild_name
    self.handlers.hostName = hostName
    menu:InsertItemEx(targetGuild_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制帮会名称", 1)
    self:Register(menu, "复制帮主名称", _Rcm_Guild + 1)
  end
}
gUI.Rcm.Menus.ON_ENEMY = {
  InitMenu = function(self, guid, target_name)
    local menu = self.menu
    menu:Clear()
    self.handlers.guid = guid
    self.handlers.name = target_name
    menu:InsertItemEx(target_name, -1, "0xFFFFFF00", "", false)
    self:Register(menu, "复制名称", 1)
    local isOnline = true
    if isOnline then
      self:Register(menu, "悄悄话", 14)
      self:Register(menu, "查看位置", _Rcm_relation + 10)
      self:Register(menu, "使用天诛令", 26)
    end
    local info_b = GetRelationInfo(guid, gUI_RELATION_TYPE.blacklist)
    if info_b == nil then
      self:Register(menu, "添加屏蔽", _Rcm_relation + 11)
    end
  end
}
gUI.Rcm.Menus.ON_GUILD_MEMBER = {
  InitMenu = function(self, guild_member_name, guild_member_guid, is_online)
    local menu = self.menu
    menu:Clear()
    self.handlers.guid = guild_member_guid
    self.handlers.name = guild_member_name
    self.handlers.object_id = -1
    menu:InsertItemEx(guild_member_name, -1, "0xFFFFFF00", "", false)
    local _, has_team, is_leader = GetPlaySelfProp(6)
    self:Register(menu, "复制名称", 1)
    if is_online then
      self:Register(menu, "悄悄话", 14)
      local info_f = GetRelationInfo(guild_member_guid, gUI_RELATION_TYPE.friend)
      if info_f == nil then
        self:Register(menu, "加为好友", _Rcm_relation)
      end
      local _, name_tmp, _, _, _, _, _, _, _, _, _, _ = GetTeamMemberInfo(guild_member_name, true)
      if name_tmp == nil then
        self:Register(menu, "邀请组队", _Rcm_relation + 2)
      end
    end
    local my_pos, _ = GetGuildAuthority()
    local _, _, target_pos, _ = GetGuildMemberInfo(guild_member_guid)
    if gUI_GUILD_POSITION.viceleader == target_pos and my_pos == gUI_GUILD_POSITION.leader then
      self:Register(menu, "帮主转让", 24)
    end
    if my_pos > target_pos then
      self:Register(menu, "开除帮会", 25)
    end
  end
}
gUI.Rcm.Menus.ON_SPEAK_CHANNEL = {
  InitMenu = function(self, ...)
    local menu = self.menu
    menu:Clear()
    menu:InsertItemEx("频道选择   ", -1, "0xFFFFFF00", "kaiti_10", false)
    local item = menu:InsertItemEx("当前 /s", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Normal], "", true)
    item:SetUserData(_Rcm_chat)
    item = menu:InsertItemEx("世界 /w", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.World], "", true)
    item:SetUserData(_Rcm_chat + 1)
    item = menu:InsertItemEx("队伍 /t", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Team], "", true)
    item:SetUserData(_Rcm_chat + 2)
    item = menu:InsertItemEx("联盟 /l", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Alliance], "", true)
    item:SetUserData(_Rcm_chat + 3)
    item = menu:InsertItemEx("帮会 /g", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Guild], "", true)
    item:SetUserData(_Rcm_chat + 4)
    item = menu:InsertItemEx("密语 /m", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Tell], "", true)
    item:SetUserData(_Rcm_chat + 5)
    item = menu:InsertItemEx("跨服 /k", -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Map], "", true)
    item:SetUserData(_Rcm_chat + 6)
    for i = 0, 4 do
      if Lua_Chat_GetLastWishperName(i) ~= "" then
        item = menu:InsertItemEx(string.format("密语 /%s ", Lua_Chat_GetLastWishperName(i)), -1, gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Tell], "", true)
        item:SetUserData(_Rcm_chat + 7 + i)
      end
    end
    item = menu:InsertItemEx("取消", -1, "0xFFFFFFFF", "", true)
    item:SetUserData(_Rcm_total)
    menu:SetAnchorPosition(1)
  end
}
for k, v in pairs(gUI.Rcm.Menus) do
  if type(v) == "table" then
    setmetatable(v, gUI.Rcm.Menus)
  end
end
function Lua_Rcm_SetDistributeMode(b)
  gUI.Rcm.CurDistributeMode = b
end
function Lua_Rcm_SetDistributeLevel(b)
  gUI.Rcm.CurDistributeLevel = b
end
function Lua_Rcm_SetPKMode(b)
  gUI.Rcm.CurPKMode = b
end
function Lua_MenuShow(which, ...)
  gUI.Rcm.Menus.cur_menu = gUI.Rcm.Menus[which]
  if gUI.Rcm.Menus.menu:IsVisible() and _Rcm_strLastMenu == which then
    gUI.Rcm.Menus.menu:SetProperty("Visible", "false")
    _Rcm_strLastMenu = ""
  else
    gUI.Rcm.Menus.cur_menu:InitMenu(...)
    gUI.Rcm.Menus.menu:SetProperty("Visible", "true")
    _Rcm_strLastMenu = which
  end
end
function Lua_MenuHide(which)
  if gUI.Rcm.Menus.menu:IsVisible() and _Rcm_strLastMenu == which then
    gUI.Rcm.Menus.menu:SetProperty("Visible", "false")
    _Rcm_strLastMenu = ""
  end
end
function RightClickMeun_HideAll()
  if gUI.Rcm.Menus.menu:IsVisible() then
    gUI.Rcm.Menus.menu:SetProperty("Visible", "false")
    _Rcm_strLastMenu = ""
  end
end
function UI_Rcm_SelectChanged(msg)
  gUI.Rcm.Menus.cur_menu:OnSelected(msg)
end
function UI_Rcm_SelectSpeakChannel(msg)
  local index = _Chat_GetChatChannel()
  UI_Rcm_ShowChannelList(index)
end
function UI_ChanSetCheckBtnState(parent, index, CurrIndex, BtnShow, CheckBtnShow, btnText, CurrChannel)
  if parent then
    parent:SetCustomUserData(1, index)
    parent:SetCustomUserData(2, CurrIndex)
    parent:SetCustomUserData(3, CurrChannel)
    parent:SetVisible(BtnShow)
    local tempCheckBtn = WindowToCheckButton(parent:GetGrandChild("ChanTemp_cbtn"))
    local flag = 0
    if index < gUI_CHANNEL_STR.Map then
      flag = GetShowChannelConfig(index - 1, CurrIndex)
    else
      flag = GetShowChannelConfig(index - 2, CurrIndex)
    end
    UI_SetFilter_State(flag, tempCheckBtn)
    if flag == 1 or flag == 4 then
      tempCheckBtn:SetVisible(false)
    else
      tempCheckBtn:SetVisible(CheckBtnShow)
    end
    local tempBtn = parent:GetGrandChild("ChanTemp_btn")
    tempBtn:SetProperty("Text", tostring(btnText))
    tempBtn:SetVisible(BtnShow)
  end
end
function UI_Rcm_ShowChannelList(index)
  gUI.Chat.ChanContainer:DeleteAll()
  local iFalg = 0
  gUI.Chat.InterfaceTem:SetProperty("Visible", "true")
  gUI.Chat.ChanInterface:SetProperty("Visible", "true")
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, -1, -1, false, false, "", -1)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 0, true, true, "{#C^0xFFffffff^当前 /s}", 1)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 1, true, true, "{#C^0xFF54cdff^世界 /w}", 2)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 3, true, true, "{#C^0xFFffdc89^队伍 /t}", 4)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 5, true, true, "{#C^0xFF00ffd8^联盟 /l}", 6)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 4, true, true, "{#C^0xFF24fc3d^帮会 /g}", 5)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 2, true, true, "{#C^0xFFf3a5ff^密语 /m}", 3)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 10, true, true, "{#C^0xFFc1ff4a^好友 /f}", 7)
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, index, 11, true, true, "{#C^0xFFFF7A91^战场 /b}", 9)
  for i = 0, 4 do
    if Lua_Chat_GetLastWishperName(i) ~= "" then
      local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
      local strBtnText = string.format("{#C^0xFFcf6be4^%s}", Lua_Chat_GetLastWishperName(i))
      UI_ChanSetCheckBtnState(temp, -1, 20 + i, true, false, strBtnText, 20 + i)
      iFalg = iFalg + 1
    end
  end
  local temp = gUI.Chat.ChanContainer:Insert(-1, gUI.Chat.ChanListTemp)
  UI_ChanSetCheckBtnState(temp, -1, -1, true, false, "{#C^gUI_CHATMSG_COLOR[gUI_CHANNEL_STR.Normal]^取消 }", -1)
  iFalg = iFalg + 8 + 2
  local height = 24 * iFalg
  gUI.Chat.ChanContainer:SetProperty("Height", "p" .. tostring(height))
  local top = gUI.Chat.ChannelBtn:GetWndRect():get_top() - gUI.Chat.ChanContainer:GetWndRect():get_height()
  gUI.Chat.ChanInterface:SetTop(top)
end
function UI_ChanFilte_SelClick(msg)
  local wnd = msg:get_window():GetParent()
  local index = wnd:GetCustomUserData(1)
  local CurrIndex = wnd:GetCustomUserData(2)
  UI_Rcm_SettingChannal(WindowToCheckButton(msg:get_window()), index, CurrIndex)
  gUI.Chat.Filter:SetProperty("Visible", "false")
end
function UI_Rcm_SettingChannal(wnd, index, CurrIndex)
  UI_SaveConfigs(wnd, CurrIndex, index - 1)
  SaveToConfig()
end
function UI_ChanFiltem_BtnClick(msg)
  local wnd = msg:get_window():GetParent()
  local index = wnd:GetCustomUserData(3)
  if index > 0 and index <= gUI_CHANNEL_STR.BattleGrounp then
    local RealIndex = gUI_CHANNEL_INDEX_RELATION1[index]
    Lua_Chat_SelectChatChannel(RealIndex)
  elseif index >= 20 and index <= 24 then
    local text = msg:get_window():GetProperty("Text")
    text = GetParsedStr(text)
    Lua_Chat_SelectChatChannel(gUI_CHANNEL_STR1.Tell, text)
  end
  local wnd = WindowSys_Instance:GetWindow("ChanFiltem.ChanFiltem_bg")
  wnd:SetProperty("Visible", "false")
end
function GetChannelIndexByText(btnText)
  return gUI_NAME_CHANNEL[btnText]
end
function UI_Rcm_OnTeammateRClick(msg)
  local wnd = msg:get_window():GetParent()
  local index = tonumber(string.sub(wnd:GetProperty("WindowName"), -1, -1))
  local name = wnd:GetChildByName("name"):GetProperty("Text")
  Lua_MenuShow("ON_TEAMMATE", index, name)
end
function UI_Rcm_ListMenu(msg)
  if gUI.Rcm.CurMenuListName == "FriendSearchList" then
    UI_Fri_SearchListClick(msg)
  elseif gUI.Rcm.CurMenuListName == "ChatExList" then
    UI_Cp2p_ChooseExList(msg)
  elseif gUI.Rcm.CurMenuListName == "ChatWaiting" then
    UI_Cp2p_ChooseWaitingWnd(msg)
  end
end
function Script_Rcm_OnLoad()
  local m = gUI.Rcm.Menus
  m.root = WindowSys_Instance:GetWindow("MainMenu")
  m.menu = WindowToRightClickMenu(m.root:GetChildByName("RightClickMenu"))
  m.child_menu = WindowToRightClickMenu(m.root:GetChildByName("ChildMenu"))
  m.child_menu2 = WindowToRightClickMenu(m.root:GetChildByName("ChildMenu2"))
  m.child_menu3 = WindowToRightClickMenu(m.root:GetChildByName("ChildMenu3"))
  gUI.Rcm.CurMenuListName = nil
  gUI.Rcm.CurDistributeMode = 1
  gUI.Rcm.CurDistributeLevel = 1
  gUI.Rcm.CurPKMode = 1
end
function Script_Rcm_OnHide()
  gUI.Rcm.Menus.menu:SetProperty("Visible", "false")
end

