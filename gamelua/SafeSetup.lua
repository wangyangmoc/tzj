if gUI and not gUI.SafeS then
  gUI.SafeS = {}
end
function _CheckTwoPass(pass1, pass2)
  if string.len(pass1) == 0 or string.len(pass2) == 0 then
    Lua_Chat_ShowOSD("SAFE_PASS_CANNOTENMTY")
    return false
  elseif string.len(pass1) < 6 then
    Lua_Chat_ShowOSD("SAFE_PASS_SHORT")
    return false
  elseif string.len(pass1) > 10 then
    Lua_Chat_ShowOSD("SAFE_PASS_LONG")
    return false
  elseif pass1 ~= pass2 then
    Lua_Chat_ShowOSD("SAFE_PASS_DIFF")
    return false
  else
    return true
  end
end
function _CheckOnePass(pass)
  if string.len(pass) == 0 then
    Lua_Chat_ShowOSD("SAFE_PASS_CANNOTENMTY")
    return false
  elseif string.len(pass) < 6 then
    Lua_Chat_ShowOSD("SAFE_PASS_SHORT")
    return false
  elseif string.len(pass) > 10 then
    Lua_Chat_ShowOSD("SAFE_PASS_LONG")
    return false
  else
    return true
  end
end
function _SafeSetup_GetCurMode()
  if gUI.SafeS.Set:IsVisible() then
    return 0
  elseif gUI.SafeS.Modify:IsVisible() then
    return 1
  elseif gUI.SafeS.Unlock:IsVisible() then
    return 2
  end
end
function _SafeSetup_RefreshPad()
  local aNum = {}
  for i = 0, 9 do
    aNum[i + 1] = tostring(i)
  end
  local leftSoftRandKeyNumber = 10
  for i = 1, leftSoftRandKeyNumber do
    local rdIndex = math.random(1, leftSoftRandKeyNumber - i + 1)
    gUI.SafeS.NumericPad:GetChildByName("Button" .. i .. "_btn"):SetProperty("Text", aNum[rdIndex])
    table.remove(aNum, rdIndex)
  end
end
function _SafeSetup_GetActiveEditBox()
  if gUI.SafeS.SetEditNew:IsKeyboardCaptureWindow() then
    return gUI.SafeS.SetEditNew
  elseif gUI.SafeS.SetEditNew1:IsKeyboardCaptureWindow() then
    return gUI.SafeS.SetEditNew1
  elseif gUI.SafeS.ModifyEditOld:IsKeyboardCaptureWindow() then
    return gUI.SafeS.ModifyEditOld
  elseif gUI.SafeS.ModifyEditNew:IsKeyboardCaptureWindow() then
    return gUI.SafeS.ModifyEditNew
  elseif gUI.SafeS.ModifyEditNew1:IsKeyboardCaptureWindow() then
    return gUI.SafeS.ModifyEditNew1
  elseif gUI.SafeS.UnlockEdit:IsKeyboardCaptureWindow() then
    return gUI.SafeS.UnlockEdit
  end
  return nil
end
function _SafeSetup_SetLockBagItem(globleSlot, bLock)
  local winBag, index = Lua_Bag_GetBagWinAndItemId(globleSlot)
  winBag:SetItemsLock(index, 1, bLock)
end
function _SafeSetup_ShowModeUI(mode)
  gUI.SafeS.Set:SetProperty("Visible", "false")
  gUI.SafeS.Modify:SetProperty("Visible", "false")
  gUI.SafeS.Unlock:SetProperty("Visible", "false")
  gUI.SafeS.SetEditNew:SetProperty("Text", "")
  gUI.SafeS.SetEditNew1:SetProperty("Text", "")
  gUI.SafeS.ModifyEditOld:SetProperty("Text", "")
  gUI.SafeS.ModifyEditNew:SetProperty("Text", "")
  gUI.SafeS.ModifyEditNew1:SetProperty("Text", "")
  gUI.SafeS.UnlockEdit:SetProperty("Text", "")
  if mode == 0 then
    gUI.SafeS.rootTitle:SetProperty("Text", "设置密码")
    gUI.SafeS.Set:SetProperty("Visible", "true")
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.SafeS.SetEditNew)
  elseif mode == 1 then
    gUI.SafeS.rootTitle:SetProperty("Text", "修改密码")
    gUI.SafeS.Modify:SetProperty("Visible", "true")
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.SafeS.ModifyEditOld)
  elseif mode == 2 then
    gUI.SafeS.rootTitle:SetProperty("Text", "解锁")
    gUI.SafeS.Unlock:SetProperty("Visible", "true")
    WindowSys_Instance:SetKeyboardCaptureWindow(gUI.SafeS.UnlockEdit)
  end
  if HasMinorPassSet() then
    if IsInDeleteMinorPass() then
      gUI.SafeS.ClearPassBtn:SetProperty("Text", "取消遗忘")
      gUI.SafeS.ClearPassBtn1:SetProperty("Text", "取消遗忘")
    else
      gUI.SafeS.ClearPassBtn:SetProperty("Text", "遗忘密码")
      gUI.SafeS.ClearPassBtn1:SetProperty("Text", "遗忘密码")
    end
  else
  end
end
function UI_SafeSetup_SetSecondPassOk()
  local pass1 = gUI.SafeS.SetEditNew:GetProperty("PasswordText")
  local pass2 = gUI.SafeS.SetEditNew1:GetProperty("PasswordText")
  if _CheckTwoPass(pass1, pass2) then
    SetMinorPass(pass1)
    UI_SafeSetup_Close()
  end
end
function UI_SafeSetup_ModifySecondPassOk()
  local pass1 = gUI.SafeS.ModifyEditOld:GetProperty("PasswordText")
  local pass2 = gUI.SafeS.ModifyEditNew:GetProperty("PasswordText")
  local pass3 = gUI.SafeS.ModifyEditNew1:GetProperty("PasswordText")
  if _CheckTwoPass(pass2, pass3) then
    ModifyMinorPass(pass1, pass2)
    UI_SafeSetup_Close()
  end
end
function UI_SafeSetup_CheckSecondPassOk()
  local pass1 = gUI.SafeS.UnlockEdit:GetProperty("PasswordText")
  if _CheckOnePass(pass1) then
    CheckMinorPass(pass1)
    UI_SafeSetup_Close()
  end
end
function UI_SafeSetup_CancelSecondPassOk()
  if IsInDeleteMinorPass() then
    CancelDeleteMinorPass()
    gUI.SafeS.ClearPassBtn:SetProperty("Text", "遗忘密码")
  else
    DeleteMinorPass()
    gUI.SafeS.ClearPassBtn:SetProperty("Text", "取消遗忘")
  end
end
function UI_SafeSetup_KeyPress(msg)
  local editbox = _SafeSetup_GetActiveEditBox()
  if editbox == nil then
    return
  end
  local pass = editbox:GetProperty("PasswordText")
  if string.len(pass) < 10 then
    pass = pass .. msg:get_window():GetProperty("Text")
    editbox:SetProperty("Text", pass)
  end
end
function UI_SafeSetup_Close(msg)
  gUI.SafeS.root:SetProperty("Visible", "false")
  Lua_Fri_SetFoucsToWorld()
end
function UI_SafeSetup_KeyOk(msg)
  local mode = _SafeSetup_GetCurMode()
  if mode == 0 then
    UI_SafeSetup_SetSecondPassOk()
  elseif mode == 1 then
    UI_SafeSetup_ModifySecondPassOk()
  elseif mode == 2 then
    UI_SafeSetup_CheckSecondPassOk()
  end
end
function UI_SafeSetup_KeyBack(msg)
  local editbox = _SafeSetup_GetActiveEditBox()
  if editbox == nil then
    return
  end
  local pass = editbox:GetProperty("PasswordText")
  if string.len(pass) > 0 then
    pass = string.sub(pass, 1, string.len(pass) - 1)
    editbox:SetProperty("Text", pass)
  end
end
function UI_Secect_Click(msg)
  _OnSafeSetup_PopNumericPad(0)
end
function UI_Secect_Click1(msg)
  _OnSafeSetup_PopNumericPad(1)
end
function _OnSafeSetup_PopNumericPad(mode)
  _SafeSetup_RefreshPad()
  gUI.SafeS.root:SetProperty("Visible", "true")
  if mode == 1 then
    _SafeSetup_ShowModeUI(2)
  elseif HasMinorPassSet() then
    _SafeSetup_ShowModeUI(1)
  else
    _SafeSetup_ShowModeUI(0)
  end
end
function _OnSafeSetup_ProtectStart()
  local ptime = WindowSys_Instance:GetWindow("ProtectTime")
  local remain_time = GetRemainProtectTime()
  if remain_time <= 0 then
    ptime:SetVisible(false)
    return
  end
  AddTimerEvent(1, "protect_time", function()
    if remain_time > 0 then
      str = string.format("当前剩余保护时间：%d分%d秒", math.floor(remain_time / 60), remain_time % 60)
      ptime:SetProperty("Text", str)
      ptime:SetProperty("Visible", "true")
      remain_time = remain_time - 1
    else
      ptime:SetProperty("Visible", "false")
      DelTimerEvent(1, "protect_time")
    end
  end)
end
function _OnSafeSetup_RefreshUI()
end
function Script_SafeSetup_OnLoad()
  gUI.SafeS.root = WindowSys_Instance:GetWindow("Password")
  gUI.SafeS.rootTitle = WindowSys_Instance:GetWindow("Password.Password_bg.Title_dlab")
  gUI.SafeS.Set = WindowSys_Instance:GetWindow("Password.Password_bg.SetPassword_bg")
  gUI.SafeS.SetEditNew = WindowSys_Instance:GetWindow("Password.Password_bg.SetPassword_bg.Picture1_bg.EditBox1_ebox")
  gUI.SafeS.SetEditNew1 = WindowSys_Instance:GetWindow("Password.Password_bg.SetPassword_bg.Picture2_bg.EditBox1_ebox")
  gUI.SafeS.Modify = WindowSys_Instance:GetWindow("Password.Password_bg.ChangePassword_bg")
  gUI.SafeS.ModifyEditOld = WindowSys_Instance:GetWindow("Password.Password_bg.ChangePassword_bg.Picture1_bg.EditBox1_ebox")
  gUI.SafeS.ModifyEditNew = WindowSys_Instance:GetWindow("Password.Password_bg.ChangePassword_bg.Picture2_bg.EditBox1_ebox")
  gUI.SafeS.ModifyEditNew1 = WindowSys_Instance:GetWindow("Password.Password_bg.ChangePassword_bg.Picture3_bg.EditBox1_ebox")
  gUI.SafeS.Unlock = WindowSys_Instance:GetWindow("Password.Password_bg.ClearPassword_bg")
  gUI.SafeS.UnlockEdit = WindowSys_Instance:GetWindow("Password.Password_bg.ClearPassword_bg.Picture1_bg.EditBox1_ebox")
  gUI.SafeS.NumericPad = WindowSys_Instance:GetWindow("Password.Password_bg.Picture3_bg")
  gUI.SafeS.ClearPassBtn = WindowSys_Instance:GetWindow("Password.Password_bg.ChangePassword_bg.Button1_btn")
  gUI.SafeS.ClearPassBtn1 = WindowSys_Instance:GetWindow("Password.Password_bg.ClearPassword_bg.Button1_btn")
  gUI.SafeS.rootSign = WindowSys_Instance:GetWindow("Signature")
  gUI.SafeS.SignOK = WindowSys_Instance:GetWindow("Signature.Button1_btn")
  gUI.SafeS.SignLableText = WindowSys_Instance:GetWindow("Signature.Picture3.Label1_slab")
  gUI.SafeS.SignCtrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("Signature.TableCtrl_tctl"))
  gUI.SafeS.SignClose = WindowSys_Instance:GetWindow("Signature.Close_btn")
  gUI.SafeS.SignGoodBox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Signature.Picture2_bg.GoodsBox1_gbox"))
  gUI.SafeS.SignLableInfo = WindowSys_Instance:GetWindow("Signature.Label1_slab")
end
function Script_SafeSetup_OnEvent(event)
  if event == "PLAYER_LOGIN_PROTECT" then
    _OnSafeSetup_ProtectStart()
  elseif event == "SAFESETUP_REFRESH_UI" then
    _OnSafeSetup_RefreshUI()
  elseif event == "SAFESETUP_POP_KEYPAD" then
    _OnSafeSetup_PopNumericPad(arg1)
  elseif event == "NPC_OPEN_SIGN" then
  end
end

