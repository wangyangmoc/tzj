local g_wndLastSelected, g_wndLastSelected2
local g_nLastSelectedSererId = -1
local g_nServerIndex = 20
local g_wndLastSelectedGroup
local g_bShowServer = true
local g_nServerFlag = {
  [0] = "",
  [1] = "UiBtn001:Image_New",
  [2] = "UiBtn001:Image_New",
  [3] = "UiBtn001:Image_New"
}
local g_nLastServerInfo = {
  group = "",
  server = "",
  id = 0,
  groupId = 0
}
function UI_Login_OnServerSelect(msg)
  if g_wndLastSelected then
    g_wndLastSelected:SetStatus("normal")
  end
  if g_wndLastSelected2 then
    g_wndLastSelected2:SetStatus("normal")
  end
  g_nServerIndex = 20
  local wnd = WindowSys_Instance:GetWindow("Server.CurrentGroup_dlab")
  local groupID = wnd:GetCustomUserData(0)
  local bReslut = CheckServerGroupChange(groupID)
  wnd = WindowSys_Instance:GetWindow("Server.CurrentGroup_dlab")
  local curGroupName = wnd:GetProperty("Text")
  g_nLastServerInfo.groupId = groupID
  wnd = WindowSys_Instance:GetWindow("Server.CurrentServer_dlab")
  local curServerName = wnd:GetProperty("Text")
  g_nLastServerInfo.group = curGroupName
  g_nLastServerInfo.server = curServerName
  g_nLastServerInfo.id = wnd:GetCustomUserData(0)
  SaveLoginServerInfo(curGroupName, curServerName, wnd:GetCustomUserData(0), wnd:GetCustomUserData(0), g_nLastServerInfo.groupId)
  StopDownLoadServerInfo()
  if bReslut then
    Lua_LoginGrp_ShowServerUI(false)
    wnd = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
    wnd:SetProperty("Visible", "true")
    wnd = WindowToButton(WindowSys_Instance:GetWindow("Server.Close_btn"))
    wnd:SetProperty("Enable", "true")
    wnd = WindowSys_Instance:GetWindow("LoginGroup.BtnLeft_bg.Area_dlab")
    if wnd then
      wnd:SetProperty("Text", curGroupName .. "   " .. curServerName)
      SaveServerInfo(curServerName)
    end
    wnd = WindowSys_Instance:GetWindow("CharList")
    if wnd then
      BackToLogin()
    end
  else
    SaveClientServerInfo()
    StartNewVerClient(groupID)
    Exit()
  end
end
function UI_Login_OnServerBtnClick(msg)
  if g_wndLastSelected then
    g_wndLastSelected:SetStatus("normal")
  end
  if g_wndLastSelected2 then
    g_wndLastSelected2:SetStatus("normal")
  end
  local wnd = msg:get_window()
  local text = wnd:GetProperty("Text")
  local serverindex = wnd:GetCustomUserData(1)
  wnd = WindowToButton(wnd)
  wnd:SetStatus("selected")
  g_wndLastSelected = wnd
  g_wndLastSelected2 = nil
  local index = wnd:GetCustomUserData(0)
  if index > -1 then
    local wnd1
    if index >= 16 then
      wnd1 = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index) .. ".Server_btn")
    else
      wnd1 = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index + 1) .. ".Server_btn")
    end
    if wnd1 then
      wnd1 = WindowToButton(wnd1)
      wnd1:SetStatus("selected")
      g_wndLastSelected2 = wnd1
    end
  end
  _Login_ShowCurrentSelection(serverindex)
end
function UI_Login_OnGroupBtnClick(msg)
  if g_wndLastSelectedGroup then
    g_wndLastSelectedGroup:SetStatus("normal")
  end
  g_nLastSelectedSererId = 0
  if g_wndLastSelected then
    g_wndLastSelected:SetStatus("normal")
  end
  if g_wndLastSelected2 then
    g_wndLastSelected2:SetStatus("normal")
  end
  local wnd = msg:get_window()
  local serverindex = wnd:GetCustomUserData(1)
  ChangeServerList(serverindex)
  wnd = WindowToButton(wnd)
  wnd:SetStatus("selected")
  g_wndLastSelectedGroup = wnd
  local str = wnd:GetProperty("Text")
  wnd = WindowSys_Instance:GetWindow("Server.CurrentGroup_dlab")
  wnd:SetProperty("Text", str)
  wnd:SetCustomUserData(0, serverindex)
end
function UI_Login_OnCancleBtnClick(msg)
  if g_wndLastSelected then
    g_wndLastSelected:SetStatus("normal")
  end
  if g_wndLastSelected2 then
    g_wndLastSelected2:SetStatus("normal")
  end
  g_nServerIndex = 20
  local wnd = WindowSys_Instance:GetWindow("LoginGroup.BtnLeft_bg.Area_dlab")
  if wnd ~= nil then
    SaveLoginServerInfo(g_nLastServerInfo.group, g_nLastServerInfo.server, g_nLastServerInfo.id, g_nLastServerInfo.id, g_nLastServerInfo.groupId)
  end
  Lua_LoginGrp_ShowServerUI(false)
  wnd = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
  wnd:SetProperty("Visible", "true")
end
function UI_Login_OnAgreeConferBtnClick(msg)
  local wnd = WindowSys_Instance:GetWindow("Treaty")
  wnd:SetProperty("Visible", "false")
  local wndLogin = WindowSys_Instance:GetWindow("LoginGroup.Picture5_bg")
  if g_bShowServer then
    Lua_LoginGrp_ShowServerUI(true)
    wndLogin:SetProperty("Visible", "false")
  else
    Lua_LoginGrp_ShowServerUI(false)
    wndLogin:SetProperty("Visible", "true")
  end
  wnd = WindowToCheckButton(WindowSys_Instance:GetWindow("Treaty.Treaty_bg.Treaty_cbtn"))
  local bChecked = wnd:IsChecked()
  if bChecked then
    SetUserConfer(false)
  end
end
function UI_Login_OnRefuseConferBtnClick(msg)
  Exit()
end
function UI_Login_ReDownloadServerList(msg)
  ReDownloadServerList()
end
function _OnLogin_OnSeverCount(rec, total)
  local test = WindowSys_Instance:GetWindow("Server.Picture1.Server_btn")
  if not test then
    return
  end
  local last = 16 + rec
  for i = last, 18 do
    local wnd = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(i))
    if wnd then
      wnd:SetProperty("Visible", "false")
      local wnd1 = WindowToButton(wnd:GetChildByName("Server_btn"))
      wnd1:SetStatus("normal")
    end
  end
  last = total + 1
  for i = last, 15 do
    local wnd = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(i))
    if wnd then
      wnd:SetProperty("Visible", "false")
      local wnd1 = WindowToButton(wnd:GetChildByName("Server_btn"))
      wnd1:SetStatus("normal")
    end
  end
  if g_nLastSelectedSererId == 0 then
    local wnd = WindowSys_Instance:GetWindow("Server.Picture1.Server_btn")
    if wnd then
      local text = wnd:GetProperty("Text")
      local serverindex = wnd:GetCustomUserData(1)
      wnd = WindowToButton(wnd)
      wnd:SetStatus("selected")
      g_wndLastSelected = wnd
      _Login_ShowCurrentSelection(serverindex)
      if g_wndLastSelected then
        local index = g_wndLastSelected:GetCustomUserData(0)
        if index > -1 then
          local wnd1
          if index >= 16 then
            wnd1 = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index) .. ".Server_btn")
          else
            wnd1 = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index + 1) .. ".Server_btn")
          end
          if wnd1 then
            wnd1 = WindowToButton(wnd1)
            wnd1:SetStatus("selected")
            g_wndLastSelected2 = wnd1
          end
        end
      end
    end
  end
end
function _OnLogin_InitLastSelection(lastGroup, lastServer, currentGroup, nLastServerId, nLastGroupId)
  g_nLastServerInfo.group = lastGroup
  g_nLastServerInfo.server = lastServer
  g_nLastServerInfo.groupId = nLastGroupId
  if nLastServerId > -1 then
    g_nLastServerInfo.id = nLastServerId
  end
  if g_wndLastSelected then
    g_wndLastSelected:SetStatus("normal")
    g_wndLastSelected = nil
  end
  if g_wndLastSelected2 then
    g_wndLastSelected2:SetStatus("normal")
    g_wndLastSelected2 = nil
  end
  if g_wndLastSelectedGroup then
    g_wndLastSelectedGroup:SetStatus("normal")
    g_wndLastSelectedGroup = nil
  end
  local wnd = WindowSys_Instance:GetWindow("Server.CurrentGroup_dlab")
  wnd:SetProperty("Text", currentGroup)
  wnd:SetCustomUserData(0, nLastGroupId)
  if nLastServerId > -1 then
    wnd = WindowSys_Instance:GetWindow("Server.LastGroup_dlab")
    wnd:SetProperty("Text", lastGroup)
    wnd = WindowSys_Instance:GetWindow("Server.LastServer.Server_btn")
    wnd:SetProperty("Text", lastServer)
    g_nLastSelectedSererId = nLastServerId
    wnd = WindowToButton(WindowSys_Instance:GetWindow("Server.Picture" .. tostring(g_nLastSelectedSererId + 1) .. ".Server_btn"))
    if wnd then
      wnd:SetStatus("selected")
      g_wndLastSelected = wnd
    end
  else
    wnd = WindowToButton(WindowSys_Instance:GetWindow("Server.Picture1.Server_btn"))
    if not wnd then
      return
    end
    g_nLastSelectedSererId = 0
    wnd:SetStatus("selected")
    g_wndLastSelected = wnd
    str = wnd:GetProperty("Text")
  end
  if g_wndLastSelected then
    local index = g_wndLastSelected:GetCustomUserData(0)
    if index > -1 then
      local wnd1
      if index >= 16 then
        wnd1 = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index) .. ".Server_btn")
      else
        wnd1 = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index + 1) .. ".Server_btn")
      end
      if wnd1 then
        wnd1 = WindowToButton(wnd1)
        wnd1:SetStatus("selected")
        g_wndLastSelected2 = wnd1
      end
    end
  end
  if nLastGroupIndex ~= nil and -1 < nLastGroupIndex then
    wnd = WindowToButton(WindowSys_Instance:GetWindow("Server.Picture" .. tostring(nLastGroupIndex + 20) .. ".Server_btn"))
    if wnd then
      wnd:SetStatus("selected")
      g_wndLastSelectedGroup = wnd
    end
  end
  _Login_ShowCurrentSelection(g_nLastSelectedSererId)
  SaveLoginServerInfo(g_nLastServerInfo.group, g_nLastServerInfo.server, g_nLastServerInfo.id, g_nLastServerInfo.id, g_nLastServerInfo.groupId)
end
function _OnLogin_ServerDownloadList(flag)
  local _label = WindowSys_Instance:GetWindow("Server.ListResult_dlab")
  if flag > 0 then
    _label:SetProperty("Text", "正在下载服务器列表..")
  elseif flag == 0 then
    _label:SetProperty("Text", "")
  elseif flag < 0 then
    _label:SetProperty("Text", "下载服务器列表失败")
  end
end
function _OnLogin_AddServer(index, GroupName, Status, GroupFlag, Tip)
  local wnd = WindowSys_Instance:GetWindow("Server.Picture" .. index + 1)
  if not wnd then
    return
  end
  wnd:SetProperty("Visible", "true")
  wnd = WindowSys_Instance:GetWindow("Server.Picture" .. index + 1 .. ".Server_btn")
  if wnd then
    wnd:SetProperty("Visible", "true")
    wnd:SetProperty("Text", GroupName)
    wnd:SetCustomUserData(1, index)
    wnd:SetCustomUserData(0, -1)
  end
  _Login_SetStatus(GroupName, index + 1, Status, GroupFlag)
end
function _OnLogin_AddRecommendServer(index, mapto, GroupName, Status, GroupFlag, Tip)
  index = index + 16
  if index > 18 then
    return
  end
  local wnd = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(index) .. ".Server_btn")
  if not wnd then
    return
  end
  wnd:SetProperty("Visible", "true")
  wnd:SetProperty("Text", GroupName)
  wnd:SetCustomUserData(1, mapto)
  wnd:SetCustomUserData(0, mapto)
  wnd = WindowSys_Instance:GetWindow("Server.Picture" .. mapto + 1 .. ".Server_btn")
  wnd:SetCustomUserData(0, index)
  _Login_SetStatus(GroupName, index, Status, GroupFlag)
end
function _OnLogin_AddGroup(index, ServerName, Recommend, New, ServerGroup)
  local wnd = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(g_nServerIndex))
  if not wnd then
    return
  end
  wnd:SetProperty("Visible", "true")
  wnd = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(g_nServerIndex) .. ".Server_btn")
  if not wnd then
    return
  end
  wnd:SetProperty("Visible", "true")
  wnd:SetProperty("Text", ServerName)
  wnd:SetCustomUserData(1, index)
  g_nServerIndex = g_nServerIndex + 1
end
function _Login_ShowCurrentSelection(serverindex)
  if g_wndLastSelected then
    local text = g_wndLastSelected:GetProperty("Text")
    local nowServer = WindowSys_Instance:GetWindow("Server.CurrentServer_dlab")
    nowServer:SetProperty("Text", text)
    nowServer:SetCustomUserData(0, serverindex)
  end
end
function _Login_SetStatus(GroupName, Index, Status, GroupFlag)
  local backImage = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(Index))
  local serverButton = WindowToButton(WindowSys_Instance:GetWindow("Server.Picture" .. tostring(Index) .. ".Server_btn"))
  local flag_pic = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(Index) .. ".Picture1")
  if Status == 0 then
    backImage:SetProperty("BackImage", "UiBtn001:Serveline_gray")
    serverButton:SetProperty("Enable", "false")
  elseif Status == 1 then
    backImage:SetProperty("BackImage", "UiBtn001:Serveline_green")
  elseif Status == 2 then
    backImage:SetProperty("BackImage", "UiBtn001:Serveline_yellow")
  elseif Status == 3 then
    backImage:SetProperty("BackImage", "UiBtn001:Serveline_orange")
  elseif Status == 4 then
    backImage:SetProperty("BackImage", "UiBtn001:Serveline_red")
  end
  if GroupFlag == 1 then
    flag_pic:SetProperty("Visible", "true")
  end
end
function Script_Login_OnEvent(event)
  if event == "SERVER_COUNT" then
    _OnLogin_OnSeverCount(arg1, arg2)
  elseif event == "ADD_RECOMMEND_SERVER" then
    _OnLogin_AddRecommendServer(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "ADD_GROUP" then
    _OnLogin_AddGroup(arg1, arg2, arg3, arg4, arg5)
  elseif event == "ADD_SERVER" then
    _OnLogin_AddServer(arg1, arg2, arg3, arg4, arg5)
  elseif event == "SERVER_INIT_LAST_SELECT" then
    _OnLogin_InitLastSelection(arg1, arg2, arg3, arg4, arg5)
  elseif event == "SERVER_DOWNLOAD_LIST" then
    _OnLogin_ServerDownloadList(arg1)
  elseif event == "UPDATE_SERVER_PING" then
    _OnLogin_UpdateServerPing(arg1, arg2, arg3)
  end
end
function Script_Login_OnLoad()
  local wnd = WindowSys_Instance:GetWindow("Server")
  wnd:SetCustomUserData(0, 0)
  g_bShowServer = InitServerListShow()
  g_nServerIndex = 20
  LoadServerList(true)
  StartDownLoadServerInfo()
end
function _OnLogin_UpdateServerPing(id, errorCode, delayTime)
  local wnd = WindowSys_Instance:GetWindow("Server.Picture" .. tostring(id + 1) .. ".Server_btn")
  if wnd then
    wnd:SetProperty("Visible", "true")
    local str
    if errorCode == 0 then
      if delayTime <= 100 then
        str = "延迟:" .. tostring(delayTime) .. "ms 极快"
      elseif delayTime > 100 and delayTime <= 200 then
        str = "延迟:" .. tostring(delayTime) .. "ms 良好"
      elseif delayTime > 200 and delayTime <= 500 then
        str = "延迟:" .. tostring(delayTime) .. "ms 较慢"
      else
        str = "延迟:" .. tostring(delayTime) .. "ms 极慢"
      end
    else
      str = ""
    end
    wnd:SetProperty("TooltipTextDes", str)
  end
end

