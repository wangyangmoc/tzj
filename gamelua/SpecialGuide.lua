if gUI and not gUI.SpecialGuide then
  gUI.SpecialGuide = {}
end
local _SP_CURRENT_PAGE = 0
local _SP_MAX_PAGE = 0
local _SP_GUIDE_INFO = {}
function On_SpecialGuide_RootShow(msg)
  local wnd = WindowToButton(msg:get_window())
  local stepId = tonumber(wnd:GetProperty("CustomUserData"))
  local spInfoList = gUI_SpecialGuideInfo[stepId]
  _SP_CURRENT_PAGE = 1
  _SP_MAX_PAGE = #spInfoList + 1
  _SP_GUIDE_INFO = {}
  for i = 0, #spInfoList do
    table.insert(_SP_GUIDE_INFO, {
      INDEX = i + 1,
      PICTURE = spInfoList[i].pciture
    })
  end
  SpecailGuide_InitBtnData()
  gUI.SpecialGuide.Root:SetVisible(true)
end
function SpecailGuide_InitBtnData()
  gUI.SpecialGuide.CurrLab:SetProperty("Text", tostring(_SP_CURRENT_PAGE) .. "/" .. tostring(_SP_MAX_PAGE))
  if _SP_CURRENT_PAGE == 1 then
    gUI.SpecialGuide.btnLast:SetProperty("Enable", "false")
  else
    gUI.SpecialGuide.btnLast:SetProperty("Enable", "true")
  end
  if _SP_CURRENT_PAGE == _SP_MAX_PAGE then
    gUI.SpecialGuide.btnNext:SetProperty("Enable", "false")
  else
    gUI.SpecialGuide.btnNext:SetProperty("Enable", "true")
  end
  gUI.SpecialGuide.Pic:SetProperty("BackImage", tostring(_SP_GUIDE_INFO[_SP_CURRENT_PAGE].PICTURE))
end
function On_SpecailGuide_CloseClick(msg)
  gUI.SpecialGuide.Root:SetVisible(false)
end
function On_SpecailGuide_UpClick(msg)
  if _SP_CURRENT_PAGE > 1 then
    _SP_CURRENT_PAGE = _SP_CURRENT_PAGE - 1
    SpecailGuide_InitBtnData()
  end
end
function On_SpecailGuide_DownClick(msg)
  if _SP_CURRENT_PAGE < _SP_MAX_PAGE then
    _SP_CURRENT_PAGE = _SP_CURRENT_PAGE + 1
    SpecailGuide_InitBtnData()
  end
end
function Script_SpecialGuide_OnHide()
end
function Script_SpecialGuide_OnEvent(event)
end
function Script_SpecialGuide_OnLoad()
  gUI.SpecialGuide.Root = WindowSys_Instance:GetWindow("SpecialGuide")
  gUI.SpecialGuide.Pic = WindowToPicture(WindowSys_Instance:GetWindow("SpecialGuide.Help_bg.Center_bg.Detail_pic"))
  gUI.SpecialGuide.btnLast = WindowToButton(WindowSys_Instance:GetWindow("SpecialGuide.Help_bg.Down_bg.Left_btn"))
  gUI.SpecialGuide.btnNext = WindowToButton(WindowSys_Instance:GetWindow("SpecialGuide.Help_bg.Down_bg.Right_btn"))
  gUI.SpecialGuide.CurrLab = WindowToLabel(WindowSys_Instance:GetWindow("SpecialGuide.Help_bg.Down_bg.Page_dlab"))
end
