local _Split_ITEM_MAX_COUNT = 1
local _Split_CALLBACKFUNC
function _Split_GetInputNum()
  local inputbox = WindowSys_Instance:GetWindow("SplitFrame.inputPic.inputbox")
  local count = inputbox:GetProperty("Text")
  return tonumber(count)
end
function _Split_SetInputNum(num)
  local inputbox = WindowSys_Instance:GetWindow("SplitFrame.inputPic.inputbox")
  inputbox:SetProperty("Text", tostring(num))
end
function Lua_Split_Inidata(maxcount, callback)
  _Split_ITEM_MAX_COUNT = maxcount
  _Split_CALLBACKFUNC = callback
  local inputbox = WindowSys_Instance:GetWindow("SplitFrame.inputPic.inputbox")
  inputbox:SetProperty("MaxValue", tostring(_Split_ITEM_MAX_COUNT))
end
function Lua_Split_Show(isShow)
  local splitframe = WindowSys_Instance:GetWindow("SplitFrame")
  if isShow then
    local posx, posy = GetMousePos()
    splitframe:MoveTo(posx, posy)
    posx = splitframe:GetProperty("Right")
    if tonumber(string.sub(posx, 2, #posx)) > 1 then
      splitframe:SetProperty("Right", "1")
    end
    local inputbox = WindowSys_Instance:GetWindow("SplitFrame.inputPic.inputbox")
    WindowSys_Instance:SetKeyboardCaptureWindow(inputbox)
    _Split_SetInputNum(1)
    splitframe:SetProperty("Visible", "true")
  else
    _Split_ITEM_MAX_COUNT = 1
    _Split_CALLBACKFUNC = nil
    splitframe:SetProperty("Visible", "false")
    local root = WindowSys_Instance:GetRootWindow()
    WindowSys_Instance:SetKeyboardCaptureWindow(root)
  end
end
function UI_Split_DownbtnClick()
  local count = _Split_GetInputNum()
  if count ~= nil then
    if count > 1 then
      _Split_SetInputNum(count - 1)
    end
  else
    ShowNewStr(INPUT_NOT_NUMBER)
  end
end
function UI_Split_UpbtnClick()
  local count = _Split_GetInputNum()
  if count ~= nil then
    if count < _Split_ITEM_MAX_COUNT then
      _Split_SetInputNum(count + 1)
    end
  else
    ShowNewStr(INPUT_NOT_NUMBER)
  end
end
function UI_Split_CancelbtnClick(msg)
  Lua_Split_Show(false)
  Lua_Bag_CancelSplitItem()
end
function UI_Split_ConfirmbtnClick(msg)
  local count = _Split_GetInputNum()
  if count ~= nil and count > 0 and count <= _Split_ITEM_MAX_COUNT then
    _Split_CALLBACKFUNC(count)
  end
  Lua_Split_Show(false)
  Lua_Bag_CancelSplitItem()
end
function UI_Split_EditboxEnter(msg)
  UI_Split_ConfirmbtnClick(msg)
end
