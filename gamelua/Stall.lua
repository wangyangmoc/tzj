if gUI and not gUI.Stall then
  gUI.Stall = {}
end
local MAXSTALLGOODS = 24
local _Stall_NotePlane, _Stall_TotalPlane, _Stall_Au_ebox, _Stall_Ag_ebox, _Stall_Cu_ebox, _Stall_CurrItemNum, _Stall_CurrStallModel, _Stall_CurrItemGuid
local _Stall_TotalNumber = 0
local _Stall_TotalMoney = 0
local _Stall_NoteItemDropUp = {}
local _Stall_NoteItemDropPrice = {}
local _Stall_ItemTax
local _STALL_TYPE_GENERAL = -2
local _STALL_TYPE_EQUIP = -3
local _STALL_TYPE_JEWEL = -4
local _STALL_USER_GUIDE = 20
function _Stall_SetWndName()
  gUI.Stall.Root = WindowSys_Instance:GetWindow("Stall")
  gUI.Stall.Goods_gbox = WindowToGoodsBox(WindowSys_Instance:GetWindow("Stall.Sell_bg.Center_bg.Goods_gbox"))
  _Stall_NotePlane = WindowToDisplayBox(WindowSys_Instance:GetWindow("Stall.Sell_bg.Center_bg.Note_bg.Note_dbox"))
  _Stall_TotalPlane = WindowSys_Instance:GetWindow("Stall.Sell_bg.Center_bg.Message_bg.Message_dlab")
  _Stall_Au_ebox = WindowToEditBox(WindowSys_Instance:GetWindow("Stall_Child.SetPrice_bg.BuyDi_bg.Price_bg.AuDi_bg.Au_ebox"))
  _Stall_Ag_ebox = WindowToEditBox(WindowSys_Instance:GetWindow("Stall_Child.SetPrice_bg.BuyDi_bg.Price_bg.AgDi_bg.Ag_ebox"))
  _Stall_Cu_ebox = WindowToEditBox(WindowSys_Instance:GetWindow("Stall_Child.SetPrice_bg.BuyDi_bg.Price_bg.CuDi_bg.Cu_ebox"))
  local name_wnd = WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.Name_bg.Name_slab")
  local myName = GetMyPlayerStaticInfo()
  name_wnd:SetProperty("Text", myName .. "的摊位")
end
function _OnStall_Open()
  gUI.Stall.Root:SetProperty("Visible", "true")
  UserGuideUpdateCustomData(20)
  local wndNew = WindowSys_Instance:GetWindow("ActionBar_frm.btns2_bg.Assist_bg.Stall_btn.StallEff_bg")
  wndNew:SetVisible(false)
  if IsStall() then
    return
  end
  local value
  for index = 1, table.maxn(_Stall_NoteItemDropUp) do
    value = _Stall_NoteItemDropUp[index]
    if value then
      local guid = value.Guid
      local bagType, bagSlot = GetItemPosByGuid(guid)
      if bagType == gUI_GOODSBOX_BAG.backpack0 then
        SetStallItem(gUI_GOODSBOX_BAG.backpack0, bagSlot, tonumber(value.Price))
      end
    end
  end
end
function _Stall_DoSetModel(curr_cbn)
  local General_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.General_cbtn"))
  local Equip_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.Equip_cbtn"))
  local Jewel_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.Jewel_cbtn"))
  General_cbtn:SetCheckedState(false)
  Equip_cbtn:SetCheckedState(false)
  Jewel_cbtn:SetCheckedState(false)
  curr_cbn:SetCheckedState(true)
  if General_cbtn:IsChecked() then
    _Stall_CurrStallModel = _STALL_TYPE_GENERAL
  elseif Equip_cbtn:IsChecked() then
    _Stall_CurrStallModel = _STALL_TYPE_EQUIP
  elseif Jewel_cbtn:IsChecked() then
    _Stall_CurrStallModel = _STALL_TYPE_JEWEL
  end
end
function _Stall_VisiblePriceWnd(bVal)
  local Child_rootWnd = WindowSys_Instance:GetWindow("Stall_Child")
  Child_rootWnd:SetTop(gUI.Stall.Root:GetTop())
  Child_rootWnd:SetLeft(gUI.Stall.Root:GetLeft())
  Child_rootWnd:SetProperty("Visible", tostring(bVal))
  local priceWnd = WindowSys_Instance:GetWindow("Stall_Child.SetPrice_bg")
  priceWnd:SetProperty("Visible", tostring(bVal))
  Lua_Bag_SetSplitBtnEnable(not bVal)
  if bVal then
    _Stall_InitDefaultPrice()
    _Stall_SetPriceCount()
    WindowSys_Instance:SetKeyboardCaptureWindow(_Stall_Au_ebox)
  else
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  end
end
function _Stall_InitDefaultPrice()
  if _Stall_Au_ebox then
    _Stall_Au_ebox:SetProperty("Text", "")
    _Stall_Ag_ebox:SetProperty("Text", "")
    _Stall_Cu_ebox:SetProperty("Text", "")
  end
end
function _Stall_GetSinglePrice()
  if _Stall_Au_ebox == nil then
    return 0
  end
  local Val = tonumber(_Stall_Au_ebox:GetProperty("Text"))
  if Val == nil then
    Val = 0
  end
  local price = Val * 10000
  Val = tonumber(_Stall_Ag_ebox:GetProperty("Text"))
  if Val == nil then
    Val = 0
  end
  price = price + Val * 100
  Val = tonumber(_Stall_Cu_ebox:GetProperty("Text"))
  if Val == nil then
    Val = 0
  end
  price = price + Val
  return price
end
function _Stall_SetPriceCount()
  local curNum = 1
  if _Stall_CurrItemNum ~= "" and 1 < tonumber(_Stall_CurrItemNum) then
    curNum = tonumber(_Stall_CurrItemNum)
  end
  local priceCount = _Stall_GetSinglePrice() * curNum
  local Au_dlab = WindowSys_Instance:GetWindow("Stall_Child.SetPrice_bg.BuyDi_bg.Total_bg.Au_dlab")
  Au_dlab:SetProperty("Text", Lua_UI_Money2String(priceCount))
end
function _Stall_OpenBtnVisible(Vbl)
  local close_btn = WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.Close_btn")
  local open_btn = WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.OK_btn")
  open_btn:SetProperty("Visible", tostring(Vbl))
  close_btn:SetProperty("Visible", tostring(not Vbl))
end
function _Stall_SendStallName()
  local name_wnd = WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.Name_bg.Name_slab")
  local strName = name_wnd:GetProperty("Text")
  SetStallName(strName)
end
function _Stall_Init()
  _Stall_OpenBtnVisible(true)
  _OnStall_DelAllItem()
  local General_cbtn = WindowToCheckButton(WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.General_cbtn"))
  _Stall_DoSetModel(General_cbtn)
  local notes_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Stall.Chat_bg.Note_bg.Note_dbox"))
  notes_dbox:ClearAllContent()
  _Stall_NotePlane:ClearAllContent()
  _Stall_NotePlane:MoveToEnd()
  _Stall_NotePlane:UpdateScrollBarConfig()
  notes_dbox:MoveToEnd()
  notes_dbox:UpdateScrollBarConfig()
  _Stall_TotalNumber = 0
  _Stall_TotalMoney = 0
  _Stall_TotalPlane:SetProperty("Text", "")
  _Stall_NoteItemDropUp = {}
  _Stall_NoteItemDropPrice = {}
end
function Lua_Stall_AddItem(from_win, from_off)
  from_win = WindowToGoodsBox(from_win)
  local from_type = from_win:GetProperty("BoxType")
  if from_type == "backpack1" then
    WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_INVALID_ITEM)
    return
  end
  _Stall_CurrItemNum = from_win:GetItemSubscript(from_off)
  from_off = Lua_Bag_GetRealSlot(from_win, from_off)
  local form_typeIndex = from_win:GetProperty("GoodBoxIndex")
  if form_typeIndex == "GB_ItemMainBag" then
    if IsStall() then
      WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_INSTALL)
      return
    end
    local bCanStall = ItemCanStall(gUI_GOODSBOX_BAG.backpack0, from_off)
    if bCanStall then
      Lua_Stall_HideAllChild()
      _Stall_VisiblePriceWnd(true)
      local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.backpack0, from_off, 0)
      _Stall_CurrItemGuid = guid
    end
  end
end
function Lua_Stall_HideAllChild()
  local Child_rootWnd = WindowSys_Instance:GetWindow("Stall_Child")
  local SetPrice_bg = Child_rootWnd:GetChildByName("SetPrice_bg")
  local Rename_bg = Child_rootWnd:GetChildByName("Rename_bg")
  local MyBuy_bg = Child_rootWnd:GetChildByName("MyBuy_bg")
  Child_rootWnd:SetVisible(false)
  SetPrice_bg:SetVisible(false)
  Rename_bg:SetVisible(false)
  MyBuy_bg:SetVisible(false)
end
function _OnStall_AddItem(index, name, icon, num, id, quality, price)
  if index >= MAXSTALLGOODS then
    return
  end
  gUI.Stall.Goods_gbox:SetItemGoods(index, icon, quality)
  gUI.Stall.Goods_gbox:SetItemData(index, price)
  local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.stallbag1, index)
  if ForgeLevel_To_Stars[intenLev] > 0 then
    gUI.Stall.Goods_gbox:SetItemStarState(index, ForgeLevel_To_Stars[intenLev])
  else
    gUI.Stall.Goods_gbox:SetItemStarState(index, 0)
  end
  if num and num > 1 then
    gUI.Stall.Goods_gbox:SetItemSubscript(index, tostring(num))
  end
end
function _OnStall_DelItem(index)
  if index >= MAXSTALLGOODS then
    return
  end
  gUI.Stall.Goods_gbox:ClearGoodsItem(index)
  gUI.Stall.Goods_gbox:SetItemData(index, 0)
end
function _OnStall_DelAllItem()
  gUI.Stall.Goods_gbox:ResetAllGoods(true)
end
function _OnStall_AddMsg(strName, strGuid, strNote)
  local notes_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Stall.Chat_bg.Note_bg.Note_dbox"))
  if strName then
    local strBuff = string.format("{PlAyEr^%s^%s^%s^2}:%s", strName, strGuid, "0xFFFFFFFF", strNote)
    notes_dbox:InsertBack(strBuff, 4294966016, 0, 0)
  else
    notes_dbox:ClearAllContent()
  end
end
function _OnStall_Establish(stallCost, stallTax, modelType)
  _Stall_ItemTax = stallTax * 0.01
  Messagebox_Show("STALL_ESTABLISH", stallCost, stallTax, 1)
end
function _OnStall_CreateStall()
  Lua_Chat_ShowSysLog("STALL_SUCCEED")
  _Stall_OpenBtnVisible(false)
end
function _OnStall_StopStall()
  _Stall_OpenBtnVisible(true)
end
function _OnStall_UpdateLog(buyerName, buyerGuid, buyerMenpai, itemID, money, num, ItemName, stalltype)
  local strMoney = ""
  local strTax = ""
  local getMoney = 0
  if money == 0 then
    strMoney = "金钱携带达到上限，不能再出售物品"
  else
    if money >= 100 then
      local moneyTex = math.floor(money * _Stall_ItemTax + 0.5)
      strTax = Lua_UI_Money2String(moneyTex)
      strTax = "扣除了" .. strTax .. "手续费 "
      getMoney = money - moneyTex
    else
      getMoney = money
    end
    strMoney = Lua_UI_Money2String(money)
  end
  local strDate, strTime = GetDateAndTime()
  local strBuff = string.format("{PlAyEr^%s^%s^%s^2}花费了%s向你购买了%d个[%s],%s(%s%s)\n", buyerName, buyerGuid, gUI_MenPaiColor[buyerMenpai], strMoney, num, ItemName, strTax, strDate, strTime)
  _Stall_NotePlane:InsertBack(strBuff, 4294967295, 0, 0)
  Lua_Chat_ShowSysLog("STALL_SELL_ITEM", strDate .. strTime, num, ItemName, strMoney)
  _Stall_TotalNumber = _Stall_TotalNumber + num
  _Stall_TotalMoney = _Stall_TotalMoney + getMoney
  local TotalMoneyText = Lua_UI_Money2String(_Stall_TotalMoney)
  local str = "卖出" .. _Stall_TotalNumber .. "件商品，获得" .. TotalMoneyText
  _Stall_TotalPlane:SetProperty("Text", str)
end
function _OnStall_ChangeMyIcon()
end
function UI_Stall_ChangePrice()
  _Stall_SetPriceCount()
end
function UI_Stall_CancelPrice()
  _Stall_VisiblePriceWnd(false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Stall_OKPrice()
  local SinglePrice = _Stall_GetSinglePrice()
  if SinglePrice <= 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_NEED_MONEY)
    return
  end
  if _Stall_CurrItemGuid then
    local bagType, bagSlot = GetItemPosByGuid(_Stall_CurrItemGuid)
    if bagType == gUI_GOODSBOX_BAG.backpack0 then
      SetStallItem(bagType, bagSlot, SinglePrice)
    end
  end
  _Stall_VisiblePriceWnd(false)
end
function UI_Stall_Rename()
  if IsStall() then
    WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_INSTALL)
    return
  end
  Lua_Stall_HideAllChild()
  local Child_rootWnd = WindowSys_Instance:GetWindow("Stall_Child")
  Child_rootWnd:SetTop(gUI.Stall.Root:GetTop())
  Child_rootWnd:SetLeft(gUI.Stall.Root:GetLeft())
  Child_rootWnd:SetProperty("Visible", "true")
  local rename_bg = WindowSys_Instance:GetWindow("Stall_Child.Rename_bg")
  rename_bg:SetProperty("Visible", "true")
  local rename_wnd = WindowSys_Instance:GetWindow("Stall_Child.Rename_bg.Name_bg.Name_ebox")
  rename_wnd:SetProperty("Text", "")
  WindowSys_Instance:SetKeyboardCaptureWindow(rename_wnd)
end
function UI_Stall_RenameClose()
  local Child_rootWnd = WindowSys_Instance:GetWindow("Stall_Child")
  Child_rootWnd:SetProperty("Visible", "false")
  local rename_bg = WindowSys_Instance:GetWindow("Stall_Child.Rename_bg")
  rename_bg:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Stall_DoRename()
  local rename_wnd = WindowSys_Instance:GetWindow("Stall_Child.Rename_bg.Name_bg.Name_ebox")
  local strName = rename_wnd:GetProperty("Text")
  if string.len(strName) > 0 then
    local name_wnd = WindowSys_Instance:GetWindow("Stall.Sell_bg.Di_bg.Name_bg.Name_slab")
    if SetStallName(strName) then
      name_wnd:SetProperty("Text", strName)
      UI_Stall_RenameClose()
    end
  else
    UI_Stall_RenameClose()
  end
end
function UI_Stall_EditBoxTAB(msg)
  local wnd = msg:get_window()
  local wndName = wnd:GetProperty("WindowName")
  if string.find(wndName, "Au_ebox") and _Stall_Ag_ebox then
    WindowSys_Instance:SetKeyboardCaptureWindow(_Stall_Ag_ebox)
  elseif string.find(wndName, "Ag_ebox") and _Stall_Cu_ebox then
    WindowSys_Instance:SetKeyboardCaptureWindow(_Stall_Cu_ebox)
  elseif string.find(wndName, "Cu_ebox") and _Stall_Au_ebox then
    WindowSys_Instance:SetKeyboardCaptureWindow(_Stall_Au_ebox)
  end
end
function UI_Stall_Close()
  _OnStallRemItem(false)
  gUI.Stall.Root:SetProperty("Visible", "false")
end
function UI_Stall_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Stall.Sell_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Stall_Open()
  _Stall_SendStallName()
  RequestStall(_Stall_CurrStallModel)
end
function UI_Stall_Stop()
  if IsStall() then
    CloseStallMy()
  end
end
function UI_Stall_DialogClose()
  local dialog_wnd = WindowSys_Instance:GetWindow("Stall.Chat_bg")
  dialog_wnd:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Stall_DialogOpen()
  local dialog_wnd = WindowSys_Instance:GetWindow("Stall.Chat_bg")
  local bVisible = dialog_wnd:GetProperty("Visible")
  if bVisible == "true" then
    dialog_wnd:SetProperty("Visible", "false")
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  else
    dialog_wnd:SetProperty("Visible", "true")
  end
end
function UI_Stall_ChooseModel(msg)
  local curr_cbn = WindowToCheckButton(msg:get_window())
  if IsStall() then
    WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_INSTALL)
    curr_cbn:SetCheckedState(not curr_cbn:IsChecked())
    return
  end
  _Stall_DoSetModel(curr_cbn)
end
function UI_Stall_TableCtrlShow(msg)
  local cur_page = msg:get_wparam()
  if cur_page == 0 then
    gUI.Stall.Goods_gbox:SetProperty("Visible", "true")
    _Stall_NotePlane:GetParent():SetProperty("Visible", "false")
    _Stall_TotalPlane:GetParent():SetProperty("Visible", "false")
  elseif cur_page == 1 then
    gUI.Stall.Goods_gbox:SetProperty("Visible", "false")
    _Stall_NotePlane:GetParent():SetProperty("Visible", "true")
    _Stall_TotalPlane:GetParent():SetProperty("Visible", "true")
  end
end
function UI_Stall_ItemDrop(msg)
  local to_win = WindowToGoodsBox(msg:get_window())
  local from_off = msg:get_wparam()
  local from_win = to_win:GetDragItemParent()
  Lua_Stall_AddItem(from_win, from_off)
end
function UI_Stall_ItemClear(msg)
  if not IsStall() then
    local index = msg:get_lparam()
    ClearStallItem(index)
  end
end
function UI_Stall_Show()
  local _, _, _, _, level = GetPlaySelfProp(4)
  if level < 30 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_NEED_LEVEL)
  else
    local Val = gUI.Stall.Root:GetProperty("Visible")
    if Val == "true" then
      UI_Stall_Close()
    elseif Val == "false" then
      _OnStall_Open()
    end
  end
end
function UI_Stall_Enter(msg)
  UI_Stall_SendBtnClick()
end
function UI_Stall_SendBtnClick()
  local myChat_ebox = WindowSys_Instance:GetWindow("Stall.Chat_bg.MyChat_bg.MyChat_ebox")
  if not IsStall() then
    WorldGroup_ShowNewStr(UI_SYS_MSG.STALL_NOTINSTALL)
    myChat_ebox:SetProperty("Text", "")
    return
  end
  local strMsg = myChat_ebox:GetProperty("Text")
  local _, _, _, _, _, _, _, my_guid = GetMyPlayerStaticInfo()
  AddStallBBSMsg(strMsg, my_guid)
  myChat_ebox:SetProperty("Text", "")
  local btn = WindowSys_Instance:GetWindow("Stall.Chat_bg.Send_btn")
  btn:SetProperty("Enable", "false")
  btn:SetProperty("Text", "发送留言&1&")
end
function UI_Stall_TimeOver(msg)
  local btn = msg:get_window()
  btn:SetProperty("Enable", "true")
  btn:SetProperty("Text", "发送留言")
end
function UI_Stall_DialogClear()
  ClearStallBBSMsg()
  local notes_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("Stall.Chat_bg.Note_bg.Note_dbox"))
  notes_dbox:ClearAllContent()
end
function UI_StallGoods_ShowTip(msg)
  local goodsbox = msg:get_window()
  local box_type = goodsbox:GetProperty("BoxType")
  local tooltip = goodsbox:GetToolTipWnd(0)
  local bag = gUI_GOODSBOX_BAG[box_type]
  local slot = msg:get_wparam()
  local wnd = WindowToGoodsBox(goodsbox)
  local price = wnd:GetItemData(slot)
  if price then
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil, price)
    Lua_Tip_ShowEquipCompare(goodsbox, bag, slot)
  end
end
function Script_Stall_OnLoad()
  _Stall_SetWndName()
  _Stall_Init()
end
function Script_Stall_OnHide()
end
function Script_Stall_OnEscape()
  _OnStallRemItem(true)
end
function _OnStallRemItem(param)
  UI_Stall_RenameClose()
  if not IsStall() and (gUI.Stall.Root:IsVisible() or param == true) then
    _OnStall_DelAllItem()
    _Stall_NoteItemDropUp = {}
    for i = 0, MAXSTALLGOODS do
      local guid = GetItemGUIDByPos(gUI_GOODSBOX_BAG.stallbag1, i)
      if guid ~= nil then
        local price = GetStallPriceByIndex(i)
        table.insert(_Stall_NoteItemDropUp, {Guid = guid, Price = price})
      end
    end
    DelAllGoods()
  end
end
function Script_Stall_OnEvent(event)
  if event == "STALL_ADD_GOODS" then
    _OnStall_AddItem(arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  elseif event == "STALL_DEL_GOODS" then
    _OnStall_DelItem(arg1)
  elseif event == "STALL_REM_GOODS" then
    _OnStallRemItem(false)
  elseif event == "STALL_ESTABLISH" then
    _OnStall_Establish(arg1, arg2, arg3)
  elseif event == "STALL_CREATESTALL" then
    _OnStall_CreateStall()
  elseif event == "STALL_UPDATE_BBS" then
    _OnStall_AddMsg(arg1, arg2, arg3)
  elseif event == "STALL_CLOSE_MINE" then
    _OnStall_StopStall()
  elseif event == "STALL_CLEAR_MINE" then
    UI_Stall_Close()
  elseif event == "STALL_OPEN_MINE" then
    _OnStall_Open()
  elseif event == "STALL_UPDATE_LOG" then
    _OnStall_UpdateLog(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
  elseif event == "STALL_UPDATE_MY_ICON" then
    _OnStall_ChangeMyIcon()
  end
end
