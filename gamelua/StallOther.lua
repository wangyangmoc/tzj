local MAXSTALLGOODS = 24
local _OtherStall_Root, _OtherStall_GoodsBoxbg, _OtherStall_ItemNum_ebox, _OtherStall_OwnerID, _OtherStall_BuyItemCurrNum, _OtherStall_BuyItemName, _OtherStall_BuyItemSinglePrice, _OtherStall_BuyItemIndex
local _OtherStall_ItemSortList = {}
function _OtherStall_SetWndName()
  _OtherStall_Root = WindowSys_Instance:GetWindow("OtherStall")
  _OtherStall_GoodsBoxbg = WindowToGoodsBox(WindowSys_Instance:GetWindow("OtherStall.Buy_bg.Center_bg.Buy_gbox"))
  _OtherStall_ItemNum_ebox = WindowToEditBox(WindowSys_Instance:GetWindow("Stall_Child.MyBuy_bg.BuyDi_bg.Num_bg.Num_ebox"))
end
function _OtherStall_SetBuyBox()
  local BuyBox_Text = WindowSys_Instance:GetWindow("Stall_Child.MyBuy_bg.BuyDi_bg.Name_dlab")
  local text = string.format("您将购买 {#G^%d} 件【%s】", _OtherStall_BuyItemCurrNum, _OtherStall_BuyItemName)
  BuyBox_Text:SetProperty("Text", text)
  local money_str = Lua_UI_Money2String(_OtherStall_BuyItemSinglePrice * _OtherStall_BuyItemCurrNum)
  local Au_dlab = WindowSys_Instance:GetWindow("Stall_Child.MyBuy_bg.BuyDi_bg.Money_bg.Au_dlab")
  Au_dlab:SetProperty("Text", money_str)
  _OtherStall_ItemNum_ebox:SetProperty("Text", tostring(_OtherStall_BuyItemCurrNum))
  return string.format("您将花费%s购买%d件%s", money_str, _OtherStall_BuyItemCurrNum, _OtherStall_BuyItemName)
end
function _OtherStall_ShowBuyBox(itemName, max_num, price)
  _OtherStall_BuyItemCurrNum = 1
  Lua_Stall_HideAllChild()
  local Child_rootWnd = WindowSys_Instance:GetWindow("Stall_Child")
  Child_rootWnd:SetTop(_OtherStall_Root:GetTop())
  Child_rootWnd:SetLeft(_OtherStall_Root:GetLeft())
  Child_rootWnd:SetProperty("Visible", "true")
  local BuyBox_bg = WindowSys_Instance:GetWindow("Stall_Child.MyBuy_bg")
  BuyBox_bg:SetProperty("Visible", "true")
  _OtherStall_ItemNum_ebox:SetProperty("MaxValue", tostring(max_num))
  _OtherStall_BuyItemName = itemName
  _OtherStall_BuyItemSinglePrice = price
  _OtherStall_SetBuyBox()
end
function _OtherStall_MarkbtnTxet(bVal)
  local mark_btn = WindowSys_Instance:GetWindow("OtherStall.Buy_bg.Di_bg.Sign_btn")
  mark_btn:SetProperty("Text", bVal and "取消标记" or "标记摊位")
end
function _OtherStall_ItemListSortByPrice(ItemA, ItemB)
  if ItemA.ItemPrice == ItemB.ItemPrice then
    return ItemA.DataIndex < ItemB.DataIndex
  else
    return ItemA.ItemPrice > ItemB.ItemPrice
  end
end
function _OtherStall_ShowGoodsBox()
  _OtherStall_GoodsBoxbg:ResetAllGoods(true)
  table.sort(_OtherStall_ItemSortList, _OtherStall_ItemListSortByPrice)
  for tableIdx, value in pairs(_OtherStall_ItemSortList) do
    local showIdx = tableIdx - 1
    _OtherStall_GoodsBoxbg:SetItemGoods(showIdx, value.StrIcon, value.ItemQuality)
    local intenLev = Lua_Bag_GetStarInfo(gUI_GOODSBOX_BAG.stallbag2, value.DataIndex)
    if ForgeLevel_To_Stars[intenLev] > 0 then
      _OtherStall_GoodsBoxbg:SetItemStarState(showIdx, ForgeLevel_To_Stars[intenLev])
    else
      _OtherStall_GoodsBoxbg:SetItemStarState(showIdx, 0)
    end
    _OtherStall_GoodsBoxbg:SetItemData(showIdx, value.ItemPrice)
    if value.ItemCount and 1 < value.ItemCount then
      _OtherStall_GoodsBoxbg:SetItemSubscript(showIdx, tostring(value.ItemCount))
    end
  end
end
function _OnOtherStall_Close()
  UI_OtherStall_Close()
  _OtherStall_ItemSortList = {}
end
function _OnOtherStall_Open(owner_guid, owner_name, brand_name, stall_type, is_follow, item_count)
  _OtherStall_Root:SetProperty("Visible", "true")
  local owner_dlab = WindowSys_Instance:GetWindow("OtherStall.Buy_bg.Top_bg.Title_dlab")
  owner_dlab:SetProperty("Text", owner_name .. "的摊位")
  local brand_dlab = WindowSys_Instance:GetWindow("OtherStall.Buy_bg.Di_bg.Name_bg.Name_dlab")
  brand_dlab:SetProperty("Text", brand_name)
  _OtherStall_OwnerID = owner_guid
  local notes_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("OtherStall.ChatBuy_bg.Note_bg.Note_dbox"))
  notes_dbox:ClearAllContent()
  notes_dbox:MoveToEnd()
  notes_dbox:UpdateScrollBarConfig()
  _OtherStall_MarkbtnTxet(is_follow)
  _OtherStall_ShowGoodsBox()
end
function _OnOtherStall_AddItem(index, icon, count, price, quality)
  if index >= MAXSTALLGOODS then
    return
  end
  for tableIdx, value in pairs(_OtherStall_ItemSortList) do
    if value.DataIndex == index then
      value.ItemCount = count
      if value.ItemCount and value.ItemCount > 1 then
        _OtherStall_GoodsBoxbg:SetItemSubscript(tableIdx - 1, tostring(value.ItemCount))
      else
        _OtherStall_GoodsBoxbg:SetItemSubscript(tableIdx - 1, "")
      end
      return
    end
  end
  table.insert(_OtherStall_ItemSortList, {
    DataIndex = index,
    StrIcon = icon,
    ItemCount = count,
    ItemPrice = price,
    ItemQuality = quality
  })
end
function _OnOtherStall_DelItem(index)
  if index >= MAXSTALLGOODS then
    return
  end
  for tableIdx, value in pairs(_OtherStall_ItemSortList) do
    if value.DataIndex == index then
      table.remove(_OtherStall_ItemSortList, tableIdx)
      _OtherStall_ShowGoodsBox()
      return
    end
  end
end
function _OnOtherStall_AddMsg(strName, strGuid, strNote)
  local notes_dbox = WindowToDisplayBox(WindowSys_Instance:GetWindow("OtherStall.ChatBuy_bg.Note_bg.Note_dbox"))
  if strName then
    local strBuff = string.format("{PlAyEr^%s^%s^%s^2}:%s", strName, strGuid, "0xFFFFFFFF", strNote)
    notes_dbox:InsertBack(strBuff, 4294966016, 0, 0)
  else
    notes_dbox:ClearAllContent()
  end
end
function UI_OtherStall_NameClicked(msg)
  local wnd = msg:get_window()
  local stallType = wnd:GetCustomUserData(1)
  local stallObj = wnd:GetCustomUserData(0)
  StallOpen(stallObj, stallType)
end
function UI_OtherStall_Close()
  _OtherStall_Root:SetProperty("Visible", "false")
  CloseStallOther()
  UI_OtherStall_BuyCancel()
  Messagebox_Hide("STALL_BUY_ITEM")
end
function UI_OtherStall_DialogClose()
  local dialog_wnd = WindowSys_Instance:GetWindow("OtherStall.ChatBuy_bg")
  dialog_wnd:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_OtherStall_DialogOpen()
  local dialog_wnd = WindowSys_Instance:GetWindow("OtherStall.ChatBuy_bg")
  local bVisible = dialog_wnd:GetProperty("Visible")
  if bVisible == "true" then
    dialog_wnd:SetProperty("Visible", "false")
    WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
  else
    dialog_wnd:SetProperty("Visible", "true")
  end
end
function UI_OtherStall_Enter(msg)
  UI_OtherStall_SendBtnClick()
end
function UI_OtherStall_SendBtnClick()
  local msg_ebox = WindowSys_Instance:GetWindow("OtherStall.ChatBuy_bg.MyChat_bg.MyChat_ebox")
  local msgText = msg_ebox:GetProperty("Text")
  if _OtherStall_OwnerID then
    AddStallBBSMsg(msgText, _OtherStall_OwnerID)
  end
  msg_ebox:SetProperty("Text", "")
  local btn = WindowSys_Instance:GetWindow("OtherStall.ChatBuy_bg.Send_btn")
  btn:SetProperty("Enable", "false")
  btn:SetProperty("Text", "发送留言&1&")
end
function UI_OtherStall_TimeOver(msg)
  local btn = msg:get_window()
  btn:SetProperty("Enable", "true")
  btn:SetProperty("Text", "发送留言")
end
function UI_OtherStall_MarkStall()
  local isMark = false
  local mark_btn = WindowSys_Instance:GetWindow("OtherStall.Buy_bg.Di_bg.Sign_btn")
  local markStr = mark_btn:GetProperty("Text")
  if markStr == "标记摊位" then
    isMark = false
  elseif markStr == "取消标记" then
    isMark = true
  end
  isMark = not isMark
  _OtherStall_MarkbtnTxet(isMark)
  MarkStall(isMark)
end
function UI_OtherStall_BuyItem(msg)
  local money, name = 0, ""
  local GoodsBox = WindowToGoodsBox(msg:get_window())
  local index = msg:get_lparam()
  local max_num = tonumber(GoodsBox:GetItemSubscript(index))
  local price = GoodsBox:GetItemData(index)
  _OtherStall_BuyItemIndex = _OtherStall_ItemSortList[index + 1].DataIndex
  local itemName = GetItemInfoBySlot(gUI_GOODSBOX_BAG.stallbag2, _OtherStall_BuyItemIndex)
  if max_num == nil then
    max_num = 1
  end
  _OtherStall_ShowBuyBox(itemName, max_num, price)
end
function UI_OtherStall_BuyLeftBtn()
  _OtherStall_BuyItemCurrNum = _OtherStall_BuyItemCurrNum - 1
  if _OtherStall_BuyItemCurrNum < 1 then
    _OtherStall_BuyItemCurrNum = 1
  end
  _OtherStall_SetBuyBox()
end
function UI_OtherStall_BuyRightBtn()
  _OtherStall_BuyItemCurrNum = _OtherStall_BuyItemCurrNum + 1
  local maxNum = tonumber(_OtherStall_ItemNum_ebox:GetProperty("MaxValue"))
  if maxNum < _OtherStall_BuyItemCurrNum then
    _OtherStall_BuyItemCurrNum = maxNum
  end
  _OtherStall_SetBuyBox()
end
function UI_OtherStall_BuyNumChange()
  local inputNum = _OtherStall_ItemNum_ebox:GetProperty("Text")
  if inputNum == "" then
    return
  end
  inputNum = tonumber(inputNum)
  if inputNum < 1 then
    inputNum = 1
  end
  _OtherStall_BuyItemCurrNum = inputNum
  _OtherStall_SetBuyBox()
end
function UI_OtherStall_BuyConfirm(msg)
  local _, money = GetMyMoney()
  local singleMoney = _OtherStall_BuyItemSinglePrice * _OtherStall_BuyItemCurrNum / 10000
  if money < _OtherStall_BuyItemCurrNum * _OtherStall_BuyItemSinglePrice then
    Lua_Chat_ShowOSD("NOT_ENOUGH_MONEY")
  elseif singleMoney >= 100 then
    Messagebox_Show("STALL_BIG_ONEH", _OtherStall_SetBuyBox(), _OtherStall_BuyItemIndex, _OtherStall_BuyItemCurrNum)
  else
    UI_MessageConfim()
  end
  UI_OtherStall_BuyCancel()
end
function UI_MessageConfimBiger()
  UI_MessageConfim()
end
function UI_MessageConfim()
  Messagebox_Show("STALL_BUY_ITEM", _OtherStall_SetBuyBox(), _OtherStall_BuyItemIndex, _OtherStall_BuyItemCurrNum)
end
function UI_OtherStall_BuyCancel()
  local Child_rootWnd = WindowSys_Instance:GetWindow("Stall_Child")
  Child_rootWnd:SetProperty("Visible", "false")
  local BuyBox_bg = WindowSys_Instance:GetWindow("Stall_Child.MyBuy_bg")
  BuyBox_bg:SetProperty("Visible", "false")
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_OtherStallGoods_ShowTip(msg)
  local goodsbox = msg:get_window()
  local box_type = goodsbox:GetProperty("BoxType")
  local tooltip = goodsbox:GetToolTipWnd(0)
  local bag = gUI_GOODSBOX_BAG[box_type]
  local slot = msg:get_wparam()
  local wnd = WindowToGoodsBox(goodsbox)
  local price = wnd:GetItemData(slot)
  if price then
    if slot >= #_OtherStall_ItemSortList then
      return
    end
    slot = _OtherStall_ItemSortList[slot + 1].DataIndex
    Lua_Tip_Item(tooltip, slot, bag, nil, nil, nil, price)
    Lua_Tip_ShowEquipCompare(goodsbox, bag, slot)
  end
end
function Script_OtherStall_OnLoad()
  _OtherStall_SetWndName()
end
function Script_OtherStall_OnEvent(event)
  if event == "STALL_ADD_OTH_GOODS" then
    _OnOtherStall_AddItem(arg2, arg3, arg4, arg5, arg6)
  elseif event == "STALL_DEL_OTH_GOODS" then
    _OnOtherStall_DelItem(arg2)
  elseif event == "STALL_UPDATE_OTHERS" then
    _OnOtherStall_Open(arg1, arg2, arg3, arg4, arg5, arg6)
  elseif event == "STALL_CLOSE_OTHERS" then
    _OnOtherStall_Close()
  elseif event == "STALL_UPDATE_OTH_BBS" then
    _OnOtherStall_AddMsg(arg1, arg2, arg3)
  end
end
function Script_OtherStall_OnEscape()
  CloseStallOther()
end

