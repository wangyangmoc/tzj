function SystemOption_Cancel(msg)
  local wnd = WindowSys_Instance:GetWindow("SystemOption")
  wnd:SetProperty("Visible", "false")
end
function SystemOption_Show(isshow)
  local option = WindowSys_Instance:GetWindow("SystemOption")
  if isshow then
    option:SetProperty("Visible", "true")
  else
    option:SetProperty("Visible", "false")
  end
  if IsFinal() then
    WindowSys_Instance:GetWindow("SystemOption.DisplayAdjust"):SetProperty("Visible", "false")
  end
end
function SystemOption_Exit(msg)
  SystemOption_Show(false)
  local isIncom = IsInCombat()
  if isIncom == true then
    Messagebox_Show("EXIT_APP")
  end
end
function SystemOption_Return(msg)
  SystemOption_Show(false)
  Messagebox_Show("ENTERRETURN_APP")
end
function SystemOption_Login(msg)
  SystemOption_Show(false)
  Messagebox_Show("ENTERLOGIN_APP")
end
function SystemOption_Display(msg)
  SystemOption_Show(false)
  DisSetup_Show()
end
function SystemOption_KeySet(msg)
  SystemOption_Show(false)
  Lua_KeySet_Show()
end
function SystemOption_DisplayAdjust(msg)
  SystemOption_Show(false)
  DisplayAdjust_Show()
end
function SystemOption_UIReset(msg)
  WindowSys_Instance:RestoreTopLevelWindowPos()
  SystemOption_Show(true)
end
