if gUI and not gUI.TeamRoom then
  gUI.TeamRoom = {}
end
local _TeamRoom_NearTeamCurPag = 0
local _TeamRoom_NearPlayerCurPag = 0
local _TeamRoom_NearTeamOnePage = 5
local _TeamRoom_NearPlayerOnePage = 5
local _TeamRoom_CurrentTipTeamId = -1
local _TeamRoom_GuildPage = 0
local _TeamRoom_MaxTeamNum = 5
local _TeamRoom_GuildImage = "UiIamge012:Image_TeamGuild"
local _TeamRoom_AllianceImage = "UiIamge012:Image_TeamAlliance"
local _TeamRoom_Learder = "UiIamge012:Image_Captain"
local _TeamRoom_GuildTeamOnePage = 8
local _TeamRoom_CurrentGetLeaderTeamId = -1
function _TeamRoom_GetNearTeam()
  gUI.TeamRoom.NearByList:RemoveAllItems()
  local list = SearchNearTeam()
  if list == nil or list[0] == nil then
    return
  end
  local num = table.maxn(list)
  gUI.TeamRoom.dlabPag:SetProperty("Text", tostring(_TeamRoom_NearTeamCurPag + 1) .. "/" .. tostring(math.floor(num / _TeamRoom_NearTeamOnePage) + 1))
  if _TeamRoom_NearTeamCurPag >= math.floor(num / _TeamRoom_NearTeamOnePage) then
    gUI.TeamRoom.btnNextPage:SetEnable(false)
  else
    gUI.TeamRoom.btnNextPage:SetEnable(true)
  end
  if _TeamRoom_NearTeamCurPag <= 0 then
    gUI.TeamRoom.btnLastPage:SetEnable(false)
  else
    gUI.TeamRoom.btnLastPage:SetEnable(true)
  end
  if num > _TeamRoom_NearTeamCurPag * _TeamRoom_NearTeamOnePage + _TeamRoom_NearTeamOnePage - 1 then
    num = _TeamRoom_NearTeamCurPag * _TeamRoom_NearTeamOnePage + _TeamRoom_NearTeamOnePage - 1
  end
  for i = _TeamRoom_NearTeamCurPag * _TeamRoom_NearTeamOnePage, num do
    local str = string.format("%s|%d|%s|%d", list[i].TeamName, list[i].TeamNum, list[i].Name, list[i].Leader)
    local item = gUI.TeamRoom.NearByList:InsertString(str, -1)
    item:SetUserData(list[i].TeamId)
    item:SetUserData64(list[i].Guid)
  end
end
function _TeamRoom_GetNearPlayer()
  gUI.TeamRoom.NearByPlayerList:RemoveAllItems()
  local list = SearchNearPlayer()
  if list == nil or list[0] == nil then
    return
  end
  local num = table.maxn(list)
  gUI.TeamRoom.dlabPlayerPag:SetProperty("Text", tostring(_TeamRoom_NearPlayerCurPag + 1) .. "/" .. tostring(math.floor(num / _TeamRoom_NearPlayerOnePage) + 1))
  if _TeamRoom_NearPlayerCurPag >= math.floor(num / _TeamRoom_NearPlayerOnePage) then
    gUI.TeamRoom.btnPlayerNextPage:SetEnable(false)
  else
    gUI.TeamRoom.btnPlayerNextPage:SetEnable(true)
  end
  if _TeamRoom_NearPlayerCurPag <= 0 then
    gUI.TeamRoom.btnPlayerLastPage:SetEnable(false)
  else
    gUI.TeamRoom.btnPlayerLastPage:SetEnable(true)
  end
  if num > _TeamRoom_NearPlayerCurPag * _TeamRoom_NearPlayerOnePage + _TeamRoom_NearPlayerOnePage - 1 then
    num = _TeamRoom_NearPlayerCurPag * _TeamRoom_NearPlayerOnePage + _TeamRoom_NearPlayerOnePage - 1
  end
  for i = _TeamRoom_NearPlayerCurPag * _TeamRoom_NearPlayerOnePage, num do
    local GuildName = "未加入帮会"
    if list[i].GuidName ~= "" then
      GuildName = list[i].GuidName
    end
    local str = string.format("%s|%s|%s|%d", list[i].Name, GuildName, gUI_MenPaiName[list[i].Menpai], list[i].Lv)
    local item = gUI.TeamRoom.NearByPlayerList:InsertString(str, -1)
    item:SetUserData64(list[i].Guid)
  end
end
function _TeamRoom_GetGuildTeam()
  gUI.TeamRoom.Container:DeleteAll()
  local alliNumber = GetGuildAllianceNumber()
  local nAskType = 0
  if gUI.TeamRoom.CheckButton:IsChecked() and alliNumber > 0 then
    nAskType = 1
  end
  local maxPag, list = GetGuildTeamFlatData(nAskType, _TeamRoom_GuildPage)
  if maxPag == 0 then
    maxPag = 1
  end
  gUI.TeamRoom.GuildPage:SetProperty("Text", tostring(_TeamRoom_GuildPage) .. "/" .. tostring(maxPag))
  if _TeamRoom_GuildPage >= math.floor(maxPag) then
    gUI.TeamRoom.GuildNextPage:SetEnable(false)
  else
    gUI.TeamRoom.GuildNextPage:SetEnable(true)
  end
  if _TeamRoom_GuildPage <= 1 then
    gUI.TeamRoom.GuildLastPage:SetEnable(false)
  else
    gUI.TeamRoom.GuildLastPage:SetEnable(true)
  end
  if list == nil or list[0] == nil then
    return
  end
  local _, has_team = GetPlaySelfProp(6)
  for i = 0, table.maxn(list) do
    local tmpl = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.Template_btn")
    local newitem = gUI.TeamRoom.Container:Insert(-1, tmpl)
    newitem:SetProperty("Visible", "true")
    newitem:SetCustomUserData(0, list[i].TeamId)
    newitem:SetCustomUserData(1, i)
    local wnd = newitem:GetChildByName("TeamName_dlab")
    wnd:SetProperty("Text", list[i].TeamName)
    wnd = newitem:GetChildByName("Name_dlab")
    wnd:SetProperty("Text", list[i].Name)
    wnd = newitem:GetChildByName("Map_dlab")
    wnd:SetProperty("Text", list[i].MapName)
    wnd = newitem:GetChildByName("Count_dlab")
    wnd:SetProperty("Text", tostring(list[i].TeamNum))
    wnd = newitem:GetChildByName("Add_btn")
    wnd:SetUserData64(list[i].Guid)
    wnd:SetProperty("TooltipText", list[i].Name)
    if not has_team then
      if _TeamRoom_MaxTeamNum == list[i].TeamNum then
        wnd:SetEnable(false)
      else
        wnd:SetEnable(true)
      end
    else
      wnd:SetEnable(false)
    end
    wnd = newitem:GetChildByName("Image_bg")
    if list[i].Alliance then
      wnd:SetProperty("BackImage", _TeamRoom_AllianceImage)
    else
      wnd:SetProperty("BackImage", _TeamRoom_GuildImage)
    end
  end
end
function _TeamRoom_UpdateNearBy()
  gUI.TeamRoom.dlabPag:SetProperty("Text", "1/1")
  gUI.TeamRoom.dlabPlayerPag:SetProperty("Text", "1/1")
  gUI.TeamRoom.btnLastPage:SetEnable(false)
  gUI.TeamRoom.btnNextPage:SetEnable(false)
  gUI.TeamRoom.btnPlayerNextPage:SetEnable(false)
  gUI.TeamRoom.btnPlayerLastPage:SetEnable(false)
  _TeamRoom_NearTeamCurPag = 0
  _TeamRoom_NearPlayerCurPag = 0
  _TeamRoom_GetNearTeam()
  _TeamRoom_GetNearPlayer()
end
function _TeamRoom_UpdateGuild()
  gUI.TeamRoom.GuildPage:SetProperty("Text", "1/1")
  gUI.TeamRoom.GuildNextPage:SetEnable(false)
  gUI.TeamRoom.GuildLastPage:SetEnable(false)
  _TeamRoom_GuildPage = 1
  local alliNumber = GetGuildAllianceNumber()
  if gUI.TeamRoom.CheckButton:IsChecked() and alliNumber > 0 then
    AskAllianceTeamFlatInfo()
  else
    AskGuildTeamFlatInfo()
  end
end
function _TeamRoom_UpdateGuildPage()
  local alliNumber = GetGuildAllianceNumber()
  if gUI.TeamRoom.CheckButton:IsChecked() and alliNumber > 0 then
    AskAllianceTeamFlatNextPage(_TeamRoom_GuildPage)
  else
    AskGuildTeamFlatNextPage(_TeamRoom_GuildPage)
  end
end
function _TeamRoom_TimeStop()
  gUI.TeamRoom.TipTimer:Stop()
  if _TeamRoom_CurrentTipTeamId ~= -1 then
    RequestTeamInfoOther(_TeamRoom_CurrentTipTeamId)
  end
end
function _TeamRoom_TeamJoin(name)
  local myname = GetMyPlayerStaticInfo()
  if name == myname then
    gUI.TeamRoom.btnRequest:SetEnable(false)
    _TeamRoom_UpdateGuildBottonState(false)
  end
end
function _TeamRoom_TeamLeave(name)
  local myname = GetMyPlayerStaticInfo()
  if name == myname then
    gUI.TeamRoom.btnRequest:SetEnable(true)
    _TeamRoom_UpdateGuildBottonState(true)
  end
end
function _TeamRoom_UpdateGuildBottonState(bCanJoin)
  for i = 0, _TeamRoom_GuildTeamOnePage do
    local itemTmp = gUI.TeamRoom.Container:GetChildByData1(i)
    if itemTmp then
      local wnd = itemTmp:GetChildByName("Add_btn")
      wnd:SetEnable(bCanJoin)
    end
  end
end
function UI_TeamRoom_HelpShow(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Image_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function _OnTeamRoom_GetNearTeamTip(teamId)
  local currentTeamID, teamName, list = GetOtherTeamInfo()
  if currentTeamID ~= teamId and _TeamRoom_CurrentTipTeamId == teamId then
    return
  end
  if list == nil or list[0] == nil then
    return
  end
  local title = WindowSys_Instance:GetWindow("TooltipWindows.TeamRoom_pic.Title_bg.Title_dlab")
  title:SetProperty("Text", teamName)
  local tipList = WindowToListBox(WindowSys_Instance:GetWindow("TooltipWindows.TeamRoom_pic.Image_bg.Team_lbox"))
  tipList:RemoveAllItems()
  for i = 0, table.maxn(list) do
    local GuildName = "未加入帮会"
    if list[i].GuildName ~= "" then
      GuildName = list[i].GuildName
    end
    local str = string.format("%s|%s|%s|%d", list[i].Name, GuildName, gUI_MenPaiName[list[i].Job], list[i].Lv)
    tipList:InsertString(str, -1)
  end
  local wnd = WindowSys_Instance:GetWindow("TooltipWindows.TeamRoom_pic")
  local wndWidth = wnd:GetWidth()
  local wndHeight = wnd:GetHeight()
  local wndAct = gUI.TeamRoom.Root
  local wndActTop = wndAct:GetTop()
  local wndActRight = wndAct:GetRight()
  WindowSys_Instance:GetWindow("TooltipWindows"):SetProperty("Visible", "true")
  wnd:SetProperty("Visible", "true")
  wnd:SetLeft(wndActRight + 0.02)
  wnd:SetTop(wndActTop + 0.02)
  wnd:SetWidth(wndWidth)
  wnd:SetHeight(wndHeight)
end
function _OnTeamRoom_GetTeamLeaderInfo(teamid, name, guid)
  if teamid == _TeamRoom_CurrentGetLeaderTeamId then
    Lua_MenuShow("ON_TEAM_NEAR", name, guid)
  end
end
function UI_TeamRoom_ShowMain(msg)
  if gUI.TeamRoom.Root:IsVisible() then
    gUI.TeamRoom.Root:SetProperty("Visible", "false")
    gUI.TeamRoom.TipTimer:Stop()
  else
    gUI.TeamRoom.Root:SetProperty("Visible", "true")
    gUI.TeamRoom.NearByRoot:SetProperty("Visible", "true")
    gUI.TeamRoom.NearGuildRoot:SetProperty("Visible", "false")
    _TeamRoom_UpdateNearBy()
    local _, has_team = GetPlaySelfProp(6)
    if not has_team then
      gUI.TeamRoom.btnRequest:SetEnable(true)
    else
      gUI.TeamRoom.btnRequest:SetEnable(false)
    end
    local _, _, _, level = GetGuildBaseInfo()
    if level >= 1 then
      gUI.TeamRoom.WaitTeamBtn:SetProperty("Enable", "true")
      gUI.TeamRoom.WaitTeamBtn:SetProperty("TooltipText", "")
    else
      gUI.TeamRoom.WaitTeamBtn:SetProperty("Enable", "false")
      gUI.TeamRoom.WaitTeamBtn:SetProperty("TooltipText", "加入帮会后才能切换该分页")
    end
    gUI.TeamRoom.NearByBtn:SetStatus("selected")
  end
  _TeamRoom_CurrentTipTeamId = -1
end
function UI_TeamRoom_TypeChange(msg)
  local wnd = msg:get_window()
  local selected = wnd:GetCustomUserData(0)
  if selected == 0 then
    gUI.TeamRoom.NearByRoot:SetProperty("Visible", "true")
    gUI.TeamRoom.NearGuildRoot:SetProperty("Visible", "false")
    _TeamRoom_UpdateNearBy()
    gUI.TeamRoom.NearByBtn:SetStatus("selected")
    gUI.TeamRoom.WaitTeamBtn:SetStatus("normal")
  else
    gUI.TeamRoom.NearByRoot:SetProperty("Visible", "false")
    gUI.TeamRoom.NearGuildRoot:SetProperty("Visible", "true")
    _TeamRoom_UpdateGuild()
    gUI.TeamRoom.NearByBtn:SetStatus("normal")
    gUI.TeamRoom.WaitTeamBtn:SetStatus("selected")
  end
end
function UI_TeamRoom_LastPage(msg)
  _TeamRoom_NearTeamCurPag = _TeamRoom_NearTeamCurPag - 1
  _TeamRoom_GetNearTeam()
end
function UI_TeamRoom_NextPage(msg)
  _TeamRoom_NearTeamCurPag = _TeamRoom_NearTeamCurPag + 1
  _TeamRoom_GetNearTeam()
end
function UI_TeamRoom_LastPlayerPage(msg)
  _TeamRoom_NearPlayerCurPag = _TeamRoom_NearPlayerCurPag - 1
  _TeamRoom_GetNearPlayer()
end
function UI_TeamRoom_NextPlayerPage(msg)
  _TeamRoom_NearPlayerCurPag = _TeamRoom_NearPlayerCurPag + 1
  _TeamRoom_GetNearPlayer()
end
function UI_TeamRoom_LastGuildPage(msg)
  _TeamRoom_GuildPage = _TeamRoom_GuildPage - 1
  _TeamRoom_GetGuildTeam()
end
function UI_TeamRoom_NextGuildPage(msg)
  _TeamRoom_GuildPage = _TeamRoom_GuildPage + 1
  _TeamRoom_UpdateGuildPage()
end
function UI_TeamRoom_ApplyTeam(msg)
  local button = msg:get_window()
  local list = gUI.TeamRoom.NearByList
  local item = list:GetSelectedItem()
  if item then
    local player_name, guid = item:GetText(2), item:GetUserData64()
    button:SetProperty("Enable", "false")
    InviteJoinTeamByGUID(player_name, guid)
    button:SetProperty("Text", "申请入队&3&")
  else
    ShowErrorMessage("请选择需要加入的队伍")
  end
end
function UI_TeamRoom_InviteTeam(msg)
  local button = msg:get_window()
  local list = gUI.TeamRoom.NearByPlayerList
  local item = list:GetSelectedItem()
  if item then
    local player_name, guid = item:GetText(0), item:GetUserData64()
    button:SetProperty("Enable", "false")
    InviteJoinTeamByGUID(player_name, guid)
    button:SetProperty("Text", "邀请组队&3&")
  else
    ShowErrorMessage("请选择需要邀请的玩家")
  end
end
function UI_TeamRoom_ApplyTeamGuild(msg)
  if not gUI.TeamRoom.Container:IsMouseIn() then
    return
  end
  local wnd = msg:get_window()
  local guid = wnd:GetUserData64()
  local name = wnd:GetProperty("TooltipText")
  wnd:SetProperty("Enable", "false")
  InviteJoinTeamByGUID(name, guid)
  wnd:SetProperty("Text", "申请入队&3&")
end
function UI_TeamRoom_NearTeamRClick(msg)
  local index = msg:get_wparam()
  local list = gUI.TeamRoom.NearByList
  local item = list:GetItem(index)
  if item then
    local isLeader = item:GetText(3)
    if tonumber(isLeader) == 0 then
      _TeamRoom_CurrentGetLeaderTeamId = item:GetUserData()
      RequestTeamInfoLeader(_TeamRoom_CurrentGetLeaderTeamId)
    else
      local player_name = item:GetText(2)
      local guid = item:GetUserData64()
      Lua_MenuShow("ON_TEAM_NEAR", player_name, guid)
    end
  end
end
function UI_TeamRoom_NearGuildRClick(msg)
  local wnd = msg:get_window()
  local Namewnd = wnd:GetChildByName("Name_dlab")
  local player_name = Namewnd:GetProperty("Text")
  local Guidwnd = wnd:GetChildByName("Add_btn")
  local guid = Guidwnd:GetUserData64()
  Lua_MenuShow("ON_TEAM_NEAR2", player_name, guid)
end
function UI_TeamRoom_NearPlayerRClick(msg)
  local index = msg:get_wparam()
  local list = gUI.TeamRoom.NearByPlayerList
  local item = list:GetItem(index)
  if item then
    local guid = item:GetUserData64()
    local player_name = item:GetText(0)
    Lua_MenuShow("ON_TEAM_NEAR1", player_name, guid)
  end
end
function UI_TeamRoom_NearTeamTip(msg)
  local list = gUI.TeamRoom.NearByList
  local item = list:GetHoverItem()
  if item then
    _TeamRoom_CurrentTipTeamId = item:GetUserData()
    gUI.TeamRoom.TipTimer:Restart()
  else
    _TeamRoom_CurrentTipTeamId = -1
  end
end
function UI_TeamRoom_GuildTeamTip(msg)
  local wnd = msg:get_window()
  local timeId = wnd:GetCustomUserData(0)
  _TeamRoom_CurrentTipTeamId = timeId
  gUI.TeamRoom.TipTimer:Restart()
end
function UI_TeamRoom_NearTeamTipClose(msg)
  WindowSys_Instance:GetWindow("TooltipWindows.TeamRoom_pic"):SetProperty("Visible", "false")
  _TeamRoom_CurrentTipTeamId = -1
end
function UI_TeamRoom_CheckShowGuildType(msg)
  local alliNumber = GetGuildAllianceNumber()
  if gUI.TeamRoom.CheckButton:IsChecked() and alliNumber > 0 then
    AskAllianceTeamFlatInfo()
  else
    AskGuildTeamFlatInfo()
  end
  _TeamRoom_GuildPage = 1
end
function UI_TeamRoom_RefurbishGuildTeam()
  _TeamRoom_UpdateGuild()
  gUI.TeamRoom.RefurbishGuid:SetProperty("Enable", "false")
  gUI.TeamRoom.RefurbishGuid:SetProperty("Text", "刷新&5&")
end
function UI_TeamRoom_RefurbishNearTeam()
  _TeamRoom_UpdateNearBy()
  gUI.TeamRoom.RefurbishNear:SetProperty("Enable", "false")
  gUI.TeamRoom.RefurbishNear:SetProperty("Text", "刷新&5&")
end
function Script_TeamRoom_OnLoad()
  gUI.TeamRoom.Root = WindowSys_Instance:GetWindow("TeamRoom")
  gUI.TeamRoom.NearByBtn = WindowToButton(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Top_bg.Nearby_btn"))
  gUI.TeamRoom.WaitTeamBtn = WindowToButton(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Top_bg.WaitTeam_btn"))
  gUI.TeamRoom.NearGuildRoot = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg")
  gUI.TeamRoom.Container = WindowToContainer(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.Container_cnt"))
  gUI.TeamRoom.CheckButton = WindowToCheckButton(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.CheckButton1"))
  gUI.TeamRoom.RefurbishGuid = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.Refurbish_btn")
  gUI.TeamRoom.GuildPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.Page_bg.Page_dlab")
  gUI.TeamRoom.GuildNextPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.Page_bg.Right_btn")
  gUI.TeamRoom.GuildLastPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.WaitTeam_bg.Page_bg.Left_btn")
  gUI.TeamRoom.NearByRoot = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg")
  gUI.TeamRoom.NearByList = WindowToListBox(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Up_bg.Team_lbox"))
  gUI.TeamRoom.dlabPag = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Up_bg.Page_bg.Page_dlab")
  gUI.TeamRoom.btnNextPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Up_bg.Page_bg.Right_btn")
  gUI.TeamRoom.btnLastPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Up_bg.Page_bg.Left_btn")
  gUI.TeamRoom.btnRequest = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Up_bg.Request_btn")
  gUI.TeamRoom.RefurbishNear = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Refurbish_btn")
  gUI.TeamRoom.NearByPlayerList = WindowToListBox(WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Down_bg.Player_lbox"))
  gUI.TeamRoom.dlabPlayerPag = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Down_bg.Page_bg.Page_dlab")
  gUI.TeamRoom.btnPlayerNextPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Down_bg.Page_bg.Right_btn")
  gUI.TeamRoom.btnPlayerLastPage = WindowSys_Instance:GetWindow("TeamRoom.TeamRoom_bg.Room_bg.Image_bg.Nearby_bg.Down_bg.Page_bg.Left_btn")
  _TeamRoom_NearTeamCurPag = 0
  _TeamRoom_NearPlayerCurPag = 0
  _TeamRoom_GuildPage = 1
  gUI.TeamRoom.TipTimer = TimerSys_Instance:CreateTimerObject("near_teamtip", 0.25, 1, "_TeamRoom_TimeStop", false, false)
end
function Script_TeamRoom_OnEvent(event)
  if event == "TEAM_UPDATE_GUILD_FLAT" then
    _TeamRoom_GetGuildTeam()
  elseif event == "TEAM_UPDATE_OTHER_TIP" then
    _OnTeamRoom_GetNearTeamTip(arg1)
  elseif event == "TEAM_CREATED" then
    _TeamRoom_TeamJoin(arg1)
  elseif event == "TEAM_MEMBER_JOIN" then
    _TeamRoom_TeamJoin(arg1)
  elseif event == "TEAM_MEMBER_LEAVE" then
    _TeamRoom_TeamLeave(arg1)
  elseif event == "TEAM_UPDATE_LEADERINFO" then
    _OnTeamRoom_GetTeamLeaderInfo(arg1, arg2, arg3)
  end
end

