gTip_EquipTypeName = {
  [1] = "武器",
  [2] = "",
  [3] = "衣服",
  [4] = "手套",
  [5] = "手镯",
  [6] = "肩饰",
  [7] = "腰带",
  [8] = "鞋子",
  [9] = "戒指",
  [10] = "头盔",
  [11] = "项链",
  [12] = "耳饰",
  [20] = "",
  [21] = "时装",
  [22] = "",
  [23] = ""
}
gTip_EquipTypeNameNew = {
  [1] = "武器",
  [2] = "",
  [3] = "防具",
  [4] = "防具",
  [5] = "饰品",
  [6] = "防具",
  [7] = "防具",
  [8] = "防具",
  [9] = "饰品",
  [10] = "防具",
  [11] = "饰品",
  [12] = "饰品",
  [20] = "",
  [21] = "时装",
  [22] = "",
  [23] = ""
}
colorRed = "FFFF0611"
colorWhite = "FFFAFAFA"
colorPurper = "FF8B008B"
colorBlue = "FF48D1CC"
colorLightYellow = "FFFFFA99"
colorDarkYellow = "FFDAA520"
colorGold = "FFFFD700"
colorGreen = "FF00FF00"
colorGrey = "FFA9A9A9"
colorYellow = "FFFFFF00"
colorDarkBlue = "FF7070FF"
colorBluePurper = "FF6A5ACD"
colorUserDefine = "FF6699FF"
colorUserDefine1 = "FF99CCFF"
colorUserDefine2 = "FF00FF33"
EQUIPEQUIP = "UiBtn001:Image_Equip"
SKILLLINE = "UiIamge005:Right_Image"
MAXSKILL = "UiIamge005:Imageframe_yellow"
RUNE_ITEM_SKILLID = 133
FORM_TYPE_SHORTCUT = 1
PetUseEquipStart = 19
PetUseEquipEnd = 24
ScreenWidthPixelMax = 1
ScreenWidthPixelMin = 0
gTip_SpeciText = {
  text = "",
  color = "FFFAFAFA",
  font = ""
}
function Lua_Tip_FriendDetail(tooltip, name, level, job, guild, secene, friPoint, lineId)
  tooltip = WindowToTooltip(tooltip)
  local width = 120
  Lua_Tip_Begin(tooltip, width, "false")
  tooltip:InsertLeftText("" .. tostring(name), colorWhite, "kaiti_13", 0, 0)
  if guild ~= "" then
    tooltip:InsertLeftText(tostring(guild), colorGreen, "", 0, 0)
  end
  if secene ~= "" and secene ~= nil then
    if lineId >= 0 then
      tooltip:InsertLeftText("区域  " .. tostring(secene) .. "(" .. tostring(lineId + 1) .. "线)", colorWhite, "", 0, 0)
    else
      tooltip:InsertLeftText("区域  " .. tostring(secene), colorWhite, "", 0, 0)
    end
  end
  Lua_Tip_End(tooltip)
end
function Lua_Tip_Item(tooltip, slot, bag, bEquiped, reserved, tableId, buyPrice, formType, preInfo, ISEXTRACTION)
  local name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, sellType, equipKind, offlineTime, isTimeLimit, nKind, MaxInBag, IsFreeze
  if tableId == nil then
    name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, sellType, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, nitemId, nKind, MaxInBag, IsFreeze = GetItemInfoBySlot(bag, slot, reserved)
  else
    name, icon, quality, minLevel, job, sex, isUnique, canBank, canSell, canDiscard, bindinfo, count, maxCount, itemType, desc1, desc2, desc3, leftEffTime, sellPrice, sellType, equipKind, offlineTime, isTimeLimit, EquipPoint, SkillId, RuneLv, nitemId, nKind, MaxInBag, IsFreeze = GetItemInfoBySlot(-1, tableId, reserved)
  end
  if name and icon and icon ~= "" then
    tooltip = WindowToTooltip(tooltip)
    Lua_Tip_Begin(tooltip, nil, "true")
    if itemType == 8 then
      _Tip_ShowCommonInfoHead(tooltip, name, quality, minLevel, nil, icon, sex, job, 0, nil, nil)
      if formType == nil then
        if maxCount > 1 then
          if tableId == nil and count < maxCount and count > 0 then
            itemid = tooltip:InsertLeftText("当前数量 " .. tostring(count) .. "  最大数量 " .. tostring(maxCount), colorWhite, "", 0, 0)
          else
            itemid = tooltip:InsertLeftText("最大数量 " .. tostring(maxCount), colorWhite, "", 0, 0)
          end
        end
      elseif formType == FORM_TYPE_SHORTCUT and MaxInBag > 1 and tableId == nil and MaxInBag > 0 then
        itemid = tooltip:InsertLeftText("当前数量 " .. tostring(MaxInBag), colorWhite, "", 0, 0)
      end
      local name, icon, types, value, nCompose, NextId
      if tableId == nil then
        name, icon, types, value, nCompose, NextId = GetGemInfoEx(bag, slot, reserved)
      else
        name, icon, types, value, nCompose, NextId = GetGemInfoEx(-1, slot, tableId)
      end
      if nKind ~= 7 and name ~= nil then
        if value and value > 0 then
          tooltip:InsertLeftText("神玉属性 ", colorUserDefine1, "", 0, 0)
          if NextId == 0 then
            tooltip:InsertLeftText(string.format("%s +%d%%", name, value), colorGreen, "", 0, 0)
          else
            local nameNew, iconNew, typesNew, valueNew, nComposeNew, NextIdNew = GetGemInfoEx(-1, NextId, NextId)
            tooltip:InsertLeftPraseText(string.format("%s +%d%% {#R^(下一级: +%d%%)}", name, value, valueNew), colorGreen, "", 0, 0)
          end
          tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
          local listAttr
          if tableId == nil then
            listAttr = GetItemGemAttr(bag, slot, reserved)
          else
            listAttr = GetItemGemAttr(-1, slot, tableId)
          end
          if preInfo and preInfo == true then
            local bagMain, slotMain = GetNpcFunctionEquip(NpcFunction.NPC_FUNC_GEM_SACRIFICE, GEM_SENIORBLEND.MAINSLOT)
            if slotMain then
              listAttr = GetItemGemAttr(bagMain, slotMain, 0)
            end
          end
          if listAttr ~= nil and listAttr[1].MainAttrValue ~= 0 and listAttr[1].MainAttrDes ~= "" then
            if listAttr[1].BlendLev > 2 then
              tooltip:InsertLeftText(string.format("注灵等级%d", listAttr[1].BlendLev - 2), colorUserDefine1, "", 0, 0)
            end
            for i = 0, listAttr[1].AttrCount - 1 do
              local strAttr = ""
              strAttr = string.format("%s + %d%%", listAttr[1][i].AttrDes, listAttr[1][i].AttrValue)
              tooltip:InsertLeftText(strAttr, gUI_Color_GemBlend[listAttr[1].BlendLev - 1], "", 0, 0)
            end
          end
          if ISEXTRACTION and ISEXTRACTION == true then
            local listAttrEx
            local _, bagEm, slotEm = GetEquipExtractionInfo()
            if slotEm then
              listAttrEx = GetItemGemExInfo(bagEm, slotEm, 0)
              if listAttrEx ~= nil and listAttrEx[0] ~= nil then
                if listAttrEx[0].BlendLev > 2 then
                  tooltip:InsertLeftText(string.format("注灵等级%d", listAttrEx[0].BlendLev - 2), colorWhite, "", 0, 0)
                end
                for i = 0, #listAttrEx do
                  local strAttr = ""
                  strAttr = string.format("%s + %d%%", listAttrEx[i].AttrDes, listAttrEx[i].AttrValue)
                  tooltip:InsertLeftText(strAttr, gUI_Color_GemBlend[listAttrEx[i].BlendLev - 1], "", 0, 0)
                end
              end
            end
          end
        end
      else
        tooltip:InsertLeftText("灵符属性 ", colorUserDefine1, "", 0, 0)
        local name1, attr1, attrRate
        if tableId == nil then
          name1, attr1, attrRate = GetGemInfoBySlot(bag, slot, reserved)
        else
          name1, attr1, attrRate = GetGemInfoBySlot(-1, tableId, 0)
        end
        if name1 and attrRate == 0 then
          tooltip:InsertLeftText(string.format("%s +%d", name1, attr1), colorYellow, "", 0, 0)
        elseif attrRate ~= 0 and name1 then
          tooltip:InsertLeftText(string.format("%s +(%d~%d)", name1, attr1, attrRate), colorYellow, "", 0, 0)
        end
      end
      if nCompose ~= -1 and nCompose then
        tooltip:InsertLeftText("可合成", colorYellow, "", 0, 0)
      end
      _Tip_ShowCommonInfoMid(tooltip, isUnique, bindinfo, itemType)
      local gemLev
      if tableId == nil then
        gemLev = GetGemBlendBaseInfo(bag, slot, reserved)
      else
        gemLev = GetGemBlendBaseInfo(-1, tableId, 0)
      end
      if gemLev >= 3 then
        local canSign, bSigned, bUnsigned, unsignTime = GetEquipSignInfo(bag, slot, reserved)
        if canSign ~= nil then
          if canSign == true then
            if bSigned == true then
              if bUnsigned == true then
                if unsignTime > 0 then
                  tooltip:InsertLeftText("已取消签名，不可转移", colorPurper, "", 0, 0)
                  tooltip:InsertLeftText("剩余" .. Lua_Tip_ConVertTimesNew(unsignTime * 1000) .. "可解除签名", colorPurper, "", 0, 0)
                else
                  tooltip:InsertLeftText("可签名", colorPurper, "", 0, 0)
                end
              else
                tooltip:InsertLeftText("已签名，不可转移，", colorPurper, "", 0, 0)
              end
            else
              tooltip:InsertLeftText("可签名", colorPurper, "", 0, 0)
            end
          else
            tooltip:InsertLeftText("不可签名", colorPurper, "", 0, 0)
          end
        end
      end
      _Tip_ShowCommonInfoEnd(tooltip, canBank, canSell, canDiscard, desc1, desc2, desc3, sellPrice, sellType, buyPrice, leftEffTime, bag, slot, reserved, offlineTime, isTimeLimit, IsFreeze)
    elseif itemType == 5 then
      if bag == nil then
        bag = -1
        slot = tableId
      end
      local a = {}
      local s = {}
      local petName
      petName, needLevel, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], s[0], s[1], s[2], s[3] = GetPetBaseInfoBySlot(bag, slot, reserved)
      if petName then
        if bindinfo == 1 then
          petName = petName .. tostring("(已绑定)")
        end
        tooltip:InsertLeftText(petName, colorYellow, "", 0, 0)
        tooltip:InsertLeftText("携带等级 " .. tostring(needLevel), colorWhite, "", 0, 0)
        _Pet_Attr_Map = {
          [0] = "力量",
          [1] = "敏捷",
          [2] = "智力",
          [3] = "体质",
          [4] = "力量资质",
          [5] = "敏捷资质",
          [6] = "智力资质",
          [7] = "体质资质"
        }
        for i = 0, 7 do
          local color = gUI_GOODS_QUALITY_COLOR_PREFIX[0]
          if i >= 4 then
            if a[i] > 1200 and a[i] <= 1500 then
              color = gUI_GOODS_QUALITY_COLOR_PREFIX[1]
            elseif a[i] > 1500 and a[i] <= 2000 then
              color = gUI_GOODS_QUALITY_COLOR_PREFIX[2]
            elseif a[i] > 2000 and a[i] <= 2300 then
              color = gUI_GOODS_QUALITY_COLOR_PREFIX[3]
            elseif a[i] > 2300 and a[i] <= 3000 then
              color = gUI_GOODS_QUALITY_COLOR_PREFIX[4]
            elseif a[i] > 3000 then
              color = gUI_GOODS_QUALITY_COLOR_PREFIX[5]
            else
              color = gUI_GOODS_QUALITY_COLOR_PREFIX[0]
            end
          end
          tooltip:InsertLeftText(tostring(_Pet_Attr_Map[i]) .. "：" .. a[i], color, "", 0, 0)
        end
        for i = 0, 3 do
          local name, skillIcon, category, Type, desc = GetSkillBaseInfo(s[i])
          if name ~= nil then
            itemid = tooltip:InsertLeftImage(tostring(skillIcon), 25, 25, 0, 6, false)
            tooltip:InsertText(tostring(name), colorGreen, itemid, "RIGHTBOTTOM", 0, 0)
            tooltip:InsertLeftText(tostring(desc), colorWhite, "", 0, 0)
          end
        end
        if buyPrice ~= nil and buyPrice ~= 0 then
          local itemid = tooltip:InsertLeftText("单价 ", colorWhite, "", 0, 0)
          _Tip_ShowMoney(tooltip, buyPrice, itemid)
        end
      else
        local mountName, hisLevel, enhanceLevel, needLevel, speed = GetMountBaseInfoBySlot(bag, slot, reserved)
        _Tip_ShowCommonInfoHead(tooltip, name, quality, minLevel, nil, icon, sex, job, 0, nil, nil, enhanceLevel)
        local maxMount = GetMountMaxLevel(slot, bag, reserved)
        if enhanceLevel ~= -1 and enhanceLevel ~= nil then
          tooltip:InsertLeftText(string.format("当前位阶  %s", gUI_Mount_Rank[enhanceLevel]), colorYellow, "", 0, 0)
          tooltip:InsertLeftText(string.format("最大位阶  %s", gUI_Mount_Rank[maxMount]), colorYellow, "", 0, 0)
        end
        if speed ~= -1 and speed ~= nil then
          tooltip:InsertLeftText(string.format("骑乘后增加人物%d%%移动速度", speed), colorYellow, "", 0, 0)
        end
        _Tip_ShowCommonInfoMid(tooltip, isUnique, bindinfo, itemType)
        _Tip_ShowCommonInfoEnd(tooltip, canBank, canSell, canDiscard, desc1, desc2, desc3, sellPrice, sellType, buyPrice, leftEffTime, bag, slot, reserved, offlineTime, isTimeLimit, IsFreeze)
      end
    elseif itemType == 2 then
      local level, maxXiuWeiLevel, xiuwei, MaxForgeLevel, maxClrLevel, CurClrLevel
      if tableId == nil then
        _, _, level, maxXiouweiLevel, maxForgeLevel, xiuwei, CurClrLevel = GetForgeEquipInfo(slot, bag, reserved)
      else
        _, _, level, maxXiouweiLevel, maxForgeLevel, xiuwei = GetForgeEquipInfoByTableId(tableId)
      end
      if not _Tip_CheckAviable(tooltip, level, "level") then
        return
      end
      local scroe, BlendTimes, stars = GetEquipScore(bag, slot, reserved)
      _Tip_ShowCommonInfoHeadEquip(tooltip, name, quality, minLevel, nil, icon, sex, job, level, EquipPoint, MaxForgeLevel, level, CurClrLevel, bindinfo, itemType, bEquiped, equipKind, stars, scroe)
      tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
      _Tip_ShowForgeAttr(tooltip, slot, level, bag, xiuwei, reserved, tableId, false, equipKind)
      if CurClrLevel ~= nil then
        _Tip_ShowForgeExAttr(tooltip, slot, CurClrLevel, bag, maxForgeLevel, true, reserved, tableId)
        tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
      end
      if CurClrLevel ~= level and level > 0 then
        tooltip:InsertLeftText("噬灵状态:当前锻造成功率  +10%", colorYellow, "", 0, 0)
      end
      local attrList = {}
      GemId0, GemIcon0, GemType0, GemValue0, GemName0, GemNa0, GemId1, GemIcon1, GemType1, GemValue1, GemName1, GemNa1, GemId2, GemIcon2, GemType2, GemValue2, GemName2, GemNa2 = GetEquipHasGemInfo(2, bag, slot, reserved)
      local NewValue1, NewValue2, NewValue3
      if tabelId == nil then
        attrList = GetBlendAttr(slot, bag, reserved)
        local pItem
        if attrList[0] ~= nil then
          tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
        end
        if attrList[0] ~= nil then
          for n = 0, table.maxn(attrList) do
            for m = n, table.maxn(attrList) do
              if attrList[m].typeId > attrList[n].typeId then
              end
            end
          end
          for i = 0, table.maxn(attrList) do
            if GemId0 then
              if GemType0 == attrList[i].typeId then
                NewValue1 = math.floor(attrList[i].value * GemValue0 / 100)
              end
              if GemType1 == attrList[i].typeId then
                NewValue2 = math.floor(attrList[i].value * GemValue1 / 100)
              end
              if GemType2 == attrList[i].typeId then
                NewValue3 = math.floor(attrList[i].value * GemValue2 / 100)
              end
            end
            local colorBlend = Lua_ToolTip_GetBlendColor(nitemId, attrList[i].typeId, attrList[i].value)
            if Lua_Tip_NeedShowRate(attrList[i].typeId) then
              local itemId1 = tooltip:InsertLeftText("[融魂属性]" .. tostring(attrList[i].name) .. "  " .. "+" .. string.format("%.2f", Lua_Tip_PostProcessVal(attrList[i].typeId, attrList[i].value)) .. "%", colorBlend, "fzheiti_11", 0, 0)
            else
              local itemId1 = tooltip:InsertLeftText("[融魂属性]" .. tostring(attrList[i].name) .. "  " .. "+" .. tostring(attrList[i].value), colorBlend, "fzheiti_11", 0, 0)
            end
          end
          tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
        end
      end
      if tabelId == nil and BlendTimes and BlendTimes ~= 0 then
        local attrList1 = {}
        attrList1 = GetBlendGift(slot, bag, reserved, 0)
        local pItem
        if attrList1[0] ~= nil then
          for n = 0, table.maxn(attrList1) do
            for m = n, table.maxn(attrList1) do
              if attrList1[m].typeId > attrList1[n].typeId then
                pItem = attrList1[n]
                attrList1[n] = attrList1[m]
                attrList1[m] = pItem
              end
            end
          end
          for i = 0, table.maxn(attrList1) do
            if Lua_Tip_NeedShowRate(attrList1[i].typeId) then
              tooltip:InsertLeftText("融魂次数:" .. tostring(BlendTimes) .. "" .. "(" .. tostring(attrList1[i].name) .. " " .. "+" .. string.format("%.2f", Lua_Tip_PostProcessVal(attrList1[i].typeId, attrList1[i].value)) .. "%" .. ")", colorPurper, "fzheiti_11", 0, 0)
            else
              tooltip:InsertLeftText("融魂次数:" .. tostring(BlendTimes) .. "" .. "(" .. tostring(attrList1[i].name) .. " " .. "+" .. tostring(attrList1[i].value) .. ")", colorPurper, "fzheiti_11", 0, 0)
            end
          end
        else
          tooltip:InsertLeftText("融魂次数:" .. tostring(BlendTimes), colorPurper, "fzheiti_11", 0, 0)
        end
        tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
      end
      local noGemAl = false
      if GemId0 and GemId0 ~= 0 then
        local itemid1 = tooltip:InsertLeftImage(tostring(GemIcon0), 20, 20, 0, 0, false)
        itemid1 = tooltip:InsertText(tostring(GemNa0), colorGreen, itemid1, "LEFT", 25, 3)
        tooltip:InsertLeftText("     " .. tostring(GemName0) .. "  +" .. tostring(NewValue1) .. " ", colorGreen, "", 0, 0)
        noGemAl = true
        local listAttr = GetItemGemExInfo(bag, slot, reserved)
        local attrList = GetBlendAttr(slot, bag, reserved)
        local isActive = false
        if attrList[0] ~= nil and listAttr[0] ~= nil then
          for i = 0, #listAttr do
            for j = 0, #attrList do
              if listAttr[i].typeId == attrList[j].typeId then
                isActive = true
                currVal = math.floor(attrList[j].value * listAttr[i].AttrValue)
                if Lua_Tip_NeedShowRate(attrList[j].typeId) then
                  tooltip:InsertLeftText("     " .. tostring(listAttr[i].AttrDes) .. "  +" .. string.format("%.2f", Lua_Tip_PostProcessVal(attrList[i].typeId, currVal / 10000)) .. "%" .. " ", colorGreen, "", 0, 0)
                else
                  tooltip:InsertLeftText("     " .. tostring(listAttr[i].AttrDes) .. "  +" .. string.format("%.f", currVal / 100) .. " ", colorGreen, "", 0, 0)
                end
              end
            end
            if not isActive then
              tooltip:InsertLeftText("     " .. tostring(listAttr[i].AttrDes) .. "  " .. "(未激活) ", colorGrey, "", 0, 0)
            end
          end
        end
      end
      if tableId == nil then
        do
          local isEmbbed = GetItemGemInfo(bag, slot, reserved)
          if isEmbbed == nil then
          elseif isEmbbed > 0 and not noGemAl then
            tooltip:InsertLeftText("可镶嵌神玉", colorBlue, "", 0, 0)
          end
        end
      else
      end
      tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
      if tableId == nil then
        _Tip_EquipSuit(tooltip, bag, slot, reserved)
      else
        _Tip_EquipSuit(tooltip, -1, tableId, reserved)
      end
      local AuthAttr, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney
      if tableId == nil then
        AuthAttr, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney = GetAuthenticateAttr(bag, slot, reserved)
      else
        AuthAttr, name, icon, quality, reidentValue, materialName, materialIcon, BagNumInBag, BagNumNeed, bagNeedMoney = GetAuthenticateAttr(-1, tableId, reserved)
      end
      if not AuthAttr or AuthAttr == -1 then
      else
      end
      local canDo, Time, deltaTime, des, value, indexID
      if tableId == nil then
        canDo = GetInEffectiveInfos(bag, slot, reserved)
        Time, deltaTime, des, value, indexID = GetIneffectiveGemInfosEx(bag, slot, reserved)
      else
        canDo = GetInEffectiveInfos(-1, tableId, reserved)
        Time, deltaTime, des, value, indexID = GetIneffectiveGemInfosEx(-1, tableId, reserved)
      end
      if canDo == 1 and Time == 0 then
        tooltip:InsertLeftText("可附魔", colorPurper, "", 0, 0)
      elseif canDo == 1 and Time ~= 0 then
      else
        tooltip:InsertLeftText("不可附魔", colorPurper, "", 0, 0)
      end
      if Time == 0 then
      else
        local nameItem = GetItemInfoBySlot(-1, indexID, nil)
        if deltaTime > 0 then
          local desc2 = ""
          desc2 = desc2 .. tostring(nameItem) .. ":{#Y^" .. " +" .. tostring(math.floor(value / 100)) .. tostring(des) .. "}"
          tooltip:InsertLeftPraseText(tostring(desc2), colorYellow, "fzheiti_11", 0, 0)
          local desc3 = ""
          desc3 = "(剩余" .. Lua_Tip_ConVertTimesNew(deltaTime * 1000) .. ")"
          tooltip:InsertLeftText(tostring(desc3), colorYellow, "fzheiti_11", 0, 0)
        else
          local desc2 = ""
          desc2 = desc2 .. tostring(nameItem) .. ":{#R^" .. " +" .. tostring(math.floor(value / 100)) .. tostring(des) .. " " .. "}"
          tooltip:InsertLeftPraseText(tostring(desc2), colorRed, "fzheiti_11", 0, 0)
          tooltip:InsertLeftText(tostring("(已过期)"), colorRed, "fzheiti_11", 0, 0)
        end
      end
      local canSign, bSigned, bUnsigned, unsignTime = GetEquipSignInfo(bag, slot, reserved)
      if canSign ~= nil then
        if canSign == true then
          if bSigned == true then
            if bUnsigned == true then
              if unsignTime > 0 then
                tooltip:InsertLeftText("已取消签名，不可转移，+10体质", colorPurper, "", 0, 0)
                tooltip:InsertLeftText("剩余" .. Lua_Tip_ConVertTimesNew(unsignTime * 1000) .. "可解除签名", colorPurper, "", 0, 0)
              else
                tooltip:InsertLeftText("可签名", colorPurper, "", 0, 0)
              end
            else
              tooltip:InsertLeftText("已签名，不可转移，+10体质", colorPurper, "", 0, 0)
            end
          else
            tooltip:InsertLeftText("可签名", colorPurper, "", 0, 0)
          end
        else
          tooltip:InsertLeftText("不可签名", colorPurper, "", 0, 0)
        end
      end
      local nEInCtrl, SuitJob, nEPoint, nForgeLev, nClrLev, nUseLev
      if tableId == nil then
        nEInCtrl, SuitJob, nEPoint, nForgeLev, nClrLev, nUseLev = GetInheritInfo(slot, bag, reserved)
      else
        nEInCtrl, SuitJob, nEPoint, nForgeLev, nClrLev, nUseLev = GetInheritInfo(tableId, -1, reserved)
      end
      if nEInCtrl and nEInCtrl <= 0 then
        tooltip:InsertLeftText("不可当作传承主装备", colorPurper, "", 0, 0)
      end
      if isUnique == 1 then
        itemid = tooltip:InsertLeftText("唯一", colorPurper, "fzheiti_11", 0, 0)
      end
      _Tip_ShowCommonInfoEnd(tooltip, canBank, canSell, canDiscard, desc1, desc2, desc3, sellPrice, sellType, buyPrice, leftEffTime, bag, slot, reserved, offlineTime, isTimeLimit, IsFreeze)
    else
      local maxLevel, maxTimes, leftTimes, speakTime, CDTime, leftCDTime, id
      if tableId == nil then
        maxLevel, maxTimes, leftTimes, speakTime, CDTime, leftCDTime, id, faerieLv, canFusionDecomposs = GetItemExInfo(bag, slot, reserved)
      else
        maxLevel, maxTimes, leftTimes, speakTime, CDTime, leftCDTime, id, faerieLv, canFusionDecomposs = GetItemExInfoByTableId(tableId)
      end
      if not _Tip_CheckAviable(tooltip, maxLevel, "maxLevel") then
        return
      end
      _Tip_ShowCommonInfoHead(tooltip, name, quality, minLevel, maxLevel, icon, sex, job, 0, SkillId, RuneLv)
      _Tip_ShowCommonInfoMid(tooltip, isUnique, bindinfo, itemType)
      if itemType == 1 then
        itemid = tooltip:InsertLeftText("任务道具", colorBlue, "", 0, 0)
      end
      if maxTimes > 1 then
        tooltip:InsertLeftText(string.format("使用次数：%d/%d", leftTimes, maxTimes), colorWhite, "", 0, 0)
      end
      if CDTime > 0 then
        itemid = tooltip:InsertLeftText("冷却时间 " .. tostring(Lua_Tip_ConvertTime(CDTime)), colorWhite, "", 0, 0)
      end
      if leftCDTime > 0 then
        if leftCDTime > 120000 then
          itemid = tooltip:InsertLeftText("剩余时间 " .. tostring(Lua_Tip_ConvertTimeToMin(leftCDTime)), colorYellow, "", 0, 0)
        else
          local des = string.format("剩余时间 $CT(39,%d)$", id)
          itemid = tooltip:InsertLeftText(tostring(des), colorYellow, "", 0, 0)
        end
      end
      if speakTime > 0 then
        itemid = tooltip:InsertLeftText("吟唱时间 " .. tostring(Lua_Tip_ConvertTime(speakTime)), colorWhite, "", 0, 0)
      end
      if formType == nil then
        if maxCount > 1 then
          if tableId == nil and count < maxCount and count > 0 then
            itemid = tooltip:InsertLeftText("当前数量 " .. tostring(count) .. "  最大数量 " .. tostring(maxCount), colorWhite, "", 0, 0)
          else
            itemid = tooltip:InsertLeftText("最大数量 " .. tostring(maxCount), colorWhite, "", 0, 0)
          end
        end
      elseif formType == FORM_TYPE_SHORTCUT and MaxInBag > 1 and tableId == nil and MaxInBag > 0 then
        itemid = tooltip:InsertLeftText("当前数量 " .. tostring(MaxInBag), colorWhite, "", 0, 0)
      end
      if faerieLv ~= nil and 0 < faerieLv then
        local color = colorWhite
        local desc, param1, param2
        if tableId == nil then
          desc, param1, param2 = GetFaerieItemInfo(bag, slot, reserved)
        else
          desc, param1, param2 = GetFaerieItemInfoByTableId(tableId)
        end
        if desc ~= nil then
          local _, current = GetFaerieBase()
          local psy = GetFaeriePsychokinesis()
          if current < faerieLv then
            color = colorRed
          end
          tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
          itemid = tooltip:InsertLeftText("仙灵境界    " .. gUI_Faerie_LvName[faerieLv], color, "", 0, 0)
          if canFusionDecomposs then
            tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
            itemid = tooltip:InsertLeftText(desc .. "    " .. tostring(param1), colorGreen, "", 0, 0)
          else
            color = colorWhite
            if param1 > psy then
              color = colorRed
            end
            itemid = tooltip:InsertLeftText("念力值    " .. tostring(param1), color, "", 0, 0)
            itemid = tooltip:InsertLeftText("成功率    " .. tostring(param2) .. "%", colorWhite, "", 0, 0)
          end
          tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 5, 0, 0, false)
        end
      end
      _Tip_ShowCommonInfoEnd(tooltip, canBank, canSell, canDiscard, desc1, desc2, desc3, sellPrice, sellType, buyPrice, leftEffTime, bag, slot, reserved, offlineTime, isTimeLimit, IsFreeze)
    end
    Lua_Tip_End(tooltip)
  end
end
function Lua_ToolTip_GetBlendColor(itemdId, BlendType, BlendValue)
  local BlendLev = GetItemBlendColor(itemdId, BlendType, BlendValue)
  if BlendLev >= 0 and BlendLev <= 6 then
    return gUI_GOODS_QUAL_COLORNEW[BlendLev]
  else
    return gUI_GOODS_QUAL_COLORNEW[7]
  end
end
function Lua_Tip_ConvertTime(timed, igoreSeconds)
  local days = math.floor(timed / 86400000)
  local hours = math.floor((timed - days * 86400000) / 3600000)
  local mins = math.floor((timed - days * 86400000 - hours * 3600000) / 60000)
  local seconds = (timed - days * 86400000 - hours * 3600000 - mins * 60000) / 1000
  local strTime = ""
  if days > 0 then
    strTime = strTime .. string.format("%d", days) .. "天"
  end
  if hours > 0 then
    strTime = strTime .. string.format("%d", hours) .. "小时"
  end
  if mins > 0 then
    strTime = strTime .. string.format("%d", mins) .. "分钟"
  end
  if not igoreSeconds and seconds > 0 then
    strTime = strTime .. Lua_UI_StringFormatNum(seconds) .. "秒"
  end
  return strTime
end
function Lua_Tip_ConVertTimesNew(timed)
  local strTime = ""
  local days = math.floor(timed / 86400000)
  local hours = math.floor((timed - days * 86400000) / 3600000)
  local mins = math.floor((timed - days * 86400000 - hours * 3600000) / 60000)
  local seconds = (timed - days * 86400000 - hours * 3600000 - mins * 60000) / 1000
  local strTime = ""
  if days > 0 then
    if hours > 0 then
      strTime = strTime .. string.format("%d", days) .. "天" .. string.format("%d", hours) .. "小时"
    else
      strTime = strTime .. string.format("%d", days) .. "天"
    end
  elseif hours > 0 then
    if mins > 0 then
      strTime = strTime .. string.format("%d", hours) .. "小时" .. string.format("%d", mins) .. "分钟"
    else
      strTime = strTime .. string.format("%d", hours) .. "小时"
    end
  elseif mins > 0 then
    if seconds > 0 then
      strTime = strTime .. string.format("%d", mins) .. "分钟" .. Lua_UI_StringFormatNum(seconds) .. "秒"
    else
      strTime = strTime .. string.format("%d", mins) .. "分钟"
    end
  elseif seconds > 0 then
    strTime = strTime .. Lua_UI_StringFormatNum(seconds) .. "秒"
  end
  return strTime
end
function Lua_Tip_ShowObjTooltip(objId, name)
  local tooltip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  UI_Chat_CloseBtnClick()
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(name), colorGreen, "kaiti_13", 0, 0)
  local list = GetQuestMonsterInfo(objId)
  _Tip_ShowQuestList(tooltip, list)
  Lua_Tip_End(tooltip)
  _Tip_AdjustPos(tooltip)
end
function Lua_Tip_ShowCharTooltip(objId, ctype)
  local tooltip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  local info = GetCharaTipInfo(objId)
  if info == nil then
    return
  end
  Lua_Tip_Begin(tooltip)
  if ctype == gUI_CharType.CHAR_PLAYER then
    local name, guildName, title, level, job, isDead, isMember = GetCharaTipInfo(objId)
    local color = colorWhite
    if isMember then
      color = colorBluePurper
    end
    local itemid = tooltip:InsertLeftText(tostring(name), color, "kaiti_13", 0, 0)
    itemid = tooltip:InsertText(tostring(title), colorLightYellow, itemid, "RIGHTTOP", 40, 0)
    if isDead == 1 then
      tooltip:InsertLeftText("（尸体）", colorGrey, "", 0, 0)
    end
    tooltip:InsertLeftText(tostring(guildName), colorGreen, "", 0, 0)
    tooltip:InsertLeftText(tostring(gUI_MenPaiName[job] .. " " .. tostring(level) .. "级"), colorWhite, "", 0, 0)
  elseif ctype == gUI_CharType.CHAR_SPIRIT then
    local name, description = GetCharaTipInfo(objId)
    tooltip:InsertLeftText(tostring(name), colorBluePurper, "kaiti_13", 0, 0)
    if description then
      tooltip:InsertLeftText(tostring(description), colorWhite, "", 0, 0)
    end
  elseif ctype == gUI_CharType.CHAR_NPC then
    local name, level, subName, monsterRank, nameColor, bGuard = GetCharaTipInfo(objId)
    local tColor = {
      [1] = colorGreen,
      [2] = colorYellow,
      [3] = colorRed
    }
    local tColor2 = {
      [0] = colorRed,
      [1] = colorRed,
      [2] = colorYellow,
      [3] = colorGreen,
      [4] = colorGrey
    }
    tooltip:InsertLeftText(tostring(name), tColor[nameColor], "kaiti_13", 0, 0)
    if bGuard == true then
      tooltip:InsertLeftText("护送目标", colorLightYellow, "", 0, 0)
    else
      tooltip:InsertLeftText(tostring(subName), colorLightYellow, "", 0, 0)
    end
    local playLevel = gUI_MainPlayerAttr.Level
    local delLevel = playLevel - level
    local tColorId
    if delLevel >= 10 then
      tColorId = 4
    elseif delLevel >= 4 and delLevel <= 9 then
      tColorId = 3
    elseif delLevel >= -3 and delLevel <= 3 then
      tColorId = 2
    elseif delLevel >= -9 and delLevel <= -4 then
      tColorId = 1
    elseif delLevel <= -10 then
      tColorId = 0
    end
    local strMonsterType = ""
    if monsterRank == 0 then
    elseif monsterRank == 1 or monsterRank == 2 then
      strMonsterType = "(银英) "
    elseif monsterRank == 3 or monsterRank == 4 then
      strMonsterType = "(精英) "
    end
    local itemid = tooltip:InsertLeftText(strMonsterType, colorBlue, "", 0, 0)
    if strMonsterType == "" then
      if delLevel < -10 then
        itemid = tooltip:InsertLeftText("等级" .. "??", tColor2[tColorId], "", 0, 0)
      else
        itemid = tooltip:InsertLeftText("等级" .. tostring(level), tColor2[tColorId], "", 0, 0)
      end
    elseif delLevel < -10 then
      itemid = tooltip:InsertText("等级" .. "??", tColor2[tColorId], itemid, "RIGHTTOP", 0, 0)
    else
      itemid = tooltip:InsertText("等级" .. tostring(level), tColor2[tColorId], itemid, "RIGHTTOP", 0, 0)
    end
    if ctype == gUI_CharType.CHAR_NPC then
      if not IsCharEnemy(objId) then
        local list = GetNpcQuestList(objId, 1)
        if list[0] ~= nil then
          for i = 0, table.maxn(list) do
            local strTitle = GetQuestInfoLua(list[i], gUI_QuestRewardItemSourceID.INVALID)
            tooltip:InsertLeftText(strTitle .. "(已完成)", colorYellow, "", 0, 0)
          end
        end
        list = GetNpcQuestList(objId, 3)
        if list[0] ~= nil then
          for i = 0, table.maxn(list) do
            local strTitle = GetQuestInfoLua(list[i], gUI_QuestRewardItemSourceID.INVALID)
            tooltip:InsertLeftText(strTitle .. "（未完成）", colorYellow, "", 0, 0)
            local conList = GetQuestTargetCon(list[i])
          end
        end
        list = GetNpcQuestList(objId, 4)
        if list[0] ~= nil then
          for i = 0, table.maxn(list) do
            local strTitle = GetQuestInfoLua(list[i], gUI_QuestRewardItemSourceID.INVALID)
            tooltip:InsertLeftText(strTitle .. "(可接取)", colorYellow, "", 0, 0)
          end
        end
      else
        local list = GetQuestMonsterInfo(objId)
        if list[0] ~= nil then
          _Tip_ShowQuestList(tooltip, list)
        end
      end
    end
  end
  local posx, posy = GetMousePos()
  Lua_Tip_End(tooltip)
  _Tip_AdjustPos(tooltip)
end
function Lua_Tip_ShowOfflineTooltip(name)
  local tooltip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  UI_Chat_CloseBtnClick()
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(name), colorBluePurper, "kaiti_13", 0, 0)
  tooltip:InsertLeftText("（离线）", colorGrey, "", 0, 0)
  Lua_Tip_End(tooltip)
  _Tip_AdjustPos(tooltip)
end
function Lua_Tip_ShowPartyTooltip(name)
  local tooltip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  UI_Chat_CloseBtnClick()
  local _, _, level, max_hp, hp, max_mp, mp, is_leader, head_id, _, _, offline, menpai, objid, _, isDead, guildName, mSenceId, mBSameSence, _, _, mSenceKey, _, _, _, lineId = GetTeamMemberInfo(name, true)
  if level == nil then
    return
  end
  Lua_Tip_Begin(tooltip)
  local itemid = tooltip:InsertLeftText(tostring(name), colorBluePurper, "kaiti_13", 0, 0)
  if isDead == 0 then
    tooltip:InsertLeftText("（尸体）", colorGrey, "", 0, 0)
  end
  tooltip:InsertLeftText(tostring(guildName), colorGreen, "", 0, 0)
  tooltip:InsertLeftText(tostring(gUI_MenPaiName[menpai]) .. " " .. tostring(level) .. "级", colorWhite, "", 0, 0)
  local MapName = GetMapNameByMAPId(mSenceId)
  if MapName ~= nil then
    if lineId ~= -1 then
      tooltip:InsertLeftText("区域 " .. tostring(MapName) .. "(" .. tostring(lineId + 1) .. "线)", colorWhite, "", 0, 0)
    else
      tooltip:InsertLeftText("区域 " .. tostring(MapName), colorWhite, "", 0, 0)
    end
  end
  Lua_Tip_End(tooltip)
  _Tip_AdjustPos(tooltip)
end
function Lua_Tip_HideObjTooltip()
  WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"):SetProperty("Visible", "false")
end
function Lua_Tip_ConvertTimeToMin(timed)
  local days = math.floor(timed / 86400000)
  local hours = math.floor((timed - days * 86400000) / 3600000)
  local mins = math.floor((timed - days * 86400000 - hours * 3600000) / 60000)
  if days > 0 then
    return string.format("%d天%d小时%d分钟", days, hours, mins)
  elseif hours > 0 then
    return string.format("%d小时%d分钟", hours, mins)
  else
    return string.format("%d分钟", mins)
  end
end
function Lua_Tip_Property(tooltip, desc)
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(desc), colorUserDefine1, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function Lua_Tip_ChatFace(tooltip, desc)
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(desc), colorUserDefine1, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function Lua_Tip_BankBag(tooltip, desc)
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(desc), "FF4169E1", "", 0, 0)
  Lua_Tip_End(tooltip)
end
function Lua_Tip_Skill(tooltip, skillId, currentlv, bSkillUI, bShowNext)
  if skillId == nil then
    return
  end
  tooltip = WindowToTooltip(tooltip)
  local c = {}
  local a = {}
  local b = {}
  local skillType, chargeTime, channelTime, maxRange, cooldown, desc, lv, maxlv
  local name, icon, category = GetSkillBaseInfo(skillId)
  if category == 4 then
    c[1], a[1], b[1], c[2], a[2], b[2], c[3], a[3], b[3], skillType, chargeTime, channelTime, maxRange, cooldown, desc, desc2, _, _ = GetSkillExInfo(skillId, 1)
  else
    if not currentlv then
      return
    end
    c[1], a[1], b[1], c[2], a[2], b[2], c[3], a[3], b[3], skillType, chargeTime, channelTime, maxRange, cooldown, desc, desc2, _, _, maxlv = GetSkillExInfo(skillId, currentlv)
  end
  local _, myLevel, myMenpai = GetMyLvlaAndNimbusLv()
  if name == nil then
    return
  end
  local maxHp, hp, maxSp, sp = GetSelfEaseChangeInfo()
  Lua_Tip_Begin(tooltip, 220, "false")
  local itemid = tooltip:InsertLeftText(tostring(name), colorDarkYellow, "kaiti_14", 55, 0)
  local itemidImg = tooltip:InsertImage(icon, 48, 48, itemid, "LEFTTOP", -55, 0)
  if currentlv and maxlv ~= -1 then
    local str = string.format("技能等级   %d/%d", currentlv, maxlv)
    tooltip:InsertLeftText(str, colorDarkYellow, "fzheiti_11", 57, -20)
    if currentlv == maxlv then
      tooltip:InsertImage(MAXSKILL, 49, 49, itemid, "LEFTTOP", -55, 0)
    end
  end
  if bSkillUI then
    if currentlv <= 0 and maxlv ~= -1 then
      tooltip:InsertText("未习得", colorRed, itemid, "BYRIGHT", 0, 0)
    elseif currentlv > 0 and currentlv <= maxlv and bShowNext then
      tooltip:InsertText("下一等级", colorRed, itemid, "BYRIGHT", 0, 0)
    end
  end
  tooltip:InsertImage(SKILLLINE, 220, 8, itemidImg, "RIGHTBOTTOM", -48, 0)
  if chargeTime ~= 0 and chargeTime ~= -1 and chargeTime > 0 then
    itemid = tooltip:InsertLeftText(tostring(Lua_Tip_ConvertTime(chargeTime)) .. tostring(gUI_SKILL_TYPE[skillType]), colorWhite, "fzheiti_11", 0, 0)
  elseif channelTime ~= 0 and channelTime ~= -1 and channelTime > 0 then
    itemid = tooltip:InsertLeftText(tostring(Lua_Tip_ConvertTime(channelTime)) .. tostring(gUI_SKILL_TYPE[skillType]), colorWhite, "fzheiti_11", 0, 0)
  else
    itemid = tooltip:InsertLeftText(tostring(gUI_SKILL_TYPE[skillType]), colorWhite, "fzheiti_11", 0, 0)
  end
  if maxRange > 0 then
    tooltip:InsertText(tostring(maxRange) .. "米施法距离", colorWhite, itemid, "BYRIGHT", 0, 0)
  end
  local strCondition = ""
  local strType = "点法力 "
  if myMenpai == gUI_MenPaiID.CK then
    strType = "点精力 "
  elseif myMenpai == gUI_MenPaiID.ZS then
    strType = "点怒气 "
  elseif myMenpai == gUI_MenPaiID.GS then
    strType = "点箭枝 "
  end
  for i = 1, 3 do
    if c[i] == 0 then
      local spConsu = 0
      if a[i] ~= -1 then
        spConsu = spConsu + a[i]
      end
      if b[i] ~= -1 then
        spConsu = spConsu + math.ceil(sp * b[i] / 100)
      end
      strCondition = strCondition .. tostring(spConsu) .. strType
    elseif c[i] == 21 then
      local hpConsu = 0
      if a[i] ~= -1 then
        hpConsu = hpConsu + a[i]
      end
      if b[i] ~= -1 then
        hpConsu = hpConsu + math.ceil(hp * b[i] / 100)
      end
      strCondition = strCondition .. tostring(hpConsu) .. "点气血 "
    elseif c[i] == 33 then
      strCondition = strCondition .. tostring(a[i]) .. "点精力 "
    elseif c[i] == 36 then
      strCondition = strCondition .. "[" .. tostring(a[i]) .. "]*" .. tostring(b[i]) .. " "
    elseif c[i] == 37 then
      strCondition = strCondition .. tostring(a[i]) .. "点法力 "
    end
  end
  if strCondition ~= "" then
    itemid = tooltip:InsertLeftText(strCondition, colorWhite, "fzheiti_11", 0, 2)
  end
  if cooldown > 0 then
    if strCondition ~= "" then
      tooltip:InsertText(tostring(Lua_Tip_ConvertTime(cooldown)) .. "冷却", colorWhite, itemid, "BYRIGHT", 0, 2)
    else
      tooltip:InsertLeftText(tostring(Lua_Tip_ConvertTime(cooldown)) .. "冷却", colorWhite, "fzheiti_11", 0, 2)
    end
  end
  tooltip:InsertLeftImage(SKILLLINE, 220, 8, 0, 0, false)
  if desc2 then
    tooltip:InsertLeftPraseText(tostring(desc2), "FF4169E1", "fzheiti_11", 0, 0)
  end
  tooltip:InsertLeftImage("UiIamge001:Image_Tooltip", 5, 3, 0, 0, false)
  if desc then
    tooltip:InsertLeftPraseText(tostring(desc), "FFF4A460", "fzheiti_11", 0, 0)
  end
  if bSkillUI and bShowNext then
    tooltip:InsertLeftImage(SKILLLINE, 220, 8, 0, 0, false)
    local needlv, blv, needSkill, needSkillLv, bSkill, needSkillP, bSkillP, needNimbus, bNimbus, needMoney, bMoney, StudyType, ItemName, nItemCount = GetSkillLVCondition(skillId)
    if needlv > 0 then
      if blv then
        tooltip:InsertLeftText("需要等级:" .. tostring(needlv) .. "级", colorWhite, "fzheiti_11", 0, 2)
      else
        tooltip:InsertLeftText("需要等级:" .. tostring(needlv) .. "级", colorRed, "fzheiti_11", 0, 2)
      end
    end
    if needSkillP > 0 then
      if bSkillP then
        tooltip:InsertLeftText("需要前置技能点:" .. tostring(needSkillP) .. "点", colorWhite, "fzheiti_11", 0, 2)
      else
        tooltip:InsertLeftText("需要前置技能点:" .. tostring(needSkillP) .. "点", colorRed, "fzheiti_11", 0, 2)
      end
    end
    if needSkill ~= "" and needSkillLv > 0 then
      if bSkill then
        tooltip:InsertLeftText("需要前置技能:" .. tostring(needSkill) .. tostring(needSkillLv) .. "级", colorWhite, "fzheiti_11", 0, 2)
      else
        tooltip:InsertLeftText("需要前置技能:" .. tostring(needSkill) .. tostring(needSkillLv) .. "级", colorRed, "fzheiti_11", 0, 2)
      end
    end
    if needNimbus > 0 then
      if bNimbus then
        tooltip:InsertLeftText("需消耗精魄值:" .. tostring(needNimbus) .. "点", colorWhite, "fzheiti_11", 0, 2)
      else
        tooltip:InsertLeftText("需消耗精魄值:" .. tostring(needNimbus) .. "点", colorRed, "fzheiti_11", 0, 2)
      end
    end
    if needMoney > 0 then
      if bMoney then
        itemid = tooltip:InsertLeftText("需消耗金钱:", colorWhite, "fzheiti_11", 0, 2)
      else
        itemid = tooltip:InsertLeftText("需消耗金钱:", colorRed, "fzheiti_11", 0, 2)
      end
      _Tip_ShowMoney(tooltip, needMoney, itemid)
    end
    if StudyType and ItemName then
      if nItemCount > 0 then
        tooltip:InsertLeftText("需消耗技能书:" .. ItemName, colorWhite, "fzheiti_11", 0, 2)
      else
        tooltip:InsertLeftText("需消耗技能书:" .. ItemName, colorRed, "fzheiti_11", 0, 2)
      end
    end
  end
  Lua_Tip_End(tooltip)
end
function Lua_Tip_ExpBar(curExp, maxExp)
  if maxExp == 0 then
    return
  end
  local tooltip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  UI_Chat_CloseBtnClick()
  tooltip = WindowToTooltip(tooltip)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(string.format("经验(%.2f%%)", math.floor(curExp / maxExp * 10000) / 100), colorDarkBlue, "kaiti_13", 0, 0)
  tooltip:InsertLeftText("60级经验满了之后，将不再获得经验。", colorWhite, "fzheiti_11", 0, 0)
  Lua_Tip_End(tooltip)
  local posx, posy = GetMousePos()
  tooltip:SetLeft(posx + 0.01)
  tooltip:SetTop(posy - 0.1)
  _Tip_AdjustPos(tooltip)
end
function Lua_Tip_Transmission(tooltip, name, needLevel, cost)
  local _, money, bmoney = GetBankAndBagMoney()
  local _, _, _, _, level_self = GetPlaySelfProp(4)
  local color1 = colorWhite
  if cost > money + bmoney then
    color1 = colorRed
  end
  local color2 = colorWhite
  if needLevel > level_self then
    color2 = colorRed
  end
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(name, colorWhite, "kaiti_13", 0, 0)
  if cost == -1 then
  else
    tooltip:InsertLeftText("需求等级: " .. tostring(needLevel), tostring(color2), "fzheiti_11", 0, 0)
    local itemId = tooltip:InsertLeftText("传送消耗: ", tostring(color1), "fzheiti_11", 0, 0)
    _Tip_ShowMoney(tooltip, cost, itemId, color1 == colorRed)
  end
  Lua_Tip_End(tooltip)
end
function Lua_Tip_EquipSlot(tooltip, name)
  tooltip = WindowToTooltip(tooltip)
  if name == nil then
    return
  end
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(name), colorWhite, "kaiti_13", 0, 0)
  Lua_Tip_End(tooltip)
end
function Lua_Tip_GroupChatMember(name, x, y)
  local tooltip = WindowToTooltip(WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2"))
  if name == nil then
    return
  end
  local level, job = GetGroupMemInfo(name)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(tostring(name), colorWhite, "kaiti_13", 0, 0)
  tooltip:InsertLeftText(tostring(gUI_MenPaiName[job]), colorWhite, "kaiti_13", 0, 0)
  tooltip:InsertLeftText(tostring(level) .. "级", colorWhite, "kaiti_13", 0, 0)
  Lua_Tip_End(tooltip)
  local wndParent = WindowSys_Instance:GetWindow("TooltipWindows.Tooltip2")
  wndParent:SetProperty("Visible", "true")
  wndParent:SetLeft(x)
  wndParent:SetTop(y)
end
function Lua_Tip_Gift(tooltip, index)
  tooltip = WindowToTooltip(tooltip)
  local uid, giftid, lv, maxlv = GetGiftInfo(index)
  local p = {}
  local name, needXiuweiLevel, level, maxLevel, desc1, desc2
  name, needXiuweiLevel, level, maxLevel, p[1], p[2], p[3], p[4], p[5], desc1, desc2 = GetGiftDetailInfo(uid)
  if name == nil then
    return
  end
  Lua_Tip_Begin(tooltip, nil, "false")
  tooltip:InsertLeftText(tostring(name), "FF1E90FF", "kaiti_13", 0, 0)
  if needXiuweiLevel > 0 then
    local itemid = tooltip:InsertLeftText("需要修为等级 ", colorWhite, "", 0, 0)
    local myXiuwei = GetMyXiuweiLevel()
    if needXiuweiLevel <= myXiuwei then
      tooltip:InsertText(tostring(needXiuweiLevel) .. "级", colorGreen, itemid, "RIGHTTOP", 0, 0)
    else
      tooltip:InsertText(tostring(needXiuweiLevel) .. "级", colorRed, itemid, "RIGHTTOP", 0, 0)
    end
  end
  itemid = tooltip:InsertLeftText("天赋等级 ", colorWhite, "", 0, 0)
  tooltip:InsertText(tostring(level) .. "/" .. tostring(maxLevel), colorGreen, itemid, "RIGHTTOP", 0, 0)
  local preFlag = false
  for i = 1, 5 do
    if p[i] ~= -1 then
      preFlag = true
      break
    end
  end
  if preFlag then
    tooltip:InsertLeftText("需求前置 ", colorWhite, "", 0, 0)
    for i = 1, 5 do
      if p[i] ~= -1 then
        local needGiftId = math.floor(p[i] / 100)
        local needlevel = p[i] - math.floor(p[i] / 10) * 10
        uid, giftid, lv, maxlv = GetGiftInfo(needGiftId)
        local tName = GetGiftDetailInfo(uid)
        if lv < needlevel then
          tooltip:InsertLeftText(" " .. tostring(tName) .. "天赋" .. tostring(needlevel) .. "级", colorRed, "", 0, 0)
        else
          tooltip:InsertLeftText(" " .. tostring(tName) .. "天赋" .. tostring(needlevel) .. "级", colorGreen, "", 0, 0)
        end
      end
    end
  end
  tooltip:InsertLeftText(tostring(desc1), colorDarkBlue, "", 0, 0)
  tooltip:InsertLeftText(tostring(desc2), colorYellow, "", 0, 0)
  if level < maxLevel then
    tooltip:InsertLeftText("——————————", colorGreen, "kaiti_13", 0, 0)
    name, needXiuweiLevel, level, maxLevel, p[1], p[2], p[3], p[4], p[5], desc1, desc2 = GetGiftDetailInfo(uid + 1)
    tooltip:InsertLeftText("下一等级", colorBlue, "kaiti_13", 0, 0)
    if needXiuweiLevel > 0 then
      local itemid = tooltip:InsertLeftText("需要修为等级 ", colorWhite, "", 0, 0)
      local myXiuwei = GetMyXiuweiLevel()
      if needXiuweiLevel <= myXiuwei then
        tooltip:InsertText(tostring(needXiuweiLevel) .. "级", colorGreen, itemid, "RIGHTTOP", 0, 0)
      else
        tooltip:InsertText(tostring(needXiuweiLevel) .. "级", colorRed, itemid, "RIGHTTOP", 0, 0)
      end
    end
    itemid = tooltip:InsertLeftText("天赋等级 ", colorWhite, "", 0, 0)
    tooltip:InsertText(tostring(level) .. "/" .. tostring(maxLevel), colorGreen, itemid, "RIGHTTOP", 0, 0)
    local preFlag = false
    for i = 1, 5 do
      if p[i] ~= -1 then
        preFlag = true
        break
      end
    end
    if preFlag then
      tooltip:InsertLeftText("需求前置 ", colorWhite, "", 0, 0)
      for i = 1, 5 do
        if p[i] ~= -1 then
          local needGiftId = math.floor(p[i] / 100)
          local needlevel = p[i] - math.floor(p[i] / 10) * 10
          uid, giftid, lv, maxlv = GetGiftInfo(needGiftId)
          local tName = GetGiftDetailInfo(uid)
          if lv < needlevel then
            tooltip:InsertLeftText(" " .. tostring(tName) .. "天赋" .. tostring(needlevel) .. "级", colorRed, "", 0, 0)
          else
            tooltip:InsertLeftText(" " .. tostring(tName) .. "天赋" .. tostring(needlevel) .. "级", colorGreen, "", 0, 0)
          end
        end
      end
    end
    tooltip:InsertLeftText(tostring(desc1), colorDarkBlue, "", 0, 0)
    tooltip:InsertLeftText(tostring(desc2), colorYellow, "", 0, 0)
  end
  Lua_Tip_End(tooltip)
end
function Lua_Tip_ShowEquipCompare(goodsbox, bag, slot, rever)
  local box_type = goodsbox:GetProperty("BoxType")
  if box_type ~= myequipbox then
    local bag1, slot1, bag2, slot2 = GetEquipedItemByType(bag, slot, rever)
    local tooltip = goodsbox:GetToolTipWnd(0)
    local wnd = goodsbox:GetToolTipWnd(1)
    local wnd1 = goodsbox:GetToolTipWnd(2)
    local top = tooltip:GetTop()
    local toleft = false
    local tooltipOrg = tooltip
    if bag1 and slot1 then
      local left = tooltip:GetLeft()
      Lua_Tip_Item(wnd, slot1, bag1, true, nil, nil)
      if wnd:GetWidth() + left + tooltip:GetWidth() > ScreenWidthPixelMax then
        wnd:SetLeft(left - wnd:GetWidth())
        toleft = true
      else
        wnd:SetLeft(left + tooltip:GetWidth())
      end
      wnd:SetTop(top)
      wnd:SetProperty("Visible", "true")
      tooltip = wnd
      if bag2 and slot2 then
        local left = tooltip:GetLeft()
        Lua_Tip_Item(wnd1, slot2, bag2, true, nil, nil)
        if toleft and wnd1:GetWidth() + left + tooltip:GetWidth() > ScreenWidthPixelMax then
          wnd1:SetLeft(left - wnd1:GetWidth())
        elseif not toleft and not (wnd1:GetWidth() + left + tooltip:GetWidth() > ScreenWidthPixelMax) then
          wnd1:SetLeft(left + tooltip:GetWidth())
        elseif not toleft and wnd1:GetWidth() + left + tooltip:GetWidth() > ScreenWidthPixelMax then
          wnd1:SetLeft(left - tooltipOrg:GetWidth() - wnd1:GetWidth())
        elseif toleft and not (wnd1:GetWidth() + left + tooltip:GetWidth() > ScreenWidthPixelMax) then
          wnd1:SetLeft(left - wnd1:GetWidth())
        end
        wnd1:SetTop(top)
        wnd1:SetProperty("Visible", "true")
        tooltip = wnd1
      else
        wnd1:SetProperty("Visible", "false")
      end
    else
      wnd:SetProperty("Visible", "false")
      wnd1:SetProperty("Visible", "false")
    end
  end
end
function Lua_Tip_ShowSkilCompare(wnd, skill_id, skillLv)
  local tooltip = wnd:GetToolTipWnd(0)
  local tooltip1 = wnd:GetToolTipWnd(1)
  local top = tooltip:GetTop()
  local left = tooltip:GetLeft()
  Lua_Tip_Skill(tooltip1, skill_id, skillLv, true, true)
  if 1 < tooltip1:GetWidth() + left + tooltip:GetWidth() then
    tooltip1:SetLeft(left - tooltip1:GetWidth())
  else
    tooltip1:SetLeft(left + tooltip:GetWidth())
  end
  tooltip1:SetTop(top)
  tooltip1:SetProperty("Visible", "true")
  tooltip = tooltip1
end
function Lua_Tip_ShowMiniMapTip(tooltip, Text, color)
  if color == nil then
    color = colorWhite
  end
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(Text, color, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function Lua_Tip_ToolBar(tooltip, index)
  Lua_Tip_Begin(tooltip)
  local hotkey = gUI.KeySet.ActionToolBar[index]
  if index == 7 then
    hotkey = "ESC"
  end
  local {
    [0] = "角色",
    [1] = "背包",
    [2] = "任务",
    [3] = "技能",
    [4] = "仙灵",
    [5] = "帮会",
    [6] = "好友",
    [7] = "系统菜单",
    [8] = "地图",
    [9] = "活动",
    [10] = "摆摊",
    [11] = "钻石交易",
    [12] = "坐骑",
    [13] = "商城",
    [14] = "排行榜",
    [15] = "聚灵",
    [16] = "组队"
  }[100], strToolBarTip = "帮助", {
    [0] = "角色",
    [1] = "背包",
    [2] = "任务",
    [3] = "技能",
    [4] = "仙灵",
    [5] = "帮会",
    [6] = "好友",
    [7] = "系统菜单",
    [8] = "地图",
    [9] = "活动",
    [10] = "摆摊",
    [11] = "钻石交易",
    [12] = "坐骑",
    [13] = "商城",
    [14] = "排行榜",
    [15] = "聚灵",
    [16] = "组队"
  }
  local {
    [0] = "可以查看到角色的装备和详细信息",
    [1] = "可以查看随时携带的物品和任务物品",
    [2] = "可以查看到当前已经接取的任务和当前可以接取的任务",
    [3] = "查看已经学习到和未学习到的技能",
    [4] = "点击打开仙灵界面",
    [5] = "点击打开帮会界面",
    [6] = "好友的信息列表",
    [7] = "包含了系统设置和按键设置",
    [8] = "点击打开大地图",
    [9] = "参加活动可以获得更多的奖励",
    [10] = "摆摊可以将自己不需要的物品出售给其他玩家",
    [11] = "出售钻石获得非绑定游戏币或购买钻石获得福利点",
    [12] = "可以查看到你使用过的所有坐骑，还能查看坐骑的技能",
    [13] = "15级开放，可以在商城购买到很多稀有物品",
    [14] = "可以查看到角色的排名",
    [15] = "可以打开聚灵界面",
    [16] = "可以打开组队界面"
  }[100], strToolBarTipDesc = "可以查看帮助", {
    [0] = "可以查看到角色的装备和详细信息",
    [1] = "可以查看随时携带的物品和任务物品",
    [2] = "可以查看到当前已经接取的任务和当前可以接取的任务",
    [3] = "查看已经学习到和未学习到的技能",
    [4] = "点击打开仙灵界面",
    [5] = "点击打开帮会界面",
    [6] = "好友的信息列表",
    [7] = "包含了系统设置和按键设置",
    [8] = "点击打开大地图",
    [9] = "参加活动可以获得更多的奖励",
    [10] = "摆摊可以将自己不需要的物品出售给其他玩家",
    [11] = "出售钻石获得非绑定游戏币或购买钻石获得福利点",
    [12] = "可以查看到你使用过的所有坐骑，还能查看坐骑的技能",
    [13] = "15级开放，可以在商城购买到很多稀有物品",
    [14] = "可以查看到角色的排名",
    [15] = "可以打开聚灵界面",
    [16] = "可以打开组队界面"
  }
  local strTooltip = string.format("%s", strToolBarTip[index])
  if hotkey ~= "" then
    strTooltip = string.format("%s（%s）", strToolBarTip[index], hotkey)
  end
  tooltip:InsertLeftText(strTooltip, colorDarkBlue, "kaiti_13", 0, 0)
  tooltip:InsertLeftText(strToolBarTipDesc[index], colorWhite, "", 0, 0)
  Lua_Tip_End(tooltip)
end
function Lua_Tip_ShowInterfaceCommon(tooltip, des, BeginFlag, EndFlag, clor, font)
  if BeginFlag == 0 then
    Lua_Tip_Begin(tooltip)
  end
  tooltip:InsertLeftText(des, clor, font, 0, 0)
  if EndFlag == 0 then
    Lua_Tip_End(tooltip)
  end
end
function Lua_Tip_PropertyNew(tooltip, value)
  if value then
    local ATTR_PARRY_RATE, ATTR_CRITICAL_RATE, ATTR_MISS_RATE, ATTR_DEADLY_RATE = GetPlaySelfProp(8)
    local maxAttack, minAttack, absDmg, def, absImDmg, parry, critical, miss, deadly, tenacity, absor = GetPlaySelfProp(1)
    local _, menpai = GetMyPlayerStaticInfo()
    local _, _, _, _, level = GetPlaySelfProp(4)
    local rate1, rate2 = GetSelfPropTipInfo(1010, critical, level, menpai)
    local rate11, rate21 = GetSelfPropTipInfo(1012, deadly, level, menpai)
    local rate31, rate22 = GetSelfPropTipInfo(1011, miss, level, menpai)
    local rate41, rate23 = GetSelfPropTipInfo(1009, parry, level, menpai)
    local rate42, rate24 = GetSelfPropTipInfo(1013, tenacity, level, menpai)
    local DefRate = 2
    Lua_Tip_Begin(tooltip)
    if value == 1 then
      tooltip:InsertLeftText("暴击", colorUserDefine, "kaiti_13", 0, 0)
      local itemId = tooltip:InsertLeftText("总暴击率: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate1 / 100 + rate2 / 100) .. "%", colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
      local itemId1 = tooltip:InsertLeftText("通过暴击等级获得: " .. tostring(critical) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText("(" .. tostring(rate1 / 100 - 2) .. "%)", colorUserDefine2, itemId1, "RIGHTTOP", 0, 0)
      local itemId2 = tooltip:InsertLeftText("通过暴击率获得: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate2 / 100 + 2) .. "%", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
      tooltip:InsertLeftText("攻击敌人时有概率造成暴击", colorYellow, "fzheiti_11", 0, 0)
    elseif value == 2 then
      tooltip:InsertLeftText("会心", colorUserDefine, "kaiti_13", 0, 0)
      local itemId = tooltip:InsertLeftText("总会心率: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate11 / 100 + rate21 / 100) .. "%", colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
      local itemId1 = tooltip:InsertLeftText("通过会心等级获得: " .. tostring(deadly) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText("(" .. tostring(rate11 / 100) .. "%)", colorUserDefine2, itemId1, "RIGHTTOP", 0, 0)
      local itemId2 = tooltip:InsertLeftText("通过会心率获得: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate21 / 100) .. "%", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
      tooltip:InsertLeftText("攻击敌人时有概率无视目标防御，造成会心一击", colorYellow, "fzheiti_11", 0, 0)
    elseif value == 3 then
      tooltip:InsertLeftText("闪避", colorUserDefine, "kaiti_13", 0, 0)
      local itemId = tooltip:InsertLeftText("总闪避率: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate31 / 100 + rate22 / 100) .. "%", colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
      local itemId1 = tooltip:InsertLeftText("通过闪避等级获得: " .. tostring(miss) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText("(" .. tostring(rate31 / 100) .. "%)", colorUserDefine2, itemId1, "RIGHTTOP", 0, 0)
      local itemId2 = tooltip:InsertLeftText("通过闪避率获得: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate22 / 100) .. "%", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
      tooltip:InsertLeftText("被敌人普通攻击时有几率闪避", colorYellow, "fzheiti_11", 0, 0)
    elseif value == 4 then
      tooltip:InsertLeftText("招架", colorUserDefine, "kaiti_13", 0, 0)
      local itemId = tooltip:InsertLeftText("总招架率: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate41 / 100 + rate23 / 100) .. "%", colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
      local itemId1 = tooltip:InsertLeftText("通过招架等级获得: " .. tostring(parry) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText("(" .. tostring(rate41 / 100) .. "%)", colorUserDefine2, itemId1, "RIGHTTOP", 0, 0)
      local itemId2 = tooltip:InsertLeftText("通过招架率获得: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate23 / 100) .. "%", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
      tooltip:InsertLeftText("被敌人普通攻击时有几率招架", colorYellow, "fzheiti_11", 0, 0)
    elseif value == 5 then
      tooltip:InsertLeftText("御劲", colorUserDefine, "kaiti_13", 0, 0)
      local itemId = tooltip:InsertLeftText("总御劲率: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate42 / 100 + rate24 / 100) .. "%", colorUserDefine2, itemId, "RIGHTTOP", 0, 0)
      local itemId1 = tooltip:InsertLeftText("通过御劲等级获得: " .. tostring(tenacity) .. " ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText("(" .. tostring(rate42 / 100) .. "%)", colorUserDefine2, itemId1, "RIGHTTOP", 0, 0)
      local itemId2 = tooltip:InsertLeftText("通过御率劲获得: ", colorUserDefine1, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(rate24 / 100) .. "%", colorUserDefine2, itemId2, "RIGHTTOP", 0, 0)
      local des = string.format("若被敌人暴击，可减免%.2f%%的暴击伤害", rate42 / 100 + rate24 / 100)
      tooltip:InsertLeftText(des, colorYellow, "fzheiti_11", 0, 0)
    end
    Lua_Tip_End(tooltip)
  end
end
function UI_Tip_ShowText(msg)
  local propertyList = {}
  local tooltip = WindowToTooltip(msg:get_window():GetToolTipWnd(0))
  local text = msg:get_window():GetProperty("TooltipText")
  local text1 = msg:get_window():GetProperty("TooltipTextDes")
  local name = msg:get_window():GetProperty("WindowName")
  local color = colorWhite
  local font = ""
  if propertyList[name] ~= nil then
    color = propertyList[name].color
    font = propertyList[name].font
  end
  if text == "" and text1 == "" then
    return
  end
  Lua_Tip_Begin(tooltip)
  if text ~= "" then
    tooltip:InsertLeftText(text, colorDarkBlue, "kaiti_13", 0, 0)
  end
  if text1 ~= "" then
    tooltip:InsertLeftText(text1, colorWhite, "", 0, 0)
  end
  Lua_Tip_End(tooltip)
end
function Lua_Tip_ShowAraMapPlayerTip(tooltip, name, info, color)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(name .. "<" .. info .. ">", color, "", 0, 0)
  Lua_Tip_End(tooltip, true)
end
function Lua_Tip_ShowAreaMapTaskTip(tooltip, Title, Desc, curValue)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(Title .. "(进行中)", colorYellow, "", 0, 0)
  if Desc and Desc ~= "" then
    Desc = GetParsedStr(Desc)
    Desc = Lua_Tip_TranslateSpecil(Desc)
    tooltip:InsertLeftText("  " .. string.format(Desc, curValue), colorWhite, "", 0, 0)
  end
  Lua_Tip_End(tooltip, true)
end
function Lua_Tip_ShowMapTransTip(tooltip, LevelLimited, Desc)
  Lua_Tip_Begin(tooltip)
  tooltip:InsertLeftText(Desc .. "<等级 " .. LevelLimited .. ">", "FF00FF00", "", 0, 0)
  Lua_Tip_End(tooltip, true)
end
function Lua_Tip_ShowMapTip(tooltip)
  Lua_Tip_Begin(tooltip)
  local targetList = GetAreaTargetList()
  for i = 0, table.maxn(targetList) do
    local currTipType = targetList[i].m_tipFlag
    local title = targetList[i].sTitle
    local desc = targetList[i].s_descript
    local param = targetList[i].m_paramEx1
    local levLim = targetList[i].m_LevLimit
    local NpcID = targetList[i].m_npcId
    if currTipType == gUI_TIPFLAG.TIPNONE then
    elseif currTipType == gUI_TIPFLAG.TIPNPC or currTipType == gUI_TIPFLAG.TIPGUARD then
      if title ~= "" then
        tooltip:InsertLeftText(string.format("%s <%s> ", desc, title), colorGreen, "kaiti_13", 0, 0)
      else
        tooltip:InsertLeftText(string.format("%s  ", desc), colorGreen, "kaiti_13", 0, 0)
      end
      local list = GetNpcQuestList(0, 1, NpcID)
      if list[0] ~= nil then
        local strTitle = GetQuestInfoLua(list[0], gUI_QuestRewardItemSourceID.INVALID)
        tooltip:InsertLeftText("[" .. strTitle .. "]" .. "（可交付）", colorYellow, "", 0, 0)
      end
      local list = GetNpcQuestList(0, 4, NpcID)
      for i, value in pairs(list) do
        local strTitle, strObjective, strGossip, nQuestLevel, nStatus, strQuestDetail, bDisableAbandon, MapName, NpcName, nCurrTimes, nTotalTimes, nQuestType, bCalLimitTime, IsTracking, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, IsGuildExp, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(value, gUI_QuestRewardItemSourceID.MAP_TOOLTIP)
        if nTotalTimes > 0 and nCurrTimes == nTotalTimes then
        else
          tooltip:InsertLeftText(tostring("[" .. strTitle .. "]") .. "（可接取）", colorYellow, "", 0, 0)
          local color = colorGreen
          if gUI_MainPlayerAttr.Level - nQuestLevel <= 3 then
            color = colorYellow
          elseif gUI_MainPlayerAttr.Level - nQuestLevel >= 10 then
            color = colorGrey
          end
          if v1 then
          end
        end
      end
      list = GetNpcQuestList(0, 3, NpcID)
      if list[0] ~= nil then
        strTitle = GetQuestInfoLua(list[0], gUI_QuestRewardItemSourceID.INVALID)
        tooltip:InsertLeftText("[" .. strTitle .. "]" .. "（未完成）", colorYellow, "", 0, 0)
      end
    elseif currTipType == gUI_TIPFLAG.TIPTEAM then
      local infos = "--"
      if gUI_MenPaiName[param] then
        infos = gUI_MenPaiName[param]
      end
      if levLim >= 0 then
        tooltip:InsertLeftText(desc .. "<" .. infos .. ">" .. "(" .. tostring(levLim + 1) .. "线)", "FF6A5ACD", "", 0, 0)
      else
        tooltip:InsertLeftText(desc .. "<" .. infos .. ">", "FF6A5ACD", "", 0, 0)
      end
    elseif currTipType == gUI_TIPFLAG.TIPTRAN then
      tooltip:InsertLeftText(desc .. "<等级 " .. levLim .. ">", "FF00FF00", "", 0, 0)
    elseif currTipType == gUI_TIPFLAG.TIPTASK then
      tooltip:InsertLeftText(title .. "(进行中)", colorYellow, "", 0, 0)
      if desc and desc ~= "" then
        desc = GetParsedStr(desc)
        desc = Lua_Tip_TranslateSpecil(desc)
        tooltip:InsertLeftText("  " .. string.format(desc, param), colorWhite, "", 0, 0)
      end
    elseif currTipType == gUI_TIPFLAG.TIPSPECIAL then
      local infos = "--"
      if gUI_MAPSPCIALINFO[param] ~= nil and gUI_MAPSPCIALINFO[param] ~= "" then
        tooltip:InsertLeftText(desc .. "(" .. gUI_MAPSPCIALINFO[param] .. ")", colorBlue, "", 0, 0)
      else
        tooltip:InsertLeftText(desc, colorBlue, "", 0, 0)
      end
    end
  end
  Lua_Tip_End(tooltip, true)
end
function Lua_Tip_ShowPetTip(tooltip, index)
  local name = GetPetName(index)
  local level = GetPetLevel(index)
  if name ~= "" then
    Lua_Tip_Begin(tooltip)
    tooltip:InsertLeftText("名称:" .. name, "FF00FF00", "", 0, 0)
    tooltip:InsertLeftText("等级:" .. tostring(level), "FF00FF00", "", 0, 0)
    Lua_Tip_End(tooltip)
  end
end
function Lua_Tip_ShowMapNpcTip(tooltip, NpcID, Name, Title)
  Lua_Tip_Begin(tooltip)
  local itemId = tooltip:InsertLeftText(Name, colorGreen, "", 0, 0)
  if Title ~= "" then
    tooltip:InsertText(string.format("<%s>", Title), colorYellow, itemId, "RIGHTTOP", 0, 0)
  end
  local list = GetNpcQuestList(0, 1, NpcID)
  if list[0] ~= nil then
    local strTitle = GetQuestInfoLua(list[0], gUI_QuestRewardItemSourceID.INVALID)
    tooltip:InsertLeftText(" ", colorYellow, "", 0, 0)
    tooltip:InsertLeftText("[" .. strTitle .. "]" .. "（可交付）", colorYellow, "", 0, 0)
    Lua_Tip_End(tooltip, true)
    return
  end
  local list = GetNpcQuestList(0, 4, NpcID)
  for i, value in pairs(list) do
    local strTitle, strObjective, strGossip, nQuestLevel, nStatus, strQuestDetail, bDisableAbandon, MapName, NpcName, nCurrTimes, nTotalTimes, nQuestType, bCalLimitTime, IsTracking, RewExp, RewMoney, RewNimbus, RewBindedMoney, RewWrathValue, GuildMoney, GuildActive, GuildContribution, GuildConstruction, GuildPractice, bHaveChoiceItem, nCommission, v1, v2, v3, v4, v5, v6, v7 = GetQuestInfoLua(value, gUI_QuestRewardItemSourceID.MAP_TOOLTIP)
    if nTotalTimes > 0 and nCurrTimes == nTotalTimes then
    else
      tooltip:InsertLeftText(" ", colorYellow, "", 0, 0)
      tooltip:InsertLeftText(tostring("[" .. strTitle .. "]") .. "（可接取）", colorYellow, "", 0, 0)
      local color = colorGreen
      if gUI_MainPlayerAttr.Level - nQuestLevel <= 3 then
        color = colorYellow
      elseif gUI_MainPlayerAttr.Level - nQuestLevel >= 10 then
        color = colorGrey
      end
      if v1 then
      end
      Lua_Tip_End(tooltip, true)
      return
    end
  end
  list = GetNpcQuestList(0, 3, NpcID)
  if list[0] ~= nil then
    strTitle = GetQuestInfoLua(list[0], gUI_QuestRewardItemSourceID.INVALID)
    tooltip:InsertLeftText(" ", colorYellow, "", 0, 0)
    tooltip:InsertLeftText("[" .. strTitle .. "]" .. "（未完成）", colorYellow, "", 0, 0)
    Lua_Tip_End(tooltip, true)
    return
  end
  Lua_Tip_End(tooltip, true)
  return
end
function Lua_Tip_Begin(tooltip, width, bAutoWidth, colSpace, pageWSpace, pageHSpace)
  local strBackImage = "UiIamge004:Image_tips"
  tooltip:SetProperty("BackImage", strBackImage)
  if bAutoWidth ~= nil then
    tooltip:SetProperty("TooltipAutoWidth", bAutoWidth)
  else
    tooltip:SetProperty("TooltipAutoWidth", "true")
  end
  tooltip:ClearAllContent()
  if width == nil then
    width = 230
  end
  if colSpace == nil then
    colSpace = 2
  end
  if pageWSpace == nil then
    pageWSpace = 14
  end
  if pageHSpace == nil then
    pageHSpace = 14
  end
  tooltip:InsertBegin("fzheiti_11", width, colSpace, pageWSpace, pageHSpace)
end
function Lua_Tip_End(tooltip, bShowDirect)
  tooltip:InsertEnd()
  if bShowDirect then
    tooltip:SetProperty("Visible", "true")
  else
    tooltip:ShowTips(true)
  end
end
function Lua_Tip_SetSpeciText(text, color, font, money)
  if color == "" then
    color = colorWhite
  end
  gTip_SpeciText.text = text
  gTip_SpeciText.color = color
  gTip_SpeciText.font = font
  gTip_SpeciText.money = money
end
function _Tip_ShowQuestList(tooltip, list)
  if list[0] ~= nil then
    tooltip:InsertLeftText(" ", colorYellow, "", 0, 0)
    for i = 0, table.maxn(list) do
      tooltip:InsertLeftText(tostring(list[i].name), colorYellow, "", 0, 0)
      local strObjective = list[i].conName
      strObjective = GetParsedStr(strObjective)
      if list[i] ~= nil and list[i].curValue ~= nil and strObjective ~= nil then
        strObjective = string.format(strObjective, list[i].curValue, 0, 0, 0, 0, 0)
        strObjective = Lua_UI_TranslateText(strObjective)
        strObjective = Lua_Tip_TranslateSpecil(strObjective)
        if list[i].curValue < list[i].needValue then
          tooltip:InsertLeftText("  " .. strObjective, colorBlue, "", 0, 0)
        else
          tooltip:InsertLeftText("  " .. strObjective, colorGrey, "", 0, 0)
        end
      end
    end
  end
end
function Lua_Tip_TranslateSpecil(text)
  return string.gsub(text, "\\n", "#r")
end
function _Tip_ShowForgeAttr(tooltip, slot, level, bag, xiuwei, reserved, tableId, bNextXiuwei, equipKind)
  local rateList, fixList
  local color = colorWhite
  if equipKind == 21 then
    color = "ff00ffff"
  end
  if tableId == nil then
    rateList = GetForgeRateBounce(slot, 0, bag, xiuwei, reserved)
    fixList = GetForgeFixBounce(slot, 0, bag, xiuwei, reserved)
  else
    rateList = GetForgeRateBounce(nil, nil, -1, xiuwei, nil, tableId)
    fixList = GetForgeFixBounce(nil, nil, -1, xiuwei, nil, tableId)
  end
  local rateListFlag = {
    [0] = false,
    [1] = false,
    [2] = false,
    [3] = false,
    [4] = false,
    [5] = false,
    [6] = false,
    [7] = false
  }
  local minAtkCur = -1
  local maxAtkCur = -1
  local countAttr = 0
  if rateList[0] == nil then
    return
  end
  for i = 0, table.maxn(rateList) do
    if rateList[i].attrName == "攻击最小值" then
      minAtkCur = rateList[i].value
    elseif rateList[i].attrName == "攻击最大值" then
      maxAtkCur = rateList[i].value
    else
      rateListFlag[i] = true
    end
  end
  local reidentValue
  if tableId == nil then
    _, _, _, _, reidentValue = GetAuthenticateAttr(bag, slot, reserved)
  else
    _, _, _, _, reidentValue = GetAuthenticateAttr(-1, tableId, reserved)
  end
  if minAtkCur ~= -1 and maxAtkCur ~= 0 then
    local strAtk = string.format("+%d～%d", minAtkCur, maxAtkCur)
    local itemid = tooltip:InsertLeftText("攻击" .. "  " .. strAtk, color, "fzheiti_11", 0, 0)
    if reidentValue and reidentValue > 0 then
      itemid = tooltip:InsertText("+" .. reidentValue .. "%", colorGold, itemid, "RIGHTTOP", 50, 0)
      itemid = tooltip:InsertText("(" .. tostring(math.floor(minAtkCur / (100 + reidentValue) * reidentValue)) .. "-" .. tostring(math.floor(maxAtkCur / (100 + reidentValue) * reidentValue)) .. ")", colorGold, itemid, "RIGHTTOP", 5, 0)
    end
  end
  for i = 0, table.maxn(rateList) do
    if rateListFlag[i] == true and rateList[i].typeId ~= -1 then
      local strAttr = ""
      if Lua_Tip_NeedShowRate(rateList[i].typeId) then
        strAttr = "+%d%% "
      else
        strAttr = "+%d    "
      end
      strAttr = string.format(strAttr, Lua_Tip_PostProcessVal(rateList[i].typeId, rateList[i].value))
      local itemid = tooltip:InsertLeftText(rateList[i].attrName .. "  " .. strAttr, color, "fzheiti_11", 0, 0)
      if reidentValue and reidentValue > 0 then
        itemid = tooltip:InsertText("+" .. reidentValue .. "%", colorGold, itemid, "RIGHTTOP", 50, 0)
        itemid = tooltip:InsertText("(" .. tostring(math.floor(rateList[i].value / (100 + reidentValue) * reidentValue)) .. ")", colorGold, itemid, "RIGHTTOP", 5, 0)
      end
    end
  end
  if bNextXiuwei ~= true then
    if not (1 <= fixList[0].value) or fixList[0].attrName ~= "" then
    end
    for i = 0, table.maxn(fixList) do
      local strAttr = ""
      if 1 <= fixList[i].value and fixList[i].attrName ~= "" then
        if Lua_Tip_NeedShowRate(fixList[i].typeId) then
          strAttr = "+%d%% "
        else
          strAttr = "+%d   "
        end
        strAttr = string.format(strAttr, Lua_Tip_PostProcessVal(fixList[i].typeId, fixList[i].value))
        local itemid = tooltip:InsertLeftText(fixList[i].attrName .. "  " .. strAttr, color, "fzheiti_11", 0, 0)
      end
    end
  end
end
function _Tip_ShowForgeExAttr(tooltip, solt, forgelevel, bag, maxforgelevel, bIsforge, reserved, tableId)
  if maxforgelevel < 7 then
    return
  end
  local exAttList
  if tableId == nil then
    exAttList = GetForgeExAttList(solt, forgelevel, maxforgelevel, bIsforge, bag, reserved, tableId)
  else
    exAttList = GetForgeExAttList(solt, forgelevel, maxforgelevel, bIsforge, bag, reserved, tableId)
  end
  for i = 0, #exAttList do
    local strAttr = "[+%d]仙蕴奖励 "
    local strAttr1
    local curI = forgelevel + i
    if forgelevel < 8 then
      curI = i + 7
    end
    if 1 <= exAttList[i].value and exAttList[i].attName ~= "" then
      if Lua_Tip_NeedShowRate(exAttList[i].typeId) then
        strAttr = string.format(strAttr, curI)
        strAttr1 = "%s +%d%%"
        strAttr1 = string.format(strAttr1, exAttList[i].attName, Lua_Tip_PostProcessVal(exAttList[i].typeId, exAttList[i].value))
        if forgelevel < curI then
          local itemidclr = tooltip:InsertLeftText(strAttr, colorGrey, "fzheiti_11", 0, 0)
          tooltip:InsertText(tostring(strAttr1), colorGrey, itemidclr, "LEFT", 100, 0)
        else
          local itemidclr = tooltip:InsertLeftText(strAttr, colorPurper, "fzheiti_11", 0, 0)
          tooltip:InsertText(tostring(strAttr1), colorPurper, itemidclr, "LEFT", 100, 0)
        end
      else
        strAttr = string.format(strAttr, curI)
        strAttr1 = "%s +%d"
        strAttr1 = string.format(strAttr1, exAttList[i].attName, Lua_Tip_PostProcessVal(exAttList[i].typeId, exAttList[i].value))
        if forgelevel < curI then
          local itemidclr = tooltip:InsertLeftText(strAttr, colorGrey, "fzheiti_11", 0, 0)
          tooltip:InsertText(tostring(strAttr1), colorGrey, itemidclr, "LEFT", 100, 0)
        else
          local itemidclr = tooltip:InsertLeftText(strAttr, colorPurper, "fzheiti_11", 0, 0)
          tooltip:InsertText(tostring(strAttr1), colorPurper, itemidclr, "LEFT", 100, 0)
        end
      end
    end
  end
end
function _Tip_AdjustPos(tooltip)
  local wndParent = WindowSys_Instance:GetWindow("TooltipWindows"):SetProperty("Visible", "true")
  local wndWidth = tooltip:GetWidth()
  local wndHeight = tooltip:GetHeight()
  tooltip:SetProperty("Visible", "true")
  tooltip:SetWidth(wndWidth)
  tooltip:SetHeight(wndHeight)
end
function _Tip_ShowCommonInfoEnd(tooltip, canBank, canSell, canDiscard, desc1, desc2, desc3, sellPrice, sellType, buyPrice, leftEffTime, bag, slot, reserved, offlineTime, isTimeLimit, IsFreeze)
  local lockTime = GetTradeBindInfo(bag, slot, reserved)
  if lockTime ~= nil and lockTime > 0 then
    if reserved == nil then
      reserved = 0
    end
    local desc = string.format("剩余$CT(9,%d,%d,%d)$可解除交易保护状态", bag, slot, reserved)
    tooltip:InsertLeftText(desc, colorRed, "fzheiti_11", 0, 0)
  end
  if canBank == 0 then
  end
  if canSell == 0 then
  end
  if canDiscard == 0 then
  end
  if desc1 then
    tooltip:InsertLeftPraseText(tostring(desc1), colorGreen, "fzheiti_11", 0, 0)
  end
  if desc2 then
    tooltip:InsertLeftPraseText(tostring(desc2), colorLightYellow, "fzheiti_11", 0, 0)
  end
  if desc3 then
    tooltip:InsertLeftPraseText(tostring(desc3), colorLightYellow, "fzheiti_11", 0, 0)
  end
  if isTimeLimit then
    if leftEffTime > 0 then
      if offlineTime ~= "" then
        itemid = tooltip:InsertLeftText("有效期至 " .. tostring(offlineTime), colorGold, "fzheiti_11", 0, 0)
      else
        if reserved == nil then
          reserved = 0
        end
        local des = string.format("剩余有效时间 $CT(128,%d,%d,%d)$", bag, slot, reserved)
        itemid = tooltip:InsertLeftText(tostring(des), colorGold, "fzheiti_11", 0, 0)
      end
    else
      itemid = tooltip:InsertLeftText("已过期", colorRed, "fzheiti_11", 0, 0)
    end
  end
  if canSell == 1 and buyPrice == nil and sellPrice > 0 then
    local strSellDes = "出售价格 " .. " 绑金:  "
    if sellType == 1 then
      strSellDes = "出售价格 " .. " 金币:  "
    end
    local itemid = tooltip:InsertLeftText(strSellDes, colorWhite, "fzheiti_11", 0, 0)
    _Tip_ShowMoney(tooltip, sellPrice, itemid)
  end
  if buyPrice ~= nil and buyPrice ~= 0 then
    local itemid = tooltip:InsertLeftText("单价 ", colorWhite, "fzheiti_11", 0, 0)
    _Tip_ShowMoney(tooltip, buyPrice, itemid)
  end
  _Tip_ShowSpeciText(tooltip)
  _Tip_ShowFreezeItem(tooltip, IsFreeze)
end
function _Tip_ShowFreezeItem(tooltip, IsFreeze)
  if IsFreeze == 1 then
    local itemid = tooltip:InsertLeftText("该物品已经被强制锁定，无法使用或装备", colorRed, "fzheiti_11", 0, 0)
  end
end
function _Tip_ShowCommonInfoMid(tooltip, isUnique, bindinfo, itemType)
  if isUnique == 1 then
    itemid = tooltip:InsertLeftText("唯一", colorPurper, "", 0, 0)
  end
  if bindinfo == 1 then
    itemid = tooltip:InsertLeftText("已绑定", colorLightYellow, "", 0, 0)
  elseif bindinfo == 2 then
    itemid = tooltip:InsertLeftText("拾取绑定", colorDarkYellow, "", 0, 0)
  elseif bindinfo == 3 then
    if itemType == 2 then
      itemid = tooltip:InsertLeftText("装备绑定", colorGold, "", 0, 0)
    else
      itemid = tooltip:InsertLeftText("使用绑定", colorGold, "", 0, 0)
    end
  end
end
function _Tip_CheckAviable(tooltip, ver, str)
  if not ver then
    Lua_Chat_AddSysLog(str .. "出错：" .. tostring(ver))
    if tooltip:GetProperty("Visible") == "true" then
      Lua_Tip_End(tooltip)
    end
    return false
  end
  return true
end
function _Tip_EquipSuit(tooltip, bag, slot, reserved)
  local name, count, maxCount = GetSuitBaseInfo(bag, slot, reserved)
  if name == nil then
    return
  end
  tooltip:InsertLeftText(string.format("%s(%d/%d)", name, count, maxCount), "FFFF00FF", "fzheiti_11", 0, 0)
  local EquipList = GetSuitEquipList(bag, slot, reserved)
  if EquipList[0] == nil then
    return
  end
  for i = 0, table.maxn(EquipList) do
    if EquipList[i].bEquip == true then
      tooltip:InsertLeftText(EquipList[i].name, colorLightYellow, "fzheiti_11", 0, 0)
    end
  end
  for i = 0, table.maxn(EquipList) do
    if EquipList[i].bEquip == false then
      tooltip:InsertLeftText(EquipList[i].name, colorGrey, "fzheiti_11", 0, 0)
    end
  end
  local list = GetSuitAttrInfo(bag, slot, reserved)
  if list[0] == nil then
    return
  end
  for i = 0, table.maxn(list) do
    local color = ""
    if list[i][0].IsActive == true then
      color = colorGreen
    else
      color = colorGrey
    end
    local str = string.format("(%d)", list[i].lv)
    for j = 0, list[i].count - 1 do
      local comm = ""
      if j ~= list[i].count - 1 then
        comm = ","
      end
      if Lua_Tip_NeedShowRate(list[i][j].typeId) then
        str = str .. tostring(list[i][j].name) .. "+" .. tostring(list[i][j].value / 1000) .. "%" .. comm
      else
        str = str .. tostring(list[i][j].name) .. "+" .. tostring(math.floor(list[i][j].value / 10)) .. comm
      end
    end
    tooltip:InsertLeftText(str, color, "fzheiti_11", 0, 0)
  end
end
function _Tip_ShowCommonInfoHead(tooltip, name, quality, minLevel, maxLevel, icon, sex, job, equipPoint, shillId, RuneLv, mountELv)
  local playName = gUI_MainPlayerAttr.Name
  local playJob = gUI_MainPlayerAttr.MenPai
  local playSex = gUI_MainPlayerAttr.Sex
  local playLevel = gUI_MainPlayerAttr.Level
  local itemid
  if mountELv ~= nil then
    itemid = tooltip:InsertLeftText("[" .. gUI_Mount_Rank[mountELv] .. "]  " .. tostring(name), gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
  else
    itemid = tooltip:InsertLeftText(tostring(name), gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
  end
  local startId = itemid
  if SkillId == RUNE_ITEM_SKILLID then
    for i = 1, RuneLv do
      local tmpid = tooltip:InsertImage(tostring("UiBtn001:Yellowstar_n"), 16, 16, startId, "RIGHTTOP", 0, 0)
      startId = tmpid
    end
  end
  itemid1 = tooltip:InsertImage(tostring(gUI_GOODS_QUALITY_IMAGE[quality]), 49, 49, startId, "RIGHT", 0, 0)
  tooltip:InsertImage(tostring(icon), 49, 49, startId, "RIGHT", 0, 0)
  color = colorWhite
  if minLevel > playLevel then
    color = colorRed
  end
  local strMsg
  if equipPoint > PetUseEquipStart and equipPoint < PetUseEquipEnd then
    strMsg = string.format("宠物使用等级  %d级  ", minLevel)
  else
    strMsg = string.format("使用等级    %d级  ", minLevel)
  end
  if minLevel > 0 then
    itemid = tooltip:InsertLeftText(tostring(strMsg), color, "fzheiti_11", 0, 0)
  end
  if maxLevel and maxLevel ~= -1 then
    color = colorWhite
    if maxLevel < playLevel then
      color = colorRed
    end
    strMsg = string.format("使用最大等级  %d", maxLevel)
    itemid = tooltip:InsertText(tostring(strMsg), color, itemid, "LEFTBOTTOM", 0, 0)
  end
  if job ~= -1 then
    color = colorWhite
    if not checkJobKindIsCanUse(playJob, job) then
      color = colorRed
    end
    itemid = tooltip:InsertLeftText("职业", colorWhite, "fzheiti_11", 0, 0)
    local job1, job2, job3, job4, job5 = GetJobNamesByJobID(job)
    local jobnames = ""
    if job1 then
      jobnames = jobnames .. job1 .. " "
    end
    if job2 then
      jobnames = jobnames .. job2 .. " "
    end
    if job3 then
      jobnames = jobnames .. job3 .. " "
    end
    if job4 then
      jobnames = jobnames .. job4 .. " "
    end
    if job5 then
      jobnames = jobnames .. job5 .. " "
    end
    tooltip:InsertText(jobnames, color, itemid, "LEFT", 40, 0)
  else
  end
  if sex ~= -1 then
    itemid = tooltip:InsertText("性别 ", colorWhite, itemid, "LEFTBOTTOM", 0, 0)
    color = colorWhite
    if playSex ~= sex then
      color = colorRed
    end
    if sex == 1 then
      tooltip:InsertText("男性", color, itemid, "RIGHTTOP", 0, 0)
    else
      tooltip:InsertText("女性", color, itemid, "RIGHTTOP", 0, 0)
    end
  end
end
function _Tip_ShowCommonInfoHeadEquip(tooltip, name, quality, minLevel, maxLevel, icon, sex, job, level, equipPoint, maxForgeLev, curForge, curClr, bindInfo, itemType, Equiped, equipKind, stars, scores)
  local playName = gUI_MainPlayerAttr.Name
  local playJob = gUI_MainPlayerAttr.MenPai
  local playSex = gUI_MainPlayerAttr.Sex
  local playLevel = gUI_MainPlayerAttr.Level
  local itemid
  if Equiped == true then
    itemId = tooltip:InsertLeftText("当前装备", "FFFF00FF", "", 0, 0)
  end
  local incomeName = "1阶"
  if level and level > 0 then
    if itemid == nil then
      itemid = tooltip:InsertLeftText(tostring(name) .. " +" .. tostring(level) .. "     ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
    else
      tooltip:InsertLeftText(tostring(name) .. " +" .. tostring(level) .. "     ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
    end
  elseif itemid == nil then
    itemid = tooltip:InsertLeftText(tostring(name) .. "    ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
  else
    tooltip:InsertLeftText(tostring(name) .. "    ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
  end
  local itemidclr
  if gTip_EquipTypeName[equipKind] ~= "" then
    incomeName = tostring(math.floor((minLevel + 10) / 10)) .. "阶"
    if equipKind == 1 then
      itemidclr = tooltip:InsertLeftText(tostring(incomeName) .. tostring(gTip_EquipTypeName[equipKind]) .. "  ", colorWhite, "fzheiti_11", 0, 0)
    elseif equipKind == 21 then
      itemidclr = tooltip:InsertLeftText(tostring(gTip_EquipTypeName[equipKind]) .. "  ", colorWhite, "fzheiti_11", 0, 0)
    else
      itemidclr = tooltip:InsertLeftText(tostring(incomeName) .. tostring(gTip_EquipTypeName[equipKind]) .. "(" .. tostring(gTip_EquipTypeNameNew[equipKind]) .. ")  ", colorWhite, "fzheiti_11", 0, 0)
    end
  end
  itemid1 = tooltip:InsertImage(tostring(gUI_GOODS_QUALITY_IMAGE[quality]), 49, 49, itemid, "RIGHTTOP", 50, 0)
  itemid1 = tooltip:InsertImage(tostring(icon), 49, 49, itemid, "RIGHTTOP", 50, 0)
  if Equiped then
    tooltip:InsertImage(tostring(EQUIPEQUIP), 47, 47, itemid, "RIGHTTOP", 60, -10)
  end
  local strForgeDes, strClrDes
  if curForge == 0 then
    strForgeDes = ""
    strClrDes = ""
  else
    strForgeDes = "历史最高锻造等级    +" .. tostring(curForge)
    strClrDes = "当前锻造等级    +" .. tostring(curClr)
  end
  itemid = tooltip:InsertLeftText(strForgeDes, colorWhite, "fzheiti_11", 0, 0)
  itemid = tooltip:InsertLeftText(strClrDes, colorWhite, "fzheiti_11", 0, 0)
  if bindInfo == 1 then
    itemidclr = tooltip:InsertLeftText("已绑定", colorLightYellow, "fzheiti_11", 0, 0)
  elseif bindInfo == 2 then
    itemidclr = tooltip:InsertLeftText("拾取绑定", colorDarkYellow, "fzheiti_11", 0, 0)
  elseif bindInfo == 3 then
    if itemidclr == 2 then
      itemidclr = tooltip:InsertLeftText("装备绑定", colorGold, "fzheiti_11", 0, 0)
    else
      itemidclr = tooltip:InsertLeftText("使用绑定", colorGold, "fzheiti_11", 0, 0)
    end
  else
    itemidclr = tooltip:InsertLeftText("", colorGold, "fzheiti_11", 0, 0)
  end
  color = colorWhite
  if minLevel > playLevel then
    color = colorRed
  end
  if minLevel > 0 then
    if equipPoint > PetUseEquipStart and equipPoint < PetUseEquipEnd then
      itemidclr = tooltip:InsertLeftText("宠物等级", colorWhite, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(minLevel), color, itemidclr, "LEFT", 40, 0)
    else
      itemidclr = tooltip:InsertLeftText("等级", colorWhite, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(minLevel), color, itemidclr, "LEFT", 40, 0)
    end
  end
  if job ~= -1 then
    color = colorWhite
    if not checkJobKindIsCanUse(playJob, job) then
      color = colorRed
    end
    itemidclr = tooltip:InsertLeftText("职业", colorWhite, "fzheiti_11", 0, 0)
    local job1, job2, job3, job4, job5 = GetJobNamesByJobID(job)
    local jobnames = ""
    if job1 then
      jobnames = jobnames .. job1 .. " "
    end
    if job2 then
      jobnames = jobnames .. job2 .. " "
    end
    if job3 then
      jobnames = jobnames .. job3 .. " "
    end
    if job4 then
      jobnames = jobnames .. job4 .. " "
    end
    if job5 then
      jobnames = jobnames .. job5 .. " "
    end
    tooltip:InsertText(jobnames, color, itemidclr, "LEFT", 40, 0)
  else
    itemidclr = tooltip:InsertLeftText("职业", colorWhite, "fzheiti_11", 0, 0)
    tooltip:InsertText("全职业", colorWhite, itemidclr, "LEFT", 40, 0)
  end
  if sex ~= -1 then
    itemid = tooltip:InsertText("性别 ", colorWhite, itemid, "LEFTBOTTOM", 0, 0)
    color = colorWhite
    if playSex ~= sex then
      color = colorRed
    end
    if sex == 1 then
      tooltip:InsertText("男性", color, itemid, "LEFT", 60, 0)
    else
      tooltip:InsertText("女性", color, itemid, "LEFT", 60, 0)
    end
  end
  if scores ~= nil and scores > 0 then
    tooltip:InsertLeftText(string.format("装备评分  %d分", scores), colorGold, "", 0, 0)
  end
end
function _Tip_ShowSpeciText(tooltip)
  if gTip_SpeciText.text ~= "" then
    local itemid = tooltip:InsertLeftText(gTip_SpeciText.text, gTip_SpeciText.color, gTip_SpeciText.font, 0, 0)
    if gTip_SpeciText.money ~= nil then
      _Tip_ShowMoney(tooltip, gTip_SpeciText.money, itemid)
    end
    gTip_SpeciText.text = ""
    gTip_SpeciText.color = colorWhite
    gTip_SpeciText.font = ""
    gTip_SpeciText.money = nil
  end
end
function _Tip_ShowMoney(tooltip, sellPrice, itemid, isColorRed)
  local color = Lua_UI_GetGoldColor(sellPrice / 10000, true)
  if isColorRed then
    color = colorRed
  end
  local str = Lua_UI_Money2String(sellPrice, false, true)
  str, itemid = _Tip_ShowMoneyUnity(tooltip, str, "g", itemid, color)
  str, itemid = _Tip_ShowMoneyUnity(tooltip, str, "s", itemid, color)
  str, itemid = _Tip_ShowMoneyUnity(tooltip, str, "c", itemid, color)
end
function _Tip_ShowMoneyUnity(tooltip, str, mark, itemid, color)
  local i, j = string.find(str, mark)
  if i ~= nil then
    itemid = tooltip:InsertText(tostring(string.sub(str, 1, i - 1)), color, itemid, "RIGHTTOP", 0, 0)
    itemid = tooltip:InsertImage(gUI_MoneyPic[tostring(mark)], 13, 13, itemid, "RIGHTTOP", 0, 0)
    str = string.sub(str, j + 1, -1)
  end
  return str, itemid
end
function _Tip_ConvertMoney(money)
  local gold = RewMoney / 10000
  local silver = RewMoney - RewMoney / 10000
  local copper = RewMoney - RewMoney / 100 - RewMoney / 10000
  return gold, silver, copper
end
function Lua_Tip_NeedShowRate(id)
  if id >= 100 and id < 200 then
    return true
  elseif id == 56 or id == 57 or id == 58 or id == 59 or id == 60 then
    return true
  elseif id == 202 or id == 204 then
    return true
  else
    return false
  end
end
function Lua_Tip_PostProcessVal(id, value)
  if id == 56 or id == 57 or id == 58 or id == 59 or id == 60 then
    return value / 100
  else
    return value
  end
end
_QUALITY_IMAGE[quality]), 49, 49, startId, "RIGHT", 0, 0)
  tooltip:InsertImage(tostring(icon), 49, 49, startId, "RIGHT", 0, 0)
  color = colorWhite
  if minLevel > playLevel then
    color = colorRed
  end
  local strMsg
  if equipPoint > PetUseEquipStart and equipPoint < PetUseEquipEnd then
    strMsg = string.format("\179\232\206\239\202\185\211\195\181\200\188\182  %d\188\182  ", minLevel)
  else
    strMsg = string.format("\202\185\211\195\181\200\188\182    %d\188\182  ", minLevel)
  end
  if minLevel > 0 then
    itemid = tooltip:InsertLeftText(tostring(strMsg), color, "fzheiti_11", 0, 0)
  end
  if maxLevel and maxLevel ~= -1 then
    color = colorWhite
    if maxLevel < playLevel then
      color = colorRed
    end
    strMsg = string.format("\202\185\211\195\215\238\180\243\181\200\188\182  %d", maxLevel)
    itemid = tooltip:InsertText(tostring(strMsg), color, itemid, "LEFTBOTTOM", 0, 0)
  end
  if job ~= -1 then
    color = colorWhite
    if not checkJobKindIsCanUse(playJob, job) then
      color = colorRed
    end
    itemid = tooltip:InsertLeftText("\214\176\210\181", colorWhite, "fzheiti_11", 0, 0)
    local job1, job2, job3, job4, job5 = GetJobNamesByJobID(job)
    local jobnames = ""
    if job1 then
      jobnames = jobnames .. job1 .. " "
    end
    if job2 then
      jobnames = jobnames .. job2 .. " "
    end
    if job3 then
      jobnames = jobnames .. job3 .. " "
    end
    if job4 then
      jobnames = jobnames .. job4 .. " "
    end
    if job5 then
      jobnames = jobnames .. job5 .. " "
    end
    tooltip:InsertText(jobnames, color, itemid, "LEFT", 40, 0)
  else
  end
  if sex ~= -1 then
    itemid = tooltip:InsertText("\208\212\177\240 ", colorWhite, itemid, "LEFTBOTTOM", 0, 0)
    color = colorWhite
    if playSex ~= sex then
      color = colorRed
    end
    if sex == 1 then
      tooltip:InsertText("\196\208\208\212", color, itemid, "RIGHTTOP", 0, 0)
    else
      tooltip:InsertText("\197\174\208\212", color, itemid, "RIGHTTOP", 0, 0)
    end
  end
end
function _Tip_ShowCommonInfoHeadEquip(tooltip, name, quality, minLevel, maxLevel, icon, sex, job, level, equipPoint, maxForgeLev, curForge, curClr, bindInfo, itemType, Equiped, equipKind, stars, scores)
  local playName = gUI_MainPlayerAttr.Name
  local playJob = gUI_MainPlayerAttr.MenPai
  local playSex = gUI_MainPlayerAttr.Sex
  local playLevel = gUI_MainPlayerAttr.Level
  local itemid
  if Equiped == true then
    itemId = tooltip:InsertLeftText("\181\177\199\176\215\176\177\184", "FFFF00FF", "", 0, 0)
  end
  local incomeName = "1\189\215"
  if level and level > 0 then
    if itemid == nil then
      itemid = tooltip:InsertLeftText(tostring(name) .. " +" .. tostring(level) .. "     ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
    else
      tooltip:InsertLeftText(tostring(name) .. " +" .. tostring(level) .. "     ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
    end
  elseif itemid == nil then
    itemid = tooltip:InsertLeftText(tostring(name) .. "    ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
  else
    tooltip:InsertLeftText(tostring(name) .. "    ", gUI_GOODS_QUALITY_COLOR_PREFIX[quality], "kaiti_13", 0, 0)
  end
  local itemidclr
  if gTip_EquipTypeName[equipKind] ~= "" then
    incomeName = tostring(math.floor((minLevel + 10) / 10)) .. "\189\215"
    if equipKind == 1 then
      itemidclr = tooltip:InsertLeftText(tostring(incomeName) .. tostring(gTip_EquipTypeName[equipKind]) .. "  ", colorWhite, "fzheiti_11", 0, 0)
    elseif equipKind == 21 then
      itemidclr = tooltip:InsertLeftText(tostring(gTip_EquipTypeName[equipKind]) .. "  ", colorWhite, "fzheiti_11", 0, 0)
    else
      itemidclr = tooltip:InsertLeftText(tostring(incomeName) .. tostring(gTip_EquipTypeName[equipKind]) .. "(" .. tostring(gTip_EquipTypeNameNew[equipKind]) .. ")  ", colorWhite, "fzheiti_11", 0, 0)
    end
  end
  itemid1 = tooltip:InsertImage(tostring(gUI_GOODS_QUALITY_IMAGE[quality]), 49, 49, itemid, "RIGHTTOP", 50, 0)
  itemid1 = tooltip:InsertImage(tostring(icon), 49, 49, itemid, "RIGHTTOP", 50, 0)
  if Equiped then
    tooltip:InsertImage(tostring(EQUIPEQUIP), 47, 47, itemid, "RIGHTTOP", 60, -10)
  end
  local strForgeDes, strClrDes
  if curForge == 0 then
    strForgeDes = ""
    strClrDes = ""
  else
    strForgeDes = "\192\250\202\183\215\238\184\223\182\205\212\236\181\200\188\182    +" .. tostring(curForge)
    strClrDes = "\181\177\199\176\182\205\212\236\181\200\188\182    +" .. tostring(curClr)
  end
  itemid = tooltip:InsertLeftText(strForgeDes, colorWhite, "fzheiti_11", 0, 0)
  itemid = tooltip:InsertLeftText(strClrDes, colorWhite, "fzheiti_11", 0, 0)
  if bindInfo == 1 then
    itemidclr = tooltip:InsertLeftText("\210\209\176\243\182\168", colorLightYellow, "fzheiti_11", 0, 0)
  elseif bindInfo == 2 then
    itemidclr = tooltip:InsertLeftText("\202\176\200\161\176\243\182\168", colorDarkYellow, "fzheiti_11", 0, 0)
  elseif bindInfo == 3 then
    if itemidclr == 2 then
      itemidclr = tooltip:InsertLeftText("\215\176\177\184\176\243\182\168", colorGold, "fzheiti_11", 0, 0)
    else
      itemidclr = tooltip:InsertLeftText("\202\185\211\195\176\243\182\168", colorGold, "fzheiti_11", 0, 0)
    end
  else
    itemidclr = tooltip:InsertLeftText("", colorGold, "fzheiti_11", 0, 0)
  end
  color = colorWhite
  if minLevel > playLevel then
    color = colorRed
  end
  if minLevel > 0 then
    if equipPoint > PetUseEquipStart and equipPoint < PetUseEquipEnd then
      itemidclr = tooltip:InsertLeftText("\179\232\206\239\181\200\188\182", colorWhite, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(minLevel), color, itemidclr, "LEFT", 40, 0)
    else
      itemidclr = tooltip:InsertLeftText("\181\200\188\182", colorWhite, "fzheiti_11", 0, 0)
      tooltip:InsertText(tostring(minLevel), color, itemidclr, "LEFT", 40, 0)
    end
  end
  if job ~= -1 then
    color = colorWhite
    if not checkJobKindIsCanUse(playJob, job) then
      color = colorRed
    end
    itemidclr = tooltip:InsertLeftText("\214\176\210\181", colorWhite, "fzheiti_11", 0, 0)
    local job1, job2, job3, job4, job5 = GetJobNamesByJobID(job)
    local jobnames = ""
    if job1 then
      jobnames = jobnames .. job1 .. " "
    end
    if job2 then
      jobnames = jobnames .. job2 .. " "
    end
    if job3 then
      jobnames = jobnames .. job3 .. " "
    end
    if job4 then
      jobnames = jobnames .. job4 .. " "
    end
    if job5 then
      jobnames = jobnames .. job5 .. " "
    end
    tooltip:InsertText(jobnames, color, itemidclr, "LEFT", 40, 0)
  else
    itemidclr = tooltip:InsertLeftText("\214\176\210\181", colorWhite, "fzheiti_11", 0, 0)
    tooltip:InsertText("\200\171\214\176\210\181", colorWhite, itemidclr, "LEFT", 40, 0)
  end
  if sex ~= -1 then
    itemid = tooltip:InsertText("\208\212\177\240 ", colorWhite, itemid, "LEFTBOTTOM", 0, 0)
    color = colorWhite
    if playSex ~= sex then
      color = colorRed
    end
    if sex == 1 then
      tooltip:InsertText("\196\208\208\212", color, itemid, "LEFT", 60, 0)
    else
      tooltip:InsertText("\197\174\208\212", color, itemid, "LEFT", 60, 0)
    end
  end
  if scores ~= nil and scores > 0 then
    tooltip:InsertLeftText(string.format("\215\176\177\184\198\192\183\214  %d\183\214", scores), colorGold, "", 0, 0)
  end
end
function _Tip_ShowSpeciText(tooltip)
  if gTip_SpeciText.text ~= "" then
    local itemid = tooltip:InsertLeftText(gTip_SpeciText.text, gTip_SpeciText.color, gTip_SpeciText.font, 0, 0)
    if gTip_SpeciText.money ~= nil then
      _Tip_ShowMoney(tooltip, gTip_SpeciText.money, itemid)
    end
    gTip_SpeciText.text = ""
    gTip_SpeciText.color = colorWhite
    gTip_SpeciText.font = ""
    gTip_SpeciText.money = nil
  end
end
function _Tip_ShowMoney(tooltip, sellPrice, itemid, isColorRed)
  local color = Lua_UI_GetGoldColor(sellPrice / 10000, true)
  if isColorRed then
    color = colorRed
  end
  local str = Lua_UI_Money2String(sellPrice, false, true)
  str, itemid = _Tip_ShowMoneyUnity(tooltip, str, "g", itemid, color)
  str, itemid = _Tip_ShowMoneyUnity(tooltip, str, "s", itemid, color)
  str, itemid = _Tip_ShowMoneyUnity(tooltip, str, "c", itemid, color)
end
function _Tip_ShowMoneyUnity(tooltip, str, mark, itemid, color)
  local i, j = string.find(str, mark)
  if i ~= nil then
    itemid = tooltip:InsertText(tostring(string.sub(str, 1, i - 1)), color, itemid, "RIGHTTOP", 0, 0)
    itemid = tooltip:InsertImage(gUI_MoneyPic[tostring(mark)], 13, 13, itemid, "RIGHTTOP", 0, 0)
    str = string.sub(str, j + 1, -1)
  end
  return str, itemid
end
function _Tip_ConvertMoney(money)
  local gold = RewMoney / 10000
  local silver = RewMoney - RewMoney / 10000
  local copper = RewMoney - RewMoney / 100 - RewMoney / 10000
  return gold, silver, copper
end
function Lua_Tip_NeedShowRate(id)
  if id >= 100 and id < 200 then
    return true
  elseif id == 56 or id == 57 or id == 58 or id == 59 or id == 60 then
    return true
  elseif id == 202 or id == 204 then
    return true
  else
    return false
  end
end
function Lua_Tip_PostProcessVal(id, value)
  if id == 56 or id == 57 or id == 58 or id == 59 or id == 60 then
    return value / 100
  else
    return value
  end
end
