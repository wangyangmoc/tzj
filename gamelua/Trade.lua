gUI_TRADE_DEFAULT_NUMTIME = 100
local _Trade_Root, _Trade_TradeChildRoot, _Trade_BuySetting, _Trade_QuickBuy, _Trade_SaleSetting, _Trade_QuickSale, _Trade_MyAuction_lbox, _Trade_Buy_lbox, _Trade_TradeNote, _Trade_TradeNote_dbox, _Trade_MyBagMoney_dlab, _Trade_Diamond_dlab, _Trade_Welfare_dlab, _Trade_AllTarde_bg, _Trade_MyTarde_bg, _Trade_Page_tctrl, _Trade_MaxPrice_dlab, _Trade_Price, _Trade_Num, _Trade_CurrShowType, _Trade_SendType
local TRADE_BUY = "购买福利点"
local TRADE_SALE = "出售钻石"
local TRADE_TAXNUM = 0.01
local _Trade_CurrNum = 0
function _Trade_Init()
  _Trade_Root = WindowSys_Instance:GetWindow("Trade_frm")
  _Trade_TradeChildRoot = WindowSys_Instance:GetWindow("Trade_Child_frm")
  _Trade_MyAuction_lbox = WindowToListBox(WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.MyTrade_bg.Mine_bg.MineTrade_bg.MyAuction_lbox"))
  _Trade_Buy_lbox = WindowToListBox(WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.AllTrade_bg.Right_bg.Buy_bg.Buy_lbox"))
  _Trade_MyBagMoney_dlab = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.Money_bg.Money_dlab")
  _Trade_Diamond_dlab = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.Money_bg.Diamond_dlab")
  _Trade_Welfare_dlab = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.Money_bg.Welfare_dlab")
  _Trade_TradeNote_lbox = WindowToListBox(WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.MyTrade_bg.Mine_bg.Notes_bg.Notes_lbox"))
  _Trade_AllTarde_bg = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.AllTrade_bg")
  _Trade_MyTarde_bg = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.MyTrade_bg")
  _Trade_Page_tctrl = WindowToTableCtrl(WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.Page_tctrl"))
  _Trade_MaxPrice_dlab = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.AllTrade_bg.Main_bg.Sell_bg.MaxPrice_bg.MaxPrice_dlab")
  _Trade_AllTarde_bg:SetVisible(true)
  _Trade_MyTarde_bg:SetVisible(false)
  _Trade_Page_tctrl:SelectItem(0)
  UI_Trade_ClearPriceAndNum()
end
function _Trade_ShowBuySaleSetting(showType)
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  if IsRegister() then
    WorldGroup_ShowNewStr(UI_SYS_MSG.TRADE_REGISTER)
    return
  end
  _Trade_CurrShowType = showType
  local CurrSetting = _Trade_GetCurrWnd()
  CurrSetting:SetVisible(true)
  if _Trade_CurrShowType == 0 then
    _Trade_SendType = 1
  elseif _Trade_CurrShowType == 1 then
    _Trade_SendType = 0
  end
  local price = GetCurrPrice(_Trade_SendType)
  local MinPrice_dlab = CurrSetting:GetChildByName("Price_dlab")
  if price and price > 0 then
    local strMoney = Lua_UI_Money2String(price)
    MinPrice_dlab:SetProperty("Text", strMoney)
  elseif _Trade_CurrShowType == 0 then
    MinPrice_dlab:SetProperty("Text", "暂时没有玩家买钻石")
  elseif _Trade_CurrShowType == 1 then
    MinPrice_dlab:SetProperty("Text", "暂时没有玩家卖钻石")
  end
  local Au_ebox = CurrSetting:GetGrandChild("AuDi_bg.Au_ebox")
  local Ag_ebox = CurrSetting:GetGrandChild("AgDi_bg.Ag_ebox")
  local Cu_ebox = CurrSetting:GetGrandChild("CuDi_bg.Cu_ebox")
  Au_ebox:SetProperty("Text", "")
  Ag_ebox:SetProperty("Text", "")
  Cu_ebox:SetProperty("Text", "")
  local Num_ebox = CurrSetting:GetGrandChild("Number_bg.Number_ebox")
  Num_ebox:SetProperty("Text", "")
  local Sum_dlab = CurrSetting:GetChildByName("Pay_dlab")
  Sum_dlab:SetProperty("Text", Lua_UI_Money2String(0))
  local DiamondNum, BagMoney = GetMyMoney()
  local DiamondNum_dlab = CurrSetting:GetChildByName("Money1_dlab")
  DiamondNum_dlab:SetProperty("Text", tostring(DiamondNum))
  local BagMoney_dlab = CurrSetting:GetChildByName("Money_dlab")
  local strBagMoney = Lua_UI_Money2String(BagMoney)
  BagMoney_dlab:SetProperty("Text", strBagMoney)
end
function _Trade_GetCurrWnd()
  if _Trade_CurrShowType == 0 then
    return _Trade_SaleSetting, _Trade_QuickSale
  elseif _Trade_CurrShowType == 1 then
    return _Trade_BuySetting, _Trade_QuickBuy
  end
end
function Lua_Trade_ShowSetting(Type)
end
function Lua_Trade_Show()
  local _, _, _, _, level = GetPlaySelfProp(4)
  if level < 15 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.TRADE_NEED_LEVEL)
    return
  end
  local isVisible = _Trade_Root:IsVisible()
  if isVisible then
    UI_Trade_Hide()
  else
    _Trade_Root:SetVisible(true)
    AskMoneyAuction()
    AskMoneyAuctionSum()
  end
end
function Lua_Trade_Hide()
  _Trade_Root:SetVisible(false)
  _Trade_TradeChildRoot:SetVisible(false)
end
function _OnTrade_UpdateMyAuction(_type)
  _Trade_MyAuction_lbox:RemoveAllItems()
  local SellTable = IsRegister()
  if SellTable and #SellTable then
    local strNum = ""
    for i, value in pairs(SellTable) do
      if value then
        local strMoney = Lua_UI_Money2String(value.Price)
        local strTime = Lua_UI_Formattime(value.ExpireTime)
        strNum = string.format("%d*%d", value.Num, gUI_TRADE_DEFAULT_NUMTIME)
        local strListItem = string.format("%s|%s|%s", strMoney, strNum, strTime)
        _Trade_MyAuction_lbox:InsertString(strListItem, -1)
      end
    end
  end
end
function _OnTrade_UpdateAllAuction(_type)
  local saleTable = GetMoneyAuctionSum()
  local strMoney = ""
  local strNum = ""
  local strListItem = ""
  if _type == 0 then
    _Trade_Buy_lbox:RemoveAllItems()
    for tableIdx, value in pairs(saleTable) do
      if 0 < value.Num then
        strMoney = Lua_UI_Money2String(value.Price)
        strNum = string.format("%d*%d", value.Num, gUI_TRADE_DEFAULT_NUMTIME)
        strListItem = string.format("%s|%s", strMoney, strNum)
        _Trade_Buy_lbox:InsertString(strListItem, -1)
      end
    end
  end
end
function _OnTrade_ShowFeedback(othertype, price, remNum, tradeNum)
  local myType
  if othertype == 0 then
    myType = 1
  elseif othertype == 1 then
    myType = 0
  end
  Messagebox_Show("TRADE_FEEDBACK", myType, math.abs(price), remNum, tradeNum)
end
function _OnTrade_InsertNote(Type, price, num)
  price = math.abs(price)
  local strDate, strTime = GetDateAndTime()
  local strNowTime = string.format("(%s%s):", strDate, strTime)
  local strText = ""
  local strEmpty = ""
  local strUnitPrice = Lua_UI_Money2String(price)
  if Type == 0 then
    local strTotalPrice = Lua_UI_Money2String(price * num)
    strText = string.format("您以%s的单价购买了%d钻石，共花费%s", strUnitPrice, num * gUI_TRADE_DEFAULT_NUMTIME, strTotalPrice)
  elseif Type == 1 then
    local strTax = Lua_UI_Money2String(math.floor(price * num * TRADE_TAXNUM))
    local strTotalPrice = Lua_UI_Money2String(price * num - math.floor(price * num * TRADE_TAXNUM))
    strText = string.format("您以%s的单价出售了%d钻石，共获得%s，扣除了手续费%s", strUnitPrice, num * gUI_TRADE_DEFAULT_NUMTIME, strTotalPrice, strTax)
  else
    return
  end
end
function _OnTrade_UpdateMoney(Type, moneyNum)
  if Type == 1 then
    local strMoney = Lua_UI_Money2String(moneyNum)
    _Trade_MyBagMoney_dlab:SetProperty("Text", strMoney)
  elseif Type == 2 then
    _Trade_Welfare_dlab:SetProperty("Text", tostring(moneyNum))
  elseif Type == 0 then
    _Trade_Diamond_dlab:SetProperty("Text", tostring(moneyNum))
  end
end
function _OnTrade_RequestClearPassward(name)
  Messagebox_Show("TRADA_REQUEST_CLEAR_PASS", name)
end
function _OnTrade_NoteBegin()
  _Trade_TradeNote_lbox:RemoveAllItems()
  _Trade_CurrNum = 0
end
function _OnTrade_NoteAdd(_type, price, num, strOpTime)
  if _Trade_CurrNum >= 30 then
    return
  end
  local strType
  if _type == 0 then
    strType = TRADE_SALE
  else
    strType = TRADE_BUY
  end
  local strMoney = Lua_UI_Money2String(price)
  local strNum = string.format("%d*%d", num, gUI_TRADE_DEFAULT_NUMTIME)
  local strListItem = string.format("%s|%s|%s|%s", strType, strMoney, strNum, strOpTime)
  _Trade_TradeNote_lbox:InsertString(strListItem, -1)
  _Trade_CurrNum = _Trade_CurrNum + 1
end
function _OnTrade_MaxPriceUpdate()
  local MaxPrice = GetMaxPrice()
  strMoney = Lua_UI_Money2String(MaxPrice)
  _Trade_MaxPrice_dlab:SetProperty("Text", strMoney)
end
function UI_Trade_Show()
  Lua_Trade_Show()
end
function UI_Trade_Hide()
  Lua_Trade_Hide()
end
function UI_Tradef_HelpBtnClick(msg)
  local ID = tonumber(WindowToButton(WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.Top_bg.Help_btn")):GetProperty("CustomUserData"))
  Lua_Help_ShowUI_ByPath(ID)
end
function UI_Trade_HideSetting()
  local CurrSetting = _Trade_GetCurrWnd()
  CurrSetting:SetVisible(false)
  WindowSys_Instance:SetKeyboardCaptureWindow(WindowSys_Instance:GetRootWindow())
end
function UI_Trade_ChangeSumPrice(msg)
  local mainWnd = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.AllTrade_bg.Main_bg.Sell_bg")
  local num_ebox = mainWnd:GetGrandChild("SellNum_bg.Num_bg.Num_ebox")
  local numStr = num_ebox:GetProperty("Text")
  if numStr == "" then
    numStr = "0"
  end
  _Trade_Num = tonumber(numStr)
  local diamondNum = GetMyMoney()
  local diamondMaxSetNum = math.floor(diamondNum / gUI_TRADE_DEFAULT_NUMTIME)
  if diamondMaxSetNum < _Trade_Num then
    _Trade_Num = diamondMaxSetNum
    num_ebox:SetProperty("Text", tostring(diamondMaxSetNum))
  end
  local Au_ebox = mainWnd:GetGrandChild("SellPrice_bg.Au_bg.Au_ebox")
  local AuStr = Au_ebox:GetProperty("Text")
  local Ag_ebox = mainWnd:GetGrandChild("SellPrice_bg.Ag_bg.Ag_ebox")
  local AgStr = Ag_ebox:GetProperty("Text")
  local Cu_ebox = mainWnd:GetGrandChild("SellPrice_bg.Cu_bg.Cu_ebox")
  local CuStr = Cu_ebox:GetProperty("Text")
  if AuStr == "" then
    AuStr = "0"
  end
  if AgStr == "" then
    AgStr = "0"
  end
  if CuStr == "" then
    CuStr = "0"
  end
  _Trade_Price = tonumber(AuStr) * 10000 + tonumber(AgStr) * 100 + tonumber(CuStr)
  local maxPrice = GetMaxPrice()
  if maxPrice < _Trade_Price then
    _Trade_Price = maxPrice
    AuStr = tostring(math.floor(maxPrice / 10000))
    AgStr = tostring(math.floor(maxPrice / 100) - tonumber(AuStr) * 100)
    CuStr = tostring(maxPrice % 100)
    Au_ebox:SetProperty("Text", AuStr)
    Ag_ebox:SetProperty("Text", AgStr)
    Cu_ebox:SetProperty("Text", CuStr)
  end
  local SellTotalMoney = _Trade_Num * _Trade_Price
  local strMoney = Lua_UI_Money2String(SellTotalMoney)
  local TotalMoney_dlab = mainWnd:GetGrandChild("SellMoney_bg.Money_dlab")
  TotalMoney_dlab:SetProperty("Text", strMoney)
end
function UI_Trade_ClearPriceAndNum()
  local mainWnd = WindowSys_Instance:GetWindow("Trade_frm.Trade_bg.AllTrade_bg.Main_bg.Sell_bg")
  local num_ebox = mainWnd:GetGrandChild("SellNum_bg.Num_bg.Num_ebox")
  num_ebox:SetProperty("Text", "0")
  local Au_ebox = mainWnd:GetGrandChild("SellPrice_bg.Au_bg.Au_ebox")
  local Ag_ebox = mainWnd:GetGrandChild("SellPrice_bg.Ag_bg.Ag_ebox")
  local Cu_ebox = mainWnd:GetGrandChild("SellPrice_bg.Cu_bg.Cu_ebox")
  Au_ebox:SetProperty("Text", "0")
  Ag_ebox:SetProperty("Text", "0")
  Cu_ebox:SetProperty("Text", "0")
  local TotalMoney_dlab = mainWnd:GetGrandChild("SellMoney_bg.Money_dlab")
  TotalMoney_dlab:SetProperty("Text", Lua_UI_Money2String(0))
  _Trade_Num = 0
  _Trade_Price = 0
end
function UI_Trade_QuickCostRMB()
  OpenWebPage(gUI_AddRMBWeb)
end
function UI_Trade_Quash()
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  local SellTable = IsRegister()
  if not SellTable then
    Lua_Chat_ShowOSD("TRADE_NEED_AUCTION")
    return
  end
  local selectedIndex = _Trade_MyAuction_lbox:GetSelectedItemIndex()
  if selectedIndex < 0 then
    Lua_Chat_ShowOSD("TRADE_NEED_SEL")
    return
  end
  Messagebox_Show("TRADE_AUCTION_QUASH", selectedIndex)
end
function UI_Trade_QuickTrade()
  if GetRemainProtectTime() > 0 then
    Lua_Chat_ShowOSD("SAFE_TIME_LIMIT")
    return
  end
  local SelectedItem = _Trade_Buy_lbox:GetSelectedItem()
  if SelectedItem == nil then
    WorldGroup_ShowNewStr(UI_SYS_MSG.TRADE_NEED_ITEM)
    return
  end
  local strPrcie = SelectedItem:GetText(0)
  _Trade_TradeChildRoot:SetVisible(true)
  local price_dlab = _Trade_TradeChildRoot:GetGrandChild("Buy_bg.BuyMain_bg.Price_bg.Price_dlab")
  price_dlab:SetProperty("Text", strPrcie)
  local num_ebox = _Trade_TradeChildRoot:GetGrandChild("Buy_bg.BuyMain_bg.Number_bg.Number_ebox")
  num_ebox:SetProperty("Text", "")
  local totalPrice_dlab = _Trade_TradeChildRoot:GetGrandChild("Buy_bg.BuyMain_bg.Total_bg.Total_dlab")
  totalPrice_dlab:SetProperty("Text", Lua_UI_Money2String(0))
end
function UI_Trade_NumChange()
  local num_ebox = _Trade_TradeChildRoot:GetGrandChild("Buy_bg.BuyMain_bg.Number_bg.Number_ebox")
  local num = tonumber(num_ebox:GetProperty("Text"))
  if num == nil then
    num = 0
  end
  local SelectedItem = _Trade_Buy_lbox:GetSelectedItem()
  local strPrcie = SelectedItem:GetText(0)
  local price = Lua_UI_String2Money(strPrcie)
  local _, money = GetMyMoney()
  if money < price * num then
    num = math.floor(money / price)
  end
  if num > 10000 then
    num = 999
  end
  num_ebox:SetProperty("Text", tostring(num))
  local totalPrice = price * num
  local strTotalPrice = Lua_UI_Money2String(totalPrice)
  local totalPrice_dlab = _Trade_TradeChildRoot:GetGrandChild("Buy_bg.BuyMain_bg.Total_bg.Total_dlab")
  totalPrice_dlab:SetProperty("Text", strTotalPrice)
end
function UI_Trade_QuickConfirm()
  local num_ebox = _Trade_TradeChildRoot:GetGrandChild("Buy_bg.BuyMain_bg.Number_bg.Number_ebox")
  local num = tonumber(num_ebox:GetProperty("Text"))
  if num == nil or num == 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.TRADE_NEED_NUM)
    return
  end
  local SelectedItem = _Trade_Buy_lbox:GetSelectedItem()
  local strPrcie = SelectedItem:GetText(0)
  local price = Lua_UI_String2Money(strPrcie)
  Messagebox_Show("TRADE_CONFIRM", 0, price, num, false)
  _Trade_TradeChildRoot:SetVisible(false)
end
function UI_Trade_QuickCancel()
  _Trade_TradeChildRoot:SetVisible(false)
end
function UI_Trade_Reflash(msg)
  AskMoneyAuctionSum()
  local btn = msg:get_window()
  btn:SetProperty("Enable", "false")
  btn:SetProperty("Text", "刷新&10&")
end
function UI_Trade_TimeOver(msg)
  local btn = msg:get_window()
  btn:SetProperty("Enable", "true")
  btn:SetProperty("Text", "刷新")
end
function UI_Trade_Auction()
  if _Trade_MyAuction_lbox:GetItemCount() >= 5 then
    Lua_Chat_ShowOSD("TRADE_COUNT")
    return
  end
  if _Trade_Num * _Trade_Price == 0 then
    WorldGroup_ShowNewStr(UI_SYS_MSG.TRADE_NEED_MONEY)
    return
  end
  Messagebox_Show("TRADE_AUCTION_CONFIRM", 0, _Trade_Price, _Trade_Num)
end
function UI_Trade_SelectPage(msg)
  local cur_page = msg:get_wparam()
  if cur_page == 0 then
    _Trade_AllTarde_bg:SetVisible(true)
    _Trade_MyTarde_bg:SetVisible(false)
  else
    _Trade_AllTarde_bg:SetVisible(false)
    _Trade_MyTarde_bg:SetVisible(true)
    AskMoneyAuction()
  end
end
function Script_Trade_OnLoad()
  _Trade_Init()
end
function Script_Trade_OnEvent(event)
  if event == "AUCTIONGOLD_SELFITEMUPDATE" then
    _OnTrade_UpdateMyAuction(arg1)
  elseif event == "AUCTIONGOLD_OTHERITEMUPDATE" then
    _OnTrade_UpdateAllAuction(arg1)
  elseif event == "AUCTIONGOLD_SELFADDNEW" then
    _OnTrade_ShowFeedback(arg1, arg2, arg3, arg4)
  elseif event == "SHOW_AUCTIONGOLD" then
  elseif event == "PLAYER_MONEY_CHANGED" then
    _OnTrade_UpdateMoney(1, arg1)
  elseif event == "PLAYER_RMB_CHANGED" then
    _OnTrade_UpdateMoney(0, arg1)
  elseif event == "PLAYER_BIND_RMB_CHANGED" then
    _OnTrade_UpdateMoney(2, arg1)
  elseif event == "ON_TRADE_NOTIFY_LOCK" then
    _OnTrade_RequestClearPassward(arg1)
  elseif event == "AUCTIONSHOP_NOTE_BEGIN" then
    _OnTrade_NoteBegin()
  elseif event == "AUCTIONSHOP_NOTE_ADD" then
    _OnTrade_NoteAdd(arg1, arg2, arg3, arg4)
  elseif event == "AUCTIONSHOP_MAX_PRICE" then
    _OnTrade_MaxPriceUpdate()
  elseif event == "AUCTIONSHOP_SHOWUI" then
    Lua_Trade_Show()
  elseif event == "CLOSE_NPC_RELATIVE_DIALOG" then
    Lua_Trade_Hide()
  end
end
