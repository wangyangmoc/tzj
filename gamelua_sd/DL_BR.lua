GroupBegin("CPB")

--HG--


STake_Start("HG_DL_BR_wait_000","HG_DL_BR_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait") 
    
STake_Start("HG_DL_BR_wait_000_ui","HG_DL_BR_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()
    
STake_Start("HG_DL_BR_link_000","HG_DL_BR_link_000")
    BlendMode(2)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.504, "DL_BR_MAN_2")

STake_Start("HG_DL_BR_link_001","HG_DL_BR_link_001")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.648, "DL_BR_MAN_1")
    
    
STake_Start("HG_DL_BR_move_001","HG_DL_BR_move_001")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
    --GroundFx(0.831, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.484, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.831, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.484, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    


STake_Start("HG_DL_BR_move_003","HG_DL_BR_move_003")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
    --GroundFx(0.831, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.484, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.450, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.831, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    

    

STake_Start("HG_DL_BR_move_004","HG_DL_BR_move_004")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 
    
    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  

STake_Start("HG_DL_BR_move_005","HG_DL_BR_move_005")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 


    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  

STake_Start("HG_DL_BR_move_006","HG_DL_BR_move_006")
    BlendTime(0.2)
    BlendMode(0)
    BlendPattern("right_left_move")    
    --Sound(0.601,"PC_step_normal")
  Loop() 

    GroundFx(0.432, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.432, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.073, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
  

STake_Start("HG_DL_BR_move_009","HG_DL_BR_move_009")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")    
    --Sound(0.601,"PC_step_normal")
  Loop() 

STake_Start("HG_DL_BR_move_010","HG_DL_BR_move_010")
    BlendTime(0.2)
    BlendMode(0)
    --BlendPattern("Down")        
    --Sound(0.601,"PC_step_normal")
  Loop() 
  
  
    
STake_Start("HG_DL_BR_dodge_000","HG_DL_BR_dodge_000")
    BlendMode(0)
    BlendTime(0.05)

    --Fx(0.015,"")   

STake_Start("HG_DL_BR_def_000","HG_DL_BR_def_000")
    BlendMode(0)
    BlendTime(0.05)
    --Sound(0.00,"")
    --Fx(0.015,"")   
    
    
STake_Start("HG_DL_BR_hit","HG_DL_BR_hit")
    BlendMode(0)
    BlendTime(0.05)
    --Sound(0.059,"voc_derais_injure_01_short_01")
    BlendPattern("Upper_hurt") 
    
STake_Start("HG_DL_BR_dead","HG_DL_BR_dead")
    BlendMode(0)
    BlendTime(0.2)
    StopChannel(0,2)
    --Sound(0.059,"voc_Male_death")
    
  
    
STake_Start("HG_DL_BR_attack_000","HG_DL_BR_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.49)  
    Soft(0.49,1.000)

  	
  	Fx(0.000,"HG_DL_BR_attack_000_fire")
    AttackStart()

  	Hurt(0.35,"hurt_11")

        --Sound(0.000,"")
        --Sound(0.000,"") 
        --HurtSound(0.30,"") 

  	  	
    DragFx(0.35, 0.12, "HG_DL_BR_attack_tracker", "HG_DL_BR_attack_hit", 0, 0, 0, "T_L", "Spine1",0,0,30,0,45,1)  


STake_Start("HG_DL_BR_attack_001","HG_DL_BR_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.49)  
    Soft(0.49,0.966)
 	
  	
  	Fx(0.000,"HG_DL_BR_attack_001_fire")
        
  	AttackStart()

  	Hurt(0.222,"hurt_11")

        --Sound(0.000,"")
        --Sound(0.000,"") 
  	--HurtSound(0.266,"")
  
    DragFx(0.222, 0.12, "HG_DL_BR_attack_tracker", "HG_DL_BR_attack_hit", 0, 0, 0, "T_L", "Spine1",0,0,30,0,-45,1)  


STake_Start("HG_DL_BR_attack_002","HG_DL_BR_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")      
    
    MustPlay(0.000,0.900)  
  	
  	Fx(0.000,"HG_DL_BR_attack_002_fire")
        
    
  	AttackStart()
	
  	Hurt(0.450,"hurt_21")
  	Hurt(0.450,"")
        --Sound(0.000,"")
        --Sound(0.000,"") 
        --HurtSound(0.39,"")
  	
    DragFx(0.450, 0.12, "HG_DL_BR_attack_tracker", "HG_DL_BR_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  
    
    
 
   

STake_Start("HG_DL_BR_skill_000","HG_DL_BR_skill_000")    --一阳指 闭
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.408)

		
		AttackStart()
  	Hurt(0.27,"hurt_21")
  	
  	Fx(0.000,"HG_DL_BR_skill_000_fire")

    DragFx(0.27, 0.12, "HG_DL_BR_skill_000_tracker", "HG_DL_BR_skill_000_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  

  	Charge(0.199,0.066,0,0,2)	 
  	
  	
STake_Start("HG_DL_BR_skill_001","HG_DL_BR_skill_001")    --一阳指 极
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.810)

		
		AttackStart()
  	Hurt(0.34,"hurt_21")
  	
  	Fx(0.000,"HG_DL_BR_skill_001_fire")
        LinkFx(0.34,"HG_DL_BR_skill_001_chain",0.5,"T_R","Spine1",1,0.1)
        AttackHurtFx(0.35,"HG_DL_BR_skill_001_hit","Spine1",1)

  	Charge(0.240,0.100,0,0,2)	 

  	
STake_Start("HG_DL_BR_skill_002","HG_DL_BR_skill_002")    --一阳指 景
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.600)

		
		AttackStart()
  	Hurt(0.38,"hurt_21")
  	
  	
  	Fx(0.000,"HG_DL_BR_skill_002_fire")
        DragFx(0.38, 0.12, "HG_DL_BR_skill_002_tracker", "HG_DL_BR_skill_002_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  
  	DirFx(0.3,"HG_DL_BR_skill_002_hit",25,1)

  	Charge(0.270,0.100,0,0,2)	
  	
  	
STake_Start("HG_DL_BR_skill_003","HG_DL_BR_skill_003")    --先天一阳指
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.953)

		
		AttackStart()
  	Hurt(0.53,"")
  	
  	
  	Fx(0.000,"HG_DL_BR_skill_003_fire")
        DragFx(0.53, 0.2, "HG_DL_BR_skill_003_tracker", "HG_DL_BR_skill_003_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  
  	--DirFx(0.403,"HG_DL_BR_skill_003_hit",15,1,1.1)
  	
  	Charge(0.0,0.2,0,0,2)	 
 
  	
STake_Start("HG_DL_BR_skill_004","HG_DL_BR_skill_004")    --一阳指 灭
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.650)

		
		AttackStart()
  	Hurt(0.360,"hurt_21")
  	
  	
  	Fx(0.000,"HG_DL_BR_skill_004_fire")
        DragFx(0.360, 0.2, "HG_DL_BR_skill_004_tracker", "HG_DL_BR_skill_004_hit", 0, 0, 0, "RightHandIndex2", "Spine1",0,0,0,0,0,1) 
  	DirFx(0.560,"HG_DL_BR_skill_004_hit",25,1)
  	
  	Charge(0.2,0.16,0,0,2)  
  	
STake_Start("HG_DL_BR_skill_005_fire","HG_DL_BR_skill_005_fire")    --点苍炙日 
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.408)

		
		AttackStart()
  	Hurt(0.132,"hurt_21")
  	Fx(0.000,"HG_DL_BR_skill_005_fire")
        DragFx(0.132, 0.2, "HG_DL_BR_skill_005_tracker", "HG_DL_BR_skill_005_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  
  	--DirFx(0.132,"HG_DL_BR_skill_005_hit",25,1)          

  	Charge(0.072,0.072,0,0,2)      
    
STake_Start("HG_DL_BR_skill_005_start","HG_DL_BR_skill_005_start")    --点苍炙日 
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    Loop(1.164,1.200)
    
  	Fx(0.000,"HG_DL_BR_skill_005_start")   
    
  	
STake_Start("HG_DL_BR_skill_006","HG_DL_BR_skill_006")    --X海悬月
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    Loop()
    

		
		AttackStart()
  	Hurt(0.001,"hurt_11")
  	

        Fx(0.38,"HG_DL_BR_skill_006_fire")
        --Fx(0.0,"HG_DL_BR_skill_006_hot")
        DragFx(0.001, 0.5, "HG_DL_BR_skill_006_tracker", "HG_DL_BR_skill_006_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  
  	    --DirFx(0.9,"HG_DL_BR_skill_006_hit",25,1)
  	Charge(0.0,0.1,0,0,2)  
  	
  	
STake_Start("HG_DL_BR_skill_007","HG_DL_BR_skill_007")    --普济往生
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.720)

		
		--AttackStart()
  	--Hurt(0.5,"hurt_11")
  	
  	
  	--Fx(0.000,"HG_DL_BR_skill_007")
  	DirFx(0.5,"HG_DL_BR_skill_007_hit",25,1)
  	
  	
STake_Start("HG_DL_BR_skill_008","HG_DL_BR_skill_008")    --一阳指 封
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.751)

		AttackStart()
  	Hurt(0.469,"hurt_21")		
		
  	
  	Fx(0.000,"HG_DL_BR_skill_008_fire")
  	DirFx(0.469,"HG_DL_BR_skill_008_hit",25,1)
  	Charge(0.43,0.1,0,0,2)  
  	
  	
STake_Start("HG_DL_BR_skill_009","HG_DL_BR_skill_009")    --地涌
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Loop(1.500,2.500)
    
		AttackStart()
  	--Hurt(1.250,"hurt_11")				
  	
  	Fx(0.000,"HG_DL_BR_skill_009_G_fire")             --吸血特效
    LinkFx(0.00,"HG_DL_BR_skill_009_G_loop",1.5,"Spine1","Spine1",0,0)

        AttackHurtFx(0.000,"HG_DL_BR_skill_009_G_hit") 
  	

STake_Start("HG_DL_BR_skill_010","HG_DL_BR_skill_010")    --残伤
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.510)
 	
  	Fx(0.000,"HG_DL_BR_skill_010_start")
  	--DirFx(0.000,"",25,1)
  	
  	
STake_Start("HG_DL_BR_skill_012","HG_DL_BR_skill_012")    --闭月
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.641)
		
  	
  	Fx(0.000,"HG_DL_BR_skill_012_fire")


STake_Start("HG_DL_BR_skill_013","HG_DL_BR_skill_013")    --定影
		BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 
    
    Hard(0.000,0.425)
		
		
		AttackStart()
  	Hurt(0.30,"hurt_21")
  	
  	
  	Fx(0.000,"HG_DL_BR_skill_013_fire")
    DragFx(0.30, 0.5, "HG_DL_BR_skill_013_tracker", "HG_DL_BR_skill_013_hit", 0, 0, 0, "T_L", "Spine1",0,0,0,0,0,1)
  	--DirFx(0.27,"HG_DL_BR_skill_013_hit",25,1)   	
  	
  	
STake_Start("HG_DL_BR_diehuabiying","HG_DL_BR_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"7")       
  	Fx(0.000,"HG_DL_BR_diehuabiying_fire")
  	
  	
STake_Start("HG_DL_BR_shen","HG_DL_BR_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"7")       
  	Fx(0.000,"HG_DL_BR_shen_fire")
      	