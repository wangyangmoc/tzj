
STake_Start("HG_CS_000","HG_CS_000")  --空中挂着
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("HG_CS_001","HG_CS_001")  --被人抱着跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("HG_CS_002","HG_CS_002")  --被人抱着跳
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_YXY_QG_move_004","N_YXY_QG_move_004")  --抱着别人跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_YXY_QG_move_014_start","N_YXY_QG_move_014")  --抱人起跳
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N_YXY_QG_move_014_loop","N_YXY_QG_move_014")  --抱人跳loop
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(2,4)
    Loop(2,4)

STake_Start("N_YXY_QG_move_014_end","N_YXY_QG_move_014")  --抱人跳落地
    BlendMode(0)
    BlendTime(0.1) 
    FrameTime(4.1,4.666)


-------------------------------------------------------------------------------
--试剑崖CUTSCENE用动作

--CS2坐白雕下山

--张阳配

STake_Start("CS_SJY_BDXS_002_HG","HG_CS_BDXS_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_BDXS_003_HG","HG_CS_BDXS_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_jump_HG","HG_QG_move_014(0,22)(23,(40,60))")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.9999,2)

STake_Start("CS_SJY_stand_HG","HG_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_SJY_run_HG","HG_QG_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_SJY_look_HG","HG_emo_002")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.22,4)
    PlaySpeed(0,4,0.5) 
    Loop(3.9999,4)

STake_Start("CS_SJY_look01_HG","HG_CS_BDXS_001")
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0,3.1343,0.9)  
    Loop(3.1342,3.1343)



------------------------------------------------------------------------------------
--马贼王出场CUTSCENE用动作        

--于震轩配

STake_Start("CS_CS_SWGC_NB_MAN_wait001","CS_SWGC_NB_MAN_wait001")
    BlendMode(0)
    BlendTime(0.4) 
    Loop()

STake_Start("CS_SWGC_NB_MAN_emo001_","CS_SWGC_NB_MAN_emo001")
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(0,0.575)
    PlaySpeed(0,0.575,0.75)
    Loop(0.574,0.575)

STake_Start("CS_SWGC_NB_MAN_emo001_part","CS_SWGC_NB_MAN_emo001")
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(0.43,2.133)
    Loop(2.132,2.133)

STake_Start("CS_SWGC_NB_MAN_taitou","CS_SWGC_NB_MAN_emo001")
    BlendMode(0)
    BlendTime(0.4) 
    FrameTime(1.4,2.133) 
    Loop(2.132,2.133)

--蟠龙府邸CUTSCENE用动作

--CS1西毒施毒

--张阳配

STake_Start("CS_PLFD_wait_000_HG","HG_GB_PA_wait_000")
    BlendMode(0)
    BlendTime(0.4) 
    Loop(0)

STake_Start("CS_PLFD_gedang_01_HG","HG_GB_PA_def_wm")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_back_001_HG","HG_C_back_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_back_002_HG","HG_C_back_002")
    BlendMode(0)
    BlendTime(0) 
    FrameTime(0,0.0001)
    Loop(0,0.0001)

STake_Start("CS_PLFD_back_001_loop_HG","HG_C_back_001_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_HAJJL_emo_duobi_HG","CS_PLFD_HG_GB_emo_001")
    BlendMode(0)
    BlendTime(0.4) 
    Loop(3.4659,3.466)

STake_Start("CS_PLFD_HAJJL_emo_zhengtuo_HG","CS_PLFD_HG_GB_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(4.1579,4.158)

STake_Start("CS_PLFD_HAJJL_emo_taopao1_HG","CS_PLFD_HG_GB_emo_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_HAJJL_emo_taopao2_HG","CS_PLFD_HG_GB_emo_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)

STake_Start("CS_PLFD_huanshi_HG","HG_CS_BDXS_001")
    BlendMode(0)
    BlendTime(0) 
    Loop(3.1342,3.1343)

STake_Start("CS_PLFD_gedang_02_HG","HG_CS_emo_dang_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.5175,1.5176)

STake_Start("CS_PLFD_gedang_03_HG","HG_CS_emo_xr_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0.9999,1)

STake_Start("CS_PLFD_look_stop_HG","HG_emo_002")
    BlendMode(0)
    BlendTime(0)
    FrameTime(0.22,0.2201)
    Loop(0.22,0.2201)

------------------------------------
--牛家村开场
--于震轩配


STake_Start("CS_NJC_move-wait_HG","PC_HG_CS_move-wait_000")
    BlendMode(0)
    BlendTime(0.1) 
    PlaySpeed(0,2.658,0.6)
    Loop()

-----------------------------------------------------------------------------------
--赵王府预览

--王林配

STake_Start("CS_life_001_N1_CM03_HG","N1_CM03_life_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_NB_HG_emo_001_HG","HG_ZWF_NB_HG_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_NB_HG_emo_002_HG","HG_ZWF_NB_HG_emo_002")
    BlendMode(0)
    BlendTime(0)    
    FrameTime(1,3.3)                                 
    Loop()

STake_Start("CS_ZWF_NB_HG_emo_002_wait_HG","HG_wait_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_ZWF_NB_HG_emo_002_loop_HG","HG_ZWF_NB_HG_emo_002")
    BlendMode(0)
    BlendTime(0)  
    FrameTime(0,0.000001)                                 
    Loop()

STake_Start("CS_ZWF_NB_HG_emo_003_HG","HG_ZWF_NB_HG_emo_003")
    BlendMode(0)
    BlendTime(0)                                  
    Loop()

STake_Start("CS_ZWF_NB_HG_wait_001_HG","HG_ZWF_NB_HG_wait_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("HG_CS_QRZWF_001_HG","HG_CS_QRZWF_001")
    BlendMode(0)
    BlendTime(0.2) 
    PlaySpeed(0,1.86,0.7)

STake_Start("HG_WS_jump2_004","HG_QG_move_014(0,22)(23,(40,60))_2")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_jump_start","HG_WS_move_004_start")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_HG_WS_jump_004","HG_WS_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZWF_HG_QG_run_004","HG_move_014(0,22)(23,51)(53,72)_2")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()