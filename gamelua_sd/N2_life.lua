STake_Start("N2_wait_000","N2_wait_000")  --N2标准待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_000+","N2_wait_000+")   --N2标准待机+
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

---------------------------------------
STake_Start("N2_bout_001","N2_bout_001")  --出生跳入
    BlendMode(0)
    BlendTime(0) 

----------------------------------------

STake_Start("N2_wait_001","N2_wait_001")  --普通站立
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_002","N2_wait_002")  --坐姿待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_003","N2_wait_003")  --跪地打坐养神
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_004","N2_wait_004")  --躺地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_005","N2_wait_005")  --跪地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_006","N2_wait_006")  --轻伤站立待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_007","N2_wait_007")  --重伤坐地待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_008","N2_wait_008")  --酒店老板娘招呼客人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_008_emo_001","N2_wait_008_emo_001")  --酒店老板娘记账
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_008_emo_002","N2_wait_008_emo_002")  --酒店老板娘打招呼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_008_move_001","N2_wait_008_move_001")  --酒店老板娘走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_009","N2_wait_009")  --卖艺女
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_009_emo_001","N2_wait_009_emo_001")  --卖艺女抱拳行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_009_emo_002","N2_wait_009_emo_002")  --卖艺女坐下休息
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_009_move_0019","N2_wait_009_move_001")  --卖艺女走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_010","N2_wait_010")  --丫鬟服务生待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_010_emo_001","N2_wait_010_emo_001")  --丫鬟服务生受惊
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_010_emo_002","N2_wait_010_emo_002")  --丫鬟服务生鞠躬行礼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_010_emo_003","N2_wait_010_emo_003")  --丫鬟服务生害怕
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_010_emo_004","N2_wait_010_emo_004")  --丫鬟服务生送客
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_010_move_001","N2_wait_010_move_001")  --丫鬟服务生走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_011","N2_wait_011")  --炒菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_011_emo_001","N2_wait_011_emo_001")  --盛菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_012","N2_wait_012")  --服务生端盘子待机
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_012_emo_001","N2_wait_012_emo_001")  --服务生端盘子上菜
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_012_move_001","N2_wait_012_move_001")  --服务生端盘子走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_013","N2_wait_013")  --坐着等菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_013_emo_001","N2_wait_013_emo_001")  --坐着等菜喝茶
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_013_emo_002","N2_wait_013_emo_002")  --坐着等菜聊天
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_wait_014","N2_wait_014")  --弹古筝
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_015","N2_wait_015")  --弹琵琶
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_016","N2_wait_016")  --服务生打算盘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_017","N2_wait_017")  --切菜
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_028","N2_wait_028")  --扎马步waiting
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_wait_028_emo_001","N2_wait_028_emo_001")  --扎马步emo
    BlendMode(0)
    BlendTime(0.2) 

----------------------------------------

STake_Start("N2_move_000","N2_move_000")  --一般走
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("N2_move_001_F","N2_move_001_F")  --前跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_move_001_B","N2_move_001_B")  --后退
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_move_001_L","N2_move_001_L")  --左跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_move_001_R","N2_move_001_R")  --右跑
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_move_003","N2_move_003")  --右踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_move_004","N2_move_004")  --左踏转
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_move_005","N2_move_005-007")  --起跳
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.8333)

STake_Start("N2_move_006","N2_move_006")  --空中待机loop
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N2_move_007","N2_move_005-007")  --落地
    BlendMode(0)
    BlendTime(0.02) 
    FrameTime(0.84,1.566)

STake_Start("N2_move_020","N2_move_020")  --惊恐跑步
    BlendMode(0)
    BlendTime(0.2)
    Loop()

----------------------------------------

STake_Start("N2_life_000","N2_life_000")  --站立伸懒腰
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_001","N2_life_001")  --站立左右看
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_002","N2_life_002")  --站立大笑
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_003","N2_life_003")  --站立哭泣
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_004","N2_life_004")  --跪着哭泣
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_005","N2_life_005")  --站着织网
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_006","N2_life_006")  --打招呼
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_007","N2_life_007")  --招呼客人
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_008","N2_life_008")  --站立说话
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_009","N2_life_009")  --惊恐害怕
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_010","N2_life_010")  --弯腰锄地
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_life_011","N2_life_011")  --弯腰插秧
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_life_012","N2_life_012")  --站着擦汗
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_013","N2_life_013")  --蹲着洗衣服
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_life_014","N2_life_014")  --修理摆弄
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_015","N2_life_015")  --死亡自杀
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_016","N2_life_016")  --坐着聊天
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_017","N2_life_017")  --跪地磕头
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_life_018","N2_life_018")  --蹲下照顾伤员
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_life_019","N2_life_019")  --向前一指
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N2_life_033","N2_life_033")  --打坐
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("N2_life_015_long","N2_life_015")  --表现倒地不起
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.832,1.833)

----------------------------------------

STake_Start("N2_CF01_wait_000","N2_CF01_wait_000")  --村妇
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_CF01_emo_000","N2_CF01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_CF01_emo_001","N2_CF01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_YRFR_wait_000","N2_YRFR_wait_000")  --印染夫人
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_YRFR_emo_000","N2_YRFR_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_YRFR_emo_001","N2_YRFR_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_MGN01_wait_000","N2_MGN01_wait_000")  --蒙古女
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_MGN01_wait_001","N2_MGN01_wait_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_MGN01_emo_001","N2_MGN01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_MGN01_emo_002","N2_MGN01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_MGN01_emo_003","N2_MGN01_emo_003")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_KZLBN_wait_000","N2_KZLBN_wait_000")  --客栈老板娘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_KZLBN_emo_001","N2_KZLBN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_KZLBN_emo_002","N2_KZLBN_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_QJXJ_wait_000","N2_QJXJ_wait_000")  --千金小姐
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_QJXJ_emo_001","N2_QJXJ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_QJXJ_emo_002","N2_QJXJ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_QZZJDZ01_wait_000","N2_QZZJDZ01_wait_000")  --全真中介弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_QZZJDZ01_emo_001","N2_QZZJDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_QZZJDZ01_emo_002","N2_QZZJDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_MGDN_wait_000","N2_MGDN_wait_000")  --蒙古大娘
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_MGDN_emo_001","N2_MGDN_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_MGDN_emo_002","N2_MGDN_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSGJDZ_wait_000","N2_BTSGJDZ_wait_000")  --白驼山高阶女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_BTSGJDZ_emo_001","N2_BTSGJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSGJDZ_emo_002","N2_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSDJDZ_wait_000","N2_BTSDJDZ_wait_000")  --白驼山低阶女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_BTSDJDZ_emo_001","N2_BTSDJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSDJDZ_emo_002","N2_BTSDJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSDJDZ_emo_003","N2_BTSDJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_BTSZJDZ_wait_000","N2_BTSZJDZ_wait_000")  --白驼山中阶女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_BTSZJDZ_emo_001","N2_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSZJDZ_emo_002","N2_BTSZJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_BTSZJDZ_emo_003","N2_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_XN01_wait_000","N2_XN01_wait_000")  --新娘01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_XN01_wait_001","N2_XN01_wait_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_XN01_emo_001","N2_XN01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_XN01_emo_002","N2_XN01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_GBEDNDZ01_wait_000","N2_GBEDNDZ01_wait_000")  --丐帮二袋女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_GBEDNDZ01_emo_001","N2_GBEDNDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_GBEDNDZ01_emo_002","N2_GBEDNDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_GBQDNDZ01_wait_000","N2_GBEDNDZ01_wait_000")  --丐帮七袋女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_GBQDNDZ01_emo_001","N2_GBEDNDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_GBQDNDZ01_emo_002","N2_GBEDNDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    
STake_Start("N2_life_def_000","YP_def_000")  --N2格挡
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N2_life_dodge_000","YP_dodge_000")  --N2躲闪
    BlendMode(0)
    BlendTime(0.2)
    

STake_Start("N2_THDDJNDZ01_wait_000","N2_THDDJNDZ01_wait_000")  --桃花岛低阶女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_THDDJNDZ01_emo_001","N2_THDDJNDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_THDDJNDZ01_emo_002","N2_THDDJNDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_THDZJNDZ01_wait_000","N2_THDZJNDZ01_wait_000")  --桃花岛中阶女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_THDZJNDZ01_emo_001","N2_THDZJNDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_THDZJNDZ01_emo_002","N2_THDZJNDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_THDGJNDZ01_wait_000","N2_THDGJNDZ_wait_000")  --桃花岛高阶女弟子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_THDGJNDZ01_emo_001","N2_THDGJNDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_THDGJNDZ01_emo_002","N2_THDGJNDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2)     

STake_Start("N2_SNZ_wait_000","N2_SNZ_wait_000")             --三娘子
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N2_SNZ_emo_001","N2_SNZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N2_SNZ_emo_002","N2_SNZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("N2_Nvnu_dead_001","N2_life_015")  --死亡
    BlendMode(0)
    BlendTime(0.2)
	FrameTime(1.81467,1.81468)
	Loop()


---------------------------------------------------------------------------------------------------------------------------------------
--CutScene用

--塞外古城

STake_Start("CS_walk","N2_move_000")  --蒙古女2
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_run","N2_move_020")  --蒙古女2
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_talk","N2_QZZJDZ01_emo_001")  --蒙古女2
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_afraidA","N2_life_009")  --蒙古女2
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_around","N2_CF01_emo_001")  --蒙古女2
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



-------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_N2_GBEDNDZ01_emo_001","N2_GBEDNDZ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_N2_BTSGJDZ_wait_000","N2_BTSGJDZ_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_N2_BTSGJDZ_emo_002","N2_BTSGJDZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_N2_MGN01_emo_002","N2_MGN01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_N2_BTSZJDZ_emo_001","N2_BTSZJDZ_emo_001")
    BlendMode(0)
    BlendTime(0.2)



STake_Start("CS_N2_BTSZJDZ_emo_003","N2_BTSZJDZ_emo_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



STake_Start("CS_N2_life_004","N2_life_004")
    BlendMode(0)
    BlendTime(0.2) 


-----------------------------------------------------------------------------------
--CUTSCENE用
-----------------------------------------------------------------------------------
--牛家村开场CS
--俞章廷配

STake_Start("CS_ZhaoHuanHaizi_N2_life","N2_wait_008_emo_002")
    BlendMode(0)
    BlendTime(0.2)
    Loop()


STake_Start("CS_wait_000+_N2_life","N2_wait_000+")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_zhaoHuHuKe_N2_life","N2_life_007")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-----------------------------------------------------------------------------------

--牛家村CUTSCENE用动作

--CS4金兵来袭

--张阳配


STake_Start("CS_dead_stop_000_N2_life","N2_DoubleSupport_dead")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.999,2)
    Loop(1.999,2)

STake_Start("CS_dead_stop_001_N2_life","N2_DoubleSupport_dead_001")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.999,2)
    Loop()

STake_Start("CS_dead_001_N2_life","N2_DoubleSupport_dead_001")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.06,2)
    PlaySpeed(0.06,2,2)
    Loop(1.999,2)

STake_Start("CS_wait_001_emo_001_N2_life","S1_wait_001_emo_001")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dead1_a_N2_life","N2_DoubleSupport_dead")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.3,2)
    PlaySpeed(0.912,1.37,1.3)
    Loop(1.999,2)

STake_Start("CS_Arrow_dead_001_stop","N2_Arrow_dead_001")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,1.3) 
    Loop(1.299,1.3)

STake_Start("CS_dead1_S_N2_life","N2_DoubleSupport_dead")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.999,2)

STake_Start("CS_escape2_N2_life","N2_move_020")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_escape3_N2_life","N2_move_022")
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_dead4_S_N2_life","N2_DoubleSupport_dead_002")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(1.999,2)


