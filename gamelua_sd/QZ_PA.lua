
 
----------------------------------PA------------------------------------------ 
STake_Start("HG_QZ_PA_wait_000","HG_QZ_PA_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")  
    
STake_Start("HG_QZ_PA_wait_000_ui","HG_QZ_PA_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()   

STake_Start("HG_wait_000_QZPA","HG_wait_000_QZPA")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("wait") 
    Loop()
 
STake_Start("HG_wait_000_QZPA_ui","HG_wait_000_QZPA")
    BlendMode(0)
    BlendTime(0)
    Loop()

STake_Start("HG_emo_000_QZPA","HG_emo_000_QZPA")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("HG_emo_001_QZPA","HG_emo_001_QZPA")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("HG_QZ_PA_link_000","HG_QZ_PA_link_000")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.24, "QZ_PA_MAN_2")

STake_Start("HG_QZ_PA_link_001","HG_QZ_PA_link_001")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.924, "QZ_PA_MAN_1")
    
 
STake_Start("HG_QZ_PA_move_003","HG_QZ_PA_move_003")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.381,"Sound_step")
  Loop()


    --GroundFx(0.510, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.095, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.510, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.095, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
 
 
  
STake_Start("HG_QZ_PA_move_004","HG_QZ_PA_move_004")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.410,"Sound_step")
  Loop()


    GroundFx(0.065, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.065, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    


STake_Start("HG_move_012_QZPA","HG_move_012_QZPA")
    BlendTime(0.2)
    BlendMode(1) 

  Loop()


STake_Start("HG_move_013_QZPA","HG_move_013_QZPA")
    BlendTime(0.2)
    BlendMode(1) 

  Loop()
  

STake_Start("HG_QZ_PA_dead","HG_QZ_PA_dead")
    BlendMode(0)
    BlendTime(0.2)
    StopChannel(0,2)
    GroundSound(0.860,"Sound_fall") 
    GroundSound(1.380,"Sound_daodi")

STake_Start("HG_QZ_PA_hit","HG_QZ_PA_hit")
    BlendMode(0)
    BlendTime(0.05)
    Sound(0.059,"voc_derais_injure_01_short_01")
    BlendPattern("Upper_hurt")    


STake_Start("HG_QZ_PA_dodge_000","HG_QZ_PA_dodge_000")
    BlendMode(0)
    BlendTime(0.05)

    Fx(0.015,"HG_THD_SW_dodge_001")   

    GroundSound(0.279,"Sound_fall")
    GroundSound(0.625,"Sound_step")

STake_Start("HG_QZ_PA_def_000","HG_QZ_PA_def_000")
    BlendMode(0)
    BlendTime(0.05)
    Sound(0.00,"weap_Sword_block")
    Fx(0.015,"Fx_gedang_wuqi")   


STake_Start("HG_QZ_PA_attack_000","HG_QZ_PA_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.418)  
    Soft(0.418,1.1)
       
    AttackStart()

  	Hurt(0.26,"hurt_11")
        
        Sound(0.165,"QZ_PA_Attack_11")
        Sound(0.165,"QZ_HG_AttackVOC_1_1")
        HurtSound(0.76,"QZ_PA_Hit_11")    

        Fx(0.000,"HG_QZ_PA_attack_000")
    DragFx(0.26, 0.5, "HG_QZ_PA_attack_tracker", "HG_QZ_PA_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
  	  	

STake_Start("HG_QZ_PA_attack_001","HG_QZ_PA_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.405)  
    Soft(0.405,0.9)
 	        
  	AttackStart()

  	Hurt(0.29,"hurt_11")

        Sound(0.216,"QZ_PA_Attack_11")
        Sound(0.216,"QZ_HG_AttackVOC_1_1")
        HurtSound(0.79,"QZ_PA_Hit_11")

        Fx(0.0,"HG_QZ_PA_attack_001")
    DragFx(0.29, 0.5, "HG_QZ_PA_attack_tracker", "HG_QZ_PA_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  	
  	

STake_Start("HG_QZ_PA_attack_002","HG_QZ_PA_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")      
    
    MustPlay(0.000,1.231)  
    
  	AttackStart()
	
  	Hurt(0.400,"hurt_21")

        Sound(0.249,"QZ_PA_Attack_12")
        Sound(0.249,"QZ_HG_AttackVOC_1_2")
        HurtSound(0.900,"QZ_PA_Hit_12")

  	Fx(0.00,"HG_QZ_PA_attack_002")
    DragFx(0.400, 0.5, "HG_QZ_PA_attack_tracker", "HG_QZ_PA_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  	
  	


---------------------------------------------------------------------------------------------------------------------

STake_Start("HG_QZ_PA_skill_000_start_yin","HG_QZ_PA_skill_000_start")  		--���� �� casting useless
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(1.5,2.5)

  	Fx(0.000,"HG_QZ_PA_skill_000_start")
        
        Sound(0.050,"QZ_PA_Skill_000_1")
        Sound(0.050,"QZ_HG_AttackVOC_2_1") 	


STake_Start("HG_QZ_PA_skill_000_fire_yin","HG_QZ_PA_skill_000_fire")  		--���� �� ���� useless
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.192)
		Soft(0.192,0.6)	--0.392,old
		
  	AttackStart()
  	Hurt(0.041,"hurt_21")
    DragFx(0.041,0.3, "HG_QZ_PA_skill_000_tracker", "HG_QZ_PA_skill_000_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
        
        Sound(0.041,"QZ_PA_Skill_000_2")
        Sound(0.041,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.141,"QZ_PA_Skill_000_hit")

  	Fx(0.041,"HG_QZ_PA_skill_000_fire")

    Charge(0.000,0.103,0,0,2) 	

    StartJA(0.4,500)
    PrivateJAFx(0.2,"Fx_common_JuseAtt_01")
    PrivateJAFx(0.4,"Fx_common_JuseAtt_02")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
    
    
    
STake_Start("HG_QZ_PA_skill_000_start_yang","HG_QZ_PA_skill_000_start")  		--���� �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(1.5,2.5)

  	Fx(0.000,"HG_QZ_PA_skill_000_start")
        
        Sound(0.050,"QZ_PA_Skill_000_1")
        Sound(0.050,"QZ_HG_AttackVOC_2_1") 	

STake_Start("HG_QZ_PA_skill_000_fire_yang","HG_QZ_PA_skill_000_fire")  		--���� �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.15)
		Soft(0.15,0.392)
		
  	AttackStart()
  	Hurt(0.041,"hurt_21")
    DragFx(0.041,0.3, "HG_QZ_PA_skill_000_tracker", "HG_QZ_PA_skill_000_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
        
        Sound(0.041,"QZ_PA_Skill_000_2")
        Sound(0.041,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.141,"QZ_PA_Skill_000_hit")

  	Fx(0.041,"HG_QZ_PA_skill_000_fire")

    Charge(0.000,0.103,0,0,2) 	
        
    
--------------------------------------------------------------------------------------------------------------- 
    
    
    

STake_Start("HG_QZ_PA_skill_001_start_yin","HG_QZ_PA_skill_001_start(loop50-60)")  		--�ɹ���  �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(1.666,2.000)

  	Fx(0.000,"HG_QZ_PA_skill_001_start")
        
        Sound(0.050,"QZ_PA_Skill_001_1")
        Sound(0.050,"QZ_HG_AttackVOC_2_1")  	

STake_Start("HG_QZ_PA_skill_001_fire_yin","HG_QZ_PA_skill_001_fire")  		--�ɹ��� �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.693)

		
  	AttackStart()
  	Hurt(0.039,"hurt_41")

        Sound(0.039,"QZ_PA_Skill_001_2")
        Sound(0.039,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.139,"QZ_PA_Skill_000_hit")
 
        DragFx(0.039, 0.1, "HG_QZ_PA_skill_001_tracker", "HG_QZ_PA_skill_001_aoe", 0, 0, 0, "T_R", "Spine1")

  	Fx(0.000,"HG_QZ_PA_skill_001_fire")

    CameraShake(0.139,"ShakeTimes = 0.05","ShakeFrequency = 0.1","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	
     
    Charge(0.000,0.119,0,0,2) 
    



STake_Start("HG_QZ_PA_skill_001_start_yang","HG_QZ_PA_skill_001_start(loop50-60)")  		--�ɹ��� �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(1.666,2.000)

  	Fx(0.000,"HG_QZ_PA_skill_001_start")
        
        Sound(0.050,"QZ_PA_Skill_001_1")
        Sound(0.050,"QZ_HG_AttackVOC_2_1")  	

STake_Start("HG_QZ_PA_skill_001_fire_yang","HG_QZ_PA_skill_001_fire")  		--�ɹ���  �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.693)

		
  	AttackStart()
  	Hurt(0.039,"hurt_41")

        Sound(0.039,"QZ_PA_Skill_001_2")
        Sound(0.039,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.139,"QZ_PA_Skill_000_hit")
 
        DragFx(0.039, 0.1, "HG_QZ_PA_skill_001_tracker", "HG_QZ_PA_skill_001_aoe", 0, 0, 0, "T_R", "Spine1")

  	Fx(0.000,"HG_QZ_PA_skill_001_fire")

    CameraShake(0.139,"ShakeTimes = 0.05","ShakeFrequency = 0.1","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	
     
    Charge(0.000,0.119,0,0,2) 
    
    
    
-------------------------------------------------------------------------------------------------------------------    
    
    
 

STake_Start("HG_QZ_PA_skill_004_fire_yin","HG_QZ_PA_skill_004_fire")  		--���� �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.699)

  	Fx(0.000,"HG_QZ_PA_skill_004_Yin_start")
  	Fx(0.250,"HG_QZ_PA_skill_004_Yin_fire")

		
  	AttackStart()
  	Hurt(0.25,"hurt_21")


    DirFx(0.25,"HG_QZ_PA_skill_004_Yang_hit",33,1)
	 

    Sound(0.000,"QZ_PA_Skill_004_2")
    Sound(0.050,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.25,"QZ_PA_Skill_000_hit")
    
    



STake_Start("HG_QZ_PA_skill_004_fire_yang","HG_QZ_PA_skill_004_fire")  		--�������
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.699)


		
  	AttackStart()
  	Hurt(0.25,"hurt_21")


  	Fx(0.000,"HG_QZ_PA_skill_014")


    DirFx(0.25,"HG_QZ_PA_skill_004_Yin_hit",33,1)
	 

    Sound(0.000,"QZ_PA_Skill_004_2")
    Sound(0.050,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.25,"QZ_PA_Skill_000_hit")    
    
    
---------------------------------------------------------------------------------------------------------    
    
    

STake_Start("HG_QZ_PA_skill_005_yin","HG_QZ_PA_skill_007_fire+")  		--����ѿ�  ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.6)
		

		
  	--AttackStart()
  	--Hurt(0.414,"")
	--Charge(0.0,0.3,0,0,10)
  	Charge(0.349,0.01,300,0,4)	--added by fumengyan��no select location

  	Fx(0.50,"HG_QZ_PA_skill_005_Yin_fire")
  	Fx(0.01,"HG_QZ_PA_skill_005_Yin_aoe")
  	
  	
    Sound(0.000,"QZ_PA_Skill_005_1")
    Sound(0.080,"QZ_HG_AttackVOC_1_1")
    Sound(0.400,"QZ_PA_Skill_005_2")	
    Sound(0.200,"QZ_PA_Skill_005_3")



STake_Start("HG_QZ_PA_skill_005_yang","HG_QZ_PA_skill_007_fire+")  		--����ѿ�  ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.6)
		

		
  	--AttackStart()
  	--Hurt(0.414,"")
  	--Charge(0.0,0.3,0,0,10)
	Charge(0.349,0.01,300,0,4)	--added by fumengyan��no select location

  	Fx(0.50,"HG_QZ_PA_skill_005_Yang_fire")



    Sound(0.000,"QZ_PA_Skill_005_1")
    Sound(0.080,"QZ_HG_AttackVOC_1_1")
    Sound(0.400,"QZ_PA_Skill_005_2")	
    Sound(0.200,"QZ_PA_Skill_005_3")



----------------------------------------------------------------------------------------------------------


STake_Start("HG_QZ_PA_skill_006_yin","HG_QZ_PA_skill_006")  		--������Ԫ ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,1.035)
		
		
  	AttackStart()
  	Hurt(0.49,"hurt_21")

  	Fx(0.000,"HG_QZ_PA_skill_006_Yin_fire")
        DirFx(0.5,"HG_QZ_PA_skill_hit_Yin",25,1)

    CameraShake(0.49,"ShakeTimes = 0.1","ShakeFrequency = 0.1","ShakeMode = 3","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 2")	

  	HitGroundFx(0.49,"SFX_hitground2")   		 		
    HitGroundFx( 0.49 ,"SFX_hitground")
    
    Sound(0.000,"QZ_PA_Skill_006")
    Sound(0.405,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.49,"QZ_PA_Skill_000_hit")
    
    
    
STake_Start("HG_QZ_PA_skill_006_yang","HG_QZ_PA_skill_006")  		--������Ԫ  ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,1.035)
		
		
  	AttackStart()
  	Hurt(0.49,"hurt_21")

  	Fx(0.000,"HG_QZ_PA_skill_006_Yang_fire")
        DirFx(0.5,"HG_QZ_PA_skill_hit_Yang",25,1)

    CameraShake(0.49,"ShakeTimes = 0.1","ShakeFrequency = 0.1","ShakeMode = 3","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 2")	

  	HitGroundFx(0.49,"SFX_hitground2")   		 		
    HitGroundFx( 0.49 ,"SFX_hitground")
    
    Sound(0.000,"QZ_PA_Skill_006")
    Sound(0.405,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.49,"QZ_PA_Skill_000_hit")    
    
    
  
    
    
------------------------------------------------------------------------------------------------------------------    
 

STake_Start("HG_QZ_PA_skill_007_start_yin","HG_QZ_PA_skill_007_start(28_45)")  		--������ң�� �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(0.93,1.5)
   

  	Fx(0.001,"HG_QZ_PA_skill_007_start")
        Fx(1.00,"HG_QZ_PA_skill_007_loop")


    Sound(0.050,"QZ_PA_Skill_007_1")
    Sound(0.135,"QZ_HG_AttackVOC_3_1")	

STake_Start("HG_QZ_PA_skill_007_fire_yin","HG_QZ_PA_skill_007_fire+")  		--������ң��  �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.369)

		
  	AttackStart()
  	Hurt(0.100,"hurt_21")

    DragFx(0.1, 0.9, "HG_QZ_PA_skill_007_tracker", "HG_QZ_PA_skill_007_aoe", 0, 0, 0, "T_R", "Head")
  	Fx(0.000,"HG_QZ_PA_skill_007_fire") 
    

    --CameraShake(0.699,"ShakeTimes = 0.05","ShakeFrequency = 0.04","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	


    Sound(0.000,"QZ_PA_Skill_007_2")
    Sound(0.098,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.600,"QZ_PA_Skill_007_windhit")
    HurtSound(0.600,"QZ_PA_Skill_000_hit")

    Charge(0.000,0.135,0,0,2)    
    
    
    
STake_Start("HG_QZ_PA_skill_007_start_yang","HG_QZ_PA_skill_007_start(28_45)")  		--������ң��  �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(0.93,1.5)
   

  	Fx(0.001,"HG_QZ_PA_skill_007_start")
        Fx(1.00,"HG_QZ_PA_skill_007_loop")


    Sound(0.050,"QZ_PA_Skill_007_1")
    Sound(0.135,"QZ_HG_AttackVOC_3_1")	

STake_Start("HG_QZ_PA_skill_007_fire_yang","HG_QZ_PA_skill_007_fire+")  		--������ң��  �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.369)

		
  	AttackStart()
  	Hurt(0.100,"hurt_21")

    DragFx(0.1, 0.9, "HG_QZ_PA_skill_007_tracker", "HG_QZ_PA_skill_007_aoe", 0, 0, 0, "T_R", "Head")
  	Fx(0.000,"HG_QZ_PA_skill_007_fire") 
    

    --CameraShake(0.699,"ShakeTimes = 0.05","ShakeFrequency = 0.04","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	


    Sound(0.000,"QZ_PA_Skill_007_2")
    Sound(0.098,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.600,"QZ_PA_Skill_007_windhit")
    HurtSound(0.600,"QZ_PA_Skill_000_hit")

    Charge(0.000,0.135,0,0,2)        
    
    
 
    
    
----------------------------------------------------------------------------------------------------    
    
    

STake_Start("HG_QZ_PA_skill_008_start_yin","HG_QZ_PA_skill_008_start")  		--����1 �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    
    --Loop(1.4,1.5)
   

  	Fx(0.001,"HG_QZ_PA_skill_008_start")
    JAHitFx(0.01,"Fx_common_JuseAtt_03")
      Sound(0.010,"HG_QZ_PA_skill_008")
      Sound(0.100,"QZ_HG_AttackVOC_3_1")	

STake_Start("HG_QZ_PA_skill_008_fire_yin","HG_QZ_PA_skill_008_fire")  		--����1 �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.465)
		Soft(0.465,1.2)	--added


		
  	AttackStart()
  	Hurt(0.015,"hurt_21")

    DragFx(0.015, 0.3, "HG_QZ_PA_skill_008_tracker", "HG_QZ_PA_skill_008_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
  	Fx(0.000,"HG_QZ_PA_skill_008_fire") 

    --CameraShake(0.199,"ShakeTimes = 0.05","ShakeFrequency = 0.04","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	


    Sound(0.010,"QZ_PA_Skill_007_2")
    Sound(0.098,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.315,"QZ_PA_Skill_000_hit")

    --Charge(0.000,0.135,0,0,2)     
    
    
    
STake_Start("HG_QZ_PA_skill_008_start_yang","HG_QZ_PA_skill_008_start")  		--̫���� �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    
    --Loop(1.4,1.5)
   

  	Fx(0.001,"HG_QZ_PA_skill_008_start")

      Sound(0.010,"HG_QZ_PA_skill_008")
      Sound(0.100,"QZ_HG_AttackVOC_3_1")	

STake_Start("HG_QZ_PA_skill_008_fire_yang","HG_QZ_PA_skill_008_fire")  		--̫���� �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.465)

		
  	AttackStart()
  	Hurt(0.015,"hurt_21")

    DragFx(0.015, 0.3, "HG_QZ_PA_skill_008_tracker", "HG_QZ_PA_skill_008_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
  	Fx(0.000,"HG_QZ_PA_skill_008_fire") 

    --CameraShake(0.199,"ShakeTimes = 0.05","ShakeFrequency = 0.04","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	


    Sound(0.010,"QZ_PA_Skill_007_2")
    Sound(0.098,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.315,"QZ_PA_Skill_000_hit")
    
    Charge(0.000,0.135,0,0,2)     
        
    
    
--------------------------------------------------------------------------------------------------------------------    
    
    
    
STake_Start("HG_QZ_PA_skill_009_start_yin","HG_QZ_PA_skill_009_start")  		--������ �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   

    Loop(2.9,3)
   

  	Fx(0.001,"HG_QZ_PA_skill_009_Yin_start")

    LoopFx(1.0, 30.0, 0, "HG_QZ_PA_skill_009_Yin_loop","",1)



    Sound(0.010,"QZ_HG_AttackVOC_1_2")
    Sound(0.090,"QZ_PA_Skill_005_3")
    Sound(1.020,"HG_QZ_PA_skill_009")
    Sound(1.500,"HG_QZ_PA_skill_009_1")	
    Sound(1.300,"QZ_HG_AttackVOC_3_1")

STake_Start("HG_QZ_PA_skill_009_fire_yin","HG_QZ_PA_skill_009_fire")  		--������ �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.375)

		
  	AttackStart()
  	Hurt(0.199,"hurt_21")

  	Fx(0.100,"HG_QZ_PA_skill_009_Yin_fire") 
    
    DirFx(0.699,"HG_QZ_PA_skill_hit_Yin")  

    --CameraShake(0.699,"ShakeTimes = 0.05","ShakeFrequency = 0.04","ShakeMode = 3","ShakeAmplitudeX = 0.5","ShakeAmplitudeY = 0.5","ShakeAmplitudeZ = 0.5")	


    Sound(0.000,"QZ_PA_Skill_007_2")
    Sound(0.050,"QZ_HG_AttackVOC_3_2")	
    HurtSound(0.199,"QZ_PA_Skill_007_windhit")
    HurtSound(0.199,"QZ_PA_Skill_000_hit")
    
    Charge(0.000,0.075,0,0,2)     
    
    
    
STake_Start("HG_QZ_PA_skill_009_start_yang","HG_QZ_PA_skill_009_start")  		--������ �� casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   

    Loop(2.9,3)
   

  	Fx(0.001,"HG_QZ_PA_skill_009_Yang_start")
    LoopFx(1.0, 30.0, 0, "HG_QZ_PA_skill_009_Yang_loop","",1)

    Sound(0.010,"QZ_HG_AttackVOC_1_2")
    Sound(0.090,"QZ_PA_Skill_005_3")
    Sound(1.020,"HG_QZ_PA_skill_009")
    Sound(1.500,"HG_QZ_PA_skill_009_1")	
    Sound(1.300,"QZ_HG_AttackVOC_3_1")

STake_Start("HG_QZ_PA_skill_009_fire_yang","HG_QZ_PA_skill_009_fire")  		--������ �� ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.375)

		
  	AttackStart()
  	--Hurt(0.199,"hurt_21")


    Sound(0.000,"QZ_PA_Skill_007_2")
    Sound(0.050,"QZ_HG_AttackVOC_3_2")	
    --HurtSound(0.199,"QZ_PA_Skill_007_windhit")
    --HurtSound(0.199,"QZ_PA_Skill_000_hit")
    
    Charge(0.000,0.075,0,0,2)     
        
        
    
    
----------------------------------------------------------------------------------------------------    
    
    
    
STake_Start("HG_QZ_PA_skill_010_yin","HG_QZ_PA_skill_010")  		--��Ϊ������ ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 

  
		Hard(0.000,0.999)

		
  	AttackStart()
    
  	
        
    Sound(0.050,"QZ_PA_Skill_007_2")
    Sound(0.050,"HG_QZ_PA_skill_010")



        
STake_Start("HG_QZ_PA_skill_010_yang","HG_QZ_PA_skill_010")  		--��Ϊ������ ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 

  
		Hard(0.000,0.999)

		
  	AttackStart()
    
  	
        
    Sound(0.050,"QZ_PA_Skill_007_2")
    Sound(0.050,"HG_QZ_PA_skill_010")
        
        
         
        
        
---------------------------------------------------------------------------------------------------        



    
STake_Start("HG_QZ_PA_skill_013_yin","HG_QZ_PA_skill_013")  		--����� ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 

  
		Hard(0.000,1.064)
		
		
		
  	AttackStart()		
  	Hurt(0.266,"hurt_21")

		
    Charge(0.172,0.125,0,0,2)
    Charge(0.422,0.564,200,180,4)
 
    
  	
        
    --Sound(0.050,"QZ_PA_Skill_007_2")
    --Sound(0.050,"HG_QZ_PA_skill_010")

  	Fx(0.266,"HG_QZ_PA_skill_013_Yin_fire")
  	
    DragFx(0.266, 0.2, "HG_QZ_PA_skill_013_Yin_tracker", "HG_QZ_PA_skill_013_Yin_hit", 0, 0, 0, "T_R", "Spine1")

    --DirFx(0.266,"HG_QZ_PA_skill_013_Yin_hit")  



    CameraShake(0.266,"ShakeTimes = 0.05","ShakeFrequency = 0.1","ShakeMode = 3","ShakeAmplitudeX = 0","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 0")	


        
STake_Start("HG_QZ_PA_skill_013_yang","HG_QZ_PA_skill_013")  		--�����  ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync") 

  
		Hard(0.000,1.064)
		
		
		
  	AttackStart()		
  	Hurt(0.266,"hurt_21")

		
    Charge(0.172,0.125,0,0,2)
    Charge(0.422,0.564,200,180,4)		
    
  	
        
    --Sound(0.050,"QZ_PA_Skill_007_2")
    --Sound(0.050,"HG_QZ_PA_skill_010")
        
  	Fx(0.266,"HG_QZ_PA_skill_013_Yang_fire")
  	
    DragFx(0.266, 0.2, "HG_QZ_PA_skill_013_Yang_tracker", "HG_QZ_PA_skill_013_Yang_hit", 0, 0, 0, "T_R", "Spine1")

    --DirFx(0.266,"HG_QZ_PA_skill_013_Yang_hit")  

    CameraShake(0.266,"ShakeTimes = 0.05","ShakeFrequency = 0.1","ShakeMode = 3","ShakeAmplitudeX = 0","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 0")	



---------------------------------------------���ü���------------------------------------------------------
        

    
STake_Start("HG_QZ_PA_skill_011","HG_QZ_PA_skill_011")  		--Ǭ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,1.0)

  	Fx(0,"HG_QZ_skill_011_fire")
        

    Sound(0.050,"QZ_PA_Skill_002")
    Sound(0.050,"QZ_HG_AttackVOC_1_1")	
            
    
    
STake_Start("HG_QZ_PA_skill_012","HG_QZ_PA_skill_012")  		--�⾳��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.501)

  	
  	Fx(0,"HG_QZ_PA_skill_012")
        

    Sound(0.050,"HG_QZ_PA_skill_012")
    Sound(0.300,"QZ_HG_AttackVOC_1_1")	
                
    
    
STake_Start("HG_QZ_PA_skill_002","HG_QZ_PA_skill_002")  		--�⾳��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.702)

  	
  	Fx(0,"HG_QZ_PA_skill_002")
        

    Sound(0.050,"HG_QZ_PA_skill_002_new")
    Sound(0.300,"QZ_HG_AttackVOC_1_1")	
                    
    
    
STake_Start("HG_QZ_PA_skill_003","HG_QZ_PA_skill_003")  		--�⾳��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,1.005)

  	
  	Fx(0,"HG_QZ_PA_skill_003")
        

    Sound(0.050,"HG_QZ_PA_skill_003_new")
    Sound(0.800,"QZ_HG_AttackVOC_1_1")	
    

    
STake_Start("HG_QZ_PA_skill_011_1","HG_QZ_PA_skill_011")  		--���ͬ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,1.0)

  	Fx(0,"HG_QZ_PA_skill_000_fire")
        

    Sound(0.050,"QZ_PA_Skill_002")
    Sound(0.050,"QZ_HG_AttackVOC_1_1")	
            
    
STake_Start("HG_QZ_PA_skill_012_1","HG_QZ_PA_skill_012")  		--����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.501)

  	
  	Fx(0,"HG_QZ_skill_001_start")
        

    Sound(0.050,"HG_QZ_PA_skill_012")
    Sound(0.300,"QZ_HG_AttackVOC_1_1")	
                
    
    
STake_Start("HG_QZ_PA_skill_002_1","HG_QZ_PA_skill_002")  		--�����۶�
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.702)

  	
  	Fx(0,"HG_QZ_skill_002_start")
        

    Sound(0.050,"HG_QZ_PA_skill_002_new")
    Sound(0.300,"QZ_HG_AttackVOC_1_1")	  
    
    
STake_Start("HG_QZ_PA_skill_012_2","HG_QZ_PA_skill_012")  		--��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.501)

  	
  	Fx(0,"HG_QZ_skill_003_start")
  	Fx(0.500,"HG_QZ_skill_003_end")            

    Sound(0.050,"HG_QZ_PA_skill_012")
    Sound(0.300,"QZ_HG_AttackVOC_1_1")	    
    
    
STake_Start("HG_QZ_PA_skill_011_2","HG_QZ_PA_skill_011")  		--���칦
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,1.0)

  	Fx(0.880,"HG_QZ_skill_004")
        

    Sound(0.050,"QZ_PA_Skill_002")
    Sound(0.050,"QZ_HG_AttackVOC_1_1")	
    
    
    
STake_Start("HG_QZ_PA_skill_002_2","HG_QZ_PA_skill_002")  		--̫��ӡ��
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.702)

  	Fx(0.000,"HG_QZ_PA_skill_011_start")  	
  	Fx(0.400,"HG_QZ_PA_skill_011_fire")
        

    Sound(0.050,"HG_QZ_PA_skill_002_new")
    Sound(0.300,"QZ_HG_AttackVOC_1_1")	        


    
STake_Start("HG_QZ_PA_skill_000_start_yang_JA","HG_QZ_PA_skill_000_start")  		--���� �� casting--JA����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(1.5,2.5)

  	Fx(0.000,"HG_QZ_PA_skill_007_start")
        
        Sound(0.050,"QZ_PA_Skill_000_1")
        Sound(0.050,"QZ_HG_AttackVOC_2_1") 	

STake_Start("HG_QZ_PA_skill_000_fire_yang_JA","HG_QZ_PA_skill_000_fire")  		--���� �� ����--JA����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.15)
		Soft(0.15,0.392)
		
  	AttackStart()
  	Hurt(0.041,"hurt_21")
    DragFx(0.041,0.3, "HG_QZ_PA_skill_007_tracker", "HG_QZ_PA_skill_007_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
        
        Sound(0.041,"QZ_PA_Skill_000_2")
        Sound(0.041,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.141,"QZ_PA_Skill_000_hit")

  	Fx(0.041,"HG_QZ_PA_skill_000_fire")

    Charge(0.000,0.103,0,0,2) 	



STake_Start("HG_QZ_PA_skill_000_start_xl","HG_QZ_PA_skill_000_start")  		--������ casting
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop(1.5,2.5)

  	--Fx(0.000,"HG_QZ_PA_skill_000_start")
        
        Sound(0.050,"QZ_PA_Skill_000_1")
        Sound(0.050,"QZ_HG_AttackVOC_2_1") 
	
    JAHitFx(0.01,"Fx_common_JuseAtt_03")


STake_Start("HG_QZ_PA_skill_000_fire_xl","HG_QZ_PA_skill_000_fire")  		--������ ����
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.15)
		Soft(0.15,0.692)		--0.392
		
  	AttackStart()
  	Hurt(0.041,"hurt_21")
    DragFx(0.041,0.3, "HG_QZ_PA_skill_000_tracker", "HG_QZ_PA_skill_000_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
        
        Sound(0.041,"QZ_PA_Skill_000_2")
        Sound(0.041,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.141,"QZ_PA_Skill_000_hit")

  	Fx(0.041,"HG_QZ_PA_skill_000_fire")

    StartJA(0.35,500)		--JA
    PrivateJAFx(0.15,"Fx_common_JuseAtt_01")
    PrivateJAFx(0.35,"Fx_common_JuseAtt_02")

    

   -- Charge(0.000,0.103,0,0,2) 	

STake_Start("HG_QZ_PA_skill_000_fire_xl1","HG_QZ_PA_skill_000_fire")  		--������ ����1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.15)
		Soft(0.15,0.692)		--0.392
		
  	AttackStart()
  	Hurt(0.041,"hurt_21")
    DragFx(0.041,0.3, "HG_QZ_PA_skill_000_tracker", "HG_QZ_PA_skill_000_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
        
        Sound(0.041,"QZ_PA_Skill_000_2")
        Sound(0.041,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.141,"QZ_PA_Skill_000_hit")

  	Fx(0.041,"HG_QZ_PA_skill_000_fire")

    StartJA(0.35,500)		--JA
    PrivateJAFx(0.15,"Fx_common_JuseAtt_01")
    PrivateJAFx(0.35,"Fx_common_JuseAtt_02")

    

   -- Charge(0.000,0.103,0,0,2) 	
STake_Start("HG_QZ_PA_skill_000_fire_xl2","HG_QZ_PA_skill_000_fire")  		--������ ����2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")   
  
		Hard(0.000,0.15)
		Soft(0.15,0.692)		--0.392
		
  	AttackStart()
  	Hurt(0.041,"hurt_21")
    DragFx(0.041,0.3, "HG_QZ_PA_skill_007_tracker", "HG_QZ_PA_skill_009_Yang_aoe", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
        
        Sound(0.041,"QZ_PA_Skill_000_2")
        Sound(0.041,"QZ_HG_AttackVOC_2_2") 
        HurtSound(0.141,"QZ_PA_Skill_000_hit")

  	Fx(0.041,"HG_QZ_PA_skill_000_fire")

    StartJA(0.35,500)		--JA
    PrivateJAFx(0.15,"Fx_common_JuseAtt_01")
    PrivateJAFx(0.35,"Fx_common_JuseAtt_02")

    

   -- Charge(0.000,0.103,0,0,2) 	
                
                               