----------------------------HG------------------------------

STake_Start("Ride_HG_casting","Ride_HG_casting")
    BlendMode(0)
    BlendTime(0.2)
    Loop(3.833,5.833)
    --FrameTime(0.00,3.833)

    Sound(0.466,"SKskill")

STake_Start("RideHorse_HG_brake_004","RideHorse_HG_brake_004")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_brake")

STake_Start("RideHorse_HG_brake_005","RideHorse_HG_brake_005")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_brake")

STake_Start("RideHorse_HG_brake_006","RideHorse_HG_brake_006")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_brake")

STake_Start("RideHorse_HG_brake_007","RideHorse_HG_brake_007")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_brake_long")

STake_Start("RideHorse_HG_brake_008","RideHorse_HG_brake_008")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_brake_long")

STake_Start("RideHorse_HG_brake_009","RideHorse_HG_brake_009")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_brake_long")



STake_Start("RideHorse_HG_shangma","RH_HG_emo_000")
    BlendMode(0)
    BlendTime(0)

    Sound(0.010,"Pc_Horse_up")
    
    SkeletonMod(0.0, 0.0)
	
STake_Start("RideHorse_HG_shangma_front","RH_HG_emo_004")
    BlendMode(0)
    BlendTime(0)

    Sound(0.010,"Pc_Horse_up")
    
    SkeletonMod(0.0, 0.0)	
	
STake_Start("RideHorse_HG_xiama","HG_move_014(0,22)(23,51)(53,72)")
    BlendMode(0)
    BlendTime(0)
    FrameTime(1.94,1.941)
	
STake_Start("RideHorse_HG_emo_000","RH_HG_emo_000")
    BlendMode(0)
    BlendTime(0)

    Sound(0.010,"Pc_Horse_up")

STake_Start("RideHorse_HG_emo_001","RideHorse_HG_emo_001")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("RideHorse_HG_emo_002","RH_HG_emo_002")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_HG_emo_003","RH_HG_emo_003")
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("RW_HG_emo_003","RW_HG_emo_003")
    BlendMode(0)
    BlendTime(0.2)	
	
STake_Start("RW_HG_emo_004","RW_HG_emo_003")
    BlendMode(0)
    BlendTime(0.2)	
	FrameTime(0,2.666)
	
STake_Start("RW_HG_emo_005","RW_HG_emo_003")
    BlendMode(0)
    BlendTime(0.2)		
	FrameTime(0,4)
	
STake_Start("RideHorse_HG_hit_000","RideHorse_HG_hit_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("RideHorse_HG_hit_001","RideHorse_HG_hit_001")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.010,"Pc_Horse_runnoise_000")

STake_Start("RideHorse_HG_move_000","RH_HG_move_000")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_HG_move_001","RideHorse_HG_move_001")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_HG_move_002","RideHorse_HG_move_002")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_HG_move_003","RideHorse_HG_move_003")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_HG_move_004","RH_HG_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.302,"Pc_Horse_runnoise_000")
	
STake_Start("RDeer_HG_move_004","RDeer_HG_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.302,"Pc_Horse_runnoise_000")	
	

STake_Start("RideHorse_HG_move_005","RideHorse_HG_move_005")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.302,"Pc_Horse_runnoise_000")

STake_Start("RideHorse_HG_move_006","RideHorse_HG_move_006")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.302,"Pc_Horse_runnoise_000")

STake_Start("RideHorse_HG_move_007","RH_HG_move_007")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    
    
    Sound(0.316,"Pc_Horse_runnoise_000")
	
STake_Start("RideHorse_HG_move_007_D","RH_HG_move_004_D")
    BlendMode(1)
    BlendTime(0.2)
    Loop() 	

STake_Start("RideHorse_HG_move_008","RideHorse_HG_move_008")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.316,"Pc_Horse_runnoise_000")

STake_Start("RideHorse_HG_move_009","RideHorse_HG_move_009")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.316,"Pc_Horse_runnoise_000")

STake_Start("RideHorse_HG_move_012","RH_HG_move_012")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.316,"Pc_Horse_brake_long")

STake_Start("RideHorse_HG_move_013","RH_HG_move_013")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.316,"Pc_Horse_brake_long")

STake_Start("RideHorse_HG_move_015_up","RH_HG_move_015") --ǰ��Ծ
    BlendTime(0.2)
    BlendMode(0)
      
    FrameTime(0,0.8)  
  
STake_Start("RideHorse_HG_move_015_down","RH_HG_move_015") --ǰ��Ծ
    BlendTime(0.2)
    BlendMode(0)
       
    FrameTime(0.8,1.799)      
    
            
STake_Start("RideHorse_HG_move_015_wait","RH_HG_move_015_wait") --ǰ��Ծ
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    


STake_Start("RideHorse_HG_move_017_up","RideHorse_HG_move_017") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
      
    FrameTime(0,0.866)  
  
STake_Start("RideHorse_HG_move_017_down","RideHorse_HG_move_017") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
       
    FrameTime(0.866,1.800)      
    
            
STake_Start("RideHorse_HG_move_017_wait","RideHorse_HG_move_017_wait") --����Ծ
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    


STake_Start("RideHorse_HG_move_018_up","RideHorse_HG_move_018") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
      
    FrameTime(0,0.866)  
  
STake_Start("RideHorse_HG_move_018_down","RideHorse_HG_move_018") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
       
    FrameTime(0.866,1.800)      
    
            
STake_Start("RideHorse_HG_move_018_wait","RideHorse_HG_move_018_wait") --����Ծ
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    

STake_Start("RideHorse_HG_move_23","RH_HG_move_23")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_HG_move_24","RH_HG_move_24")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_HG_move_23_45degree","RH_HG_move_23")
    BlendMode(1)
    BlendTime(0.2)
    Loop()       

    Sound(0.130,"Pc_Horse_brake_long")

STake_Start("RideHorse_HG_move_24_45degree","RH_HG_move_24")
    BlendMode(1)
    BlendTime(0.2)
    Loop()         

    Sound(0.233,"Pc_Horse_brake_long")

STake_Start("RideHorse_HG_wait_000","RH_HG_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("RideHorse_HG_wait_000_D","RH_HG_wait_000_D")
    BlendMode(0)
    BlendTime(0.2)
    Loop()	

STake_Start("RideHorse_HG_wait_001","RideHorse_HG_wait_001")
    BlendMode(0)
    BlendTime(0.2)

    Loop()

STake_Start("RideHorse_HG_wait_002","RideHorse_HG_wait_002")
    BlendMode(0)
    BlendTime(0.2)

    Loop()
    
STake_Start("RideHorse_HG_life_000","RH_HG_life_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    
    
STake_Start("RideHorse_HG_dodge_000","RideHorse_HG_dodge_000")
    BlendMode(0)
    BlendTime(0.2)    
    
    
----------------------------SG----------------------------------
STake_Start("Ride_SG_casting","Ride_SG_casting")
    BlendMode(0)
    BlendTime(0.2)
    Loop(3.833,5.833)
    --FrameTime(0.00,3.833)
    
    Sound(0.466,"SKskill")

STake_Start("RideHorse_SG_brake_004","RideHorse_SG_brake_004")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_brake_005","RideHorse_SG_brake_005")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_brake_006","RideHorse_SG_brake_006")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_brake_007","RideHorse_SG_brake_007")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_brake_008","RideHorse_SG_brake_008")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_brake_009","RideHorse_SG_brake_009")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_shangma","RH_SG_emo_000")
    BlendMode(0)
    BlendTime(0)
    --FrameTime(0.69,0.7)
	--Fx(0.690,"SG_THD_SW_skill_000_new_fire2")
    Sound(0.010,"Pc_Horse_up")    
    SkeletonMod(0.0, 0.0)
	
STake_Start("RideHorse_SG_shangma_front","RH_SG_emo_004")
    BlendMode(0)
    BlendTime(0)
    --FrameTime(0.69,0.7)
	--Fx(0.690,"SG_THD_SW_skill_000_new_fire2")
    Sound(0.010,"Pc_Horse_up")    
    SkeletonMod(0.0, 0.0)	

STake_Start("RideHorse_SG_xiama","SG_move_014(0,25)(26,(50,90))(92,112)")
    BlendMode(0)
    BlendTime(0)
    FrameTime(3.28,3.281)
	

STake_Start("RideHorse_SG_emo_000","RH_SG_emo_000")
    BlendMode(0)
    BlendTime(0)



STake_Start("RideHorse_SG_emo_001","RH_SG_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("RideHorse_SG_emo_002","RH_SG_emo_002")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_emo_003","RH_SG_emo_003")
    BlendMode(0)
    BlendTime(0.2)
	
STake_Start("RW_SG_emo_003","RW_SG_emo_003")
    BlendMode(0)
    BlendTime(0.2)	

STake_Start("RW_SG_emo_004","RW_SG_emo_003")
    BlendMode(0)
    BlendTime(0.2)	
	FrameTime(0,2.666)
	
STake_Start("RW_SG_emo_005","RW_SG_emo_003")
    BlendMode(0)
    BlendTime(0.2)		
	FrameTime(0,4)	
	
STake_Start("RideHorse_SG_hit_000","RideHorse_SG_hit_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("RideHorse_SG_hit_001","RideHorse_SG_hit_001")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("RideHorse_SG_move_000","RH_SG_move_000")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_SG_move_001","RideHorse_SG_move_001")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_SG_move_002","RideHorse_SG_move_002")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_SG_move_003","RideHorse_SG_move_003")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

STake_Start("RideHorse_SG_move_004","RH_SG_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    
	
STake_Start("RDeer_SG_move_004","RDeer_SG_move_004")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    

    Sound(0.302,"Pc_Horse_runnoise_000")	

STake_Start("RideHorse_SG_move_005","RideHorse_SG_move_005")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_006","RideHorse_SG_move_006")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_007","RH_SG_move_007")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    
 
STake_Start("RideHorse_SG_move_007_D","RH_SG_move_004_D")
    BlendMode(1)
    BlendTime(0.2)
    Loop() 
 

STake_Start("RideHorse_SG_move_008","RideHorse_SG_move_008")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_009","RideHorse_SG_move_009")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_012","RH_SG_move_012")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_013","RH_SG_move_013")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_015_up","RH_SG_move_015") --ǰ��Ծ
    BlendTime(0.2)
    BlendMode(0)

    FrameTime(0.000,0.8)          
  
STake_Start("RideHorse_SG_move_015_down","RH_SG_move_015") --ǰ��Ծ
    BlendTime(0.2)
    BlendMode(0)
       
      
    FrameTime(0.8,1.799)      
            
STake_Start("RideHorse_SG_move_015_wait","RH_SG_move_015_wait") --ǰ��Ծ
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    


STake_Start("RideHorse_SG_move_017_up","RideHorse_SG_move_017") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
    
    FrameTime(0.000,0.866)  
      
  
STake_Start("RideHorse_SG_move_017_down","RideHorse_SG_move_017") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
       
    FrameTime(0.866,1.800)  
            
STake_Start("RideHorse_SG_move_017_wait","RideHorse_SG_move_017_wait") --����Ծ
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    


STake_Start("RideHorse_SG_move_018_up","RideHorse_SG_move_018") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
    
    FrameTime(0.000,0.866) 
      
  
STake_Start("RideHorse_SG_move_018_down","RideHorse_SG_move_018") --����Ծ
    BlendTime(0.2)
    BlendMode(0)
       
    FrameTime(0.866,1.800)  
            
STake_Start("RideHorse_SG_move_018_wait","RideHorse_SG_move_018_wait") --����Ծ
    BlendMode(0)
    BlendTime(0.2)     
    Loop()    

STake_Start("RideHorse_SG_move_023","RideHorse_SG_move_023")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_024","RideHorse_SG_move_024")
    BlendMode(1)
    BlendTime(0.2)
    Loop()    


STake_Start("RideHorse_SG_move_023_45degree","RideHorse_SG_move_023")
    BlendMode(1)
    BlendTime(0.2)
    Loop()       


STake_Start("RideHorse_SG_move_024_45degree","RideHorse_SG_move_024")
    BlendMode(1)
    BlendTime(0.2)
    Loop()         


STake_Start("RideHorse_SG_wait_000","RH_SG_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	
STake_Start("RideHorse_SG_wait_000_D","RH_SG_wait_000_D")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
	

STake_Start("RideHorse_SG_wait_001","RideHorse_SG_wait_001")
    BlendMode(0)
    BlendTime(0.2)

    Loop()

STake_Start("RideHorse_SG_wait_002","RideHorse_SG_wait_002")
    BlendMode(0)
    BlendTime(0.2)

    Loop()
    
STake_Start("RideHorse_SG_life_000","RH_SG_life_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    
    
STake_Start("RideHorse_SG_dodge_000","RideHorse_SG_dodge_000")
    BlendMode(0)
    BlendTime(0.2)        
    
    