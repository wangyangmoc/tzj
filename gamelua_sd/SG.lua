GroupBegin("CPB")

--SG--

STake_Start("SG_Meridian_down","SG_wait_010(0,109)(110,170)(180,260)")
    BlendMode(0)
    BlendTime(0.2)
    Loop(3.633,6)
    FrameTime(0.000,6) 

    GroundFx(0.693, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(1.126, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(1.646, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.693, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(1.126, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(1.646, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)	
    GroundSound(0.693,"Sound_step")
    GroundSound(1.126,"Sound_step") 
    GroundSound(1.646,"Sound_step")
    
STake_Start("SG_Meridian_down_ui","SG_wait_010(0,109)(110,170)(180,260)")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(3.666,5.5) 

STake_Start("SG_Meridian_up","SG_wait_010(0,109)(110,170)(180,260)")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(6,8.666)  

    GroundSound(7.106,"Sound_jump")
    GroundSound(7.366,"Sound_step")
    GroundSound(8.059,"Sound_step")
    GroundFx(7.399, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(8.059, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(7.399, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(8.059, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)	

STake_Start("SG_fishing_wait_000","SG_wait_000")  --�������
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,0.001)  

STake_Start("SG_wait_000","SG_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    
STake_Start("SG_wait_000_ui","SG_wait_xuanjue+")
    BlendMode(0)
    BlendTime(0.2)
    Loop(0.667,8)
    
STake_Start("SG_wait_000_select","SG_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()    

STake_Start("SG_emo_000","SG_emo_000")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_emo_001","SG_emo_001")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("SG_move_001","SG_move_001")    
    BlendTime(0.2)
    BlendMode(1)
    GroundFx(0.181, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.597, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)
    GroundFx(0.181, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.597, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)	
    GroundSound(0.181,"Sound_step")
    GroundSound(0.597,"Sound_step")

  Loop()               

STake_Start("SG_move_004","SG_move_004")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.029,"Sound_step")
    GroundSound(0.373,"Sound_step")
    GroundFx(0.029, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.373, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.029, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.373, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	

  Loop()

STake_Start("SG_move_004_THD","SG_move_004_new")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.029,"Sound_step")
    GroundSound(0.373,"Sound_step")
    GroundFx(0.029, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.373, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
    GroundFx(0.029, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.373, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
  Loop()

STake_Start("SG_move_005","SG_move_005")    
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.058,"Sound_step")
    GroundSound(0.425,"Sound_step")
    GroundFx(0.058, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.425, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.058, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.425, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 		
  Loop()

STake_Start("SG_move_006","SG_move_006")
    BlendTime(0.2)
    BlendMode(1)
    GroundSound(0.065,"Sound_step")
    GroundSound(0.373,"Sound_step")
    GroundFx(0.065, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.373, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.065, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.373, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 		
  Loop()

STake_Start("SG_PY_move_004","SG_PY_move_004")         --ǰ����
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5")   


    Fx(0.1,"SG_SL_ST_skill_move","",1.2,0,0,5,-3,0,0,0,1.4)	
	
    Sound(0.010,"char_05_cloth_flap_02")
    Sound(0.010,"N2_BYJQ01_dodge_001")
    GroundFx(0.01, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.15, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)   
    GroundFx(0.01, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.15, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundSound(0.01,"Sound_step")
    GroundSound(0.15,"Sound_step") 
    EffectStart(0.0,"MB",4)  
  
  
  
STake_Start("SG_SU_move_004","SG_SU_move_004")  --ǰ������
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.27,"Sound_step")
    GroundSound(0.576,"Sound_step")
    GroundFx(0.27, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.576, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.27, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.576, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	

STake_Start("SG_SU_move_001","SG_SU_move_001")  --�������
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.3,"Sound_step")
    GroundSound(0.558,"Sound_step")
    GroundFx(0.3, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.558, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.3, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.558, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	

STake_Start("SG_SU_move_005","SG_SU_move_005")  --�������
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.252,"Sound_step")
    GroundSound(0.558,"Sound_step")
    GroundFx(0.252, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.558, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 		
    GroundFx(0.252, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.558, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 
	
	
STake_Start("SG_SU_move_006","SG_SU_move_006")  --�Ҽ�����
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    --EffectStart(0, "FOV", 15)
    GroundSound(0.312,"Sound_step")
    GroundSound(0.6,"Sound_step")
    GroundFx(0.312, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.6, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 		
    GroundFx(0.312, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.6, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)
	
	
STake_Start("SG_move_012","SG_move_012")
    BlendTime(0.2)
    BlendMode(0)
    GroundSound(0.594,"Sound_step")
    GroundSound(0.804,"Sound_step")
    GroundFx(0.594, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.804, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.594, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.804, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
  Loop() 

STake_Start("SG_move_013","SG_move_013")
    BlendTime(0.2)
    BlendMode(0)
    GroundSound(0.396,"Sound_step")
    GroundSound(0.851,"Sound_step")
    GroundFx(0.396, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.851, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 	
    GroundFx(0.396, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.851, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0) 		
  Loop()

STake_Start("SG_move_014_up","SG_move_014(0,25)(26,(50,90))(92,112)")  --ԭ����start
    BlendTime(0.1)
    BlendMode(0)


    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
      
    FrameTime(0,0.833)



STake_Start("SG_move_014_loop","SG_move_014(0,25)(26,(50,90))(92,112)")  --ԭ����loop
    BlendTime(0.2)
    BlendMode(0)
      
    FrameTime(0.866,3.000)
    Loop(1.666,3)


STake_Start("SG_move_014_down","SG_move_014(0,25)(26,(50,90))(92,112)")   --ԭ����end
    BlendTime(0.1)
    BlendMode(0)
    StopChannel(0,2)
	
    GroundSound(3.090,"Sound_fall")
      
    FrameTime(3.066,3.733)

STake_Start("SG_move_014_def","SG_move_014(0,25)(26,(50,90))(92,112)")  --ԭ�������и񵲴���
    BlendTime(0.2)
    BlendMode(0)
    Priority(0.866,"5")     
    FrameTime(0.866,0.867)
    --Loop(1.666,3)



STake_Start("SG_move_015_up","SG_move_015(0,25)(26,(50,90))(92,112)") --ǰ��start
    BlendTime(0.1)
    BlendMode(0)
 
    FrameTime(0,0.833)

    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")

STake_Start("SG_move_015_loop","SG_move_015(0,25)(26,(50,90))(92,112)")  --ǰ��loop
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.74,1.7)

    FrameTime(0.866,3.000)
    Loop(1.666,3)
    

STake_Start("SG_move_015_down","SG_move_015(0,25)(26,(50,90))(92,112)")  --ǰ��end
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    StopChannel(0,2)
	
    GroundSound(3.090,"Sound_fall")


STake_Start("SG_move_016_up","SG_move_016(0,25)(26,(50,90))(92,112)")   --����start
    BlendTime(0.1)
    BlendMode(0)

    FrameTime(0,0.833)

    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")


STake_Start("SG_move_016_loop","SG_move_016(0,25)(26,(50,90))(92,112)")   --����loop
    BlendTime(0.2)
    BlendMode(0)

    FrameTime(0.866,3.000)
    Loop(1.666,3)


STake_Start("SG_move_016_down","SG_move_016(0,25)(26,(50,90))(92,112)")   --����end
    BlendTime(0.1)
    BlendMode(1)
    FrameTime(3.066,3.733)
    StopChannel(0,2)
	
    GroundSound(3.090,"Sound_fall")


STake_Start("SG_move_017_up","SG_move_017(0,25)(26,(50,90))(92,112)")  --����start
    BlendTime(0.1)
    BlendMode(0)

    FrameTime(0,0.833)

    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")

STake_Start("SG_move_017_loop","SG_move_017(0,25)(26,(50,90))(92,112)")  --����loop
    BlendTime(0.2)
    BlendMode(0)
    
    FrameTime(0.866,3.000)
    Loop(1.666,3)

STake_Start("SG_move_017_down","SG_move_017(0,25)(26,(50,90))(92,112)")   --����end
    BlendTime(0.1)
    BlendMode(1)
    FrameTime(3.066,3.733)
    StopChannel(0,2)
	
    GroundSound(3.090,"Sound_fall")

STake_Start("SG_move_018_up","SG_move_018(0,25)(26,(50,90))(92,112)")  --����start
    BlendTime(0.1)
    BlendMode(0)
    
    FrameTime(0,0.833)

    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")

STake_Start("SG_move_018_loop","SG_move_018(0,25)(26,(50,90))(92,112)")  --����loop
    BlendTime(0.2)
    BlendMode(0)

    FrameTime(0.866,3.000)
    Loop(1.666,3)


STake_Start("SG_move_018_down","SG_move_018(0,25)(26,(50,90))(92,112)")  --����end
    BlendTime(0.1)
    BlendMode(1)
    FrameTime(3.066,3.733)
    StopChannel(0,2)
	
    GroundSound(3.090,"Sound_fall")
    
--------------------------------------------------------------------------


STake_Start("SG_move_014_up_2","SG_move_014(0,25)(26,(50,90))(92,112)_2")  --ԭ��2���� ����
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.833)

STake_Start("SG_move_014_loop_2","SG_move_014(0,25)(26,(50,90))(92,112)_2")  --ԭ��2���� ����
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.866,3.000)
    Loop(1.666,3)



STake_Start("SG_move_014_down_2","SG_move_014(0,25)(26,(50,90))(92,112)_2")  --ԭ��2���� ���
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    GroundSound(1.8,"Sound_fall")       
    StopChannel(0,2)

------------------------------------------------------------------------

STake_Start("SG_move_017_up_2","SG_move_017(0,25)(26,(50,90))(92,112)_2")  --��2���� ����
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.833)

STake_Start("SG_move_017_loop_2","SG_move_017(0,25)(26,(50,90))(92,112)_2")  --��2���� ����
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.866,3.000)
    Loop(1.666,3)



STake_Start("SG_move_017_down_2","SG_move_017(0,25)(26,(50,90))(92,112)_2")  --��2���� ���
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    GroundSound(1.8,"Sound_fall")       
    StopChannel(0,2)

------------------------------------------------------------------------

STake_Start("SG_move_018_up_2","SG_move_018(0,25)(26,(50,90))(92,112)_2")  --��2���� ����
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.833)

STake_Start("SG_move_018_loop_2","SG_move_018(0,25)(26,(50,90))(92,112)_2")  --��2���� ����
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.866,3.000)
    Loop(1.666,3)



STake_Start("SG_move_018_down_2","SG_move_018(0,25)(26,(50,90))(92,112)_2")  --��2���� ���
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    GroundSound(1.8,"Sound_fall")       
    StopChannel(0,2)

------------------------------------------------------------------------


STake_Start("SG_move_014_up_3","SG_move_014(0,25)(26,(50,90))(92,112)_3")  --ԭ��3���� ����
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.833)

STake_Start("SG_move_014_loop_3","SG_move_014(0,25)(26,(50,90))(92,112)_3")  --ԭ��3���� ����
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.866,3.000)
    Loop(1.666,3)



STake_Start("SG_move_014_down_3","SG_move_014(0,25)(26,(50,90))(92,112)_3")  --ԭ��3���� ���
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    GroundSound(1.8,"Sound_fall")       
    StopChannel(0,2)


--------------------------------------------------------------------------
STake_Start("SG_move_017_up_3","SG_move_017(0,25)(26,(50,90))(92,112)_3")  --��3���� ����
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.833)

STake_Start("SG_move_017_loop_3","SG_move_017(0,25)(26,(50,90))(92,112)_3")  --��3���� ����
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.866,3.000)
    Loop(1.666,3)



STake_Start("SG_move_017_down_3","SG_move_017(0,25)(26,(50,90))(92,112)_3")  --��3���� ���
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    GroundSound(1.8,"Sound_fall")       
    StopChannel(0,2)

------------------------------------------------------------------------

STake_Start("SG_move_018_up_3","SG_move_018(0,25)(26,(50,90))(92,112)_3")  --��3���� ����
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.833)

STake_Start("SG_move_018_loop_3","SG_move_018(0,25)(26,(50,90))(92,112)_3")  --��3���� ����
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.866,3.000)
    Loop(1.666,3)



STake_Start("SG_move_018_down_3","SG_move_018(0,25)(26,(50,90))(92,112)_3")  --��3���� ���
    BlendTime(0.1)
    BlendMode(0)
    FrameTime(3.066,3.733)
    GroundSound(1.8,"Sound_fall")       
    StopChannel(0,2)

------------------------------------------------------------------------

STake_Start("SG_wait_002","SG_wait_002")
    BlendTime(0.2)
    BlendMode(0)
   Loop()

STake_Start("SG_wait_003","SG_wait_003")
    BlendTime(0.2)
    BlendMode(0)
   Loop()

STake_Start("SG_wait_004","SG_wait_004")
    BlendTime(0.2)
    BlendMode(0)
   Loop()

STake_Start("SG_wait_005","SG_wait_005")
    BlendTime(0.2)
    BlendMode(0)
   Loop()


STake_Start("SG_GB_ST_link_000","SG_GB_ST_link_000")
    BlendTime(0.05)
    BlendMode(2)
    
STake_Start("SG_GB_ST_link_001","SG_GB_ST_link_001")
    BlendTime(0.05)
    BlendMode(0) 


STake_Start("SG_hit_000","SG_hit_000")  --ǰ����
    BlendTime(0.2)
    BlendMode(0)

    HurtHard(0,0.3)
    Priority(0.3,"5")
    GroundSound(0.120,"Sound_step")
    GroundSound(0.250,"Sound_step")
    ContralShake(0,0.1,0,0.8)
	
STake_Start("SG_hit_001","SG_hit_001")--�����
    BlendTime(0.2)
    BlendMode(0)

    --[[Priority(0.300,"5")
    Charge(0.000,0.300,30,0,5)]]--
    HurtHard(0,0.3)
    Priority(0.3,"5")	
    GroundSound(0.160,"Sound_step")
    GroundSound(0.310,"Sound_step")
    ContralShake(0,0.1,0,0.8)
	
STake_Start("SG_hit_002","SG_hit_002")--�����
    BlendTime(0.2)
    BlendMode(0)
    Priority(0.3,"5")
    HurtHard(0,0.3)

    GroundSound(0.270,"Sound_step")
    ContralShake(0,0.1,0,0.8)
	
STake_Start("SG_hit_003","SG_hit_003")--�һ���
    BlendTime(0.2)
    BlendMode(0)
    Priority(0.3,"5")
    HurtHard(0,0.3)

    GroundSound(0.350,"Sound_step")
    ContralShake(0,0.1,0,0.8)
	
STake_Start("SG_hit_005","SG_hit_005")
    BlendTime(0.2)
    BlendMode(0)

    HurtHard(0,1.666)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")

STake_Start("SG_hit_006","SG_hit_006")
    BlendTime(0.2)
    BlendMode(0)

    HurtHard(0,1.666)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")  

STake_Start("SG_hit_007","SG_hit_007")
    BlendTime(0.2)
    BlendMode(0)

    HurtHard(0,1.666)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi") 

STake_Start("SG_hit_008","SG_hit_008")
    BlendTime(0.2)
    BlendMode(0)

    HurtHard(0,1.666)

    GroundSound(0.433,"Sound_daodi")

STake_Start("SG_hit_016_start","SG_hit_016")  --ǰ����start
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    HurtHard(0,0.666)
    ContralShake(0,0.45,0,0.9)	

STake_Start("SG_hit_016_loop","SG_hit_016_loop")  --ǰ����loop
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    HurtHard(0,1)
	Loop()

STake_Start("SG_hit_016_end","SG_hit_016_down")  --ǰ����end
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.2,"5")	
    HurtHard(0,1.2)
	Sound(0,"HG_Hit_005")
    GroundSound(0.01,"")  
	
STake_Start("SG_hit_021_start","SG_hit_021")  --ǰ����start
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    HurtHard(0,0.666)
    ContralShake(0,0.3,0,0.8)		
	Sound(0,"HG_Hit_005")
	
STake_Start("SG_hit_021_loop","SG_hit_021_loop")  --ǰ����loop
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    HurtHard(0,2)
	Loop()

STake_Start("SG_hit_021_end","SG_hit_021_down")  --ǰ����end
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.5,"5")	
    HurtHard(0,0.5)


STake_Start("SG_hit_stun","SG_hit_009(loop6-66)")
    BlendTime(0.2)
    BlendMode(0)

    Loop(0.2,2.2)

STake_Start("SG_Earthquake_start","SG_hit_009(loop6-66)")  --����ʼ
    BlendTime(0.2)
    BlendMode(0)

    Loop(0.2,2.2)
	
    CameraShake(0,"ShakeMode = 3","ShakeTimes = 10","ShakeFrequency = 0.1","ShakeAmplitudeX = 5","ShakeAmplitudeY = 5","ShakeAmplitudeZ = 8")	
	
STake_Start("SG_Earthquake_fire","SG_wait_000")  --�������
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,0.001)  	
	
STake_Start("SG_hit_plug_air","SG_hit_010")
    BlendTime(0.2)
    BlendMode(0)

    Loop(0,0.333)

STake_Start("SG_hit_plug_ground","SG_hit_011")
    BlendTime(0.2)
    BlendMode(0)
    HurtHard(0,1)

STake_Start("SG_hit_plug_in","SG_hit_012")
    BlendTime(0.2)
    BlendMode(0)

    Loop(0,0.333)



STake_Start("SG_dead_001","SG_dead_001")
    BlendTime(0.2)
    BlendMode(0)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")
	StopChannel(0,2)

--[[STake_Start("SG_hit_L_000","SG_hit_L_000")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")
    HurtHard(0,1)
    Charge(0.000,0.300,30,0,5)

    GroundSound(0.120,"Sound_step")
    GroundSound(0.250,"Sound_step")

STake_Start("SG_hit_L_001","SG_hit_L_001")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")
    HurtHard(0,1)
    Charge(0.000,0.300,30,0,5)

    GroundSound(0.160,"Sound_step")
    GroundSound(0.310,"Sound_step")

STake_Start("SG_hit_L_002","SG_hit_L_002")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")
    HurtHard(0,1)
    Charge(0.000,0.300,30,0,5)
    GroundSound(0.270,"Sound_step")

STake_Start("SG_hit_L_003","SG_hit_L_003")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")
    HurtHard(0,1)
    Charge(0.000,0.300,30,0,5)

    GroundSound(0.350,"Sound_step")

STake_Start("SG_hit_L_005","SG_hit_L_005")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,100,0,5,1.2)

    Sound(0.1666,"SG_Hit_005")
    GroundSound(0.433,"Sound_daodi")

STake_Start("SG_hit_L_006","SG_hit_L_006")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,100,0,5,1.2)

    Sound(0.1666,"SG_Hit_005")
    GroundSound(0.433,"Sound_daodi")  

STake_Start("SG_hit_L_007","SG_hit_L_007")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,100,0,5,1.2)

    Sound(0.1666,"SG_Hit_005")
    GroundSound(0.433,"Sound_daodi") 

STake_Start("SG_hit_L_008","SG_hit_L_008")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,100,0,5,1.2)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")

STake_Start("SG_hit_L_009","SG_hit_L_009(loop6-66)")
    BlendTime(0.2)
    BlendMode(0)

    Loop(0.2,2.2)

STake_Start("SG_dead_L_001","SG_dead_L_001")
    BlendTime(0.2)
    BlendMode(0)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")


STake_Start("SG_hit_000_2","SG_hit_000")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,60,0,5)

    GroundSound(0.120,"Sound_step")
    GroundSound(0.250,"Sound_step")

STake_Start("SG_hit_001_2","SG_hit_001")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,60,0,5)
    GroundSound(0.160,"Sound_step")
    GroundSound(0.310,"Sound_step")

STake_Start("SG_hit_002_2","SG_hit_002")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,60,0,5)

    Charge(0.000,0.300,30,0,5)
    GroundSound(0.270,"Sound_step")

STake_Start("SG_hit_003_2","SG_hit_003")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(0.300,"5")

    Charge(0.000,0.300,60,0,5)

    GroundSound(0.350,"Sound_step")    
    
STake_Start("SG_hit_0053","SG_hit_005")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,200,0,5,1.2)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")

STake_Start("SG_hit_0063","SG_hit_006")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,200,0,5,1.2)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi") 

STake_Start("SG_hit_0073","SG_hit_007")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,200,0,5,1.2)

    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi") 

STake_Start("SG_hit_0083","SG_hit_008")
    BlendTime(0.2)
    BlendMode(0)
    Priority(0,"1")
    Priority(1.470,"5")
    HurtHard(0,1.666)
    Charge(0.000,0.470,200,0,5,1.2)


    Sound(0.1666,"HG_Hit_005")
    GroundSound(0.433,"Sound_daodi")]]--
-----------------------------------------------------------------ս������---------------------------------------------------------------------------

STake_Start("SG_life_ZC_001","SG_life_ZC_001")         --�ӹ���	16183
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	PlaySpeed(0.15,0.63,2)
    Fx(0,"HG_life_ZC_001_fire",1,1,0,0,0,0,0,0,1)	
    Fx(0.63,"HG_life_ZC_001_fire_1",1,1,0,0,0,0,0,0,1)
	Sound(0.03,"HG_GB_ST_Skill_004")
	
	AttackStart()
    Hurt(0.1,"hurt_21",0,1)
	HurtSound(0.3,"MQX_hit_0")
    Charge(0,0.2,0,20,21)
	CameraShake(0.15,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")	
	
STake_Start("SG_life_ZC_002","SG_life_ZC_002")         --��ʯ��	16182
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"HG_life_ZC_002",2,1,0,0,-20,0,0,0,1)
    Fx(0,"HG_life_ZC_002",2,1,0,-40,-20,0,0,0,1)	
	Sound(0.03,"HG_GB_ST_Skill_015_2")


	AttackStart()
    Hurt(0.18,"hurt_21",0,1)
	HurtSound(0.18,"QZ_PA_Hit_11")
    HurtBoneFx(0.18,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)	

STake_Start("SG_life_ZC_003","SG_life_ZC_003")         --����ͷ	16185
    BlendMode(0)
    BlendTime(0.2)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	FrameTime(0,1.4)
	Loop(0.233,1.4)
    Fx(0,"HG_life_ZC_003_start",1,1,0,0,0,0,0,0,1)
    Fx(0.233,"HG_life_ZC_003_start",1,1,0,0,0,0,0,0,1)	
    Fx(0.233,"HG_life_ZC_003_fire",1,1,0,0,0,0,0,0,1)	
	Sound(0.03,"Life_Recover_Loop_1")
	
	
STake_Start("SG_life_ZC_003_fire","SG_life_ZC_003")         --����ͷ	16185
    BlendMode(0)
    BlendTime(0.2)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	FrameTime(1.4,1.7)	
    Fx(1.4,"HG_life_ZC_003_fire",1,1,0,0,0,0,0,0,1)		
	
STake_Start("SG_life_ZC_003_Q","SG_life_ZC_003_Q")         --����ͷ	Q
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	Sound(0.03,"TY_Attack_006")
	
	AttackStart()
    Hurt(0,"hurt_21",0,1)
	HurtSound(0,"PA_Hit_001")
    HurtBoneFx(0,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)

STake_Start("SG_life_ZC_004","SG_life_ZC_004")         --��Х	16184
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0.3,"HG_life_ZC_004",2,1,0,0,0,0,0,0,1)	
	Sound(0.03,"HG_THD_QMDJ_skill_001")
	Sound(0.01,"SG_attackVoc_5")
	Sound(0.27,"BTS_FAN_skill_008_att02")

	
	AttackStart()
    Hurt(0.3,"hurt_71",0,1)
	HurtSound(0.3,"QZ_PA_Hit_11")
    HurtBoneFx(0.3,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	
STake_Start("SG_life_ZC_005","SG_life_ZC_005")         --����ֹˮ	16186
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"HG_life_ZC_005",1,1,0,0,0,0,0,0,1)	
	Sound(0.03,"QZ_PA_Skill_007_2")

	
	
STake_Start("SG_life_ZC_006","SG_life_ZC_006")         --��������	16189
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	Loop(0.2,1)
    Fx(0.1,"HG_life_ZC_006",1,1,0,0,0,0,0,0,1)
	Sound(0.03,"QZ_PA_Skill_007_windhit")



STake_Start("SG_life_ZC_007","SG_life_ZC_007")         --��Ѫ�ӻ�	16188
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	PlaySpeed(0,0.4,2)
	Charge(0.01,0.2,300,20,12)	
	Charge(0.02,0.2,300,20,22)
	Charge(0.03,0.2,300,20,12)	
    Fx(0,"HG_life_ZC_007_fire1",1,1,0,0,0,0,0,0,1)
	Sound(0.03,"Skill_Fire_FireBall_01")
	
STake_Start("SG_life_ZC_007_1","SG_life_ZC_007_Q")         --��Ѫ�ӻ�	16209
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0.2,"HG_life_ZC_007_fire2",1,1,0,0,0,0,0,0,1)
	Sound(0.03,"QZ_SW_Skill013_Start2")
	
	
STake_Start("SG_life_ZC_008","SG_life_ZC_008")         --Ѫɫ����	16192
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0.5,"HG_life_ZC_008",3,1,0,0,-55,0,0,0,1)
	Sound(0.03,"HG_GB_PA_Skill_006_2")
	
	AttackStart()
    Hurt(0.49,"hurt_21",0,1)
	HurtSound(0.49,"QZ_PA_Hit_11")
    HurtBoneFx(0.49,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)
	CameraShake(0.49,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")
	
STake_Start("SG_life_ZC_009","SG_life_ZC_009")         --�������	16194
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"HG_life_ZC_009")
	Sound(0.03,"HG_GB_PA_skill_005")




STake_Start("SG_life_ZC_010","SG_life_ZC_010")         --��˿����	16195
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"HG_life_ZC_010_fire")
    Charge(0,0.3,5,250,12)	
	Sound(0.03,"HG_GB_PA_Skill_001_Combo1")
	Sound(0.8,"GB_New2_attack_003")
	CameraShake(0.39,"ShakeMode = 3","ShakeTimes = 0.2","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")	
	
STake_Start("SG_life_ZC_kong","SG_life_emo004")         --��˿�����ſ�	16195
    BlendMode(0)
    BlendTime(0.2)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 	
	Sound(0.2,"N2_BYJQ01_deadVOC_001")


	
STake_Start("SG_life_ZC_011","SG_life_ZC_011")         --Ы֮��	16196
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	Sound(0.03,"QZ_SW_QXQJ_fire")

    Fx(0,"HG_life_ZC_011",1,1,0,0,0,0,0,0,1)


 
STake_Start("SG_life_ZC_012","SG_life_ZC_012")         --���߿���	16198
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
	Sound(0.03,"FX_Dead_Light1")
	CameraShake(0.3,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")


STake_Start("SG_life_ZC_013","SG_life_ZC_013")         --����ҩˮ	16200
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 	
    Fx(0,"HG_life_emo013",2,1,0,0,0,0,0,0,1)	
	Sound(0.03,"HG_SL_SB_Skill_000")


STake_Start("SG_life_ZC_014","SG_life_ZC_014")         --��ҩˮ	16201
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"HG_life_emo014",2.2,1,0,0,-40,0,0,0,1)
	Sound(0.23,"QZ_SW_Skill_008_2")
	CameraShake(1.2,"ShakeMode = 3","ShakeTimes = 0.3","ShakeFrequency = 0.033","ShakeAmplitudeX = 2","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 8")

STake_Start("SG_life_ZC_015","SG_life_ZC_015")         --��ҩˮ	16202
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"HG_life_emo015_fire")
    DrawDragFx(0.36, 0, 0.2, "HG_life_emo015_T", "", 0, 0, 0, "T_R", 0, 0, 0, 0, 0, 0, 0,0,1,1)
	Sound(0.03,"QZ_PA_Skill_005_1")
	
STake_Start("SG_life_ZC_016","SG_life_ZC_016_001")         --ҽ��ת��	16204
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 	
    Fx(0,"FX_ZC_Buff_YS_YB_start")	
	Sound(0.03,"HG_QZ_PA_skill_012")

STake_Start("SG_life_ZC_Equip1","SG_life_ZC_003_Q")         --װ���ӷ���
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 	
	Sound(0.03,"HG_SL_ST_Skill_009")
	
    DragFx(0.3, 0.2, "NB_LZW_skill_006_tracker", "N1_Dart_skill_001_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,0,0,0,1)
	
	AttackStart()
    Hurt(0.3,"hurt_61",0,1)
	HurtSound(0.3,"ST_Hit_002")
    HurtBoneFx(0.3,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)
	

STake_Start("SG_SL_ZCskill_flag","SG_SL_ST_skill_013")         --���ְ��켼��
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 	
    HideWeapon(0)
    Fx(0,"N1_SBTR01_skill_002_start","Reference",1,0,0,0,0,0,0,0,1.1)	
	Sound(0.01,"HG_SL_ST_Skill_001")
	Sound(0.23,"SG_SoundVOC_Jump_1")
	Sound(0.23,"HG_SL_ST_Skill_013")

STake_Start("SG_GB_ZCskill_flag","SG_GB_PA_skill_028")   -- ؤ����켼��
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("steady") 
    Priority(0,"4")     
	FrameTime(0,1.892)
    HideWeapon(0)
    Fx(0,"HG_GB_PA_skill_028_fire","Reference",1.2,0,0,0,0,0,0,0,1,2)
    Fx(0,"NB_TZD_skill_011_fire1","Reference",0.6,0,0,0,0,0,0,0,2,2)
    Fx(0.321,"HG_GB_PA_skill_028_1","Reference",0.75,0,0,0,10,0,0,0,1,2)
	Sound(0.01,"SG_SoundVOC_attack5_1")
	Sound(0.01,"HG_GB_PA_Skill_000_Combo1")
	Sound(0.4,"HG_GB_PA_skill_017")
	Sound(0.2,"GB_New2_skill_005")
	Sound(1.5,"SG_SoundVOC_attack3_1")
	Sound(1.5,"HG_GB_PA_skill_005")	

	AttackStart()	
    Hurt(0,"hurt_81",0,1)	
	DirFx(0,"HG_GB_PA_attack_B_001_hit",25,1)		
	HurtSound(0.02,"BTS_CR_HIT_new_2_1") 
    HurtBoneFx(0,"FX_Violent_blood_PT","Spine1",1,0,0,0,0,0,0)
    ContralShake(1.68,0.65,0,0.9)		
	
	
STake_Start("SG_THD_ZCskill_flag","SG_THD_SW_attack_A_JA_003")   --�һ������켼��
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("steady")  
    Priority(0,"5")     
    HideWeapon(0)
    Fx(0.3,"T1_JGQS_skill_004_fire","Reference",0.7,0,0,100,0,0,0,0,1,1)
    CameraShake(0.35,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.2","ShakeAmplitudeX = 8","ShakeAmplitudeY = 8","ShakeAmplitudeZ = 5")
	Sound(0.02,"HG_THD_SW_skill_000")
	Sound(0.12,"HG_THD_QMDJ_skill_002_Yanshibizhen")
	Sound(0.15,"SG_SoundVOC_attack3_1")


STake_Start("SG_QZ_ZCskill_flag","SG_QZ_PA_skill_009_start")   --ȫ����켼��
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("steady")  
    Priority(0,"5")     
    HideWeapon(0)
    Fx(0.01,"FX_QZ_Duoshan","Reference",1,0,0,0,0,0,0,0,1,1)
    Fx(0.01,"SG_QZ_SW_attack_B_001","Reference",1,0,0,0,0,0,0,0,0.7,1)
    Fx(0.84,"Fx_round_water","Reference",0.8,0,0,15,0,0,0,0,1,1)
	Sound(0.03,"QZ_SW_Skill_011_1")
	Sound(2.978,"QZ_SW_Skill013_Fire_2")
	Sound(0.15,"SG_SoundVOC_attack1_1")

----------------------------------------------------------------------------------------------------------------------------------------------------

STake_Start("SG_life_029_snowball","SG_life_029")         --��ѩ��  10621
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    HideWeapon(0)	
	PlaySpeed(0,0.44,2)
	Sound(0.03,"TY_Attack_005")
    --Fx(0,"FX_SDJ_snowball","T_R",1,1,0,0,0,0,0,0,3.5)	
	
	AttackStart()
    Hurt(0.22,"hurt_21",0,1)
	HurtSound(0.22,"SnowBallHit")
    HurtBoneFx(0.22,"FX_blood_siyao_a","Spine1",1,0,0,0,0,0,0)

STake_Start("SG_life_skill_snowball_001","SG_QZ_SW_attack_A_001_new")   -- ��ǹѩ��	 16301
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
    HideWeapon(0)	
	Fx(0.1,"FX_skill_Snowbattle_001_tracker","Reference",1,1,0,0,0,0,0,0,1)
	Sound(0.03,"TY_Attack_005")
	Sound(0.03,"QZ_SW_Skill014_Start2")

	AttackStart()
	Hurt(0.35,"hurt_21",0,1)
	HurtSound(0.35,"SnowBallHit")
	AttackHurtFx(0.35, "FX_SDJ_snowball_hit", "Spine1",0.7)
	CameraShake(0.35,"ShakeMode = 3","ShakeTimes = 0.15","ShakeFrequency = 0.025","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 5")	
	
	
STake_Start("SG_life_skill_snowball_002","SG_THD_SW_skill_006_001")   -- ��ǹѩ��	16302
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	Loop()
    HideWeapon(0)
	Fx(0.01,"FX_skill_Snowbattle_002","Reference",1,1,0,0,3,0,0,0,1,2)  
	Sound(0.03,"TY_Attack_005")
	
  	AttackStart()	
    Hurt(0,"hurt_21",0,1) 	
	HurtSound(0,"SnowBallHit")
	DirFx(0,"FX_SDJ_snowball_hit",0,1)


	
STake_Start("SG_life_skill_snowball_002_fire","SG_THD_SW_skill_006_002")   -- ��ǹѩ�����  
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("steady")
	
	
	
STake_Start("SG_life_skill_snowball_003","SG_QZ_SW_attack_B_002++")   -- ����ǹѩ��	  16303
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
    HideWeapon(0)	
	Fx(0.25,"FX_skill_Snowbattle_003","Reference",1,1,0,-20,3,0,0,0,1) 
	Sound(0.13,"QZ_PA_Skill_000_2")
	Sound(0.23,"QZ_SW_Skill013_Start2")

	AttackStart()
	Hurt(0.25,"hurt_71",0,1)
	HurtSound(0.25,"SnowBallHit")
	AttackHurtFx(0.25, "FX_SDJ_snowball_hit", "Spine1",0.7)
	
	
STake_Start("SG_life_skill_snowball_004","SG_life_ZC_015")   -- ѩ��	16304
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
    HideWeapon(0)	
    DrawDragFx(0.36, 0, 0.4,"FX_skill_Snowbattle_004_tracker", "", 0, 0, 0, "RightHand", 0, 0, 0, 0, 0, 0, 0,0,1,1) 
	Sound(0.13,"TY_Launch_Air_S")
	Sound(0.23,"QZ_SW_Skill012_Start1")

	AttackStart()
	Hurt(0.76,"hurt_71",0,1)
	HurtSound(0.76,"SnowBallHit")
	HurtSound(0.76,"Bomm_002")
	AttackHurtFx(0.76, "FX_SDJ_snowball_hit", "Spine1",0.7)		
	
	
STake_Start("SG_life_skill_zhadan_000","SG_life_ZC_015")   -- ը��	
	BlendMode(0)
	BlendTime(0.2)
	Priority(0,"5")     
	BlendPattern("steady")  
    HideWeapon(0)	
    DrawDragFx(0.36, 0, 0.4,"FX_LT_HuoYaoTong_tracker", "", 0, 0, 0, "RightHand", 0, 0, 0, 0, 0, 0, 0,0,1,1) 
	Sound(0.13,"TY_Launch_Air_S")
	Sound(0.23,"QZ_SW_Skill012_Start1")

	AttackStart()
	Hurt(0.76,"hurt_71",0,1)
	HurtSound(0.76,"SnowBallHit")
	HurtSound(0.76,"Bomm_002")
	AttackHurtFx(0.76, "FX_SDJ_snowball_hit", "Spine1",0.7)	
	
----------------------------------------------------------------------------------------------------------------------------------------------------

STake_Start("SG_QG_move_001","SG_QG_move_001") --ss����
    BlendTime(0.2)
    BlendMode(0)
 
 

--------------------------------------------------------------------------

STake_Start("SG_QG_move_004_start","SG_QG_move_004_start")    --�Ṧǰ��������240
    BlendTime(0.05)
    BlendMode(0)

    GroundFx(0.255, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.255, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      


    EffectStart(0, "MB",0)
 
    GroundSound(0.135,"Sound_step")
    GroundSound(0.400,"Sound_jump")


STake_Start("SG_QG_move_004_end","SG_QG_move_004_end")    --�Ṧǰ������ɲ��240
    BlendTime(0.02)
    BlendMode(0)


    GroundFx(0.101, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.101, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.161, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.161, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.090,"Sound_brake")
    GroundSound(0.690,"Sound_step")
    GroundSound(0.920,"Sound_step")

    EffectStart(0, "FOV", 0)


STake_Start("SG_QG_move_004","SG_QG_move_004")    --�Ṧǰ������240
    BlendTime(0.2)
    BlendMode(1)

    GroundFx(0.498, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.498, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    Loop()    
    
    Fx(0,"FX_move_QG_common")


    EffectStart(0, "FOV", 5)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.122,"Sound_step")
    GroundSound(0.389,"Sound_step")

-----------------------------------------------------------------------------

STake_Start("SG_QG_move_005_start","SG_QG_move_005_start")    --�Ṧ���������240
    BlendTime(0.05)
    BlendMode(0)

    GroundFx(0.255, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.255, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    EffectStart(0, "MB",0)

    GroundSound(0.135,"Sound_step")
    GroundSound(0.400,"Sound_jump")

STake_Start("SG_QG_move_005_end","SG_QG_move_005_end")    --�Ṧ�������ɲ��240
    BlendTime(0.02)
    BlendMode(0)


    GroundFx(0.101, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.101, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.161, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.161, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.090,"Sound_brake")
    GroundSound(0.690,"Sound_step")
    GroundSound(0.920,"Sound_step")

    EffectStart(0, "FOV", 15)

STake_Start("SG_QG_move_005","SG_QG_move_005")    --�Ṧ�������240
    BlendTime(0.2)
    BlendMode(1)

    GroundFx(0.498, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.498, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    Loop()    
    
    Fx(0,"FX_move_QG_common")

    EffectStart(0, "FOV", 15)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3)
    EffectStart(0, "MB",0)

    GroundSound(0.122,"Sound_step")
    GroundSound(0.389,"Sound_step")

----------------------------------------------------------------------------

STake_Start("SG_QG_move_006_start","SG_QG_move_006_start")    --�Ṧ�Ҽ�������240
    BlendTime(0.05)
    BlendMode(0)

    GroundFx(0.255, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.255, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    EffectStart(0, "MB",0)

    GroundSound(0.135,"Sound_step")
    GroundSound(0.400,"Sound_jump")

STake_Start("SG_QG_move_006_end","SG_QG_move_006_end")    --�Ṧ�Ҽ�����ɲ��240
    BlendTime(0.02)
    BlendMode(0)


    GroundFx(0.101, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.101, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.161, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.171, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.090,"Sound_brake")
    GroundSound(0.690,"Sound_step")
    GroundSound(0.920,"Sound_step")

    EffectStart(0, "FOV", 15)

STake_Start("SG_QG_move_006","SG_QG_move_006")    --�Ṧ�Ҽ�����240
    BlendTime(0.2)
    BlendMode(1)

    GroundFx(0.498, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.498, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.175, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    Loop()    
    
    Fx(0,"FX_move_QG_common")

    EffectStart(0, "FOV", 15)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3)
    EffectStart(0, "MB",0)

    GroundSound(0.122,"Sound_step")
    GroundSound(0.389,"Sound_step")

-----------------------------------------------------------------------------

STake_Start("SG_QG_move_014_up","SG_QG_move_014(0,22)(23,(40,60))")  --�Ṧǰ����
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_01") 
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_014_loop","SG_QG_move_014(0,22)(23,(40,60))")  --�Ṧǰ������
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")

-------------------------------------------------------------------------------

STake_Start("SG_QG_move_015_up","SG_QG_move_015(0,22)(23,(40,60))")  --�Ṧ������
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_01") 

    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")  

STake_Start("SG_QG_move_015_loop","SG_QG_move_015(0,22)(23,(40,60))")  --�Ṧ��������
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)
    GroundSound(1.728,"Sound_fall")

    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")

--------------------------------------------------------------------------------

STake_Start("SG_QG_move_016_up","SG_QG_move_016(0,22)(23,(40,60))")  --�Ṧ������
    BlendTime(0.1)
    BlendMode(0)
    GroundSound(0.000,"Sound_jump")
    Sound(0.000,"char_05_cloth_flap_01")
       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_01") 

    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")

STake_Start("SG_QG_move_016_loop","SG_QG_move_016(0,22)(23,(40,60))")  --�Ṧ��������
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)
    GroundSound(1.728,"Sound_fall")

    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 
    Sound(0.900,"char_05_cloth_flap_02")

-------------------------------------------------------------------------------

STake_Start("SG_QG_move_014_up_2","SG_QG_move_014(0,22)(23,(40,60))_2")  --�Ṧǰ����2��
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_02")
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_014_loop_2","SG_QG_move_014(0,22)(23,(40,60))_2")  --�Ṧǰ������2��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")

-----------------------------------------------------------------------------


STake_Start("SG_QG_move_015_up_2","SG_QG_move_015(0,22)(23,(40,60))_2")  --�Ṧ������2��
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_02")
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_015_loop_2","SG_QG_move_015(0,22)(23,(40,60))_2")  --�Ṧ��������2��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")
    

-----------------------------------------------------------------------------    
    

STake_Start("SG_QG_move_016_up_2","SG_QG_move_016(0,22)(23,(40,60))_2")  --�Ṧ������2��
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_02")
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_016_loop_2","SG_QG_move_016(0,22)(23,(40,60))_2")  --�Ṧ��������2��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")





-----------------------------------------------------------------------------


STake_Start("SG_QG_move_014_up_3","SG_QG_move_014(0,22)(23,(40,60))_3")  --�Ṧǰ����3��
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_03") 
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_014_loop_3","SG_QG_move_014(0,22)(23,(40,60))_3")  --�Ṧǰ������3��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")



-----------------------------------------------------------------------------


STake_Start("SG_QG_move_015_up_3","SG_QG_move_015(0,22)(23,(40,60))_3")  --�Ṧ������3��
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_03") 
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_015_loop_3","SG_QG_move_015(0,22)(23,(40,60))_3")  --�Ṧ��������3��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")

------------------------------------------------------------------------------


STake_Start("SG_QG_move_016_up_3","SG_QG_move_016(0,22)(23,(40,60))_3")  --�Ṧ������3��
    BlendTime(0.1)
    BlendMode(0)

       
    FrameTime(0,0.733)
    
    Fx(0,"FX_move_QG_03") 
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    GroundSound(0.140,"Sound_jump")
    Sound(0.140,"char_05_cloth_flap_01")    

STake_Start("SG_QG_move_016_loop_3","SG_QG_move_016(0,22)(23,(40,60))_3")  --�Ṧ��������3��
    BlendTime(0.2)
    BlendMode(0)
    FrameTime(0.766,2.0) 
    Loop(1.333,2.000)


    EffectStart(0.766, "FOV", 15)
    EffectStart(0.766, "MB",0)
    --EffectStart(0, "DOF", 200, 200, 3000, 3, 3) 

    Sound(0.900,"char_05_cloth_flap_02")



-------------------------------------------------------------------------------

STake_Start("SG_Swim_wait_000","SG_Swim_wait_000")  --��Ӿ
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    BlendPattern("wait") 
    
    Sound(0.090,"YouYong_yuandi_001")   

STake_Start("SG_Swim_move_004","SG_Swim_move_004")  --ǰ��
    BlendTime(0.2)
    BlendMode(1) 
    Loop()

    BlendPattern("Arrow_jump")         

    --Fx(0.582, "Fx_ground_water_move", "RightHand", 1, 0, 0, 0, 0, 45, 0)
    --Fx(0.582, "Fx_ground_water_move", "LeftHand", 1, 0, 0, 0, 0, -45, 0)

    Sound(0.090,"YouYong_yuandi_001")
    Sound(0.797,"YouYong_001")
    Sound(1.655,"YouYong_yuandi_001")
    Sound(2.207,"YouYong_001")



STake_Start("SG_Swim_move_001","SG_Swim_move_001")  --����
    BlendTime(0.2)
    BlendMode(1) 
    Loop()

    BlendPattern("Arrow_jump")         

    Sound(0.010,"YouYong_001")
    Sound(0.974,"YouYong_001")

STake_Start("SG_Swim_move_005","SG_Swim_move_005")  --����
    BlendTime(0.2)
    BlendMode(1) 
    Loop()

    BlendPattern("Arrow_jump")         

    Sound(0.010,"YouYong_001")
    Sound(0.880,"YouYong_001")

STake_Start("SG_Swim_move_006","SG_Swim_move_006")  --����
    BlendTime(0.2)
    BlendMode(1) 
    Loop()

    BlendPattern("Arrow_jump")         

    Sound(0.010,"YouYong_001")
    Sound(0.880,"YouYong_001")

------------------------------------------------------------------------------

STake_Start("SG_WS_move_004","SG_WS_move_004")  --ǰˮ��Ʈ
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    
    Fx(0,"FX_move_QG_common") 
    Fx(0.7,"FX_move_QG_common") 
    Fx(1.4,"FX_move_QG_common") 
    Fx(2.1,"FX_move_QG_common") 
    Fx(2.8,"FX_move_QG_common") 
    Fx(3.5,"FX_move_QG_common")
    Fx(4.2,"FX_move_QG_common")

    Fx(0.479, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(1.013, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(1.546, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(2.079, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(2.613, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(3.306, "Fx_ground_water_hit", "Reference", 0.7, 1, 0, 0, 0, 0, 0)
    Fx(4.746, "Fx_ground_water_hit", "Reference", 0.7, 1, 0, 0, 0, 0, 0)
    Fx(5.279, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    
                
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)   

    Sound(0.010,"Step_water_DaoDi")
    Sound(0.550,"Step_water_DaoDi")
    Sound(1.150,"Step_water_DaoDi")    
    Sound(1.700,"Step_water_DaoDi")
    Sound(2.400,"Step_water_DaoDi")
    Sound(2.350,"YouYong_jump_001")
    Sound(2.350,"char_05_cloth_flap_02")
    Sound(3.100,"char_05_cloth_flap_02")
    Sound(4.200,"Step_water_DaoDi")
    
STake_Start("SG_WS_move_004_start","SG_WS_move_004_start")  --ǰˮ��Ʈ��
    BlendTime(0.0)
    BlendMode(0) 
   

    Fx(0.078, "Fx_ground_water_hit", "Reference", 1)

    Sound(0.050,"YouYong_jump_001")
    Sound(0.100,"char_05_cloth_flap_02")

STake_Start("SG_WS_move_004_end","SG_WS_move_004_end")  --ǰˮ��Ʈ��ˮ
    BlendTime(0.2)
    BlendMode(0) 
  
    
    Fx(0.10, "Fx_ground_water_hit", "Reference", 1, 0, 0, 0, 0, 0, 0)

    Sound(0.010,"YouYong_luoshui_001")

-------------------------------------------------------------------------------

STake_Start("SG_WS_move_005","SG_WS_move_005")  --��ˮ��Ʈ
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    
    Fx(0,"FX_move_QG_common") 
    Fx(0.7,"FX_move_QG_common") 
    Fx(1.4,"FX_move_QG_common") 
    Fx(2.1,"FX_move_QG_common") 
    Fx(2.8,"FX_move_QG_common") 
    Fx(3.5,"FX_move_QG_common")
    Fx(4.2,"FX_move_QG_common")

    Fx(0.479, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(1.013, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(1.546, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(2.079, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(2.613, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(3.306, "Fx_ground_water_hit", "Reference", 0.7, 1, 0, 0, 0, 0, 0)
    Fx(4.746, "Fx_ground_water_hit", "Reference", 0.7, 1, 0, 0, 0, 0, 0)
    Fx(5.279, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    
                
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)   
    
    Sound(0.010,"Step_water_DaoDi")
    Sound(0.550,"Step_water_DaoDi")
    Sound(1.150,"Step_water_DaoDi")    
    Sound(1.700,"Step_water_DaoDi")
    Sound(2.400,"Step_water_DaoDi")
    Sound(2.350,"YouYong_jump_001")
    Sound(2.350,"char_05_cloth_flap_02")
    Sound(3.100,"char_05_cloth_flap_02")
    Sound(4.200,"Step_water_DaoDi")

STake_Start("SG_WS_move_005_start","SG_WS_move_005_start")  --��ˮ��Ʈ��
    BlendTime(0.0)
    BlendMode(0) 
   

    Fx(0.078, "Fx_ground_water_hit", "Reference", 1)

    Sound(0.050,"YouYong_jump_001")
    Sound(0.100,"char_05_cloth_flap_02")

STake_Start("SG_WS_move_005_end","SG_WS_move_005_end")  --��ˮ��Ʈ��ˮ
    BlendTime(0.2)
    BlendMode(0) 
  
    
    Fx(0.10, "Fx_ground_water_hit", "Reference", 1, 0, 0, 0, 0, 0, 0)


    Sound(0.010,"YouYong_luoshui_001")

----------------------------------------------------------------------------------


STake_Start("SG_WS_move_006","SG_WS_move_006")  --��ˮ��Ʈ
    BlendTime(0.2)
    BlendMode(1) 
    Loop()
    
    Fx(0,"FX_move_QG_common") 
    Fx(0.7,"FX_move_QG_common") 
    Fx(1.4,"FX_move_QG_common") 
    Fx(2.1,"FX_move_QG_common") 
    Fx(2.8,"FX_move_QG_common") 
    Fx(3.5,"FX_move_QG_common")
    Fx(4.2,"FX_move_QG_common")

    Fx(0.479, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(1.013, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(1.546, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(2.079, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(2.613, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    Fx(3.306, "Fx_ground_water_hit", "Reference", 0.7, 1, 0, 0, 0, 0, 0)
    Fx(4.746, "Fx_ground_water_hit", "Reference", 0.7, 1, 0, 0, 0, 0, 0)
    Fx(5.279, "Fx_ground_water_move", "Reference", 1, 1, 0, 0, 0, 0, 0)
    
                
    
    EffectStart(0, "FOV", 15)
    EffectStart(0, "MB",0)   
    
    Sound(0.010,"Step_water_DaoDi")
    Sound(0.550,"Step_water_DaoDi")
    Sound(1.150,"Step_water_DaoDi")    
    Sound(1.700,"Step_water_DaoDi")
    Sound(2.400,"Step_water_DaoDi")
    Sound(2.350,"YouYong_jump_001")
    Sound(2.350,"char_05_cloth_flap_02")
    Sound(3.100,"char_05_cloth_flap_02")
    Sound(4.200,"Step_water_DaoDi")

STake_Start("SG_WS_move_006_start","SG_WS_move_006_start")  --��ˮ��Ʈ��
    BlendTime(0.0)
    BlendMode(0) 
   

    Fx(0.078, "Fx_ground_water_hit", "Reference", 1)

    Sound(0.050,"YouYong_jump_001")
    Sound(0.100,"char_05_cloth_flap_02")

STake_Start("SG_WS_move_006_end","SG_WS_move_006_end")  --��ˮ��Ʈ��ˮ
    BlendTime(0.2)
    BlendMode(0) 
  
    
    Fx(0.10, "Fx_ground_water_hit", "Reference", 1, 0, 0, 0, 0, 0, 0)

    Sound(0.010,"YouYong_luoshui_001")


--------СԴ����ּ���----

STake_Start("Alone_in_the_dark","SG_SL_ST_skill_019")     --���ִ���ֱ���~
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady")  
    Sound(0.21,"QZ_PA_Skill_007_2")
    Sound(0.02,"QZ_PA_Skill_004_2")
    Sound(0.02,"SL_Nv_XuliVoc_2_1")
 

  	Fx(0,"HG_SL_ST_shibatongren_fire")
	CutScene(0,"path")

    AttackStart()	
    Hurt(0.25,"hurt_81",0,4)
	DirFx(0.25,"HG_SL_ST_skill_018_hit",25,0.8) 
    HurtSound(0.25,"BTS_CR_HIT_new_s1")


STake_Start("SG_wait_sleep","SG_wait_sleep")  --˯������
    BlendTime(0.2)
    BlendMode(0) 
    Loop()
	
STake_Start("SG_wait_sleepup","SG_wait_sleepup")  --��������
    BlendTime(0.2)
    BlendMode(0) 
	
	

STake_Start("SG_life_emo015","SG_life_emo015")   --��Ϣ 10602
    BlendMode(0)
    BlendTime(0.0)
    Priority(0,"5")     
    BlendPattern("steady")
	Sound(0,"Life_Recover_Loop_1")
	Fx(0,"FX_Go_Home_loop","Reference",1,1,0,0,0,0,0,0,1,3)
	--Fx(0,"HG_BTS_CR_skill_001_loop01","Reference",1,1,0,0,0,0,0,0,1,3)
	Loop()


STake_Start("SG_tk_001","SG_tk_001")         --���	16204
    BlendMode(0)
    BlendTime(0)
	HideWeapon(0)
    BlendPattern("steady") 
    Priority(0,"5") 	
	FrameTime(0.3,0.833)
    Fx(0.301,"HG_tk_001","Reference",1,0,0,0,-10,0,0,0,1,1)
	Sound(0,"")	


	CameraShake(0.301,"ShakeMode = 3","ShakeTimes = 0.08","ShakeFrequency = 0.033","ShakeAmplitudeX = 10","ShakeAmplitudeY = 10","ShakeAmplitudeZ = 10")


---------------------------------------------------------------------------------------------------------------------------------------
--CutScene��

--������������

STake_Start("SG_move_014(0,25)(26,(50,90))(92,112)_2","SG_move_014(0,25)(26,(50,90))(92,112)_2") 
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_around_SG","SG_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_stand_SG","SG_wait_000")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_run_SG","SG_move_004")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_rush_start_SG","SG_QG_move_004_start")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_rush_SG","SG_QG_move_004")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_rush_end_SG","SG_QG_move_004_end")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

--����ų�

STake_Start("CS_look_left_SG","SG_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1,1.7)

STake_Start("CS_look_left_stop_SG","SG_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.7,1.7001)
    Loop()

--�Խ���

STake_Start("CS_wait1_SG","SG_wait_000")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait2_SG","N2_CF01_emo_001")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait3_SG","N2_QZZJDZ01_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_handed_fast_SG","SG_CS_001")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_handed_slow_SG","SG_CS_002")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_land1_SG","SG_CS_004")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop(1.5329,1.533)

STake_Start("CS_afraid1_SG","SG_CS_003")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,1.5667)
    Loop(0.8333,1.5667)

STake_Start("CS_DianBo_SG","SG_CS_emo_003")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

------------------------------------------------------------------------------------
--СԴ��CUTSCENE��������


STake_Start("CS_hit1_SG","SG_CS_hit_001(110-140)")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,4.666)
    Loop(3.6667,4.666)

STake_Start("CS_saved1_SG","SG_SG_C_link_001_new")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait5_SG","N2_BTSDJDZ_wait_000")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_around1_SG","SG_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_around2_SG","N2_CF01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,4,1.2)

STake_Start("CS_look_around3_SG","N2_THDDJNDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_look_around4_SG","N2_THDZJNDZ01_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_BianShen1_SG","SG_CS_bout_001")  
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"SG_CS_bout_001")
    Fx(5,"FX_Cutscene_mohuacibang")
    Loop(5.829,5.83)

STake_Start("CS_BianShen1_S_SG","SG_CS_bout_001")  
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"FX_Cutscene_mohuacibang")
    FrameTime(5.829,5.83)
    Loop(0,1)

STake_Start("CS_back_SG","SG_CS_back_001")
    BlendMode(0)
    BlendTime(0.01) 
    Loop()

STake_Start("CS_back_loop_SG","SG_CS_back_001_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZhongDu_SG","SG_CS_bout_001_start")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_ZhongDu_loop_SG","SG_CS_bout_001_start_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_XiaoYuan_SG","SG_CS_emo_xr_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_TouYun1_SG","SG_CS_emo_zd_001")
    BlendMode(0)
    BlendTime(0.2) 
    Fx(0,"Fx_Cutscene_mohuacibang_yan_start")
    Fx(0.5,"Fx_Cutscene_mohuacibang_yan_loop") 
    Fx(2.5,"Fx_Cutscene_mohuacibang_yan_loop") 
    Loop()

STake_Start("CS_TouYun1_loop_SG","SG_CS_emo_zd_001_loop")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"Fx_Cutscene_mohuacibang_yan_loop") 
    Loop()

STake_Start("CS_TaiTou_SG","SG_CS_emo_zd_002")
    BlendMode(0)
    BlendTime(0.5) 
    Fx(0,"Fx_Cutscene_mohuacibang_yan_loop") 
    Loop()

STake_Start("CS_attack1_SG","SG_PA_attack_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk1_fight_SG","SG_PA_move_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_wait1_fight_SG","SG_CS_mwait_001_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_turn_right_fight_SG","SG_PA_move_009")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_turn_left_fight_SG","SG_PA_move_010")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


-------------------------------------------------------------------------------
--СԴ��CUTSCENEФ�Ʒ���


STake_Start("CS_pao_SG","SG_QG_move_004")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_tiao_SG","SG_QG_move_014(0,22)(23,(40,60))_2")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_gun_SG","SG_move_014(0,25)(26,(50,90))(92,112)_2")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.04,3.7)
    Loop()

STake_Start("CS_jieshu_SG","SG_QG_move_004_end")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_kan_SG","SG_emo_002")  
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

-------------------------------------------------------------------------------
--�ɹŲ�ԭCUTSCENE�ö���

--CS1����Ϯ��

--����͢��


STake_Start("CS_fly_SG","SG_CS_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_kan1_SG","SG_emo_002")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.8,2.2) 
    Loop()

STake_Start("CS_Def_SG","SG_CS_emo_dang_001")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.85,1.518)
    Loop(1.5179,1.518)

--CS2�ڱ�����

--����͢��

STake_Start("CS_Ridehorse_emo004_SG","SG_CS_RideHorse_emo_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_hurt&breath_loop_SG","SG_CS_hurt&breath_loop")
    BlendMode(0)
    BlendTime(0) 
    Loop()


STake_Start("CS_Ridehorse_emo004_end_SG","SG_CS_RideHorse_emo_004")  ---�ܾ����ζ�������˦������
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(2.8,4.66) 
    Loop()

STake_Start("CS_Ridehorse_emo004_up_SG","SG_CS_RideHorse_emo_004")  
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.8,2.8) 

STake_Start("CS_Ridehorse_move001_SG","RideHorse_SG_move_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

--CS3��׽�˺�

--��������

STake_Start("CS_ZhuoBaHai_SG","SG_CS_beibang")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk_SG","SG_CS_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    PlaySpeed(0,1,1.25)
    Loop()

STake_Start("CS_walk_slow_SG","SG_CS_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_walk1_SG","N2_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    PlaySpeed(0,1,1.35)


--CS4��ʲ��

--����͢��

STake_Start("CS_def_sand_SG","N2_life_009")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.001,0.32) 
    Loop(0.31999,0.32)

STake_Start("CS_def_sand2_SG","CS_N2_Throw_def_000")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.12654) 
    Loop(0.1245,0.12654)


--CS5���

--��������

STake_Start("CS_JieBai1_start_SG","SG_life_024(20,60)")  --���
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,0.666)

STake_Start("CS_JieBai1_loop_SG","SG_life_024(20,60)")  --���
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0.667,2)
    Loop(0.667,2)

STake_Start("CS_JieBai1_end_SG","SG_life_024(20,60)")  --���
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(2,3.2)

STake_Start("CS_RideHorse_run_SG","RideHorse_SG_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_RideHorse_wait_SG","RideHorse_SG_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_RideHorse_look_SG","RideHorse_SG_move_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_RideHorse_stop_SG","RideHorse_SG_brake_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_RideHorse_ShangMa_SG","RideHorse_SG_emo_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_DunXia_SG","SG_life_011")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
	
	

---------------------------------------------
--����CUTSCENE�ö���

--CS3��׽��ľ��

--����͢��

STake_Start("CS_PA_attack_000_SG","SG_PA_attack_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_PA_attack_001_SG","SG_PA_attack_001")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_PA_attack_002_SG","SG_PA_attack_002")
    BlendMode(0)
    BlendTime(0) 
    Loop()


STake_Start("CS_PA_def_001_SG","SG_PA_def_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()



STake_Start("CS_LA_attack_000_SG","NB_LA_attack_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_LA_attack_001_SG","NB_LA_attack_001")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_LA_attack_002_SG","NB_LA_attack_002")
    BlendMode(0)
    BlendTime(0) 
    Loop()
STake_Start("CS_PA_dodge_000_SG","SG_PA_dodge_000")
    BlendMode(0)
    BlendTime(0) 
    Loop()

STake_Start("CS_PA_hit_000_SG","SG_PA_hit")
    BlendMode(0)
    BlendTime(0) 
    Loop()


STake_Start("CS_hit_003_SG","SG_hit_003")
    BlendMode(0)
    BlendTime(0) 
    Loop()

-----------------------------------------------------------------------------------

--�Խ���CUTSCENE�ö���

--CS1��������

--������

STake_Start("CS_SJY_talk_01_wljs_SG","SG_CS_CS_talk_wljs")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,6.435) 
    Loop(6.4349,6.435)

STake_Start("CS_SJY_talk_02_wljs_SG","SG_CS_CS_talk_wljs")
    BlendMode(0)
    BlendTime(0)
    FrameTime(6.435,9.8)
    PlaySpeed(6.435,9.8,0.6) 
    Loop(9.79999,9.8)

STake_Start("CS_SJY_talk_wljs2_SG","SG_CS_CS_talk_wljs2")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_wait_wljs_SG","SG_CS_CS_wait_wljs")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_talk_wljs_loop_SG","SG_CS_CS_talk_wljs_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_SJY_talk_wljs2_loop_SG","SG_CS_CS_talk_wljs2_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



------------------------------------
--ţ�Ҵ忪��
--����͢��


STake_Start("CS_NJC_walk_slow_SG","SG_CS_move_000")
    BlendMode(0)
    BlendTime(0.1) 
    PlaySpeed(0,1.066,0.6)
    Loop()


-------------------------------------
--�ں�����
--����͢��

STake_Start("CS_phct_emo_001_SG","SG_CS_phct_emo_001")
    BlendMode(0)
    BlendTime(0) 
    Loop()



STake_Start("CS_phct_emo_002_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0) 
    FrameTime(0.3,3.866)
    PlaySpeed(0,3,0.5)
    Loop()

STake_Start("CS_phct_emo_003_SG","SG_CS_phct_emo_003")
    BlendMode(0)
    BlendTime(0) 
    Loop()


STake_Start("CS_phct_chuanxi_003_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0)
    FrameTime(1.89434,3.866) 
    Loop()


STake_Start("CS_phct_gun_start_003_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0.3) 
    FrameTime(0,0.50)
    PlaySpeed(0,0.50,0.5)
    Loop(0.4999,0.50)

STake_Start("CS_phct_gun_end_003_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0) 
    FrameTime(0.51,0.75)
    PlaySpeed(0.51,0.75,0.2)
    Loop(0.7499,0.75)


STake_Start("CS_phct_gun_end1_003_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0) 
    FrameTime(0.60,3)
    PlaySpeed(0.60,3,0.5)
    Loop(2.999,3)


STake_Start("CS_phct_emo_003pose_SG","SG_CS_phct_emo_003")
    BlendMode(0)
    BlendTime(0) 
    Loop(0,0.001)



STake_Start("CS_phct_chuanxi_loop_SG","SG_CS_bs_bs_loop")
    BlendMode(0)
    BlendTime(0.4) 
    Loop()


STake_Start("CS_phct_hit_SG","SG_hit_001")
    BlendMode(0)
    BlendTime(0.3) 
    Loop(0.4399,0.44)

STake_Start("CS_look_around1_slow_SG","SG_emo_001") 
    BlendMode(0)
    BlendTime(0.2) 
    PlaySpeed(0,4,0.5)
    Loop()



-----------------------------------------------------------------------------------
--ţ�Ҵ�CUTSCENE�ö���

--CS6��ұ���

--��������

STake_Start("CS_NJC_BianShen3_SG","SG_CS_bs_bs")  
    BlendMode(0)
    BlendTime(0.2)
    Fx(2.6,"FX_Cutscene_mohuacibang")     
    Fx(7.333,"FX_Cutscene_mohuacibang_loop") 
    Fx(9.333,"FX_Cutscene_mohuacibang_loop") 
    Loop()

STake_Start("CS_NJC_Dun_SG","SG_CS_bs_bs_loop")  
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_mohuacibang_loop") 
    Loop()

STake_Start("CS_NJC_BianShen3_S_SG","SG_CS_bs_bs")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.0001)
    Loop()

STake_Start("CS_NJC_hit1_SG","SG_hit_005")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_hit2_SG","SG_hit_007")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_attack3_SG","SG_CS_GB_PA_attack_A_003")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_CunZhang_SG","SG_CS_emo_xr_001")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,1) 
    Loop(0.99999,1)

STake_Start("CS_NJC_YouZhuan_SG","SG_PA_move_010")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()


--CS7��ҩʦ˧

--��������

STake_Start("CS_NJC_JiuHuangRong_SG","SG_CS_mwait_001")  
    BlendMode(0)
    BlendTime(0.2)
    Loop()

STake_Start("CS_NJC_JiuHuangRong_S_SG","SG_CS_mwait_001")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.0001) 
    Loop()

STake_Start("CS_NJC_JiuHuangRong_run_SG","SG_CS_mwait_001")  
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(2.41,5.133)
    Loop()

STake_Start("CS_NJC_wait1_fight_SG","SG_CS_mwait_001_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()



-------------------------------------------------------------------------------
--ţ�Ҵ�CUTSCENE�ö���

--�����Ϯ

--������

STake_Start("CS_NJC_jblx_emo1_SG","SG_CS_jblx_emo1")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_jblx_emo1_a_SG","SG_CS_jblx_emo1")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(0,1.3) 
    PlaySpeed(1.105,1.3,0.2)   
    Loop(1.2999,1.3)

STake_Start("CS_NJC_jblx_emo1_b_SG","SG_CS_jblx_emo1")
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(1.3,6.435)  
    PlaySpeed(1.3,1.973,0.5)
    PlaySpeed(1.973,2.795,0.4)
    Fx(5.73,"FX_Cutscene_mohuacibang_start1")   
    Loop(6.4349,6.435)

STake_Start("CS_NJC_jblx_emo1_loop_SG","SG_CS_jblx_emo1_loop")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_mohuacibang_loop1")   
    Loop(0)

STake_Start("CS_NJC_jblx_emo2_SG","SG_CS_jblx_emo2")
    BlendMode(0)
    BlendTime(0.2)
    Fx(0,"FX_Cutscene_mohuacibang_end1") 
    Loop()

STake_Start("CS_NJC_jblx_emo2_02_SG","SG_CS_jblx_emo2")
    BlendMode(0)
    BlendTime(0.4)
    FrameTime(0.87,1.733) 
    PlaySpeed(1.3864,1.733,0.5)  
    Loop(1.7329,1.733)

STake_Start("CS_NJC_004_SG","SG_CS_004")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.55,1.20)  
    Loop(1.1999,1.2)

-------------------------------------------------------------------------------
--ţ�Ҵ�CUTSCENE�ö���

--β��

--������

STake_Start("CS_NJC_GB_PA_skill_020_SG","SG_GB_PA_skill_020")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_PA_skill_move_att3_SG","SG_GB_PA_skill_move_att3")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_GB_PA_attack_A_001_SG","SG_GB_PA_attack_A_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_GB_PA_attack_A_002_SG","SG_GB_PA_attack_A_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_GB_PA_attack_A_003_SG","SG_GB_PA_attack_A_003")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_GB_PA_skill_move_att2_SG","SG_GB_PA_skill_move_att2")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_GB_PA_skill_move_att3_SG","SG_GB_PA_skill_move_att3")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_PA_attack_002_SG","SG_PA_attack_002")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_PA_def_000_SG","SG_PA_def_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_PA_dodge_000_SG","SG_PA_dodge_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_PA_hit_SG","SG_PA_hit")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_PA_wait_000_SG","SG_PA_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_QG_move_004_SG","SG_QG_move_004")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_dead_001_SG","SG_dead_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_hit_000_SG","SG_hit_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_daodi_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,1.684)
    PlaySpeed(1.57,1.684,0.2)  
    Loop(1.6839,1.684)

STake_Start("CS_NJC_daodi_loop_SG","SG_CS_phct_emo_002")
    BlendMode(0)
    BlendTime(0.4)
    FrameTime(1.8126,3.8273)
    PlaySpeed(1.8126,3.8273,0.7) 
    Loop(3.8272,3.8273)

STake_Start("CS_NJC_shenyin_SG","SG_CS_WS_8_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_WS_1_SG","SG_CS_WS_1")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,4.8) 
    Loop(4.7999,4.8)

STake_Start("CS_NJC_WS_2_SG","SG_CS_WS_2")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("CS_NJC_WS_4_SG","SG_CS_WS_4")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,1.733)  
    Loop(1.7329,1.733)

STake_Start("CS_NJC_WS_6_a_SG","SG_CS_WS_6")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.0001)  

STake_Start("CS_NJC_WS_6_SG","SG_CS_WS_6")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,2.433)  
    Loop(2.4329,2.433)

STake_Start("CS_NJC_WS_8_SG","SG_CS_WS_8")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,5.643)  
    Loop(5.6429,5.643)

STake_Start("CS_NJC_WS_9_SG","SG_CS_WS_9")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,2.4) 
    Loop(2.3999,2.4)

STake_Start("CS_NJC_skill_move_SG","SG_GB_PA_skill_move")
    BlendMode(0)
    BlendTime(0.2)
    PlaySpeed(0,0.4613,0.5)  
    Loop(0.4612,0.4613)

STake_Start("CS_NJC_daodi_emo_SG","SG_CS_phct_emo_002_2")
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,1.8)  
    Loop(1.7999,1.8)

STake_Start("CS_NJC_daodi_emo_end_SG","SG_CS_phct_emo_002_end")
    BlendMode(0)
    BlendTime(0)
    FrameTime(0,3.9) 
    Loop(3.8999,3.9)

STake_Start("CS_NJC_daodi_emo_loop_SG","SG_CS_phct_emo_002_loop")
    BlendMode(0)
    BlendTime(0.2) 
    Loop(0)


--����˿����CUTSCENE�ö���     

--���X�� 

STake_Start("CS_JYH_SG_GB_emo_001","CS_JYH_SG_GB_emo_001")             
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.284) 
    Loop(0.2839,0.284)

STake_Start("CS_JYH_SG_GB_emo_004","CS_JYH_SG_GB_emo_001")          
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0.284,3.166) 
    --Loop(1.1079,1.108)

STake_Start("CS_JYH_SG_GB_emo_002","CS_JYH_SG_GB_emo_004")     
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("CS_JYH_SG_GB_emo_003","CS_JYH_SG_GB_emo_003")      
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("CS_JYH_SG_GB_stand","CS_JYH_SG_GB_emo_003")               
    BlendMode(0)
    BlendTime(0.2)
    FrameTime(0,0.001)
    Loop(0.0009,0.001)

STake_Start("CS_JYH_SG_GB_look","CS_JYH_SG_GB_emo_002")               
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("CS_JYH_SG_GB_jump_back2","CS_JYH_SG_GB_emo_005")        
    BlendMode(0)
    BlendTime(0.3) 
    Loop(1.4999,1.5)

STake_Start("CS_JYH_SG_GB_wait_001","CS_JYH_SG_GB_wait_001")               
    BlendMode(0)
    BlendTime(0.2) 
	
-----------------------------------------------------------------------------------
STake_Start("SG_chuxin_skill_gs","SG_wait_000")
    BlendMode(0)
    BlendTime(0.03)
    Priority(0,"5")     
    BlendPattern("steady") 
	Charge(0.3,0.1,15,0,21,0,0) 
	
	
---------------------slzd-----------------------------------
STake_Start("SG_chuxinzhe_skill_001","SG_PA_skill_001")         --������ŭ    16000
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"FX_ZC_Buff_CXZ_Anger",1,1,0,0,0,0,0,0,2.1)
	Sound(0.01,"HG_GB_PA_Skill_000_Combo1")

STake_Start("SG_yongwei_skill_002","SG_PA_skill_002")         --�������ȷ���    16105
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0,"FX_ZC_Buff_YW_ZRFH")
	Sound(0.01,"BTS_FAN_Skill_004")

STake_Start("SG_yongwei_skill_004","SG_PA_skill_002")         --�������ѻ���     16115
    BlendMode(0)
    BlendTime(0)
    BlendPattern("steady") 
    Priority(0,"5") 
    Fx(0.26,"FX_ZC_Buff_YE_BLHD_bao",1,1,0,0,0,0,0,0,1)
	Sound(0.01,"QZ_SW_QXQJ_001")	
