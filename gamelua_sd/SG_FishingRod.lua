

STake_Start("SG_wait_000_FishingRod","SG_wait_000_FishingRod")   --一般待机
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")  
    
STake_Start("SG_wait_000_FishingRod_ui","SG_wait_000_FishingRod")   --一般待机
    BlendMode(0)
    BlendTime(0)
    Loop()   

STake_Start("SG_FishingRod_wait_000","SG_FishingRod_wait_000")   --战斗待机
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("wait") 
    Loop()
 
STake_Start("SG_FishingRod_wait_000_ui","SG_FishingRod_wait_000")  --战斗待机
    BlendMode(0)
    BlendTime(0)
    Loop()

STake_Start("SG_emo_000_FishingRod","SG_emo_000_FishingRod")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("SG_emo_001_FishingRod","SG_emo_001_FishingRod")
    BlendMode(0)
    BlendTime(0.2)


STake_Start("SG_FishingRod_link_000","SG_FishingRod_link_000")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.324, "Fish_WOMAN_2")

STake_Start("SG_FishingRod_link_001","SG_FishingRod_link_001")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.652, "Fish_WOMAN_1")
    
 
STake_Start("SG_FishingRod_move_001","SG_FishingRod_move_001")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.381,"Sound_step")
  Loop()


    --GroundFx(0.510, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.095, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.510, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.095, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
 

STake_Start("SG_FishingRod_move_003","SG_FishingRod_move_003")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.381,"Sound_step")
  Loop()


    --GroundFx(0.510, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.095, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.510, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.095, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
 
 
  
STake_Start("SG_FishingRod_move_004","SG_FishingRod_move_004")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.410,"Sound_step")
  Loop()


    GroundFx(0.065, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.065, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    



STake_Start("SG_FishingRod_move_005","SG_FishingRod_move_005")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.410,"Sound_step")
  Loop()


    GroundFx(0.065, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.065, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    


STake_Start("SG_FishingRod_move_006","SG_FishingRod_move_006")
    BlendTime(0.2)
    BlendMode(1) 
    GroundSound(0.000,"Sound_step")
    GroundSound(0.410,"Sound_step")
  Loop()


    GroundFx(0.065, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.065, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    




STake_Start("SG_FishingRod_move_009","SG_FishingRod_move_009")  --战斗踏转
    BlendTime(0.2)
    BlendMode(1) 

  Loop()


STake_Start("SG_FishingRod_move_010","SG_FishingRod_move_010")  --战斗踏转
    BlendTime(0.2)
    BlendMode(1) 

  Loop()
  
  
  
STake_Start("SG_move_012_FishingRod","SG_move_012_FishingRod")  --一般踏转
    BlendTime(0.2)
    BlendMode(1) 

  Loop()


STake_Start("SG_move_013_FishingRod","SG_move_013_FishingRod")  --一般踏转
    BlendTime(0.2)
    BlendMode(1) 

  Loop()
   
  

STake_Start("SG_FishingRod_dead","SG_FishingRod_dead")
    BlendMode(0)
    BlendTime(0.2)
    
    GroundSound(0.860,"Sound_fall") 
    GroundSound(1.380,"Sound_daodi")

STake_Start("SG_FishingRod_hit","SG_FishingRod_hit")
    BlendMode(0)
    BlendTime(0.05)
    Sound(0.059,"voc_derais_injure_01_short_01")
    BlendPattern("Upper_hurt")    


STake_Start("SG_FishingRod_dodge_000","SG_FishingRod_dodge_000")
    BlendMode(0)
    BlendTime(0.05)

    Fx(0.015,"HG_THD_SW_dodge_001")   

    GroundSound(0.279,"Sound_fall")
    GroundSound(0.625,"Sound_step")

STake_Start("SG_FishingRod_def_000","SG_FishingRod_def_000")
    BlendMode(0)
    BlendTime(0.05)
    Sound(0.00,"weap_Sword_block")
    Fx(0.015,"Fx_gedang_wuqi")   


STake_Start("SG_FishingRod_attack_000","SG_FishingRod_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.418)  
    Soft(0.418,1.1)
       
    AttackStart()

  	Hurt(0.26,"hurt_11")
        
        Sound(0.165,"QZ_PA_Attack_11")
        Sound(0.165,"QZ_SG_AttackVOC_1_1")
        HurtSound(0.76,"QZ_PA_Hit_11")    

        Fx(0.000,"HG_QZ_PA_attack_000")
    DragFx(0.26, 0.5, "HG_QZ_PA_attack_tracker", "HG_QZ_PA_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)
  	  	

STake_Start("SG_FishingRod_attack_000_1","SG_FishingRod_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.405)  
    Soft(0.405,0.9)
 	        
  	AttackStart()

  	Hurt(0.29,"hurt_11")

        Sound(0.216,"QZ_PA_Attack_11")
        Sound(0.216,"QZ_SG_AttackVOC_1_1")
        HurtSound(0.79,"QZ_PA_Hit_11")

        Fx(0.0,"HG_QZ_PA_attack_001")
    DragFx(0.29, 0.5, "HG_QZ_PA_attack_tracker", "HG_QZ_PA_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  	
  	
  	
STake_Start("SG_FishingRod_attack_001","SG_FishingRod_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")  

    MustPlay(0.000,0.405)  
    Soft(0.405,0.9)
 	        
  	AttackStart()

  	Hurt(0.29,"hurt_11")

        Sound(0.216,"QZ_PA_Attack_11")
        Sound(0.216,"QZ_SG_AttackVOC_1_1")
        HurtSound(0.79,"QZ_PA_Hit_11")

        Fx(0.0,"HG_QZ_PA_attack_001")
    DragFx(0.29, 0.5, "HG_QZ_PA_attack_tracker", "HG_QZ_PA_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)  	
  	  	