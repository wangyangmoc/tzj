STake_Start("T1_wait_000","T1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_bout_001","T1_bout_001")  --出生跳入
    BlendMode(0)
    BlendTime(0)

STake_Start("T1_wait_001","T1_wait_001")  --镖师
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_wait_001_emo_001","T1_wait_001_emo_001")  --镖师生气表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_emo_002","T1_wait_001_emo_002")  --镖师表情拿刀出来擦擦
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_move_001","T1_wait_001_move_001")  --镖师走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("T1_TJ01_wait_000","T1_TJ01_wait_000")  --铁匠01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    

STake_Start("T1_TJ01_emo_000","T1_TJ01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.715,"T1_TJ01_emo_000_1")
    Sound(1.454,"T1_TJ01_emo_000_1")
    Sound(2.293,"T1_TJ01_emo_000_1")

STake_Start("T1_TJ01_emo_001","T1_TJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_wait_000","T1_LH01_wait_000")  --猎户01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_LH01_emo_000","T1_LH01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_emo_001","T1_LH01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_wait_000","T1_BK01_wait_000")  --捕快01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_BK01_emo_000","T1_BK01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_emo_001","T1_BK01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_wait_000","T1_MGDH_wait_000")  --蒙古大汗01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_emo_001","T1_MGDH_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_wait_000","N_T1_LieHu02_wait_000")  --猎户02
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_T1_LieHu02_emo_001","N_T1_LieHu02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_emo_002","N_T1_LieHu02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N_T1_LieHu02_emo_003","N_T1_LieHu02_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_ZDLBFZ_wait_000","T1_ZDLBFZ_wait_000")  --扎达兰百夫长
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_ZDLBFZ_emo_001","T1_ZDLBFZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_ZDLBFZ_emo_002","T1_ZDLBFZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
	
	
	
	
	
	
	
	
	
-----------------------------------------------------------------------------------

--塞外CUTSCENE用动作

--CS1大军冲

--单晓箫配

STake_Start("CS_SW_hurt1_T1_Blade","N1_HAJ01_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt2_T1_Blade","N1_HAJ01_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt3_T1_Blade","N1_MGSB_01_wait_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt4_T1_Blade","N1_life_024")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt5_T1_Blade","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()




STake_Start("CS_SW_WoDi1_T1_GL_B","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_escape1_T1_GL_B","N1_move_023")
    BlendMode(2)
    BlendTime(0.2)
    Loop()



-----------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_T1_wait_001_move_001","T1_wait_001_move_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_LH01_wait_000","T1_LH01_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()




--牛家村CUTSCENE用动作

--CS1开场动画

--俞章廷配

STake_Start("CS_move000_T1_Liehu","N_T1_LieHu02_walk")
    BlendMode(2)
    BlendTime(0.2)
    Loop()STake_Start("T1_wait_000","T1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_bout_001","T1_bout_001")  --出生跳入
    BlendMode(0)
    BlendTime(0)

STake_Start("T1_wait_001","T1_wait_001")  --镖师
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_wait_001_emo_001","T1_wait_001_emo_001")  --镖师生气表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_emo_002","T1_wait_001_emo_002")  --镖师表情拿刀出来擦擦
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_move_001","T1_wait_001_move_001")  --镖师走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("T1_TJ01_wait_000","T1_TJ01_wait_000")  --铁匠01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    

STake_Start("T1_TJ01_emo_000","T1_TJ01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.715,"T1_TJ01_emo_000_1")
    Sound(1.454,"T1_TJ01_emo_000_1")
    Sound(2.293,"T1_TJ01_emo_000_1")

STake_Start("T1_TJ01_emo_001","T1_TJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_wait_000","T1_LH01_wait_000")  --猎户01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_LH01_emo_000","T1_LH01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_emo_001","T1_LH01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_wait_000","T1_BK01_wait_000")  --捕快01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_BK01_emo_000","T1_BK01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_emo_001","T1_BK01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_wait_000","T1_MGDH_wait_000")  --蒙古大汗01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_emo_001","T1_MGDH_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_wait_000","N_T1_LieHu02_wait_000")  --猎户02
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_T1_LieHu02_emo_001","N_T1_LieHu02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_emo_002","N_T1_LieHu02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N_T1_LieHu02_emo_003","N_T1_LieHu02_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_ZDLBFZ_wait_000","T1_ZDLBFZ_wait_000")  --扎达兰百夫长
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_ZDLBFZ_emo_001","T1_ZDLBFZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_ZDLBFZ_emo_002","T1_ZDLBFZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
	
	
	
	
	
	
	
	
	
-----------------------------------------------------------------------------------

--塞外CUTSCENE用动作

--CS1大军冲

--单晓箫配

STake_Start("CS_SW_hurt1_T1_Blade","N1_HAJ01_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt2_T1_Blade","N1_HAJ01_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt3_T1_Blade","N1_MGSB_01_wait_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt4_T1_Blade","N1_life_024")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt5_T1_Blade","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()




STake_Start("CS_SW_WoDi1_T1_GL_B","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_escape1_T1_GL_B","N1_move_023")
    BlendMode(2)
    BlendTime(0.2)
    Loop()



-----------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_T1_wait_001_move_001","T1_wait_001_move_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_LH01_wait_000","T1_LH01_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()




--牛家村CUTSCENE用动作

--CS1开场动画

--俞章廷配

STake_Start("CS_move000_T1_Liehu","N_T1_LieHu02_walk")
    BlendMode(2)
    BlendTime(0.2)
    Loop()STake_Start("T1_wait_000","T1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_bout_001","T1_bout_001")  --出生跳入
    BlendMode(0)
    BlendTime(0)

STake_Start("T1_wait_001","T1_wait_001")  --镖师
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_wait_001_emo_001","T1_wait_001_emo_001")  --镖师生气表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_emo_002","T1_wait_001_emo_002")  --镖师表情拿刀出来擦擦
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_move_001","T1_wait_001_move_001")  --镖师走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("T1_TJ01_wait_000","T1_TJ01_wait_000")  --铁匠01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    

STake_Start("T1_TJ01_emo_000","T1_TJ01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.715,"T1_TJ01_emo_000_1")
    Sound(1.454,"T1_TJ01_emo_000_1")
    Sound(2.293,"T1_TJ01_emo_000_1")

STake_Start("T1_TJ01_emo_001","T1_TJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_wait_000","T1_LH01_wait_000")  --猎户01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_LH01_emo_000","T1_LH01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_emo_001","T1_LH01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_wait_000","T1_BK01_wait_000")  --捕快01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_BK01_emo_000","T1_BK01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_emo_001","T1_BK01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_wait_000","T1_MGDH_wait_000")  --蒙古大汗01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_emo_001","T1_MGDH_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_wait_000","N_T1_LieHu02_wait_000")  --猎户02
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_T1_LieHu02_emo_001","N_T1_LieHu02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_emo_002","N_T1_LieHu02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N_T1_LieHu02_emo_003","N_T1_LieHu02_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_ZDLBFZ_wait_000","T1_ZDLBFZ_wait_000")  --扎达兰百夫长
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_ZDLBFZ_emo_001","T1_ZDLBFZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_ZDLBFZ_emo_002","T1_ZDLBFZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
	
	
	
	
	
	
	
	
	
-----------------------------------------------------------------------------------

--塞外CUTSCENE用动作

--CS1大军冲

--单晓箫配

STake_Start("CS_SW_hurt1_T1_Blade","N1_HAJ01_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt2_T1_Blade","N1_HAJ01_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt3_T1_Blade","N1_MGSB_01_wait_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt4_T1_Blade","N1_life_024")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt5_T1_Blade","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()




STake_Start("CS_SW_WoDi1_T1_GL_B","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_escape1_T1_GL_B","N1_move_023")
    BlendMode(2)
    BlendTime(0.2)
    Loop()



-----------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_T1_wait_001_move_001","T1_wait_001_move_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_LH01_wait_000","T1_LH01_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()




--牛家村CUTSCENE用动作

--CS1开场动画

--俞章廷配

STake_Start("CS_move000_T1_Liehu","N_T1_LieHu02_walk")
    BlendMode(2)
    BlendTime(0.2)
    Loop()STake_Start("T1_wait_000","T1_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_bout_001","T1_bout_001")  --出生跳入
    BlendMode(0)
    BlendTime(0)

STake_Start("T1_wait_001","T1_wait_001")  --镖师
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_wait_001_emo_001","T1_wait_001_emo_001")  --镖师生气表情
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_emo_002","T1_wait_001_emo_002")  --镖师表情拿刀出来擦擦
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_wait_001_move_001","T1_wait_001_move_001")  --镖师走路
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("T1_TJ01_wait_000","T1_TJ01_wait_000")  --铁匠01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()
    

STake_Start("T1_TJ01_emo_000","T1_TJ01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

    Sound(0.715,"T1_TJ01_emo_000_1")
    Sound(1.454,"T1_TJ01_emo_000_1")
    Sound(2.293,"T1_TJ01_emo_000_1")

STake_Start("T1_TJ01_emo_001","T1_TJ01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_wait_000","T1_LH01_wait_000")  --猎户01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_LH01_emo_000","T1_LH01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_LH01_emo_001","T1_LH01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_wait_000","T1_BK01_wait_000")  --捕快01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_BK01_emo_000","T1_BK01_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_BK01_emo_001","T1_BK01_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_wait_000","T1_MGDH_wait_000")  --蒙古大汗01
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_MGDH_emo_001","T1_MGDH_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_wait_000","N_T1_LieHu02_wait_000")  --猎户02
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("N_T1_LieHu02_emo_001","N_T1_LieHu02_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("N_T1_LieHu02_emo_002","N_T1_LieHu02_emo_002")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("N_T1_LieHu02_emo_003","N_T1_LieHu02_emo_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("T1_ZDLBFZ_wait_000","T1_ZDLBFZ_wait_000")  --扎达兰百夫长
    BlendMode(0)
    BlendTime(0.2) 
    Loop()

STake_Start("T1_ZDLBFZ_emo_001","T1_ZDLBFZ_emo_001")
    BlendMode(0)
    BlendTime(0.2) 

STake_Start("T1_ZDLBFZ_emo_002","T1_ZDLBFZ_emo_002")
    BlendMode(0)
    BlendTime(0.2) 
	
	
	
	
	
	
	
	
	
-----------------------------------------------------------------------------------

--塞外CUTSCENE用动作

--CS1大军冲

--单晓箫配

STake_Start("CS_SW_hurt1_T1_Blade","N1_HAJ01_wait_000")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt2_T1_Blade","N1_HAJ01_emo_002")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt3_T1_Blade","N1_MGSB_01_wait_001")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt4_T1_Blade","N1_life_024")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_hurt5_T1_Blade","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()




STake_Start("CS_SW_WoDi1_T1_GL_B","N1_life_034")
    BlendMode(2)
    BlendTime(0.2)
    Loop()

STake_Start("CS_SW_escape1_T1_GL_B","N1_move_023")
    BlendMode(2)
    BlendTime(0.2)
    Loop()



-----------------------------------------------------------------------------------
--蒙古草原CUTSCENE用动作

--CS7场景预览

--程譞配

STake_Start("CS_T1_MGDH_emo_000","T1_MGDH_emo_000")
    BlendMode(0)
    BlendTime(0.2) 


STake_Start("CS_T1_wait_001_move_001","T1_wait_001_move_001")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()


STake_Start("CS_LH01_wait_000","T1_LH01_wait_000")
    BlendMode(0)
    BlendTime(0.2) 
    Loop()




--牛家村CUTSCENE用动作

--CS1开场动画

--俞章廷配

STake_Start("CS_move000_T1_Liehu","N_T1_LieHu02_walk")
    BlendMode(2)
    BlendTime(0.2)
    Loop()