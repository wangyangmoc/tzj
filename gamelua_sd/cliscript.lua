-------------------------------------------------------------------------------
-- Entry point script
-- -------------------------------------------------------------------------------
guiSystem = CEGUI.System:getSingleton()
guiMouse  = CEGUI.MouseCursor:getSingleton()
wndMgr = CEGUI.WindowManager:getSingleton()
worklistMgr = SD.WorkListManager:Instance()
gameEventMgr = SD.CLuaEventManager:Instance()
sceneInfoMgr = SD.CSceneInfoMgr:Instance()
gameInput = SD.CHSGameInput:Instance()

package.path =  '../ui/datafiles/lua_scripts/?.lua;' ..
				'../ui/datafiles/lua_scripts/helpers/?.lua;'..
				'../ui/datafiles/lua_scripts/feature/?.lua;'..
				'../ui/datafiles/lua_scripts/newui/?.lua;'..
				'../ui/datafiles/lua_scripts/newui/frame/?.lua;'..
				'../ui/datafiles/lua_scripts/tutorial/?.lua;'..
				'../ui/datafiles/lua_scripts/tutorial/guides/?.lua;'..
				package.path

-- new modules initialization
function initModules()
	require('utility')
	require('modules')
	require('GameEvents')
	require('FeatureDefine')
	require('guide_constants')
	
	-- load pre-required modules
	modules.load('bars')
	modules.load('attributes')
	
	-- load common modules
	modules.load('DelayJobWorker')
	modules.load('FeatureActivation')
	modules.load('GameEventsListener')
	modules.load('WorldMap')
	modules.load('InstanceMap')
	modules.load('ConfigModule')
	modules.load('UIShortcut')
	modules.load('ItemHelper')
	modules.load('Player')
	
	-- load ui module
	modules.load('ui')
	modules.load('ui_load')
	
	-- load guide module
	modules.load('indicatorFlow')
	modules.load('guideCfg')
	modules.load('guide')
	modules.load('guideMgr')
	
	DelayJobWorker.Init()
	FeatureActivation.Init()
end

function uninitModules()
	DelayJobWorker.UnInit()
	FeatureActivation.UnInit()
	
	modules.unLoadAll()
	package.loaded['utility'] = nil
	package.loaded['modules'] = nil
	package.loaded['GameEvents'] = nil
	package.loaded['FeatureDefine'] = nil
	package.loaded['guide_constants'] = nil
end

-- Execute when SDClient load lua scripts
initModules()

-------------------------------------------------------------------------------
-- global callbacks
-------------------------------------------------------------------------------
SD.ENABLE_DEBUG_USAGE = true

SD.STAGE_LOGIN      = 1
SD.STAGE_SELECTCHAR = 3
SD.STAGE_WORLD      = 4
SD.STAGE_LOADING    = 5
SD.MINUS_ONE = 4294967295  -- (uint)-1
SD.currentStage = 0
SD.currentSceneID = SD.MINUS_ONE

function SD.OnEnterScene(oldSceneId, newSceneId)
	lout('----------------------------START  SD.OnEnterScene() '.. SD.currentSceneID .. ', ' .. newSceneId..'----------------------------')
		
	-- previous scene id is -1, means we are first enter game world 
	if SD.currentSceneID == SD.MINUS_ONE then
		lout('Enter world, load UI')
		ui_load.loadWorldUI()
	end
	
	-- Notify Modules
	ui_load.WorldUI_OnEnterScene(oldSceneId, newSceneId)
	GameEventsListener.OnEnterScene()
	FeatureActivation.OnEnterScene()
	guideMgr.ListenStartGuideEvent()
	
	SD.currentSceneID = newSceneId
	
	
	lout('----------------------------END    SD.OnEnterScene()----------------------------')
end

function SD.OnLeaveScene(oldSceneId)
	lout('----------------------------START SD.OnLeaveScene()----------------------------')
	
	-- Notify modules
	ui_load.WorldUI_OnLeaveScene(oldSceneId)
	GameEventsListener.OnLeaveScene()
	FeatureActivation.OnLeaveScene()
	guideMgr.RemoveStartGuideEvent()
	
	if guideMgr.IsGuideStarted() then
		guideMgr.UnInit()
		collectgarbage()
	end
	
	lout('----------------------------END    SD.OnLeaveScene()----------------------------')
end

function SD.OnEnterStage(oldStageId, newStageId)
	lout('----------------------------START SD.OnEnterStage()----------------------------')
	
	SD.currentStage = newStageId 
	gameInput = SD.CHSGameInput:Instance()
	lout('----------------------------END    SD.OnEnterStage()----------------------------')
end

function SD.OnLeaveStage(newStageId)
	lout('----------------------------START SD.OnLeaveStage()----------------------------')
	
	-- Leave from worldstage and next stage is not worldstage
	if SD.currentStage == SD.STAGE_WORLD then
		if SD.currentStage ~= newStageId then
			lout('Leave world, unload UI')
			gameEventMgr:removeAllEvents()
			ui_load.unloadUI()
			SD.currentSceneID = SD.MINUS_ONE
		end
	end
	SD.currentStage = 0 
	
	lout('----------------------------END    SD.OnLeaveStage()----------------------------')
end

function SD.BeforeScriptReload()
	lout('----------------------------START  SD.BeforeScriptReload()----------------------------')
	
	SD.OnLeaveScene(SD.currentSceneID)
	ui_load.unloadUI()
	
	gameEventMgr:removeAllEvents()
	uninitModules()
	
	lout('----------------------------END    SD.BeforeScriptReload()----------------------------')
end

function SD.AfterScriptReload()
	lout('----------------------------START  SD.AfterScriptReload()----------------------------')
	
	SD.currentStage   = SD.STAGE_WORLD
	SD.currentSceneID = sceneInfoMgr:GetCurrentMapID()
	
	if WorldMap then WorldMap.OnInit() end
	
	ui_load.loadWorldUI()
	SD.OnEnterScene(SD.currentSceneID, SD.currentSceneID)
	
	lout('----------------------------END    SD.BeforeScriptReload()----------------------------')
end

function __G__TRACEBACK__(msg)
	lout('----------------------------')
	lout('LUA ERROR: ' .. tostring(msg))
	lout(debug.traceback())
	lout('----------------------------')
end

function hidemm()
	local mm = wndMgr:getWindow('DefaultMainMenu')
	if mm then mm:hide() end
end
function showmm()
	local mm = wndMgr:getWindow('DefaultMainMenu')
	if mm then mm:show() end
end

function fuckayy()
	if ui.BattleCountFrame then
		ui.BattleCountFrame:Show()
	end
end
function hideayy()
	if ui.BattleCountFrame then
		ui.BattleCountFrame:Hide()
	end
end
lout('done loading cliscript')


