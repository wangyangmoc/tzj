local modname = ...


DELAY_BY_FRAME = 1
DELAY_BY_TIME  = 2

module(modname, package.seeall)

local self = nil

function Init()
	self = _M
	self.jobQueueByFrame = {}
	self.jobQueueByTime  = {}

	self.updateConn = nil

	self.jobQueueByFrame.current = 0
	self.jobQueueByTime.current  = 0
end

function UnInit()
	if self.updateConn then
		self.updateConn:disconnect()
		self.updateConn = nil
	end
	self = nil
end

function PostJob(delayType, delayInterval, jobFunction, args)
	if not self.updateConn then
		local pRootWin = InitWindowPtr('Root')
		if pRootWin then
			self.updateConn = pRootWin:subscribeEvent('WindowUpdate', OnUpdate)
		end
	end
	
	if not self.updateConn then
		lout('WARNING: DelayJobWorker, Update Event was not subscribed, can not handle delay work!!!!')
		return
	end
	
	local jobQueue = nil
	local current = nil
	if delayType == DELAY_BY_FRAME then
		jobQueue = self.jobQueueByFrame
	elseif delayType == DELAY_BY_TIME then
		jobQueue = self.jobQueueByTime
	end
	if not jobQueue then
		lout('WARNING: DelayJobWorker.PostJob, Wrong `delayType` ' .. tostring(delayType))
		return 
	end
	
	local newJob = {delay = delayInterval + jobQueue.current, func = jobFunction, func_args = args}
	local insertIndex = nil
	for i, v in ipairs(jobQueue) do
		if v.delay <= newJob.delay then
			insertIndex = i
			break
		end
	end
	if not insertIndex then insertIndex = #jobQueue + 1 end
	
	table.insert(jobQueue, insertIndex, newJob)
	
end

function OnUpdate(args)
	if not self.updateConn then return end
	
	local e = tolua.cast(args, 'CEGUI::UpdateEventArgs')
	local fTime = e.lastFrameTime
	self.jobQueueByFrame.current = self.jobQueueByFrame.current + 1
	self.jobQueueByTime.current = self.jobQueueByTime.current + fTime
	
	local queues = {self.jobQueueByFrame, self.jobQueueByTime}
	for k, queue in ipairs(queues) do
		local processed_num = 0
		for i = #queue, 1, -1 do 
			local job = queue[i]
			if job.delay > queue.current then
				break
			end
			local func = job.func
			if func then func(job.func_args) end
			processed_num = processed_num + 1
		end
		for i = 1, processed_num do
			table.remove(queue)
		end
	end
	--lout('Job Num: By Frame ' .. #self.jobQueueByFrame .. ', By Time: ' .. #self.jobQueueByTime)
	if #self.jobQueueByFrame == 0 and #self.jobQueueByTime == 0 then
		if self.updateConn then
			self.updateConn:disconnect()
			self.updateConn = nil
		end
	end
end





