Attributes = {}


 --[[
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
`=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]
-------------------------------------------------------------------------------
-- configurable content
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- utilities
-------------------------------------------------------------------------------
local function GetPlayerUnit()
    local playerEnt = Utility.GetPlayerEntity()
    return playerEnt:GetUnit()
end

local function setTips(warg, fmt, ...)
    local we_arg = tolua.cast(warg, 'CEGUI::WindowEventArgs')
    local w = we_arg.window
    local tips = string.format(fmt, unpack(arg))
    w:setTooltipText(tips)
end

local function iGetAttr(attr)
    local u = GetPlayerUnit()
    return u:GetAttr(attr)
end

-- set number to smaller font type
local function sm(attr)
	local default_font="[font='MSYH-9']"
	local destiny_font="[font='MSYH-9']"
    --return  destiny_font..attr..default_font
	return	attr
end

-- 100        v=100
local function DispFunc1_title(attr)
    local u = GetPlayerUnit()    
    return string.format('%d', u:GetAttr(attr))
end

local function DispFunc1(attr)
    local u = GetPlayerUnit()    
    return string.format("[font='MSYH-9'] %d", u:GetAttr(attr))
end

-- 110% (+10%)    v=10%
local function DispFunc2_title(attr)
    local u = GetPlayerUnit()
    local val = u:GetAttr(attr)
    if val == 0 then
        return '100%'
    elseif val > 0 then
        return string.format( "%.2f%% [colour='ff02ca02'] (%+.2f%%)", (1+val)*100,val*100 )
    else
        return string.format( "%.2f%% [colour='ffb50202'] (%+.2f%%)", (1+val)*100,val*100 )	
	end
end

local function DispFunc2(attr)
    local u = GetPlayerUnit()
    local val = u:GetAttr(attr)
    if val == 0 then
        return '100%'
    elseif val > 0 then
        return string.format( "[font='MSYH-9'] [colour='ff02ca02']%.2f%% [colour='ff9aff9a'] (%+.2f%%)", (1+val)*100,val*100 )
    else
        return string.format( "[font='MSYH-9'] [colour='ffb50202']%.2f%% [colour='ffff4500'] (%+.2f%%)", (1+val)*100,val*100 )	
	end
end

-- A ( A - B )   if A - B == 0 then hide parentheses
local function DispFunc3_title(attr1, attr2)
    local u = GetPlayerUnit()
    local a = u:GetAttr(attr1)
    local b = u:GetAttr(attr2)
    local diff = a - b
    if diff == 0 then
        return tostring(a)
    elseif diff>0 then
        return string.format( "[colour='ff02ca02'] %d", a)
	else
		return string.format( "[colour='ffb50202'] %d", a)
    end
end



local function DispFunc3(attr1, attr2)
    local u = GetPlayerUnit()
    local a = u:GetAttr(attr1)
    local b = u:GetAttr(attr2)
    local diff = a - b
    if diff == 0 then
        return tostring(a)
    elseif diff>0 then
        return string.format( "[font='MSYH-9'] [colour='ff02ca02']%d [colour='ff9aff9a'](%+d)", a, diff)
	else
		return string.format( "[font='MSYH-9'] [colour='ffb50202']%d [colour='ffff4500'](%+d)", a, diff)
    end
end


-- A - B
local function DispFunc4_title(attr1, attr2)
    local u = GetPlayerUnit()
    local a1 = u:GetAttr(attr1)
    local a2 = u:GetAttr(attr2)
    return string.format( '%.1f - %.1f', a1, a2 )
end

local function DispFunc4(attr1, attr2)
    local u = GetPlayerUnit()
    local a1 = u:GetAttr(attr1)
    local a2 = u:GetAttr(attr2)
    return string.format( "[font='MSYH-9'] %d - %d", a1, a2 )
end

-- v%
local function DispFunc5_title(attr,digits)
--digits只要存在，就保留0位小数，否则保留2位小数
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
    if v == 0 then
        return '0'
    else
		if digits == nil then
			return string.format('%.2f%%', v*100)
		else
			return string.format('%.0f%%', v*100)
		end
    end
end

local function DispFunc5(attr,digits)
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
    if v == 0 then
        return '0'
    else
		if digits == nil then
			return string.format("[font='MSYH-9'] %.2f%%", v*100)
		else
			return string.format("[font='MSYH-9'] %.0f%%", v*100)
		end
    end
end

-- +v%
local function DispFunc6_title(attr)
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
    return string.format('%+.2f%%', v*100)
end

local function DispFunc6(attr)
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
    return string.format("[font='MSYH-9'] %+.2f%%", v*100)
end


-- A(B + A-B )

local function DispFunc7(attr1, attr2)
    local u = GetPlayerUnit()
    local a = u:GetAttr(attr1)
    local b = u:GetAttr(attr2)
    local diff = a - b
    if diff == 0 then
        return tostring(a)
    elseif diff>0 then
        return string.format( "%d (%d[colour='ff02ca02']%+d[colour='FFFFFFFF'])", a, b,diff)
	else
		return string.format( "%d (%d[colour='ffb50202']%+d[colour='FFFFFFFF'])", a,b, diff)
    end
end

-- 极效伤害率 +400%    default_yuansujixiao  = 4
local function DispFunc8_title(attr)
    local u = GetPlayerUnit()
	local a = 4
    local v = u:GetAttr(attr) - a
    return string.format('%+.0f%%', v*100)
end

local function DispFunc8(attr)
    local u = GetPlayerUnit()
	local a = 4
    local v = u:GetAttr(attr) -a
    return string.format("[font='MSYH-9'] %+.0f%%", v*100)
end

-- 特效转换率 *0.012 *0.1    --累计值转几率 0.03  --buff modify 0.1
local function DispFunc9_title(attr)
    local u = GetPlayerUnit()
	local a = 0.0012
    local v = u:GetAttr(attr) * a
    return string.format('%.2f%%', v*100)
end

local function DispFunc9(attr)
    local u = GetPlayerUnit()
	local a = 0.0012
    local v = u:GetAttr(attr) *a
    return string.format("[font='MSYH-9'] %.2f%%", v*100)
end


local function DispCuilianFunc()
		--return string.format("%s", Utility.GetExtractSuitStr():c_str());
		return Utility.UnicodeToUTF8(Utility.GetExtractSuitStr()):c_str()
end

-- for speed  110(+30)			default speed=80
local function DispFunc_speed_title(attr)
    local u = GetPlayerUnit()
    local a = u:GetAttr(attr)
    local b = 80		--default speed
    local diff = a - b
    if diff == 0 then
        return tostring(a)
    elseif diff>0 then
        return string.format( "[colour='ff02ca02'] %d", a)
	else
		return string.format( "[colour='ffb50202'] %d", a)
    end
end


-- 显示战斗力
local function DispFunc_fighting(attr)
    local u = GetPlayerUnit()    
    return string.format('%d', u:GetFightingCapacity(attr))
end


local function minf(v1,v2)
	if v1>v2 then
		return v2
	else
		return v1
	end
end

local function maxf(v1,v2)
	if v1>v2 then
		return v1
	else
		return v2
	end
end


local function flv(lv)			--临时使用的函数，等萱儿给了EProfessionType的取值接口后可以直接读表
	local sep_lv=50
	if lv<sep_lv then
		return lv/25+0.06
	else
		return (sep_lv/25+0.06)*(1.02^(lv-sep_lv))
	end
end

--v%,最大80%，default_kangbao =0.8
local function DispFunc10_title(attr)
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
	local a = 0.8
    return string.format('%.2f%%', minf(a,v)*100)
end

local function DispFunc10(attr)
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
	local a = 0.8
    return string.format("[font='MSYH-9'] %.2f%%", minf(a,v)*100)
end

--+acc%
local function DispFunc_Acc(attr,coef)
	if coef == nil then
		coef=0.1
	end
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
	local lv=u:GetAttr(ATTR_LEVEL)
	local acc=v/flv(lv)*coef--临时方案，在可以取到门派参数后需要读表获取
    return string.format('%+.2f%%', acc)
end

--percent%
local function DispFunc_Percent(attr,coef,autodecrease,attr2)
	if coef == nil then
		coef=0.1
	end
	
	local attr2num
	if attr2== nil then
		attr2num=0
	else
		attr2num=iGetAttr(attr2)
	end 
	
    local u = GetPlayerUnit()
    local v = u:GetAttr(attr)
	local lv=u:GetAttr(ATTR_LEVEL)
	local acc=v/flv(lv)*coef/100--临时方案，在可以取到门派参数后需要读表获取
	
	if autodecrease == nil then
		acc=acc
	else
		local c=0.9
		local k=1.1111
		acc=c*acc/(c*k+acc)+attr2num
	end
		
    return string.format('%.2f%%', acc*100)
end


local function GetPEHp()
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local mhp=iGetAttr(ATTR_MAX_HP)
	local Pdef=iGetAttr(ATTR_DEFENCE_PHYSICS)
    return mhp /(1-minf(0.95,Pdef/(1000*const_flv+Pdef)))	  --外功有效生命
end 

local function GetIEHp()
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local mhp=iGetAttr(ATTR_MAX_HP)
	local Idef=iGetAttr(ATTR_DEFENCE_INTERNAL)
    return mhp /(1-minf(0.95,Idef/(1000*const_flv+Idef)))	  --内功有效生命
end 

local function GetIP()
	local str=iGetAttr(ATTR_PRIMAL_STRENGTH)
	local cou=iGetAttr(ATTR_PRIMAL_COURAGE)
	if str>cou then
		return 1	--外功职业
	else
		return 2	--内功职业
	end 
end

local function GetHeal()
	if GetIP()==1 then
		return (iGetAttr(ATTR_ATTACK_PHYSICS)+0.5*(iGetAttr(ATTR_WEAPON_DAMAGE_PHYSICS_MIN)+iGetAttr(ATTR_WEAPON_DAMAGE_PHYSICS_MAX)))*0.9+iGetAttr(ATTR_HEAL)
	else	
		return (iGetAttr(ATTR_ATTACK_INTERNAL)+0.5*(iGetAttr(ATTR_WEAPON_DAMAGE_INTERNAL_MIN)+iGetAttr(ATTR_WEAPON_DAMAGE_INTERNAL_MAX)))*0.9+iGetAttr(ATTR_HEAL)
	end 
end 

-- 显示外功坚韧
local function DispFunc_PEHP()  
    return string.format('%d', GetPEHp())
end


-- 显示内功坚韧
local function DispFunc_IEHP()  
    return string.format('%d', GetIEHp())
end

-- 显示治疗
local function DispFunc_HEAL()  
    return string.format('%d', GetHeal())
end

local function updateText()
    local w = nil 

    lout( 'in updateText' )
    -------------------------------------------------------------------------------------------------
	--战斗力
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/zhandouli_Frame/Value') 
    w:setText(DispFunc_fighting('FIGHTING_CAPACITY'))
	w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/zhandouli_Frame/Value_PVP') 
    w:setText(DispFunc_fighting('PVP_FIGHTING_CAPACITY'))
	--外功坚韧
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/waigongjianren_Frame/Value') 
    w:setText(DispFunc_PEHP())
	--内功坚韧
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/neigongjianren_Frame/Value') 
    w:setText(DispFunc_IEHP())	
    --血气
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/qixue_Frame/Value') 
    w:setText(DispFunc1_title(ATTR_MAX_HP))
    --真气
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/nuqi_Frame/Value') 
    w:setText(DispFunc1_title(ATTR_MAX_MP))
    --跑步速度
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/yidong_Frame/Value')
    w:setText(DispFunc_speed_title(ATTR_RUN_SPEED))
    --治疗效果
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/zhiliao_Frame/Value')
    w:setText(DispFunc_HEAL())
	
    -------------------------------------------------------------------------------------------------
    --力道
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/lidao_Frame/Value')
    w:setText(DispFunc3_title(ATTR_STRENGTH, ATTR_PRIMAL_STRENGTH))
    --身法
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/shenfa_Frame/Value')
    w:setText(DispFunc3_title(ATTR_DEXTERITY, ATTR_PRIMAL_DEXTERITY))
    --内力
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/neili_Frame/Value')
    w:setText(DispFunc3_title(ATTR_COURAGE, ATTR_PRIMAL_COURAGE))
    --神念
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/shennian_Frame/Value')
    w:setText(DispFunc3_title(ATTR_COMPREHENSION, ATTR_PRIMAL_COMPREHENSION))
    --筋骨
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/jingu_Frame/Value')
    w:setText(DispFunc3_title(ATTR_PHYSIQUE, ATTR_PRIMAL_PHYSIQUE))
    --体魄
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/tipo_Frame/Value')
    w:setText(DispFunc3_title(ATTR_STAMINA, ATTR_PRIMAL_STAMINA))
    --幸运
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/xingyun_Frame/Value')
    w:setText(DispFunc3_title(ATTR_LUCKY, ATTR_PRIMAL_LUCKY))
    -------------------------------------------------------------------------------------------------
    --攻击强度(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/gongjili_Frame/Value')
    w:setText(DispFunc1_title(ATTR_ATTACK_PHYSICS))
	--武器伤害(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/wuqishanghai_Frame/Value')
    w:setText(DispFunc4_title(ATTR_WEAPON_DAMAGE_PHYSICS_MIN,ATTR_WEAPON_DAMAGE_PHYSICS_MAX))
    --暴击率(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/baojilv_Frame/Value')
    w:setText(DispFunc5_title(ATTR_MORTAL_PHYSICS_RATE))
    --爆击效果(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/baojixiaoguo_Frame/Value')
    w:setText(DispFunc6_title(ATTR_MORTAL_PHYSICS_DAMAGE_RATE))
    --破招率(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/pozhaozhi_Frame/Value')
    w:setText(DispFunc_Percent(ATTR_PHYSICS_BREAK_LEVEL,0.25,1,ATTR_PHYSICS_BREAK_RATE_SKILL))
    --穿透(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/chuantou_Frame/Value')
    w:setText(DispFunc1_title(ATTR_STRIKE_PHYSICS))
    --精准率(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/jingzhunlv_Frame/Value')
    w:setText(DispFunc_Acc(ATTR_PHYSICS_ACCURACY_LEVEL))
    -------------------------------------------------------------------------------------------------
    --攻击强度(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/gongjili_Frame/Value')
    w:setText(DispFunc1_title(ATTR_ATTACK_INTERNAL))
	--武器伤害(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/wuqishanghai_Frame/Value')
    w:setText(DispFunc4_title(ATTR_WEAPON_DAMAGE_INTERNAL_MIN,ATTR_WEAPON_DAMAGE_INTERNAL_MAX))
    --暴击率(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/baojilv_Frame/Value')
    w:setText(DispFunc5_title(ATTR_MORTAL_INTERNAL_RATE))
    --爆击效果(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/baojixiaoguo_Frame/Value')
    w:setText(DispFunc6_title(ATTR_MORTAL_INTERNAL_DAMAGE_RATE))
    --破招值(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/pozhaozhi_Frame/Value')
    w:setText(DispFunc_Percent(ATTR_INTERNAL_BREAK_LEVEL,0.25,1,ATTR_INTERNAL_BREAK_RATE_SKILL))
    --穿透(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/chuantou_Frame/Value')
    w:setText(DispFunc1_title(ATTR_STRIKE_INTERNAL))
    --精准率(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/jingzhunlv_Frame/Value')
    w:setText(DispFunc_Acc(ATTR_INTERNAL_ACCURACY_LEVEL))
    -------------------------------------------------------------------------------------------------
    --防御力(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/fangyuli_Frame/Value')
    w:setText(DispFunc1_title(ATTR_DEFENCE_PHYSICS))
    --抗暴率(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/kangbaolv_Frame/Value')
    w:setText(DispFunc10_title(ATTR_ANTIMORTAL_PHYSICS_RATE))
    --卸力值(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/xielizhi_Frame/Value')
    w:setText(DispFunc_Percent(ATTR_PHYSICS_WITHSTAND_LEVEL,0.25,1,ATTR_PHYSICS_WITHSTAND_RATE_SKILL))
    --卸力效果(外)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/xielixiaoguo_Frame/Value')
    w:setText(DispFunc1_title(ATTR_PHYSICS_WITHSTAND_EFFECT))
    -------------------------------------------------------------------------------------------------
    --防御力(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/fangyuli_Frame/Value')
    w:setText(DispFunc1_title(ATTR_DEFENCE_INTERNAL))
    --抗暴率(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/kangbaolv_Frame/Value')
    w:setText(DispFunc10_title(ATTR_ANTIMORTAL_INTERNAL_RATE))
    --卸力值(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/xielizhi_Frame/Value')
    w:setText(DispFunc_Percent(ATTR_INTERNAL_WITHSTAND_LEVEL,0.25,1,ATTR_INTERNAL_WITHSTAND_RATE_SKILL))
    --卸力效果(内)
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/xielixiaoguo_Frame/Value')
    w:setText(DispFunc1_title(ATTR_INTERNAL_WITHSTAND_EFFECT))
    -------------------------------------------------------------------------------------------------
    --水攻
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/shui_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WATER_ATTACK))
    --火攻
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/huo_Frame/Value')
    w:setText(DispFunc1_title(ATTR_FIRE_ATTACK))
    --风攻
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/feng_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WIND_ATTACK))
    --雷攻
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/lei_Frame/Value')
    w:setText(DispFunc1_title(ATTR_THUNDER_ATTACK))
    --虹攻
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/hong_Frame/Value')
    w:setText(DispFunc1_title(ATTR_RAINBOW_ATTACK))
    --闇攻
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/an_Frame/Value')
    w:setText(DispFunc1_title(ATTR_DARK_ATTACK))

    --水伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/shuijixiao_Frame/Value')
    w:setText(DispFunc5_title(ATTR_WATER_HURT_INCREASE,0))
    --火伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/huojixiao_Frame/Value')
    w:setText(DispFunc5_title(ATTR_FIRE_HURT_INCREASE,0))
    --风伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/fengjixiao_Frame/Value')
    w:setText(DispFunc5_title(ATTR_WIND_HURT_INCREASE,0))
    --雷伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/leijixiao_Frame/Value')
    w:setText(DispFunc5_title(ATTR_THUNDER_HURT_INCREASE,0))
    --虹伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/hongjixiao_Frame/Value')
    w:setText(DispFunc5_title(ATTR_RAINBOW_HURT_INCREASE,0))
    --闇伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/anjixiao_Frame/Value')
    w:setText(DispFunc5_title(ATTR_DARK_HURT_INCREASE,0))

    --水抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/shuikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WATER_RESIST))
    --火抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/huokang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_FIRE_RESIST))
    --风抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/fengkang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WIND_RESIST))
    --雷抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/leikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_THUNDER_RESIST))
    --虹抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/hongkang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_RAINBOW_RESIST))
    --闇抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/ankang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_DARK_RESIST))	
    -------------------------------------------------------------------------------------------------
    --水极效
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/shui_Frame/Value')
    w:setText(DispFunc5_title(ATTR_WATER_MORTAL_RATE,0))
    --火极效
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/huo_Frame/Value')
    w:setText(DispFunc5_title(ATTR_FIRE_MORTAL_RATE,0))
    --风极效
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/feng_Frame/Value')
    w:setText(DispFunc5_title(ATTR_WIND_MORTAL_RATE,0))
    --雷极效
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/lei_Frame/Value')
    w:setText(DispFunc5_title(ATTR_THUNDER_MORTAL_RATE,0))
    --虹极效
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/hong_Frame/Value')
    w:setText(DispFunc5_title(ATTR_RAINBOW_MORTAL_RATE,0))
    --闇极效
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/an_Frame/Value')
    w:setText(DispFunc5_title(ATTR_DARK_MORTAL_RATE,0))
	
    --水极效伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/shuijixiao_Frame/Value')
    w:setText(DispFunc8_title(ATTR_WATER_MORTAL_DAMAGE_RATE))
    --火极效伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/huojixiao_Frame/Value')
    w:setText(DispFunc8_title(ATTR_FIRE_MORTAL_DAMAGE_RATE))
    --风极效伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/fengjixiao_Frame/Value')
    w:setText(DispFunc8_title(ATTR_WIND_MORTAL_DAMAGE_RATE))
    --雷极效伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/leijixiao_Frame/Value')
    w:setText(DispFunc8_title(ATTR_THUNDER_MORTAL_DAMAGE_RATE))
    --虹极效伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/hongjixiao_Frame/Value')
    w:setText(DispFunc8_title(ATTR_RAINBOW_MORTAL_DAMAGE_RATE))
    --闇极效伤害
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/anjixiao_Frame/Value')
    w:setText(DispFunc8_title(ATTR_DARK_MORTAL_DAMAGE_RATE))

    --水极效伤害抵抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/shuikang_Frame/Value')
    w:setText(DispFunc5_title(ATTR_WATER_MORTAL_SUB_DAMAGE_RATE,0))
    --火极效伤害抵抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/huokang_Frame/Value')
    w:setText(DispFunc5_title(ATTR_FIRE_MORTAL_SUB_DAMAGE_RATE,0))
    --风极效伤害抵抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/fengkang_Frame/Value')
    w:setText(DispFunc5_title(ATTR_WIND_MORTAL_SUB_DAMAGE_RATE,0))
    --雷极效伤害抵抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/leikang_Frame/Value')
    w:setText(DispFunc5_title(ATTR_THUNDER_MORTAL_SUB_DAMAGE_RATE,0))
    --虹极效伤害抵抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/hongkang_Frame/Value')
    w:setText(DispFunc5_title(ATTR_RAINBOW_MORTAL_SUB_DAMAGE_RATE,0))
    --闇极效伤害抵抗
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/ankang_Frame/Value')
    w:setText(DispFunc5_title(ATTR_DARK_MORTAL_SUB_DAMAGE_RATE,0))
	
	--水特效累计值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/shuichufa_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WATER_SPECIAL_TRIG_POINT))
    --火特效累计值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/huochufa_Frame/Value')
    w:setText(DispFunc1_title(ATTR_FIRE_SPECIAL_TRIG_POINT))
    --风特效累计值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/fengchufa_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WIND_SPECIAL_TRIG_POINT))
    --雷特效累计值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/leichufa_Frame/Value')
    w:setText(DispFunc1_title(ATTR_THUNDER_SPECIAL_TRIG_POINT))
    --虹特效累计值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/hongchufa_Frame/Value')
    w:setText(DispFunc1_title(ATTR_RAINBOW_SPECIAL_TRIG_POINT))
    --闇特效累计值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/anchufa_Frame/Value')
    w:setText(DispFunc1_title(ATTR_DARK_SPECIAL_TRIG_POINT))
	
	--水特效累计抵抗值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/shuichufadikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WATER_SPECIAL_TRIG_ANTI_POINT))
    --火特效累计抵抗值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/huochufadikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_FIRE_SPECIAL_TRIG_ANTI_POINT))
    --风特效累计抵抗值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/fengchufadikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_WIND_SPECIAL_TRIG_ANTI_POINT))
    --雷特效累计抵抗值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/leichufadikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_THUNDER_SPECIAL_TRIG_ANTI_POINT))
    --虹特效累计抵抗值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/hongchufadikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_RAINBOW_SPECIAL_TRIG_ANTI_POINT))
    --闇特效累计抵抗值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/anchufadikang_Frame/Value')
    w:setText(DispFunc1_title(ATTR_DARK_SPECIAL_TRIG_ANTI_POINT))
    
    --淬炼套装值
    w = wndMgr:getWindow('Root/Attribute/Frame/ScrollablePane/Cuilian_Frame')
    w:setText(DispCuilianFunc());
end

local function subscribeEvents()

    -- checkbox handlers
    local map = {}
    map['Root/Attribute/Frame/ScrollablePane/Tongyong_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/Tongyong_Frame'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame'
    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame'
    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame'
    map['Root/Attribute/Frame/ScrollablePane/waigongfangyu_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame'
    map['Root/Attribute/Frame/ScrollablePane/neigongfangyu_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame'
    map['Root/Attribute/Frame/ScrollablePane/Cuilian_Text/Btn'] = 'Root/Attribute/Frame/ScrollablePane/Cuilian_Frame'

    for k,v in pairs(map) do
        local w = wndMgr:getWindow(k)
        w:subscribeEvent('CheckStateChanged', 'Attributes_toggleSubgroupVisible')
        w:setUserString('target', v)
    end

    -- Root/Attribute show/hide handlers
    -- wndMgr:getWindow('Root/CharacterEquip/CharacterAttribute'):subscribeEvent('Clicked', 'Attributes_toggleAttributesPanel')
    -- wndMgr:getWindow('Root/Attribute/TitleBG/CloseBtn'):subscribeEvent('Clicked', 'Attributes_toggleAttributesPanel')

    --tips handlers
    map = {}
	map['Root/Attribute/Frame/ScrollablePane/Zhanli_Text/zhandouli_tips'] = 'Attributes_Zhandouli'
	map['Root/Attribute/Frame/ScrollablePane/Zhanli_Text/zhandouliPVP_tips'] = 'Attributes_Zhandouli_PVP'
	map['Root/Attribute/Frame/ScrollablePane/Zhanli_Text/waigongjianren_tips'] = 'Attributes_Waigongjianren'
	map['Root/Attribute/Frame/ScrollablePane/Zhanli_Text/neigongjianren_tips'] = 'Attributes_Neigongjianren'
	
    map['Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/qixue_tips'] = 'Attributes_QiXue'
    map['Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/nuqi_tips'] = 'Attributes_NuQi'
    map['Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/yidong_tips'] = 'Attributes_YiDong'
	map['Root/Attribute/Frame/ScrollablePane/Tongyong_Frame/zhiliao_tips'] = 'Attributes_zhiliao'

    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/lidao_tips'] = 'Attributes_LiDao'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/shenfa_tips'] = 'Attributes_ShenFa'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/neili_tips'] = 'Attributes_NeiLi'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/shennian_tips'] = 'Attributes_ShenNian'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/jingu_tips'] = 'Attributes_JinGu'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/tipo_tips'] = 'Attributes_TiPo'
    map['Root/Attribute/Frame/ScrollablePane/Yijishuxing_Frame/xingyun_tips'] = 'Attributes_XingYun'

    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/gongjili_tips'] = 'Attributes_WaiGongGongJiLi'
	map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/wuqishanghai_tips'] = 'Attributes_WaiGongWeapon'
    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/baojilv_tips'] = 'Attributes_WaiGongBaoJiLv'
    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/baojixiaoguo_tips'] = 'Attributes_WaiGongBaoJiXiaoGuo'
    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/pozhaozhi_tips'] = 'Attributes_WaiGongPoZhaoZhi'
    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/chuantou_tips'] = 'Attributes_WaiGongChuanTou'
    map['Root/Attribute/Frame/ScrollablePane/waigonggongji_Frame/jingzhunlv_tips'] = 'Attributes_WaiGongJingZhunLv'

    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/gongjili_tips'] = 'Attributes_NeiGongGongJiLi'
	map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/wuqishanghai_tips'] = 'Attributes_NeiGongWeapon'
    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/baojilv_tips'] = 'Attributes_NeiGongBaoJiLv'
    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/baojixiaoguo_tips'] = 'Attributes_NeiGongBaoJiXiaoGuo'
    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/pozhaozhi_tips'] = 'Attributes_NeiGongPoZhaoZhi'
    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/chuantou_tips'] = 'Attributes_NeiGongChuanTou'
    map['Root/Attribute/Frame/ScrollablePane/neigonggongji_Frame/jingzhunlv_tips'] = 'Attributes_NeiGongJingZhunLv'

    map['Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/fangyuli_tips'] = 'Attributes_WaiGongFangYuLi'
    map['Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/kangbaolv_tips'] = 'Attributes_WaiGongKangBaoLv'
    map['Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/xielizhi_tips'] = 'Attributes_WaiGongXieLiZhi'
    map['Root/Attribute/Frame/ScrollablePane/waigongfangyu_Frame/xielixiaoguo_tips'] = 'Attributes_WaiGongXieLiXiaoGuo'

    map['Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/fangyuli_tips'] = 'Attributes_NeiGongFangYuLi'
    map['Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/kangbaolv_tips'] = 'Attributes_NeiGongKangBaoLv'
    map['Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/xielizhi_tips'] = 'Attributes_NeiGongXieLiZhi'
    map['Root/Attribute/Frame/ScrollablePane/neigongfangyu_Frame/xielixiaoguo_tips'] = 'Attributes_NeiGongXieLiXiaoGuo'

    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/shui_tips'] = 'Attributes_Shui'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/huo_tips'] = 'Attributes_Huo'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/feng_tips'] = 'Attributes_Feng'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/lei_tips'] = 'Attributes_Lei'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/hong_tips'] = 'Attributes_Hong'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/an_tips'] = 'Attributes_An'

    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/shuijizhi_tips'] = 'Attributes_Shuishanghai'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/huojizhi_tips'] = 'Attributes_Huoshanghai'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/fengjizhi_tips'] = 'Attributes_Fengshanghai'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/leijizhi_tips'] = 'Attributes_Leishanghai'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/hongjizhi_tips'] = 'Attributes_Hongshanghai'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/anjizhi_tips'] = 'Attributes_Anshanghai'

    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/shuikang_tips'] = 'Attributes_ShuiKang'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/huokang_tips'] = 'Attributes_HuoKang'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/fengkang_tips'] = 'Attributes_FengKang'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/leikang_tips'] = 'Attributes_LeiKang'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/hongkang_tips'] = 'Attributes_HongKang'
    map['Root/Attribute/Frame/ScrollablePane/yuansunengli_Frame/ankang_tips'] = 'Attributes_AnKang'

    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/shui_tips'] = 'Attributes_Shuijizhi'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/huo_tips'] = 'Attributes_Huojizhi'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/feng_tips'] = 'Attributes_Fengjizhi'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/lei_tips'] = 'Attributes_Leijizhi'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/hong_tips'] = 'Attributes_Hongjizhi'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/an_tips'] = 'Attributes_Anjizhi'

    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/shuijizhi_tips'] = 'Attributes_Shuijixiao'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/huojizhi_tips'] = 'Attributes_Huojixiao'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/fengjizhi_tips'] = 'Attributes_Fengjixiao'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/leijizhi_tips'] = 'Attributes_Leijixiao'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/hongjizhi_tips'] = 'Attributes_Hongjixiao'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/anjizhi_tips'] = 'Attributes_Anjixiao'

    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/shuikang_tips'] = 'Attributes_Shuijixiaokang'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/huokang_tips'] = 'Attributes_Huojixiaokang'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/fengkang_tips'] = 'Attributes_Fengjixiaokang'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/leikang_tips'] = 'Attributes_Leijixiaokang'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/hongkang_tips'] = 'Attributes_Hongjixiaokang'
    map['Root/Attribute/Frame/ScrollablePane/yuansujizhi_Frame/ankang_tips'] = 'Attributes_Anjixiaokang'
	
	map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/shuichufa_tips'] = 'Attributes_Shuichufa'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/huochufa_tips'] = 'Attributes_Huochufa'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/fengchufa_tips'] = 'Attributes_Fengchufa'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/leichufa_tips'] = 'Attributes_Leichufa'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/hongchufa_tips'] = 'Attributes_Hongchufa'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/anchufa_tips'] = 'Attributes_Anchufa'
	
	map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/shuichufadikang_tips'] = 'Attributes_Shuichufadikang'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/huochufadikang_tips'] = 'Attributes_Huochufadikang'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/fengchufadikang_tips'] = 'Attributes_Fengchufadikang'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/leichufadikang_tips'] = 'Attributes_Leichufadikang'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/hongchufadikang_tips'] = 'Attributes_Hongchufadikang'
    map['Root/Attribute/Frame/ScrollablePane/yuansukangxing_Frame/anchufadikang_tips'] = 'Attributes_Anchufadikang'

    for k,v in pairs(map) do
        local w = wndMgr:getWindow(k)
        w:subscribeEvent('SetTgtWnd', v)        
    end
end
-------------------------------------------------------------------------------
-- global callbacks
-------------------------------------------------------------------------------
function Attributes.OnInit()
    updateText() 
    subscribeEvents()
end

function Attributes.OnUpdate()
    updateText()
end

-------------------------------------------------------------------------------
-- handlers
-------------------------------------------------------------------------------
function Attributes_toggleSubgroupVisible(arg)
    local we_arg = tolua.cast(arg, 'CEGUI::WindowEventArgs')
    local me = tolua.cast(we_arg.window, 'CEGUI::Checkbox')
    local targetName = me:getUserString('target')
    local target = wndMgr:getWindow(targetName:c_str())
    target:setVisible(not me:isSelected())
    return true
end

function Attributes_toggleAttributesPanel(arg)
    local w = wndMgr:getWindow('Root/Attribute')
    w:setVisible(not w:isVisible())
    return true
end


-------------------------------------------------------------------------------
-- handlers : tips
-------------------------------------------------------------------------------

function Attributes_Zhandouli(arg) --战斗力
    local fmt = 
[[[font='MSYH-10']PVE战斗力      [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']角色对怪物的战斗能力的数值评估
受角色的等级、装备、技能、宝物等因素影响
]]
    local v1 = DispFunc_fighting('FIGHTING_CAPACITY')
    setTips(arg, fmt, v1)
    return true
end

function Attributes_Zhandouli_PVP(arg) --战斗力
    local fmt = 
[[[font='MSYH-10']PVP战斗力      [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']角色对玩家的战斗能力的数值评估
受角色的等级、装备、技能、宝物、勋章等因素影响
]]
    local v1 = DispFunc_fighting('PVP_FIGHTING_CAPACITY')
    setTips(arg, fmt, v1)
    return true
end

function Attributes_Waigongjianren(arg) --外功坚韧
    local fmt = 
[[[font='MSYH-10']外功坚韧      [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']根据你的当前外功防御和血气上限，计算得出的你能够承受的结算防御前的伤害总量
该数值可作为参考，评估你的角色面对外功攻击时的坚挺程度
请注意，卸力，元素抗性，抗暴等能够有效提升防御能力的属性不在该数值评估范围内
外功坚韧=血气上限/（1-外功防御减伤）
]]
    local v1 = DispFunc_PEHP()
    setTips(arg, fmt, v1)
    return true
end

function Attributes_Neigongjianren(arg) --内功坚韧
    local fmt = 
[[[font='MSYH-10']内功坚韧      [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']根据你的当前内功防御和血气上限，计算得出的你能够承受的结算防御前的伤害总量
该数值可作为参考，评估你的角色面对内功攻击时的坚挺程度
请注意，卸力，元素抗性，抗暴等能够有效提升防御能力的属性不在该数值评估范围内
内功坚韧=血气上限/（1-内功防御减伤）
]]
    local v1 = DispFunc_IEHP()
    setTips(arg, fmt, v1)
    return true
end

function Attributes_QiXue(arg)--血气
    local fmt =
 [[[font='MSYH-10']血气     [colour='FFFFFFFF']%s
 
[font='MSYH-9'][colour='FFc6ff00']决定了你能够承受的伤害总量
根据你的当前内外功防御，你能够承受的结算防御前的伤害量为（不考虑元素抗性）：
外功坚韧(可承受外功伤害)：[colour='FFFFFFFF']%s[colour='FFc6ff00']
内功坚韧(可承受内功伤害)：[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc1_title(ATTR_MAX_HP)
	phpv = string.format("[font='MSYH-9'] %d", GetPEHp())
	ihpv = string.format("[font='MSYH-9'] %d", GetIEHp())
    setTips(arg, fmt, v1,phpv,ihpv)
    return true
end

function Attributes_NuQi(arg) --怒气
    local fmt = 
[[[font='MSYH-10']真气      [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']施展大部分招式需要消耗真气，真气不足时无法施展相应招式
左键招式能够恢复真气
]]
    local v1 = DispFunc1_title(ATTR_MAX_MP)
    setTips(arg, fmt, v1)
    return true
end

function Attributes_YiDong(arg) --移动
    local fmt = 
[[[font='MSYH-10']跑步速度     [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']跑步速度[horz-alignment='right'][right-padding='10'][colour='FFFFFFFF']%s[colour='FFc6ff00']
[horz-alignment='left']加速跑速度[horz-alignment='right'][right-padding='10'][colour='FFFFFFFF']%s[colour='FFc6ff00']
[horz-alignment='left']轻功速度[horz-alignment='right'][right-padding='10'][colour='FFFFFFFF']%s
]]
	local speed=iGetAttr(ATTR_RUN_SPEED)
	local base_speed=80
	local diff=speed-base_speed

		
    if diff == 0 then
        v1=string.format( "[colour='FFFFFFFF']%d ", speed)
    elseif diff>0 then
        v1=string.format( "%d (%d[colour='ff02ca02']%+d[colour='FFFFFFFF'])", speed, base_speed,diff)
	else
		v1=string.format( "%d (%d[colour='ffb50202']%+d[colour='FFFFFFFF'])", speed,base_speed,diff)
    end
	
	local v2 = string.format('%d', speed)
	local v3 = string.format('%d', speed*1.3)
	local v4 = string.format('%d', speed*2.2)
    setTips(arg, fmt, v1, sm(v2), sm(v3) ,sm(v4))
    return true
end


function Attributes_zhiliao(arg) --治疗
    local fmt = 
[[[font='MSYH-10']治疗效果      [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']治疗效果能够提升治疗招式的治疗能力
1点武器伤害提供0.9点治疗效果
1点强度提供0.9点治疗效果
1点神念提供0.6点治疗效果
]]
	--GetHeal()
	local v1 = string.format('%d', GetHeal())
    setTips(arg, fmt, sm(v1))
    return true
end
-----------------------------------------------------------------
function Attributes_LiDao(arg) --力道
    local fmt = 
[[[font='MSYH-10']力道        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']外功强度提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
]]

    local v1 = DispFunc7(ATTR_STRENGTH, ATTR_PRIMAL_STRENGTH)	
	
	local str_atk=0.5			--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
    local v2 = string.format('%d', iGetAttr(ATTR_STRENGTH)*str_atk)			
    setTips(arg, fmt, v1, sm(v2))
    return true
end
function Attributes_ShenFa(arg) --身法
    local fmt = 
[[[font='MSYH-10']身法        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']外功暴击等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']
外功破招等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
外功卸力等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
]]
    local v1 = DispFunc7(ATTR_DEXTERITY, ATTR_PRIMAL_DEXTERITY)
	
	local dex_pcrit=2			--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local dex_pbs=0.4				--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local dex_pws=0.4			--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local v2 = string.format('%d', iGetAttr(ATTR_DEXTERITY)*dex_pcrit)		
	local v3 = string.format('%d', iGetAttr(ATTR_DEXTERITY)*dex_pbs)	
	local v4 = string.format('%d', iGetAttr(ATTR_DEXTERITY)*dex_pws)	
    setTips(arg, fmt, v1,sm(v2),sm(v3),sm(v4))
    return true
end
function Attributes_NeiLi(arg) --内力
    local fmt = 
[[[font='MSYH-10']内力        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']内功强度提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
]]
    local v1 = DispFunc7(ATTR_COURAGE, ATTR_PRIMAL_COURAGE)
	
	local cou_atk=0.5			--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local v2 = string.format('%d', iGetAttr(ATTR_COURAGE)*cou_atk)			
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_ShenNian(arg) --神念
    local fmt = 
[[[font='MSYH-10']神念        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']内功暴击等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']
治疗效果提高[colour='FFFFFFFF']%s[colour='FFc6ff00']
内功破招等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
内功卸力等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
]]
    local v1 = DispFunc7(ATTR_COMPREHENSION, ATTR_PRIMAL_COMPREHENSION)
	
	local dex_icrit=2				--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local dex_ibs=0.4			--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local dex_iws=0.4			--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local com_heal=0.6
	local v2 = string.format('%d', iGetAttr(ATTR_COMPREHENSION)*dex_icrit)	
	local healv=string.format('%d', iGetAttr(ATTR_COMPREHENSION)*com_heal)	
	local v3 = string.format('%d', iGetAttr(ATTR_COMPREHENSION)*dex_ibs)	
	local v4 = string.format('%d', iGetAttr(ATTR_COMPREHENSION)*dex_iws)	
    setTips(arg, fmt, v1,sm(v2),healv,sm(v3),sm(v4))
    return true
end
function Attributes_JinGu(arg) --筋骨
    local fmt = 
[[[font='MSYH-10']筋骨        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']内外功防御提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
]]
    local v1 = DispFunc7(ATTR_PHYSIQUE, ATTR_PRIMAL_PHYSIQUE)
	
	local psq_def=10				--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
	local v2 = string.format('%d', iGetAttr(ATTR_PHYSIQUE)*psq_def)					 
    setTips(arg, fmt, v1, sm(v2))
    return true
end
function Attributes_TiPo(arg) --体魄
    local fmt = 
[[[font='MSYH-10']体魄        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']血气上限提高[colour='FFFFFFFF']%s[colour='FFc6ff00']点
]]
    local v1 = DispFunc7(ATTR_STAMINA, ATTR_PRIMAL_STAMINA)
	
	local sta_hp=40				--$此处需要从table\coefficient文件夹下职业属性中调取属性，暂无
    local v2 = string.format('%d', iGetAttr(ATTR_STAMINA) * sta_hp)
    setTips(arg, fmt, v1, sm(v2))
    return true
end
function Attributes_XingYun(arg) --幸运
    local fmt = 
[[[font='MSYH-10']幸运        [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']赋予你各种神奇能力的属性!
外功暴击伤害效果等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']
内功暴击伤害效果等级提高[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc7(ATTR_LUCKY, ATTR_PRIMAL_LUCKY)
	
	local luk_pcritd=1.25
	local luk_icritd=1.25
	local v2 = string.format('%d', iGetAttr(ATTR_LUCKY) * luk_pcritd )
	local v3 = string.format('%d', iGetAttr(ATTR_LUCKY) * luk_icritd )
	local v8 = string.format('%.2f%%', iGetAttr(ATTR_LUCKY) * 0.05 )
	
    setTips(arg, fmt, v1,sm(v2),sm(v3))
    return true
end
-----------------------------------------------------------------
function Attributes_WaiGongGongJiLi(arg) --外功强度
    local fmt = 
[[[font='MSYH-10']外功强度    [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']增加外功招式的伤害和治疗效果，幅度根据招式而定]]
    local v1 = DispFunc1_title(ATTR_ATTACK_PHYSICS)
    setTips(arg, fmt, v1)
    return true
end

function Attributes_WaiGongWeapon(arg) --外功武器伤害
    local fmt = 
[[[font='MSYH-10']武器伤害    [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']增加外功招式的威力，幅度根据招式而定]]
    local v1 = DispFunc4_title(ATTR_WEAPON_DAMAGE_PHYSICS_MIN,ATTR_WEAPON_DAMAGE_PHYSICS_MAX)
    setTips(arg, fmt, v1)
    return true
end

function Attributes_WaiGongBaoJiLv(arg) --外功暴击率
    local fmt = 
[[[font='MSYH-10']外功暴击率   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']影响使用外功招式时产生暴击的机会
[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功暴击等级可使外功暴击率提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_MORTAL_PHYSICS_RATE)
	local v2 = DispFunc1(ATTR_MORTAL_PHYSICS_LEVEL)
	
	
	local crit_per=1/flv(iGetAttr(ATTR_LEVEL))*1/14	--临时方案，在可以取到门派参数后需要读表获取
    local v3 = string.format('%.2f%%', iGetAttr(ATTR_MORTAL_PHYSICS_LEVEL) * crit_per )

    setTips(arg, fmt, v1, sm(v2), sm(v3))
    return true
end
function Attributes_WaiGongBaoJiXiaoGuo(arg) --外功爆击效果
    local fmt = 
[[[font='MSYH-10']外功暴击效果  [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']影响外功招式产生暴击时的伤害增幅
[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功暴击效果等级可使外功暴击效果提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
在产生外功暴击时可以对目标造成[colour='FFFFFFFF']%s[colour='FFc6ff00']的伤害]]
    local v1 = DispFunc6_title(ATTR_MORTAL_PHYSICS_DAMAGE_RATE)
	local v2 = DispFunc1(ATTR_MORTAL_PHYSICS_DAMAGE_LEVEL)
	
	local critd_per=1/flv(iGetAttr(ATTR_LEVEL))*1/2	--临时方案，在可以取到门派参数后需要读表获取
    local v3 = string.format('%.2f%%', iGetAttr(ATTR_MORTAL_PHYSICS_DAMAGE_LEVEL) * critd_per )	
	local v4 = string.format('%.2f%%', iGetAttr(ATTR_MORTAL_PHYSICS_DAMAGE_RATE)*100 +100 )
    setTips(arg, fmt, v1,sm(v2),sm(v3),sm(v4))
    return true
end
function Attributes_WaiGongPoZhaoZhi(arg) --外功破招值
    local fmt = 
[[[font='MSYH-10']外功破招率   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低对目标造成外功伤害时被卸力的几率
[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功破招值可使同级目标的外功卸力率降低[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc1_title(ATTR_PHYSICS_BREAK_LEVEL)
    local v2 = DispFunc1_title(ATTR_PHYSICS_BREAK_LEVEL)
    local break_per=1/flv(iGetAttr(ATTR_LEVEL))*0.25	--临时方案，在可以取到门派参数后需要读表获取
	local x=iGetAttr(ATTR_PHYSICS_BREAK_LEVEL) * break_per/100
	local c=0.9
	local k=1.1111
	local xx=c*x/(c*k+x)	
    local v3 = string.format('%.2f%%', xx*100 )	
	
    setTips(arg, fmt, v3,sm(v2),sm(v3))
    return true
end
function Attributes_WaiGongChuanTou(arg) --外功穿透
    local fmt = 
[[[font='MSYH-10']外功穿透   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']可以使目标被外功招式攻击时受到更多的伤害
对目标造成外功伤害时，忽略目标[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功防御]]
    local v1 = DispFunc1_title(ATTR_STRIKE_PHYSICS)
    local v2 = DispFunc1(ATTR_STRIKE_PHYSICS)
    setTips(arg, fmt, v1, sm(v2))
    return true
end
function Attributes_WaiGongJingZhunLv(arg) --外功精准率
    local fmt = 
[[[font='MSYH-10']外功精准率   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']降低外功失准的可能
[colour='FFFFFFFF']%s[colour='FFc6ff00']外功精准等级减少攻击目标时的外功失准率[colour='FFFFFFFF']%s[colour='FFc6ff00']
每次对目标进行攻击时，有一定几率失准，造成的伤害[colour='FFFFFFFF']减半[colour='FFc6ff00'];
目标等级比你高时，攻击时发生失准的几率较大

[horz-alignment='left']目标等级[horz-alignment='right'][right-padding='100']失准率[colour='FFFFFFFF']
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
]]
	local lv=iGetAttr(ATTR_LEVEL)
	local acc_per=1/flv(iGetAttr(ATTR_LEVEL))*0.1--临时方案，在可以取到门派参数后需要读表获取
	local acc=iGetAttr(ATTR_PHYSICS_ACCURACY_LEVEL) * acc_per
    local v1 = string.format('%.2f%%', acc )
	local x1 = DispFunc1(ATTR_PHYSICS_ACCURACY_LEVEL)
    local x2 = string.format('%.2f%%', acc )	
	local v2 = string.format('%d', lv-1)	
	local v4 = string.format('%d', lv)
	local v6 = string.format('%d', lv+1)
	local v8 = string.format('%d', lv+2)
	local v3 = string.format('%.2f%%', 100*minf(maxf(0,0.05-acc/100),0.8))
	local v5 = string.format('%.2f%%', 100*minf(maxf(0,0.10-acc/100),0.8))
	local v7 = string.format('%.2f%%', 100*minf(maxf(0,0.15-acc/100),0.8))	
	local v9 = string.format('%.2f%%', 100*minf(maxf(0,0.20-acc/100),0.8))		
    setTips(arg, fmt, v1,sm(x1),sm(x2),sm(v2),sm(v3),sm(v4),sm(v5),sm(v6),sm(v7),sm(v8),sm(v9))
    return true
end
-----------------------------------------------------------------
function Attributes_NeiGongGongJiLi(arg) --内功强度
    local fmt = 
[[[font='MSYH-10']内功强度   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']增加内功招式的伤害和治疗效果，幅度根据招式而定]]
    local v1 = DispFunc1_title(ATTR_ATTACK_INTERNAL)
    setTips(arg, fmt, v1)
    return true

end
function Attributes_NeiGongWeapon(arg) --内功武器伤害
    local fmt = 
[[[font='MSYH-10']武器伤害    [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']增加内功招式的威力，幅度根据招式而定]]
    local v1 = DispFunc4_title(ATTR_WEAPON_DAMAGE_INTERNAL_MIN,ATTR_WEAPON_DAMAGE_INTERNAL_MAX)
    setTips(arg, fmt, v1)
    return true
end
function Attributes_NeiGongBaoJiLv(arg) --内功暴击率
   local fmt = 
[[[font='MSYH-10']内功暴击率   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']影响使用内功招式时产生暴击的机会
[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功暴击等级可使内功暴击率提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_MORTAL_INTERNAL_RATE)
	local v2 = DispFunc1(ATTR_MORTAL_INTERNAL_LEVEL)
	
	local crit_per=1/flv(iGetAttr(ATTR_LEVEL))*1/14		--临时方案，在可以取到门派参数后需要读表获取
    local v3 = string.format('%.2f%%', iGetAttr(ATTR_MORTAL_INTERNAL_LEVEL) * crit_per )

    setTips(arg, fmt, v1, sm(v2), sm(v3))
    return true
end
function Attributes_NeiGongBaoJiXiaoGuo(arg) --内功暴击效果
    local fmt = 
[[[font='MSYH-10']内功暴击效果  [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']影响内功招式产生暴击时的伤害增幅
[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功暴击效果等级可使内功暴击效果提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
在产生内功暴击时可以对目标造成[colour='FFFFFFFF']%s[colour='FFc6ff00']的伤害]]
    local v1 = DispFunc6_title(ATTR_MORTAL_INTERNAL_DAMAGE_RATE)
	local v2 = DispFunc1(ATTR_MORTAL_INTERNAL_DAMAGE_LEVEL)
	
	local critd_per=1/flv(iGetAttr(ATTR_LEVEL))*1/2	--临时方案，在可以取到门派参数后需要读表获取
    local v3 = string.format('%.2f%%', iGetAttr(ATTR_MORTAL_INTERNAL_DAMAGE_LEVEL) * critd_per )
	
	local v4 = string.format('%.2f%%', iGetAttr(ATTR_MORTAL_INTERNAL_DAMAGE_RATE)*100 +100 )
    setTips(arg, fmt, v1,sm(v2),sm(v3),sm(v4))
    return true
end
function Attributes_NeiGongPoZhaoZhi(arg) --内功破招值
    local fmt = 
[[[font='MSYH-10']内功破招率   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低对目标造成内功伤害时被卸力的几率
[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功破招值可使同级目标的内功卸力率降低[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc1_title(ATTR_INTERNAL_BREAK_LEVEL)
    local v2 = DispFunc1_title(ATTR_INTERNAL_BREAK_LEVEL)
    local break_per=1/flv(iGetAttr(ATTR_LEVEL))*0.25	--临时方案，在可以取到门派参数后需要读表获取
	local x=iGetAttr(ATTR_INTERNAL_BREAK_LEVEL) * break_per/100
	local c=0.9
	local k=1.1111
	local xx=c*x/(c*k+x)
	
    local v3 = string.format('%.2f%%', xx*100 )	

    setTips(arg, fmt, v3,sm(v2),sm(v3))
    return true
end
function Attributes_NeiGongChuanTou(arg) --内功穿透
    local fmt = 
[[[font='MSYH-10']内功穿透   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']可以使目标被内功招式攻击时受到更多的伤害
对目标造成内功伤害时，忽略目标[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功防御]]
    local v1 = DispFunc1_title(ATTR_STRIKE_INTERNAL)
    local v2 = DispFunc1(ATTR_STRIKE_INTERNAL)
    setTips(arg, fmt, v1,sm(v2))
    return true
end

function Attributes_NeiGongJingZhunLv(arg) --内功精准率
    local fmt = 
[[[font='MSYH-10']内功精准率   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']降低内功失准的可能
[colour='FFFFFFFF']%s[colour='FFc6ff00']内功精准等级减少攻击目标时的内功失准率[colour='FFFFFFFF']%s[colour='FFc6ff00']
每次对目标进行攻击时，有一定几率失准，造成的伤害[colour='FFFFFFFF']减半[colour='FFc6ff00'];
目标等级比你高时，攻击时发生失准的几率较大

[horz-alignment='left']目标等级[horz-alignment='right'][right-padding='100']失准率[colour='FFFFFFFF']
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
[horz-alignment='left'][left-padding='20']%s[horz-alignment='right'][right-padding='90']%s
]]
	local lv=iGetAttr(ATTR_LEVEL)
	local acc_per=1/flv(iGetAttr(ATTR_LEVEL))*0.1	--临时方案，在可以取到门派参数后需要读表获取
	local acc=iGetAttr(ATTR_INTERNAL_ACCURACY_LEVEL) * acc_per
    local v1 = string.format('%.2f%%', acc )
	local x1 = DispFunc1(ATTR_INTERNAL_ACCURACY_LEVEL)
    local x2 = string.format('%.2f%%', iGetAttr(ATTR_INTERNAL_ACCURACY_LEVEL) * acc_per )	
	local v2 = string.format('%d', lv-1)	
	local v4 = string.format('%d', lv)
	local v6 = string.format('%d', lv+1)
	local v8 = string.format('%d', lv+2)
	local v3 = string.format('%.2f%%', 100*minf(maxf(0,0.05-acc/100),0.8))
	local v5 = string.format('%.2f%%', 100*minf(maxf(0,0.1-acc/100),0.8))
	local v7 = string.format('%.2f%%', 100*minf(maxf(0,0.15-acc/100),0.8))	
	local v9 = string.format('%.2f%%', 100*minf(maxf(0,0.2-acc/100),0.8))		
    setTips(arg, fmt, v1,sm(x1),sm(x2),sm(v2),sm(v3),sm(v4),sm(v5),sm(v6),sm(v7),sm(v8),sm(v9))
    return true
end
-----------------------------------------------------------------
function Attributes_WaiGongFangYuLi(arg) --外功防御力
    local fmt = 
[[[font='MSYH-10']外功防御力   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']降低被外功招式攻击到时承受的伤害
降低受到的外功伤害[colour='FFFFFFFF']%s
]]
	local def=iGetAttr(ATTR_DEFENCE_PHYSICS)
    local v1 = DispFunc1_title(ATTR_DEFENCE_PHYSICS)
	
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
    local v2 = string.format('%.2f%%', minf(95,100*def/(1000*const_flv+def)))	  --防御上限 95%
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_WaiGongKangBaoLv(arg) --外功抗暴率
    local fmt = 
[[[font='MSYH-10']外功抗暴率   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']降低被外功暴击时承受的伤害
[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功抗暴等级降低受到的额外外功暴击伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
提高外功抗暴率并不减少被外功暴击的几率，但是会使你被外功暴击时承受更少的伤害
]]
    local v1 = DispFunc10_title(ATTR_ANTIMORTAL_PHYSICS_RATE)
    local v2 = DispFunc1(ATTR_ANTIMORTAL_PHYSICS_LEVEL)	
	
	local acritd_per=0.2/3/flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取
    local v3 = string.format('%.2f%%', minf(80,iGetAttr(ATTR_ANTIMORTAL_PHYSICS_LEVEL) * acritd_per) )
	
    setTips(arg, fmt, v1,sm(v2),sm(v3))
    return true
end
function Attributes_WaiGongXieLiZhi(arg) --外功卸力率
    local fmt = 
[[[font='MSYH-10']外功卸力率   [colour='FFFFFFFF']%s[colour='FFc6ff00']
[font='MSYH-9']提高触发外功卸力的几率
[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功卸力值可使你被同级目标外功攻击时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发外功卸力
外功卸力触发后，会抵消[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功伤害
目标的外功破招值会降低你的外功卸力的触发几率
]]
	local withstand_per=1/flv(iGetAttr(ATTR_LEVEL))*0.25
	local x=iGetAttr(ATTR_PHYSICS_WITHSTAND_LEVEL) * withstand_per/100
	local c=0.9
	local k=1.1111
	local xx=c*x/(c*k+x)+iGetAttr(ATTR_PHYSICS_WITHSTAND_RATE_SKILL)
    local v1 = string.format('%.2f%%',xx*100 )	
	local v2 = DispFunc1(ATTR_PHYSICS_WITHSTAND_LEVEL)	
	local v3 = DispFunc1(ATTR_PHYSICS_WITHSTAND_EFFECT)
	
    setTips(arg, fmt, v1,v2,v1,v3)
    return true
end
function Attributes_WaiGongXieLiXiaoGuo(arg) --外功卸力效果
    local fmt = 
[[[font='MSYH-10']外功卸力效果   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']影响外功卸力成功后抵挡掉的伤害量
卸力触发后抵挡[colour='FFFFFFFF']%s[colour='FFc6ff00']点外功伤害]]
    local v1 = DispFunc1_title(ATTR_PHYSICS_WITHSTAND_EFFECT)
    local v2 = DispFunc1(ATTR_PHYSICS_WITHSTAND_EFFECT)	
    setTips(arg, fmt, v1,sm(v2))
    return true
end
-----------------------------------------------------------------
function Attributes_NeiGongFangYuLi(arg) --内功防御力
    local fmt = 
[[[font='MSYH-10']内功防御力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低被内功招式攻击到时承受的伤害
降低受到的内功伤害[colour='FFFFFFFF']%s
]]
	local def=iGetAttr(ATTR_DEFENCE_INTERNAL)
    local v1 = DispFunc1_title(ATTR_DEFENCE_INTERNAL)
	
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
    local v2 = string.format('%.2f%%', minf(95,100*def/(1000*const_flv+def)))	  --防御减伤上限 95%
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_NeiGongKangBaoLv(arg) --内功抗暴率
    local fmt = 
[[[font='MSYH-10']内功抗暴率   [colour='FFFFFFFF']%s[colour='FFc6ff00']

[font='MSYH-9']降低被内功暴击时承受的伤害
[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功抗暴等级降低受到的额外内功暴击伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
提高内功抗暴率并不减少被内功暴击的几率，但是会使你被内功暴击时承受更少的伤害
]]
    local v1 = DispFunc10_title(ATTR_ANTIMORTAL_INTERNAL_RATE)
    local v2 = DispFunc1(ATTR_ANTIMORTAL_INTERNAL_LEVEL)	
	
	local acritd_per=0.2/3/flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取
    local v3 = string.format('%.2f%%', minf(80,iGetAttr(ATTR_ANTIMORTAL_INTERNAL_LEVEL) * acritd_per) )
	
    setTips(arg, fmt, v1,sm(v2),sm(v3))
    return true
end
function Attributes_NeiGongXieLiZhi(arg) --内功卸力率

    local fmt = 
[[[font='MSYH-10']内功卸力率   [colour='FFFFFFFF']%s[colour='FFc6ff00']
[font='MSYH-9']提高触发内功卸力的几率
[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功卸力值可使你被同级目标内功攻击时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发内功卸力
内功卸力触发后，会抵消[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功伤害
目标的内功破招值会降低你的内功卸力的触发几率
]]
	local withstand_per=1/flv(iGetAttr(ATTR_LEVEL))*0.25
	local x=iGetAttr(ATTR_INTERNAL_WITHSTAND_LEVEL) * withstand_per/100
	local c=0.9
	local k=1.1111
	local xx=c*x/(c*k+x)+iGetAttr(ATTR_INTERNAL_WITHSTAND_RATE_SKILL)
    local v1 = string.format('%.2f%%',xx*100 )	
	local v2 = DispFunc1(ATTR_INTERNAL_WITHSTAND_LEVEL)	
	local v3 = DispFunc1(ATTR_INTERNAL_WITHSTAND_EFFECT)
	
    setTips(arg, fmt, v1,v2,v1,v3)
    return true
end
function Attributes_NeiGongXieLiXiaoGuo(arg) --内功卸力效果
    local fmt = 
[[[font='MSYH-10']内功卸力效果   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']影响内功卸力成功后抵挡掉的伤害量
卸力触发后抵挡[colour='FFFFFFFF']%s[colour='FFc6ff00']点内功伤害]]
    local v1 = DispFunc1_title(ATTR_INTERNAL_WITHSTAND_EFFECT)
    local v2 = DispFunc1(ATTR_INTERNAL_WITHSTAND_EFFECT)	
    setTips(arg, fmt, v1,sm(v2))
    return true
end
-----------------------------------------------------------------
function Attributes_Shui(arg) --水
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shui']水攻击力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使水属性招式额外附加水属性伤害
附加的伤害值随着水攻击力数值的提高而提高]]
    local v1 = DispFunc1_title(ATTR_WATER_ATTACK)
    setTips(arg, fmt, v1)
    return true
end
function Attributes_Huo(arg) --火
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huo']火攻击力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使火属性招式额外附加火属性伤害
附加的伤害值随着火攻击力数值的提高而提高]]
    local v1 = DispFunc1_title(ATTR_FIRE_ATTACK)
    setTips(arg, fmt, v1)
    return true
end
function Attributes_Feng(arg) --风
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:feng']风攻击力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使风属性招式额外附加风属性伤害
附加的伤害值随着风攻击力数值的提高而提高]]
    local v1 = DispFunc1_title(ATTR_WIND_ATTACK)
    setTips(arg, fmt, v1)
    return true
end
function Attributes_Lei(arg) --雷
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:lei']雷攻击力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使雷属性招式额外附加雷属性伤害
附加的伤害值随着雷攻击力数值的提高而提高]]
    local v1 = DispFunc1_title(ATTR_THUNDER_ATTACK)
    setTips(arg, fmt, v1)
    return true    
end
function Attributes_Hong(arg) --虹
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hong']虹攻击力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使所有招式额外附加虹属性伤害
附加的伤害值随着虹攻击力数值的提高而提高]]
    local v1 = DispFunc1_title(ATTR_RAINBOW_ATTACK)
    setTips(arg, fmt, v1)
    return true
end
function Attributes_An(arg) --闇
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:an']闇攻击力   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使所有招式额外附加闇属性伤害
附加的伤害值随着闇攻击力数值的提高而提高]]
    local v1 = DispFunc1_title(ATTR_DARK_ATTACK)
    setTips(arg, fmt, v1)
    return true
end
--元素伤害
-----------------------------------------------------------------
function Attributes_Shuishanghai(arg) --水伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shuijizhi']水伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的水元素技能和附带的水属性伤害提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_WATER_HURT_INCREASE,0)
    local v2 = DispFunc5(ATTR_WATER_HURT_INCREASE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Huoshanghai(arg) --火伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huojizhi']火伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的火元素技能和附带的火属性伤害提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_FIRE_HURT_INCREASE,0)
    local v2 = DispFunc5(ATTR_FIRE_HURT_INCREASE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Fengshanghai(arg) --风伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:fengjizhi']风伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的风元素技能和附带的风属性伤害提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_WIND_HURT_INCREASE,0)
    local v2 = DispFunc5(ATTR_WIND_HURT_INCREASE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Leishanghai(arg) --雷伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:leijizhi']雷伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的雷元素技能和附带的雷属性伤害提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_THUNDER_HURT_INCREASE,0)
    local v2 = DispFunc5(ATTR_THUNDER_HURT_INCREASE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Hongshanghai(arg) --虹伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hongjizhi']虹伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式附带的虹属性伤害提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_RAINBOW_HURT_INCREASE,0)
    local v2 = DispFunc5(ATTR_RAINBOW_HURT_INCREASE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Anshanghai(arg) --闇伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:anjizhi']闇伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式附带的闇属性伤害提升[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_DARK_HURT_INCREASE,0)
    local v2 = DispFunc5(ATTR_DARK_HURT_INCREASE,0)
    setTips(arg, fmt, v1,v2)
    return true
end

--抗性
-----------------------------------------------------------------
function Attributes_ShuiKang(arg) --水抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shuikang']水抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到的水属性伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]

	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local def=iGetAttr(ATTR_WATER_RESIST)
    local v1 = DispFunc1_title(ATTR_WATER_RESIST)
    local v2 = string.format('%.2f%%', minf(75,100*def/(100*const_flv+def)))	 --抗性减少伤害最高75%
    setTips(arg, fmt, v1,sm(v2))
    return true

end
function Attributes_HuoKang(arg) --火抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huokang']火抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到的火属性伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local def=iGetAttr(ATTR_FIRE_RESIST)
    local v1 = DispFunc1_title(ATTR_FIRE_RESIST)
    local v2 = string.format('%.2f%%', minf(75,100*def/(100*const_flv+def)))	 --抗性减少伤害最高75%
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_FengKang(arg) --风抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:fengkang']风抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到的风属性伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local def=iGetAttr(ATTR_WIND_RESIST)
    local v1 = DispFunc1_title(ATTR_WIND_RESIST)
    local v2 = string.format('%.2f%%', minf(75,100*def/(100*const_flv+def)))	 --抗性减少伤害最高75%
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_LeiKang(arg) --雷抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:leikang']雷抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到的雷属性伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local def=iGetAttr(ATTR_THUNDER_RESIST)
    local v1 = DispFunc1_title(ATTR_THUNDER_RESIST)
    local v2 = string.format('%.2f%%', minf(75,100*def/(100*const_flv+def)))	 --抗性减少伤害最高75%
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_HongKang(arg) --虹抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hongkang']虹抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到的虹属性伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local def=iGetAttr(ATTR_RAINBOW_RESIST)
    local v1 = DispFunc1_title(ATTR_RAINBOW_RESIST)
    local v2 = string.format('%.2f%%', minf(75,100*def/(100*const_flv+def)))	 --抗性减少伤害最高75%	
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_AnKang(arg) --闇抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:ankang']闇抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到的闇属性伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
	local const_flv=flv(iGetAttr(ATTR_LEVEL))	--临时方案，在可以取到门派参数后需要读表获取	
	local def=iGetAttr(ATTR_DARK_RESIST)
    local v1 = DispFunc1_title(ATTR_DARK_RESIST)
    local v2 = string.format('%.2f%%', minf(75,100*def/(100*const_flv+def)))	 --抗性减少伤害最高75%	
    setTips(arg, fmt, v1,sm(v2))
    return true
end

--极致
-----------------------------------------------------------------
function Attributes_Shuijizhi(arg) --水极致
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shuijizhi']水之极效   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带水属性伤害时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率附带出巨量的附带伤害
]]
    local v1 = DispFunc5_title(ATTR_WATER_MORTAL_RATE,0)
    local v2 = DispFunc5(ATTR_WATER_MORTAL_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Huojizhi(arg) --火极致
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huojizhi']火之极效   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带火属性伤害时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率附带出巨量的附带伤害
]]
    local v1 = DispFunc5_title(ATTR_FIRE_MORTAL_RATE,0)
    local v2 = DispFunc5(ATTR_FIRE_MORTAL_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Fengjizhi(arg) --风极致
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:fengjizhi']风之极效   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带风属性伤害时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率附带出巨量的附带伤害
]]
    local v1 = DispFunc5_title(ATTR_WIND_MORTAL_RATE,0)
    local v2 = DispFunc5(ATTR_WIND_MORTAL_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Leijizhi(arg) --雷极致
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:leijizhi']雷之极效   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带雷属性伤害时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率附带出巨量的附带伤害
]]
    local v1 = DispFunc5_title(ATTR_THUNDER_MORTAL_RATE,0)
    local v2 = DispFunc5(ATTR_THUNDER_MORTAL_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Hongjizhi(arg) --虹极致
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hongjizhi']虹之极效   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带虹属性伤害时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率附带出巨量的附带伤害
]]
    local v1 = DispFunc5_title(ATTR_RAINBOW_MORTAL_RATE,0)
    local v2 = DispFunc5(ATTR_RAINBOW_MORTAL_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Anjizhi(arg) --闇极致
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:anjizhi']闇之极效   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带闇属性伤害时，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率附带出巨量的附带伤害
]]
    local v1 = DispFunc5_title(ATTR_DARK_MORTAL_RATE,0)
    local v2 = DispFunc5(ATTR_DARK_MORTAL_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end

--极效伤害
-----------------------------------------------------------------
function Attributes_Shuijixiao(arg) --水极效伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shuijizhi']水之极效伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带水属性伤害产生极致时，造成额外的[colour='FFFFFFFF']%s[colour='FFc6ff00']伤害
该伤害加成只取决于玩家面板的元素攻击力值
]]
    local v1 = DispFunc8_title(ATTR_WATER_MORTAL_DAMAGE_RATE)
    local v2 = string.format('%.0f%%', iGetAttr(ATTR_WATER_MORTAL_DAMAGE_RATE)*100+100 )
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_Huojixiao(arg) --火极效伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huojizhi']火之极效伤害  [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带火属性伤害产生极致时，造成额外的[colour='FFFFFFFF']%s[colour='FFc6ff00']伤害
该伤害加成只取决于玩家面板的元素攻击力值
]]
    local v1 = DispFunc8_title(ATTR_FIRE_MORTAL_DAMAGE_RATE)
    local v2 = string.format('%.0f%%', iGetAttr(ATTR_FIRE_MORTAL_DAMAGE_RATE)*100+100 )
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_Fengjixiao(arg) --风极效伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:fengjizhi']风之极效伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带风属性伤害产生极致时，造成额外的[colour='FFFFFFFF']%s[colour='FFc6ff00']伤害
该伤害加成只取决于玩家面板的元素攻击力值
]]
    local v1 = DispFunc8_title(ATTR_WIND_MORTAL_DAMAGE_RATE)
    local v2 = string.format('%.0f%%', iGetAttr(ATTR_WIND_MORTAL_DAMAGE_RATE)*100+100)
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_Leijixiao(arg) --雷极效伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:leijizhi']雷之极效伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带雷属性伤害产生极致时，造成额外的[colour='FFFFFFFF']%s[colour='FFc6ff00']伤害
该伤害加成只取决于玩家面板的元素攻击力值
]]
    local v1 = DispFunc8_title(ATTR_THUNDER_MORTAL_DAMAGE_RATE)
    local v2 = string.format('%.0f%%', iGetAttr(ATTR_THUNDER_MORTAL_DAMAGE_RATE)*100+100  )
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_Hongjixiao(arg) --虹极效伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hongjizhi']虹之极效伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带虹属性伤害产生极致时，造成额外的[colour='FFFFFFFF']%s[colour='FFc6ff00']伤害
该伤害加成只取决于玩家面板的元素攻击力值
]]
    local v1 = DispFunc8_title(ATTR_RAINBOW_MORTAL_DAMAGE_RATE)
    local v2 = string.format('%.0f%%', iGetAttr(ATTR_RAINBOW_MORTAL_DAMAGE_RATE)*100+100  )
    setTips(arg, fmt, v1,sm(v2))
    return true
end
function Attributes_Anjixiao(arg) --闇极效伤害
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:anjizhi']闇之极效伤害   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使你的招式在附带闇属性伤害产生极致时，造成额外的[colour='FFFFFFFF']%s[colour='FFc6ff00']伤害
该伤害加成只取决于玩家面板的元素攻击力值
]]
    local v1 = DispFunc8_title(ATTR_DARK_MORTAL_DAMAGE_RATE)
    local v2 = string.format('%.0f%%', iGetAttr(ATTR_DARK_MORTAL_DAMAGE_RATE)*100+100  )
    setTips(arg, fmt, v1,sm(v2))
    return true
end

--极效伤害抵抗
-----------------------------------------------------------------
function Attributes_Shuijixiaokang(arg) --水极效伤害抵抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shuijizhi']水极效伤害抵抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到水属性极效伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_WATER_MORTAL_SUB_DAMAGE_RATE,0)
    local v2 = DispFunc5(ATTR_WATER_MORTAL_SUB_DAMAGE_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Huojixiaokang(arg) --火极效伤害抵抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huojizhi']火极效伤害抵抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到火属性极效伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_FIRE_MORTAL_SUB_DAMAGE_RATE,0)
    local v2 = DispFunc5(ATTR_FIRE_MORTAL_SUB_DAMAGE_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Fengjixiaokang(arg) --风极效伤害抵抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:fengjizhi']风极效伤害抵抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到风属性极效伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_WIND_MORTAL_SUB_DAMAGE_RATE,0)
    local v2 = DispFunc5(ATTR_WIND_MORTAL_SUB_DAMAGE_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Leijixiaokang(arg) --雷极效伤害抵抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:leijizhi']雷极效伤害抵抗  [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到雷属性极效伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_THUNDER_MORTAL_SUB_DAMAGE_RATE,0)
    local v2 = DispFunc5(ATTR_THUNDER_MORTAL_SUB_DAMAGE_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Hongjixiaokang(arg) --虹极效伤害抵抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hongjizhi']虹极效伤害抵抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到虹属性极效伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_RAINBOW_MORTAL_SUB_DAMAGE_RATE,0)
    local v2 = DispFunc5(ATTR_RAINBOW_MORTAL_SUB_DAMAGE_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end
function Attributes_Anjixiaokang(arg) --闇极效伤害抵抗
    local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:anjizhi']闇极效伤害抵抗   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']降低受到闇属性极效伤害[colour='FFFFFFFF']%s[colour='FFc6ff00']
]]
    local v1 = DispFunc5_title(ATTR_DARK_MORTAL_SUB_DAMAGE_RATE,0)
    local v2 = DispFunc5(ATTR_DARK_MORTAL_SUB_DAMAGE_RATE,0)
    setTips(arg, fmt, v1,v2)
    return true
end

--特效触发值
function Attributes_Shuichufa(arg)  --水特效触发值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shui']水特效触发累积值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使用水属性招式，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发水元素封穴

水元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_WATER_SPECIAL_TRIG_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_WATER_SPECIAL_TRIG_POINT )
    setTips(arg, fmt, v1, v2)
    return true
end

function Attributes_Huochufa(arg)  --火特效触发值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huo']火特效触发累积值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使用火属性招式，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发火元素封穴

火元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_FIRE_SPECIAL_TRIG_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_FIRE_SPECIAL_TRIG_POINT )
    setTips(arg, fmt, v1, v2)
    return true
end

function Attributes_Fengchufa(arg)  --风特效触发值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:feng']风特效触发累积值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使用风属性招式，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发风元素封穴

风元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_WIND_SPECIAL_TRIG_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_WIND_SPECIAL_TRIG_POINT )
    setTips(arg, fmt, v1, v2)
    return true
end

function Attributes_Leichufa(arg)  --雷特效触发值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:lei']雷特效触发累积值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使用雷属性招式，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发雷元素封穴

雷元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_THUNDER_SPECIAL_TRIG_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_THUNDER_SPECIAL_TRIG_POINT )
    setTips(arg, fmt, v1, v2)
    return true
end

function Attributes_Hongchufa(arg)  --虹特效触发值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hong']虹特效触发累积值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使用虹属性招式，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发虹元素封穴，持续2秒

虹元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_RAINBOW_SPECIAL_TRIG_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_RAINBOW_SPECIAL_TRIG_POINT )
    setTips(arg, fmt, v1, v2)
    return true
end

function Attributes_Anchufa(arg)  --闇特效触发值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:an']闇特效触发累积值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']使用闇属性招式，有[colour='FFFFFFFF']%s[colour='FFc6ff00']的几率触发闇元素封穴，持续2秒

闇元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_DARK_SPECIAL_TRIG_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_DARK_SPECIAL_TRIG_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

--特效触发抵抗值
function Attributes_Shuichufadikang(arg)  --水特效触发抵抗值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:shui']水特效触发抵抗值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']被水属性招式攻击时，触发水元素封穴的几率减少[colour='FFFFFFFF']%s

[colour='FFc6ff00']水元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_WATER_SPECIAL_TRIG_ANTI_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_WATER_SPECIAL_TRIG_ANTI_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

function Attributes_Huochufadikang(arg)  --火特效触发抵抗值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:huo']火特效触发抵抗值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']被火属性招式攻击时，触发火元素封穴的几率减少[colour='FFFFFFFF']%s

[colour='FFc6ff00']火元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_FIRE_SPECIAL_TRIG_ANTI_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_FIRE_SPECIAL_TRIG_ANTI_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

function Attributes_Fengchufadikang(arg)  --风特效触发抵抗值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:feng']风特效触发抵抗值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']被风属性招式攻击时，触发风元素封穴的几率减少[colour='FFFFFFFF']%s

[colour='FFc6ff00']风元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_WIND_SPECIAL_TRIG_ANTI_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_WIND_SPECIAL_TRIG_ANTI_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

function Attributes_Leichufadikang(arg)  --雷特效触发抵抗值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:lei']雷特效触发抵抗值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']被雷属性招式攻击时，触发雷元素封穴的几率减少[colour='FFFFFFFF']%s

[colour='FFc6ff00']雷元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_THUNDER_SPECIAL_TRIG_ANTI_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_THUNDER_SPECIAL_TRIG_ANTI_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

function Attributes_Hongchufadikang(arg)  --虹特效触发抵抗值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:hong']虹特效触发抵抗值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']被虹属性招式攻击时，触发虹元素封穴的几率减少[colour='FFFFFFFF']%s

[colour='FFc6ff00']虹元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_RAINBOW_SPECIAL_TRIG_ANTI_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_RAINBOW_SPECIAL_TRIG_ANTI_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

function Attributes_Anchufadikang(arg)  --闇特效触发抵抗值
	local fmt = 
[[[font='MSYH-10'][image='set:Total_Playeravatar image:an']闇特效触发抵抗值   [colour='FFFFFFFF']%s

[font='MSYH-9'][colour='FFc6ff00']被闇属性招式攻击时，触发闇元素封穴的几率减少[colour='FFFFFFFF']%s

[colour='FFc6ff00']闇元素封穴：使受击者不能移动并且不能释放技能，持续2秒
]]
    local v1 = DispFunc1_title(ATTR_DARK_SPECIAL_TRIG_ANTI_POINT)
	 --累计值转几率 0.03  --buff modify 0.1
    local v2 = DispFunc9(ATTR_DARK_SPECIAL_TRIG_ANTI_POINT )
    setTips(arg, fmt, v1,v2)
    return true
end

function Cuilian_TipsHandler(arg)
		return true
end
-------------------------------------------------------------------------------
-- Entry point script
-------------------------------------------------------------------------------

lout('done loading attributes.lua')

 --[[
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
`=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
]]