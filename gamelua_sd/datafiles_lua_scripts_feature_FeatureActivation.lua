local modname = ...

module(modname, package.seeall)

local self = nil

function Init()
	self = _M
	self.bEnableFunction = true
	-----------------------主界面菜单，需要重新排序布局的一些按钮控件---------------------------------------------------
	self.MainMenus = {
		DisplayOrder = {
			'Avatar', 'Package', 'Quest', 'Skill', 'Kungfu', 'Gallery', 'Guild', 'Craft', 'Social', 'Setting', 'Wardrobe', 'Mall'
		}, 
		FeatureMapping = {
			['Gallery']  = { FEATURE_ACHIEVEMENT_SYSTEM, FEATURE_ATLAS_SYSTEM, FEATURE_CIMELIA_SYSTEM, FEATURE_MEDAL_SYSTEM, FEATURE_REPUTE_SYSTEM},
			['Craft']    = { FEATURE_LIFE_SKILL} ,
			['Kungfu']   = { FEATURE_SKILL_TALENT, FEATURE_CRAFT_TALENT} ,  
		},
		ButtonMapping  = { 
			['Avatar']   = 'Root/MainMenu/ProfileBtn',  ['Package'] = 'Root/MainMenu/PackageBtn', ['Quest'] = 'Root/MainMenu/MissionBtn', 
			['Gallery']  = 'Root/MainMenu/Gallery',     ['Guild']   = 'Root/MainMenu/Family',     ['Craft'] = 'Root/MainMenu/Life',
			['Social']   = 'Root/MainMenu/GroupBtn',    ['Setting'] = 'Root/MainMenu/SetupBtn', 
			['Skill']    = 'MainMenu_FunctionAndEXP/SkillBtnFrame/Skill',
			['Kungfu']   = 'MainMenu_FunctionAndEXP/SkillBtnFrame/Kungfu', 
			--['Auction']  = 'MainMenu_FunctionAndEXP/ShopBtnFrame/AuctionHouse', 
			['Wardrobe'] = 'MainMenu_FunctionAndEXP/ShopBtnFrame/Wardrobe', 
			['Mall']     = 'MainMenu_FunctionAndEXP/ShopBtnFrame/MallFrame',
		},
		SubFrameMapping = {
			['Setting']   = 'MainMenu_FunctionAndEXP/System',
			['Social']    = 'MainMenu_FunctionAndEXP/ESCMenu/SocialBtnFrame',
		},
	}
	--------------------分散在各个窗口上的系统功能的按钮，位置固定，不需要重新布局--------------------------------------
	self.FixedSystems = {
		Items = {
			'Trial', 'RandomDungeon', 'BattleArenaQueue', 'Rank', 'Fatigue',
		},
		FeatureMapping = {
			['Trial']             = { FEATURE_TRAINING_PLACE },
			['RandomDungeon']     = { FEATURE_RANDOM_DUNGON  } ,
			['BattleArenaQueue']  = { FEATURE_BATTLE_QUEUE   } , 
			['Rank']              = { FEATURE_GAME_RANK      } , 
			['Fatigue']           = { FEATURE_FATIGUE_DEGREE } ,
		},
		ButtonMapping  = {
			['Trial']             = 'Map_LD/Trial',
			['RandomDungeon']     = 'Map_LD/RandomLevelQueue' ,
			['BattleArenaQueue']  = 'Map_LD/PvpQueue' ,
			['Rank']              = 'Titan/BtnFrame/Ranking' ,
			['Fatigue']           = 'MainMenu_FunctionAndEXP/Frame/ENERGE_BG',
		},
	}
	------------------带有UI快捷键的系统功能对应的窗口------------------------------------------------------------------
	self.FrameWindows = {
		MainMenuMapping = {
			['Journey'] = 'Gallery',
			['LifeSkillMainWindow'] = 'Craft',
			['KungfuNewWindow'] = 'Kungfu',
		},
		FeatureMapping = {
			-- 暂时还没用
			--['xx'] = {FEATURE_LIFE_SKILL}
		},
	}
end

function UnInit()
	self = nil
end

function OnEnterScene()
	self.featureOpenConn = gameEventMgr:subscribeEvent(GameEvents.FeatureOpened,  HandleFeatureOpened)
	self.featureSyncConn = gameEventMgr:subscribeEvent(GameEvents.FeatureSync,    HandleFeatureSync)
end

function OnLeaveScene()
	if self.featureOpenConn then
		self.featureOpenConn:disconnect()
		self.featureOpenConn = nil
	end
	if self.featureSyncConn then
		self.featureSyncConn:disconnect()
		self.featureSyncConn = nil
	end
end

function IsAnyFeatureForUIOpened(frameWindowName)
	local menuName = self.FrameWindows.MainMenuMapping[frameWindowName]
	if menuName then
		return MainMenu.IsAnyFeatureOpenedInMainmenu(menuName)
	end
	
	local features = self.FrameWindows.FeatureMapping[frameWindowName]
	if features then
		local bAnyOpened = false
		for _, feature in ipairs(features) do 
			if SD.IsSystemFeatureOpened(feature) then
				bAnyOpened = true
				break
			end
		end
		return bAnyOpened
	end
	
	-- Not in MainMenuMapping and FeatureMapping, cannot handle it, leaving it true.
	return true
end

function Test()
	gameEventMgr:fireEvent(GameEvents.FeatureSync, SD.LuaNoArgEvent())

end

MainMenu={}
function MainMenu.GetFeatureForMainMenu(menu)
	local features = self.MainMenus.FeatureMapping[menu]
	return features
end

function MainMenu.IsMainMenuContainsFeature(menu)
	local features = MainMenu.GetFeatureForMainMenu(menu)
	return features ~= nil
end

function MainMenu.IsAnyFeatureOpenedInMainmenu(menu)
	local features = MainMenu.GetFeatureForMainMenu(menu)
	if not features then return false end
	for _, feature in ipairs(features) do 
		if SD.IsSystemFeatureOpened(feature) then
			return true
		end
	end
	return false
end

function MainMenu.GetRelayoutMenus()
	local menus = {}
	for i = #self.MainMenus.DisplayOrder, 1, -1 do
		local menu = self.MainMenus.DisplayOrder[i]
		if not MainMenu.IsMainMenuContainsFeature(menu) or MainMenu.IsAnyFeatureOpenedInMainmenu(menu) then
			--lout(menu)
			table.insert(menus, menu)
		end
	end
	return menus
end

function MainMenu.RelayoutMainMenus()
	for i, menu in ipairs(self.MainMenus.DisplayOrder) do
		local menuBtnName = self.MainMenus.ButtonMapping[menu]
		local pMenuBtn = InitWindowPtr(menuBtnName)
		if pMenuBtn then pMenuBtn:hide() end
	end
	
	local menus = MainMenu.GetRelayoutMenus()
	local offset = 0
	for i, menu in ipairs(menus) do 
		local menuBtnName = self.MainMenus.ButtonMapping[menu]
		local menuSubFrameName = self.MainMenus.SubFrameMapping[menu]
		offset = MainMenu.LayoutMainMenuItem(menuBtnName, menuSubFrameName, offset)
	end
	
end

function MainMenu.LayoutMainMenuItem(menuBtnName, menuSubFrameName, positionX)
	local btnWidth = nil
	if menuBtnName then
		local pMenuBtn = InitWindowPtr(menuBtnName)
		if pMenuBtn then 
			lout('FeatureActivation: Show MainMenu ' .. menuBtnName)
			pMenuBtn:setProperty('HorizontalAlignment','Right')
			pMenuBtn:setXPosition(CEGUI.UDim(0, positionX))
			btnWidth = pMenuBtn:getPixelSize().width - 1 -- 1 pixel overlap
			pMenuBtn:show()
			--lout(menuBtnName)
		end
	end
	
	if btnWidth and menuSubFrameName  then 
		local pMenuFrame = InitWindowPtr(menuSubFrameName)
		if pMenuFrame then 
			pMenuFrame:setXPosition(CEGUI.UDim(0, positionX))
		end
	end
	if btnWidth then
		positionX = positionX - btnWidth
	end
	return positionX
end

function MainMenu.HandleFeatureOpened(feature)
	MainMenu.RelayoutMainMenus()
end

function MainMenu.HandleFeatureSync()
	MainMenu.RelayoutMainMenus()
end

---------FixedSystem------------------------------------------------------------------------------------------------------
FixedSystem={}
function FixedSystem.GetFeatureForSystem(system)
	local features = self.FixedSystems.FeatureMapping[system]
	return features
end

function FixedSystem.IsAnyFeatureOpened(system)
	local features = FixedSystem.GetFeatureForSystem(system)
	if not features then return false end
	for _, feature in ipairs(features) do 
		if SD.IsSystemFeatureOpened(feature) then
			return true
		end
	end
	return false
end

function FixedSystem.EnableSystemEntrance(system, bEnable)
	local entranceBtnName = self.FixedSystems.ButtonMapping[system]
	if entranceBtnName then
		local pEntranceBtn = InitWindowPtr(entranceBtnName)
		if pEntranceBtn then
			pEntranceBtn:setVisible(bEnable)
		end
	end
end

function FixedSystem.HandleFeatureOpened(feature)
	for _, system in ipairs(self.FixedSystems.Items) do 
		local bShouldEnabled =  FixedSystem.IsAnyFeatureOpened(system)
		FixedSystem.EnableSystemEntrance(system, bShouldEnabled)
	end
end

function FixedSystem.HandleFeatureSync()
	for _, system in ipairs(self.FixedSystems.Items) do 
		local bShouldEnabled =  FixedSystem.IsAnyFeatureOpened(system)
		FixedSystem.EnableSystemEntrance(system, bShouldEnabled)
	end
end
------------------------------------------------------------------------------------------------------------------------
function HandleFeatureOpened(args)
	lout('FeatureActivation: HandleFeatureOpened')
	if not self.bEnableFunction then return end
	
	local e = tolua.cast(evtArgs, 'SD::LuaUInt32ArgEvent')
	local newFeature = e.dwValue
	
	MainMenu.HandleFeatureOpened(newFeature)
	FixedSystem.HandleFeatureOpened(newFeature)
	-- ....
end

function HandleFeatureSync(args)
	lout('FeatureActivation: HandleFeatureSync')
	if not self.bEnableFunction then return end
	
	MainMenu.HandleFeatureSync()
	FixedSystem.HandleFeatureSync()
	-- ....
end
