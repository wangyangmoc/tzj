local modname = ...
lout('start loading '.. modname)

module(modname, package.seeall)

function GetUnit()
	local playerEnt = Utility.GetPlayerEntity()
	return playerEnt:GetUnit()
end

function GetProfession()
	local unit = GetUnit()
	return unit:GetProfession()
end

function GetSex()
	local playerEnt = Utility.GetPlayerEntity()
	return playerEnt:GetSex()
end

function GetLevel()
	local unit = GetUnit()
	return unit:GetLevel()
end