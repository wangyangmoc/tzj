-- GuideFrame

local layoutName = 'GuideFrame.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	lout('GuideFrame:Init()')
	self.pWindow    = self:GetWindow()
	self.pTextTpl   = self:GetChildWindow('GuideFrame/Text')
	self.pTextTpl:setProperty('TextColours', 'FFE5B70C')
	
	self.guideQueue = {}
	self.bFadingOut = false
	self.fFadingTime = 0.0
	self.fFadingTimeout = 1.0
	self.currentGuideID = nil
	
	self:Show()
	self.pWindow:hide()
end

function UnInit(self)
	lout('GuideFrame:UnInit()')

end

function Subscribe(self)
	self:SubscribeUIEvents()
end

function SubscribeUIEvents(self)
	self.pWindow:subscribeEvent('WindowUpdate',    self._OnUpdate,     self)
	self.pWindow:setUpdateMode(CEGUI.WUM_ALWAYS)
end

function AddNewGuide(self, nID)
	lout('GuideFrame: AddNewGuide ID: ' .. nID)
	table.insert(self.guideQueue, {ID = nID, Displayed = false})
	
end

function SetGuideComplete(self, nID)
	lout('GuideFrame: SetGuideComplete ID: ' .. nID)
	if self.currentGuideID and self.currentGuideID == nID then
		self:_StartFadeOut()
	end
	
end

function SetGuideProgressed(self, nID)
	
end

function _StartFadeOut(self)
	self.pTextTpl:setProperty('TextColours', '9F00FF00')
	self:_UpdateWindowAlpha(1.0)

	self.fFadingTime = 0
	self.bFadingOut = true
end

function _UpdateFadeOut(self, fTime)
	if self.bFadingOut then
		self:_UpdateWindowAlpha(1 - self.fFadingTime)
		fTime = Clamp(fTime, 0, 0.3)
		if self.fFadingTime > self.fFadingTimeout then
			self:_ResetCurrentGuide()
			self.bFadingOut = false
		end
		self.fFadingTime = self.fFadingTime + fTime
	end
end

function _OnUpdate(self, evtArgs)
	--lout('GuideFrame:_OnUpdate()')
	local e = tolua.cast(evtArgs, 'CEGUI::UpdateEventArgs')
	local fTime = e.lastFrameTime

	guideMgr.Update(fTime)
	self:_UpdateFadeOut(fTime)
	self:_TryToShrinkQueue()
	
	if not self.currentGuideID then
		local queueIndex, guideItem = self:_FindCandidateGuide()
		if queueIndex and guideItem then
			lout('GuideFrame: Find candidate guide, ID: ' .. guideItem.nID .. ', Display it')
			self:_SetCurrentGuide(queueIndex, guideItem)
		end
	end
end

function _TryToShrinkQueue(self)
	if table.getn(self.guideQueue) > 0 then
		local newQueue = {}
		for _, v in ipairs(self.guideQueue) do
			local guideItem = guideMgr.GetGuideInstance(v.ID)
			if guideItem:HasCompleted() then
				v.Displayed = true
			end
			if not v.Displayed then
				table.insert(newQueue, v)
			end
		end
		if table.getn(self.guideQueue) ~= table.getn(newQueue) then
			lout('GuideFrame: Shrink queue, size change: '..table.getn(self.guideQueue)..'->'..table.getn(newQueue))
			self.guideQueue = newQueue
		end
	end
end

function _FindCandidateGuide(self)
	local candidateIndex = nil
	local candidateGuide = nil
	local minDiffTime = 0
	
	for k, v in ipairs(self.guideQueue) do
		local guideItem = guideMgr.GetGuideInstance(v.ID)
		local guideCfg = guideItem.guideCfg
		if not v.Displayed then
			local nDiffTime = guideCfg.DelayShowTime - guideItem.createdTime
			if nDiffTime < minDiffTime then
				minDiffTime = nDiffTime
				candidateIndex = k
				candidateGuide = guideItem
			end
			break
		end
	end
	
	return candidateIndex, candidateGuide
end

function _ResetCurrentGuide(self)
	self.pTextTpl:setText('')
	self.pTextTpl:setProperty('TextColours', 'FFE5B70C')
	self.pWindow:hide()
	
	self.currentGuideID = nil
end

function _SetCurrentGuide(self, nIndex, guideItem)
	self.guideQueue[nIndex].Displayed = true
	self.currentGuideID = guideItem.nID
	
	self:_SetGuideItemAttr(guideItem)
	
	self.pWindow:show()
	self:_UpdateWindowAlpha(1.0)
end

function _SetGuideItemAttr(self, guideItem)
	guideItem.displaying = true
	
	local guideCfg = guideItem.guideCfg
	
	local strText = nil
	if not guideCfg.Text then
		local curOperationMode = SD.GetOperationMode()
		if curOperationMode == SD.HS_OPERATION_ZERO then
			strText = guideCfg.Mode1Text
		elseif curOperationMode == SD.HS_OPERATION_USUAL then
			strText = guideCfg.Mode2Text
		else
			strText = guideCfg.Mode3Text
		end
	else
		strText = guideCfg.Text
	end
	strText, _ = string.gsub(strText, "【(%w+)】", " [image='set:Total_Tutorial image:%1'] ")
	self.pTextTpl:setText(strText)
end

function _UpdateWindowAlpha(self, fAlpha)
	self.pWindow:setAlpha(fAlpha)
	self.pTextTpl:setAlpha(fAlpha)
end
