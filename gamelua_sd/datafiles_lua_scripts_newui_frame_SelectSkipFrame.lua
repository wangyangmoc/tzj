-- SelectSkipFrame

local layoutName = 'Tutorial_Skip.layout'

ui.defineFrame(..., layoutName)
function Init(self)
	self.btnMode1 = self:GetChildWindow('Tutorial_Skip/ModBtn1')
	self.btnMode2 = self:GetChildWindow('Tutorial_Skip/ModBtn2')
	
	self.isIn = false
	self.completed = false
	self.isSkipped = false
end

function UnInit(self)
	lout('SelectSkipFrame:UnInit()')
end

function Subscribe(self)
	self:SubscribeUIEvents()
end

function SubscribeUIEvents(self)
	lout('SelectSkipFrame:SubscribeEvents()')
	self.btnMode1:subscribeEvent('Clicked', self.OnClickModBtn1, self)
	self.btnMode2:subscribeEvent('Clicked', self.OnClickModBtn2, self)
end

function Enter(self)
	if not self.isIn then
		self.isIn = true
		self:Show()
	end
end

function Leave(self)
	if self.isIn then
		self:Hide()
		self.isIn = false
	end
end

function HasCompleted(self)
	return self.completed
end

function IsSkipped(self)
	return self.isSkipped
end

function OnClickModBtn1(self, evtArgs)
	lout('SelectSkipFrame:OnClickModBtn1() Continue Tutorial')
	self.isSkipped = false
	self.completed = true
end

function OnClickModBtn2(self, evtArgs)
	lout('SelectSkipFrame:OnClickModBtn2() Skip Tutorial')
	self.isSkipped = true
	self.completed = true
end

