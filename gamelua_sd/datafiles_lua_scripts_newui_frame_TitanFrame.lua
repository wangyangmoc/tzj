-- TitanFrame 

local layoutName = 'Titan.layout'

ui.defineFrame(..., layoutName)

local TITAN_GUIDE_INDEX = 15

local ESCAPE_JAM_PROMPT = '正在脱离卡死'
local ESCAPE_JAM_WAIT_TIME = 7.0 -- seconds

function Init(self)
	lout('TitanFrame:Init()')
	self.pWindow = self:GetWindow()
	self.updateTimeout = 0
	self.gcTimeout = 0
	self.bContainerLocked = true
	self.bNeedNotifyGuide = false
	
	self.pContainer = self:GetChildWindow('Titan/TextInfoFrame')
	self.pUnusedTpl = self:GetChildWindow('Titan/TextInfoFrame/Container')
	self.pTextTpl   = self:GetChildWindow('Titan/TextInfoFrame/TextTpl')
	self.pXingchen   = self:GetChildWindow('Titan/TextInfoFrame/Xingchen')
	self.pContainer:removeChildWindow(self.pTextTpl)
	self.pContainer:removeChildWindow(self.pUnusedTpl)
	self.pContainer:removeChildWindow(self.pXingchen)
	
	self.externalUIEvents = {}
	self.gameEvents = {}
	
	self.entryCfgTable = {}
	self.entryDict     = {}
	
	self.activeEntries = {}  -- it is an array list
	self.activeNumDirty = false
	
	-- 下面在小地图上
	self.pBtnEscJam   = InitWindowPtr('MainMenu_Buttons/ButtonFrame/Template_6')
	self.pChannelLabel  = InitWindowPtr('Titan/BtnFrame/ChannelInfo')
	self.pBtnChannel    = InitWindowPtr('Titan/BtnFrame/ChannelBtn')
	--self.pImgMail     = self:GetChildWindow('Titan/Mail')
	--self.pBtnSafeLock = self:GetChildWindow('Titan/BtnFrame/SafeLock')
	--self.pBtnBug      = self:GetChildWindow('Titan/BtnFrame/BugSubmit')
	--self.pBtnHelp     = self:GetChildWindow('Titan/BtnFrame/Helper')
	--self.pGTController  = self:GetChildWindow('Titan/BtnFrame/GT')
	--self.pRankBtn       = self:GetChildWindow('Titan/BtnFrame/Ranking')
	--self.pPerfectRadio  = self:GetChildWindow('Titan/BtnFrame/PerfectRadio')
		
	self.pEscJamDelegate = nil
	
	--self.pImgMail:hide()
		

	self:_InitEntryCfgTable()
	self:_InitEntryDict()

end

function UnInit(self)
	lout('TitanSetFrame:UnInit()')
	-- Remove game events
	for i, v in ipairs(self.gameEvents) do
		v:disconnect()
	end
	self.gameEvents = {}
	
	-- Remove External UI Events
	for i, v in ipairs(self.externalUIEvents) do 
		v:disconnect()
	end
	self.externalUIEvents = {}
	
	-- Clear cloned children
	self.pContainer:addChildWindow(self.pTextTpl)
	self.pContainer:addChildWindow(self.pUnusedTpl)
	if not self.pXingchen:getParent() then
		self.pContainer:addChildWindow(self.pXingchen)
	end
	
	for k, v in pairs(self.entryDict) do
		--lout('destroyWindow ' .. v.pWindow:getName():c_str())
		wndMgr:destroyWindow(v.pWindow)
	end	
	
	-- Destroy c++ helper
	self.pHelper:delete()
	self.pHelper = nil
end

function Subscribe(self)
	self:SubscribeUIEvents()
	self:SubscribeGameEvents()
	
		
	self.pHelper = CTitanWindowHelper:new()
	self.pHelper:Init(self, self.pWindow)
	
	self:HandleSettingChanged()
end

function SubscribeUIEvents(self)
	self.pWindow:subscribeEvent('MouseEntersArea', self._OnMouseEnter,      self)
	self.pWindow:subscribeEvent('MouseLeavesArea', self._OnMouseLeave,      self)
	
	self.pContainer:subscribeEvent('WindowUpdate', self._OnContainerUpdate, self)
	local pBtnSet = self:GetChildWindow('Titan/SetBtn')
	pBtnSet:subscribeEvent('Clicked',              self._OnClickSetBtn,     self)
	
	local conn = nil
	conn = self.pBtnEscJam:subscribeEvent('Clicked',      self._OnClickEscJam,     self)
	table.insert(self.externalUIEvents, conn)
	
	conn = self.pBtnChannel:subscribeEvent('Clicked',     self._OnClickChannel,    self)
	table.insert(self.externalUIEvents, conn)
	
	
	--self.pBtnSafeLock:subscribeEvent('Clicked',    self._OnClickSafeLock,   self)
	--self.pBtnBug:subscribeEvent('Clicked',         self._OnClickBug,        self)
	--self.pBtnHelp:subscribeEvent('Clicked',        self._OnClickHelp,       self)
	--self.pRankBtn:subscribeEvent('Clicked',        self._OnClickRankBtn,    self)
	--self.pPerfectRadio:subscribeEvent('Clicked',   self._OnClickPerfRadio,  self)
	--self.pGTController:subscribeEvent('Clicked',   self._OnClickOpenGT, self)
	
end

function SubscribeGameEvents(self)
	if not gameEventMgr then 
		return
	end
	
	local conn = nil
	conn = gameEventMgr:subscribeEvent(GameEvents.EscapeJamWaitEnded, self._OnEscJamWaitEnd, self)
	table.insert(self.gameEvents, conn)
	
	--conn = gameEventMgr:subscribeEvent(GameEvents.NewEmailNotify,     self._OnNewMailNotify, self)
	--table.insert(self.gameEvents, conn)
	
	conn = gameEventMgr:subscribeEvent(GameEvents.ChannelInfoUpdate,  self._OnUpdateChannel, self)
	table.insert(self.gameEvents, conn)
	
	conn = gameEventMgr:subscribeEvent('Tutorial_Add',                     self._OnNewGuideAdd,   self)
	table.insert(self.gameEvents, conn)
end 

function ToggleContainerLocked(self)
	self.bContainerLocked = not self.bContainerLocked
	self:_SetContainerVisible(self.bContainerLocked)
	return self.bContainerLocked
end

function SubscribeEventForEntry(self, pWindow)
	if not pWindow then
		return
	end
	
	pWindow:subscribeEvent('MouseClick', self._OnClickEntry, self)
end

function OnEnterScene(self, oldSceneID, newSceneID)
    if SD.GenericFunc('IsInInstance', newSceneID) == 'false' then
		self.pBtnChannel:enable()
		self.pChannelLabel:setText('')
		self.pHelper:RequestChannelList(false)
	else
		self.pBtnChannel:disable()
		self.pChannelLabel:setText('')
	end
	self:Show()
end

function OnLeaveScene(self, oldSceneID)
	if self.pHelper.BreakEscapeJam then
		self.pHelper:BreakEscapeJam()
	end
end

function HandleSettingChanged(self)
	local options = self:_CollectActiveEntries()
	
	self.activeEntries = {}
	for i, v in ipairs(options) do
		local entry = self.entryDict[v]
		table.insert(self.activeEntries, entry)
	end
	
	self:Relayout(true)
end

function Relayout(self, bActiveNumDirty)
	--lout('TitanFrame:Relayout BEGIN')

	if bActiveNumDirty then
		lout('TitanFrame:Relayout, remove previous active list and add new active list')
		while self.pContainer:getChildCount() > 0 do
			local widget = self.pContainer:getChildAtIdx(0)
			self.pContainer:removeChildWindow(widget)
		end
		for i, v in ipairs(self.activeEntries) do
			self.pContainer:addChildWindow(v.pWindow)
		end
		self:RefreshInfo()
	else 
		--return
	end
	
	local max_put_index = 0
	
	local SPACE_WIDTH = 2
	local widthOffset = 0 -- start width offset
	local childNum = self.pContainer:getChildCount()
	local containerWidth = self.pContainer:getPixelSize().width
	local bContainerFull = false
	
	--lout('TitanFrame:Relayout Set Position, Child Num '  .. childNum)
	for i, v in ipairs(self.activeEntries) do
		local pWidget = v.pWindow
		
		if bContainerFull then
			pWidget:hide()
		else
			local widget_width = 0.0
			if pWidget == self.pXingchen then
				widget_width = pWidget:getPixelSize().width
			else
				widget_width = self.pHelper:GetTextWidth(pWidget)
				if widget_width <= 0.0 then
					lout('self.pHelper:GetTextWidth failed ' .. pWidget:getName():c_str())
					widget_width = 20
				end
			end
			
			max_put_index = i
			if widthOffset + SPACE_WIDTH + widget_width <= containerWidth then
				local new_x_pos = CEGUI.UDim(0, widthOffset)
				local new_width = CEGUI.UDim(0, widget_width)
				--lout(pWidget:getName():c_str() ..' widthOffset: ' .. widthOffset)
				pWidget:setWidth(new_width)
				pWidget:setXPosition(new_x_pos)
				
				widthOffset = new_x_pos.offset + widget_width + SPACE_WIDTH
				pWidget:show()
			else
				bContainerFull = true
				pWidget:hide()
			end
		end
	end
	
	--lout('TitanFrame:Relayout END, max_put_index is ' .. max_put_index)
end

function RefreshInfo(self)
	--lout('TitanFrame:RefreshInfo...')
	for i, v in ipairs(self.activeEntries) do
	
		local config = self.entryCfgTable[v.Name]
		if v.pWindow and config.DisplayFunc then
		
			local RefreshFunc = config.DisplayFunc 
			RefreshFunc(config, v.pWindow)
		end	
	end	
end

function _OnMouseEnter(self, evtArgs)
	if not self.bContainerLocked then
		lout('_OnMouseEnter')
		self:_SetContainerVisible(true)
	end
end

function _OnMouseLeave(self, evtArgs)
	if not self.bContainerLocked then
		lout('_OnMouseLeave')
		self:_SetContainerVisible(false)
	end
end

local UPDATE_TIME_INTERVAL = 1.0 -- seconds
local GC_TIME_INTERVAL = 5.0 -- seconds
function _OnContainerUpdate(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::UpdateEventArgs')
	local fTime = e.lastFrameTime
	
	self.updateTimeout = self.updateTimeout + fTime
	self.gcTimeout = self.gcTimeout + fTime
	
	if self.gcTimeout > GC_TIME_INTERVAL then
		collectgarbage()
		self.gcTimeout = 0
	end
	
	if self.updateTimeout > UPDATE_TIME_INTERVAL then
		self:_OnRefreshEvent()
		self.updateTimeout = 0
	end
end

function _OnRefreshEvent(self, evtArgs)
	--lout('TitanFrame:_OnRefreshEvent...')
	self:RefreshInfo()
	self:Relayout(false)
end

function _OnClickEntry(self, evtArgs)
	local e = tolua.cast(evtArgs, 'CEGUI::MouseEventArgs')
	if e.button ~= CEGUI.LeftButton then return end
	
	lout('TitanFrame:_OnClickEntry')
	local clickedEntry = nil
	for i, v in ipairs(self.activeEntries) do
		if e.window == v.pWindow then
			clickedEntry = v
			break
		end
	end
	if not clickedEntry then
		local win_name = e.window:getName():c_str()
		lout('TitanFrame:_OnClickEntry, can not find clickedEntry for '..win_name)
		return
	end
	
	local entryCfg = self.entryCfgTable[clickedEntry.Name]
	if not entryCfg then
		lout('TitanFrame:_OnClickEntry, can not find cfg for entry '..clickedEntry.Name)
		return
	end
	
	lout('TitanFrame:_OnClickEntry, Handle by entry ' .. clickedEntry.Name)
	if entryCfg.ClickHandler then
		local Handler = entryCfg.ClickHandler
		Handler(entryCfg, clickedEntry.pWindow)
		self:_TryNotifyGuideMgr()
	end
end

function _OnClickSetBtn(self, evtArgs)
	lout('TitanFrame:_OnClickSetBtn')
	if ui.TitanSetFrame then
		if ui.TitanSetFrame:IsVisible() then
			ui.TitanSetFrame:Hide()
		else
			ui.TitanSetFrame:Show()
		end
	end
	self:_TryNotifyGuideMgr()
end

function _OnClickOpenGT(self, evtArgs)
	self:_TryNotifyGuideMgr()
	self.pHelper:GTClientOpen()
end

function _OnClickSafeLock(self, evtArgs)
	self:_TryNotifyGuideMgr()
	self.pHelper:ShowProtectAssetsSettings()
end

function _OnClickBug(self, evtArgs)
	self:_TryNotifyGuideMgr()
	self.pHelper:ReportDefect()
end

function _OnClickHelp(self, evtArgs)
	self.pHelper:ShowHandBook()
	self:_TryNotifyGuideMgr()
end

function _OnClickChannel(self, evtArgs)
	self:_TryNotifyGuideMgr()
	self.pHelper:RequestChannelList(true)
end

function _OnClickRankBtn(self, evtArgs)
	self:_TryNotifyGuideMgr()
	self.pHelper:ShowRankWindow()
end

function _OnClickPerfRadio(self, evtArgs)
	self:_TryNotifyGuideMgr()
	self.pHelper:ShowPerectRadio()
end

function _OnClickEscJam(self, evtArgs)
	lout('TitanFrame:_OnClickEscJam')
	
	local unit = GetPlayersUnit()
	if not unit then
		lout('TitanFrame:_OnClickEscJam - GetPlayersUnit failed')
		return
	end
	if unit:IsDead() then
		lout('TitanFrame:_OnClickEscJam - Player is dead')
		return
	end
	
	self.pEscJamDelegate = CLuaComboMsgBoxDelegate:new()
	self.pEscJamDelegate.__CEGUI__EventSet__:subscribeEvent(
		self.pEscJamDelegate:ConfirmEventName():c_str(),
		self._DoEscJam,
		self)
		
	self.pEscJamDelegate.__CEGUI__EventSet__:subscribeEvent(
		self.pEscJamDelegate:CancelEventName():c_str(),
		self._CancelEscJam,
		self)
	
	--lout('Show Msgbox')
	UiUtility.ShowComboMsgBox(
		Utility.MultiByteToUnicode('messagebox'),
		CEGUI.String('确定要脱离卡死吗？'),
		self.pEscJamDelegate,
		CEGUI.String('TitanEscJam'))

	--
	self:_TryNotifyGuideMgr()
	
	self.pBtnEscJam:disable()
end

function _OnEscJamWaitEnd(self, evtArgs)
	lout('TitanFrame: _OnEscJamWaitEnd')
	
	self.pBtnEscJam:enable()
end

function _OnNewMailNotify(self, evtArgs)
	lout('TitanFrame: _OnNewMailNotify')
	local e = tolua.cast(evtArgs, 'SD::LuaInt32ArgEvent')
	if e.nValue == 0 then
		self.pImgMail:hide()
	else
		self.pImgMail:show()
	end
end

function _OnUpdateChannel(self, evtArgs)
	local e = tolua.cast(evtArgs, 'SD::LuaCEGUIStringArgEvent')
	lout('TitanFrame: _OnUpdateChannel, '.. e.strText:c_str())
	self.pChannelLabel:setText(e.strText:c_str())
end

function _OnNewGuideAdd(self, evtArgs)
	local arg = tolua.cast(evtArgs, 'SD::LuaInt32ArgEvent')
	local guide_id = arg.nValue
	if guide_id ~= TITAN_GUIDE_INDEX then
		return
	end
	self.bNeedNotifyGuide = true
end

-----------------------------------------------------
function _DoEscJam(self, evtArgs)
	local strPrefix = CEGUI.String:new_local(ESCAPE_JAM_PROMPT)
	local fWaitTime = ESCAPE_JAM_WAIT_TIME  -- waiting time
	self.pHelper:StartWaitingEscJam(strPrefix, fWaitTime)
	
	-- The delegate object will be deleted by ComboMessageBox
	self.pEscJamDelegate = nil
end

function _CancelEscJam(self, evtArgs)
	self.pBtnEscJam:enable()
	-- The delegate object will be deleted by ComboMessageBox
	self.pEscJamDelegate = nil
end

function _TryNotifyGuideMgr(self)
	if self.bNeedNotifyGuide and guideMgr then
		guideMgr.OnServerCompleteGuide(SD.LuaInt32ArgEvent(TITAN_GUIDE_INDEX))
		self.bNeedNotifyGuide = false
	end
end

function _SetContainerVisible(self, bVisible)
	local pBtnSet = self:GetChildWindow('Titan/SetBtn')
	local frameImage = bVisible and 'set:BG_Normal1 image:TitanBk_' or 
									'set:BG_Normal1 image:Empty'
	
	pBtnSet:setVisible(bVisible)
	self.pContainer:setVisible(bVisible)
	self.pWindow:setProperty('frameRight',  frameImage..'Right')
	self.pWindow:setProperty('frameMiddle', frameImage..'Middle')
	self.pWindow:setProperty('frameLeft',   frameImage..'Left')
end

function _CollectActiveEntries(self)
	if not ui.TitanSetFrame then
		lout('ui.TitanFrame:_CollectActiveEntries, ui.TitanSetFrame is nil')
		return {}
	end
	
	return ui.TitanSetFrame:GetCheckedOptionList()
end

function _GetEntryWindow(self, entryName)
	if entryName ~= 'Xingchen' then
		local newWinName = self.pTextTpl:getName():c_str() .. '_' .. entryName
		local pNewWin = self.pTextTpl:clone(newWinName)
	
		return pNewWin
	else
		return self.pXingchen
	end
end

function _InitEntryDict(self)
	lout('TitanFrame:_InitEntryDict')
	
	for k, v in pairs(self.entryCfgTable) do
		local newEntry = {
			Name   = k,
			pWindow = self:_GetEntryWindow(k),
		}
		self:SubscribeEventForEntry(newEntry.pWindow)
		
		self.entryDict[k] = newEntry
	end
end

function _InitEntryCfgTable(self)
	if not ui.TitanSetFrame then
		return
	end
	
	local cfgs = self.entryCfgTable
	local options = ui.TitanSetFrame:GetOptionList()
	for i, v in ipairs(options) do
		cfgs[v] = {Name = v}
	end
	
	local refreshFuncs = self:_InitRefreshFuncs()
	local handlerFuncs = self:_InitHandlerFuncs()
	for k, v in pairs(self.entryCfgTable) do
		v.DisplayFunc  = refreshFuncs[k]
		v.ClickHandler = handlerFuncs[k]
	end
	
end

function _InitRefreshFuncs(self)
	local refreshFuncs = {}
	local labelColor = "[colour='FFf9ecb9']"
	local valueColor = "[colour='FFFFFFFF']"
	-- Money
	function s_ShowMoney(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetMoneyString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' '..
				valueColor .. strTxt:c_str() ..
				" ")
	
	end
	refreshFuncs['Money'] = s_ShowMoney
	
	-- RMB
	function s_ShowRMB(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetYuanbaoString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' ' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['RMB'] = s_ShowRMB
	
	-- BindingRMB
	function s_ShowBindingRMB(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetBindYuanbaoString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' ' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['BindingRMB'] = s_ShowBindingRMB
	
	-- PVP Money
	function s_ShowPvPMoney(option, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetPVPMoneyString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 记功牌：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['PvPMoney'] = s_ShowPvPMoney
	
	-- PVP Money
	function s_ShowPvPKillCount(option, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetPVPKillCountString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 人头数：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['KillPlayer'] = s_ShowPvPKillCount

	-- Fatigue
	function s_ShowFatigue(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetFatigueString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 疲劳度：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['Fatigue'] = s_ShowFatigue

	-- JJC
	function s_ShowJJC(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetJJCPointString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 虎牌：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['JJC'] = s_ShowJJC
	
	-- BattleMoney
	function s_ShowBattleMoney(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetJJCPointString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 祝融石：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['BattleMoney'] = s_ShowBattleMoney

	-- Xingchen
	function s_ShowXingchen(config, pWindow)
		--local strTxt = ui.TitanFrame.pHelper:GetStarDustString()
		--pWindow:setText("[vert-alignment='centre']" .. 
		--		labelColor .. ' 星尘：' .. valueColor .. strTxt:c_str() ..
		--		" ")
	end
	refreshFuncs['Xingchen'] = s_ShowXingchen
	
	-- YouQingJianZheng
	function s_ShowYouQingJianZheng(config, pWindow)
		local strText = ''
		local strTipText = ''
		for i, name in ipairs(ItemHelper.YQJZ_List) do 
			local yqCfg = ItemHelper.YQJZ_Cfg[name]
			if yqCfg then
				local count = ItemHelper.GetMoneyTokenAmount(yqCfg.ItemID)
				strText = strText .. yqCfg.IconImage..tostring(count)
				strTipText = strTipText ..'\n[colour=\'FFFFFFFF\']' ..yqCfg.IconImage.. yqCfg.ItemName .. '：' .. '[colour="FF12C744"]' .. tostring(count)-- .. '今日可获取：'.. tostring(yqCfg.LimitPerDay)
								
			end
		end
		pWindow:setText("[vert-alignment='top']" .. 
				labelColor .. ' 友情之证：' .. valueColor .. strText ..
				" ")
		Utility.SetWindowTooltip(pWindow, strTipText)
	end
	refreshFuncs['YouQingJianZheng'] = s_ShowYouQingJianZheng
	
	-- SocialPoint
	function s_ShowSocialPoint(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetSocialPointString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 社交点：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['SocialPoint'] = s_ShowSocialPoint

	-- LovePoint
	function s_ShowLovePoint(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetLovePointString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 爱心点：'..strTxt:c_str() ..
				" ")
	end
	refreshFuncs['LovePoint'] = s_ShowLovePoint
	
	-- Package
	function s_ShowPackage(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetPackageString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 包裹：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['Package'] = s_ShowPackage
	
	-- Time
	function s_ShowTime(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetTimeString('%H:%M:%S')
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 当前时间：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['Time'] = s_ShowTime

	-- Delay
	function s_ShowDelay(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetPingString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 延迟：' .. valueColor .. strTxt:c_str() .. '毫秒' ..
				" ")
	end
	refreshFuncs['Delay'] = s_ShowDelay
	
	-- Fps
	function s_ShowFps(config, pWindow)
		local strTxt = ui.TitanFrame.pHelper:GetFpsString()
		pWindow:setText("[vert-alignment='centre']" .. 
				labelColor .. ' 帧数：' .. valueColor .. strTxt:c_str() ..
				" ")
	end
	refreshFuncs['Fps'] = s_ShowFps
	
	return refreshFuncs
end

function _InitHandlerFuncs(self)
	local handlers = {}
	
	-- Money
	function s_HandleClickMoney(config, pWindow)
		
	
	end
	handlers['Money'] = s_HandleClickMoney
	
	-- RMB
	function s_HandleClickRMB(config, pWindow)
		
	
	end
	handlers['RMB'] = s_HandleClickRMB
	
	-- BindingRMB
	function s_HandleClickBindingRMB(config, pWindow)
		
	
	end
	handlers['BindingRMB'] = s_HandleClickBindingRMB
	
	-- PVP Money
	function s_HandleClickPvPMoney(config, pWindow)
		
	
	end
	handlers['PvPMoney'] = s_HandleClickPvPMoney
	
	-- PVP Kill Count
	function s_HandleClickPvPKill(config, pWindow)
		
	
	end
	handlers['KillPlayer'] = s_HandleClickPvPKill

	-- Fatigue
	function s_HandleClickFatigue(config, pWindow)
		local pInst = SD.CFatigueWindow:GetFatigueWindow()
		if pInst then
			pInst:ToggleShowHide()
		end
	end
	handlers['Fatigue'] = s_HandleClickFatigue

	-- JJC
	function s_HandleClickJJC(config, pWindow)
		
	
	end
	handlers['JJC'] = s_HandleClickJJC
	
	-- BattleMoney
	function s_HandleClickBattleMoney(config, pWindow)
		
	
	end
	handlers['BattleMoney'] = s_HandleClickBattleMoney

	-- Xingchen
	function s_HandleClickXingchen(config, pWindow)
		
	
	end
	handlers['Xingchen'] = s_HandleClickXingchen
	
		-- YouQingJianZheng
	function s_HandleClickYouQingJianZheng(option)
		
	end
	handlers['YouQingJianZheng'] = s_HandleClickYouQingJianZheng
	
	-- SocialPoint
	function s_HandleClickSocialPoint(config, pWindow)
		
	
	end
	handlers['SocialPoint'] = s_HandleClickSocialPoint

	-- LovePoint
	function s_HandleClickLovePoint(config, pWindow)
		
	
	end
	handlers['LovePoint'] = s_HandleClickLovePoint
	
	-- Package
	function s_HandleClickPackage(config, pWindow)
		local pInst = CPackageWindow:GetSelf()
		if pInst then
			pInst:ToggleShowHide()
		end	
	end
	handlers['Package'] = s_HandleClickPackage
	
	-- Time
	function s_HandleClickTime(config, pWindow)
		
	
	end
	handlers['Time'] = s_HandleClickTime

	-- Delay
	function s_HandleClickDelay(config, pWindow)
		
	
	end
	handlers['Delay'] = s_HandleClickDelay
	
	-- Fps
	function s_HandleClickFps(config, pWindow)
		
	
	end
	handlers['Fps'] = s_HandleClickFps
	
	return handlers
end
