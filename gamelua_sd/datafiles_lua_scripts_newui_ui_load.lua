
local modname = ...

module(modname, package.seeall)

-- LoginStage Frames
local login_frame_modules = 
{
}
-- SelectCharStage Frames
local select_frame_modules = 
{
}
-- World Stage Windows
local world_frame_modules = 
{
	'TitanSetFrame', 'TitanFrame', 'BattleCountFrame', 
	create_in_time = 
	{
		'FullScreenVideoFrame', 
	},
	debug_usage = 
	{
		
	},
}

-- Tutorial Fames
local tutorial_frame_modules = 
{
	--'SelectModeFrame', 'SelectSkipFrame', 'CheckListFrame', 'TutorialVideoFrame',
	'SelectModeFrame', 'SelectSkipFrame', 'GuideFrame', 'TutorialVideoFrame',
	create_in_time = 
	{
		'UpIndicatorFrame', 'RightIndicatorFrame', 'DownIndicatorFrame', 'LeftIndicatorFrame'
	}
}


local function loadUI(frame_names)
	-- load frame modules
	modules.loadList(frame_names)
	
	-- create frame instances
	for i, v in ipairs(frame_names) do
		ui.createFrame(v)
	end
end

function unloadUI()
	ui.destroyFrames()
end

-- Load Login Stage Frames
function loadLoginUI()
	loadUI(login_frame_modules)
end
-- Load SelectChar Frames
function loadSelectCharUI()
	loadUI(select_frame_modules)
end

-- Load World Stage Frames
function loadWorldUI()
	loadUI(world_frame_modules)
	if SD.ENABLE_DEBUG_USAGE  then
		loadUI(world_frame_modules.debug_usage)
	end
	modules.loadList(world_frame_modules.create_in_time)
end

-- World UI EnterScene / Leave Scene
function WorldUI_OnEnterScene(oldSceneId, newSceneId)
	for i, v in ipairs(world_frame_modules) do
		local frame = ui[v]
		if frame and frame.OnEnterScene then
			frame:OnEnterScene(oldSceneId, newSceneId)
		end
	end
end	

function WorldUI_OnLeaveScene(oldSceneId)
	for i, v in ipairs(world_frame_modules) do
		local frame = ui[v]
		if frame and frame.OnLeaveScene then
			frame:OnLeaveScene(oldSceneId)
		end
	end
end	

-- Load Tutorial Frames
function loadTutorialUI()
	loadUI(tutorial_frame_modules)
	modules.loadList(tutorial_frame_modules.create_in_time)
end

-- UnLoad Tutorial Frames
function unLoadTutorialUI()
	lout('UnLoad TutorialUI')
	for i, v in ipairs(tutorial_frame_modules) do
		ui.destroyFrame(v)
	end
end




