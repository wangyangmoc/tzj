local modname = ...

module(modname, package.seeall)

local EVENT_TUTORIAL_STARTED         = "Tutorial_Start";
local EVENT_TUTORIAL_ADD_NEW         = "Tutorial_Add";
local EVENT_TUTORIAL_PROGRESS        = "Tutorial_Progress";
local EVENT_TUTORIAL_COMPLETE        = "Tutorial_Complete";
local EVENT_TUTORIAL_SERVER_COMPLETE = "Tutorial_ServerComplete";
local EVENT_TUTORIAL_SIPPED          = "Tutorial_Skipped";
local EVENT_TUTORIAL_SHOW_SP_BAR     = "Tutorial_ShowSpBar";
local EVENT_TUTORIAL_OPMODE_FIRST_SELECT = "Tutorial_FirstSlectOpMode";

local guide_name_list = {
	'Move',            'CameraControl',   'MouseRule',     'FNpcDlg',       -- 1*4
	'TaskSubmitGuide', 'TaskAcceptGuide', 'EquipGuide',    'DoodadGuide',   -- 2*4
	'LeftMouseAttack', 'EvadeAttack',     'TwoPhaseJump',  'QingGong',      -- 3*4
	'BreakScene',      'EvadeCircle',     'TitanGuide',    'Dazuo',         -- 4*4
	'LootGuide',       'MoDao',           'LeftJA',        'ActiveCimelia', -- 5*4 
	'UnlockNewBag',    'SkillUpgrade',    'LearnNewSkill', 'KeyboardSkill', -- 6*4
	'MedicineGuide',   'FirstWeapon',     'DungeonGate',   'FirstTalk',     -- 7*4
	'JASkill',         'ModeChange',      'Fishing',       'Identify',      -- 8*4
	'Strengthen',      'Evolution',       'Compound',      'Pharmacy',      -- 9*4
	'Cook',            'GuardVideoGuide',  'EmergencyEvadeVideoGuide',    'QinggongAttackVideoGuide',   -- 10*4
	'FinalAttackVideoGuide', 'StardustVideoGuide',        'MiniMapGuide', 'MiniMapGuide2', -- 11*4
	'FirstTalk2',      'DoodadGuide2',    'TalkShiJianYa', 'TalkZiWuGu',       -- 12*4
	'LearnNewSkill2',  'OperationMode',   'UseJaSkill',    'EquipWaiZhuang',   -- 13*4
	'DungeonChoose',   'DungeonReward',   'BaoBox',        'JumpPoint',        -- 14*4
	'TaskTrace',       'EquipDecompose',  'EquipDecompose2', 'JoinGuild',      -- 15*4
	'FishingPrepare',  'FishingLearn',    'FishingStart',  'FishingWait',      -- 16*4
	'FishingStart2',   'EnterBattleField','OpenBattleShop','ReturnBattleHome', -- 17*4
	'OpenBattleScore', 'BuyEquipInBattle','UpgradeSkillInBattle', 'ModeSelect',     -- 18*4
	'EquipGuide2',     'Wardrobe',         'CimeliaGuide',   'RandomLevel',         -- 19*4
	'ExpandLevel',     'JAVideoGuide',      'EnterList',     'CimeliaGuide2',       -- 20*4
}

local guide_unused_list = {
	['MouseRule'] = true, ['ModeChange'] = true, ['TitanGuide'] = true, ['Fishing'] = true, 
}

local guide_frame_list = {
}

local MGR_STATE_NOT_START = 0
local MGR_STATE_PREPARE   = 1
local MGR_STATE_RUN       = 2
local MGR_STATE_WAIT      = 3
local MGR_STATE_FINISH    = 4

local MGR_STATE_PREPARE_SELECTMODE = 1
local MGR_STATE_PREPARE_SELECTSKIP = 2

local MaxSleepTime = 1.0 -- seconds

local g_startGuideConn = nil 
local self = nil
function Init()
	lout('GuideMgr: ------------- Init guideMgr Module -------------')
	
	self = _M
	self.name_to_index_map = nil
	self.guide_dict = {}  -- name->guide
	self.guide_list = {}  -- id  -> guide
	self.guide_name_list = guide_name_list -- list of name
	self.guide_frame_list = guide_frame_list -- list of frame
	self.guide_unused_list = guide_unused_list
	self.added_num     = 0
	self.completed_num = 0
	
	self.guide_queued_ids  = {} -- list of {ID=, Completed=}
	self.guide_running_dict = {} -- id->guide
	
	self.state = { State = MGR_STATE_NOT_START, SubState = 0, Duration = 0.0 }

	-- Init related UI resources
	self.indicator_animations = {}
	InitIndicatorAnimations()
	
	-- build guide index <-> name bimap
	BuildName2Index()
	
	-- load guide cfg
	guideCfg.InitGuideCfg(guide)
	
	-- load guide declaration
	modules.loadList(self.guide_name_list)
	
	-- load guide related UIs
	ui_load.loadTutorialUI()
	
	self.events = {}
	SubscribeEvents()
	
	local sceneId = SD.CSceneInfoMgr:Instance():GetCurrentMapID()
	if sceneId == 3 then
		SetSpBarVisible(false)
	end
end

function UnInit()
	if not self then return end	
	lout('GuideMgr: UnInit guideMgr module')
	
	for k, v in pairs(self.guide_running_dict) do
		if v and v.Leave then v:Leave() end	
	end
	for k, v in pairs(self.guide_dict) do
		if v and v.UnInit then v:UnInit() end
	end
	for i, v in ipairs(self.events) do
		if v then v:disconnect() end
	end
	ui_load.unLoadTutorialUI()
	
	self.name_to_index_map = nil
	self.guide_dict = nil
	self.guide_list = nil
	self.guide_name_list = nil
	self.guide_frame_list = nil
	self.added_num     = 0
	self.completed_num = 0
	self.guide_queued_ids  = nil
	self.guide_running_dict = nil
	self.state = nil
	self.indicator_animations = nil
	self = nil
end

function ListenStartGuideEvent()
	if gameEventMgr then
		g_startGuideConn = gameEventMgr:subscribeEvent(EVENT_TUTORIAL_STARTED, OnStartGuide)
	end
end

function RemoveStartGuideEvent()
	if g_startGuideConn then
		g_startGuideConn:disconnect()
		g_startGuideConn = nil
	end
end

function SubscribeEvents()
	if gameEventMgr then
		local ev = nil
		ev = gameEventMgr:subscribeEvent(EVENT_TUTORIAL_ADD_NEW, OnAddNewGuide)
		table.insert(self.events, ev)
		ev = gameEventMgr:subscribeEvent(EVENT_TUTORIAL_SERVER_COMPLETE, OnServerCompleteGuide)
		table.insert(self.events, ev)
		ev = gameEventMgr:subscribeEvent(EVENT_TUTORIAL_SHOW_SP_BAR, OnServerShowSpBar)
		table.insert(self.events, ev)
	end
end

-- Response events from NewTutorialManager in cpp. That means server send ptc
-- to start tutorial.
function OnStartGuide(evtArgs)
	local guideEnabled = true
	if not guideEnabled then
		return
	end
	-- init guide manager
	UnInit()
	lout('GuideMgr: -------------Start New Tutorial-------------')
	Init()
	StartGuide()
end

-- Response events from NewTutorialManager in cpp. That means server send ptc
-- to start tutorial.
function OnAddNewGuide(evtArgs)
	lout('GuideMgr: OnAddNewGuide')
	local arg = tolua.cast(evtArgs, 'SD::LuaInt32ArgEvent')
	if arg.nValue < 1 or arg.nValue > table.getn(self.guide_name_list) then
		lout('GuideMgr: OnAddNewGuide: invalid index ' .. arg.nValue)
		return
	end
	
	local guide_id = arg.nValue
	local inst = self.guide_list[guide_id]
	
	local bAlreadyExists = false
	if inst then 
		bAlreadyExists = true
	else
		for i, v in ipairs(self.guide_queued_ids ) do
			if v.ID == guide_id then 
				bAlreadyExists = true
				break
			end
		end
	end
	if bAlreadyExists then
		lout('GuideMgr: Attemp to add id which has already been added before.')
		return
	end
	
	-- Add the First guide, need to show select mode && select skip
	if self.state.State == MGR_STATE_WAIT and arg.nValue == 1 then
		ChangeState(MGR_STATE_PREPARE)
	elseif self.state.State ~= MGR_STATE_PREPARE then
		ChangeState(MGR_STATE_RUN) 
	end
	table.insert(self.guide_queued_ids, {ID = arg.nValue, Completed = false} )
end

-- Response events from NewTutorialManager in cpp. That means server send ptc
-- to complete an guide
function OnServerCompleteGuide(evtArgs)
	local arg = tolua.cast(evtArgs, 'SD::LuaInt32ArgEvent')
	if arg.nValue < 1 or arg.nValue > table.getn(self.guide_name_list) then
		lout('GuideMgr: OnServerCompleteGuide: invalid index ' .. arg.nValue)
		return
	end

	local guide_id = arg.nValue
	lout('GuideMgr: OnServerCompleteGuide: ' .. guide_id)
	
	local inst = self.guide_running_dict[guide_id]
	if inst then
		inst:SetCompleted(true)  -- May be server complete it
	else 
		for i, v in ipairs(self.guide_queued_ids) do
			if v.ID == guide_id then
				v.Completed = true
			end
		end
	end
end

-- Response events from NewTutorialManager in cpp. That means server send ptc
-- to show sp bar
function OnServerShowSpBar(evtArgs)
	SetSpBarVisible(true)
end

-- Report to NewTutorialManager in cpp
function ReportTutorialProgressed(guide_id)
	if gameEventMgr then
		gameEventMgr:fireEvent(EVENT_TUTORIAL_PROGRESS, SD.LuaInt32ArgEvent(guide_id))
	end
end

-- Report to NewTutorialManager in cpp
function ReportTutorialComplete()
	if gameEventMgr then
		gameEventMgr:fireEvent(EVENT_TUTORIAL_COMPLETE, SD.LuaNoArgEvent())
	end
end

-- Report to NewTutorialManager in cpp
function ReportTutorialSkipped()
	if gameEventMgr then
		gameEventMgr:fireEvent(EVENT_TUTORIAL_SIPPED, SD.LuaNoArgEvent())
	end
end

-- Report to NewTutorialManager in cpp
function ReportTutorialFirstSelectOpMode(nOpMode)
	if gameEventMgr then
		gameEventMgr:fireEvent(EVENT_TUTORIAL_OPMODE_FIRST_SELECT, SD.LuaInt32ArgEvent(nOpMode))
	end
end

function CheckTutorialSkipped()
	if ui.SelectSkipFrame:IsSkipped() then
		ReportTutorialSkipped()
		ChangeState(MGR_STATE_NOT_START)
		return true
	end
	return false
end

function IsGuideStarted()
	return self ~= nil
end

function StartGuide()
	-- show check list frame
	--ui.CheckListFrame:Show()
	ChangeState(MGR_STATE_WAIT)
end

function AddNewGuide(new_guide_id)
	-- add to created list
	local guide_inst = CreateGuideByIndex(new_guide_id)
	if guide_inst then
		self.added_num = self.added_num + 1
		self.guide_running_dict[new_guide_id] = guide_inst
		guide_inst:Enter()
		if guide_inst.guideCfg.OnlyIndicator then
			guide_inst.displaying = true
		end
		TryAttachAutoCompleteTaskCallback(guide_inst)
	end
	return guide_inst
end

function CreateGuideByName(guide_name)
	if self.guide_unused_list[guide_name] then
		return nil
	end
	
	local guide_id = self.name_to_index_map[guide_name]
	if not guide_id then
		lout('GuideMgr: CreateGuideByName fail, invalid guide_name ' .. tostring(guide_name))
		return nil
	end
	return CreateGuideByIndex(guide_id)
end

function CreateGuideByIndex(guide_id)
	if guide_id <=0 or guide_id > table.getn(self.guide_name_list) then
		lout('GuideMgr: CreateGuideByIndex fail, invalid guide_id ' .. tostring(guide_id))
		return nil
	end
	
	local guide_name = self.guide_name_list[guide_id]
	if self.guide_unused_list[guide_name] then
		return nil
	end
	
	local frame_name = self.guide_frame_list[guide_id]
	local guide_cfg  = guide.GuideCfgs[guide_name]
	local frame = ui[frame_name]
	local guide_instance = guide.createGuide(guide_name, guide_cfg, frame)
	
	guide_instance.nID = guide_id
	self.guide_dict[guide_name] = guide_instance
	self.guide_list[guide_id]   =  guide_instance
	
	-- notify UI
	--ui.CheckListFrame:AddNewGuide(guide_id)
	
	if not guide_cfg.OnlyIndicator then
		ui.GuideFrame:AddNewGuide(guide_id)
	end
	
	return guide_instance
end

function OnAfterGuideComplete(guide_instance, notifyServer)
	-- notify cpp
	if notifyServer then
		ReportTutorialProgressed(guide_instance.nID)
	end
	-- notify UI
	--ui.CheckListFrame:SetGuideComplete(guide_instance.nID)
	if not guide_instance.guideCfg.OnlyIndicator then
		ui.GuideFrame:SetGuideComplete(guide_instance.nID)
	end
	
	-- clean up
	guide_instance:Leave()
	
	TryDettachAutoCompleteTaskCallback(guide_instance)
	
	-- increase completed_num
	self.completed_num = self.completed_num + 1
end

function RefreshGuideProgress(guide_id)
	-- notify UI
	--ui.CheckListFrame:SetGuideProgressed(guide_id)
	ui.GuideFrame:SetGuideProgressed(guide_id)
end

function GetGuideInstance(guide_id)
	return self.guide_list[guide_id]
end

function Update(fTime)
	if self.state.State == MGR_STATE_WAIT then
		--lout('GuideMgr: MGR_STATE_WAIT') 
		return
	end
	
	if self.state.State == MGR_STATE_NOT_START then
		return
	end

	if self.state.State == MGR_STATE_FINISH then
		ReportTutorialComplete()
		ChangeState(MGR_STATE_NOT_START)
		return
	end
	
	if self.state.State == MGR_STATE_PREPARE then
		ChangeState(MGR_STATE_RUN)
		--if self.state.SubState == MGR_STATE_PREPARE_SELECTMODE then
		--	if ui.SelectModeFrame.completed then
		--		self.state.SubState = MGR_STATE_PREPARE_SELECTSKIP
		--	end
		--end
		--if self.state.SubState == MGR_STATE_PREPARE_SELECTSKIP then
			-- ui.SelectSkipFrame:Enter()
			-- if ui.SelectSkipFrame.completed then
				-- ui.SelectSkipFrame:Leave()
				-- self.state.SubState = 0
				-- if not CheckTutorialSkipped() then
					-- ChangeState(MGR_STATE_RUN)
				-- end
			-- end
		--end
	end
	
	if self.state.State ~= MGR_STATE_RUN then
		return
	end
	
	-- add guide in queue to running list
	for _, v in ipairs(self.guide_queued_ids) do
		local inst = AddNewGuide(v.ID)
		if inst and v.Completed then
			inst:SetCompleted(true) -- may be server instruct to complete it.
		end
	end
	while table.getn(self.guide_queued_ids) > 0 do 
		table.remove(self.guide_queued_ids)
	end
		
	for k, v in pairs(self.guide_running_dict) do
		if not v:HasCompleted() then
			v.createdTime = v.createdTime + fTime
			
			UpdateAutoCompletionTimer(v, fTime)
			
			if v.Update then v:Update(fTime) end
			--if v.HasProgress and v:HasProgress() then 
			--	RefreshGuideProgress(v.nID)
			--end
		end
		if v:HasCompleted() then
			OnAfterGuideComplete(v, true)
			self.guide_running_dict[k] = nil
		end
	end
	
	if self.completed_num == self.added_num then
		ChangeState(MGR_STATE_WAIT)
	end
end

function UpdateAutoCompletionTimer(guide_inst, fTime)
	local guideCfg = guide_inst.guideCfg
	
	if guide_inst.displaying then 
		guide_inst.displayTime = guide_inst.displayTime + fTime 
		
		-- AutoCompleteByTime 
		if not guide_inst.completed then
			if guideCfg.AutoCompleteType == EAutoCompleteByTime then
				if guide_inst.displayTime > guideCfg.AutoCompleteTime then
					lout('guideMgr: Auto Complete Guide By Timer, ' .. guide_inst.name )
					guide_inst:SetCompleted(true)
				end
			end
		end
		-- AutoCompleteByUIOpen
		if not guide_inst.completed then
			if guideCfg.AutoCompleteType == EAutoCompleteByUIOpen then
				local pUI = InitWindowPtr(guideCfg.AutoCompleteUI)
				if pUI and pUI:isVisible() then
					lout('guideMgr: Auto Complete Guide By UI Open, ' .. guideCfg.AutoCompleteUI)
					guide_inst:SetCompleted(true)
				end
			end
		end
		----
	end
end

function ChangeState(nState)
	self.state.State = nState
	self.state.Duration = 0.0
	if nState == MGR_STATE_PREPARE and self.state.SubState == 0 then
		self.state.SubState = MGR_STATE_PREPARE_SELECTMODE
	end
end

function __TaskFinishCallback(guide_inst, evtArgs)
	local wrappedArgs = tolua.cast(evtArgs, 'SD::LuaWrappedEvtArg')
	local realArgs = tolua.cast(wrappedArgs.realArgs, 'SD::EventTaskFinished')
	
	local submitTaskID = realArgs.m_dwTaskID
	local autocompTaskID = guide_inst.guideCfg.AutoCompleteTaskID
	if submitTaskID == autocompTaskID then
		lout('guideMgr: __TaskFinishCallback, Auto Complete Guide By Finish Task, ' .. guide_inst.name )
		guide_inst:SetCompleted(true)
	end
end

function __TaskAcceptCallback(guide_inst, evtArgs)
	local wrappedArgs = tolua.cast(evtArgs, 'SD::LuaWrappedEvtArg')
	local realArgs = tolua.cast(wrappedArgs.realArgs, 'SD::EventTaskAccepted')
	
	local submitTaskID = realArgs.m_dwTaskID
	local autocompTaskID = guide_inst.guideCfg.AutoCompleteTaskID
	if submitTaskID == autocompTaskID then
		lout('guideMgr: __TaskAcceptCallback, Auto Complete Guide By Accept Task, ' .. guide_inst.name )
		guide_inst:SetCompleted(true)
	end
end

function TryAttachAutoCompleteTaskCallback(guide_inst)
	local autocompType = guide_inst.guideCfg.AutoCompleteType
	local wTaskID = guide_inst.guideCfg.AutoCompleteTaskID
	
	if wTaskID and wTaskID > 0 then
		if autocompType == EAutoCompleteByTaskFinish then
			lout('guideMgr: TryAttachAutoCompleteTaskCallback_Finish, ' .. guide_inst.name )
			guide_inst._autocomptask_conn_ = gameEventMgr:subscribeEvent('EventTaskFinished', __TaskFinishCallback, guide_inst)
			
		elseif autocompType == EAutoCompleteByTaskAccept then
			lout('guideMgr: TryAttachAutoCompleteTaskCallback_Accept, ' .. guide_inst.name )
			guide_inst._autocomptask_conn_ = gameEventMgr:subscribeEvent('EventTaskAccepted', __TaskAcceptCallback, guide_inst)
			
		end
	end
end

function TryDettachAutoCompleteTaskCallback(guide_inst)
	local autocompType = guide_inst.guideCfg.AutoCompleteType
	if autocompType == EAutoCompleteByTaskFinish or autocompType == EAutoCompleteByTaskAccept then
		
		lout('guideMgr: TryDettachAutoCompleteTaskCallback, ' .. guide_inst.name )
		if guide_inst._autocomptask_conn_ then
			guide_inst._autocomptask_conn_:disconnect()
			guide_inst._autocomptask_conn_ = nil
		end
	end
end

function BuildName2Index()
	self.name_to_index_map = {}
	for i, v in ipairs(self.guide_name_list) do 
		self.name_to_index_map[v] = i
	end
end

function InitIndicatorAnimations()
	self.indicator_animations['UP']    = CEGUI.String:new_local('UIIndicatorUP')
	self.indicator_animations['RIGHT'] = CEGUI.String:new_local('UIIndicatorRIGHT')
	self.indicator_animations['DOWN']  = CEGUI.String:new_local('UIIndicatorDOWN')
	self.indicator_animations['LEFT']  = CEGUI.String:new_local('UIIndicatorLEFT')
	
	self.indicator_animations['LEFT_UP']    = CEGUI.String:new_local('UIIndicatorUP')
	self.indicator_animations['RIGHT_UP']   = CEGUI.String:new_local('UIIndicatorUP')
	self.indicator_animations['LEFT_DOWN']  = CEGUI.String:new_local('UIIndicatorDOWN')
	self.indicator_animations['RIGHT_DOWN'] = CEGUI.String:new_local('UIIndicatorDOWN')
end

function SetSpBarVisible(bVisible)
	bVisible = (bVisible and true) or false
	local spBarNames = {
		"Root/PlayerSPBar",
		"Root/PlayerMPBar",
		"Root/PersonalInfo/BG_MPBar",
		"Root/PersonalInfo/BG_SPBar",
	}
	for _, v in ipairs(spBarNames) do
		if InitWindowPtr then
			local pBar = InitWindowPtr(v)
			if pBar then pBar:setVisible(bVisible) end
		end
		if utility and utility.InitWindowPtr then
			local pBar = utility.InitWindowPtr(v)
			if pBar then pBar:setVisible(bVisible) end
		end
	end
end

function Test()
	for i = 1, 4 do
		OnAddNewGuide(SD.LuaInt32ArgEvent(i))
	end
end
