
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openJourney = indicatorFlow.createIndicatorSpec()
	openJourney.frameName     = 'openJourney'
	openJourney.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openJourney.frameText     = '宝物都在收集界面中查看，按这里可以快速打开宝物收集界面'
	openJourney.attachFunc    = self.OnOpenJourneyAttached
	openJourney.attachFuncParam = self
	openJourney.triggerFunc   = nil
	openJourney.triggerFuncParam = nil
	openJourney.triggerWin    = 'MainMenu_Buttons/ButtonFrame/Template_3'
	openJourney.attachWin     = 'MainMenu_Buttons/ButtonFrame/Template_3'
	openJourney.attachWinRoot = 'MainMenu_Buttons'
	openJourney.triggerKey    = nil
	openJourney.priority      = 1
	
	self.indicatorSpecs = {
		openJourney, 
	}
	
	self.indicatorFlows = nil
	
	-- Server Completion
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.downIndicator = ui.createFrame('RightIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function OnOpenJourneyAttached(self, indicator)
	local pWin = InitWindowPtr('Journey')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end
