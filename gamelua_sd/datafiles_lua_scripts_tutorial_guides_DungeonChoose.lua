
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local dungeonBtnSpec = indicatorFlow.createIndicatorSpec()
	dungeonBtnSpec.frameName     = 'dungeonBtnSpec'
	dungeonBtnSpec.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	dungeonBtnSpec.frameText     = '在这里选择要去的副本'
	dungeonBtnSpec.triggerWin    = nil
	dungeonBtnSpec.attachWin     = nil
	dungeonBtnSpec.attachWinRoot = 'Level_Enter'
	dungeonBtnSpec.priority      = 0
	
	local createBtnSpec = indicatorFlow.createIndicatorSpec()
	createBtnSpec.frameName     = 'createBtnSpec'
	createBtnSpec.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	createBtnSpec.frameText     = '然后点击创建关卡就可以进入副本啦'
	createBtnSpec.triggerWin    = 'Level_Enter/Oder_Frame/NewLevel'
	createBtnSpec.attachWin     = 'Level_Enter/Oder_Frame/NewLevel'
	createBtnSpec.attachWinRoot = 'Level_Enter'
	createBtnSpec.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Level_Enter', false)
	createBtnSpec.fallbackFuncParam = self
	createBtnSpec.fallbackPri   = 0
	createBtnSpec.priority      = 0
	
	self.dungeonBtnSpec = dungeonBtnSpec
	self.createBtnSpec  = createBtnSpec
	
	self.indicatorSpecs = {
		dungeonBtnSpec, createBtnSpec,
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	local mapID = 0
	local unit = GetPlayersUnit()
	local eProf = unit:GetProfession()
	if eProf == WM_PROF_XIANGLONG then
		mapID = 58
	elseif eProf == WM_PROF_YAOGUANG then
		mapID = 56
	elseif eProf == WM_PROF_JIYING then
		mapID = 59
	else
		mapID = 57
	end
	
	self.dungeonBtnSpec.attachWin =  'Level_Enter/Map/EnterBtnSpec_'  .. mapID
	self.dungeonBtnSpec.triggerWin = self.dungeonBtnSpec.attachWin
		
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

