
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	local openRoleFrame = indicatorFlow.createIndicatorSpec()
	openRoleFrame.frameName     = 'openRoleFrame'
	openRoleFrame.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openRoleFrame.frameText     = '恭喜大侠达到60级！新的挑战已经开启！赶紧点开看看吧！'
	openRoleFrame.attachFunc    = self.OnOpenRoleFrameAttached
	openRoleFrame.attachFuncParam = self
	openRoleFrame.triggerFunc   = nil
	openRoleFrame.triggerFuncParam = nil
	openRoleFrame.triggerWin    = 'Root/MainMenu/ProfileBtn'
	openRoleFrame.attachWin     = 'Root/MainMenu/ProfileBtn'
	openRoleFrame.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openRoleFrame.triggerKey    = 'PlayerAvaterUI'
	openRoleFrame.priority      = 1

	local openExpandLevelPage = indicatorFlow.createIndicatorSpec()
	openExpandLevelPage.frameName     = 'openExpandLevelPage'
	openExpandLevelPage.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openExpandLevelPage.frameText     = '扩展等级玩法开放啦，点这里打开扩展等级界面'
	openExpandLevelPage.attachFunc    = self.OnOpenExpandLevelAttached
	openExpandLevelPage.attachFuncParam = self
	openExpandLevelPage.triggerFunc   = nil
	openExpandLevelPage.triggerFuncParam = nil
	openExpandLevelPage.triggerWin    = 'Root/PlayerAvatar/AvatarFrame/ExpandLevelButton'
	openExpandLevelPage.attachWin     = 'Root/PlayerAvatar/AvatarFrame/ExpandLevelButton'
	openExpandLevelPage.attachWinRoot = 'Root/CharacterEquip'
	openExpandLevelPage.priority      = 2
		
	self.indicatorSpecs = {
		openRoleFrame, openExpandLevelPage
	}
	
	self.indicatorFlows = nil
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	-- Create Indicator
	self.downIndicator = ui.createFrame('DownIndicatorFrame')
	if self.guideCfg.IndicatorText then
		self.downIndicator.pWindow:setText(self.guideCfg.IndicatorText)
	end
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
	
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end


function OnOpenRoleFrameAttached(self, indicator)
	local pWin = InitWindowPtr('Root/CharacterEquip')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenExpandLevelAttached(self, indicator)
	local pWin = InitWindowPtr('ExpandLevel')
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function Update(self, fTime)
	if self.completed then
		return
	end
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

