
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openIdentifyUI = indicatorFlow.createIndicatorSpec()
	openIdentifyUI.frameName     = 'openIdentifyUI'
	openIdentifyUI.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openIdentifyUI.frameText     = '打开界面'
	openIdentifyUI.attachFunc    = self.OnIdentifyUIAttached
	openIdentifyUI.attachFuncParam = self
	openIdentifyUI.triggerFunc   = nil
	openIdentifyUI.triggerFuncParam = nil
	openIdentifyUI.triggerWin    = 'Craft_Identify/Confirm'
	openIdentifyUI.attachWin     = 'Craft_Identify/Confirm'
	openIdentifyUI.attachWinRoot = 'Craft_Identify'
	openIdentifyUI.priority      = 0
	
	local placeItem = indicatorFlow.createIndicatorSpec()
	placeItem.frameName     = 'PlaceUnIdentifyItem'
	placeItem.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	placeItem.frameText     = '点击放置未鉴定的道具'
	placeItem.attachFunc    = nil
	placeItem.attachFuncParam = nil
	placeItem.triggerFunc   = nil
	placeItem.triggerFuncParam = nil
	placeItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	placeItem.attachItemID  = '4_998'
	placeItem.dependWin     = 'Craft_Identify'
	placeItem.priority      = 1
	placeItem.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Identify', false)
	placeItem.fallbackFuncParam = self
	placeItem.fallbackPri   = 0
	
	local confirmIdentify = indicatorFlow.createIndicatorSpec()
	confirmIdentify.frameName     = 'ConfirmIdentify'
	confirmIdentify.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	confirmIdentify.frameText     = '点击开始鉴定'
	confirmIdentify.attachFunc    = nil
	confirmIdentify.attachFuncParam = nil
	confirmIdentify.triggerFunc   = nil
	confirmIdentify.triggerFuncParam = nil
	confirmIdentify.triggerWin    = 'Craft_Identify/Confirm'
	confirmIdentify.attachWin     = 'Craft_Identify/Confirm'
	confirmIdentify.attachWinRoot = 'Craft_Identify'
	confirmIdentify.priority      = 2
	confirmIdentify.fallbackFunc  = indicatorFlow.getFallbackFunc_UIDependency('Craft_Identify', false)
	confirmIdentify.fallbackFuncParam = self
	confirmIdentify.fallbackPri   = 0
	
	self.indicatorSpecs = {
		openIdentifyUI, placeItem, confirmIdentify
	}
	
	self.indicatorFlows = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnIdentifyUIAttached(self, indicator)
	indicator.bTriggered = true
	indicator.pFrame:Hide()
end	

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

