
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.progressed = false
	self.guideCfg = _cfg
	self.frame = _frame
	self.conditions = 
	{
		['F10'] = { action = 'ZeroMode1',    triggered = false },
		['F11'] = { action = 'ZeroMode2',    triggered = false },
		['F12'] = { action = 'ClassicMode',  triggered = false },
	}
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function HasProgress(self)
	return self.progressed
end

function MeetCondition(self, con)
	return self.conditions[con].triggered
end

function Update(self, fTime)
	local pGameInput = SD.CHSGameInput:Instance()
	
	local total = 0
	local count = 0
	for k, v in pairs(self.conditions) do
		total = total + 1
		if v.triggered or pGameInput:IsActionTriggered(v.action) then
			v.triggered = true
			count = count + 1
		end
	end
	
	if total == count then
		self.completed = true
	elseif count > 0 then
		self.progressed = true
	end
	
	if self.frame and self.frame.Update then
		self.frame:Update(fTime)
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

