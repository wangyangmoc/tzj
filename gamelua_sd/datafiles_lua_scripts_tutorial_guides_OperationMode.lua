
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.progressed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local openOpModeUI = indicatorFlow.createIndicatorSpec()
	openOpModeUI.frameName     = 'openOpModeUI'
	openOpModeUI.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	openOpModeUI.frameText     = self.guideCfg.IndicatorText
	openOpModeUI.frameWidth    = 200
	openOpModeUI.attachFunc    = nil
	openOpModeUI.attachFuncParam = nil
	openOpModeUI.triggerFunc   = nil
	openOpModeUI.triggerFuncParam = nil
	openOpModeUI.triggerWin    = 'DefaultMainMenu/Mod/Ani'
	openOpModeUI.attachWin     = 'DefaultMainMenu/Mod'
	openOpModeUI.attachWinRoot = 'DefaultMainMenu'
	openOpModeUI.priority      = 0
	
	self.indicatorSpecs = {
		openOpModeUI, 
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then return end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

