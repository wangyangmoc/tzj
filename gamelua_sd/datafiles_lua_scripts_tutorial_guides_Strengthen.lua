
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame
	
	self.materialItemID = '4_995'
	self.equipItemID = '7_10'

	local openBag = indicatorFlow.createIndicatorSpec()
	openBag.frameName     = 'openBag'
	openBag.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	openBag.frameText     = '点击打开包裹进行装备强化'
	openBag.attachFunc    = self.OnOpenBagAttached
	openBag.attachFuncParam = self
	openBag.triggerFunc   = nil
	openBag.triggerFuncParam = nil
	openBag.triggerWin    = 'Root/MainMenu/PackageBtn'
	openBag.attachWin     = 'Root/MainMenu/PackageBtn'
	openBag.attachWinRoot = 'MainMenu_FunctionAndEXP'
	openBag.triggerKey    = 'PackageUI'
	openBag.priority      = 1
	
	local selectMaterial = indicatorFlow.createIndicatorSpec()
	selectMaterial.frameName     = 'selectMaterial'
	selectMaterial.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	selectMaterial.frameText     = '右键点击选择强化石'
	selectMaterial.attachFunc    = nil
	selectMaterial.attachFuncParam = nil
	selectMaterial.triggerFunc   = nil
	selectMaterial.triggerFuncParam = nil
	selectMaterial.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	selectMaterial.attachItemID  = self.materialItemID 
	selectMaterial.priority      = 2
	
	local openBagEquipPage = indicatorFlow.createIndicatorSpec()
	openBagEquipPage.frameName     = 'openBagEquipPage'
	openBagEquipPage.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	openBagEquipPage.frameText     = '点击切换至装备包裹'
	openBagEquipPage.attachFunc    = self.OnOpenBagPageAttached
	openBagEquipPage.attachFuncParam = self
	openBagEquipPage.triggerFunc   = nil
	openBagEquipPage.triggerFuncParam = nil
	openBagEquipPage.triggerWin    = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWin     = 'Root/PackageFrame/TagContainer1/Btn2_1_0'
	openBagEquipPage.attachWinRoot = 'Root/PackageFrame'
	openBagEquipPage.priority      = 3
	
	local selectEquipItem = indicatorFlow.createIndicatorSpec()
	selectEquipItem.frameName     = 'selectEquipItem'
	selectEquipItem.frameType     = indicatorFlow.INDICATOR_FRAME_RIGHT
	selectEquipItem.frameText     = '点击装备进行强化'
	selectEquipItem.attachFunc    = nil
	selectEquipItem.attachFuncParam = nil
	selectEquipItem.triggerFunc   = nil
	selectEquipItem.triggerFuncParam = nil
	selectEquipItem.attachType    = indicatorFlow.INDICATOR_ATTACH_BAGITEM
	selectEquipItem.attachItemID  = self.equipItemID
	selectEquipItem.priority      = 4
	
	local confirmEquipItem = indicatorFlow.createIndicatorSpec()
	confirmEquipItem.frameName     = 'confirmStren'
	confirmEquipItem.frameType     = indicatorFlow.INDICATOR_FRAME_UP
	confirmEquipItem.frameText     = '点击开始强化'
	confirmEquipItem.attachFunc    = nil
	confirmEquipItem.attachFuncParam = nil
	confirmEquipItem.triggerFunc   = nil
	confirmEquipItem.triggerFuncParam = nil
	confirmEquipItem.triggerWin    = 'Equip_Enhance/Confirm'
	confirmEquipItem.attachWin     = 'Equip_Enhance/Confirm'
	confirmEquipItem.attachWinRoot = 'Equip_Enhance'
	confirmEquipItem.priority      = 5
	
	self.indicatorSpecs = {
		openBag, selectMaterial, openBagEquipPage, selectEquipItem, confirmEquipItem, 
	}
	
	self.indicatorFlows = nil
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnOpenBagAttached(self, indicator)
	local pWin = InitWindowPtr('Root/PackageFrame')
	if pWin and pWin:isVisible() then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function OnOpenBagPageAttached(self, indicator)
	local pBagItemWin = SD.GetFirstBagItemButtonContainer(self.equipItemID)
	if pBagItemWin then
		indicator.bTriggered = true
		indicator.pFrame:Hide()
	end
end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

