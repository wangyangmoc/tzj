
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.guideCfg = _cfg
	self.frame = _frame

	-- self.windowAttached = false
	-- self.upIndicator = nil
end

function UnInit(self)

end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	-- Create Indicator
	-- self.upIndicator = ui.createFrame('UpIndicatorFrame')
	-- self.upIndicator.pWindow:setText('可以点击上面的信息条哦！啊！啊！快！快点我！啊！~~')
		
	-- -- Attach to AcceptTask Window
	-- self:TryAttachWindow()
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)
	-- if self.upIndicator then
		-- ui.destroyFrame(self.upIndicator)
		-- self.upIndicator = nil
	-- end
	
	-- self.windowAttached = false
end

-- function TryAttachWindow(self)
	-- if self.windowAttached then 
		-- return
	-- end
	
	-- if not ui.TitanFrame then return end
	-- local dialogBtn = ui.TitanFrame.pContainer
	-- if not dialogBtn then return end
	
	
	-- self.upIndicator:Show()
	-- UiUtility.AttachWndToWnd(self.upIndicator.pWindow, dialogBtn, UiUtility.PUT_BOTTOM)
		
	-- -- Add Animation
	-- local animName = guideMgr.indicator_animations['UP']
	-- SD.WndAnimManager:Instance():CreateAnim(self.upIndicator.pWindow, animName, false, true, true)
	
	-- self.windowAttached = true
-- end


function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function MeetCondition(self, con)
	return true
end

function Update(self, fTime)
	if self.completed then
		return
	end
	-- self:TryAttachWindow()
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

