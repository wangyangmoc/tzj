
guide.defineGuide(...)
function Init(self, _cfg, _frame)
	self.completed = false
	self.progressed = false
	self.guideCfg = _cfg
	self.frame = _frame

	local updateBattleSkill = indicatorFlow.createIndicatorSpec()
	updateBattleSkill.frameName     = 'updateBattleSkill'
	updateBattleSkill.frameType     = indicatorFlow.INDICATOR_FRAME_DOWN
	updateBattleSkill.frameText     = self.guideCfg.IndicatorText
	updateBattleSkill.attachFunc    = nil
	updateBattleSkill.attachFuncParam = nil
	updateBattleSkill.triggerFunc   = nil
	updateBattleSkill.triggerFuncParam = nil
	updateBattleSkill.triggerKey    = {'DOTA_ADD_SKILL_1', }
	updateBattleSkill.triggerWin    = {'MainMenu_SkillRelease/DotaSkillFrame/Item1/Upgrade','MainMenu_SkillRelease/DotaSkillFrame/Item2/Upgrade', 'MainMenu_SkillRelease/DotaSkillFrame/Item3/Upgrade', 'MainMenu_SkillRelease/DotaSkillFrame/Item4/Upgrade',  }
	updateBattleSkill.attachWin     = 'MainMenu_SkillRelease/DotaSkillFrame/Item1/Upgrade'
	updateBattleSkill.attachWinRoot = 'MainMenu_SkillRelease'
	updateBattleSkill.priority      = 0
	
	self.indicatorSpecs = {
		updateBattleSkill, 
	}
end

function UnInit(self)
	self:Leave()
end

function Enter(self)
	lout('Welcome! Enter guide ' .. self.name)
	
	self.indicatorFlows = indicatorFlow.createIndicatorFlow(self.indicatorSpecs)
end

function Leave(self)
	lout('Bye! Leaving guide ' .. self.name)

	indicatorFlow.destroyIndicatorFlow(self.indicatorFlows)
end

function SetCompleted(self, setting)
	self.completed = setting
end

function HasCompleted(self)
	return self.completed
end

function Update(self, fTime)
	if self.completed then return end
	
	indicatorFlow.updateIndicatorFlow(self.indicatorFlows)
	
	if self.indicatorFlows.bDone then
		self.completed = true
	end
end

function OnEvent(self)

end

function Dump(self)
	lout('    '..self.guideCfg.Name)
	lout('    '..guide.GuideTypeName[self.guideCfg.Type])
end

