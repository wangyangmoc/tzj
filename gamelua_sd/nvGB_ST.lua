    
---------------------------------SG--------------------------------

STake_Start("SG_GB_ST_def_000","SG_GB_ST_def_000")
    BlendMode(0)
    BlendTime(0.2)
    MotionBlur(0.000,0.152,3)    

STake_Start("SG_GB_ST_dodge_000","SG_GB_ST_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
    MotionBlur(0.000,0.268,3)    
    Fx(0,"SG_GB_ST_dodge_000")   

    Sound(0.033,"N1_Axe_dodge_000")
    GroundSound(0.046,"Sound_jump")

STake_Start("SG_GB_ST_wait_000","SG_SL_ST_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")
    
STake_Start("SG_GB_ST_wait_000_ui","SG_wait_000_SLST")
    BlendMode(0)
    BlendTime(0)
    Loop()

STake_Start("SG_GB_ST_move_001","SG_GB_ST_move_001")    
    BlendTime(0.2)
    BlendMode(1)
    BlendPattern("Down")     
    
    GroundSound(0.353,"Sound_step")
    GroundSound(0.793,"Sound_step")
  Loop()               

STake_Start("SG_GB_ST_move_004","SG_SL_ST_move_004")
    BlendTime(0.2)
    BlendMode(1) 
    BlendPattern("Down")       
    
    GroundSound(0.065,"Sound_step")
    GroundSound(0.439,"Sound_step")
  Loop()

STake_Start("SG_GB_ST_move_005","SG_GB_ST_move_005")    
    BlendTime(0.2)
    BlendMode(1)  
    BlendPattern("Down")       
         
    GroundSound(0.051,"Sound_step")
    GroundSound(0.410,"Sound_step")
  Loop()

STake_Start("SG_GB_ST_move_006","SG_GB_ST_move_006")
    BlendTime(0.2)
    BlendMode(1) 
    BlendPattern("Down")       
    
    GroundSound(0.051,"Sound_step")
    GroundSound(0.410,"Sound_step")
  Loop()

STake_Start("SG_GB_ST_move_009","SG_GB_ST_move_009")
    BlendTime(0.2)
    BlendMode(0)
    GroundSound(0.190,"Sound_step")
    GroundSound(0.650,"Sound_step")
  Loop() 

STake_Start("SG_GB_ST_move_010","SG_GB_ST_move_010")
    BlendTime(0.2)
    BlendMode(0)
    GroundSound(0.150,"Sound_step")
    GroundSound(0.580,"Sound_step")
  Loop() 



STake_Start("SG_GB_ST_attack_000","SG_GB_ST_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

    MustPlay(0.00,0.252)  
    Soft(0.252,0.646)
  	
  	Fx(0.173,"HG_GB_ST_Trail01","Spine1")
  	
    AttackStart()

  	Hurt(0.218,"hurt_11")
  	
  	Sound(0.125,"SG_attackVoc_1")  	  	
  	Sound(0.170,"HG_GB_ST_Attack")  	
  	HurtSound(0.218,"ST_Hit_002")

    DirFx(0.218,"GB_ST_hurt_000",25,1)
    
    Critical("hurt_11","GB_ST_hurt_000","ST_Hit_003")     


STake_Start("SG_GB_ST_attack_001","SG_GB_ST_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    

    MustPlay(0.00,0.314)  
    Soft(0.314,0.688)	  	
  	
  	Fx(0.278,"HG_GB_ST_Trail01","Spine1")
  	
  	AttackStart()
	--CameraShake(0.36,"ShakeMode = 1","ShakeAmplitudeX = 1","ShakeAmplitudeY = 1","ShakeAmplitudeZ = 1")
  	Hurt(0.314,"hurt_11")
  	
  	Sound(0.214,"SG_attackVoc_1")    	
  	Sound(0.262,"HG_GB_ST_Attack")  	
  	HurtSound(0.314,"ST_Hit_002")

    DirFx(0.314,"GB_ST_hurt_000",25,1)

    Critical("hurt_11","GB_ST_hurt_000","ST_Hit_003")     

STake_Start("SG_GB_ST_attack_002","SG_GB_ST_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   
    BlendPattern("no_sync")    
    
    MustPlay(0.00,0.436)     
  	
  	Fx(0.312,"HG_GB_ST_Trail01","Spine1")
   
  	AttackStart()

  	Hurt(0.336,"hurt_21")
  	
  	Sound(0.300,"SG_attackVoc_1")  	
  	Sound(0.316,"HG_GB_ST_Attack")
  	HurtSound(0.336,"ST_Hit_002")

    DirFx(0.336,"GB_ST_hurt_000",25,1)
    
    Critical("hurt_11","GB_ST_hurt_000","ST_Hit_003")     

STake_Start("SG_GB_ST_attack_003","SG_GB_ST_attack_003")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_GB_ST_attack_004","SG_GB_ST_attack_004")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_GB_ST_attack_005","SG_GB_ST_attack_005")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_GB_ST_attack_006","SG_GB_ST_attack_006")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_GB_ST_attack_007","SG_GB_ST_attack_007")
    BlendMode(0)
    BlendTime(0.2)

STake_Start("SG_GB_ST_dead","SG_GB_ST_dead")
    BlendMode(0)
    BlendTime(0.2)
    Sound(0.059,"voc_Male_death")  
	StopChannel(0,2)

STake_Start("SG_GB_ST_hit","SG_GB_ST_hit")
    BlendMode(0)
    BlendTime(0.05)
    --Sound(0.059,"voc_derais_injure_01_short_01")
    BlendPattern("Upper_hurt")   
    
    
    



STake_Start("SG_GB_ST_skill_000_combo1","SG_GB_ST_skill_000_combo1")  		--棒舞旋风1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.435)

		
  	AttackStart()
  	Hurt(0.330,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_000_combo1_fire")

  	Sound(0.204,"SG_attackVoc_2")  	
  	Sound(0.234,"HG_GB_ST_Skill_001")
  	HurtSound(0.330,"ST_Hit_001")
  	
    DirFx(0.330,"HG_GB_ST_skill_000_combo1_hit",25,1)  



STake_Start("SG_GB_ST_skill_000_combo2","SG_GB_ST_skill_000_combo2")  		--棒舞旋风2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.447)

  	AttackStart()
  	Hurt(0.330,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_000_combo2_fire")
        DirFx(0.330,"HG_GB_ST_skill_000_combo1_hit",25,1)  

  	Sound(0.204,"SG_attackVoc_2")  	
  	Sound(0.074,"HG_GB_ST_Skill_004")
  	HurtSound(0.330,"ST_Hit_001")    


STake_Start("SG_GB_ST_skill_000_combo3","SG_GB_ST_skill_000_combo3")  		--棒舞旋风3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.804)		
		
  	AttackStart()
  	Hurt(0.245,"hurt_11")
  	Hurt(0.418,"hurt_11")
  	Hurt(0.640,"hurt_21")
	
  	Fx(0.000,"HG_GB_ST_skill_000_combo3_fire")
  	
    DirFx(0.245,"HG_GB_ST_skill_000_combo1_hit",25,1) 
    DirFx(0.418,"HG_GB_ST_skill_000_combo1_hit",25,1) 
    DirFx(0.640,"HG_GB_ST_skill_000_combo1_hit",25,1) 
       
    --AttackHurtFx(0.3,"HG_GB_ST_hit")  
 	  

  	Sound(0.010,"HG_GB_ST_Skill_008")  	
  	Sound(0.010,"SG_attackVoc_3")
    GroundSound(0.203,"Sound_jump")  
    GroundSound(0.657,"Sound_fall")
  	  	
  	HurtSound(0.245,"ST_Hit_001") 
  	HurtSound(0.418,"ST_Hit_001")   	 	
  	HurtSound(0.640,"ST_Hit_001")    	 	


    MotionBlur(0.180,0.599,2)    


STake_Start("SG_GB_ST_skill_001_combo1","SG_GB_ST_skill_001_combo1")  		--风吹一线1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.400)


  	AttackStart()
  	Hurt(0.316,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_001_combo1")
  	
  	Sound(0.222,"SG_attackVoc_2")   	
  	Sound(0.010,"HG_GB_ST_Skill_000")  	
  	HurtSound(0.316,"ST_Hit_001") 


    DirFx(0.316,"HG_GB_ST_skill_001_hit",28,1)  
     
    Charge(0.230,0.100,28,0,0) 


STake_Start("SG_GB_ST_skill_001_combo2","SG_GB_ST_skill_001_combo2")  		--风吹一线2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.660)

  	AttackStart()
  	Hurt(0.340,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_001_combo2")
    DirFx(0.340,"HG_GB_ST_skill_001_hit",28,1)  

    Charge(0.00,0.340,28,0,0) 

  	Sound(0.222,"SG_attackVoc_2")   	
  	Sound(0.010,"HG_GB_ST_Skill_004")  	
  	HurtSound(0.340,"ST_Hit_001")

STake_Start("SG_GB_ST_skill_001_combo3","SG_GB_ST_skill_001_combo3")  		--风吹一线3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.641)

		
  	AttackStart()
  	Hurt(0.443,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_001_combo3")
    AttackHurtFx(0.443,"HG_GB_ST_skill_001_hit","Spine1")  
    
    
    Charge(0.151,0.268,0,0,2)

        Sound(0.108,"SG_attackVoc_3")   	
  	Sound(0.050,"HG_SL_ST_Skill_003")  	
  	HurtSound(0.443,"ST_Hit_003") 

    HitGroundFx( 0.443 ,"SFX_hitground")
  	HitGroundFx(0.443,"SFX_hitground2") 


STake_Start("SG_GB_ST_skill_002_combo1","SG_GB_ST_skill_002_combo1")  		--人影虚无1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.909)

  	AttackStart()
  	Hurt(0.237,"hurt_21")
  	Hurt(0.413,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_002_combo1_fire")
    DirFx(0.237,"HG_GB_ST_skill_001_hit",28,1)  
    DirFx(0.413,"HG_GB_ST_skill_001_hit",28,1)  

        Sound(0.108,"SG_attackVoc_3")   	
  	Sound(0.050,"HG_GB_ST_Skill_008")  	
  	HurtSound(0.443,"ST_Hit_003") 

    HitGroundFx( 0.237 ,"SFX_hitground")
  	HitGroundFx(0.237,"SFX_hitground2") 


STake_Start("SG_GB_ST_skill_002_combo3","SG_GB_ST_skill_002_combo3")  		--人影虚无3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.879)

  	AttackStart()
  	Hurt(0.346,"hurt_21")
  	Hurt(0.426,"hurt_21")
  	Hurt(0.599,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_002_combo3")
    DirFx(0.346,"HG_GB_ST_skill_000_combo1_hit",28,1)  
    DirFx(0.426,"HG_GB_ST_skill_000_combo1_hit",28,1)
    DirFx(0.599,"HG_GB_ST_skill_000_combo1_hit",28,1)
   	
  	Sound(0.226,"HG_GB_ST_Skill_015_2")  
  	Sound(0.386,"HG_GB_ST_Skill_015_2")  
  	Sound(0.533,"HG_GB_ST_Skill_015_2")  	
  	HurtSound(0.346,"ST_Hit_001") 
  	HurtSound(0.426,"ST_Hit_001") 
  	HurtSound(0.599,"ST_Hit_001") 

STake_Start("SG_GB_ST_skill_003_combo1","SG_GB_ST_skill_003_combo1")  		--逆转反击1
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.649)

  	AttackStart()
  	Hurt(0.208,"hurt_21")
  	Hurt(0.399,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_003_combo1_fire")
     DirFx(0.208,"HG_GB_ST_skill_003_hit",28,1,1,90,-45) 
     DirFx(0.399,"HG_GB_ST_skill_003_hit",28,1,1,-90,-45) 
    
    Charge(0.141,0.100,0,0,2)
    
        Sound(0.050,"SG_attackVoc_3")   	
  	Sound(0.050,"HG_GB_ST_Skill_003")  	
  	HurtSound(0.208,"ST_Hit_003")  
  	HurtSound(0.399,"ST_Hit_003")   

STake_Start("SG_GB_ST_skill_003_combo2","SG_GB_ST_skill_003_combo2")  		--逆转反击2
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.690)

  	AttackStart()
  	Hurt(0.270,"hurt_21")
  	Hurt(0.470,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_003_combo2_fire")
     DirFx(0.270,"HG_GB_ST_skill_003_hit",28,1,1,90,45) 
     DirFx(0.470,"HG_GB_ST_skill_003_hit",28,1,1,-120,0) 
    
    Charge(0.200,0.070,0,0,2)
    Charge(0.360,0.090,28,0,0)

        Sound(0.400,"SG_attackVoc_3")   	
  	Sound(0.050,"HG_SL_ST_Skill_014")  	
  	HurtSound(0.270,"ST_Hit_003")  
  	HurtSound(0.470,"ST_Hit_003")  

STake_Start("SG_GB_ST_skill_003_combo3","SG_GB_ST_skill_003_combo3")  		--逆转反击3
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.879)

  	AttackStart()
  	Hurt(0.366,"hurt_21")
  	Hurt(0.696,"hurt_21")

  	Fx(0.000,"HG_GB_ST_skill_003_combo3_fire")
     DirFx(0.366,"HG_GB_ST_skill_003_hit",28,1,1,90,0) 
     DirFx(0.696,"HG_GB_ST_skill_003_hit",28,1,1,-90,0) 

        Sound(0.274,"SG_attackVoc_2")   
        Sound(0.623,"SG_attackVoc_2") 	
  	Sound(0.274,"HG_GB_ST_Skill_001")  
  	Sound(0.384,"HG_GB_ST_Skill_002") 
	
  	HurtSound(0.366,"ST_Hit_001")  
  	HurtSound(0.696,"ST_Hit_003")

STake_Start("SG_GB_ST_skill_004","SG_GB_ST_skill_004")  		--昂首吐信
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,0.620)
		
	
  	Fx(0.000,"HG_GB_ST_skill_004_start")


  	Sound(0.080,"HG_GB_ST_Skill_003")  	
  	Sound(0.080,"SG_attackVoc_1")  	



STake_Start("SG_GB_ST_skill_005","SG_GB_ST_skill_005")  		--棒扫群狗
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,1.080)

		
  	AttackStart()
  	Hurt(0.336,"hurt_34")
	
  	Fx(0.000,"HG_GB_ST_skill_005_fire")

    DirFx(0.336,"HG_GB_ST_skill_005_hit",10,1)  

  	Sound(0.111,"SG_attackVoc_2")  	
  	Sound(0.010,"HG_GB_ST_Skill_002")
	  HurtSound(0.336,"ST_Hit_001")
    GroundSound(0.192,"Sound_jump")  
    GroundSound(0.924,"Sound_fall")
    
    HitGroundFx( 0.336 ,"SFX_hitground")
  	HitGroundFx(0.336,"SFX_hitground2") 


STake_Start("SG_GB_ST_skill_006","SG_GB_ST_skill_006")  		--拨狗朝天
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,1.128)
		
  	Fx(0.000,"HG_GB_ST_skill_006_fire")		


  	AttackStart()
  	Hurt(0.161,"hurt_81")
  	Hurt(0.337,"")


   AttackHurtFx(0.161,"HG_GB_ST_skill_000_combo1_hit","Hips",1)    
   AttackHurtFx(0.337,"HG_GB_ST_skill_000_combo1_hit","Hips",1)    

		
  	Sound(0.010,"SG_attackVoc_3")   	
  	Sound(0.010,"HG_GB_ST_Skill_014")  
    GroundSound(0.131,"Sound_jump")  
    GroundSound(1.055,"Sound_fall")	
    
  	HurtSound(0.161,"PA_Hit_002")
  	HurtSound(0.337,"PA_Hit_002")


    Charge(0.114,0.088,7,0,0)    


STake_Start("SG_GB_ST_skill_007","SG_GB_ST_skill_007")  		--太祖闪影
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
		Hard(0.000,1.215)
		
		
  	AttackStart()
  	Hurt(0.492,"hurt_21")
  	Hurt(0.592,"hurt_21")
  	Hurt(0.692,"hurt_21")
  	Hurt(0.792,"hurt_21")  	
	
  	Fx(0.000,"HG_GB_ST_skill_007_fire")
    DirFx(0.492,"HG_GB_ST_skill_007_hit",25,1,1)
    DirFx(0.592,"",25,1,1)
    DirFx(0.692,"",25,1,1)
    DirFx(0.792,"",25,1,1)


    MotionBlur(0.000,1.215,5)    

  	Sound(0.315,"SG_attackVoc_5") 
  	Sound(0.050,"HG_GB_ST_Skill_005")  	
  	HurtSound(0.492,"HG_GB_ST_Skill_005_Hit")  	 	
  	HurtSound(0.592,"HG_GB_ST_Skill_005_Hit")  	 	
  	HurtSound(0.692,"HG_GB_ST_Skill_005_Hit")  	 	
  	HurtSound(0.792,"HG_GB_ST_Skill_005_Hit")  	 	
    
    
STake_Start("SG_GB_ST_skill_008","SG_GB_ST_skill_008")  		--天罗地网
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
  	Fx(0.000,"HG_GB_ST_skill_008_fire")		


  	AttackStart()
  	Hurt(0.100,"hurt_21")
  	--Hurt(0.448,"")
  	--Hurt(0.728,"")
  	--Hurt(0.944,"")
  	--Hurt(1.216,"")
  	--Hurt(1.706,"")  	  	


     AttackHurtFx(0.000,"HG_GB_ST_skill_001_hit","Hips",1)    
   --AttackHurtFx(0.448,"HG_GB_ST_skill_001_hit","Hips",1)    
   --AttackHurtFx(0.728,"HG_GB_ST_skill_001_hit","Hips",1)   
   --AttackHurtFx(0.944,"HG_GB_ST_skill_001_hit","Hips",1) 
   --AttackHurtFx(1.216,"HG_GB_ST_skill_001_hit","Hips",1) 
   --AttackHurtFx(1.706,"HG_GB_ST_skill_001_hit","Hips",1)   
  
        Sound(0.010,"SG_attackVoc_1")   	
  	Sound(0.050,"HG_GB_ST_Skill_015_1") 
        Sound(0.239,"SG_attackVoc_1")   	
  	Sound(0.239,"HG_GB_ST_Skill_015_2")
        Sound(0.500,"SG_attackVoc_1")   	
  	Sound(0.500,"HG_GB_ST_Skill_015_2")  
        Sound(0.826,"SG_attackVoc_1")   	
  	Sound(0.826,"HG_GB_ST_Skill_015_2")    	
        Sound(1.013,"SG_attackVoc_1")   	
  	Sound(1.013,"HG_GB_ST_Skill_015_2")
        Sound(1.492,"SG_attackVoc_3")   	
  	Sound(1.492,"HG_GB_ST_Skill_015_3")
  	
  	HurtSound(0.100,"ST_Hit_001")
  	--HurtSound(0.448,"ST_Hit_001")
  	--HurtSound(0.728,"ST_Hit_001")
  	--HurtSound(0.944,"ST_Hit_001")  
  	--HurtSound(1.216,"ST_Hit_001")
  	--HurtSound(1.706,"ST_Hit_001")  
  	
  	
  	
STake_Start("SG_GB_ST_skill_008_end","SG_GB_ST_wait_000")  		
    BlendTime(0.2)
    BlendMode(0)     
    Priority(0,"5")        	
  	
    
    
STake_Start("SG_GB_ST_skill_009","SG_GB_ST_skill_009")  		--天下无狗
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("no_sync")    
  
    Loop()
		
  	Fx(0.05,"HG_GB_ST_skill_009_fire")		


  	AttackStart()
  	Hurt(0.0,"hurt_45")

      	
        Sound(0.050,"HG_GB_ST_Skill_016")
        Sound(0.160,"SG_attackVoc_5")
        Sound(0.800,"SG_attackVoc_4")    	
        HurtSound(0.0,"ST_Hit_001")

    DirFx(0.0,"HG_GB_ST_skill_001_hit",20,1) 	

  	
    HitGroundFx( 0.266 ,"SFX_hitground")
  	HitGroundFx(0.266,"SFX_hitground2") 
  	
    HitGroundFx( 0.853 ,"SFX_hitground")
  	HitGroundFx(0.853,"SFX_hitground2")   	
    
STake_Start("SG_GB_ST_skill_009_end","SG_GB_ST_wait_000")  		
    BlendTime(0.2)
    BlendMode(0)  
    Priority(0,"5")     



STake_Start("SG_GB_ST_skill_010","SG_GB_ST_skill_010")  --	爆气
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("no_sync")     
    
    CameraShake(0.240,"ShakeTimes = 0.1","ShakeMode = 3","ShakeAmplitudeX = 0","ShakeAmplitudeY = 2","ShakeAmplitudeZ = 0")	    

        Sound(0.050,"HG_GB_ST_Skill_013")

