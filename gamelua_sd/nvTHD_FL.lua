
     
----------------------------------��&ָ------------------------------------------ 

STake_Start("SG_THD_FL_wait_000","SG_THD_FL_wait_000")
    BlendMode(0)
    BlendTime(0.2)
    Loop()
    BlendPattern("wait")     
    
STake_Start("SG_THD_FL_wait_000_ui","SG_THD_FL_wait_000")
    BlendMode(0)
    BlendTime(0)
    Loop()
    
    
STake_Start("SG_THD_FL_link_000","SG_THD_FL_link_000")
    BlendMode(2)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.400, "DL_BR_WOMAN_2")

STake_Start("SG_THD_FL_link_001","SG_THD_FL_link_001")
    BlendMode(0)
    BlendTime(0.2)
    SkeletonMod(0.1, 0.746, "DL_BR_WOMAN_1")    
    


STake_Start("SG_THD_FL_move_001","SG_THD_FL_move_001")
    BlendTime(0.2)
    BlendMode(0)

  Loop()
  

    --GroundFx(0.970, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    --GroundFx(0.480, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.970, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.480, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)      

    GroundSound(0.479,"Sound_step")
    GroundSound(0.970,"Sound_step")    

STake_Start("SG_THD_FL_move_004","SG_THD_FL_move_004")
    BlendTime(0.2)
    BlendMode(0)
  Loop()


    GroundFx(0.073, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.073, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.253,"Sound_step")
    GroundSound(0.632,"Sound_step")  

STake_Start("SG_THD_FL_move_005","SG_THD_FL_move_005")
    BlendTime(0.2)
    BlendMode(0)
  Loop()


    GroundFx(0.073, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.073, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.253,"Sound_step")
    GroundSound(0.632,"Sound_step") 

STake_Start("SG_THD_FL_move_006","SG_THD_FL_move_006")
    BlendTime(0.2)
    BlendMode(0)
  Loop()


    GroundFx(0.073, "SFX_step_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_step_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)       
  
    GroundFx(0.073, "SFX_stepon_left", "LeftToeBase", 1.0, 0.0,  0,0,0,0,-90,0)    
    GroundFx(0.439, "SFX_stepon_right", "RightToeBase", 1.0, 0.0,  0,0,0,0,-90,0)           

    GroundSound(0.253,"Sound_step")
    GroundSound(0.632,"Sound_step") 

STake_Start("SG_THD_FL_move_009","SG_THD_FL_move_009")
    BlendTime(0.2)
    BlendMode(0)
    
  Loop() 

    GroundSound(0.230,"Sound_step")
    GroundSound(0.630,"Sound_step")

STake_Start("SG_THD_FL_move_010","SG_THD_FL_move_010")
    BlendTime(0.2)
    BlendMode(0)

  Loop() 

    GroundSound(0.270,"Sound_step")
    GroundSound(0.570,"Sound_step")

STake_Start("SG_THD_FL_hit","SG_THD_FL_hit")
    BlendMode(0)
    BlendTime(0.2)
    BlendPattern("Upper_hurt")    


STake_Start("SG_THD_FL_dodge_000","SG_THD_FL_dodge_000")
    BlendMode(0)
    BlendTime(0.2)
 
    Fx(0.015,"HG_THD_FL_dodge_000","Reference")      

    Sound(0.050,"L1_Unarmed_dodge_000") 

STake_Start("SG_THD_FL_def_000","SG_THD_FL_def_000")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.00,"T1_Polearm_def_000")


STake_Start("SG_THD_FL_dead","SG_THD_FL_dead")
    BlendMode(0)
    BlendTime(0.2)

    Sound(0.059,"SL_DieVOC_000") 
    GroundSound(0.769,"Sound_daodi")
    StopChannel(0,2)	

STake_Start("SG_THD_FL_attack_000","SG_THD_FL_attack_000")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")     
    BlendPattern("Upper_att")       
    
		All(0.240,0.714)

		
  	AttackStart()
  	Hurt(0.170,"hurt_11")
  	

    Fx(0.000,"HG_THD_FL_attack_000")
    DragFx(0.17, 0.2, "HG_THD_FL_attack_trail", "HG_THD_FL_attack_hit", 0, 0, 0, "T_R", "Spine1",0,0,0,0,0,1)

  	Sound(0.050,"THD_SG_AttackVOC_1_1")  	
  	Sound(0.050,"HG_THD_FL_attack")
  	HurtSound(0.170,"HG_THD_FL_attack_hit")	  	  		

STake_Start("SG_THD_FL_attack_001","SG_THD_FL_attack_001")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5")   	
    BlendPattern("Upper_att")       

  	All(0.276,0.663)
 
  	AttackStart()
  	Hurt(0.218,"hurt_11")

 
    Fx(0.000,"HG_THD_FL_attack_000")
    DragFx(0.218,0.2,"HG_THD_FL_attack_trail","HG_THD_FL_attack_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)


  	Sound(0.050,"THD_SG_AttackVOC_1_1")  	
  	Sound(0.050,"HG_THD_FL_attack")
  	HurtSound(0.218,"HG_THD_FL_attack_hit")		

STake_Start("SG_THD_FL_attack_002","SG_THD_FL_attack_002")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")       
  		
  	AttackStart()
  	Hurt(0.306,"hurt_11")


  	
    Fx(0.000,"HG_THD_FL_attack_000")
    DragFx(0.306,0.2,"HG_THD_FL_attack_trail","HG_THD_FL_attack_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)

  	Sound(0.050,"THD_SG_AttackVOC_1_1")  	
  	Sound(0.050,"HG_THD_FL_attack")
  	HurtSound(0.306,"HG_THD_FL_attack_hit")		

STake_Start("SG_THD_FL_attack_003","SG_THD_FL_attack_003")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")       

    MustPlay(0.000,0.298)  
    Soft(0.298,0.8)


  	AttackStart()
  	Hurt(0.240,"hurt_11")

  	Sound(0.272,"THD_SG_AttackVOC_1_1")  	
  	Sound(0.216,"HG_THD_FL_attack")
  	HurtSound(0.240,"HG_THD_FL_attack_hit")			
  	
    Fx(0.000,"HG_THD_FL_attack_003")
    DragFx(0.240,0.2,"HG_THD_FL_attack_trail","HG_THD_FL_attack_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)


STake_Start("SG_THD_FL_attack_004","SG_THD_FL_attack_004")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")       

    MustPlay(0.000,0.336)  
    Soft(0.336,0.8)
  		
  		
  	AttackStart()
  	Hurt(0.3,"hurt_11")

  	Sound(0.272,"THD_SG_AttackVOC_1_1")  	
  	Sound(0.232,"HG_THD_FL_attack")
  	HurtSound(0.3,"HG_THD_FL_attack_hit")	
  	
    Fx(0.000,"HG_THD_FL_attack_004")
    DragFx(0.3,0.2,"HG_THD_FL_attack_trail","HG_THD_FL_attack_hit",0,0,0,"T_L","Spine1",0,0,0,0,0,1)


STake_Start("SG_THD_FL_attack_005","SG_THD_FL_attack_005")
    BlendMode(0)
    BlendTime(0.2)
    Priority(0,"5") 
    BlendPattern("Upper_att")       

    MustPlay(0.000,0.344)  

  		
  	AttackStart()
  	Hurt(0.232,"hurt_11")

  	Sound(0.272,"THD_SG_AttackVOC_1_2")  	
  	Sound(0.186,"HG_THD_FL_attack")
  	HurtSound(0.232,"HG_THD_FL_attack_hit")			
  	
    Fx(0.000,"HG_THD_FL_attack_005")
    DragFx(0.232,0.2,"HG_THD_FL_attack_trail","HG_THD_FL_attack_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)






STake_Start("SG_THD_FL_skill_000_ylp","SG_THD_FL_skill_001")  --ҡ����008
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")  
    
		Hard(0.000,0.933)
		
  	AttackStart()
  	Hurt(0.306,"hurt_21")    
  	
  	Fx(0.000,"HG_THD_FL_skill_008_fire")
        LinkFx(0.306,"HG_THD_FL_skill_008_link",0.5,"T_L","Spine1",1,0.1)
        AttackHurtFx(0.306,"HG_THD_FL_skill_008_hit","Spine1",1)  
    
    Charge(0.000,0.100,0,0,2) 	    

  	Sound(0.050,"HG_THD_FL_skill_008_fire")
  	HurtSound(0.306,"HG_THD_FL_skill_008_hit")	

STake_Start("SG_THD_FL_skill_000_zhy","SG_THD_FL_skill_008")  --�Ứ��009
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")  
    
		Hard(0.000,0.933)
		
  	AttackStart()
  	Hurt(0.306,"hurt_21")    
    
    Fx(0.000,"HG_THD_FL_skill_009_fire")

   AttackHurtFx(0.00,"HG_THD_FL_skill_009_hit","Reference",1)  

    Charge(0.000,0.100,0,0,2) 	    

  	Sound(0.050,"HG_THD_FL_skill_009_fire")
  	HurtSound(0.306,"HG_THD_FL_skill_009_hit") 
    
STake_Start("SG_THD_FL_skill_000_bhcs_loop","SG_THD_FL_skill_000")  --�̺�����000
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")  
    Loop()
    
  	AttackStart()
    --Hurt(0.690,"hurt_21")  

  	Fx(0.000,"HG_THD_FL_skill_000_loop")


  	Sound(0.050,"HG_THD_FL_skill_000_loop")     

STake_Start("SG_THD_FL_skill_000_bhcs_end","SG_THD_FL_wait_000")  --�̺�����
    BlendMode(0)
    BlendTime(0.2) 
   


STake_Start("SG_THD_FL_skill_000_czyh_loop","SG_THD_FL_skill_000")  --����̺�
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")  
    Loop()
    
  	AttackStart()
    Hurt(0.1,"hurt_21")  

  	Fx(0.000,"HG_THD_FL_skill_001_fire")
    DirFx(0,"HG_THD_FL_skill_001_hit",1,0)  

  	Sound(0.050,"HG_THD_FL_skill_000_loop")     

STake_Start("SG_THD_FL_skill_000_czyh_end","SG_THD_FL_wait_000")  --����̺�
    BlendMode(0)
    BlendTime(0.2) 
    


STake_Start("SG_THD_FL_skill_001","SG_THD_FL_skill_001")  --ѡ��UI
    BlendMode(0)
    BlendTime(0.2) 
    

STake_Start("SG_THD_FL_skill_002_start","SG_THD_FL_skill_002")  --��������start
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")      
    
    Loop(1,2)

  	Fx(0.000,"HG_THD_FL_skill_002_start")
  	Fx(1.000,"HG_THD_FL_skill_002_loop")    

  	Sound(0.050,"HG_THD_FL_skill_002_start")
 	  Sound(1.050,"HG_THD_FL_skill_002_loop")


STake_Start("SG_THD_FL_skill_002_fire","SG_THD_FL_skill_002")  --��������fire
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
    BlendPattern("no_sync")   
      
    FrameTime(2,3)
    
		Hard(0.000,0.400)
		
  	AttackStart()
  	Hurt(0.100,"hurt_21")    
  	Hurt(0.150,"")    
  	Hurt(0.200,"")    
  	Hurt(0.250,"")    
  	Hurt(0.300,"")    

  	Fx(0.000,"HG_THD_FL_skill_002_fire")
    DragFx(0.1,0.2,"HG_THD_FL_skill_002_tracker","HG_THD_FL_skill_002_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.15,0.2,"HG_THD_FL_skill_002_tracker","HG_THD_FL_skill_002_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.2,0.2,"HG_THD_FL_skill_002_tracker","HG_THD_FL_skill_002_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.25,0.2,"HG_THD_FL_skill_002_tracker","HG_THD_FL_skill_002_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.3,0.2,"HG_THD_FL_skill_002_tracker","HG_THD_FL_skill_002_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)

    Charge(0.000,0.160,0,0,2) 	    

  	HurtSound(0.100,"HG_THD_FL_skill_002_hit") 
  	HurtSound(0.150,"HG_THD_FL_skill_002_hit") 
  	HurtSound(0.200,"HG_THD_FL_skill_002_hit") 
  	HurtSound(0.250,"HG_THD_FL_skill_002_hit")  
  	HurtSound(0.300,"HG_THD_FL_skill_002_hit") 

STake_Start("SG_THD_FL_skill_003","SG_THD_FL_skill_000")  --�һ���
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
    BlendPattern("no_sync")   
    Loop() 
  
  	Hurt(0.100,"hurt_21")       
  	Fx(0.115,"HG_THD_FL_skill_003_fire")
        DirFx(0.115,"HG_THD_FL_skill_003_hit",0,1)  
 
  	Sound(0.050,"HG_THD_FL_skill_003_fire")
  	HurtSound(0.000,"HG_THD_FL_skill_003_hit")    
  	
  	
STake_Start("SG_THD_FL_skill_003_end","SG_THD_FL_wait_000")  --�һ���end
    BlendMode(0)
    BlendTime(0.2)    	
  	
  	
  	

STake_Start("SG_THD_FL_skill_004_start","SG_THD_FL_skill_004")  --����ʯstart
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")          
        
    Loop(1,2)

  	Fx(0.000,"HG_THD_FL_skill_004_start")
  	Fx(1.000,"HG_THD_FL_skill_004_loop")

  	Sound(0.050,"HG_THD_FL_skill_004_start")
  	Sound(1.000,"THD_Dianguang_Loop")

STake_Start("SG_THD_FL_skill_004_fire","SG_THD_FL_skill_004")  --����ʯfire
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
    BlendPattern("no_sync")   
    
    FrameTime(2,3.266)
    
		Hard(0.000,0.710)
		
  	AttackStart()
  	Hurt(0.220,"hurt_21")   

    DragFx(0.22,0.2,"HG_THD_FL_skill_004_trail","HG_THD_FL_skill_002_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)     
    
    Charge(0.000,0.351,0,0,2) 	    

  	Sound(0.050,"HG_THD_FL_skill_004_fire") 
  	HurtSound(0.220,"HG_THD_FL_skill_003_hit") 

STake_Start("SG_THD_FL_skill_005","SG_THD_FL_skill_005")  --��Ӱ����
    BlendMode(0)
    BlendTime(0.2) 



STake_Start("SG_THD_FL_skill_005_start","SG_THD_FL_skill_005")  --��Ӱ����start
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")      
    
    Loop(0.166,1.166)
    Fx(0.166,"HG_THD_FL_skill_005_loop")

  	Sound(0.050,"HG_QZ_PA_skill_009")
  	Sound(0.166,"HG_THD_FL_skill_005_lopp_1")


STake_Start("SG_THD_FL_skill_005_fire_1","SG_THD_FL_skill_005")  --��Ӱ����fire1
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
    BlendPattern("no_sync")   
    
    FrameTime(1.167,2.666)
    
		Hard(0.000,1.179)
		
  	AttackStart()
  	Hurt(0.167,"hurt_21")  
  	Hurt(0.3,"hurt_21")  
  	Hurt(0.5,"hurt_21")  

  	Fx(1.168,"HG_THD_FL_skill_005_fire")
    DragFx(0.167,0.2,"HG_THD_FL_skill_005_tracker","HG_THD_FL_skill_005_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.3,0.2,"HG_THD_FL_skill_005_tracker","HG_THD_FL_skill_005_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.5,0.2,"HG_THD_FL_skill_005_tracker","HG_THD_FL_skill_005_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    

  	Sound(1.226,"HG_THD_FL_skill_005_fire") 
  	HurtSound(0.167,"HG_THD_FL_skill_005_hit") 
  	HurtSound(0.300,"HG_THD_FL_skill_005_hit") 
  	HurtSound(0.500,"HG_THD_FL_skill_005_hit") 

STake_Start("SG_THD_FL_skill_005_fire_2","SG_THD_FL_skill_005")  --��Ӱ����fire2
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5")   
    BlendPattern("no_sync")   
    
    FrameTime(1.167,2.666)
    
		Hard(0.000,1.179)
		
  	AttackStart()
  	Hurt(0.1,"hurt_31")  
  	Hurt(0.3,"hurt_31")  
  	Hurt(0.5,"hurt_31") 

  	Fx(1.168,"HG_THD_FL_skill_005_fire")
    DragFx(0.167,0.2,"HG_THD_FL_skill_005_tracker","HG_THD_FL_skill_005_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.3,0.2,"HG_THD_FL_skill_005_tracker","HG_THD_FL_skill_005_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)
    DragFx(0.5,0.2,"HG_THD_FL_skill_005_tracker","HG_THD_FL_skill_005_hit",0,0,0,"T_R","Spine1",0,0,0,0,0,1)

  	Sound(0.050,"HG_THD_FL_skill_005_fire") 
  	HurtSound(0.100,"HG_THD_FL_skill_005_hit") 
  	HurtSound(0.300,"HG_THD_FL_skill_005_hit") 
  	HurtSound(0.500,"HG_THD_FL_skill_005_hit")  	

STake_Start("SG_THD_FL_skill_006","SG_THD_FL_skill_006")  --������Ѩ
    BlendMode(0)
    BlendTime(0.2) 

		Hard(0.000,1.066)
		
  	AttackStart()
  	Hurt(0.519,"hurt_21") 
   
  	Fx(0.000,"HG_THD_FL_skill_006_fire")
    DirFx(0.519,"HG_THD_FL_skill_006_hit",0,1)     

  	Sound(0.050,"HG_THD_FL_skill_006_fire") 
  	HurtSound(0.519,"HG_THD_FL_skill_006_hit") 

STake_Start("SG_THD_FL_skill_007_start","SG_THD_FL_skill_007")  --��ָ����start
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")          
    
    Loop(1.067,2.066)

  	Fx(0.000,"HG_THD_FL_skill_007_start")
  	Fx(1.067,"HG_THD_FL_skill_007_loop")
    
    CameraShake(0.214,"ShakeTimes = 0.1","ShakeMode = 3","ShakeAmplitudeX = 0","ShakeAmplitudeY = 0","ShakeAmplitudeZ = 1")	

  	HitGroundFx(0.214,"SFX_hitground2")   		 		
    HitGroundFx(0.214,"SFX_hitground")
    
  	Sound(0.050,"HG_THD_FL_skill_007_start") 
  	Sound(1.067,"HG_THD_FL_skill_007_loop") 

STake_Start("SG_THD_FL_skill_007_fire","SG_THD_FL_skill_007")  --��ָ����fire
    BlendMode(0)
    BlendTime(0.2) 
    Priority(0,"5") 
    BlendPattern("no_sync")          
    Loop(2.066,3.066)
    
    FrameTime(2.066,3.066)

  	AttackStart()
  	Hurt(0.049,"hurt_21")       
  	Hurt(0.202,"hurt_21")       
  	Hurt(0.356,"hurt_21")       
  	Hurt(0.478,"hurt_21")  

  	Fx(0.000,"HG_THD_FL_skill_007_fire") 
  	
    DragFx(0.049,0.2,"HG_THD_FL_skill_007_trail","",0,0,0,"T_L","Spine1",0,0,0,0,0,1)
    DragFx(0.202,0.2,"HG_THD_FL_skill_007_trail","",0,0,0,"T_R","Spine1",0,0,0,0,0,1) 
    DragFx(0.356,0.2,"HG_THD_FL_skill_007_trail","",0,0,0,"T_L","Spine1",0,0,0,0,0,1) 
    DragFx(0.478,0.2,"HG_THD_FL_skill_007_trail","",0,0,0,"T_R","Spine1",0,0,0,0,0,1)  
        
        DirFx(0.049,"HG_THD_FL_skill_007_hit",0,1) 
        DirFx(0.202,"HG_THD_FL_skill_007_hit",0,1) 
        DirFx(0.356,"HG_THD_FL_skill_007_hit",0,1)   
        DirFx(0.478,"HG_THD_FL_skill_007_hit",0,1)         

  	Sound(0.050,"HG_THD_FL_skill_007_fire") 
  	HurtSound(0.049,"PC_Gong_hit") 
  	HurtSound(0.202,"PC_Gong_hit") 
  	HurtSound(0.356,"PC_Gong_hit")
  	HurtSound(0.478,"PC_Gong_hit")

STake_Start("SG_THD_FL_skill_007_end","SG_THD_FL_skill_007")  --��ָ����end
    BlendMode(0)
    BlendTime(0.2) 
    FrameTime(2.066,3.066)
    Priority(0,"5") 
    

