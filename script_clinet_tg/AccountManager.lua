local io = io
local os = os
local listAccount = g_listAccount
local pairs = pairs
local print = print
local type = type
local string = string
local cbArea = g_cbArea
local cbServer = g_cbServer
local cbScript = g_cbScript
local uiConfig = g_uiConfig
local tostring = tostring
local tonumber = tonumber
local pcall = pcall
local assert = assert
local setmetatable = setmetatable
local require = require
local NetSend = NetSend
local TerminateProcess = Kernel32.TerminateProcess
local WaitForSingleObject = Kernel32.WaitForSingleObject
local OpenProcess = Kernel32.OpenProcess
local CloseHandle = Kernel32.CloseHandle
local g_scriptConfig = g_scriptConfig
local g_etCreateGame = g_etCreateGame
local g_savedTeam = g_savedTeam
local g_logger = g_logger
local table = table
module "CAccountManager"

local _waittingAccount
m_indexAccounts = {}
m_accountExtern = {}

cfg_AccountDataCol = {
    index = {"索引",0,50},
    user = {"帐号",1},
    pwd = {"密码",2},
    area = {"大区",3},
    server = {"服务器",4},
    script = {"脚本",5},
	name = {"角色",6},
	lv = {"等级",7,50},
	money = {"金钱",8,50},
    status = {"状态",9,80},
    teamleader = {"队长",10},
}

accountMt = {}
accountMt.__index = function(t,key)
	--print("try extern",key)
	local idx = t.index+1
	return m_accountExtern[idx] and m_accountExtern[idx][key]
end

function CreateAccount(o)
	o = o or {}
	o.storedata = {charidx = 0}
	setmetatable(o,accountMt)
	g_savedTeam[o] = {}
	return o
end

local listInit = false
function AccountListInit()
	if listInit then return end
    local newList = {}
    for k,v in pairs(cfg_AccountDataCol) do
        newList[v[2]+1] = {k,v}
    end
    local k,v
    for i=1,#newList do
        v = newList[i][2]
        k = newList[i][1]
        listAccount:InsertColumn(v[2],v[1],v[3] or 90)
    end
	listInit = true
end

function ParseFile(data,t)
    local index = #t+1
    for user,pwd in string.gmatch(data,"(%w%w%w%w%w%w+@?%w*%.?%w*)%W+(%w+)") do--四个以上的字母
        t[index]= CreateAccount()
        t[index].user = user
        t[index].pwd = pwd
		t[index].area = uiConfig.area
		t[index].server = uiConfig.server
		t[index].script = uiConfig.script
		t[index].areaidx = uiConfig.areaidx
		t[index].serveridx = uiConfig.serveridx
		t[index].index = index-1
		SetExtern(index-1,"charidx",0)
		SetExtern(index-1,"specia","normal")
        index = index+1
    end
end

function LoadFromFile(fname,add)
    uiConfig.area = cbArea:GetText()
    uiConfig.server = cbServer:GetText()
	local oldscript = uiConfig.script
    uiConfig.script = cbScript:GetText()
	for k,v in pairs(g_savedTeam) do
		g_savedTeam[k] = nil
	end
	--assert(uiConfig.area and uiConfig.area ~= '',"没有大区") 
	--assert(uiConfig.server and uiConfig.server ~= '',"没有服务器") 
	--assert(uiConfig.script and uiConfig.script ~= '',"没有脚本") 
	if oldscript ~= uiConfig.script then
		--uiConfig.scriptt = require(uiConfig.script)
		print('scriptt',type(uiConfig.scriptt))
	end
	uiConfig.serveridx = cbServer:GetCurSel()
    --uiConfig.areaidx = cbArea:GetCurSel()
	local f = io.open(fname,"r")
    if not f then return end
    local data = f:read("*all")
    f:close()
    if add then 
        ParseFile(data,m_indexAccounts)
    else
		ExitAll()
		uiConfig.curclientnum = 0
        m_indexAccounts = {}
		m_accountExtern = {}
        ParseFile(data,m_indexAccounts)
    end
	if #m_indexAccounts > 0 and g_scriptConfig['本机交易'] == 1 then
		m_indexAccounts[1].script = "收钱.lua"
	end
    listAccount:DeleteAllItems()
	for i=1,#m_indexAccounts do
		 listAccount:InsertItem(i-1,"")
		 listAccount:SetCheck(i-1,1)
	end
    ShowAccount()
end

function GetAccount(idx,s)
    if idx then return m_indexAccounts[idx+1] end
	if not s then return end
	for i=1,#m_indexAccounts do
		if m_indexAccounts[i].socket == s then
			return m_indexAccounts[i]
		end
	end
end

function GetAccountExtern(idx)
    if idx then return m_accountExtern[idx+1] end
end

function GetAccountByKey(key,val)
	for i=1,#m_indexAccounts do
		 if m_indexAccounts[i][key] == val then
			return m_indexAccounts[i]
		end
	end
end

function GetText(index,col)
    local t = GetAccount(index)
    if not t then return end
	local t2 = GetAccountExtern(index)
    for k,v in pairs(cfg_AccountDataCol) do
        if v[2] == col then
            return t[k] or (t2 and t2[k])
        end
    end
	return ""
end

function SetText(index,col,text)
    local t = GetAccount(index)
    if not t then return end
    for k,v in pairs(cfg_AccountDataCol) do
        if v[2] == col then
            t[k] = text
            listAccount:SetItemText(index,col,text)
            return
        end
    end
end

function IsCheck(idx)
    return listAccount:GetCheck(idx) == 1
end

function GetStatus(idx)
    local t = m_accountExtern[idx+1]
	return t and t.status or ""
end

function SetStatus(idx,status)
	return SetExtern(idx,"status",status)
end

function SetExtern(idx,key,val)
	local t = m_accountExtern[idx+1] or {}
	t[key] = val
	m_accountExtern[idx+1] = t
	local cfg = cfg_AccountDataCol[key]
	if cfg then
		listAccount:SetItemText(idx,cfg[2],val)
	end
end

function ExitAll()
	for i=1,#m_indexAccounts do
		ExitAccount(m_indexAccounts[i])
	end
end

function ExitAccount(at)
	if at.socket then
		NetSend(at.socket,'exit#ov')
		SetExtern(at.index,"socket",nil)
	end
	if not at.phandle then return end
	TerminateProcess(at.phandle,0)
	CloseHandle(at.phandle)
	SetExtern(at.index,"phandle",nil)
	SetStatus(at.index,"")
end

function GetClientNum()
	local num = 0
	local at
	for i=1,#m_indexAccounts do
		at = m_indexAccounts[i]
		if at.phandle then
			local ret = WaitForSingleObject(at.phandle,0)
			if 258 ~= ret then
				ExitAccount(at)
				num = num + 1
			end
		end
    end
	return num
end

function StatusCheck()
	local num_invalid_handle = 0
	local at
	for i=1,#m_indexAccounts do
		at = m_indexAccounts[i]
		if at.phandle then
			local ret = WaitForSingleObject(at.phandle,0)
			if 258 ~= ret then
				print("check status failed",i,at.user,ret)
				g_logger:warn("进程崩溃:%s",at.user)
				ExitAccount(at)
				if at.status == '运行中' then num_invalid_handle = num_invalid_handle + 1 end
			end
		elseif at.socket and (os.time()-at.logtime) > 120 and at.status == '登陆中' then--超过2分钟
			print("check status failed",i,at.user,ret)
			g_logger:warn("登陆超时:%s",at.user)
			ExitAccount(at)
			num_invalid_handle = num_invalid_handle + 1
		end
    end
	--if needCreate and HasIdleAccount() then
		--g_etCreateGame:SetEvent()
	--end
	return num_invalid_handle
end

function SendData(data)
	local socket
	for i=1,#m_indexAccounts do
		socket = m_indexAccounts[i].socket
		if socket then
			NetSend(socket,data)
		end
	end
end

function LogData(idx,result)
	local t = assert(GetAccount(idx))
	local f = assert(io.open("脚本日志.txt","a"))
	f:write(os.date("%c")," ",t.user,"-角色",t.charidx,"-",(t.name or "??"),"-",t.lv or 0," ",result,"\n")
	f:close()
end

function ShowAccount(t,col)
    local cfg
    if nil == t then
        for i=1,#m_indexAccounts do
            t = m_indexAccounts[i]
            for k,v in pairs(t) do
                cfg = cfg_AccountDataCol[k]
				if cfg then
					listAccount:SetItemText(i-1,cfg[2],v)
				end
            end
			t = m_accountExtern[i]
			if t then
				for k,v in pairs(t) do
					cfg = cfg_AccountDataCol[k]
					if cfg then
						listAccount:SetItemText(i-1,cfg[2],v)
					end
				end
			end
        end
    elseif t then
        for k,v in pairs(t) do
            cfg = cfg_AccountDataCol[k]
            if cfg then listAccount:SetItemText(t.index,cfg[2],v) end
        end
    end
end

function AddAccount(acc,pwd,area,server,script,areaidx,serveridx)
    if type(acc) == "table" then
        listAccount:InsertItem(acc.index,"")
		listAccount:SetCheck(acc.index,1)
        ShowAccount(acc)
        return
    end
    local newAccount = CreateAccount()
	SetExtern(#m_indexAccounts,"charidx",0)
	SetExtern(#m_indexAccounts,"specia","normal")
    newAccount.user = acc or "??"
    newAccount.pwd = pwd or "??"
    newAccount.area = area or uiConfig.area
    newAccount.server = server or uiConfig.server
    newAccount.script = script or uiConfig.script
	newAccount.areaidx = areaidx or uiConfig.areaidx
    newAccount.serveridx = serveridx or uiConfig.serveridx
    newAccount.index = #m_indexAccounts
    m_indexAccounts[#m_indexAccounts+1] = newAccount
    listAccount:InsertItem(newAccount.index,"")
	listAccount:SetCheck(newAccount.index,1)
    ShowAccount(newAccount)
end

function Restart(idx)
	local at = GetAccount(idx)
	if not at then return end
	ExitAccount(at)
	_waittingAccount = at
	return true
end

function GetIdleAccount(socket)
	if _waittingAccount then
		local at = _waittingAccount
		_waittingAccount = nil
		SetExtern(at.index,'socket',socket)
		--at.socket = socket
		SetStatus(at.index,"登录中")
		SetExtern(at.index,"logtime",os.time())
		return at
	end
    for i=1,#m_indexAccounts do
        if IsCheck(i-1) and GetStatus(i-1) == "" then
            m_accountExtern[i].socket = socket
			SetStatus(i-1,"登录中")
			SetExtern(m_indexAccounts[i].index,"logtime",os.time())
            return m_indexAccounts[i],m_accountExtern[i]
        end
    end
end

function HasIdleAccount()
	for i=1,#m_indexAccounts do
        if IsCheck(i-1) and GetStatus(i-1) == "" then
            return true
        end
    end
	return false
end