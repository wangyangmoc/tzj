require 'std'
require "alien"
require 'logging.file'
Kernel32 = alien.load 'Kernel32.dll'
Kernel32.WaitForSingleObject:types {"pointer","ulong",abi="stdcall",ret = "ulong"}
Kernel32.OpenProcess:types {"ulong","int" ,"ulong",abi="stdcall",ret = "pointer"}
Kernel32.CloseHandle:types {"pointer",abi="stdcall",ret = "int"}
Kernel32.TerminateProcess:types {"pointer","uint",abi="stdcall",ret = "int"}

User32 = alien.load 'User32.dll'
User32.ShowWindow:types {"pointer","int",abi="stdcall",ret = "int"}
ShowWindow = User32.ShowWindow

function MiniSizeProcessWindows(pid,class)
	pid = tonumber(pid)
	local hwnd = GetProcessWindow(pid,class)
	if hwnd then
		print('now mini then window',pid)
		ShowWindow(hwnd,6)
	else
		print('there is no hwnd',pid)
	end
	return hwnd
end

function print(...)
	local str = ""
	local t= {...}
	for i=1,#t do
		str = str..tostring(t[i]).." "
	end
	MskPrint(str)
end

local oldsedn = NetSend
function NetSend(s,data)
	oldsedn(s,data.."&")
end

function CopyFile(src,dest)
    local f = io.open(src,"rb")
    if not f then 
        print("copyfile failed file not exit",src)
        return 
    end
    local data = f:read("*all")
    f:close()
    f = io.open(dest,"wb")
    if not f then 
        print("copyfile failed file cant create",dest)
        return 
    end
    f:write(data)
    f:close()
    return
end

g_logger = logging.file("./��־/����̨.txt")
g_logger:setLevel (logging.INFO)