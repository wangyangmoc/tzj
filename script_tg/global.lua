--[[
门派需要兼容代码
学习技能顺序
战斗流程
--]]
local msk = msk
local coroutine = coroutine


----[[
--多开:83 F8 03 76 26
--过动画ESI 57 8B 7C 24 0C 57 E8
--这里是各种全局变量和初始化
sig = {
	--8B 40 0C 8B 40 08 8D 70 30 B9 0F 00 00 00
	selectBase = 0x898B74,--选中对象基质
	selectId = 0xe8,--选中对象便宜
	selectcharcall = 0x00525F80,--8B B5 FC 00 00 00 8B FD E8 下一个CALL
	crossEngineBase = 0,
	
	--8B 70 54 D1 E9 F6 C1 01
	charsbase = 0x897748,
	charsoff = 0x54,
	--EB 09 8B 47 08 56 E8下面那个CALL
	opencharcall = 0x00418C40,
	
	dynamicObjCanPick = 0x148,--是否可拾取
	dynamicObjFlag = 0x171,--动态物品结束标志
	dynamicObjFlag2 = 0x1c9,
	--74 59 55 E8
	collectcall = 0x005A7B10,
	
	--83 C8 FF D9 44 24 1C 8B FE D9 1C 24 51 E8
	castareaskillcall = 0x00418B00,
	
	--56 57 8B 78 54 85 FF +27
	canpathcall = 0x0062B530,
}
--]]
--gUI_MenPaiName
s_config = {
	qundis = 5,
	escortdis = 5,--护送等待距离
	defaultpro = 0,--默认职业 神将
	defaultsex = 0,
	npctalkdis = 6,--NPC对话距离 对话时小于这个距离默认为已到达目的地  五方城的青帝距离是6 其他5
    movmindis = 3,--每一次检测必须移动的距离 小于则任务卡住,重启寻路
	coorddis = 3,--寻路目标点偏差 小于则认为已经到达目的地
	menpaidis = {--门派技能释放距离 大于则寻路到怪物
		[0] = 3,
		5,
		24,
		24,
		20,
	},
	mskcmflag = "mskcm",
}
--玩家配置 动态的
d_config = {
	acceptfrient = true,
}

--背包格子
--gUI_GOODSBOX_BAG
s_bagnum = {
	40,--默认
	40,--任务
	[4] = 12,--装备
	[5] = 40,--仓库
}

cfg = {
	splv = {
		["1_35"] = 35,
	},
}

--打印级别
pt_lv_print = 0
pt_lv_debug = 1
pt_lv_warn = 2
pt_lv_error = 3
pt_lv_cur = ver_release and pt_lv_warn or pt_lv_debug

----------------------全局公用变量--------------------------------
g_globalt = g_globalt or {}
local g_global = g_globalt
--g_global.runthread--当前运行的携程
g_global.appdir = gvAppPath--gapppath:gsub("mskdata","")
g_global.accountdata = {}
--[[
if g_globalt.objlockh == nil then
	g_globalt.objlockh,g_globalt.objlockptr = msk.createmapfile("dobj_lock",10*4)
end
--]]

----------------------下面开始函数的各种初始化-----------------------
function InitGlobal()
	g_global.timers = {}
	g_global.signame = 0
	g_global.runListTask = {}
	g_global.sigcallback = {}
	g_global.friendrains = {}
	g_global.teammemers = {}
	g_global.cashers = {}
	g_global.allowfriends = {}
	g_global.listcallback2 = {}
	g_global.listcallback1 = {}
	g_global.cmdcallback = {}
	g_global.timers = {}
	g_global.timerscallback = {}
	g_global.timersloop = {}
	g_global.online = false
	g_global.beattacked = false
	g_global.pause = false
	g_global.ismove = false
	g_global.state = "login"
	g_global.kmtm = 0
	g_global.isbusy = false
	g_global.isfight = false
	g_global.selectserverok = false
	g_global.homebron = false
	g_global.itemupdate = false
	g_global.taskupdate = false
	g_global.runthread = nil
	g_global.lastcallbacklv = 0
	g_global.mainthread = nil
	g_global.mainfun = nil
	g_global.xcjtaskstart = false
	g_global.LogTaskDead = true
	g_global.TaskDead = {}
	g_global.numDead = 0
	g_global.updatedata = false
	g_global.testlv = ver_release and 10 or 3
	g_global.test_wait_trade = false
	--g_global.testlv = (not ver_release and 1) or 10--0:选区界面就终止 2:进入游戏后不执行脚本
end

function mprint(lv,...)
	if lv < pt_lv_cur then return end--级别判断
	--local sprint = "<--"
	local sprint = ""
	for _,v in ipairs{...} do
		sprint = sprint..tostring(v).."  "
	end
	if gUI_CurrentStage == 3 and ver_release and g_global.ShowErrorMessage then
		for v in sprint:gmatch("[^\n]+") do
			g_global.ShowErrorMessage(v)
		end
	end
	--sprint = sprint.."-->"
	sprint:gsub('\n','\nmsk_tg:')
	msk.print(sprint)
	if lv > pt_lv_debug then
		--ShowErrorMessage(sprint)
	end
	if false and gUI_CurrentStage == 3 and not g_hookset then
		_Chat_BroadcastToAllPage(sprint, "0xFFFFB400")
	end
end
print = function (...) return mprint(pt_lv_debug,...) end
local hasPrintt
function PrintT(t,tname,bfirst,notmt)
	if type(t) ~= "table" then 
		if type(t) == "userdata" and not notmt then
			return PrintT(getmetatable(t),ttname,bfirst,notmt)
		end
		print(type(t).." not table")
		return 
	end
	if not bfirst then
		bfirst = true
		hasPrintt = {}
	end
	if hasPrintt[t] then return end
	hasPrintt[t] = true
	tname = tname or ""
	--print(tname.." #:"..#t)
	local ttname
	for k,v in pairs(t) do
		ttname =  tname.."."..k
		if type(v) == "table" then
			PrintT(v,ttname,bfirst,notmt)
		elseif type(v) == "userdata" then
			PrintT(getmetatable(v),ttname,bfirst,notmt)
		elseif tostring(v) then
			print(ttname.." = "..tostring(v))
		else
			print(ttname.." type= "..type(v))
		end
	end
	if not notmt then
		PrintT(getmetatable(t),ttname.."__index.",bfirst,notmt)
	end
	return true
end

msk.ver_test = not ver_release
msk.call = msk.qcall
msk.error = function(err,...)
	print("error:"..err,...)
end
msk.warn = function(err,...)
	print("warn:"..err,...)
end
msk.assert = function(exper,...)
	if not exper then
		local errordata = "脚本失败:"..debug.traceback()
		print("assert failed:",...)
		for line in errordata:gmatch("(.-)\n") do
			print("非法字符-->",line)
		end
		mc_tools.CharExit(errordata)
	end
	return true
end

local loadedmodule = {}
msk.require = function(mname)
	if loadedmodule[mname] then return end
	local f = msk.loadfile(mname)
	assert(type(f)=='function','loaderr:'..tostring(f))
	local ret = f() or true
	if ret then 
		if not msk.ver_test then
			loadedmodule[mname] = ret
		end
		--print("require file ok",mname)
	end
	return ret 
end

msk.pcall = function(fun,...)
	local ret,ret1,ret2re,ret3,ret4,ret5 = pcall(fun,...)
	if not ret then
		msk.warn("pcall err:"..ret1)
		return false
	else
		return ret1,ret2re,ret3,ret4,ret5
	end
end

msk.resumethread = function(thread,...)
	--print("msk.resumethread")
	if not thread then return false,"thread is nil" end
	g_global.runthread = thread
	--print("----------------------resumethread set the rrunthread---------------------")
	local ret,err = coroutine.resume(thread,...)
	if not ret or coroutine.status(thread) == "dead" then 
		print("resumethread error",err)
		g_global.runthread = nil 
		--print("----------------------resumethread nil the rrunthread---------------------")
		ret = nil
	end
	--g_global.runthread = nil
	return ret,err
end
--[[
if flag == "spell" then
	tm = 0.5
	ret = 0
else
	return oldtm-tm-ret,flag 
end
--]]

msk.luasleep = function (tm,flag)
	print("luasleep",tm,flag)
	flag = flag or ""
	local ltm = 0
	if flag == "spell" then
		ltm = 0.8
	end
	local oldtm = tm
	local ret,fl
	while tm > 0 do
		ret,fl = coroutine.yield("msk")
		if fl == flag then 
			tm = ltm
			ret = 0
		end
		tm = tm-ret
	end
	return oldtm,flag
end

msk.waitleadercmd = function (cmd,data)
	if not g_global.teammode then return true end
	while g_global.leadercmd ~= cmd or g_global.leaderdata ~= data do
		sleep(2,cmd)
	end
end

msk.send = function (data)
	mskg.netsend(data.."&")
end
--[[
msk.lockobj = function (id)
	if g_global.objlockptr == nil then
		return
	end
	for i=1,10 do
		if msk.dword(g_global.objlockptr+(i-1)*4) == 0 then
			msk.sdword(g_global.objlockptr+(i-1)*4,id)
			return
		end
	end
	msk.sdword(g_global.objlockptr,id)
end

msk.unlockobj = function (id)
	if g_global.objlockptr == nil then
		return
	end
	for i=1,10 do
		if msk.dword(g_global.objlockptr+(i-1)*4) == id then
			msk.sdword(g_global.objlockptr+(i-1)*4,0)
			return
		end
	end
end

msk.isobjlock = function (id)
	if g_global.objlockptr == nil then
		return false
	end
	for i=1,10 do
		if msk.dword(g_global.objlockptr+(i-1)*4) == id then
			return true
		end
	end
	return false
end
--]]
local guid2double_address = msk.new(8)
msk.guid2double = function(id)
	id = tonumber(id)
	msk.sdword(guid2double_address,4)
	msk.sdword(guid2double_address+4,id)
	return msk.double(guid2double_address)
end

msk.try = function(num,f,...)
	local ok,ret
	for i=1,num do
		ok,ret = mskco.pcall(f,...)
		if ok and ret then return ret end
	end
	return false
end

--开始初始化
package.path = ';'..gvAppPath.."lua\\?.lua"
print('gvAppPath',gvAppPath)
require 'std'
require 'logging.file'
require 'mskco'

InitGlobal()
----[==[
local sigkeys = assert(msk.require("sigkeys"))
--print('sigkeys === ',sigkeys)
--sigkeys = eval(sigkeys)
--print('#gvSigData',#gvSigData)
--print('sigkeys',sigkeys)
local pt = ">"..string.rep('I',#sigkeys)
--print('pt',pt)
local sigvals = {select(2,string.unpack(gvSigData,pt))}
--print('sigvals',sigvals)
assert(#sigkeys == #sigvals)
for i=1,#sigkeys do
	print('set',sigkeys[i],string.format('%x',sigvals[i]))
	sig[sigkeys[i]] = sigvals[i]
end
--]==]

print('--------------------ver_release--------------------:',ver_release)
print("global load ok")