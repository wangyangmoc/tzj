g_player = {}

function GetName(n)
	if n.what == "C" then
		return n.name
	end
	local lc = string.format("[%s]:%s",n.short_src,n.linedefined)
end

local playermt = {}

playermt.__index = function(t,key)
	if key == "chara" then
		return g_player.updatechara("playermt")
	elseif key == "hasteam" then
		local _, has_team = GetPlaySelfProp(6)
		return has_team
	elseif key == "isleader" then
		local _, _, is_leader = GetPlaySelfProp(6)
		return is_leader
	elseif key == "menpainame" then
		return gUI_MenPaiName[gUI_MainPlayerAttr.MenPai]
	elseif key == "name" then
		local name = GetPlaySelfProp(6)
		return name
	elseif key == 'lv' then
		local _,_,_,_,lv = GetPlaySelfProp(4)
		return lv
	elseif key == 'guid' then
		local _, _, _, _, _, _, _,guid = GetMyPlayerStaticInfo()
		return guid
	else
		----[[
		local chara = g_player.chara
		if not chara and not g_player.updatechara(rs)  then
			local errordata = debug.traceback()
			for line in errordata:gmatch("(.-)\n") do
				print("chara get failed",line)
			end
		end
		--]]
		return g_player.chara[key]
	end
end


function g_player:init()
	self.name, self.menpai, self.gender, self.head, self.head_model, self.hair_model, self.hair_color,self.guid = GetMyPlayerStaticInfo()
	self.menpainame = gUI_MenPaiName[self.menpai]
end

function g_player.updatechara(rs)
	print("g_player.updatechara->",rs)
	local ret = GetCharByType(gUI_CharType.CHAR_MAIN_PLAYER,true)
	g_player.chara = ret--GetCharByName(g_player.name,true)
	return ret
end


setmetatable(g_player,playermt)
return g_player