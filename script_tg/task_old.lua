local g_player = g_player
local g_global = g_global
local coroutine = coroutine
local maxlv = 35
local minlv = 0
local taskoption = {}
local curtaskoption
local curstatustimeout = 120--当前状态超时时间
local curstatustime = 0
local canaccpettask
local canaccpettasktype
local canaccpettaskkind
local taskstatus = "ready"
local CTaskObj = {}
local curthread
local tmpsubtask
local taskopdeal
local lasttasklv
local animationwnd
local curruntask
--可以做的支线任务
local speciatask = {
	[388] = true,--斩灭心魔
	[7000] = true,--新手押镖
	[6007] = true,--装备锻造
	[6019] = true,--融魂
	[6032] = true,--了解庄园
}
--快速接取任务
local quickaccepttask = {
	[6007] = true,--装备锻造
	[6019] = true,--融魂
	[6020] = true,--融魂
	[6021] = true,--融魂
	[6022] = true,--融魂
	[6023] = true,--融魂
	[6024] = true,--志同道合 好友任务
}
--引导任务
--AcceptQuest  -1  6019  4  0--融魂
--AcceptQuest  -1  6007  4  0--装备锻造
function AddTaskOption(tm,call,...)
	local t = {}
	t.call = call
	t.tm = tm
	t.args = {...}
	taskoption[#taskoption+1] = t
end

function AddTaskSub(tm,call,...)
	local t = {}
	t.call = call
	t.tm = tm
	t.args = {...}
	curtaskoption.next = t
end

function CreateTaskObj(id,kind)
    o = {}
	o.id = id
	o.kind = kind
    setmetatable(o,CTaskObj)
    CTaskObj.__index = CTaskObj
    return o
end

function ExecuteOption(t)
	if t.tm > 0 then t.tm = t.tm-1 return false end--时机未到
	local root = t
	local lastt
	while t.next do
		lastt = t
		t = t.next 
	end
	curtaskoption = t
	local ret,err = pcall(t.call,unpack(t.args))
	if ret and err then
		if lastt then 
			lastt.next = nil
			return ExecuteOption(root) 
		end
		return true
	end
	return false
end

function TransTaskOption()
	local t,root
	for i=1,#taskoption do
		--数组 一个个的执行 前面的失败了 后面的不执行
		if not ExecuteOption(taskoption[i]) then 
			return 
		end
	end
end

local QuestKindDesc = {
  [0] = "全部任务",
  [1] = "主线",
  [2] = "支线",
  [3] = "副本",
  [4] = "职业",
  [5] = "活动",
  [6] = "引导",
  [7] = "日常",
  [8] = "跑环",
  [9] = "帮会",
  [10] = "押镖"
}

local hasLogedCanAccpetQuest = true
local lastCanAcceptQuest = {}
function FilterQuestEvent(event)
	if event == "QUEST_ADD_CAN_ACCEPT" then
		local nQuestID = tonumber(arg2)
		local questlv = tonumber(arg5)
		local kind = tonumber(arg8)+1
		local strkind = QuestKindDesc[kind]
		if kind > 2 and not speciatask[nQuestID] then 
			print("skip task1",arg1,nQuestID,strkind)
			return 
		end--只支持主线先
		if canaccpettask and questlv >= lasttasklv and not speciatask[nQuestID] then 
			print("skip task2",arg1,nQuestID,strkind)
			return 
		end--支线优先
		lasttasklv = questlv
		canaccpettask = nQuestID
		canaccpettasktype = strkind
		canaccpettaskkind = kind
		print("get a task:",arg1,nQuestID,lasttasklv,strkind)
		return
	elseif event == "QUEST_CAN_ACCEPT_END" then
		hasLogedCanAccpetQuest = true
		_OnQB_AddCanAcceptQuestEnd(arg1)
	end
end

function FilterCanAcceptTask(strTitle, nQuestID)
	if canaccpettask then return end
	canaccpettask = nQuestID
	do return end
	print("filter:"..nQuestID)
	--Tasktest
	--if g_Task[nQuestID] then
		for i=1,#canaccpettask do
			if canaccpettask[i] == nQuestID then
				return true
			end
		end
		canaccpettask[#canaccpettask+1] = nQuestID
	--end
	return true
end

function FilterTrackAddItem(Title, IsComplete, IsFailed, Desc, ReqLevel, MyLevel, QuestId)
	if curruntask and QuestId == curruntask.id then
		print("update task:",Desc)
		curruntask.Desc = Desc
	end
end

function RollBackOption(opt)
	for _,op in pairs(opt) do
		if op.type == "functionhook" then
			mc_tools.UnHookFuncion(op.value)
		elseif op.type == "timer" then
			mc_tools.RemoveTimer(op.value)
		end
	end
end
--血量检测
local ywtm = 0
function CheckHp()
	local pl = g_player.char
	if pl.hp < 80 then
		local curtm = os.time()
		if g_player.menpai == 3 and pl.lv >= 23 and notg_global.isfight  and (curtm -g_global.kmtm) > 15.5 then
			g_global.kmtm = curtm
			WorldStage:SetSkillTargetToSelf(true)
			CastSkill(3915)
			WorldStage:SetSkillTargetToSelf(false)
		elseif (curtm-ywtm) > 31 then
			local slot,bag = GetItemByName("止血散",true)
			if slot then 
				curtm =  ywtm
				UseItem(bag,slot) 
			end
		end
	end
end

--任务控制
function TaskControl(tmer)
    local threadret,threaderr = "","未执行"
    if curthread then threadret,threaderr = msk.resumethread(curthread,1) end--tmer:GetRealInterval()
	if curthread and coroutine.status(curthread) ~= "dead" then --当前有任务执行
		return
	end
    print("thread stop because:"..threaderr)
    curthread = nil
	curruntask = nil
	--检测等级
	local pl = g_player.chara
	if pl.lv > maxlv then 
		return mc_tools.Exit("suc","到达等级-"..pl.lv)
	elseif pl.lv < minlv then
		return mc_tools.Exit("failed","等级太低-"..pl.lv)
	end
	--检查身上的已接任务
	local questNum = GetAcceptedQuestNum()
	local t
	for index = questNum-1, 0, -1 do
		local mapname, mapid, _, strTitle, IsComplete, bFailed, taskid, _, _, _, _, _, QuestKind, _ = GetAcceptedQuestList(index)
		if (curthread == nil and mapname ~= nil and QuestKind <= 2) or (speciatask[taskid])  then--Tasktest
			print("get a acceptedtask:"..strTitle.." "..taskid)
			t = CreateTaskObj(taskid,QuestKind)--g_Task[taskid]
			if bFailed then
				print("restart task")
                curthread = coroutine.create(CTaskObj.Restart)
			elseif IsComplete then
				print("submit task")
                curthread = coroutine.create(CTaskObj.Submit)
			else
				print("run task")
				curthread = coroutine.create(CTaskObj.Run)
			end
			TrackQuest(taskid,true)
		else
			TrackQuest(taskid,false)
		end
	end
	if curthread then return msk.resumethread(curthread,t) end
	canaccpettask = nil
	UpdateCanAcceptTaskList(false)
	--end
	print("check num")
	if not canaccpettask then
		print("check num failed")
		return mc_tools.Exit("failed","没有可接任务")
	end
	TrackQuest(canaccpettask,true)
	t = CreateTaskObj(canaccpettask,canaccpettaskkind)--g_Task[canaccpettask[1]]
    curthread = coroutine.create(CTaskObj.Accept)
    return msk.resumethread(curthread,t)
end


function BeginTaskLoop()
	--动画窗口
	--animationwnd = WindowSys_Instance:GetWindow("MissionPlot")
	--定义处理函数
	taskopdeal = {
		["移动"] = CTaskObj.MoveToCoord,
		["不处理"] = function () return true end,
		["等待"] = CTaskObj.Wait,
		["采集"] = CTaskObj.CollectObj,
		["采集1"] = CTaskObj.CollectObj1,--不需要移动到坐标 直接采集,一般用于传送到副本采集的任务,这些任务并没有采集物的坐标
		["SPE装备门派武器"] = CTaskObj.SpeEquipProWeapon,
		["SPE捕捉酥皮蟾蜍"] = CTaskObj.SpeCaptureToad,
		["SPE解救中毒村民"] = CTaskObj.SpeRescueVillagers,
		["战斗"] = CTaskObj.FightMonster,
		["使用物品"] = CTaskObj.UseItem,
		["护送"] = CTaskObj.Escort,
		["对话"] = CTaskObj.TalkToNpc,
		["对话1"] = CTaskObj.TalkToNpc1,--对话完成后 任务提示没变的 其他都是根据任务提示判断是否完成,这个根据对话框是否消失判断
		["SPE击败自身心魔"] = CTaskObj.SpeSelfDayBreak,
		["飞行战斗"] = CTaskObj.VehicleFight,
	}
	--拦截任务进度
	mc_tools.HookFunction("_OnQB_TrackAddItem",FilterTrackAddItem)
	--拦截可接任务
	mc_tools.HookFunction("Script_QB_OnEvent",FilterQuestEvent)
	--mc_tools.HookFunction("_OnQB_AddCanAcceptQuest",FilterCanAcceptTask)
	--加载任务信息
	msk.require("alltask3")
	tmpsubtask = g_TaskSubInfo
	--[[
	local dir = msk.getglobal("taskdir")
	local f = io.open(dir.."alltask3.lua","r")
	msk.assert(f,"找不到任务文件")
	local taskstr = f:read("*all")
	f:close()
	loadstring("g_TaskSubInfo = {"..taskstr.."}")()
	--]]
	--加入回血检测
	mc_tools.SetTimer(5,"CheckHp",CheckHp)
	--加入任务控制
	mc_tools.SetTimer(1,"TaskControl",TaskControl)
	--去掉泡泡
	Lua_Chat_ApplyPaoPao = function() end
	g_player.updatechara()
end

function AcceptTask(npcid,taskid)
    AcceptQuest(npcid, taskid, DIALOG_STATUS_AVAILABLE, 0) --DIALOG_STATUS_REWARD,-1 为完成任务
end

function SubmitTask(npcid,taskid)
	print("SubmitTask",npcid,taskid)
    AcceptQuest(npcid, taskid, DIALOG_STATUS_REWARD, -1) --DIALOG_STATUS_REWARD,-1 为完成任务
end

function CTaskObj:BeforeAccept()
	if self.beforeaccept then return true end
	self.beforeaccept  = true
	return true
end

function CTaskObj:Restart()
	if GetQuestState(self.id) ~= QUEST_STATUS_FAILED then 
		return false 
	end
	--animationwnd:SetVisible(false)
	local strTitle, mapname, npcname = GetCanAcceptQuestInfo(self.id)
	msk.assert(npcname,"找不到接任务的NPC:"..self.id)
	local x, y, mapid = string.match(npcname, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
    x = tonumber(x)
    y = tonumber(y)
    mapid = tonumber(mapid)
	if not PathTo(x, y, mapid) then return false end
    if mapid == -1 then
        ReAccectQuest(GetNPCObjId(x),self.id)
       msk.luasleep(2,"utask")
    end
    if GetQuestState(self.id) ~= QUEST_STATUS_FAILED then--accept suc
        return true
    end
    return false
end

function CTaskObj:Accept()
	if GetQuestState(self.id) ~= QUEST_STATUS_AVAILABLE then 
		return false 
	end
	local tmout
	--animationwnd:SetVisible(false)
    self:BeforeAccept()
	if quickaccepttask[self.id] then
		AcceptQuest(-1,  self.id,  4,  0)
		msk.luasleep(2,"utask")
	else
		local strTitle, mapname, npcname = GetCanAcceptQuestInfo(self.id)
		local x, y, mapid = string.match(npcname, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		x = tonumber(x)
		y = tonumber(y)
		mapid = tonumber(mapid)
		if not x then
			if npcname == "北落师门" then
				x, y, mapid = 544,50,-1
			elseif npcname == "青帝" then
				x, y, mapid = 443,50,-1
			else
				msk.assert(x,"找不到接任务的NPC:"..self.id,npcname)
				return false
			end
		end
		if not PathTo(x, y, mapid,tmout,2) then return false end
		if mapid == -1 then
			if 7000 == self.id then
				SelectGossipOption(GetNPCObjId(x), 0, 0)
				msk.luasleep(1)
				SelectGossipOption(GetNPCObjId(x), 0, 0)
				msk.luasleep(1)
			else
				AcceptTask(GetNPCObjId(x),self.id)
				msk.luasleep(2,"utask")
			end
		end
		UI_Gossip_CloseBtnClick()
	end
    if GetQuestState(self.id) ~= QUEST_STATUS_AVAILABLE then--accept suc
        return self:AfterAccept() and self:Run()
    end
    return false
end

function CTaskObj:AfterAccept()
	if self.afteraccept then return true end
	self.afteraccept  = true
	return true
end

function CTaskObj:IsObjectiveOk(strObjective)
	--print("check obj:",strObjective)
	if self.sub and #self.sub == 1 then
		if GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then 
			return false
		else
			return true
		end
	end
	if self.subnum == 1 and  GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then
		if GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE then 
			return false
		else
			return true
		end
	end
	strObjective = mc_tools.decodestr(strObjective)
	if not self.Desc then
		curruntask = self
		TrackQuest(self.id,true)
		UpdateTrackQuestList()
	end
	if not self.Desc then
		msk.warn("error UpdateTrackQuestList failed")
		return true
	end
	if string.find(self.Desc,strObjective) == nil then
		print("ObjectiveOk")
		return true
	end
	return false
end

function CTaskObj:BeforeRun()
	if self.beforerun then return true end
	self.beforerun  = true
	return true
end

function CTaskObj:Wait(strObjective)
	print("Wait:")
	if self:IsObjectiveOk(strObjective) then return true end
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self:IsObjectiveOk(strObjective) then return true end
		msk.luasleep(3)
	end
	return true
end
local replaceStr = {
	["溢出的魔气"] = "魔气",
	["真正的阴谋者"] = "青女",
	["孤独心魔"] = "心魔",
	["流言心魔"] = "心魔",
	["争斗心魔"] = "心魔",
}

function CTaskObj:FightMonster(strObjective)
	print("FightMonster:")
	if self:IsObjectiveOk(strObjective) then return true end
	local tarname,x, y, mapid = string.match(strObjective, "{PaTh%^%[(%X+)%]%^[%xx]+%^([+-]?%d+)%^([+-]?%d+)%^([+-]?%d+)}")
	--local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	x, y, mapid = tonumber(x), tonumber(y), tonumber(mapid)
	tarname = replaceStr[tarname] or tarname
	num = num or 1
	--if not PathTo(x, y, mapid) then return false end
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self:IsObjectiveOk(strObjective) then return true end
		FightMonster(tarname,x, y, mapid,120)
	end
	return true
end

function CTaskObj:VehicleFight(strObjective)
	print("FightMonster:")
	if self:IsObjectiveOk(strObjective) then return true end
	local monster,skillid,skilltimer,skilldis
	if self.id == 520 then
		monster = "妖魔探军"
		skillid = 4109
		skilltimer = 3
		skilldis = 40
	else
		msk.warn("不支持的飞行战斗任务")
		return false
	end
	local ch
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		ch = GetCharByName(monster)
		if not ch or ch.dis >= skilldis then
			msk.luasleep(1)
		else
			CastAreaSkill(skillid,ch.x,ch.y)
			msk.luasleep(skilltimer+0.5)
		end
		if self:IsObjectiveOk(strObjective) then return true end
	end
	return true
end

function CTaskObj:UseItem(strObjective)
	print("UseItem:")
	--检查是否使用物品
	if self:IsObjectiveOk(strObjective) then return true end
	local gid = strObjective:match("GoOdSbOx^(%d+)}")
	gid = tonumber(gid)
	local num = 0
	local pl
	local bmove = false
	while gid do
		if num > 5 then return false end
		local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		x = tonumber(x)
		y = tonumber(y)
		mapid = tonumber(mapid)
		if not PathTo(x, y, mapid) then return false end
		if num > 2 and not bmove then
			bmove = true
			pl = g_player.chara
			MoveToTargetLocation(pl.x+math.random(-3,3), pl.y+math.random(-3,3), GetMPlayerMapId())
			msk.luasleep(1)
		end
		if mapid == -1 then
			local npcid = GetNPCObjId(x)
			if GetCurTargetObjId() ~= npcid then
				SelectCharById(npcid)
			end
		end
		local bag,slot = GetItemInBagIndex(gid)
		if not bag then return true end
		print("GetItemInBagIndex:",bag,slot)
		UseItem(bag,slot)
		msk.luasleep(4,"spell")
		if self:IsObjectiveOk(strObjective) then return true end
		num = num + 1
	end
	return true
end

function CTaskObj:SpeEquipProWeapon(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("SpeEquipProWeapon")
	local slot,bag = GetItemByType(2,1)
	if slot then 
		--print("流光",bag,slot)
		UseItem(bag,slot) 
		msk.luasleep(1) 
	end
	--[[
	local slot,bag = GetItemByName("流光")
	if slot then 
		--print("流光",bag,slot)
		UseItem(bag,slot) 
		msk.luasleep(1) 
	end
	slot,bag = GetItemByName("火舞")
	if slot then 
		--print("火舞",bag,slot)
		UseItem(bag,slot) 
		msk.luasleep(1) 
	end
	--]]
	if not self:IsObjectiveOk(strObjective) then
		local pl = g_player.chara
		MoveToTargetLocation(pl.x+math.random(-3,3), pl.y+math.random(-3,3), GetMPlayerMapId())
		return false
	end
	return true
end

function CTaskObj:SpeSelfDayBreak(strObjective)
	print("SpeSelfDayBreak:")
	if self:IsObjectiveOk(strObjective) then return true end
	local tarname = g_player.menpainame.."心魔"
	FightMonster(tarname,x, y, mapid,120)
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:SpeCaptureToad(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	while not GetMyCharByName("酥皮蟾蜍") do
		if GetQuestState(self.id) ~= QUEST_STATUS_INCOMPLETE then return true end
		local gid = strObjective:match("GoOdSbOx^(%d+)}")
		gid = tonumber(gid)
		if gid then
			local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			x = tonumber(x)
			y = tonumber(y)
			mapid = tonumber(mapid)
			local ch = GetCharByName("酥皮蟾蜍")
			if not ch and not PathTo(x, y, mapid) then return false end
			if not ch then ch = GetCharByName("酥皮蟾蜍") end
			if not ch then return false end
			ch:Select()
			local bag,slot = GetItemInBagIndex(gid)
			print("GetItemInBagIndex:",bag,slot)
			UseItem(bag,slot)
			msk.luasleep(4,"spell")
			return self:IsObjectiveOk(strObjective)
		end
	end
	return true
end

function CTaskObj:SpeRescueVillagers(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE  do
		local gid = strObjective:match("GoOdSbOx^(%d+)}")
		gid = tonumber(gid)
		if gid then
			local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
			x = tonumber(x)
			y = tonumber(y)
			mapid = tonumber(mapid)
			if not PathTo(x, y, mapid) then return false end
			local ch = GetCharByName("中毒村民")
			if ch then
				ch:Select()
				local bag,slot = GetItemInBagIndex(gid)
				print("GetItemInBagIndex:",bag,slot)
				UseItem(bag,slot)
				msk.luasleep(4,"spell")
			else
				msk.luasleep(1)
			end
		end
	end
	return true
end

function CTaskObj:Escort(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("CTaskObj Escort:")
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	local num = 0
	local pl = g_player.chara
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self.id == 7000 and pl:Dis(300,73) > 125 then
			Escort(nil,398,159,4)
		end
		Escort(nil,x,y,mapid)
		num = num + 1
		if num > 1 then
			msk.luasleep(2)
			if num > 10 then 
				msk.warn("护送失败次数过多,放弃任务")
				AbandonQuest(self.id)
				msk.luasleep(1)
				return false
			end
		end
	end
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:MoveToCoord(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("MoveToCoord:")
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	if not PathTo(x, y, mapid) then 
		local pl = g_player.chara
		MoveToTargetLocation(pl.x+math.random(-3,3), pl.y+math.random(-3,3), GetMPlayerMapId())
		msk.luasleep(1)
		return false 
	end
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:CollectObj(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("CollectObj:")
	local x, y, mapid
	if self.id == 715 then
		x, y, mapid = 487,493,5
	else
		x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		x, y, mapid = tonumber(x), tonumber(y), tonumber(mapid)
	end
	--if not PathTo(x, y, mapid) then return false end
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if self:IsObjectiveOk(strObjective) then return true end
		if not CollectObj(nil,x, y, mapid,120) then msk.luasleep(1) end
	end
	return true
end

function CTaskObj:CollectObj1(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("CollectObj:")
	if not CollectObj(nil,nil, nil, nil,120) then msk.luasleep(1) end
	return true
end

function CTaskObj:TalkToNpc(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("TalkToNpc:")
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	x, y, mapid = tonumber(x),tonumber(y),tonumber(mapid)
	--msk.assert(mapid == -1,"子任务对话信息有误,地图不是-1 :"..self.id)
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if not PathTo(x, y, mapid) then return false end
		local npcId = gUI.Gossip.CurNpcId
		if mapid ~= -1 then
			local tarname = string.match(strObjective, "{PaTh%^%[(.+)%]%^.+%^[+-]?%d+%^[+-]?%d+%^[+-]?%d+}")
			local ch = GetCharByName(tarname)
			if ch then 
				npcId = ch.id
				if not PathTo(ch.x, ch.y, GetMPlayerMapId(),nil,1) then return false end
				ch:Open()
			end
		end
		if npcId then
			for j=1,8 do
				SelectGossipOption(npcId,0,0)
				msk.luasleep(1)
				if self:IsObjectiveOk(strObjective) then return true end
			end
			--[[
			for j=1,3 do
				SelectGossipOption(npcId,0,0)
				msk.luasleep(1)
				if self:IsObjectiveOk(strObjective) then return true end
			end
			for j=1,5 do
				SelectGossipOption(npcId,0,0)
				msk.luasleep(1)
				if gUI.Gossip.Root:GetProperty("Visible") == "false" then 
					return true 
				end
			end
			--]]
		else
			msk.warn("无法获取对话NPCID:"..self.id)
			break
		end
	end
	return self:IsObjectiveOk(strObjective)
end

function CTaskObj:TalkToNpc1(strObjective)
	if self:IsObjectiveOk(strObjective) then return true end
	print("TalkToNpc1:")
	local x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	x, y, mapid = tonumber(x),tonumber(y),tonumber(mapid)
	--msk.assert(mapid == -1,"子任务对话信息有误,地图不是-1 :"..self.id)
	while GetQuestState(self.id) == QUEST_STATUS_INCOMPLETE do
		if not PathTo(x, y, mapid) then return false end
		local npcId = gUI.Gossip.CurNpcId
		if mapid ~= -1 then
			local tarname = string.match(strObjective, "{PaTh%^%[(.+)%]%^.+%^[+-]?%d+%^[+-]?%d+%^[+-]?%d+}")
			local ch = GetCharByName(tarname)
			if ch then 
				npcId = ch.id
				if not PathTo(ch.x, ch.y, GetMPlayerMapId(),nil,1) then return false end
				ch:Open()
			end
		end
		if npcId then
			for j=1,8 do
				SelectGossipOption(npcId,0,0)
				msk.luasleep(1)
				if self:IsObjectiveOk(strObjective) then return true end
				--if gUI.Gossip.Root:GetProperty("Visible") == "false" then return true end
			end
			break
		else
			msk.warn("无法获取对话NPCID:"..self.id)
			break
		end
	end
	return true
	--return self:IsObjectiveOk(strObjective)
end

function CTaskObj:Run()
	if GetQuestState(self.id) ~= QUEST_STATUS_INCOMPLETE then 
		print("Run err task complete:"..self.id)
		return false 
	end
	self:BeforeRun()
	curruntask = self
	--TrackQuest(self.id,true)
	--animationwnd:SetVisible(false)
	local st = tmpsubtask[self.id]
	local runOk = true
	local dealfun
	if st then
		for i=#st,1,-1 do
			if st[i][2] == "不处理" then
				table.remove(st,i)
			end
		end
		self.sub = st
		self.subnum = #st
		for i=1,#st do
			print("新的子任务",st[i][2],st[i][1])
			if string.find(st[i][1],"QuTrAnS") then
				print("准备传送:"..self.id)
				AcceptQuestTrans(self.id)
				msk.luasleep(1)
			end
			dealfun = taskopdeal[st[i][2]]
			if not dealfun then msk.warn("无法处理的任务类新:"..(st[i][2] or nil)) return false end
			if not dealfun(self,st[i][1]) then msk.warn("任务失败:"..(st[i][2] or nil)) return false end
			self.subnum = self.subnum - 1
		end
	else
		msk.warn("there is no SubInfo:",self.id)
		return false
	end
	print("CTaskObj:Run Over")
	return self:AfterRun() and self:Submit()
end

function CTaskObj:AfterRun()
	if self.afterrun then return true end
	self.afterrun  = true
	return true
end


function CTaskObj:BeforeSubmit()
	if self.beforesubmit then return true end
	self.beforesubmit  = true
	return true
end

function CTaskObj:Submit()
	if GetQuestState(self.id) ~= QUEST_STATUS_COMPLETE then 
		return false 
	end
	--animationwnd:SetVisible(false)
	print("CTaskObj:Submit")
	self:BeforeSubmit()
	local x, y, mapid
	if self.Desc then
		x, y, mapid = string.match(self.Desc, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
	end
	if not x then
		local _, strObjective, _, _, _, _, _, _, NpcName = GetQuestInfoLua(self.id,0)
		msk.assert(NpcName or strObjective,"无法获取任务完成NPC:"..self.id)
		x, y, mapid = string.match(NpcName, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		if not x then
			x, y, mapid = string.match(strObjective, "([+-]?%d+)^([+-]?%d+)^([+-]?%d+)}")
		end
	end
	x = tonumber(x)
	y = tonumber(y)
	mapid = tonumber(mapid)
	msk.assert(x,"找不到完成任务NPC:",GetQuestInfoLua(self.id,0))
	if not PathTo(x, y, mapid) then return false end
    if mapid == -1 then
        SubmitTask(GetNPCObjId(x),self.id)
		msk.luasleep(2,"utask")
    end
    if GetQuestState(self.id) == 4 then--accept suc
        self:AfterSubmit()
        return true
    end
    return false
end

function CTaskObj:AfterSubmit()
	if self.aftersubmit then return true end
	self.aftersubmit  = true
	return true
end

