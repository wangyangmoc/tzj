local g_global = g_globalt
local s_config = s_config
local sleep = mskco.Sleep
g_FunFilter = {}
g_DefaultFunDeal = 4
module("mc_tools",package.seeall)

--------------hook所有游戏API--------------
local sysfun = {
  "assert",
  "collectgarbage",
  "dofile",
  "error",
  "getfenv",
  "getmetatable",
  "ipairs",
  "load","loadfile","loadstring","next","pairs",
  "pcall","print","rawget","rawequal","rawset","select",
  "setfenv","setmetatable","tonumber","tostring","type","unpack",
  "xpcall","IsBaseFun","mprint"
}

local skipFun={
	"GetTimeString","GetAcceptedQuestNum","GetPlaySelfProp","WindowTo%a+","String2Color","GetShowChannelConfig","GetNpcQuestList",
	"PetIsInFight","Lua_Tip[%a_]+"
}

--UIInterface  WorldStage
local skipTable={
	"table","io","string","coroutine","package","math","os","debug","msk","socket","mime","mc_tools","lab2","UIConfig","WindowSys_Instance",
	"lab","AnimateManager_Instance","wnd","_Preview_LastStep1","RenderSys_Instance","_Trade_TradeNote_lbox","p","_RMBWnd_ScrollBar","ImeManager_Instance",
	"gFile","UILoadingBar","_RMBWnd_BuyWnd_New","TimerSys_Instance","win","gUI","hookedFun2","hookedFunf2","hookedFun2","hookedFunf2"
}

local filtertabel = {
	"cpp_private%.%.%?AV.+",
}

function IsSkipTable(name)
	for i=1,#skipTable do
		if skipTable[i] == name then
		  return true
		end
	end
	for i=1,#filtertabel do
		if string.find(name,filtertabel[i]) then
		  return true
		end
	end
  return false
end

function IsBaseFun(k,name)
	if k == "" then k = "_G" end
	if k ~= "_G" then return false end
	for i=1,#sysfun do
		if sysfun[i] == name then
		  return true
		end
	end
	return false
end

function IsSkipFun(tname,name)
	if IsBaseFun(tname,name) then return true end
	if tname == "" then tname = "_G" end
	if tname ~= "_G" then return false end
	for i=1,#skipFun do
		if string.find(name,skipFun[i]) then
		  return true
		end
	end
  return false
end

hookedFun = hookedFun or {}
hookedFunf = hookedFunf or {}
function HookFunction(fname,hfun,isPre,t,tname)
	if not fname then return end
	UnHookFunction(fname,t)
	t = t or _G
	local oldfun = t[fname]
	if not oldfun or type(oldfun) ~= "function" then print("HookFunction failed not a fun",fname) return end
	tname = tname or ""
	if IsSkipFun(tname,fname) then return end
	--print("HookFunction:",fname)
	local newhk = {}
	newhk = {t,fname,oldfun}
	if isPre then
		local newfun = hookedFunf[oldfun]
		if not newfun then
			newfun = function(...)
				if hfun(...) then return end
				return oldfun(...)
			end
		end
		t[fname] = newfun
		newhk[#newhk+1] = newfun
		hookedFunf[oldfun] = newfun
	else
		local newfun = hookedFunf[oldfun]
		if not newfun then
			newfun = function(...)
				local ret = {oldfun(...)}
				hfun(...)
				if ret then
					return unpack(ret)
				end
			end
		end
		t[fname] = newfun
		newhk[#newhk+1] = newfun
		hookedFunf[oldfun] = newfun
	end
	hookedFun[#hookedFun+1] = newhk
end

function UnHookFunction(name,t)
	t = t or _G
    if name then
		for i=1,#hookedFun do
			if hookedFun[i][1] == t and hookedFun[i][2] == name then
				print("suc unhook sig",name)
				t[name] = hookedFun[i][3]
				hookedFunf[hookedFun[i][3]] = nil
				table.remove(hookedFun,i)
				return
			end
		end
		return
	end
	for i=1,#hookedFun do
		t = hookedFun[i][1]
		name = hookedFun[i][2]
		t[name] = hookedFun[i][3]
		--print("suc unhook mul",name)
	end
	hookedFun = {}
	hookedFunf ={}
end

hookedFun2 = hookedFun2 or {}
hookedFunf2 = hookedFunf2 or {}
function HookFunction2(fname,hfun,isPre,t,tname)
	if not fname then return end
	UnHookFunction2(fname,t)
	if not hfun then 
		print("hook failed hfun not set",fname)
		return 
	end
	t = t or _G
	local oldfun = t[fname]
	if not oldfun or type(oldfun) ~= "function" then print("HookFunction failed not a fun",fname) return end
	tname = tname or ""
	if IsSkipFun(tname,fname) then return end
	print("HookFunction2:",fname)
	local newhk = {}
	newhk = {t,fname,oldfun}
	if isPre then
		local newfun = hookedFunf2[oldfun]
		if not newfun then
			newfun = function(...)
				if hfun(...) then return end
				return oldfun(...)
			end
		end
		t[fname] = newfun
		newhk[4] = newfun
		hookedFunf2[oldfun] = newfun
	else
		local newfun = hookedFunf2[oldfun]
		if not newfun then
			newfun = function(...)
				local ret = {oldfun(...)}
				hfun(...)
				if ret then
					return unpack(ret)
				end
			end
		end
		t[fname] = newfun
		newhk[4] = newfun
		hookedFunf2[oldfun] = newfun
	end
	hookedFun2[#hookedFun2+1] = newhk
end

function UnHookFunction2(name,t)
	t = t or _G
    if name then
		for i=1,#hookedFun2 do
			if hookedFun2[i][1] == t and hookedFun2[i][2] == name then
				print("suc unhook sig2",name)
				t[name] = hookedFun2[i][3]
				hookedFunf2[hookedFun2[i][3]] = nil
				table.remove(hookedFun2,i)
				return
			end
		end
		return
	end
	for i=1,#hookedFun2 do
		t = hookedFun2[i][1]
		name = hookedFun2[i][2]
		t[name] = hookedFun2[i][3]
		--print("suc unhook mul",name)
	end
	hookedFun2 = {}
	hookedFunf2 ={}
end

hookedt = hookedt or {}
function HookAll(t,tname,ftname)
	t = t or _G
	tname = tname or ""
	ftname = ftname or ""
	tname = tostring(tname)
	ftname = tostring(ftname)
	if hookedt[t] or IsSkipTable(tname) then return end
	print("hookt:"..ftname)
	hookedt[t] = true
	local fname
	for k,v in pairs(t) do
		fname = ftname..tostring(k)
		if type(v) == "table" then
			HookAll(v,k,fname..".")
		elseif type(v) == "userdata" then
			HookAll(getmetatable(v),k,fname..".")
		elseif type(v) == "function" then
			local newprint = function (...)
				if g_filterhook then
					UnHookFunction2(k,t)
					return
				end
				print(k.."(",...)
			end
			HookFunction2(k,newprint,false,t,ftname)
		end
	end
end

function GetGameVer()
	
end

function UnHookAll()
	UnHookFunction2()
	hookedt = {}
end

function __Lua_QB_ShowCanAcceptQuestInfo(QuestId, BoardTask, BoardParent)
	print("Get a CanAcceptTask:"..QuestId)
end

local hasPrintt = {}
local hasFunPrint = {}
function PrinAllApi(t,tname)
	t = t or _G
	tname = tname or ""
	if hasPrintt[t] then return end
	local truename = tname
	if truename:sub(-1,-1) == "." then truename = truename:sub(1,-2) end
	if IsSkipTable(truename) then print("skip systable:"..truename) return end
	hasPrintt[t] = true
	--print("now hook table:"..tname)
	for k,v in pairs(t) do
		local vname = tname..k
		if type(v) == "function" and not hasFunPrint[v] and not IsBaseFun(tname,k) then
			print([==[["]==]..vname..[==["] = {0},]==])
			hasFunPrint[v] = true
		elseif type(v) == "userdata" then
			PrinAllApi(getmetatable(v),tname..k..".")
		elseif type(v) == "table" then
			PrinAllApi(v,tname..k..".")
		else
			--print("skip:"..k.." = "..type(v))
		end
	end
end

local hasPrintt
function PrintT(t,tname,bfirst)
	if type(t) ~= "table" then 
		if type(t) == "userdata" then
			return PrintT(getmetatable(t),ttname,bfirst)
		end
		msk.print(type(t).." not table")
		return 
	end
	if not bfirst then
		bfirst = true
		hasPrintt = {}
	end
	if hasPrintt[t] then return end
	hasPrintt[t] = true
	tname = tname or ""
	--print(tname.." #:"..#t)
	local ttname
	for k,v in pairs(t) do
		ttname =  tname.."."..k
		if type(v) == "table" then
			PrintT(v,ttname,bfirst)
		elseif type(v) == "userdata" then
			PrintT(getmetatable(v),ttname,bfirst)
		elseif tostring(v) then
			mprint(ttname.." = "..tostring(v))
		else
			mprint(ttname.." type= "..type(v))
		end
	end
	PrintT(getmetatable(t),ttname.."__index.",bfirst)
	return true
end

function LogMapNpc()
	local f = assert(io.open("G:/bitbuket/tg_dll/gamedata/maps.lua","w"))
	local maps = GetMapList()
	local nname
	for k,t in pairs(maps) do
		f:write("id:"..t.MapID," 可见:"..tostring(t.ShowInList),"    大小:"..t.MapSize,"    名字:"..t.MapName,"    描述:"..t.CsSceneDesc,"    是否副本:"..tostring(t.IsInstance),"    PK模式:"..t.PkType,"\n")
		local TargetList = GetTargetList(t.MapID)
		for index, item in spairs(TargetList) do
			nname = item.Name
			if Title and #Title > 0 then nname = nname.."<"..Title..">" end
			f:write("	",nname," xy:",string.format("%.2f",item.X),",",string.format("%.2f",item.Y)," id:"..item.ID," 类型:"..item.Kind," 功能:"..item.Func," 模型:"..item.TemplateID,"\n")
		end
		f:write("\n")
		f:write("\n")
	end
	f:close()
end

function RemoveTimer(name)
	if not g_global.timers[name] then return end
	DelTimerEvent(1,name)
	g_global.timers[name] = nil
end

function CrateTimerClosure(tm,fun)
	local tmsec = tm
	return function (_timer)
		if tmsec > 0 then tmsec = tmsec-1 return end
		tmsec = tm
		return fun(_timer)
	end
end

function SetTimer(tm,name,fun,step)
	RemoveTimer(name)
	step = step or 1
	local newfun = CrateTimerClosure(tm,fun)
	print("set timer:",step, name, newfun)
	AddTimerEvent(step, name, newfun)
	g_global.timers[name] = {step,newfun}
end

function SendConsoleData(data)
	msk.setglobal("sdata",data)
	--msk.setevent(g_global.c_luasign)
end

function ResetControlData()
	for k,v in pairs(g_global.timers) do
		DelTimerEvent(v[1],k)
	end
	InitGlobal()
	mskco.Clear()
end

function SetCmdCallBack(key,thread)
	g_global.cmdcallback[key] = thread
end

function WaitCmd(key)
	SetCmdCallBack(key,coroutine.running())
	local rflag,data
	while true do
		rflag,data = coroutine.yield() 
		if rflag == "cmd" then
			break
		end
	end
	SetCmdCallBack(key,nil)
	return data
end

function CharRestart()
	print("CharRestart")
	ResetControlData()
	SetTimer(0.2,"GameControl",GameControl,2)
	g_global.state = "login"
	BackToLogin()
end

function AccountExit(data)
	msk.send("error#ov")
	msk.send("exit#"..data)
	Exit()
end

--准备退出游戏
function CharExit(data)
	--if msk.ver_test then return end
	print("CharExit")
	g_global.mainthread = nil
	local logger = g_global.logger
	logger:info("总死亡次数:%d",g_global.numDead)
	logger:info("角色结束:%s rs:%s lv:%d",g_player.name,data,g_player.lv)
	g_global.numDead = 0
	mc_tools.SetData('numDead',0)
	local at = g_global.accountdata
	at.charidx = at.charidx+1
	msk.send("result#"..data)
	if at.charidx > 2 then
		msk.send("extern#status=已结束")
		Exit()
	end
	msk.send("extern#charidx="..at.charidx)
	ResetControlData()
	SetTimer(0.2,"GameControl",GameControl,2)
	g_global.state = "login"
	BackToLogin()
end

local timercallidx = 0
function tcall(fun,step,...)
	step = step or 1--时钟
	local curidx = timercallidx
	timercallidx = timercallidx + 1
	local tmthread = coroutine.create(fun)
	local newcaltimerfun = function (tmer)
		local ret,err = msk.resumethread(tmthread,tmer:GetRealInterval())
		if ret then
			return
		end
		print("timercall exit:"..err)
		DelTimerEvent(step,"timercall"..curidx)
	end
	AddTimerEvent(step,"timercall"..curidx,newcaltimerfun)
	msk.resumethread(tmthread,...)
end

local magicstr = {"%","(",")",".","+","-","*","?","[","]","^","$"}
function decodestr(str)
	str = str:gsub("%%d","><")
	for i=1,#magicstr do
		str = str:gsub("%"..magicstr[i],"%%"..magicstr[i])
		--print(i..":"..str)
	end
	str = str:gsub("><","%%d")
	str = str:gsub("[-]?%d+/","%%d/")
	return str
end

function LogData(data)
	local t = g_global.accountdata
	local f = assert(io.open(g_globalt.appdir.."脚本日志.txt","a"))
	f:write(os.date("%c")," ",t.user,"-角色",t.charidx,"-",(t.name or "??")," ",data,"\n")
	f:close()
end

local ctCount = 0
local lastcheckx,lastchecky,lastcheckmapid,lastblockx,lastblocky,lastblockmap,moveblocknum = 0,0,0,0,0,0,0
local poschecktm = 0
function GameControl(_timer)
	if g_global.online == false or true == g_global.pause then 
		return 
	end
	local realtm = _timer:GetRealInterval()
	mskco.Step(realtm)
	poschecktm = poschecktm + realtm
	for k,v in pairs(g_global.timersloop) do
		v(realtm)
	end
	if g_global.notmove ~= true and poschecktm >= 30 then
		poschecktm = 0
		local curx,cury,curmap = g_player.x,g_player.y,GetMPlayerMapId()
		if false == g_global.isbusy and curx == lastcheckx and lastchecky == cury and curmap == lastcheckmapid then
			if lastblockmap == curmap and lastblockx == curx and lastblocky == cury then
				if moveblocknum > 10 then
					lastblockx,lastblocky,lastblockmap,moveblocknum = 0,0,0,0
					print("<-------------------------------------------卡死了------------------------------------------->")
					return mc_tools.CharRestart()
				end
				moveblocknum = moveblocknum + 1
			else
				moveblocknum = 0
				lastblockx,lastblocky,lastblockmap = curx,cury,curmap
			end
			MoveToTargetLocation(curx+math.random(-3,3), cury+math.random(-3,3), curmap)
		else
			moveblocknum = 0
		end
		lastcheckx,lastchecky,lastcheckmapid = curx,cury,curmap
	end
	local idx,thread,ret,flag
	local t,blist,blist1
	while true do
		if #g_global.listcallback1 > 0 then
			t = g_global.listcallback1[1]
			blist1 = true 
		elseif #g_global.listcallback2 > 0 then
			t = g_global.listcallback2[1]
			blist = true
		else
			idx = #g_global.timerscallback
			if idx == 0 then 
				g_global.runthread = nil
				return 
			end
			t = g_global.timerscallback[idx]
		end
		thread = t[1]
		ret,flag = coroutine.resume(thread,realtm)
		if ret and flag == "msk" then 
			g_global.runthread = thread
			return 
		end
		if t[2] then t[2]() end
		if not ret then
			msk.warn("thread error",flag,t[3] or "??",thread)
			if thread == g_global.mainthread then
				LogData("main thread error:"..flag)
				if g_global.mainfun ~= nil then
					g_global.mainfun()
				end
			end
			if t[4] then t[4]() end
		end
		if blist then
			msk.warn("thread remove1",t[3] or "??",thread)
			g_global.sigcallback[t[3]] = nil
			table.remove(g_global.listcallback2,1)
		elseif blist1 then
			msk.warn("thread remove2",t[3] or "??",thread)
			g_global.sigcallback[t[3]] = nil
			table.remove(g_global.listcallback1,1)
		else
			msk.warn("thread remove3",t[3] or "??",thread)
			table.remove(g_global.timerscallback,idx)
		end
	end
end

function GetSigName()
	g_global.signame = g_global.signame+1
	if g_global.signame > 10000 then
		g_global.signame = 1
	end
	return g_global.signame
end

function SetCallBack(notify,name,fun,...)
	local curthread = coroutine.create(fun)
	print("SetCallBack",name or "??",curthread)
	local ret,flag = coroutine.resume(curthread,...)
	if flag == "msk" then
		name = name or GetSigName()
		g_global.sigcallback[name] = curthread
		g_global.runthread = curthread
		g_global.timerscallback[#g_global.timerscallback+1] = {curthread,notify,name}
	else
		msk.warn("SetCallBack error",flag)
		g_global.runthread = nil
	end
end

--先后顺序的 必须执行完之前的 才能继续后面的
function SetListCallBack(notify,name,fun,...)
	local curthread = coroutine.create(fun)
	print("SetListCallBack",name or "??",curthread)
	local ret,flag = coroutine.resume(curthread,...)
	if flag == "msk" then
		g_global.runthread = curthread
		name = name or GetSigName()
		g_global.sigcallback[name] = curthread
		--[[
		if checkerror then
			checkerror = function()
				SetListCallBack(checkerror,notify,name,fun,...)
			end
		else
			checkerror = nil
		end
		--]]
		g_global.listcallback2[#g_global.listcallback2+1] = {curthread,notify,name}
	else
		if not ret then  msk.warn("SetListCallBack error",flag) end
		g_global.runthread = nil
	end
end

--先后顺序的 必须执行完之前的 才能继续后面的
function SetListCallBack1(notify,name,fun,...)
	local curthread = coroutine.create(fun)
	print("SetListCallBack1",name or "??",curthread)
	local ret,flag = coroutine.resume(curthread,...)
	if flag == "msk" then
		g_global.runthread = curthread
		name = name or GetSigName()
		g_global.sigcallback[name] = curthread
		--[[
		if checkerror then
			checkerror = function()
				SetListCallBack(checkerror,notify,name,fun,...)
			end
		else
			checkerror = nil
		end
		--]]
		g_global.listcallback1[#g_global.listcallback1+1] = {curthread,notify,name}
	else
		if not ret then  msk.warn("SetListCallBack error",flag) end
		g_global.runthread = nil
	end
end

function SetLoop(name,fun,tm)
	local _timer = tm
	g_global.timersloop[name] = function (_tm)
		if _timer > 0 then _timer = _timer-_tm return end
		_timer = tm
		return fun()
	end
end

function RemoveLoop(name)
	g_global.timersloop[name] = nil
end

function GameEvent(event)
	
end
function PostToGame(smg)
	_Chat_AddChatText(g_CHAT_TYPE.BROADCASTA, smg)
end

function SetMskStatus(staus)
	PostToGame(staus)
	mc_tools.Notify(staus)
end

function Notify(...)
	print("note:",...)
end

function ConcatFun(fname,...)
	fname = fname.."("
	local t = {...}
	local str
	for i=1,#t do
		if type(t[i]) == "string" then
			str = string.format("%q",t[i])
		else
			str = tostring(t[i])
		end
		if i ~= #t then
			fname = fname..str..","
		else
			fname = fname..str
		end
	end
	fname = fname..t[i]
	return fname
end

function GetFriendGuid(name)
	for k,v in pairs(g_global.friends) do
		if v.name == name then
			return k
		end
	end
	GetAllFriendList()
	for k,v in pairs(g_global.friends) do
		if v.name == name then
			return k
		end
	end
end

local lastid = {}
function ParseCmd(strMsg, name, exInfo, title, guid, menpaiId, GMLev, errorColor)
	local cmd,data = strMsg:match("mskcm#(%a+)#(.+)")
	if not cmd then return false end
	print("ParseCmd",cmd,data)
	if cmd == "jt" and g_global.isteamleader then
		print()
		local tguid  = assert(GetFriendGuid(name))
		InviteJoinTeamByGUID(name,tguid)
	elseif cmd == 'trade' then
		mskco.HandleEvent('trade',name)
	--请求救人 需要在非战斗情况下
	elseif cmd == "help" and not g_global.isfighting and g_global.teammemers[name] and g_player.menpai == 3 and g_player.lv >= 30 and nil == g_global.sigcallback[cmd] then
		local x,y,map = data:match("(-?%d+),(-?%d+),(%d+)")
		x,y,map = tonumber(x),tonumber(y),tonumber(map)
		if x and y and map then
			mc_tools.SetListCallBack(nil,cmd,SavePeople,5,x,y,map,name)
		end
	elseif cmd == "rspt" and g_global.sigcallback[cmd] == nil then
		msk.print('rspt:',data)
		local name,data = data:match("(%a+)#(.+)")
		local f,err =  loadstring(data)
		if not f then
			msk.warn("rspt error",err)
		else
			mc_tools.SetListCallBack(function() SendChatMessage(g_CHAT_TYPE.TELL, "mskcm#rsptok",name) end,cmd,f)
		end
	elseif cmd == "rsptok" and g_global.teammemers[name] then
		g_global.teammemers[name].ready = true
	elseif cmd == "af" then
		local tguid  = assert(GetFriendGuid(name))
		g_global.allowfriends[tguid] = true
	elseif cmd == "jh" then
		local x,y,z = data:match("(.+),(.+),(.+)")
		x,y,z = tonumber(x),tonumber(y),tonumber(z)
		mc_tools.SetListCallBack(nil ,cmd,PathTo,x,y,z)
	elseif cmd == "eh" then
		local tguid  = assert(GetFriendGuid(name))
		local lastnum = lastid[tguid] or 0
		lastnum = lastnum + 1
		if lastnum >= 7 then--10次了
			print('申请进入次数过多 移除权限!!!!!!!!!!!!',name)
			RemoveManorPermission(tguid)
			lastnum = 0
			lastid[tguid] = 0
			return
		end
		lastid[tguid] = lastnum
		--print("AddManorPermission -->",tguid,GetPlayerInfoByGUID(tguid))
		AddManorPermission(tguid)
		SendChatMessage(g_CHAT_TYPE.TELL, "mskcm#ehok",name)
	elseif cmd == "xcj" then
		local tguid  = assert(GetFriendGuid(name))
		g_global.curfarmguid = tguid
		mc_tools.SetListCallBack(nil,cmd,XcjCheck,name)
	elseif cmd == "xcjd" then
		mc_tools.SetListCallBack(nil,xcjd,XcjCheckd,name)
	elseif cmd == "at" or cmd == "st" or cmd == "rt" or cmd == "js" or cmd == "rst" or cmd == "fm" or cmd == "xlz" then
		do return end
		if cmd == "xlz" and g_player.isteamleader then
			local t = g_player.teamtask[cmd] or {}
			local tguid  = assert(GetFriendGuid(name))
			t[tguid] = true
			g_player.teamtask[cmd] = t
		else
			if not g_global.taskload then
				--msk.require"task"
				g_global.taskload = true
			end
			local thread = g_global.sigcallback["TaskControl"]
			if thread then 
				local ret,err = coroutine.resume(thread,cmd,data)
				if not ret then
					msk.warn("TaskControl resume err",err)
				end
			end
		end
	else
		print("skip cmd",cmd,data)
	end
	return true
end

function SetData(key,value)
	msk.send('setdata#'..pickle{[key]=value})
end

function SetTeamData(key,value)
	msk.send('setteamdata#'..pickle{[key]=value})
end

function GetData(key)
	return eval(SendAndWait('getteamdata',key))
end

function SendAndWait(cmd,data)
	msk.send(cmd.."#"..data)
	return mskco.Wait(cmd)
end

function Send(cmd,data)
	msk.send(cmd.."#"..data)
end

function GetData(key)
	return eval(SendAndWait('getdata',key))
end

function GetAcData(key)
	return eval(SendAndWait('getacdata',key))
end

function SetAcData(key,value)
	msk.send('setacdata#'..pickle{[key]=value})
end

function GetGlobalData(t,key)
	return eval(SendAndWait('getgldata',t..'|'..key))
end

function SetGlobalData(t,key,value)
	msk.send('setgldata#'..t..'|'..pickle{[key]=value})
end

function GetConfig(key)
	return GetGlobalData('g_scriptConfig',key)
end