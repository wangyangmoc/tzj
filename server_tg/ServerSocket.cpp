// ServerSocket.cpp : 实现文件
//

#include "stdafx.h"
#include "server_tg.h"
#include "ServerSocket.h"


// CServerSocket

CServerSocket::CServerSocket()
{
}

CServerSocket::~CServerSocket()
{
}


// CServerSocket 成员函数


void CServerSocket::OnAccept(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	CClientSocket *newSocket = new CClientSocket;
	Accept(*newSocket);
	newSocket->AsyncSelect(FD_READ | FD_CLOSE | FD_CONNECT);
	CAsyncSocket::OnAccept(nErrorCode);
}


//void CServerSocket::OnClose(int nErrorCode)
//{
//	// TODO:  在此添加专用代码和/或调用基类
//
//	CAsyncSocket::OnClose(nErrorCode);
//}


//void CServerSocket::OnConnect(int nErrorCode)
//{
//	// TODO:  在此添加专用代码和/或调用基类
//
//	CAsyncSocket::OnConnect(nErrorCode);
//}


//void CServerSocket::OnReceive(int nErrorCode)
//{
//	// TODO:  在此添加专用代码和/或调用基类
//
//	CAsyncSocket::OnReceive(nErrorCode);
//}
// ServerSocket.cpp : 实现文件
//

// CClientSocket
CTypedPtrArray<CObArray, CClientSocket*> g_arrClient;
CClientSocket::CClientSocket()
{
}

CClientSocket::~CClientSocket()
{
}


// CClientSocket 成员函数


void CClientSocket::OnClose(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	for (int i = 0; i < g_arrClient.GetSize(); ++i){
		if (g_arrClient.GetAt(i) == this){
			g_arrClient.RemoveAt(i);
			break;
		}
	}
	CAsyncSocket::OnClose(nErrorCode);
	delete this;
}

void CClientSocket::Decrypt(char *data, int len)
{
	for (int i = 1; i < len; ++i){
		data[i] ^= m_bKey;
	}
}

#include <regex>
#include <map>
void CClientSocket::OnReceive(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	char buff[4096];
	int nRead = Receive(buff, 4096);
	switch (nRead){
	case 0:
	case SOCKET_ERROR:
		Close();
		break;
	default:
		buff[nRead] = 0; //terminate the string
		//登录
		if (buff[0] == 0){
			using namespace std;
			Decrypt(buff, nRead);
			vector<string> vtKeys;
			cmatch result;
			regex pattern(R"([^=,]+)");
			const cregex_iterator end;
			for (cregex_iterator it(buff + 1, buff + nRead+1, pattern); it != end; ++it){
				vtKeys.push_back(it->str());
			}
			if (vtKeys.size() % 2 != 0){
				Close();
				return;
			}
			for (int i = 0; i < vtKeys.size(); i += 2){
				m_mapUserData.insert(make_pair(vtKeys[i], vtKeys[i + 1]));
			}
		}
	}
	CAsyncSocket::OnReceive(nErrorCode);
}


void CClientSocket::Send(char * data,int len)
{
	for (int i = 1; i < len; ++i){
		data[i] ^= m_bKey;
	}
	CAsyncSocket:Send(data,len);
}


void CClientSocket::OnConnect(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	static char sKvar[50] = { 0 };
	if (nErrorCode != 0) 
		Close();
	else{
		g_arrClient.Add(this);
		for (int i = 0; i < 49; ++i){
			sKvar[i] = theTools.GetRandomNum('A', 'z');
		}
		m_bKey = theTools.GetRandomNum(0, 19);
		char sSend[52] = {0};
		sSend[0] = 0;
		sSend[1] = m_bKey;
		memcpy(sSend, sKvar, 49);
		CAsyncSocket:Send(sSend, 51);
		m_bKey = sKvar[m_bKey];
	}
	CAsyncSocket::OnConnect(nErrorCode);
}
