
// server_tg.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "ServerSocket.h"
#include "../SQLiteWrapper/SQLiteWrapper.h"
// Cserver_tgApp: 
// 有关此类的实现，请参阅 server_tg.cpp
//

class Cserver_tgApp : public CWinApp
{
public:
	Cserver_tgApp();

// 重写
public:
	virtual BOOL InitInstance();

	CServerSocket m_sServer;
// 实现
public:
	SQLiteWrapper m_sqlite;

	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern Cserver_tgApp theApp;