#include "stdafx.h"
#include <iostream>
using namespace std;
#include "../mhook-lib/mhook.h"
#define HOOKAPIPRE(fun) auto true##fun = fun
#define HOOKAPI(fun) Mhook_SetHook((LPVOID *)& true##fun, my##fun)
#define HOOKAPINEW(fun) decltype(fun) my##fun
//HOOKAPIPRE(LoadLibraryExW);

auto trueLoadLibraryExW = (decltype(LoadLibraryExW) *)GetProcAddress(GetModuleHandleA("Kernelbase.dll"), "LoadLibraryExW");

HMODULE WINAPI myLoadLibraryExW(wchar_t *name,HANDLE hFile,DWORD dwFlags)
{
	wprintf(L"load:%s\n", name);
	return trueLoadLibraryExW(name,hFile,dwFlags);
}

int _tmain(int argc, _TCHAR* argv[])
{
	Mhook_SetHook((LPVOID *)&trueLoadLibraryExW, myLoadLibraryExW);
	LoadLibraryExW(_T("asfsafsaf"), NULL, 0);
	while (1) Sleep(2000);
	system("pause");
	return 0;
}