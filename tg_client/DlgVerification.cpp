// DlgVerification.cpp : 实现文件
//

#include "stdafx.h"
#include "DlgVerification.h"
#include "afxdialogex.h"

// CDlgVerification 对话框

IMPLEMENT_DYNAMIC(CDlgVerification, CDialogEx)

CDlgVerification::CDlgVerification(lua_State *&l, CWnd* pParent /*=NULL*/)
	:m_l(l), CDialogEx(CDlgVerification::IDD, pParent)
{

}

CDlgVerification::~CDlgVerification()
{
}

void CDlgVerification::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_USER, m_strUser);
	DDX_Text(pDX, IDC_KEY, m_strPwd);
}


BEGIN_MESSAGE_MAP(CDlgVerification, CDialogEx)
	ON_BN_CLICKED(BTN_LOG, &CDlgVerification::OnBnClickedLog)
	ON_BN_CLICKED(BTN_REG, &CDlgVerification::OnBnClickedReg)
	ON_BN_CLICKED(BTN_RECHARGE, &CDlgVerification::OnBnClickedRecharge)
	ON_BN_CLICKED(BTN_REBIND, &CDlgVerification::OnBnClickedRebind)
END_MESSAGE_MAP()


// CDlgVerification 消息处理程序

CListBox * listServer;
int AddServer(lua_State *L)
{
	USES_CONVERSION;
	auto sname = lua_tostring(L, -1);
	listServer->AddString(A2W(sname));
	return 0;
}
void CDlgVerification::OnBnClickedLog()
{
	// TODO:  在此添加控件通知处理程序代码
	auto userData = lt::table(m_l, "gtUserData");
	userData.set("cmd", "log");
	userData.set("serveridx", listServer->GetCurSel());
	auto retTable = AskData();
	if (strcmp("failed", retTable.get<const char *>("result")) == 0){

		MessageBoxA(NULL, retTable.get<const char *>("data"), "失败", MB_OK);
		return;
	}
	//auto globalT = lt::table(m_l, "_G");
	//globalT.set("gtServerData", retTable);
	auto szConfigPath = lt::get<const char *>(m_l, "gvConfigPath");
	WritePrivateProfileStringA("vf", "user", userData.get<const char *>("user"), szConfigPath);
	WritePrivateProfileStringA("vf", "pwd", userData.get<const char *>("pwd"), szConfigPath);
	OnOK();
}


void CDlgVerification::OnBnClickedReg()
{
	// TODO:  在此添加控件通知处理程序代码
	{
		auto userData = lt::table(m_l, "gtUserData");
		userData.set("cmd", "reg");
	}
	auto retTable = AskData();
	MessageBoxA(NULL, retTable.get<const char *>("data"), strcmp("suc", retTable.get<const char *>("result")) == 0 ? "成功":"失败", MB_OK);
}


void CDlgVerification::OnBnClickedRecharge()
{
	// TODO:  在此添加控件通知处理程序代码
	auto userData = lt::table(m_l, "gtUserData");
	userData.set("cmd", "rec");
	auto retTable = AskData();
	MessageBoxA(NULL, retTable.get<const char *>("data"), strcmp("suc", retTable.get<const char *>("result")) == 0 ? "成功" : "失败", MB_OK);
}


BOOL CDlgVerification::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// TODO:  在此添加额外的初始化
	auto szConfigPath = lt::get<const char *>(m_l, "gvConfigPath");
	char szUser[50] = { 0 };
	char szPwd[50] = { 0 };
	GetPrivateProfileStringA("vf", "user", "", szUser, 49, szConfigPath);
	GetPrivateProfileStringA("vf", "pwd", "", szPwd, 49, szConfigPath);

	SetDlgItemTextA(this->GetSafeHwnd(), IDC_USER, szUser);
	SetDlgItemTextA(this->GetSafeHwnd(), IDC_KEY, szPwd);

	/*
	memset(szUser, 0, 50);
	GetPrivateProfileStringA("vf", "host", "23.239.198.7", szUser, 49, szConfigPath);
	lt::set(m_l,"gvHost", szUser);
	lt::set(m_l, "gvPort", GetPrivateProfileIntA("vf", "port", 6112, szConfigPath));
	*/
	auto userData = lt::table(m_l, "gtUserData");
	char mac[20] = { 0 };
	ENSURE_RUNTIME(theTools.GetMac(mac), "无法获取机器码");
	userData.set("mac", mac);
	listServer = (CListBox *)(this->GetDlgItem(IDC_LISTSERVER));
	lua_register(m_l, "AddServer", AddServer);
	BeginWaitCursor();
	SCOPE_EXIT(EndWaitCursor(););
#ifndef _VER_RELEASE
	userData.set("debug", true);
	lt::dofile(m_l, R"(G:\bitbuket\tzj\script_debug\vfinit.lua)");
#else
	lt::dostring(m_l, 
R"==(
package.path = './lua/?.lua;./脚本/?.lua;'
package.cpath = './clibs/?.dll'
--require('mobdebug').start()
require "std"
require "base64"
require "des56"
local http = require "socket.http"
local socket = require("socket")
local gvHost = "23.239.198.7"
local gvPort = 10001
local serverVertions,oldServerVertions,serverTable
function print(...)
	local str = ""
	local t= {...}
	for i=1,#t do
		str = str..tostring(t[i]).." "
	end
	MskPrint(str)
end

local function ServerAssert(v,err,f,...)
	if v then return v end
	if f then f(...) end
	error({result = 'failed',data = err},0)
end

function ParseConfig(data)
	local block,key,value,source
	local t = {}
	local line
	local lines = data:split('\n')
	for i=1, #lines do
		line = lines[i]
		block = line:match'%[(.+)%]'
		if block then
			source = block
			t[block] = {}
		elseif line ~= "" then
			key,value = line:match("(%P+)%s*=%s*(.*)")
			if key then t[source][key] = value end
		end
	end
	return t
end

function GetServerConfig()
	if serverVertions then return end
	serverVertions = {}
	local data,err = http.request('http://launcher.tzj.iwgame.com//ServerList.txt')
	if data then
		local cfg = ParseConfig(data)
		if not cfg.Telecom then return end
		serverTable = {}
		local idx
		local snum = 0
		for k,v in pairs(cfg.Telecom) do
			idx = v:find(',')
			idx = v:sub(1,idx-1)
			idx = tonumber(idx)
			serverTable[idx] = k
			serverVertions[idx] = v:match("%d-,%d-,%d-,.-,(.+),")
			snum = snum + 1
		end
		for i=0,snum-1 do
			AddServer(serverTable[i])
		end
	end
end

local function ConnectServer(sdata)
	GetServerConfig()
	if sdata.serveridx and sdata.serveridx ~= -1 then
		sdata.ver = serverVertions[sdata.serveridx]
		gvServerIdx = sdata.serveridx
		gvServerName = serverTable[gvServerIdx]
		gvGameVer = sdata.ver
	else
		sdata.ver = nil
	end
	print('serverinfo000:',gvServerIdx,gvServerName,gvServerName,gvGameVer)
	print("sdata.ver ",sdata.ver)
	ServerAssert(sdata.cmd ~= 'log' or sdata.ver ~= nil,'没有选择大区')
	ServerAssert(nil ~= sdata.user:match("^[^']+$") and #sdata.user > 0,"输入单引号(')以外的任意字符")
	ServerAssert(nil ~= sdata.pwd:match("^[^']+$") and #sdata.pwd > 0,"输入单引号(')以外的任意字符")
	local c = ServerAssert(socket.connect(gvHost, gvPort),'连接服务器失败')
	local curHost,curPort = c:getpeername()
	ServerAssert(curHost==gvHost and curPort==gvPort,'连接服务器失败 '..curHost..' '..gvHost..' '..curPort..' '..gvPort)
	c:settimeout(5)
	sdata = pickle(sdata)
	sdata = base64.encode(sdata)..'\n'
	ServerAssert(c:send(sdata),'尝试登录失败',c.close,c)
	local data = ServerAssert(c:receive(),'无法接受信息',c.close,c)
	c:close()
	data = base64.decode(data)
	local ret = eval(data)
	gtServerData = ret
	if ret.data.sig then
		gvSigData = ret.data.sig
	end
	return ret
end
function AskData(sSata)
	local ret,err = pcall(ConnectServer,sSata)
	if not ret and type(err) == 'string' then
		err = {result = 'failed',data = "脚本错误:"..err}
	end
	--print('ret',ret,err)
	return err
end
require "mskco"
GetServerConfig()
)==");
#endif
	//PRT("mac = %s", mac);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


lt::table CDlgVerification::AskData()
{
	BeginWaitCursor();
	SCOPE_EXIT(EndWaitCursor(););
	auto userData = lt::table(m_l, "gtUserData");
	char szUser[50] = { 0 };
	char szPwd[50] = { 0 };
	GetDlgItemTextA(this->GetSafeHwnd(), IDC_USER,szUser,49);
	GetDlgItemTextA(this->GetSafeHwnd(), IDC_KEY, szPwd, 49);
	userData.set("user", szUser);
	userData.set("pwd", szPwd);
	//auto sSata = lt::call<const char *>(m_l, "pickle", userData);
	return lt::call<lt::table>(m_l, "AskData", userData);
}

void CDlgVerification::OnBnClickedRebind()
{
	// TODO:  在此添加控件通知处理程序代码
	if (MessageBoxA(NULL, "解绑扣2天,是否确定?", "解绑", MB_OKCANCEL) != IDOK)
		return;
	auto hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	USES_CONVERSION;
	if (INVALID_HANDLE_VALUE != hProcessSnap)
	{
		PROCESSENTRY32 pe32;
		pe32.dwSize = sizeof(PROCESSENTRY32);

		if (Process32First(hProcessSnap, &pe32))
		{
			do
			{
				if (pe32.th32ProcessID != GetCurrentProcessId() && (_tcscmp(pe32.szExeFile, _T("msk.exe")) == 0 || _tcscmp(pe32.szExeFile, _T("Game.exe"))==0 ))
				{
					auto hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
					if (hProcess)
					{
						TerminateProcess(hProcess, 0);
						CloseHandle(hProcess);
					}
				}
			} while (Process32Next(hProcessSnap, &pe32));
		}
		CloseHandle(hProcessSnap);
	}
	auto userData = lt::table(m_l, "gtUserData");
	userData.set("cmd", "rebind");
	auto retTable = AskData();
	MessageBoxA(NULL, retTable.get<const char *>("data"), strcmp("suc", retTable.get<const char *>("result")) == 0 ? "成功" : "失败", MB_OK);
}
