#pragma once
#include "tg_client.h"

// CDlgVerification 对话框

class CDlgVerification : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgVerification)

public:
	CDlgVerification(lua_State *&l,CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgVerification();

// 对话框数据
	enum { IDD = IDD_DLGVF };

	lua_State *&m_l;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedLog();
	afx_msg void OnBnClickedReg();
	afx_msg void OnBnClickedRecharge();
	virtual BOOL OnInitDialog();
	lt::table AskData();
	CString m_strUser;
	CString m_strPwd;
	afx_msg void OnBnClickedRebind();
};
