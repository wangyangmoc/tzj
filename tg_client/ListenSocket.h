#pragma once
#include "afxsock.h"
#include "NewSocket.h"
#include <set>
using namespace std;
class CListenSocket :
	public CAsyncSocket
{
public:
	CListenSocket(void);
	~CListenSocket(void);

	set<CNewSocket *> g_arrSocket; //指向一个连接的socket对象
	virtual void OnAccept(int nErrorCode);
};

