#pragma once


// CMyListCtrl

class CMyListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CMyListCtrl)

public:
	CMyListCtrl();
	virtual ~CMyListCtrl();

protected:
	DECLARE_MESSAGE_MAP()
public:
	int MyInsertItem(int nItem, char *str);
	int MyInsertColumn(int nCol, char* lpszColumnHeading, int nWidth);
	BOOL MySetItemText(int nItem, int nSubItem, char* lpszText);
	void GetSelectItems(lt::table selecttable);
	int MyGetFirstSelectedItemPosition();
	int MyGetNextSelectedItem();

private:
	POSITION m_pos;
};


