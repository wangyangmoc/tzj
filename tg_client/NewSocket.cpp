#include "StdAfx.h"
#include "NewSocket.h"


CNewSocket::CNewSocket(void)
: m_nLength(0)
{
	memset(m_szBuffer, 0, sizeof(m_szBuffer));
}


CNewSocket::~CNewSocket(void)
{
	if (m_hSocket != INVALID_SOCKET)
	{
		Close();
	}
}


void CNewSocket::OnReceive(int nErrorCode)
{
	// TODO: Add your specialized code here and/or call the base class
	m_nLength = Receive(m_szBuffer, sizeof(m_szBuffer), 0); //接收数据
	m_szBuffer[m_nLength] = '\0';
	//接收消息后就开始给客户端发相同的消息
	AsyncSelect(FD_WRITE); //触发OnSend函数
	CAsyncSocket::OnReceive(nErrorCode);
}


void CNewSocket::OnSend(int nErrorCode)
{
	// TODO: Add your specialized code here and/or call the base class
	char m_sendBuf[4096];	//消息缓冲区

	strcpy(m_sendBuf, "server send:");
	strcat(m_sendBuf, m_szBuffer);
	Send(m_sendBuf, strlen(m_sendBuf));

	//触发OnReceive函数
	AsyncSelect(FD_READ);
	CAsyncSocket::OnSend(nErrorCode);
}


void CNewSocket::OnClose(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	AfxMessageBox(_T("OnClose"));
	CAsyncSocket::OnClose(nErrorCode);
}
