// TgClientDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "TgClientDlg.h"
#include "afxdialogex.h"
#include "InputDlg.h"
#include "../nomodule/MemoryModule.h"
#include "tg_clientDoc.h"
#include "tg_clientView.h"
#include "CfgDlg.h"
#include "TgClientDlg.h"
#include <WinSock2.h>
#include <Psapi.h>
// CTgClientDlg 对话框

IMPLEMENT_DYNAMIC(CTgClientDlg, CDialogEx)

CTgClientDlg::CTgClientDlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CTgClientDlg::IDD, pParent), m_l(theApp.m_l)
{

}

CTgClientDlg::~CTgClientDlg()
{
}

void CTgClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//DDX_Control(pDX, EDB_GAMEDIR, m_ctGameDir);
	DDX_Control(pDX, IDC_LIST_ACCOUNT, m_listAccount);
	DDX_Control(pDX, IDC_COMBO_SERVER, m_cbServer);
	DDX_Control(pDX, IDC_COMBO_AREA, m_cbArea);
	DDX_Control(pDX, IDC_COMBO_SCRIPT, m_cbScript);
}


BEGIN_MESSAGE_MAP(CTgClientDlg, CDialogEx)
	ON_BN_CLICKED(BTN_CREATEGAME, &CTgClientDlg::OnBnClickedCreategame)
	ON_NOTIFY(NM_CLICK, IDC_LIST_ACCOUNT, &CTgClientDlg::OnNMClickListAccount)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_ACCOUNT, &CTgClientDlg::OnNMDblclkListAccount)
	ON_CBN_SELCHANGE(IDC_COMBO_AREA, &CTgClientDlg::OnCbnSelchangeComboArea)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_ACCOUNT, &CTgClientDlg::OnNMRClickListAccount)
	ON_COMMAND(ID_SHOWWINDOW, &CTgClientDlg::OnShowwindow)
	ON_COMMAND(ID_RESTART, &CTgClientDlg::OnRestart)
	ON_COMMAND(ID_SHOWDEBUGINFO, &CTgClientDlg::OnShowdebuginfo)
	ON_COMMAND(ID_STOPDEBUG, &CTgClientDlg::OnStopdebug)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_ACCOUNT, &CTgClientDlg::OnNMCustomdrawListAccount)
	ON_BN_CLICKED(IDC_RUNLOG, &CTgClientDlg::OnBnClickedRunlog)
	ON_BN_CLICKED(IDC_CLOSEALL, &CTgClientDlg::OnBnClickedCloseall)
	ON_BN_CLICKED(IDC_HIDESTR, &CTgClientDlg::OnBnClickedHidestr)
END_MESSAGE_MAP()


// CTgClientDlg 消息处理程序


int NetSend(lua_State *L)
{
	SOCKET s = lua_tointeger(L, 1);
	size_t len;
	char* buffer = (char*)luaL_checklstring(L, 2, &len);
	char newbuf[2048];
	memcpy(newbuf, buffer, len);
	for (int i = 0; i < len; ++i){
		newbuf[i] ^= 'm';
	}
	WSABUF DataBuf;
	DWORD SendBytes;
	int err = 0;
	DataBuf.buf = newbuf;
	DataBuf.len = len;
	int rc = WSASend(s, &DataBuf, 1,
		&SendBytes, 0, NULL, NULL);
	if (rc == SOCKET_ERROR){
		err = WSAGetLastError();
	}
	if (WSA_IO_PENDING == err)
		err = 0;
	return 0;
}

int NetSendTo(SOCKET s, char *data, int sa)
{
	sendto(s, data, strlen(data), 0, (sockaddr *)sa, sizeof(*(sockaddr *)sa));
	return 0;
}
#define DLGMODOAL(cls) {cls dlg;dlg.DoModal();}
int GetInput(lua_State *L)
{
	const char *text = lua_tostring(L, 1);
	CInputDlg dlg;
	if (text)
		dlg.m_strInput = text;
	if (IDOK != dlg.DoModal())
		return 0;
	CStringA str(dlg.m_strInput);
	lua_pushstring(L, str.GetBuffer());
	str.ReleaseBuffer();
	return 1;
}

char *lmc_readfile(const char *fname, size_t *sz = NULL)
{
	string strconsole = "mskdata:msk.clientsp.";
	strconsole += fname;
	USES_CONVERSION;
	HANDLE hOpenFile = fCreateFileW(A2W(strconsole.c_str()), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		strconsole = "mskdata:msk.usersp.";
		strconsole += fname;
	}

	hOpenFile = fCreateFileW(A2W(strconsole.c_str()), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		*sz = 0;
		return 0;
	}
	char *pBuffer;
	DWORD RSize;
	DWORD fileSize = 0;
	fileSize = fGetFileSize(hOpenFile, NULL);
	pBuffer = new char[fileSize];
	fReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
	fCloseHandle(hOpenFile);
	//PRT("readfile:%s size:%d", fname, RSize);
	if (sz) *sz = RSize;
	return pBuffer;
}

char* lmc_decodesp(char *data, size_t len)
{
	byte num = *data;
	num = num ^ 0xff;
	byte *newdata = new byte[len - 1];
	for (int i = 0; i < len - 1; ++i){
		newdata[i] = data[i + 1] ^ num;
	}
	delete[]data;
	return (char *)newdata;
}

const char* lmc_getfile(const char *fanme, size_t *sz)
{
#ifndef _VER_RELEASE
	string strconsole = fanme;
	if (strconsole == "") return 0;
	strconsole += ".lua";
	strconsole = "G:\\bitbuket\\tzj\\script_clinet_tg\\" + strconsole;
	USES_CONVERSION;
	HANDLE hOpenFile = fCreateFileW(A2W(strconsole.c_str()), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		*sz = 0;
		return 0;
	}
	char *pBuffer;
	DWORD RSize;
	DWORD fileSize = 0;
	fileSize = fGetFileSize(hOpenFile, NULL);
	pBuffer = new char[fileSize];
	fReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
	fCloseHandle(hOpenFile);
	PRT("readfile:%s size:%d", fanme, RSize);
	if (sz) *sz = RSize;
	return pBuffer;
#else
	//PRT("lmc_getfile: %s now", fanme);
	//string name = "msk.sp.";
	//name += fanme;
	char *data = lmc_readfile(fanme, sz);
	if (0 == data) return 0;
	char* ret = lmc_decodesp(data, *sz);
	size_t newsz = *sz;
	*sz = newsz - 1;
	//PRT("lmc_getfile %s ok", fanme);
	return ret;
#endif
}

int myloader_Lua(lua_State *L) {
	const char *name = luaL_checkstring(L, 1);
	size_t sz;
	const char* result = lmc_getfile(name, &sz);
	if (0 != result){
		luaL_loadbuffer(L, result, sz, name);
		delete[]result;
		if (lua_isfunction(L, -1)){
			return 1;
		}
	}
#ifndef _VER_RELEASE1
	lua_pop(L, 1);
	lua_getglobal(L, "loader_Lua2");
	lua_pushstring(L, name);
	lua_pcall(L, 1, 1, 0);
#else
	lua_pop(L, 1);
	string strconsole = "脚本/";
	strconsole += name;
	strconsole += ".lua";
	if (0 != luaL_loadfile(L, strconsole.c_str())){
		lua_pop(L, 1);
		strconsole = "lua/";
		strconsole += name;
		strconsole += ".lua";
		luaL_loadfile(L, strconsole.c_str());
	}
#endif
	return 1;
}


#define LUA_IGMARK	"-"
#define LUA_POF		"luaopen_"
#define LUA_OFSEP	"_"
#define POF		LUA_POF
static const char *getfuncname(lua_State *L, const char *modname) {
	const char *funcname;
	const char *mark = strchr(modname, *LUA_IGMARK);
	if (mark) modname = mark + 1;
	funcname = luaL_gsub(L, modname, ".", LUA_OFSEP);
	funcname = lua_pushfstring(L, POF"%s", funcname);
	lua_remove(L, -2);  /* remove 'gsub' result */
	return funcname;
}

lua_CFunction mymemloader_C(const char *mname, const char* funname)
{
	USES_CONVERSION;
	HANDLE hOpenFile = fCreateFileW(A2W(mname), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == hOpenFile) {
		return NULL;
	}
	char *pBuffer;
	DWORD RSize;
	DWORD fileSize = 0;
	fileSize = fGetFileSize(hOpenFile, NULL);
	pBuffer = new char[fileSize];
	fReadFile(hOpenFile, pBuffer, fileSize, &RSize, NULL);
	fCloseHandle(hOpenFile);
	HMEMORYMODULE handle = MemoryLoadLibrary(pBuffer);
	delete[]pBuffer;
	if (handle == NULL) return NULL;
	lua_CFunction openfun = (lua_CFunction)MemoryGetProcAddress(handle, funname);
	return openfun;
}

int myloader_C(lua_State *L) {
	const char *name = luaL_checkstring(L, 1);
	lua_getglobal(L, "loader_Lua3");
	lua_pushstring(L, name);
	lua_pcall(L, 1, 1, 0);
	if (lua_isfunction(L, -1)){
		return 1;
	}
	string str = name;
	str = "mskdata:msk.dll." + str;
	const char *funname = getfuncname(L, name);
	lua_CFunction openfun = mymemloader_C(str.c_str(), funname);
	if (openfun == NULL){
		str += ".";
		HMODULE hDll = LoadLibraryA(str.c_str());
		if (hDll == NULL)
			return 1;
		openfun = (lua_CFunction)GetProcAddress(hDll, funname);
		HMODULE  hFind = GetModuleHandleA(str.c_str());
		MODULEINFO mi;
		memset(&mi, 0, sizeof(MODULEINFO));
		GetModuleInformation(GetCurrentProcess(), hFind, &mi, sizeof(mi));
		byte *dllBuffer = new byte[mi.SizeOfImage];
		memcpy(dllBuffer, mi.lpBaseOfDll, mi.SizeOfImage);
		FreeLibrary(hDll);
		LPVOID dllPos = VirtualAlloc(mi.lpBaseOfDll, mi.SizeOfImage, MEM_RESERVE | MEM_COMMIT,
			PAGE_EXECUTE_READWRITE);
		memcpy(dllPos, dllBuffer, mi.SizeOfImage);
		VirtualProtect(dllPos, mi.SizeOfImage, PAGE_EXECUTE, NULL);
		delete[]dllBuffer;
	}
	if (openfun != NULL){
		lua_pop(L, 1);
		lua_pushcfunction(L, openfun);
	}
	return 1;
}

void show_error1(const char* error)
{
	//	MessageBoxA(NULL, error, "脚本错误", 0);
	printf("msk_clinet:error %s\n", error);
	MyDbgPrint(error);
}

int mydofile(lua_State * L)
{
	myloader_Lua(L);
	if (lua_isfunction(L, -1) && lua_pcall(L, 0, LUA_MULTRET, 0) == 0){
		return 1;
	}
	show_error1(lua_tostring(L, -1));
	return 1;
}

BOOL CTgClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	m_listAccount.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES | LVS_EX_GRIDLINES);
	lt::def(m_l, "SetGlobal", SetGlobal);
	lt::def(m_l, "GetGlobal", GetGlobal);
	lt::class_add<POSITION>(m_l, "POSITION");
	lt::class_con<POSITION>(m_l, lua_tinker::constructor<void>());
	lt::class_add<CMyComboBox>(m_l, "CMyComboBox");
	lt::class_def<CMyComboBox>(m_l, "AddString", &CMyComboBox::MyAddString);
	lt::class_def<CMyComboBox>(m_l, "DeleteString", &CMyComboBox::DeleteString);
	lt::class_def<CMyComboBox>(m_l, "GetCurSel", &CMyComboBox::GetCurSel);
	lt::class_def<CMyComboBox>(m_l, "SetCurSel", &CMyComboBox::SetCurSel);
	lt::class_def<CMyComboBox>(m_l, "GetText", &CMyComboBox::MyGetWindowText);
	lt::class_def<CMyComboBox>(m_l, "GetCount", &CMyComboBox::GetCount);
	lt::set(m_l, "g_cbArea", &m_cbArea);
	lt::set(m_l, "g_cbServer", &m_cbServer);
	lt::set(m_l, "g_cbScript", &m_cbScript);

	lt::class_add<CEvent>(m_l, "CEvent");
	lt::class_def<CEvent>(m_l, "SetEvent", &CEvent::SetEvent);
	lt::class_def<CEvent>(m_l, "ResetEvent", &CEvent::ResetEvent);
	lt::set(m_l, "g_etCreateGame", &g_etCreateGame);
	lt::set(m_l, "g_etPlayerSound", &g_etPlayerSound);
	
	lt::class_add<CMyListCtrl>(m_l, "CMyListCtrl");
	lt::class_def<CMyListCtrl>(m_l, "DeleteAllItems", &CMyListCtrl::DeleteAllItems);
	lt::class_def<CMyListCtrl>(m_l, "InsertColumn", &CMyListCtrl::MyInsertColumn);
	lt::class_def<CMyListCtrl>(m_l, "InsertItem", &CMyListCtrl::MyInsertItem);
	lt::class_def<CMyListCtrl>(m_l, "SetItemText", &CMyListCtrl::MySetItemText);
	lt::class_def<CMyListCtrl>(m_l, "GetCheck", &CMyListCtrl::GetCheck);
	lt::class_def<CMyListCtrl>(m_l, "SetCheck", &CMyListCtrl::SetCheck);
	lt::class_def<CMyListCtrl>(m_l, "GetItemState", &CMyListCtrl::GetItemState);
	lt::class_def<CMyListCtrl>(m_l, "GetFirstSelectedItemPosition", &CMyListCtrl::MyGetFirstSelectedItemPosition);
	lt::class_def<CMyListCtrl>(m_l, "GetNextSelectedItem", &CMyListCtrl::MyGetNextSelectedItem);
	lt::set(m_l, "g_listAccount", &m_listAccount);
	//lt::set(m_l, "g_uiConfig", lt::table(m_l, "g_uiConfig"));
	lua_register(m_l, "NetSend", NetSend);
	lua_register(m_l, "UiGetInput", GetInput);
	char szPathA[MAX_PATH] = { 0 };
	GetCurrentDirectoryA(MAX_PATH, szPathA);
	lt::set(m_l, "g_strAppPath", szPathA);
	lua_register(m_l, "dofile", mydofile);
	lt::table pk(m_l, LUA_LOADLIBNAME);
	lua_newtable(m_l);
	lua_setglobal(m_l, "g_savedTeam");
#ifndef _VER_RELEASE
	pk.set("path", R"(..\script_clinet_tg\?.lua;G:\bitbuket\tzj\ver_release_tg\lua\?.lua;G:\bitbuket\tzj\ver_release_tg\脚本\?.lua;)");
	pk.set("cpath", R"(../ver_release_tg/?.dll;./clibs/?.dll;G:\bitbuket\tzj\ver_release_tg\clibs\?.dll;F:/5.1/?.dll;F:/5.1/?51.dll;F:/5.1/clibs/?.dll;F:/5.1/clibs/?51.dll)");
#else
	pk.set("path", R"(?.lua;lua\?.lua;脚本\?.lua;)");
	pk.set("cpath", R"(?.dll;clibs\?.dll;)");
#endif
	//lt:set(m_l,"saveloaders", pk.get<lt::table>("loaders"));
	lua_getglobal(m_l, LUA_LOADLIBNAME);
	lua_getfield(m_l, -1, "loaders");
	lua_rawgeti(m_l, -1, 3);
	lua_setglobal(m_l, "loader_Lua3");
	lua_rawgeti(m_l, -1, 2);
	lua_setglobal(m_l, "loader_Lua2");
	lua_pushcfunction(m_l, myloader_Lua);
	lua_rawseti(m_l, -2, 2);
	lua_pushcfunction(m_l, myloader_C);
	lua_rawseti(m_l, -2, 3);
	lua_pop(m_l, 2);
#ifndef _VER_RELEASE
	lt::dofile(m_l, "G:\\bitbuket\\tzj\\script_clinet_tg\\Start.lua");
#else
	lt::call<void>(m_l, "dofile", "Start");
#endif
	//CString strGameDir = theApp.GetProfileString(_T("lastset"), _T("gamedir"));
	//m_ctGameDir.SetWindowText(strGameDir);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}

void CTgClientDlg::OnNMClickListAccount(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;
	UINT flag;
	int nItem = m_listAccount.HitTest(pNMItemActivate->ptAction, &flag);
	if (flag & LVHT_ONITEMSTATEICON){
		lt::call<void>(m_l, "_UI_Event", "evCheckChange", pNMItemActivate->iItem, pNMItemActivate->iSubItem);
	}
}



void CTgClientDlg::OnBnClickedCreategame()
{
	// TODO:  在此添加控件通知处理程序代码
	lt::call<void>(m_l, "_UI_Event", "evCreateGame");
	//g_etCreateGame.SetEvent();
}



void CTgClientDlg::OnNMDblclkListAccount(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;
	lt::call<void>(m_l, "_UI_Event", "evListDClik", pNMItemActivate->iItem, pNMItemActivate->iSubItem);
}


void CTgClientDlg::OnCbnSelchangeComboArea()
{
	// TODO:  在此添加控件通知处理程序代码

	lt::call<void>(m_l, "_UI_Event", "evAreaChange");
}

int curRpItem;
void CTgClientDlg::OnNMRClickListAccount(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;
	if (-1 == pNMItemActivate->iItem) return;
	curRpItem = pNMItemActivate->iItem;
	CMenu menu;
	menu.LoadMenu(IDR_MENU_ACCOUNT);
	CMenu *pop = menu.GetSubMenu(0);
	POINT point;
	GetCursorPos(&point);
	pop->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}


void CTgClientDlg::OnShowwindow()
{
	// TODO:  在此添加命令处理程序代码
	lt::call<void>(m_l, "_UI_Event", "evShwoWindow", curRpItem);
}


void CTgClientDlg::OnRestart()
{
	// TODO:  在此添加命令处理程序代码
	lt::call<void>(m_l, "_UI_Event", "evRestart", curRpItem);
}


void CTgClientDlg::OnShowdebuginfo()
{
	// TODO:  在此添加命令处理程序代码
	lt::call<void>(m_l, "_UI_Event", "evShowDebugInfo", curRpItem); 
}


void CTgClientDlg::OnStopdebug()
{
	// TODO:  在此添加命令处理程序代码
	lt::call<void>(m_l, "_UI_Event", "evStopDebugInfo", curRpItem);
}


void CTgClientDlg::OnNMCustomdrawListAccount(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	*pResult = 0;
}

void CTgClientDlg::OnBnClickedRunlog()
{
	// TODO:  在此添加控件通知处理程序代码
	static_assert(sizeof(DWORD)==sizeof(long),"error");
	auto it = (CButton*)GetDlgItem(IDC_RUNLOG);
	g_CreateClient = it->GetCheck() == 1;
	if (g_CreateClient)
		lt::call<void>(m_l, "_UI_Event", "evCreateGame");
}


void CTgClientDlg::OnBnClickedCloseall()
{
	// TODO:  在此添加控件通知处理程序代码

	lt::call<void>(m_l, "_UI_Event", "evCloseAll");
	__try{
		auto pnum = 0;
		auto name = "Game.exe";
		auto hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		USES_CONVERSION;
		auto wname = A2W(name);
		if (INVALID_HANDLE_VALUE != hProcessSnap)
		{
			PROCESSENTRY32 pe32;
			pe32.dwSize = sizeof(PROCESSENTRY32);

			if (Process32First(hProcessSnap, &pe32))
			{
				do
				{
					if (_tcscmp(pe32.szExeFile, wname) == 0)
					{
						auto handle =  OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
						TerminateProcess(handle, 0);
						CloseHandle(handle);
					}
				} while (Process32Next(hProcessSnap, &pe32));
			}
			CloseHandle(hProcessSnap);
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER){
	}
}
#include <map>
bool bhideWin = false;
map<HWND, string> mapHwndText;
map<HWND, string> mapHwndClass;

BOOL CALLBACK lpchildEnumProc(HWND hwnd /*窗口内控件句柄*/, LPARAM lParam)
{
	if (!bhideWin)
	{
		auto txt = mapHwndText.find(hwnd);
		if (txt != mapHwndText.end())
			SetWindowTextA(hwnd, txt->second.c_str());
		return TRUE;
	}
	char txt[MAX_PATH] = { 0 };
	GetWindowTextA(hwnd, txt, MAX_PATH);
	if (strlen(txt) == 0) return TRUE;
	auto pairv = make_pair(hwnd, string(txt));
	mapHwndText.insert(pairv);
	SetWindowTextA(hwnd, theTools.GetRandomString(5).c_str());
	return TRUE;
}

BOOL CALLBACK HideWindowsProc(HWND hwnd, DWORD lParam)
{
	// 窗口是否可视
	DWORD pid;
	GetWindowThreadProcessId(hwnd, &pid);
	if (pid != GetCurrentProcessId())
		return TRUE;
	if (!bhideWin)
	{
		auto txt = mapHwndText.find(hwnd);
		if (txt != mapHwndText.end())
			SetWindowTextA(hwnd, txt->second.c_str());
		::EnumChildWindows(hwnd, (WNDENUMPROC)lpchildEnumProc, 0);    //遍历窗口内的控件
		return TRUE;
	}
	// Do something
	char txt[MAX_PATH] = { 0 };
	GetWindowTextA(hwnd, txt, MAX_PATH);
	if (strlen(txt) == 0) return TRUE;
	auto pairv = make_pair(hwnd, string(txt));
	mapHwndText.insert(pairv);
	SetWindowTextA(hwnd, theTools.GetRandomString(5).c_str());
	::EnumChildWindows(hwnd, (WNDENUMPROC)lpchildEnumProc, 0);    //遍历窗口内的控件
	return TRUE;
}

void CTgClientDlg::OnBnClickedHidestr()
{
	// TODO:  在此添加控件通知处理程序代码
	g_bHideWindow = true;
	bhideWin = !bhideWin;
	EnumWindows((WNDENUMPROC)HideWindowsProc, 0);
	g_bHideWindow = false;
}
