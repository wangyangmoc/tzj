#include "stdafx.h"
#include "Thread.h"
#include "tg_clientView.h"
#include "../msktools/msktools.h"

DWORD _CreateGame()
{
	CString strGameDir(GetGlobal("gamedir"));
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	memset(&si, 0, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESHOWWINDOW;//|0x80000
	si.wShowWindow = SW_SHOW;
	BOOL bProcess;
	int pos = strGameDir.ReverseFind(_T('\\'));
	CString strDir = strGameDir.Left(pos);
	strGameDir = strGameDir + _T(" servergrp=");
	strGameDir += GetGlobal("servergrp");
	strGameDir += _T(" ");
	strGameDir = strGameDir + _T("servername=");
	strGameDir += GetGlobal("servername");
	bProcess = CreateProcess(NULL, strGameDir.GetBuffer(strGameDir.GetLength()), NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, strDir, &si, &pi);
	strGameDir.ReleaseBuffer();
	DWORD dError = GetLastError();
	if (!bProcess)
	{
		TRACE(_T("CreateProcess FAiled lasterr:%d"), dError);
		return FALSE;
	}
	CStringA strDll(g_strDllPath);
	Inject(pi.dwProcessId, strDll.GetBuffer(strDll.GetLength()));
	strDll.ReleaseBuffer();
	ResumeThread(pi.hThread);
	return pi.dwProcessId;
}
//防卡 所以开一个线程
UINT CreateGameThread(LPVOID param)
{
	while ( true)
	{
		 if (WAIT_OBJECT_0 == WaitForSingleObject(g_etCreateGame,INFINITE)){
			 DWORD pid = _CreateGame();
			 if (pid != 0){

			 }
		 }
		 Sleep(15000);
	}

}

#include <playsoundapi.h>
UINT PlayerSoundThread(LPVOID param)
{
	while (true)
	{
		DWORD ret = WaitForSingleObject(g_etPlayerSound,INFINITE);
		if (ret == WAIT_OBJECT_0 && g_etPlayerSound){
#ifndef _VER_RELEASE
			PlaySoundA(R"(../ver_release_tg/资源/封号警告.WAV)", NULL, SND_FILENAME);
#else
			PlaySoundA(R"(./资源/封号警告.WAV)", NULL, SND_FILENAME);
#endif
			
		}
	}
}